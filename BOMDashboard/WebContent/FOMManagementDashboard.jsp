<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://www.fusioncharts.com/jsp/core" prefix="fc"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>

<%
Boolean providedDateExists = false; 
String providedDate;
String providedDateFormatted;
%>
<c:set var="now" value="<%=new java.util.Date()%>" scope="request" />
<fmt:formatDate value='${now}' pattern='yyyyMMdd' var='myToday'/>

<% 
providedDate = (String) request.getParameter("date");
if(providedDate != null && !providedDate .equals("")){
	providedDateExists = true;
	SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	Date date = format1.parse(providedDate);
	providedDateFormatted = format2.format(date);
%>
	<c:set var="myToday" value="<%= providedDateFormatted %>" />
<%}
InputStream input = null;
try {
	input = new FileInputStream("/var/bloomnet/cfg/reports.properties");
} catch (FileNotFoundException e) {
	e.printStackTrace();
}	
Properties props = new Properties();
try {
	props.load(input);
} catch (IOException e) {
	e.printStackTrace();
} 
%>

<c:set var='tloColor' value='349400' />
<c:set var='bmtfsiColor' value='4c145e' />
<c:set var='tfsiColor' value='dd0178' />

<c:set var='x069Color' value='6b67ae' />
<c:set var='p539Color' value='006400' />
<c:set var='c573Color' value='de3a6e' />

<c:set var='waitingColor' value='349400' />
<c:set var='todayColor' value='F0F000' />
<c:set var='prevColor' value='FF0000' />
<c:set var='futureColor' value='0000FF' />
<c:set var='waitingColorWFR' value='99FF99' />
<c:set var='todayColorWFR' value='F0F099' />
<c:set var='prevColorWFR' value='FF9999' />
<c:set var='futureColorWFR' value='9999FF' />

<c:set var='x069AColor' value='6b67ae' />
<c:set var='x069CColor' value='FFFFFF' />
<c:set var='x069RColor' value='XXXXXX' />

<c:set var="folderPath" value="FusionCharts/" />
<c:set var="title" value="FOM Management Dashboard" scope="request" />
<c:set var="header1" value="FOM Management Dashboard" scope="request" />
<c:set var="header2" value="${myToday}" scope="request" />
<c:set var="jsPath" value="${folderPath}" scope="request" />
<c:set var="assetCSSPath" value="JSP/assets/ui/css/" scope="request" />
<c:set var="assetJSPath" value="JSP/assets/ui/js/" scope="request" />
<c:set var="assetImagePath" value="JSP/assets/ui/images/" scope="request" />

<sql:setDataSource var="BOM" driver="com.mysql.cj.jdbc.Driver"
     url="jdbc:mysql://localhost/bloomnetordermanagement"
     user='<%=props.getProperty("dbuser") %>'  password='<%=props.getProperty("dbpass") %>'/>



<%-- BUILD SUMMARY CHART --%>
<c:set var="summaryXML"
	value="<chart caption='Routing Summary' subCaption='' showBorder='1' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' xAxisName='Route' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}'>" />

<c:set var="strQuery" value="SELECT bomorderview_v2.Route as 'Route', COUNT(*) as 'Number' FROM    bomorderview_v2 WHERE  bomorderview_v2.ParentOrderDate >='${myToday}' GROUP BY bomorderview_v2.Route"/>
<sql:query dataSource="${BOM}" var="entries" sql="${strQuery}">
</sql:query>

<%-- USED FOR COMBINING TLO AND FAX --%>
<c:set var="myTLO" value="0" />
<c:set var="myTotal" value="0" />
<c:forEach var="row" items="${entries.rows}">
	<c:choose>
		<c:when test="${row.Route eq 'FAXoutbound' || row.Route eq 'TLO'}">
			<c:set var="myTLO" value="${myTLO + row.Number}" />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:when>
		<c:when test="${row.Route eq 'BMTFSI'}">
			<c:set var="route" value="${row.Route}" />
			<c:set var="count" value="${row.Number}" />
			<c:set var="setElem" value="<set label='${route}' value='${count}'  color='${bmtfsiColor}'/>" />
			<c:set var="summaryXML" value="${summaryXML}${setElem} " />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:when>
		<c:when test="${row.Route eq 'TFSI'}">
			<c:set var="route" value="${row.Route}" />
			<c:set var="count" value="${row.Number}" />
			<c:set var="setElem" value="<set label='${route}' value='${count}'  color='${tfsiColor}'/>" />
			<c:set var="summaryXML" value="${summaryXML}${setElem} " />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:when>
		<c:otherwise>
			<c:set var="route" value="${row.Route}" />
			<c:set var="count" value="${row.Number}" />
			<c:set var="setElem" value="<set label='${route}' value='${count}' />" />
			<c:set var="summaryXML" value="${summaryXML}${setElem} " />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:otherwise>
	</c:choose>
</c:forEach>
<c:set var="setElem" value="<set label='TLO' value='${myTLO}' color='${tloColor}'/>" />
<c:set var="summaryXML" value="${summaryXML}${setElem} " />
<c:set var="summaryXML" value="${summaryXML}</chart> " />


<%-- BUILD CURRENT STATUS CHART --%>
<c:set var="statusXML"
	value="<chart caption='Universal Orders in Queue' subCaption='(Lighter shades represent WFR orders)' showBorder='1' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' xAxisName='' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}' showLegend='0'>" />

<c:set var="strQuery" value="SELECT  bv2.Status as 'Status', bv2.DeliveryDate as 'DeliveryDate',  IF(bv2.DeliveryDate='${myToday}','Today',(IF(bv2.DeliveryDate<'${myToday}','Previous','Future'))) as 'DateGrouping' FROM    bomorderview_v2  bv2 inner join bomorder bo on bo.BOMOrder_ID = bv2.ParentBOMid  WHERE  ( bv2.Sts_Routing_ID IN (1,2,10,13) ) and bv2.virtualqueue_id in (3,4)" />
<sql:query dataSource="${BOM}" var="entries2" sql="${strQuery}">
</sql:query>

<%-- GO THROUGH EACH COLUMN IN ROW --%>
<c:set var="myTotalOut" value="0" />
<c:set var="myTotalWFR" value="0" />
<c:set var="myPrevOut" value="0" />
<c:set var="myPrevWFR" value="0" />
<c:set var="myTodayOut" value="0" />
<c:set var="myTodayWFR" value="0" />
<c:set var="myFutureOut" value="0" />
<c:set var="myFutureWFR" value="0" />
<c:set var="myActive" value="0" />
<c:forEach var="row" items="${entries2.rows}">
	<c:choose>
		<c:when test="${row.DateGrouping eq 'Previous' && (row.Status eq 'To be worked' || row.Status eq 'Actively being worked' || row.Status eq 'Cancelled by Sending Shop') }">
			<c:set var="myTotalOut" value="${myTotalOut + 1}" />
			<c:set var="myPrevOut" value="${myPrevOut + 1}" />
		</c:when>
		<c:when test="${row.DateGrouping eq 'Today' && (row.Status eq 'To be worked' || row.Status eq 'Actively being worked' || row.Status eq 'Cancelled by Sending Shop') }">
			<c:set var="myTotalOut" value="${myTotalOut + 1}" />
			<c:set var="myTodayOut" value="${myTodayOut + 1}" />
		</c:when>
		<c:when test="${row.DateGrouping eq 'Future' && (row.Status eq 'To be worked' || row.Status eq 'Actively being worked' || row.Status eq 'Cancelled by Sending Shop') }">
			<c:set var="myTotalOut" value="${myTotalOut + 1}" />
			<c:set var="myFutureOut" value="${myFutureOut + 1}" />
		</c:when>
		<c:when test="${row.DateGrouping eq 'Previous' && (row.Status eq 'Waiting for Response') }">
			<c:set var="myTotalWFR" value="${myTotalWFR + 1}" />
			<c:set var="myPrevWFR" value="${myPrevWFR + 1}" />
		</c:when>
		<c:when test="${row.DateGrouping eq 'Today' && (row.Status eq 'Waiting for Response') }">
			<c:set var="myTotalWFR" value="${myTotalWFR + 1}" />
			<c:set var="myTodayWFR" value="${myTodayWFR + 1}" />
		</c:when>
		<c:when test="${row.DateGrouping eq 'Future' && (row.Status eq 'Waiting for Response') }">
			<c:set var="myTotalWFR" value="${myTotalWFR + 1}" />
			<c:set var="myFutureWFR" value="${myFutureWFR + 1}" />
		</c:when>
	</c:choose>
</c:forEach>

<c:set var="setElem" value="<categories><category label='Total' /><category label='Prior' /><category label='Today' /><category label='Future' /></categories>"/>
<c:set var="statusXML" value="${statusXML}${setElem} " />
<c:set var="setElem" value="<dataset seriesName='Other' ><set value='${myTotalOut}' color='${waitingColor}' /><set value='${myPrevOut}' color='${prevColor}' /><set value='${myTodayOut}' color='${todayColor}' /><set value='${myFutureOut}' color='${futureColor}' /></dataset>"/>
<c:set var="statusXML" value="${statusXML}${setElem} " />
<c:set var="setElem" value="<dataset seriesName='WFR'><set value='${myTotalWFR}'  color='${waitingColorWFR}' /><set value='${myPrevWFR}' color='${prevColorWFR}' /><set value='${myTodayWFR}' color='${todayColorWFR}' /><set value='${myFutureWFR}' color='${futureColorWFR}' /></dataset>"/>
<c:set var="statusXML" value="${statusXML}${setElem} " />

<c:set var="setElem" value="</chart>"/>
<c:set var="statusXML" value="${statusXML}${setElem} " />




<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<HTML>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="refresh" content="900" />

        <title><c:out value="${title}"/></title>
        <!-- ../assets/ui/css/ ../assets/ui/js/ ../assets/ui/images/ -->
        <link href="${assetCSSPath}style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="${assetJSPath}jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="${assetJSPath}lib.js"></script>
        <!--[if IE 6]>
          <script type="text/javascript" src="${assetJSPath}DD_belatedPNG_0.0.8a-min.js"></script>
          <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
        <![endif]-->

        <style type="text/css">
            h2.headline {
                font: normal 110%/137.5% "Trebuchet MS", Arial, Helvetica, sans-serif;
                padding: 0;
                margin: 25px 0 25px 0;
                color: #7d7c8b;
                text-align: center;
            }
            p.small {
                font: normal 68.75%/150% Verdana, Geneva, sans-serif;
                color: #919191;
                padding: 0;
                margin: 0 auto;
                width: 664px;
                text-align: center;
            }
        </style>
        <!-- 
        //You need to include the following JS file, if you intend to embed the chart using JavaScript.
        //Embedding using JavaScripts avoids the "Click to Activate..." issue in Internet Explorer
        //When you make your own charts, make sure that the path to this JS file is correct. Else, you
        //would get JavaScript errors.
        -->
        <SCRIPT LANGUAGE="Javascript" SRC="${jsPath}FusionCharts.js"></SCRIPT>
        <script type="text/javascript">
	
	function myTimeStamp() {
	var d=new Date();
	document.getElementById('myTime').innerHTML = <% if(providedDateExists){ %> "Delivery Date: <%= providedDate %>" <% }else{ %> "Delivery Date: " + (d.getMonth()+1)+"/"+d.getDate()+"/"+(d.getUTCFullYear());<%} %>
	}
	</script>
		

        

    </head>
    <BODY onload="myTimeStamp();">

        <div id="wrapper">

            <div id="header">
                <div class="back-to-home"><a href="javascript:window.location.reload();">Refresh Data</a></div>

                <div class="logo"><a class="imagelink"  href="#">
                <img src="${assetImagePath}fusionchartsv3.2-logo.png" width="131" height="75" alt="FusionCharts v3.2 logo" /></a></div>
                <h1 class="brand-name">FOM Management Dashboard</h1>
                <h1 class="logo-text"><div id="myTime"></div></h1>
            </div>

            <div class="content-area">
                <div id="content-area-inner-main">
                    <h2 class="headline"><c:out value="${myTotal} Orders Received into FOM"/></h2>
                    <div class="gen-chart-render">
<table>
<tr>

<tags:BOMtemplate2>

<fc:render chartId="Summary"
swfFilename="${folderPath}Pie3D.swf"
width="900" height="300"
debugMode="false" registerWithJS="false" xmlData="${summaryXML}" />
<br/>
<fc:render chartId="Status"
swfFilename="${folderPath}MSBar2D.swf"
width="900" height="300"  
debugMode="false" registerWithJS="false" xmlData="${statusXML}" />

</tags:BOMtemplate2>


                    </tr>
                    </table>

                    </div>
                    <div class="clear"></div>
                    <p>&nbsp;</p>
                    <p class="small"> ${intro} 
                    </p>

                    <div class="underline-dull"></div>
                </div>
            </div>

            <div id="footer">
            </div>
        </div>

    </BODY>
</HTML>


