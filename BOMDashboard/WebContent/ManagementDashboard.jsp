<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tags" tagdir="/WEB-INF/tags"%>
<%@ taglib uri="http://www.fusioncharts.com/jsp/core" prefix="fc"%>
<%@ taglib prefix="sql" uri="http://java.sun.com/jsp/jstl/sql"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.io.InputStream"%>
<%@page import="java.io.FileInputStream"%>
<%@page import="java.util.Properties"%>
<%@page import="java.io.FileNotFoundException"%>
<%@page import="java.io.IOException"%>


<%
Boolean providedDateExists = false; 
String providedDate;
String providedDateFormatted;
%>
<c:set var="now" value="<%=new java.util.Date()%>" scope="request" />
<fmt:formatDate value='${now}' pattern='yyyyMMdd' var='myToday'/>

<% 
providedDate = (String) request.getParameter("date");
if(providedDate != null && !providedDate .equals("")){
	providedDateExists = true;
	SimpleDateFormat format1 = new SimpleDateFormat("MM/dd/yyyy");
	SimpleDateFormat format2 = new SimpleDateFormat("yyyy-MM-dd");
	Date date = format1.parse(providedDate);
	providedDateFormatted = format2.format(date);
	
%>
	<c:set var="myToday" value="<%= providedDateFormatted %>" />
<%}
InputStream input = null;
try {
	input = new FileInputStream("/var/bloomnet/cfg/reports.properties");
} catch (FileNotFoundException e) {
	e.printStackTrace();
}	
Properties props = new Properties();
try {
	props.load(input);
} catch (IOException e) {
	e.printStackTrace();
} 
%>



<c:set var='tloColor' value='DD9100' />
<c:set var='bmtfsiColor' value='078328' />
<c:set var='tfsiColor' value='CC0000' />

<c:set var='x069Color' value='0000FF' />
<c:set var='p539Color' value='0000FF' />
<c:set var='c573Color' value='0000FF' />

<c:set var='waitingColor' value='0000FF' />
<c:set var='todayColor' value='FFFFFF' />
<c:set var='prevColor' value='CC0000' />
<c:set var='futureColor' value='078328' />
<c:set var='waitingColorWFR' value='CC0000' />
<c:set var='todayColorWFR' value='078328' />
<c:set var='prevColorWFR' value='FFFFFF' />
<c:set var='futureColorWFR' value='078328' />

<c:set var='x069AColor' value='078328' />
<c:set var='x069CColor' value='FFFFFF' />
<c:set var='x069RColor' value='CC0000' />

<c:set var="folderPath" value="FusionCharts/" />
<c:set var="title" value="BOM Management Dashboard" scope="request" />
<c:set var="header1" value="BOM Management Dashboard" scope="request" />
<c:set var="header2" value="${myToday}" scope="request" />
<c:set var="jsPath" value="${folderPath}" scope="request" />
<c:set var="assetCSSPath" value="JSP/assets/ui/css/" scope="request" />
<c:set var="assetJSPath" value="JSP/assets/ui/js/" scope="request" />
<c:set var="assetImagePath" value="JSP/assets/ui/images/" scope="request" />

<sql:setDataSource var="BOM" driver="com.mysql.cj.jdbc.Driver"
     url='<%=props.getProperty("dbbaseurl") %>'
     user='<%=props.getProperty("dbuser") %>'  password='<%=props.getProperty("dbpass") %>'/>




<%-- BUILD SUMMARY CHART --%>
<c:set var="summaryXML"
	value="<chart caption='Routing Summary' subCaption='' showBorder='1' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' xAxisName='Route' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}'>" />

<c:set var="strQuery" value="SELECT bomorderview_v2.Route as 'Route', COUNT(*) as 'Number' FROM bomorderview_v2 WHERE  bomorderview_v2.ParentBOMId IN (select DISTINCT ParentOrder_ID from bomorder bo where bo.deliveryDate ='${myToday}') GROUP BY bomorderview_v2.Route"/>
<sql:query dataSource="${BOM}" var="entries" sql="${strQuery}">
</sql:query>

<%-- USED FOR COMBINING TLO AND FAX --%>
<c:set var="myTLO" value="0" />
<c:set var="myTotal" value="0" />
<c:forEach var="row" items="${entries.rows}">
	<c:choose>
		<c:when test="${row.Route eq 'FAXoutbound' || row.Route eq 'TLO'}">
			<c:set var="myTLO" value="${myTLO + row.Number}" />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:when>
		<c:when test="${row.Route eq 'BMTFSI'}">
			<c:set var="route" value="BloomNet" />
			<c:set var="count" value="${row.Number}" />
			<c:set var="setElem" value="<set label='${route}' value='${count}'  color='${bmtfsiColor}'/>" />
			<c:set var="summaryXML" value="${summaryXML}${setElem} " />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:when>
		<c:when test="${row.Route eq 'TFSI'}">
			<c:set var="route" value="Teleflora" />
			<c:set var="count" value="${row.Number}" />
			<c:set var="setElem" value="<set label='${route}' value='${count}'  color='${tfsiColor}'/>" />
			<c:set var="summaryXML" value="${summaryXML}${setElem} " />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:when>
		<c:otherwise>
			<c:set var="route" value="Manual Queue" />
			<c:set var="count" value="${row.Number}" />
			<c:set var="setElem" value="<set label='${route}' value='${count}' />" />
			<c:set var="summaryXML" value="${summaryXML}${setElem} " />
			<c:set var="myTotal" value="${myTotal + row.Number}" />
		</c:otherwise>
	</c:choose>
</c:forEach>
<c:set var="setElem" value="<set label='Manual Queue' value='${myTLO}' color='${tloColor}'/>" />
<c:set var="summaryXML" value="${summaryXML}${setElem} " />
<c:set var="summaryXML" value="${summaryXML}</chart> " />


<%-- BUILD CURRENT STATUS CHART --%>
<c:set var="statusXML"
	value="<chart caption='Order Statuses'  showBorder='1' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' xAxisName='' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}' showLegend='0'>" />

<c:set var="strQuery" value=" SELECT count(*) as total, SUM(IF(boss.sts_routing_id in(1,2,8,13),1,0)) as unassigned, SUM(IF(boss.sts_routing_id in(3,4,5,7,12,14,15,16,17,18),1,0)) as assigned, SUM(IF(boss.sts_routing_id in (9),1,0)) as rejected, SUM(IF(boss.sts_routing_id in (10,11),1,0)) as canceled, SUM(IF(boss.sts_routing_id=6,1,0))  as dlcf FROM bomorderview_v2 boss where boss.ParentBOMId IN (select DISTINCT ParentOrder_ID from bomorder bo where bo.deliveryDate ='${myToday}');" />
<sql:query dataSource="${BOM}" var="entries2" sql="${strQuery}">
</sql:query>

<%-- GO THROUGH EACH COLUMN IN ROW --%>

<c:forEach var="row" items="${entries2.rows}">
<c:set var="total" value="${row.total}" />
<c:set var="unassigned" value="${row.unassigned}" />
<c:set var="assigned" value="${row.assigned}" />
<c:set var="rejected" value="${row.rejected}" />
<c:set var="canceled" value="${row.canceled}" />
<c:set var="dlcf" value="${row.dlcf}" />

</c:forEach>

<c:set var="setElem" value="<categories><category label='Total Orders' /><category label='Assigned' /><category label='Delivered' /><category label='Unassigned' /><category label='Rejected' /><category label='Cancelled' /></categories>"/>
<c:set var="statusXML" value="${statusXML}${setElem} " />
<c:set var="setElem" value="<dataset seriesName='today' ><set value='${total}' color='${waitingColor}' /><set value='${assigned}' color='${futureColor}' /><set value='${dlcf}' color='${todayColorWFR}' /><set value='${unassigned}' color='${prevColor}' /><set value='${rejected}'  color='${waitingColorWFR}' /><set value='${canceled}' color='${prevColorWFR}' /></dataset>"/>
<c:set var="statusXML" value="${statusXML}${setElem} " />


<c:set var="setElem" value="</chart>"/>
<c:set var="statusXML" value="${statusXML}${setElem} " />


<%-- BUILD Avas CHART --%>
<c:set var="avasXML"
	value="<chart caption='Avas Flowers: Current Status' subCaption='' showBorder='1' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' xAxisName='' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}'>" />

<c:set var="strQuery" value="SELECT SUM(IF(bomorderview_v2.SendingShop_ID = 808,1,0)) as 'Processed', SUM(IF((bomorderview_v2.SendingShop_ID = 808 and (bomorderview_v2.Sts_Routing_ID!=9 and bomorderview_v2.Sts_Routing_ID!=10 and bomorderview_v2.Sts_Routing_ID!=11)),1,0)) as 'Accepted', SUM(IF((bomorderview_v2.SendingShop_ID = 808 and (bomorderview_v2.Sts_Routing_ID=9)),1,0)) as 'Rejected', SUM(IF((bomorderview_v2.SendingShop_ID = 808 and (bomorderview_v2.Sts_Routing_ID=10 or bomorderview_v2.Sts_Routing_ID=11)),1,0)) as 'Cancelled' FROM    bomorderview_v2 WHERE  bomorderview_v2.ParentBOMId IN (select DISTINCT ParentOrder_ID from bomorder bo where bo.deliveryDate ='${myToday}');" />
<sql:query dataSource="${BOM}" var="entries3" sql="${strQuery}">
</sql:query>

<%-- GO THROUGH EACH COLUMN IN ROW --%>
<c:forEach var="row3" items="${entries3.rows}">
			<c:set var="Accepted" value="${row3.Accepted}" />
			<c:set var="setElem" value="<set label='Accepted' value='${Accepted}' color='${x069AColor}' />" />
			<c:set var="avasXML" value="${avasXML}${setElem} " />
			
			<c:set var="Rejected" value="${row3.Rejected}" />
			<c:set var="setElem" value="<set label='Rejected' value='${Rejected}' color='${x069RColor}' />" />
			<c:set var="avasXML" value="${avasXML}${setElem} " />
			
			<c:set var="Cancelled" value="${row3.Cancelled}" />
			<c:set var="setElem" value="<set label='Cancelled' value='${Cancelled}' color='${x069CColor}' />" />
			<c:set var="avasXML" value="${avasXML}${setElem} " />
			
</c:forEach>
<c:set var="avasXML" value="${avasXML}</chart> " />

 
<c:set var="justFlowersXML"
	value="<chart caption='Just Flowers: Current Status' subCaption='' showBorder='1' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' xAxisName='' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}'>" />

<c:set var="strQuery" value="SELECT SUM(IF(bomorderview_v2.SendingShop_ID = 19620,1,0)) as 'Processed', SUM(IF((bomorderview_v2.SendingShop_ID = 19620 and (bomorderview_v2.Sts_Routing_ID!=9 and bomorderview_v2.Sts_Routing_ID!=10 and bomorderview_v2.Sts_Routing_ID!=11)),1,0)) as 'Accepted', SUM(IF((bomorderview_v2.SendingShop_ID = 19620 and (bomorderview_v2.Sts_Routing_ID=9)),1,0)) as 'Rejected', SUM(IF((bomorderview_v2.SendingShop_ID = 19620 and (bomorderview_v2.Sts_Routing_ID=10 or bomorderview_v2.Sts_Routing_ID=11)),1,0)) as 'Cancelled' FROM    bomorderview_v2 WHERE  bomorderview_v2.ParentBOMId IN (select DISTINCT ParentOrder_ID from bomorder bo where bo.deliveryDate ='${myToday}');" />
<sql:query dataSource="${BOM}" var="entries4" sql="${strQuery}">
</sql:query>

<%-- GO THROUGH EACH COLUMN IN ROW --%>
<c:forEach var="row4" items="${entries4.rows}">
			<c:set var="Accepted" value="${row4.Accepted}" />
			<c:set var="setElem" value="<set label='Accepted' value='${Accepted}' color='${x069AColor}' />" />
			<c:set var="justFlowersXML" value="${justFlowersXML}${setElem} " />
			
			<c:set var="Rejected" value="${row4.Rejected}" />
			<c:set var="setElem" value="<set label='Rejected' value='${Rejected}' color='${x069RColor}' />" />
			<c:set var="justFlowersXML" value="${justFlowersXML}${setElem} " />
			
			<c:set var="Cancelled" value="${row4.Cancelled}" />
			<c:set var="setElem" value="<set label='Cancelled' value='${Cancelled}' color='${x069CColor}' />" />
			<c:set var="justFlowersXML" value="${justFlowersXML}${setElem} " />
			
</c:forEach>
<c:set var="justFlowersXML" value="${justFlowersXML}</chart> " />

<%-- BUILD Blooms Today CHART --%>
<c:set var="bloomsTodayXML"
	value="<chart caption='Blooms Today: Current Status' subCaption='' showBorder='1' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' xAxisName='' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}'>" />

<c:set var="strQuery" value="SELECT SUM(IF(bomorderview_v2.SendingShop_ID = 19662,1,0)) as 'Processed', SUM(IF((bomorderview_v2.SendingShop_ID = 19662 and (bomorderview_v2.Sts_Routing_ID!=9 and bomorderview_v2.Sts_Routing_ID!=10 and bomorderview_v2.Sts_Routing_ID!=11)),1,0)) as 'Accepted', SUM(IF((bomorderview_v2.SendingShop_ID = 19662 and (bomorderview_v2.Sts_Routing_ID=9)),1,0)) as 'Rejected', SUM(IF((bomorderview_v2.SendingShop_ID = 19662 and (bomorderview_v2.Sts_Routing_ID=10 or bomorderview_v2.Sts_Routing_ID=11)),1,0)) as 'Cancelled' FROM    bomorderview_v2 WHERE  bomorderview_v2.ParentBOMId IN (select DISTINCT ParentOrder_ID from bomorder bo where bo.deliveryDate ='${myToday}');" />
<sql:query dataSource="${BOM}" var="entries5" sql="${strQuery}">
</sql:query>

<%-- GO THROUGH EACH COLUMN IN ROW --%>
<c:forEach var="row5" items="${entries5.rows}">
			<c:set var="Accepted" value="${row5.Accepted}" />
			<c:set var="setElem" value="<set label='Accepted' value='${Accepted}' color='${x069AColor}' />" />
			<c:set var="bloomsTodayXML" value="${bloomsTodayXML}${setElem} " />
			
			<c:set var="Rejected" value="${row5.Rejected}" />
			<c:set var="setElem" value="<set label='Rejected' value='${Rejected}' color='${x069RColor}' />" />
			<c:set var="bloomsTodayXML" value="${bloomsTodayXML}${setElem} " />
			
			<c:set var="Cancelled" value="${row5.Cancelled}" />
			<c:set var="setElem" value="<set label='Cancelled' value='${Cancelled}' color='${x069CColor}' />" />
			<c:set var="bloomsTodayXML" value="${bloomsTodayXML}${setElem} " />
			
</c:forEach>
<c:set var="bloomsTodayXML" value="${bloomsTodayXML}</chart> " />

<%-- BUILD BIG SENDERS CHART --%>
<c:set var="largeSendersXML"
	value="<chart caption='Large Senders Order Counts' subCaption='' showBorder='1' xAxisName='' borderThickness='1' borderColor='XXXXXX' bgColor='FFFFFF' yAxisName='Orders' formatNumberScale='0' numberSuffix='' animation='${animateChart}'>" />

<c:set var="strQuery" value="SELECT bomorderview_v2.SendingShopCode as 'Shop',  Count(*) as 'Orders' FROM    bomorderview_v2 WHERE  bomorderview_v2.ParentBOMId IN (select DISTINCT ParentOrder_ID from bomorder bo where bo.deliveryDate ='${myToday}') AND bomorderview_v2.SendingShop_ID in (19662,808,19620) GROUP BY bomorderview_v2.SendingShopCode" />
<sql:query dataSource="${BOM}" var="entries4" sql="${strQuery}">
</sql:query>

<c:set var = "seenN977" value = "false"/>
<c:set var = "seenC573" value = "false"/>
<c:set var = "seenP539" value = "false"/>

<%-- GO THROUGH EACH COLUMN IN ROW --%>
<c:forEach var="row" items="${entries4.rows}">
	<c:choose>
		<c:when test="${row.Shop eq 'N9770000'}">
			<c:set var="Shop" value="${row.Shop}" />
			<c:set var="count" value="${row.Orders}" />
			<c:set var="setElem" value="<set label='Avas Flowers' value='${count}' color='${x069Color}'/>" />
			<c:set var = "seenN977" value = "true"/>
			<c:set var="largeSendersXML" value="${largeSendersXML}${setElem} " />
		</c:when>
		<c:when test="${row.Shop eq 'C5730000'}">
			<c:set var="Shop" value="${row.Shop}" />
			<c:set var="count" value="${row.Orders}" />
			<c:set var="setElem" value="<set label='Blooms Today' value='${count}' color='${c573Color}'/>" />
			<c:set var = "seenC573" value = "true"/>
			<c:set var="largeSendersXML" value="${largeSendersXML}${setElem} " />
		</c:when>
		<c:when test="${row.Shop eq 'P5390000'}">
			<c:set var="Shop" value="${row.Shop}" />
			<c:set var="count" value="${row.Orders}" />
			<c:set var="setElem" value="<set label='Just Flowers' value='${count}' color='${p539Color}'/>" />
			<c:set var = "seenP539" value = "true"/>
			<c:set var="largeSendersXML" value="${largeSendersXML}${setElem} " />
		</c:when>
	</c:choose>
</c:forEach>

<c:if test = "${seenN977 eq 'false'}">
	<c:set var="setElem" value="<set label='Avas Flowers' value='0' color='${x069Color}'/>" />
	<c:set var="largeSendersXML" value="${largeSendersXML}${setElem} " />
</c:if>

<c:if test = "${seenC573 eq 'false'}">
	<c:set var="setElem" value="<set label='Blooms Today' value='0' color='${c573Color}'/>" />
	<c:set var="largeSendersXML" value="${largeSendersXML}${setElem} " />
</c:if>

<c:if test = "${seenP539 eq 'false'}">
	<c:set var="setElem" value="<set label='Just Flowers' value='0' color='${p539Color}'/>" />
	<c:set var="largeSendersXML" value="${largeSendersXML}${setElem} " />
</c:if>


<c:set var="largeSendersXML" value="${largeSendersXML}</chart> " />

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta http-equiv="refresh" content="900" />

        <title><c:out value="${title}"/></title>
        <!-- ../assets/ui/css/ ../assets/ui/js/ ../assets/ui/images/ -->
        <link href="${assetCSSPath}style.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="${assetJSPath}jquery-1.4.2.min.js"></script>
        <script type="text/javascript" src="${assetJSPath}lib.js"></script>
        <!--[if IE 6]>
          <script type="text/javascript" src="${assetJSPath}DD_belatedPNG_0.0.8a-min.js"></script>
          <script>
          /* select the element name, css selector, background etc */
          DD_belatedPNG.fix('img');

          /* string argument can be any CSS selector */
        </script>
        <![endif]-->

        <style type="text/css">
            h2.headline {
                font: normal 110%/137.5% "Trebuchet MS", Arial, Helvetica, sans-serif;
                padding: 0;
                margin: 25px 0 25px 0;
                color: #7d7c8b;
                text-align: center;
            }
            p.small {
                font: normal 68.75%/150% Verdana, Geneva, sans-serif;
                color: #919191;
                padding: 0;
                margin: 0 auto;
                width: 664px;
                text-align: center;
            }
        </style>
        <!-- 
        //You need to include the following JS file, if you intend to embed the chart using JavaScript.
        //Embedding using JavaScripts avoids the "Click to Activate..." issue in Internet Explorer
        //When you make your own charts, make sure that the path to this JS file is correct. Else, you
        //would get JavaScript errors.
        -->
        <script language="Javascript" src="${jsPath}FusionCharts.js"></script>
        <script type="text/javascript">
	
	function myTimeStamp() {
	var d=new Date();
	document.getElementById('myTime').innerHTML = <% if(providedDateExists){ %> "Delivery Date: <%= providedDate %>" <% }else{ %> "Delivery Date: " + (d.getMonth()+1)+"/"+d.getDate()+"/"+(d.getUTCFullYear());<%} %>
	}
	</script>
	

        

    </head>
    <body onload="myTimeStamp();">

        <div id="wrapper">

            <div id="header">
                <div class="back-to-home"><a href="javascript:window.location.reload();">Refresh Data</a></div>

                <div class="logo"><a class="imagelink"  href="#">
                <img src="${assetImagePath}fusionchartsv3.2-logo.png" width="131" height="75" alt="FusionCharts v3.2 logo" /></a></div>
                <h1 class="brand-name">BOM Management Dashboard</h1>
                <h1 class="logo-text"><div id="myTime" /></h1>
            </div>

            <div class="content-area">
                <div id="content-area-inner-main">
                    <div class="gen-chart-render">
						<table>
							<tr>
							
								<tags:BOMtemplate2>
								
								<fc:render chartId="Status"
								swfFilename="${folderPath}MSBar3D.swf"
								width="450" height="300"
								debugMode="false" registerWithJS="false" xmlData="${statusXML}" />
								
								<fc:render chartId="LargeSenders"
								swfFilename="${folderPath}Column3D.swf"
								width="450" height="300"
								debugMode="false" registerWithJS="false" xmlData="${largeSendersXML}" />
								
								</tags:BOMtemplate2>
								<tags:BOMtemplate2>
								
								<fc:render chartId="Summary"
								swfFilename="${folderPath}Pie3D.swf"
								width="450" height="300"
								debugMode="false" registerWithJS="false" xmlData="${summaryXML}" />
								
								<fc:render chartId="avasXML"
								swfFilename="${folderPath}Doughnut3D.swf"
								width="450" height="300"
								debugMode="false" registerWithJS="false" xmlData="${avasXML}" />
								
								
								</tags:BOMtemplate2>
								</tr><tr>
								<tags:BOMtemplate2>
								
								<fc:render chartId="justFlowersXML"
								swfFilename="${folderPath}Doughnut3D.swf"
								width="450" height="300"
								debugMode="false" registerWithJS="false" xmlData="${justFlowersXML}" />
								<td>
								<fc:render chartId="bloomsTodayXML"
								swfFilename="${folderPath}Doughnut3D.swf"
								width="450" height="300"
								debugMode="false" registerWithJS="false" xmlData="${bloomsTodayXML}" />
								</td>
								</tags:BOMtemplate2>
	
	                    	</tr>
	                    </table>

                    </div>
                    <div class="clear"></div>
                    <p>&nbsp;</p>
                    <p class="small"> ${intro} 
                    </p>

                    <div class="underline-dull"></div>
                </div>
            </div>

            <div id="footer">
            </div>
        </div>

    </body>
</html>