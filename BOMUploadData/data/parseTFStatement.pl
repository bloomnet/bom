#!perl

#code to parse tf statements

$myDir = "/TelefloraBOMReport/data";
$myStatement = "43030600.html";

$myMonth = "2019-09";
$myOutfileName = "TF_" . $myMonth . "_statement.txt";

$myInfile = $myDir . "/" . $myStatement;
$myOutfile = $myDir . "/" . $myOutfileName;

$myRegExp = "([\*\ ])\ ([\*\ U])\ ([0-9]{2}\-[0-9]{4})\ (.{22})\ (.{17})\ (.{5})\ (.{14})\ (.{6})\(.{14})\ (.+)";
$myRegExpB = "([\*\ ])\ ([\*\ U])\ ([0-9]{2}\-[0-9]{4})\ (.{22})\ (.{17})\ (.{5})\ (.{10})\ (.{10})\(.{14})\ (.+)";

#$myRegExp = "
#([\*\ ])\ 
#([\*\ U])\ 
#([0-9]{2}\-[0-9]{4})\ Shop#
#(.{22})\ TF Shop
#(.{17})\ City
#(.{5})\ Date
#(.{14})\ Recip
#(.{6})\ Reference
#(.{14})\ Form-seq
#(.+)"; Amount

open(INFILE,$myInfile);
open(OUTFILE,">$myOutfile");

print OUTFILE "a\tb\tShop#\tTeleflora-Shop\tCity\tDate\tRecipient\tReference#\tForm#-Seq#\tAmount\n";

while(<INFILE>) {

	$myLine = $_;
	chomp $myLine;
	if($myLine =~ /\ [0-9]{2}\-[0-9]{4}\ /) {
		#print $myLine . "\n";

		$myLine =~ s/\&amp\;/\&/g;
		$myLine =~ s/\&quot\;/\&/g;
	
		if($myLine =~ /\ [0-9]{10}\ /) {
			$myRegExp = "([\*\ ])\ ([\*\ U])\ ([0-9]{2}\-[0-9]{4})\ (.{22})\ (.{17})\ (.{5})\ (.{10})\ (.{10})\(.{14})\ (.+)";
		}
		else
		{
			$myRegExp = "([\*\ ])\ ([\*\ U])\ ([0-9]{2}\-[0-9]{4})\ (.{22})\ (.{17})\ (.{5})\ (.{10})\ (.{10})\(.{14})\ (.+)";
			#$myRegExp = "([\*\ ])\ ([\*\ U])\ ([0-9]{2}\-[0-9]{4})\ (.{22})\ (.{17})\ (.{5})\ (.{14})\ (.{6})\(.{14})\ (.+)";
		}
		
		
		$myLine =~ /$myRegExp$/;
		#print $1 . "\t" . $2 . "\t" . $3 . "\t" . $4 . "\t" . $5 . "\t" . $6 . "\t" . $7 . "\t" . $8 . "\t" . $9 . "\t" . $10 . "\n";
		$q = $1;
		$w = $2;
		$e = $3;
		$r = $4;
		$t = $5;
		$y = $6;
		$u = $7;
		$i = $8;
		$o = $9;
		$p = $10;
	
		$q =~ s/\ +$//;
		$w =~ s/\ +$//;
		$e =~ s/\ +$//;
		$r =~ s/\ +$//;
		$t =~ s/\ +$//;
		$y =~ s/\ +$//;
		$u =~ s/\ +$//;
		$i =~ s/\ +$//;
		$o =~ s/\ +$//;
		$p =~ s/\ +$//;
		$p =~ s/^\ +//;	
		
		if($p =~ /CR/){
			print "$p yoop\n";
			$p =~ s/CR//;
			$p =~ s/^/\-/;
			print "$p yoop2\n";
		
		}
		if($e ne ''){
			print OUTFILE $q . "\t" . $w . "\t" . $e . "\t" . $r . "\t" . $t . "\t" . $y . "\t" . $u . "\t" . $i . "\t" . $o . "\t" . $p . "\n";
		}
	}
}

close OUTFILE;
close INFILE;