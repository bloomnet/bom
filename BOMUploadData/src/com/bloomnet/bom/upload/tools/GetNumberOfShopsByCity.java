package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GetNumberOfShopsByCity {
	
	Map<String,ArrayList<String>> cityShops = new HashMap<String,ArrayList<String>>();
	Map<String,String> cityZip = new HashMap<String,String>();
	
	SQLData mySQL = new SQLData();

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public GetNumberOfShopsByCity(String fileName, File file) throws SQLException{
		
		String shopCode,city,state;
		
		//mySQL.login();
		//setCityZips();
		
		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else if(file != null)fstream = new FileInputStream(file);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  while((strLine = br.readLine()) != null){
			  
			  ++count;
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);
			  }
			  
			  String myArray[] = new String[16];
			  myArray = strLine.split("\t");
			  shopCode = myArray[0];
			  city = myArray[5];
			  state = myArray[6];
			  //coveredZips = myArray[15];
			  //String[] temp = coveredZips.split(",");
			  city = city+","+state;
			  
			  if(cityShops.get(city) == null){
				  
				ArrayList<String> shopsList = new ArrayList<String>();
				shopsList.add(shopCode);
				cityShops.put(city, shopsList);
				
			  }else{
				  
				ArrayList<String> currentList = cityShops.get(city);
				if(!currentList.contains(shopCode))
					currentList.add(shopCode);
				cityShops.put(city,currentList);
			  }
						
		  }
		  
		  br.close();
		
		  Iterator it = cityShops.entrySet().iterator();
		  
		  Writer writer = new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream("/opt/data/fsi/CityShops.txt"), "utf-8"));
		  
		  while (it.hasNext()) {
			  Map.Entry pair = (Map.Entry)it.next();
			  String cityState = (String) pair.getKey();
			  ArrayList<String> shops = (ArrayList<String>) pair.getValue();
			  String countShops = String.valueOf(shops.size());
			  writer.write(cityState + "\t" + countShops + "\t" + shops + "\n");
			  writer.flush();
			  it.remove(); 
		  }
		  
		  writer.close();
		  
		}catch(Exception ee){
			System.out.println(ee.getMessage());
			ee.printStackTrace();
		}
		
	}
	
	private void setCityZips() throws SQLException{
		
		System.out.println("Setting City Zips");
		
		String query = "SELECT city.name,zip.zip_code," +
					  "state.short_name " +
					  "FROM bloomnet.city_zip_xref " +
					  "INNER JOIN bloomnet.city ON bloomnet.city_zip_xref.id_city = bloomnet.city.id"
					+ " INNER JOIN bloomnet.zip ON bloomnet.city_zip_xref.id_zip = bloomnet.zip.id"
					+ " INNER JOIN bloomnet.state ON bloomnet.city.state_id = bloomnet.state.id"
					+ " WHERE city_type = \"D\";";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			cityZip.put(results.getString("zip_code"), results.getString("city.name").toUpperCase()+","+results.getString("short_name").toUpperCase());
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting City Zips");
	}
	
	public static void main(String[] args){
		
		try {
			new GetNumberOfShopsByCity("/opt/data/fsi/bmt.txt", new File("/opt/data/fsi/bmt.txt"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
