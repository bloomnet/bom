package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class UploadShopFacilityReferences {
	
	protected HashMap<String,String> cityId = new HashMap<String,String>();
	protected HashMap<String, String> cityFacilityIDs = new HashMap<String,String>();
	
	
	protected FileOutputStream fos;
	protected OutputStreamWriter out;
	
	protected String databaseName;
	
	protected SQLData mySQL;
	
	public UploadShopFacilityReferences(String fileName, String databaseName){
		
		this.databaseName = databaseName;
		mySQL = new SQLData();
		mySQL.login();
		
		try {
			fos = new FileOutputStream(fileName);
			out = new OutputStreamWriter(fos);
			
			setCityId();
			setCityFacilityIDs();
			parseData("data/input.csv");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void setCityId() throws SQLException{
		
		System.out.println("Setting City IDs");
		
		String query = "SELECT city.id AS City_ID,city.Name," +
					  "state.Short_Name " +
					  "FROM "+databaseName+".city " +
					  "INNER JOIN "+databaseName+".state ON "+databaseName+".city.state_id = "+databaseName+".state.id;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			cityId.put(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase(), results.getString("City_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished.");
	}	
	
	protected void setCityFacilityIDs() throws SQLException{
		
		System.out.println("Setting City To Facility IDs");
		
		String query = "SELECT address.city_id AS City_ID," +
					   "facility.id AS Facility_ID " +
					   "FROM "+databaseName+".facility " +
					   "INNER JOIN "+databaseName+".address ON "+databaseName+".facility.address_id = address.id " +
					   "WHERE "+databaseName+".facility.facility_type_id = 4;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			if(cityFacilityIDs.get(results.getString("City_ID")) != null) {
				String newString  = cityFacilityIDs.get(results.getString("City_ID")) + "," + results.getString("Facility_ID");
				cityFacilityIDs.put(results.getString("City_ID"), newString);
			}else {				
				cityFacilityIDs.put(results.getString("City_ID"), results.getString("Facility_ID"));
			}
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished.");
	}
	
	public void parseData(String filePath){
		
		String shopCode,
		city,
		state;
		
		FileInputStream fstream;
		
		try {
		  
		  if(filePath != null) fstream = new FileInputStream(filePath);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  while((strLine = br.readLine()) != null){
			  
			  ++count;
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);
			  }
			  
			  String myArray[] = new String[3];
			  myArray = strLine.split(",");
			  shopCode = myArray[0];
			  city = myArray[1];
			  state = myArray[2];
			  
			  if(cityId.get(city.toUpperCase()+","+state.toUpperCase()) != null){
				  if(cityFacilityIDs.get(cityId.get(city.toUpperCase()+","+state.toUpperCase())) != null) {
					  String facilites = cityFacilityIDs.get(cityId.get(city.toUpperCase()+","+state.toUpperCase()));
					  String [] facilitiesArray = facilites.split(",");
					  for(int ii=0; ii<facilitiesArray.length; ++ii) {
						  String line = "INSERT INTO shop_facility_xref(shop_code,facility_id) VALUES(\""+shopCode+"\","+facilitiesArray[ii]+")\n";
						  out.write(line);
					  }
					  out.flush();
				  }
				  
			  }
		  }
		  
		  in.close();
		  out.close();
		  mySQL.closeFile();
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}
	
	public static void main(String[] args){
		new UploadShopFacilityReferences("data/output.txt","bloomnet");
	}
	
}