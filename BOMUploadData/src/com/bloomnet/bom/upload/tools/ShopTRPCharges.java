package com.bloomnet.bom.upload.tools;

public class ShopTRPCharges {
	
	private String shopCode;
	private Double totalTRPOwed = 0.00;
	private Double totalTRPDue = 0.00;
	private Double totalRescindOwed = 0.00;
	private Double totalRescindDue = 0.00;
	
	public String getShopCode() {
		return shopCode;
	}
	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}
	public Double getTotalTRPOwed() {
		return totalTRPOwed;
	}
	public void setTotalTRPOwed(Double totalTRPOwed) {
		this.totalTRPOwed = totalTRPOwed;
	}
	public Double getTotalTRPDue() {
		return totalTRPDue;
	}
	public void setTotalTRPDue(Double totalTRPDue) {
		this.totalTRPDue = totalTRPDue;
	}
	public Double getTotalRescindOwed() {
		return totalRescindOwed;
	}
	public void setTotalRescindOwed(Double totalReciprocityOwed) {
		this.totalRescindOwed = totalReciprocityOwed;
	}
	public Double getTotalRescindDue() {
		return totalRescindDue;
	}
	public void setTotalRescindDue(Double totalReciprocityDue) {
		this.totalRescindDue = totalReciprocityDue;
	}

}
