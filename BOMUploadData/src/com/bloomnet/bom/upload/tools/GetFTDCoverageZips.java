package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class GetFTDCoverageZips extends BaseUpload{
	
	private String fileName;
	private File file;
	
	
	public GetFTDCoverageZips(File file, String databaseName) {
		super("logs/FTDBadData.txt","logs/FTDError.txt","logs/FTDNewShopCodes.txt","logs/FTDReUpload.txt", databaseName);
		this.file = file;
		try {
			parseFTDDataAndWriteFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public GetFTDCoverageZips(String fileName, String databaseName){
		super("logs/FTDBadData.txt","logs/FTDError.txt","logs/FTDNewShopCodes.txt","logs/FTDReUpload.txt",databaseName);
		this.fileName = fileName;
		try {
			parseFTDDataAndWriteFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public GetFTDCoverageZips(String fileName, String dummy, String databaseName){
		super("logs/FTDBadData.txt","logs/FTDError.txt","logs/FTDNewShopCodes.txt","logs/FTDReUpload.txt",databaseName);
		this.fileName = fileName;
		try {
			parseFTDDataAndWriteFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	@SuppressWarnings("unused")
	public void parseFTDDataAndWriteFile() throws IOException{
		
		FileWriter writer = new FileWriter("FTDCoverage.csv");
		
		writer.append("Shop Code,Shop Name,Phone Number,Address,City,State,Zip,Covered City,Covered State,Zips For Covered City/State\n");
		writer.flush();
		
		String shopCode,shopName,shopContact,address,city,state,listingCity,listingState,zip,commMethod,phone,fax,number800,
		cseq,zseq,colGroup,listingCountry,phCd1,phone2,phCd2,closePm1,closePm2,closePm3,closePm4,closePm5,closePm6,closePm7,
		close1,close2,close3,close4,close5,close6,close7,rrDelchInd,rrDelchG,delchGInd,delchG,merc,fto,mrk1,mrk2,mrk3,mrk4,mrk5,mrk6,mrk7,mrk8,mrk9,
		amt1,amt2,amt3,amt4,amt5,amt6,amt7,amt8,amt9,hol1,hol2,hol3,hol4,hol5,hol6,hol7,hol8,hol9,zipFacility,xMinOrd,areaTollFree,p800,listType,rtiInd,
		cityNote,delvTracking,hour24,sundayDeliv,productInd,intlPre,adSize,fontType,fontColor,hasDoveText,nameOfGif,openSunday,coveredZipString;

		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else if(file != null)fstream = new FileInputStream(file);
		  
		  else{
			  System.err.println("No file to parse");
			  writer.close();
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  
		  while((strLine = br.readLine()) != null){
			  
			  String [] arrayLine = strLine.split("\",\"");
			  
			  shopCode = arrayLine[0];
			  shopName =  arrayLine[1].trim().replaceAll("\"","");
			  phone = arrayLine[2].trim();
			  zip = arrayLine[6].trim();
			  city = arrayLine[4].trim().replaceAll("\"","");
			  state = arrayLine[5].trim();
			  address = arrayLine[3].trim().replaceAll("\"","");
			  listingCity = arrayLine[7].trim().replaceAll("\"","");
			  listingState = arrayLine[8].trim().replaceAll("\"","");
			  coveredZipString = "";
			  
			  if(allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase()) != null){
					
				  LinkedList<String> zips = allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase());
				 coveredZipString = zips.toString();
				 
			  }
			writer.write("\""+shopCode+"\",\""+shopName+"\",\""+phone+"\",\""+address+"\",\""+city+"\",\""+state+"\",\""+zip+"\",\""+listingCity+"\",\""+listingState+"\",\""+coveredZipString+"\"\n");
			writer.flush();
			
		  }
		  
		  writer.close();
		  in.close();
		  out.close();
		  errorOut.close();
		  shopCodesOut.close();
		  reUploadOut.close();
		  mySQL.closeFile();
		  
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}
	
}
