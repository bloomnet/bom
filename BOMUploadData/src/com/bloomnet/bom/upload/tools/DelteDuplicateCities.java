package com.bloomnet.bom.upload.tools;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DelteDuplicateCities {
	
	private SQLData mySQL;
	
	public DelteDuplicateCities(){
		mySQL = new SQLData();
		mySQL.login();
		try {
			fixCities();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void fixCities() throws SQLException{

		for(int ii=0; ii<76; ++ii){
			String query = "SELECT * FROM bloomnetordermanagement.city WHERE State_ID = '"+String.valueOf(ii)+"' GROUP BY Name HAVING count(Name) > 1;";
			ResultSet results = mySQL.executeQuery(query);
			
			while(results.next()){
				String cityName = results.getString("Name");
				query = "SELECT * FROM bloomnetordermanagement.city WHERE Name = '"+cityName+"' AND State_ID = '"+String.valueOf(ii)+"';";
				ResultSet results2 = mySQL.executeQuery(query);
				String id1 = "";
				String id2 = "";
				for(int xx=0; xx<2; ++xx){
					results2.next();
					if(xx == 0) id1 = results2.getString("City_ID");
					else id2 = results2.getString("City_ID");
				}
				String statement = "UPDATE bloomnetordermanagement.bomorder SET City_ID = "+id1+" WHERE City_ID = "+id2+"";
				mySQL.executeStatement(statement);
				statement = "UPDATE bloomnetordermanagement.shop SET City_ID = "+id1+" WHERE City_ID = "+id2+"";
				mySQL.executeStatement(statement);
				statement = "UPDATE bloomnetordermanagement.city_zip_xref SET City_ID = "+id1+" WHERE City_ID = "+id2+"";
				mySQL.executeStatement(statement);
				statement = "DELETE FROM bloomnetordermanagement.city WHERE City_ID = "+id2+"";
				mySQL.executeStatement(statement);
			}
		}
	}

}
