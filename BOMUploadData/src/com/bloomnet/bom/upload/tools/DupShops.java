package com.bloomnet.bom.upload.tools;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DupShops {
	private SQLData mySQL;
	
	public DupShops() throws SQLException{
		
		mySQL = new SQLData();
		mySQL.login();
		String useStatement = "USE bloomnetordermanagement";
		mySQL.executeStatement(useStatement);
		String query = "SELECT ShopPhone FROM shop WHERE ShopPhone != \"\" GROUP BY ShopPhone HAVING COUNT(ShopPhone) > 1;";
		ResultSet results = mySQL.executeQuery(query);
		int ii = 0;
		while(results.next()){
			ii++;
			String phone = results.getString(1);
			query = "SELECT Shop_ID FROM shop WHERE ShopPhone = \""+phone+"\";";
			ResultSet results2 = mySQL.executeQuery(query);
			String initialId = "";
			results2.next();
			initialId = results2.getString(1);
			results2.next();
			String otherId = results2.getString(1);
			
			String statement = "UPDATE shopnetwork SET Shop_ID = "+initialId+" WHERE Shop_ID = "+otherId+";";
			mySQL.executeStatement(statement);
			statement = "UPDATE bomorder SET SendingShop_ID = "+initialId+" WHERE SendingShop_ID = "+otherId+";";
			mySQL.executeStatement(statement);
			statement = "UPDATE bomorder SET ReceivingShop_ID = "+initialId+" WHERE ReceivingShop_ID = "+otherId+";";
			mySQL.executeStatement(statement);
			statement = "UPDATE act_message SET ReceivingShop_ID = "+initialId+" WHERE ReceivingShop_ID = "+otherId+";";
			mySQL.executeStatement(statement);
			statement = "UPDATE act_message SET SendingShop_ID = "+initialId+" WHERE SendingShop_ID = "+otherId+";";
			mySQL.executeStatement(statement);
			statement = "UPDATE act_logacall SET ShopCalled_ID = "+initialId+" WHERE ShopCalled_ID = "+otherId+";";
			mySQL.executeStatement(statement);
			statement = "DELETE FROM shop WHERE Shop_ID = "+otherId+";";
			mySQL.executeStatement(statement);

			mySQL.closeStatement();
		}
		mySQL.closeStatement();
		System.out.println(ii);
	}
	
	public static void main(String[] args){
		try {
			new DupShops();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
