package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Set;

public class BaseUpload {
	
	protected HashMap<String,String> cityId = new HashMap<String,String>();
	protected HashMap<String, String> zipId = new HashMap<String,String>();
	protected HashMap<String,String> shopsByPhone = new HashMap<String,String>();
	protected HashMap<String,String> shopsByAddress = new HashMap<String,String>();
	protected HashMap<String,String> shopNetwork = new HashMap<String,String>();
	protected HashMap<String,Integer> uniqueShops = new HashMap<String,Integer>();
	protected HashMap<String,Integer> shopNetworkCoverage = new HashMap<String,Integer>();
	protected HashMap<String,Integer> allFileShopCodes = new HashMap<String,Integer>();
	protected HashMap<String, LinkedList<String>> allCityZips = new HashMap<String,LinkedList<String>>();
	protected HashMap<String, LinkedList<String>> shopCoverages = new HashMap<String,LinkedList<String>>();
	protected HashMap<String, String> existingShopCodes = new HashMap<String, String>();
	protected HashMap<String, String> FLD = new HashMap<String, String>();
	
	protected LinkedList<String> uniqueBadData = new LinkedList<String>();
	protected LinkedList<String> shopsToDeactivate = new LinkedList<String>();
	protected LinkedList<String> shopsToReactivate = new LinkedList<String>();
	
	protected FileOutputStream fos;
	protected OutputStreamWriter out;
	
	protected FileOutputStream errorFos;
	protected OutputStreamWriter errorOut;
	
	protected FileOutputStream shopCodes;
	protected OutputStreamWriter shopCodesOut;
	
	protected FileOutputStream reUpload;
	protected OutputStreamWriter reUploadOut;
	
	protected String databaseName;
	
	protected SQLData mySQL;
	
	public BaseUpload(String fileName, String fileNameError, String fileNameShopCodes, String fileNameReUpload, String databaseName){
		
		this.databaseName = databaseName;
		mySQL = new SQLData();
		mySQL.login();
		
		try {
			fos = new FileOutputStream(fileName);
			out = new OutputStreamWriter(fos);
			
			errorFos = new FileOutputStream(fileNameError);
			errorOut = new OutputStreamWriter(errorFos);
			
			shopCodes = new FileOutputStream(fileNameShopCodes);
			shopCodesOut = new OutputStreamWriter(shopCodes);
			
			reUpload = new FileOutputStream(fileNameReUpload);
			reUploadOut = new OutputStreamWriter(reUpload);
			
			setCityId();
			setShopNetwork();
			setShopsByPhone();
			setShopsByAddress();
			setShopNetworkCoverage();
			setZipId();
			setAllCityZips();
			setFLD();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	protected void setCityId() throws SQLException{
		
		System.out.println("Setting City IDs");
		
		String query = "SELECT city.City_ID,city.Name," +
					  "state.Short_Name " +
					  "FROM "+databaseName+".city " +
					  "INNER JOIN "+databaseName+".state ON "+databaseName+".city.State_ID = "+databaseName+".state.State_ID;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			cityId.put(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase(), results.getString("City_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting City IDs");
	}
	
	protected void setExistingShopCodes(String network){
		
		System.out.println("Setting all existing shop codes for the current network");
		
		String query = "SELECT shopnetwork.ShopCode,shopnetwork.Shop_ID " +
				"FROM "+databaseName+".shopnetwork;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		try {
			while(results.next()){
				existingShopCodes.put(results.getString("ShopCode"),results.getString("Shop_ID"));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting all existing shop codes for the current network");
	}
	
	protected void setShopNetwork() throws SQLException{
		
		System.out.println("Setting Shop Networks");
		
		String query = "SELECT shopnetwork.ShopNetwork_ID," +
					   "shopnetwork.Shop_ID," +
					   "shopnetwork.Network_ID " +
					   "FROM "+databaseName+".shopnetwork;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			shopNetwork.put(results.getString("Shop_ID")+","+results.getString("Network_ID"),results.getString("ShopNetwork_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Shop Networks");
	}
	
	protected void setShopsByPhone() throws SQLException{
		
		System.out.println("Seting Shop to Phone References");
		
		String query = "SELECT shop.Shop_ID," +
					   "shop.ShopName," +
					   "shop.ShopPhone " +
					   "FROM "+databaseName+".shop;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			if(results.getString("ShopPhone") != null){
				shopsByPhone.put(results.getString("ShopPhone"),results.getString("Shop_ID"));
			}
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Seting Shop to Phone References");
	}
	
	protected void setShopsByAddress() throws SQLException{
		
		System.out.println("Setting Shop to Address References");
		
		String query = "SELECT shop.ShopAddress1, " +
					   "shop.Shop_ID, city.Name, " +
					   "state.Short_Name " +
					   "FROM "+databaseName+".shop" +
					   " INNER JOIN "+databaseName+".city ON shop.City_ID = city.City_ID" +
					   " INNER JOIN "+databaseName+".state ON city.State_ID = state.State_ID;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			if(results.getString("ShopAddress1") != null){
				shopsByAddress.put(results.getString("ShopAddress1").toUpperCase()+","+results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase(), results.getString("Shop_ID"));
			}
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Shop to Address References");
	}
	
	protected void setZipId() throws SQLException{
		
		System.out.println("Setting Zip Codes");
		
		String query = "SELECT zip.Zip_Code," +
					   "zip.Zip_ID " +
					   "FROM "+databaseName+".zip;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			zipId.put(results.getString("Zip_Code"), results.getString("Zip_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Zip Codes");
	}
	
	protected void setShopNetworkCoverage() throws SQLException{
		
		System.out.println("Setting Shop Network Coverages");
		
		String query = "SELECT shopnetworkcoverage.ShopNetwork_ID," +
					   "shopnetworkcoverage.Zip_ID " +
					   "FROM "+databaseName+".shopnetworkcoverage;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			shopNetworkCoverage.put(results.getString("ShopNetwork_ID")+","+results.getString("Zip_ID"),1);
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Shop Network Coverages");
	}
	
	protected void setAllCityZips() throws SQLException{
		
		System.out.println("Setting City to Zip References");
		
		String query = "SELECT city.Name," +
					   "city_zip_xref.Zip_ID," +
					   "state.Short_Name," +
					   "country.Short_Name AS 'Country_Short_Name'," +
					   "zip.Zip_Code " +
					   "FROM "+databaseName+".city_zip_xref " +
					   "INNER JOIN "+databaseName+".city ON city_zip_xref.City_ID = city.City_ID " +
					   "INNER JOIN "+databaseName+".state ON city.State_ID = state.State_ID " +
					   "INNER JOIN "+databaseName+".country ON state.Country_ID = country.Country_ID " +
					   "INNER JOIN "+databaseName+".zip ON city_zip_xref.Zip_ID = zip.Zip_ID;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			LinkedList<String> temp = allCityZips.get(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase());
			if(temp == null)
				temp = new LinkedList<String>();
			if((results.getString("Country_Short_Name").equals("CAN") && results.getString("Zip_Code").length() == 3) || (!results.getString("Country_Short_Name").equals("CAN"))){
				temp.add(results.getString("Zip_Code"));
				allCityZips.put(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase(), temp);
			}
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting City to Zip References");
	}
	
	protected void setFLD(){
		
		try {
		  
		  System.out.println("Setting FLD data");
			
		  FileInputStream fstream = new FileInputStream("data/fld.txt");
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  String phone1 = "";
		  String phone2 = "";
		  String phone3 = "";
		  String phone4 = "";
		  String phone5 = "";
		  
		  String shopCode = "";
		  String status = "";
		  
		  int count = 0;
		  
		  while((strLine = br.readLine()) != null){
			  
			  if(count != 0){
				  String myArray[] = new String[45];
				  myArray = strLine.split("\t");
				  if(myArray.length > 30) phone1 = myArray[30];
				  if(myArray.length > 32) phone2 = myArray[32];
				  if(myArray.length > 34) phone3 = myArray[34];
				  if(myArray.length > 36) phone4 = myArray[36];
				  if(myArray.length > 38) phone5 = myArray[38];
				  
				  shopCode = myArray[9];
				  status = myArray[29];
				  
				  if(!status.equals("I")){
					  if(!phone1.equals("")){
						  FLD.put(phone1, shopCode);
					  }
					  if(!phone2.equals("")){
						  FLD.put(phone2, shopCode);
					  }
					  if(!phone3.equals("")){
						  FLD.put(phone3, shopCode);
					  }
					  if(!phone4.equals("")){
						  FLD.put(phone4, shopCode);
					  }
					  if(!phone5.equals("")){
						  FLD.put(phone5, shopCode);
					  }
				  }
			  }
			  count++;
		  }
		  br.close();
		  
		  System.out.println("Finished setting FLD data");
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
	
	protected void setShopsToUpdate (String networkId) throws SQLException{
		
		LinkedList<String> shopCodes = new LinkedList<String>();
		
		String query = "SELECT shopnetwork.ShopCode," +
					   "shopnetwork.ShopNetworkStatus_ID," +
					   "shopnetwork.Network_ID " +
					   "FROM "+databaseName+".shopnetwork;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			shopCodes.add(results.getString("ShopCode")+","+results.getString("ShopNetworkStatus_ID")+","+results.getString("Network_ID"));
		}
		
		mySQL.closeStatement();
		
		String[] shopToStatus;
		
		for(int ii=0; ii<shopCodes.size(); ++ii){
			
			shopToStatus = shopCodes.get(ii).split(",");
			
			if(shopToStatus[2].equals(networkId) && shopToStatus[1].equals("1") && allFileShopCodes.get(shopToStatus[0]) == null){
				shopsToDeactivate.add(shopToStatus[0]);
			
			}else if(shopToStatus[2].equals(networkId) && shopToStatus[1].equals("2") &&  allFileShopCodes.get(shopToStatus[0]) != null){
				shopsToReactivate.add(shopToStatus[0]);
			}
		}
	}
	
	protected void updateCoverage() throws SQLException{
		
		System.out.println("Updating shop coverages");
		
		Set<String> keys = uniqueShops.keySet();
		Iterator<String> itr = keys.iterator();
		String key = "";
		
		while(itr.hasNext()){
			try{
				key = itr.next().toString();
				String query = "SELECT shopnetwork.ShopNetwork_ID " +
									"FROM "+databaseName+".shopnetwork " +
									"WHERE shopnetwork.ShopCode = '"+key+"';";
				ResultSet results = mySQL.executeQuery(query);
				results.next();
				String shopNetworkId = results.getString("ShopNetwork_ID");
				
				mySQL.closeStatement();
				
				query = "SELECT zip.Zip_ID,zip.Zip_Code FROM "+databaseName+".shopnetworkcoverage " +
						"INNER JOIN "+databaseName+".zip ON shopnetworkcoverage.Zip_ID = zip.Zip_ID " +
						"WHERE shopnetworkcoverage.ShopNetwork_ID = '"+shopNetworkId+"';";
				
				results = mySQL.executeQuery(query);
				
				while(results.next()){
					String zip = results.getString("Zip_ID");
					String zipCode = results.getString("Zip_Code");
					if(shopCoverages.get(key) != null && !(shopCoverages.get(key).contains(zipCode))){
						String statement = "DELETE FROM "+databaseName+".shopnetworkcoverage WHERE ShopNetwork_ID = '"+shopNetworkId+"' " +
											"AND Zip_ID = '"+zip+"';";
						System.out.println(statement);
						mySQL.executeStatement(statement);
					}
				}
				
				mySQL.closeStatement();
				
			}catch(Exception ee){
				
				//System.out.println("The shop: "+key+" was not previously inserted into our database due to a " +
						//"lack of the the shop's city/state/zip existing in our database");
			}
		}
		
		System.out.println("Finished updating shop coverages");
	}
	
	protected String getId(){
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		String id = "";
		try {
			results.next();
			id = results.getString("LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mySQL.closeStatement();
		return id;
	}
}