package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FixCityStateIds {
	
private SQLData mySQL;
	
	public FixCityStateIds(){
		mySQL = new SQLData();
		mySQL.login();
		try {
			findCorrectStates();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void findCorrectStates() throws IOException, SQLException{
		
		FileInputStream fstream = new FileInputStream("data/fixedCities.txt");
		  
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		String cityId = "";
		String stateName = "";
		String stateId = "";
		
		String statement = "USE bloomnetordermanagement;";
		mySQL.executeStatement(statement);
		
		while((strLine = br.readLine()) != null){
			
			String[] line = strLine.split(",");
			cityId = line[0];
			stateName = line[2];
			
			String query = "SELECT State_ID FROM state WHERE Name = \""+stateName+"\";";
			ResultSet results = mySQL.executeQuery(query);
			
			while(results.next()){
				stateId = results.getString(1);
			}
			
			statement = "UPDATE city SET State_ID = "+stateId+" WHERE City_ID = "+cityId+";";
			mySQL.executeStatement(statement);
			
		}
		br.close();
	}
	
}
