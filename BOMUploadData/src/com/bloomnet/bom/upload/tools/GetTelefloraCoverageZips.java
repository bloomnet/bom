package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedList;

public class GetTelefloraCoverageZips extends BaseUpload{
	
	private String fileName;
	private File file;
	
	
	public GetTelefloraCoverageZips(File file, String databaseName) {
		super("logs/TFBadData.txt","logs/TFError.txt","logs/TFNewShopCodes.txt","logs/TFReUpload.txt", databaseName);
		this.file = file;
		try {
			parseTelefloraDataAndWriteFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public GetTelefloraCoverageZips(String fileName, String databaseName){
		super("logs/TFBadData.txt","logs/TFError.txt","logs/TFNewShopCodes.txt","logs/TFReUpload.txt",databaseName);
		this.fileName = fileName;
		try {
			parseTelefloraDataAndWriteFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public GetTelefloraCoverageZips(String fileName, String dummy, String databaseName){
		super("logs/TFBadData.txt","logs/TFError.txt","logs/TFNewShopCodes.txt","logs/TFReUpload.txt",databaseName);
		this.fileName = fileName;
		try {
			parseTelefloraDataAndWriteFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
	@SuppressWarnings("unused")
	public void parseTelefloraDataAndWriteFile() throws IOException{
		
		FileWriter writer = new FileWriter("TelefloraCoverage.csv");
		
		writer.append("Shop Code,Shop Name,Phone Number,Address,City,State,Zip,Covered City,Covered State,Zips For Covered City/State\n");
		writer.flush();
		
		String shopCode,shopName,shopContact,address,city,state,listingCity,listingState,zip,commMethod,phone,fax,number800,
		cseq,zseq,colGroup,listingCountry,phCd1,phone2,phCd2,closePm1,closePm2,closePm3,closePm4,closePm5,closePm6,closePm7,
		close1,close2,close3,close4,close5,close6,close7,rrDelchInd,rrDelchG,delchGInd,delchG,merc,fto,mrk1,mrk2,mrk3,mrk4,mrk5,mrk6,mrk7,mrk8,mrk9,
		amt1,amt2,amt3,amt4,amt5,amt6,amt7,amt8,amt9,hol1,hol2,hol3,hol4,hol5,hol6,hol7,hol8,hol9,zipFacility,xMinOrd,areaTollFree,p800,listType,rtiInd,
		cityNote,delvTracking,hour24,sundayDeliv,productInd,intlPre,adSize,fontType,fontColor,hasDoveText,nameOfGif,openSunday,coveredZipString;

		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else if(file != null)fstream = new FileInputStream(file);
		  
		  else{
			  System.err.println("No file to parse");
			  writer.close();
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  
		  while((strLine = br.readLine()) != null){
			  
			  
			  cseq = strLine.substring(0,4).trim();
			  zseq = strLine.substring(4,8).trim();
			  shopCode = strLine.substring(8,16).trim();
			  colGroup = strLine.substring(16,17).trim();
			  listingState = strLine.substring(17,19).trim();
			  listingCity = strLine.substring(19,59).trim();
			  listingCountry = strLine.substring(59,89).trim();
			  shopName = strLine.substring(89,149).trim().replaceAll("\"","'");
			  phone = strLine.substring(149,159).trim();
			  phCd1 = strLine.substring(159,160).trim();
			  phone2 = strLine.substring(160,170).trim();
			  phCd2 = strLine.substring(170,171).trim();
			  shopContact = strLine.substring(171,221).trim().replaceAll("\"","'");
			  closePm1 = strLine.substring(221,222).trim();
			  closePm2 = strLine.substring(222,223).trim();
			  closePm3 = strLine.substring(223,224).trim();
			  closePm4 = strLine.substring(224,225).trim();
			  closePm5 = strLine.substring(225,226).trim();
			  closePm6 = strLine.substring(226,227).trim();
			  closePm7 = strLine.substring(227,228).trim();
			  close1 = strLine.substring(228,229).trim();
			  close2 = strLine.substring(229,230).trim();
			  close3 = strLine.substring(230,231).trim();
			  close4 = strLine.substring(231,232).trim();
			  close5 = strLine.substring(232,233).trim();
			  close6 = strLine.substring(233,234).trim();
			  close7 = strLine.substring(234,235).trim();
			  rrDelchInd = strLine.substring(235,236).trim();
			  rrDelchG = strLine.substring(236,242).trim();
			  delchGInd = strLine.substring(242,243).trim();
			  delchG = strLine.substring(243,249).trim();
			  merc = strLine.substring(249,250).trim();
			  fto = strLine.substring(250,251).trim();
			  mrk1 = strLine.substring(251,252).trim();
			  mrk2 = strLine.substring(252,253).trim();
			  mrk3 = strLine.substring(253,254).trim();
			  mrk4 = strLine.substring(254,255).trim();
			  mrk5 = strLine.substring(255,256).trim();
			  mrk6 = strLine.substring(256,257).trim();
			  mrk7 = strLine.substring(257,258).trim();
			  mrk8 = strLine.substring(258,259).trim();
			  mrk9 = strLine.substring(259,260).trim();
			  amt1 = strLine.substring(260,266).trim();
			  amt2 = strLine.substring(266,272).trim();
			  amt3 = strLine.substring(272,278).trim();
			  amt4 = strLine.substring(278,284).trim();
			  amt5 = strLine.substring(284,290).trim();
			  amt6 = strLine.substring(290,296).trim();
			  amt7 = strLine.substring(296,302).trim();
			  amt8 = strLine.substring(302,308).trim();
			  amt9 = strLine.substring(308,314).trim();
			  hol1 = strLine.substring(314,315).trim();
			  hol2 = strLine.substring(315,316).trim();
			  hol3 = strLine.substring(316,317).trim();
			  hol4 = strLine.substring(317,318).trim();
			  hol5 = strLine.substring(318,319).trim();
			  hol6 = strLine.substring(319,320).trim();
			  hol7 = strLine.substring(320,321).trim();
			  hol8 = strLine.substring(321,322).trim();
			  hol9 = strLine.substring(322,323).trim();
			  zip = strLine.substring(323,329).trim();
			  zipFacility = strLine.substring(329,335).trim();
			  city = strLine.substring(335,365).trim().replaceAll("\"","'");
			  state = strLine.substring(365,367).trim();
			  xMinOrd = strLine.substring(367,373).trim();
			  areaTollFree = strLine.substring(373,376).trim();
			  p800 = strLine.substring(376,383).trim();
			  fax = strLine.substring(383,393).trim();
			  listType = strLine.substring(393,394).trim();
			  number800 = strLine.substring(373,383).trim();
			  address = strLine.substring(394,424).trim().replaceAll("\"","'");
			  commMethod = strLine.substring(424,425).trim();
			  rtiInd = strLine.substring(425,426).trim();
			  cityNote =  strLine.substring(426,456).trim();
			  delvTracking =  strLine.substring(456,457).trim();
			  hour24 =  strLine.substring(457,458).trim();
			  sundayDeliv =  strLine.substring(458,459).trim();
			  productInd =  strLine.substring(459,460).trim();
			  intlPre =  strLine.substring(460,466).trim();
			  adSize =  strLine.substring(466,470).trim();
			  fontType =  strLine.substring(470,471).trim();
			  fontColor =  strLine.substring(471,472).trim();
			  hasDoveText =  strLine.substring(472,473).trim();
			  nameOfGif =  strLine.substring(473,489).trim();
			  coveredZipString = "";
			  
			  if(sundayDeliv.equals("X")) openSunday = "1";
			  else openSunday = "0";
			  
			  if(allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase()) != null){
					
				  LinkedList<String> zips = allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase());
				 coveredZipString = zips.toString();
				 
			  }
			writer.write("\""+shopCode+"\",\""+shopName+"\",\""+phone+"\",\""+address+"\",\""+city+"\",\""+state+"\",\""+zip+"\",\""+listingCity+"\",\""+listingState+"\",\""+coveredZipString+"\"\n");
			writer.flush();
			
		  }
		  
		  writer.close();
		  in.close();
		  out.close();
		  errorOut.close();
		  shopCodesOut.close();
		  reUploadOut.close();
		  mySQL.closeFile();
		  
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}
	
}
