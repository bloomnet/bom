package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class FixShops {
	
	SQLData mySQL;
	Map<String,String> FLD = new HashMap<String,String>();
	FileInputStream fstream;
	
	FileOutputStream fos;
	OutputStreamWriter out;
	
	public static void main(String[] args){
		FixShops fs = new FixShops();
		try {
			fs.setFLD();
			fs.fixShops();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void setFLD(){
		
		try {
		  
		  fstream = new FileInputStream("data/fld.txt");
		  
		  fos = new FileOutputStream("data/changedShops.txt");
		  out = new OutputStreamWriter(fos);
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  String phone1 = "";
		  String phone2 = "";
		  String phone3 = "";
		  String phone4 = "";
		  String phone5 = "";
		  
		  String shopCode = "";
		  String status = "";
		  
		  int count = 0;
		  
		  while((strLine = br.readLine()) != null){
			  
			  if(count != 0){
				  String myArray[] = new String[45];
				  myArray = strLine.split("\t");
				  if(myArray.length > 30) phone1 = myArray[30];
				  if(myArray.length > 32) phone2 = myArray[32];
				  if(myArray.length > 34) phone3 = myArray[34];
				  if(myArray.length > 36) phone4 = myArray[36];
				  if(myArray.length > 38) phone5 = myArray[38];
				  
				  shopCode = myArray[9];
				  status = myArray[29];
				  
				  if(!status.equals("I")){
					  if(!phone1.equals("")){
						  FLD.put(phone1, shopCode);
					  }
					  if(!phone2.equals("")){
						  FLD.put(phone2, shopCode);
					  }
					  if(!phone3.equals("")){
						  FLD.put(phone3, shopCode);
					  }
					  if(!phone4.equals("")){
						  FLD.put(phone4, shopCode);
					  }
					  if(!phone5.equals("")){
						  FLD.put(phone5, shopCode);
					  }
				  }
			  }
			  count++;
		  }
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
	
	public void fixShops() throws SQLException{
		
		mySQL = new SQLData();
		mySQL.login();
		
		String statement = "USE bloomnetordermanagement";
		
		mySQL.executeStatement(statement);
		
		String query = "SELECT shop.Shop_ID,shop.ShopPhone FROM shop INNER JOIN shopnetwork ON shopnetwork.Shop_ID = shop.Shop_ID WHERE shopnetwork.Network_ID != 1;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		int erroneousShops = 0;
		
		while(results.next()){
			String phone = results.getString("ShopPhone");
			String shopId = results.getString("Shop_ID");
			
			boolean isAlreadyBMT = false;
			
			query = "SELECT Network_ID FROM shopnetwork WHERE Shop_ID = "+shopId+";";
			ResultSet results2 = mySQL.executeQuery(query);
			
			while(results2.next()){
				String network = results2.getString("Network_ID");
				if(network.equals("1")) isAlreadyBMT = true;
			}
			
			results2.close();
			mySQL.closeStatement();
			
			if(!isAlreadyBMT){
				if(!phone.equals("")){
					if(FLD.get(phone) != null){
						
						String bmtShopCode = FLD.get(phone);
						
						query = "SELECT shop.Shop_ID FROM shop INNER JOIN shopnetwork ON shopnetwork.Shop_ID = shop.Shop_ID WHERE shopnetwork.ShopCode = '"+bmtShopCode+"';";
						
						ResultSet results3 = mySQL.executeQuery(query);
						
						String newShopId = "";
						
						while(results3.next()){
							newShopId = results3.getString("Shop_ID");
						}
						results3.close();
						mySQL.closeStatement();
						
						if(!newShopId.equals("")){
							statement = "UPDATE shopnetwork SET Shop_ID = '"+newShopId+"' WHERE Shop_ID = "+shopId+";";
							mySQL.executeStatement(statement);
							statement = "UPDATE act_message SET SendingShop_ID = '"+newShopId+"' WHERE SendingShop_ID = "+shopId+";";
							mySQL.executeStatement(statement);
							statement = "UPDATE act_message SET ReceivingShop_ID = '"+newShopId+"' WHERE ReceivingShop_ID = "+shopId+";";
							mySQL.executeStatement(statement);
							statement = "UPDATE act_logacall SET ShopCalled_ID = '"+newShopId+"' WHERE ShopCalled_ID = "+shopId+";";
							mySQL.executeStatement(statement);
							statement = "UPDATE bomorder SET SendingShop_ID = '"+newShopId+"' WHERE SendingShop_ID = "+shopId+";";
							mySQL.executeStatement(statement);
							statement = "UPDATE bomorder SET ReceivingShop_ID = '"+newShopId+"' WHERE ReceivingShop_ID = "+shopId+";";
							mySQL.executeStatement(statement);
							statement = "DELETE FROM shop WHERE Shop_ID = "+shopId+";";
							mySQL.executeStatement(statement);
							
							try {
								out.write("Shop: "+shopId+" is a BMT shop with shop code: "+ bmtShopCode+"\n");
							} catch (IOException e) {
								e.printStackTrace();
							}
						}else{
							System.out.println("Shop: "+shopId+" is a BMT shop with shop code: "+ bmtShopCode);
							erroneousShops++;
						}
					}
				}	
			}
		}
		
		results.close();
		mySQL.closeStatement();
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println(erroneousShops);
	}
}
