package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class GetNumberOfShopsByCityTF {
	
	Map<String,ArrayList<String>> cityShops = new HashMap<String,ArrayList<String>>();
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public GetNumberOfShopsByCityTF(String fileName, File file) throws SQLException{
		
		String shopCode,listingCity,listingState,city,state;
		
		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else if(file != null)fstream = new FileInputStream(file);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  while((strLine = br.readLine()) != null){
			  
			  ++count;
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);
			  }
			  
			  shopCode = strLine.substring(8,16).trim();
			  city = strLine.substring(335,365).trim().replaceAll("\"","'");
			  state = strLine.substring(365,367).trim();
			  listingCity = strLine.substring(19,59).trim();
			  listingState = strLine.substring(17,19).trim();
			  listingCity = listingCity + "," + listingState;
			  city = city + "," + state;
			  
			  if(cityShops.get(city) == null){
					ArrayList<String> shopsList = new ArrayList<String>();
					shopsList.add(shopCode);
					cityShops.put(city, shopsList);
			  }else{
					ArrayList<String> currentList = cityShops.get(city);
					if(!currentList.contains(shopCode))
						currentList.add(shopCode);
					cityShops.put(city,currentList);
			  }
		  }
		  
		  br.close();
		
		  Iterator it = cityShops.entrySet().iterator();
		  
		  Writer writer = new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream("/opt/data/fsi/CityShopsTF.txt"), "utf-8"));
		  
		  while (it.hasNext()) {
			  Map.Entry pair = (Map.Entry)it.next();
			  String cityState = (String) pair.getKey();
			  ArrayList<String> shops = (ArrayList<String>) pair.getValue();
			  String countShops = String.valueOf(shops.size());
			  writer.write(cityState + "\t" + countShops + "\t" + shops + "\n");
			  writer.flush();
			  it.remove(); 
		  }
		  
		  writer.close();
		  
		}catch(Exception ee){
			System.out.println(ee.getMessage());
			ee.printStackTrace();
		}
		
	}
	
	
	public static void main(String[] args){
		
		try {
			new GetNumberOfShopsByCityTF("/opt/data/fsi/combodod.dat", new File("/opt/data/fsi/combodod.dat"));
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
	}
}
