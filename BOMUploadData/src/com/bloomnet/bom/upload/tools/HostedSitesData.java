package com.bloomnet.bom.upload.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class HostedSitesData {
	
	File file = new File("data/hostedSitesData.txt");
	SQLData mySQL = new SQLData();
	BufferedWriter writer;
	
	public static void main(String[] args){
		new HostedSitesData();
	}
	
	public HostedSitesData(){
		System.out.println("started");
		try {
			writeData();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		System.out.println("finished");
	}
	
	public void writeData() throws IOException, SQLException{
		mySQL.login();
		writer = new BufferedWriter(new FileWriter(file));
		
		String statement = "USE bloomnetordermanagement;";
		mySQL.executeStatement(statement);
		
		String query = "SELECT ShopCode,ShopName,ShopNetwork_ID,ShopAddress1,city.Name,state.Short_Name,zip.Zip_Code "
				+ "FROM shop "
				+ "INNER JOIN shopnetwork ON shop.Shop_ID = shopnetwork.Shop_ID "
				+ "INNER JOIN city ON shop.City_ID = city.City_ID "
				+ "INNER JOIN state ON city.State_ID = state.State_ID "
				+ "INNER JOIN zip ON shop.Zip_ID = zip.Zip_ID "
				+ "WHERE Network_ID = 1 AND ShopNetworkStatus_ID = 1;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		int ii = 0;
		while(results.next()){
			
			++ii;
			if(ii%100 == 0){
				System.out.println(ii);
			}
			
			String shopCode = results.getString("ShopCode");
			String shopName = results.getString("ShopName");
			String shopNetworkId = results.getString("ShopNetwork_ID");
			String shopAddress = results.getString("ShopAddress1");
			String city = results.getString("Name");
			String state = results.getString("Short_Name");
			String zip = results.getString("Zip_Code");
			
			writer.write(shopCode + "\t" + shopName+" located at "+ shopAddress+", " + city + ", " + state + ", " + zip + " "
					+ "also provides local delivery to the following cities: ");
			
			String query2 = "SELECT DISTINCT city.Name, state.Short_Name FROM shopnetworkcoverage "
					+ "INNER JOIN zip ON shopnetworkcoverage.Zip_ID = zip.Zip_ID "
					+ "INNER JOIN city_zip_xref xref ON zip.Zip_ID = xref.Zip_ID "
					+ "INNER JOIN city ON xref.City_ID = city.City_ID "
					+ "INNER JOIN state ON city.State_ID = state.State_ID "
					+ "WHERE ShopNetwork_ID = "+shopNetworkId+" AND city.City_Type IN ('D');";
			
			ResultSet results2 = mySQL.executeQuery(query2);
			if(results2.next()){
				String cityServed = results2.getString("Name");
				if(!cityServed.equals(city) && !cityServed.equals(""))
					writer.write(cityServed);
				else if(results2.next()){
					cityServed = results2.getString("Name");
					writer.write(cityServed);
				}
			}
			while(results2.next()){
				String cityServed = results2.getString("Name");
				if(!cityServed.equals(city) && !cityServed.equals(""))
					writer.write(", "+cityServed);
			}
			results2.close();
			mySQL.closeStatement();
			
			writer.write(" and zip codes: ");
			
			String query3 = "SELECT zip.Zip_Code FROM shopnetworkcoverage "
					+ "INNER JOIN zip ON shopnetworkcoverage.Zip_ID = zip.Zip_ID "
					+ "WHERE ShopNetwork_ID = "+shopNetworkId+";";
			
			ResultSet results3 = mySQL.executeQuery(query3);
			if(results3.next()){
				String zipServed = results3.getString("Zip_Code");
				if(!zipServed.equals(zip) && !zipServed.equals(""))
					writer.write(zipServed);
				else if(results3.next()){
					zipServed = results3.getString("Zip_Code");
					writer.write(zipServed);
				}
					
			}
			while(results3.next()){
				String zipServed = results3.getString("Zip_Code");
				if(!zipServed.equals(zip) && !zipServed.equals(""))
					writer.write(", "+zipServed);
				
			}
			results3.close();
			mySQL.closeStatement();
			
			writer.write("\n");
		}
		results.close();
		mySQL.closeStatement();
		writer.flush();
		writer.close();
	}
	
}
