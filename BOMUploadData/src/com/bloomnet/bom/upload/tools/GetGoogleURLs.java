package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GetGoogleURLs {
	
	SQLData mySQL;
	
	Map<String,String> existingURLs = new HashMap<String,String>(); 
	Map<String,String> locationKeys = new HashMap<String,String>();
	Map<String,String> shopCity = new HashMap<String,String>();
	Map<String,String> shopState = new HashMap<String,String>();
	List<String> shopCodes = new ArrayList<String>();
	
	public GetGoogleURLs(){
		
		 try {
			 
			 Writer writer = new BufferedWriter(new OutputStreamWriter(
		              new FileOutputStream("data/GoogleURLs.csv"), "utf-8"));
			 
			 writer.write("Shop Code,Place ID,BMT Website,Google Website,BMT City,BMT State,Google City,Google State,Closed?,URLs Match?,Cities Match?\n");
			 
			 setExistingData();
		
			 String urlA = "https://maps.googleapis.com/maps/api/place/details/json?placeid=";
			 String urlB = "&fields=address_component,alt_id,formatted_address,geometry,id,name,permanently_closed,place_id,scope,type,url,utc_offset,vicinity,name,rating,formatted_phone_number,website,opening_hours&key=AIzaSyBdJrLFts-UP4Nw1fqqsdlDYOmzoqjtTpc";
			
			 for(int ii = 0; ii < shopCodes.size(); ++ii){
				 
				 String shopCode = shopCodes.get(ii);
				 String locationKey = locationKeys.get(shopCode);
				 String existingURL = existingURLs.get(shopCode);
				 String shopsCity = shopCity.get(shopCode).replaceAll(","," ").replaceAll("\n", "").replaceAll("\r", "");
				 String shopsState = shopState.get(shopCode).replaceAll(","," ").replaceAll("\n", "").replaceAll("\r", "");
				 String matchingURLs = "";
				 String matchingCities = "";
				 String googleURL = "";
				 
				 URL url = new URL(urlA + locationKey + urlB);
				 
				 System.out.println(urlA + locationKey + urlB);
		         try{
		        	 long start = System.currentTimeMillis();
		        	 URLConnection con = url.openConnection();
		        	 BufferedReader br = new BufferedReader(
                             new InputStreamReader(
                             con.getInputStream()));
				       String inputLine;
				       String response = "";
				       long end = System.currentTimeMillis();
					   System.out.println("Execution Time: " + String.valueOf(end-start) + "ms.");
				       while ((inputLine = br.readLine()) != null) 
				           response += inputLine + " ";
				       br.close();
				  	   existingURL = existingURL.replaceAll("http://","").replaceAll("https://","").replaceAll("/", "").replaceAll("www.", "").replaceAll("\r", "").replaceAll("\n", "");
				       String googleCity = "";
				       String googleState = "";
				       String closed = "";
				       
				       if(response.contains("formatted_address")){
			      			googleCity = response.split("formatted_address")[1].split(",")[1].trim();
			      			googleState = response.split("formatted_address")[1].split(",")[2].trim().split(" ")[0];
			      			int x = 1;
			      			while(googleState.length() != 2){
			      				googleCity = response.split("formatted_address")[1].split(",")[1+x].trim();
			      				googleState = response.split("formatted_address")[1].split(",")[2+x].trim().split(" ")[0];
			      				x++;
			      			}
			  
			      		}
				       
				       if(response.contains("permanently_closed")){
				    	   if(response.split("\"permanently_closed\" : ")[1].split(",")[0].equalsIgnoreCase("true"))
				    		   closed = "Yes";
				    	   else
				    		   closed = "No";
				       }else
				    	   closed = "No";
				      	 
				      	 if(shopsCity.equalsIgnoreCase(googleCity))
				      		 matchingCities = "Yes";
				      	 else
				      		 matchingCities = "No";
				       
				       if(response.contains("\"website\" : \"")){
				      	 googleURL = response.split("\"website\" : \"")[1].split("\"")[0];
				      	 googleURL = googleURL.replaceAll("http://","").replaceAll("https://","").replaceAll("www.", "").replaceAll("\r", "").replaceAll("\n", "").replaceAll("\t", "");
				      	 while(googleURL.contains("/")){
				      		 googleURL = googleURL.split("/")[0];
				      	 }
				      	 
				      	 if(googleURL.equalsIgnoreCase(existingURL))
				      		 matchingURLs = "Yes";
				      	 else
				      		 matchingURLs = "No";
				      		 			      		 
			      		 String statement = "UPDATE shop_google SET google_url = \""+googleURL+"\" WHERE location_key = \""+locationKey+"\"";
				         mySQL.executeStatement(statement);
				      	 
				       }else{
				    	   googleURL = "No URL Found";
				    	   matchingURLs = "No";
				       }
				       
				       writer.write(shopCode+","+locationKey+","+existingURL+","+googleURL+","+shopsCity+","+shopsState+","+googleCity+","+googleState+","+closed+","+matchingURLs+","+matchingCities+"\n");

				       
		         }catch(Exception ee){}

			 }
			 
			 writer.flush() ;
			 writer.close();
			 
			 //if(changes > 0)
				 //new MailAutomation("Google Places Detected Changes","Attached is a list of shops that have changes detected according to Google Places along with their original and new values. This is an automated email.","data/GoogleURLs.csv","GoogleURLs.csv","msilver@bloomnet.net,marcg@bloomnet.net");
			 //else
				 
				 //new MailAutomation("Google Places Detected Changes","There were no changes to report on the most recent run since the last run. This is an automated email.","data/GoogleURLs.csv","GoogleURLs.csv","msilver@bloomnet.net,marcg@bloomnet.net");
		 }catch (SQLException e) {
				e.printStackTrace();
		 } catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}// catch (MessagingException e) {
			//e.printStackTrace();
		//}
	}
		 
	
	private void setExistingData() throws SQLException{
		
		mySQL = new SQLData();
		mySQL.login();
		
		String statement = "USE shopdata;";
		mySQL.executeStatement(statement);

		String query = "SELECT * FROM shop_google;";
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			
			String shopCode = results.getString(1);
			String locationKey = results.getString(2);
			String url = results.getString(3);
			String city = results.getString(4);
			String state = results.getString(5);
			
			existingURLs.put(shopCode,url);
			locationKeys.put(shopCode, locationKey);
			shopCity.put(shopCode, city);
			shopState.put(shopCode, state);
			shopCodes.add(shopCode);
					
		}
	}
	
	public static void main(String[] args){
		
		new GetGoogleURLs();
		
	}
	
}
