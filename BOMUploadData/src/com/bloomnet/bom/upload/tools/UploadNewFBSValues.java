package com.bloomnet.bom.upload.tools;

import java.io.File;
import jxl.Sheet;
import jxl.Workbook;

public class UploadNewFBSValues {
	
	private SQLData mySQL;
	private String databaseName;
	private String fileName;
	
	
	public UploadNewFBSValues(String fileName, String databaseName){

		this.databaseName = databaseName;
		this.fileName = fileName;
		mySQL = new SQLData();
		mySQL.login();
		
		uploadNewFBSValues();
		
	}
	
	private void uploadNewFBSValues(){
		
		File inputWorkbook = new File(fileName);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			int ii = 0;
			for (ii = 0; ii < sheet.getRows(); ++ii){
				
				if(ii%100 == 0)
					System.out.println("Completed "+ii+" Lines");
				
				String dbId = sheet.getCell(0, ii).getContents().toString();
				String newPrice = sheet.getCell(6, ii).getContents().toString();
				
				String statement = "UPDATE "+databaseName+".fbs_florist_prod SET PROD_AMOUNT = " + newPrice + ",USR_MDFD = '2784',DT_MDFD = now() WHERE FLORIST_VEND_REF_ID = "+dbId;
				mySQL.executeStatement(statement);
			}
			System.out.println(ii+" lines itterated out of "+sheet.getRows()+ " rows in the file.");
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}

}
