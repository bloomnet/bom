package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URL;
import java.net.URLConnection;

public class GetShopAPIKeys {
	
	public GetShopAPIKeys(){
		
		String shopCode,city,state,address,name;
		
		String fileName = "data/input.csv";
		
		String urlA = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json?input=";
		String urlB = "&inputtype=textquery&key=AIzaSyBdJrLFts-UP4Nw1fqqsdlDYOmzoqjtTpc&fields=formatted_address,id,name,permanently_closed,place_id,type,name,rating";
		
		FileInputStream fstream;
		
		try {
		  
		  fstream = new FileInputStream(fileName);
		  
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  Writer writer = new BufferedWriter(new OutputStreamWriter(
	              new FileOutputStream("data/APICodes.csv"), "utf-8"));
		  
		  int count=0;
		  strLine = br.readLine();
		  
		  while((strLine = br.readLine()) != null){
			  
			  ++count;
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Processing data line "+lineNumber);
			  }
			  
			  String myArray[] = new String[17];
			  myArray = strLine.split(",");
			  shopCode = myArray[0];
			  name = myArray[2];
			  address = myArray[5];
			  city = myArray[6];
			  state = myArray[7];
			  
			  address = address.replaceAll(" ", "%20");
			  city = city.replaceAll(" ", "%20");
			  name = name.replaceAll(" ","%20");
	
	          URL url = new URL(urlA + name + "%20" + city + "%20" + state + "%20" + address + urlB);
	          URLConnection con = url.openConnection();
	          BufferedReader br2 = new BufferedReader(
	                                new InputStreamReader(
	                                con.getInputStream()));
	          String inputLine;
	          String response = "";

	          while ((inputLine = br2.readLine()) != null) 
	              response += inputLine + " ";
	          br2.close();
	          
	          if(response.contains("\"place_id\" : \""))
	        	 writer.write(shopCode + "," + response.split("\"place_id\" : \"")[1].split("\"")[0] + "," + response.split("\"name\" : \"")[1].split("\"")[0]+"\n");
	          else
	        	  writer.write(shopCode + "," + "Nothing Found,\n");
 
						
		  }
		  writer.flush();
		  writer.close();
		  br.close();
		
		}catch(Exception ee){System.out.println("error: " + ee.getMessage()); ee.printStackTrace();}
	}
	
	public static void main(String[] args){
		
		new GetShopAPIKeys();
		
	}

}
