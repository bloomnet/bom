package com.bloomnet.bom.upload.tools;

import java.io.File;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import jxl.Sheet;
import jxl.Workbook;

public class UploadTRPCompiledData {
	
	private SQLData mySQL;
	private String databaseName;
	private String fileName;
	private String fileName2;
	private String date = "11/2013";
	
	private Map<String,ShopTRPCharges> shopTRP = new HashMap<String,ShopTRPCharges>();
	
	
	public UploadTRPCompiledData(String fileName, String fileName2, String databaseName){
		this.databaseName = databaseName;
		this.fileName = fileName;
		this.fileName2 = fileName2;
		mySQL = new SQLData();
		mySQL.login();
		
		parseCompiledTRPData();
		parseCompiledRescindData();
		uploadData();
		
	}
	
	private void parseCompiledTRPData(){
		
		File inputWorkbook = new File(fileName);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(1);
			int ii = 0;
			for (ii = 0; ii < sheet.getRows(); ++ii){
				
				if(ii%100 == 0)
					System.out.println("Completed "+ii+" Lines");
				
				String shopCode = sheet.getCell(0, ii).getContents().toString();
				Double total = Math.abs(Double.valueOf(sheet.getCell(1, ii).getContents().toString()));
				
				ShopTRPCharges trp = shopTRP.get(shopCode+","+date);
				if(trp != null){
					if(trp.getTotalTRPOwed() != null)
						trp.setTotalTRPOwed(trp.getTotalTRPOwed()+total);
					else
						trp.setTotalTRPOwed(total);
				}else{
					trp = new ShopTRPCharges();
					trp.setTotalTRPOwed(total);
					shopTRP.put(shopCode+","+date, trp);
				}
			}
			sheet = w.getSheet(0);
			for (ii = 0; ii < sheet.getRows(); ++ii){
				
				if(ii%100 == 0)
					System.out.println("Completed "+ii+" Lines");
				
				String shopCode = sheet.getCell(0, ii).getContents().toString();
				Double total = Math.abs(Double.valueOf(sheet.getCell(1, ii).getContents().toString()));
				
				ShopTRPCharges trp = shopTRP.get(shopCode+","+date);
				if(trp != null){
					if(trp.getTotalTRPDue() != null)
						trp.setTotalTRPDue(trp.getTotalTRPDue()+total);
					else
						trp.setTotalTRPDue(total);
				}else{
					trp = new ShopTRPCharges();
					trp.setTotalTRPDue(total);
					shopTRP.put(shopCode+","+date, trp);
				}
			}
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}
	
	private void parseCompiledRescindData(){
		
		File inputWorkbook = new File(fileName2);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(1);
			int ii = 0;
			for (ii = 0; ii < sheet.getRows(); ++ii){
				
				if(ii%100 == 0)
					System.out.println("Completed "+ii+" Lines");
				
				String shopCode = sheet.getCell(0, ii).getContents().toString();
				Double total = Math.abs(Double.valueOf(sheet.getCell(1, ii).getContents().toString()));
				
				ShopTRPCharges rescind = shopTRP.get(shopCode+","+date);
				if(rescind != null){
					if(rescind.getTotalRescindOwed() != null)
						rescind.setTotalRescindOwed(rescind.getTotalRescindOwed()+total);
					else
						rescind.setTotalRescindOwed(total);
				}else{
					rescind = new ShopTRPCharges();
					rescind.setTotalRescindOwed(total);
					shopTRP.put(shopCode+","+date, rescind);
				}
			}
			sheet = w.getSheet(0);
			for (ii = 0; ii < sheet.getRows(); ++ii){
				
				if(ii%100 == 0)
					System.out.println("Completed "+ii+" Lines");
				
				String shopCode = sheet.getCell(0, ii).getContents().toString();
				Double total = Math.abs(Double.valueOf(sheet.getCell(1, ii).getContents().toString()));
				
				ShopTRPCharges rescind = shopTRP.get(shopCode+","+date);
				if(rescind != null){
					if(rescind.getTotalRescindDue() != null)
						rescind.setTotalRescindDue(rescind.getTotalRescindDue()+total);
					else
						rescind.setTotalRescindDue(total);
				}else{
					rescind = new ShopTRPCharges();
					rescind.setTotalRescindDue(total);
					shopTRP.put(shopCode+","+date, rescind);
				}
			}
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}
	
	private void uploadData(){
		
		Iterator<String> it = shopTRP.keySet().iterator();
		while(it.hasNext()){
			String key = it.next();
			String dateComponent = key.split(",")[1];
			String shopCode = key.split(",")[0];
			ShopTRPCharges trp = shopTRP.get(key);
			Double totalTRPOwed = trp.getTotalTRPOwed();
			Double totalTRPDue = trp.getTotalTRPDue();
			Double totalNetTRP = totalTRPOwed - totalTRPDue;
			Double totalRescindOwed = trp.getTotalRescindOwed();
			Double totalRescindDue = trp.getTotalRescindDue();
			Double totalNetRescind = totalRescindOwed - totalRescindDue;
			Double netTotal = totalNetTRP + totalNetRescind;
			
			String statement = "INSERT INTO "+databaseName+".timely_reply_billing(billing_cycle,shop_code,total_trp_owed,"
					+ "total_trp_due,net_trp,total_rescind_owed,total_rescind_due,net_rescind,net_total,created_date) VALUES("
					+ "STR_TO_DATE('"+dateComponent+"','%c/%Y'),'"+shopCode+"',"+totalTRPOwed+","+totalTRPDue+","+totalNetTRP+","+totalRescindOwed+","
							+ ""+totalRescindDue+","+totalNetRescind+","+netTotal+",now());";
			mySQL.executeStatement(statement);
		}
	}

}
