package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

public class FindCorrectStates {
	
	private SQLData mySQL;
	private Map<String, String> stateZip = new HashMap<String,String>();
	
	public FindCorrectStates(){
		mySQL = new SQLData();
		mySQL.login();
		try {
			setStateZip();
			findCorrectStates();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void setStateZip() throws SQLException{
		
		System.out.println("Setting Zip to State References");
		
		String statement = "USE shopdata;";
		mySQL.executeStatement(statement);
		
		String query = "SELECT state.Name, zip.Zip_Code FROM city_zip_xref INNER JOIN city ON city_zip_xref.City_ID = city.City_ID " +
		"INNER JOIN state ON city.State_ID = state.State_ID INNER JOIN zip ON city_zip_xref.Zip_ID = zip.Zip_ID;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			
			String zip = results.getString(2);
			String state = results.getString(1);
			stateZip.put(zip, state);
		}
		
		System.out.println("Finished Setting Zip to State References");
	}
	
	public void findCorrectStates() throws IOException, SQLException{
		
		FileInputStream fstream = new FileInputStream("data/fixTheseCities.txt");
		  
		DataInputStream in = new DataInputStream(fstream);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String strLine;
		
		FileOutputStream fos = new FileOutputStream("logs/fixedCities.txt");
		OutputStreamWriter out = new OutputStreamWriter(fos);
		
		String cityId = "";
		String cityName = "";
		String stateName = "";
		String zipCode = "";
		
		while((strLine = br.readLine()) != null){
			
			String[] line = strLine.split(",");
			
			if(line.length == 4){
				cityId = line[0];
				cityName = line[1];
				stateName = line[2];
				zipCode = line[3];
				
				String name = stateZip.get(zipCode);
				
				if(name != null && !name.equals(stateName)){
					out.write(cityId+","+cityName+","+name+"\n");
				}
			}
		}
		br.close();
		out.close();
		fos.close();
	}
}
