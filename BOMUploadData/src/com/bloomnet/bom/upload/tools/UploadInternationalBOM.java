package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class UploadInternationalBOM {
	
	SQLData mySQL;
	private File countriesFile;
	private File associationFile;
	private File internationalShops;
	
	private HashMap<String,String> cityId = new HashMap<String,String>();
	private HashMap<String,String> countryId = new HashMap<String,String>();
	private HashMap<String,String> stateId = new HashMap<String,String>();
	private HashMap<String,String> countryTwoToThree = new HashMap<String,String>();
	private HashMap<String,String> countryFromCode = new HashMap<String,String>();
	private HashMap<String,String> shopCountries = new HashMap<String,String>();
	private HashMap<String,String> shopId = new HashMap<String,String>();
	private HashMap<String,String> zipId = new HashMap<String,String>();
	private HashMap<String,String> shopnetworkId = new HashMap<String,String>();
	


	
	public UploadInternationalBOM(String file, String file2, String file3, String databaseName){
		this.countriesFile = new File(file);
		this.associationFile = new File(file2);
		this.internationalShops = new File(file3);
		mySQL = new SQLData();
		mySQL.login();
		mySQL.executeStatement("USE "+databaseName+";");
		try {
			
		
			setCountryId();
			setCityId();
			setStateId();
			//setShopId();
			setZipId();
			setShopNetworkId();
			uploadInternationalCountries();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
private void setCountryId() throws SQLException{
		
		System.out.println("Setting Country IDs");
		
		String query = "SELECT Country_ID,Short_Name FROM country;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			countryId.put(results.getString("Short_Name").toUpperCase(), results.getString("Country_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Country IDs");
	}
	
	private void setStateId() throws SQLException{
		
		System.out.println("Setting State IDs");
		
		String query = "SELECT State_ID,Short_Name FROM state;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			stateId.put(results.getString("Short_Name").toUpperCase(), results.getString("State_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting State IDs");
	}

	private void setCityId() throws SQLException{
		
		System.out.println("Setting City IDs");
		
		String query = "SELECT city.City_ID,city.Name,state.Short_Name " +
					  "FROM city " +
					  "INNER JOIN state ON city.State_ID = state.State_ID;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			try{
				cityId.put(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase(), results.getString("City_ID"));
			}catch(Exception ee){
				System.out.println("Error Alert!");
			}
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting City IDs");
	}
	
	
	
	private void setZipId() throws SQLException{
		
		System.out.println("Setting Zip IDs");
		
		String query = "SELECT Zip_Code,Zip_ID FROM zip;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			zipId.put(results.getString("Zip_Code").toUpperCase(), results.getString("Zip_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Zip IDs");
	}
	private void setShopNetworkId() throws SQLException{
		
		System.out.println("Setting Shop Network IDs");
		
		String query = "SELECT ShopCode,ShopNetwork_ID FROM shopnetwork where Network_ID=1;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			shopnetworkId.put(results.getString("ShopCode").toUpperCase(), results.getString("ShopNetwork_ID"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Shop Network IDs");
	}
/*private void setShopId() throws SQLException{
	
	System.out.println("Setting Shop IDs");
	
	String query = "SELECT ShopCode,Shop_ID FROM shop;";
	
	ResultSet results = mySQL.executeQuery(query);
	
	while(results.next()){
		shopId.put(results.getString("ShopCode").toUpperCase(), results.getString("Shop_ID"));
	}
	
	mySQL.closeStatement();
	
	System.out.println("Finished Setting Shop IDs");
}*/
	
	protected String getId(){
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		String id = "";
		try {
			results.next();
			id = results.getString("LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mySQL.closeStatement();
		return id;
	}
	
	private void   insertIntoInternationalTable(String countryCodeThree, String shopCode) throws IOException{
		
		String statement = "INSERT INTO internationalShops(countryCode,shopCode) VALUES(\""+countryCodeThree+"\",\""+shopCode+"\");";
		System.out.println(statement);
		mySQL.executeStatement(statement);
	}
	
	
	private void insertCountry(String name,String shortName) throws IOException{
		
		String statement = "INSERT INTO country(Name,Short_Name,CreatedDate,CreatedUser_ID) VALUES(\""+name+"\",\""+shortName+"\", now(),1);";
		//System.out.println(statement);
		mySQL.executeStatement(statement);
		
	}
	
	private void insertState(String name,String countryId,String shortName) throws IOException{
		
		String statement = "INSERT INTO state(Name,Country_ID,Short_Name,CreatedDate,CreatedUser_ID, ModifiedDate) VALUES(\""+name+"\",\""+countryId+"\",\""+shortName+"\", now(),1,now());";
		//System.out.println(statement);
		mySQL.executeStatement(statement);
		
	}
	
	@SuppressWarnings("unused")
	private void insertCity(String name,String stateId) throws IOException{
		
		String statement = "INSERT INTO city(Name,State_ID,Major_City,City_Type,CreatedDate,CreatedUser_ID) VALUES(\""+name.replace("\"","\\\"")+"\",\""+stateId+"\",\"0\",\"D\", now(),1);";
		//System.out.println(statement);
		mySQL.executeStatement(statement);
		
	}
	
	private void insertZip(String zip) throws IOException{
		String statement = "INSERT INTO zip(Zip_Code,CreatedDate,CreatedUser_ID, ModifiedDate) VALUES(\""+zip+"\",now(),1,now());";
		System.out.println(statement);
		mySQL.executeStatement(statement);
		
	}

	
	private void insertShop(String name,String address,String phone, String contact,String cityId,String zip) throws IOException{
		
		String statement = "INSERT INTO shop(ShopName,ShopAddress1,ShopPhone, ShopContact, City_ID,Zip_ID,CreatedDate,CreatedUser_ID) VALUES(\""+name+"\",\""+address+"\",\""+phone+"\",\""+contact+"\",\""+cityId+"\",\""+zip+"\", now(),1);";
		System.out.println(statement);
		mySQL.executeStatement(statement);
		
	}
	
	private void insertShopNetwork(String shopCode,String name,String phone,String contact,String shopId) throws IOException{
		
		String statement = "INSERT INTO shopnetwork(Network_ID,ShopCode,Shop_ID,ShopNetworkStatus_ID,CommMethod_ID,OpenSunday,CreatedDate,Created_User_ID) "
				+ "VALUES(\"1"+"\",\""+shopCode+"\",\""+shopId+"\",1,1,0,now(),1);";
		System.out.println(statement);
		mySQL.executeStatement(statement);
		
	}
	
	private void insertShopNetworkCoverage(String shopNetworkId, String zip) throws IOException{
		
		String statement = "INSERT INTO shopnetworkcoverage(ShopNetwork_ID,Zip_ID,CreatedDate,CreatedUser_ID,ModifiedUser_ID) "
				+ "VALUES(\""+shopNetworkId+"\",\""+zip+"\",now(),1,1);";
		System.out.println(statement);
		mySQL.executeStatement(statement);
	}
	
	
	
	@SuppressWarnings({ "resource", "unused" })
	private void uploadInternationalCountries() throws IOException, SQLException{
		//countryCodes.csv
		  FileInputStream fstream = new FileInputStream(associationFile);
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  while((strLine = br.readLine()) != null){
			  String[] lineItems = strLine.split(",");
			  if(lineItems.length == 3){
				  String country = lineItems[0];
				  String countryCodeTwo = lineItems[1];
				  String countryCodeThree = lineItems[2];
				  countryTwoToThree.put(countryCodeTwo,countryCodeThree);
				  countryFromCode.put(countryCodeThree, country);
			  }
		  }
		  //internationalShops.csv
		  fstream = new FileInputStream(internationalShops);
		  in = new DataInputStream(fstream);
		  br = new BufferedReader(new InputStreamReader(in));
		  
		  while((strLine = br.readLine()) != null){
			  String[] lineItems = strLine.split(",");
			  if(lineItems.length >= 3){
				  String countryCodeThree = lineItems[0];
				  String shopCode = lineItems[2];
				  shopCountries.put(countryCodeThree,shopCode);
				  insertIntoInternationalTable(countryCodeThree,shopCode);
			  }
		  }
		  //worldcitiespop.csv
		  fstream = new FileInputStream(countriesFile);
		  in = new DataInputStream(fstream);
		  br = new BufferedReader(new InputStreamReader(in));
		  
		  int lineNumber = 0;
		  while((strLine = br.readLine()) != null){
			  try{
				  lineNumber++;
				  String[] lineItems = strLine.split(",");
				  //?
				  if(lineNumber%1000 == 0){         
					  System.out.println(lineNumber);
				  }
				  if(lineItems.length >= 2){
					  String countryCodeTwo = lineItems[0].toUpperCase();
					  String city = lineItems[1].toUpperCase();
					  String countryCodeThree = countryTwoToThree.get(countryCodeTwo);
					  //check to see if country in cities list is in country codes list
					  if(countryCodeThree != null){   
						  //check to see if country in cities list is in international shops list
						  if(shopCountries.get(countryCodeThree) != null){
							  String country = countryId.get(countryCodeThree);
							  //check to see if country is in db, if not insert country and state then put in hashmap
							  if(country == null){
								  //if not get country from country codes list
								  country = countryFromCode.get(countryCodeThree);
								  if(country == null){
									  System.out.println("Could not get country name from two character country code...");
									  continue;
								  }
								  country = country.toUpperCase();
								  insertCountry(country, countryCodeThree);
								  countryFromCode.put(countryCodeThree, country);
								  String insertId = getId();   
								  countryId.put(countryCodeThree, insertId);
								
								 insertState(country, insertId, countryCodeTwo);
								  
								 String  stateid = getId();
								 System.out.println("Stateid: " + stateid);
								  stateId.put(countryCodeThree, stateid);
							  }
							  //get stage from db by country from world cities.csv
							//  String state = stateId.get(countryCodeThree);
							  //FIXME state id does not seem to be correct
							  //check to see if city already exist, if not insert into db
							  /*if (state==null){
									 String ctry = countryId.get(countryCodeThree);
									//FIXME currently inserting country id as country 
									  //temp going to use countryCodeThree instead
									  //insertState(country, ctry, countryCodeThree);
									  insertState(countryCodeThree, ctry, countryCodeTwo);
									  state = getId();
									  stateId.put(countryCodeThree, state);
									  
								  }*/
								  
							  
							 /* if(cityId.get(city+","+countryCodeThree) == null){
								  insertCity(city, state);
								  cityId.put(city+","+countryCodeThree, getId());
							  }*/
						  }else{
							  //System.out.println("No shops in: "+countryCodeThree);
						  }
						  
					  }else{
						  //System.out.println("Could not get Three Character Country Code From Two Character Coutnry Code...");
					  }
				  }
			  }catch(Exception ee){
				System.out.println(ee.getMessage());  
			  }
		  }
		  
		  fstream = new FileInputStream(internationalShops);
		  in = new DataInputStream(fstream);
		  br = new BufferedReader(new InputStreamReader(in));
		  
		  while((strLine = br.readLine()) != null){
			  try{
				  String[] lineItems = strLine.split(",");
				  if(lineItems.length >= 13){
					  String countryCodeThree = lineItems[0];
					  String shopCode = lineItems[2];
					  String shopName = lineItems[5];
					  String shopContact = lineItems[6];
					  String shopAddress = lineItems[8];
					  String shopZip = lineItems[11];
					  String shopPhone = lineItems[12];
					  //check if shopcode exists in db
					  if(shopnetworkId.get(shopCode) == null){
						  String city = cityId.get("INTERNATIONAL,INT");
						  System.out.println("city: " + city);
						  //check if zip exists in db, if not save zip and put in hashmap
						  if (zipId.get(shopZip)==null){
						  insertZip(shopZip);
						  zipId.put(shopZip,getId());
						  }

						  insertShop( shopName, shopAddress, shopPhone,  shopContact, city, zipId.get(shopZip));
						  String shop_Id = getId();
						  shopId.put(shopCode, shop_Id);
						  insertShopNetwork(shopCode, shopName, shopPhone, shopContact, shop_Id);
						  String shopNetworkId =  getId();
						  shopnetworkId.put(shopCode, shopNetworkId);
						  insertShopNetworkCoverage(shopNetworkId, zipId.get(shopZip));
						  
						  
					  }
					  
				
					  
					/*  String state = stateId.get(countryCodeThree);
					  if(state != null){
						  String query = "SELECT id FROM city WHERE state_id = "+state+";";
						  ResultSet results = mySQL.executeQuery(query);
						  int ii=0;
						  while(results.next()){
							  ii++;
							  String cityId = results.getString("id");
							  insertListing(shopCode, String.valueOf(ii), cityId);
						  }
					  }else{
						  System.out.println("No Country Found for Shop For: "+countryCodeThree);
					  }*/
				  }
			  }catch(Exception ee){
				System.out.println(ee.getMessage());  
			  }
		  }
	}
	
}
