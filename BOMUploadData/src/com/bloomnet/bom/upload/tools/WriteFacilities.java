package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class WriteFacilities {
	
	HashMap<String,String> existingStates = new HashMap<String,String>();
	SQLData mySQL;
	String databaseName;
	String fileName;
	Map<String,String> messageTypes = new HashMap<String,String>();
	Map<String, String> cityZipXref = new HashMap<String,String>();
	
	public WriteFacilities(String fileName, String databaseName, List<String> shopsToUse){
		
		this.fileName = fileName;
		this.databaseName = databaseName;

		mySQL = new SQLData();
		mySQL.login();
		
		try {
			setXrefData();
			writeFacilityData(shopsToUse);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void setXrefData() throws SQLException{
		
		String statement = "USE bloomnet;";
		mySQL.executeStatement(statement);
		
		String query = "SELECT zip_code, city.name FROM zip "
		  		+ "INNER JOIN city_zip_xref xref ON zip.id = xref.id_zip "
		  		+ "INNER JOIN city ON xref.id_city = city.id "
		  		+ "WHERE city.city_type = \"D\";";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			cityZipXref.put(results.getString(1),results.getString(2));
		}
	}
		
	private void writeFacilityData(List<String> shopsToUse) throws IOException{
		
		FileInputStream fstream = null;
		File outFile = new File("data/CommerceShopData.txt");
		BufferedWriter out = new BufferedWriter(new FileWriter(outFile));
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else{
			  System.err.println("No file to parse");
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  if((strLine = br.readLine()) != null){
			  System.out.println("File Good");
		  }
		  
		  String shopCode;
		  String shopName;
		  String[] zips;
		  String writeLine = "";
		  out.write(writeLine);
		  out.flush();
		  String writeLine2 = "";
		  
		  String previousFacType = "";
		  
		  while((strLine = br.readLine()) != null){
			  
			  try{
				 
				  count++;
				  if(count%100 == 0){
					  System.out.println(count+" lines completed.");
				  }
				  writeLine = "";
				  writeLine2 = "";
					  
				  String[] lineItems = strLine.split("\t");
				  if(lineItems.length >= 16){
					  shopCode = lineItems[0];
					  shopName = lineItems[1];
					  zips = lineItems[15].split(",");
					  
					  String zipsString = "";
					 if((shopsToUse != null && shopsToUse.contains(shopCode)) || shopsToUse == null || shopsToUse.size() == 0){ 
						  for(int ii=0; ii<zips.length; ii++){
							  if(ii == zips.length -1)
								  zipsString += zips[ii];
							  else
								  zipsString += zips[ii] + ",";
						  }
						  
						  String query = "SELECT f.name, t.name, telephone_number, street_address1, c.name, s.short_name, postal_code"
								+ " FROM facility f"
								+ " INNER JOIN address a ON f.address_id = a.id"
								+ " INNER JOIN city c ON a.city_id = c.id"
								+ " INNER JOIN state s ON c.state_id = s.id"
								+ " INNER JOIN facility_type t ON f.facility_type_id = t.id"
								+ " WHERE a.postal_code IN ("+zipsString+") ORDER BY t.name,f.name ASC;";
						  
						  ResultSet results = mySQL.executeQuery(query);
						  
						  while(results.next()){
							  String facType = results.getString("t.name");
							  if(!facType.equalsIgnoreCase(previousFacType)){
								  if(!previousFacType.equals("")){
									  writeLine += "\t</tbody>\n"
											  + "</table>\n";
								  }
								  writeLine += facType + "S:\n";
								  writeLine += "<table border=\"0\" cellpadding=\"0\" cellspacing=\"0\" style=\"width: 765px;\" width=\"765\"> \n" +
										  " \t <colgroup>\n" +
										  " \t\t<col />\n" +
										  "\t</colgroup>\n"+
										  "\t<tbody>\n";
								  previousFacType = facType;
							  }
							  writeLine += "\t\t<tr height=\"20\">\n"
							  		+ "\t\t\t<td height=\"20\" style=\"height: 20px;\">" + results.getString(1)+" located at "+ results.getString(4)+", "+results.getString(5)+", "+results.getString(6)+", "+results.getString(7)+"</td>\n"
							  		+ "\t\t</tr>\n";
						  }
						  results.close();
						  mySQL.closeStatement();
						  
							  writeLine2 += shopCode+"\t"+shopName+" serves the following cities: ";
							  List<String> citiesGotten = new ArrayList<String>();
							  for(int ii=0; ii<zips.length; ii++){
								  String zip = zips[ii];
								  String city = cityZipXref.get(zip);
								  if(!citiesGotten.contains(city) && city != null && !city.equals("")){
									  writeLine2 += city + ",";
									  citiesGotten.add(city);
								  }
							  }
							  writeLine2 = writeLine2.substring(0, writeLine2.length()-1);
							  writeLine2 += " and zip codes: "+zipsString+"\n";
						  
						  out.write(writeLine);
						  out.flush();
						  writeLine = "\t</tbody>\n"
						  		+ "</table>\n";
						  out.write(writeLine);
						  out.flush();
						  out.write("Coverage:\n"+writeLine2.replaceAll(",",", "));
						  out.flush();
					 }
				 }
				}catch(Exception ee){
					ee.printStackTrace();
				}
		  }
		  out.flush();
		  out.close();
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
	
	public static void main(String[] args){
		System.out.println("Enter the shops (comma seperated). Type All to run for all shops:");
		Scanner user_input = new Scanner( System.in );
		String input = user_input.next();
		ArrayList<String> shopsToUse = new ArrayList<String>();
		if(input.equalsIgnoreCase("All")){
			//keep the array list empty
		}else if(input.contains(",")){
			String[] values = input.split(",");
			for(int ii=0; ii<values.length; ++ii){
				shopsToUse.add(values[ii]);
			}
		}else{
			shopsToUse.add(input);
		}
		new WriteFacilities("data/bmt.txt", "bloomnet", shopsToUse);
		user_input.close();
	}
	
}
