package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class UploadTRPData {
	
	HashMap<String,String> existingStates = new HashMap<String,String>();
	SQLData mySQL;
	String databaseName;
	String fileName;
	String fileName2;
	Map<String,String> messageTypes = new HashMap<String,String>();
	
	public UploadTRPData(String fileName, String fileName2, String databaseName){
		
		this.fileName = fileName;
		this.fileName2 = fileName2;
		this.databaseName = databaseName;

		mySQL = new SQLData();
		mySQL.login();
		
		try {
			setMessageTypes();
			uploadTRPData();
			uploadRescindData();
		} catch (SQLException e) {
		}
	}
	
	private void setMessageTypes() throws SQLException{
		
		System.out.println("Setting Message Type IDs");
		
		String query = "SELECT * FROM "+databaseName+".trp_message_type;";
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			String type = results.getString("message_type");
			String subType = results.getString("message_subtype");
			String id = results.getString("id");
			
			if(subType != null)
				messageTypes.put(type+","+subType, id);
			else
				messageTypes.put(type, id);	
		}
		
		System.out.println("Finished Setting Message Type IDs");
		
	}
	
	
	private void uploadTRPData(){
		
		String orderNumber,atlasNumber,sendingShopCode,fulfillingShopCode,
		messageType,messageSubType,orderDate,orderTime,deliveryDate,price,
		dlnRejDate,dlnRejTimeDate,rejDeltaHours,messageTypeId;
		
		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  @SuppressWarnings("resource")
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  if((strLine = br.readLine()) != null){
			  System.out.println("File Good");
		  }
		  
		  while((strLine = br.readLine()) != null){
			  
			  try{
				  if(count%100 == 0){
					  System.out.println(count+" lines completed.");
				  }
					  
				  String[] lineItems = strLine.split(",");
				  orderNumber = lineItems[0];
				  atlasNumber = lineItems[1];
				  sendingShopCode = lineItems[2];
				  if(sendingShopCode != null && sendingShopCode.equals("00800000"))
						  sendingShopCode = "800000";
				  fulfillingShopCode = lineItems[3];
				  messageType = lineItems[4];
				  messageSubType = lineItems[5];
				  orderDate = lineItems[7];
				  orderTime = lineItems[8];
				  deliveryDate = lineItems[9];
				  price = lineItems[10];
				  if(lineItems.length > 13){
					  dlnRejDate = lineItems[11];
					  dlnRejTimeDate = lineItems[12];
					  rejDeltaHours = lineItems[13];
				  }else{
					  dlnRejDate = "";
					  dlnRejTimeDate = "";
					  rejDeltaHours = ""; 
				  }
				  
				  Double feeAmount = 0.00;
				  Double elapsedHours = 0.00;
					String elapsedHoursString = rejDeltaHours;
					if(elapsedHoursString != null && !elapsedHoursString.equals("")){
						elapsedHours = Double.valueOf(elapsedHoursString);
					}
				  messageTypeId = messageTypes.get(messageType+","+messageSubType);
				  Date d1 = new Date();
				  if(deliveryDate != null && !deliveryDate.equals("") && !deliveryDate.equals("0000-00-00"))
					  d1 = new SimpleDateFormat("MM/dd/yyyy").parse(deliveryDate);
				  Date d2 = new Date();
				  if(dlnRejDate != null && !dlnRejDate.equals("") && !dlnRejDate.equals("0000-00-00"))
					  d2 = new SimpleDateFormat("MM/dd/yyyy").parse(dlnRejDate);
				  
				  if(messageTypeId.equals("1")){ //No Delivery Notification 
						feeAmount = 2.25;
					}else if(messageTypeId.equals("2")){ //Late delivery Notification
						feeAmount = 2.25;
					}else{ //Late response
					    if(((d2.getTime() > d1.getTime()) || (d2.getTime() == d1.getTime() && Integer.valueOf(dlnRejTimeDate.replaceAll(":", "")) > 150000 ))){//3:00PM EST (Cut-off Time)
					    	feeAmount = Double.valueOf(price) *2;
						}else if(elapsedHours > 72.00){
							feeAmount = Double.valueOf(price);
						}else if(elapsedHours > 24.00){
							feeAmount = 20.00;
						}else if(elapsedHours > 2.00){
							feeAmount = 10.00;
						}
					}
				  
				  if(dlnRejDate != null && !dlnRejDate.equals("")){
					  String statement = "INSERT INTO "+databaseName+".timely_reply_process (order_number,atlas_order_number,sending_shop_code,fulfilling_shop_code,"
					  		+ "message_type_id,order_created_date,order_created_time,delivery_date,order_value,dln_rej_date,dln_rej_date_time,rej_delta_hours,"
					  		+ "processed,created_date,total_fee) VALUES('"+orderNumber+"','"+atlasNumber+"','"+sendingShopCode+"','"+fulfillingShopCode+"','"+messageTypeId+"',STR_TO_DATE('"+orderDate+"','%c/%e/%Y')"
					  		+ ",'"+orderTime+"',STR_TO_DATE('"+deliveryDate+"','%c/%e/%Y'),'"+price+"',STR_TO_DATE('"+dlnRejDate+"','%c/%e/%Y'),'"+dlnRejTimeDate+"','"+rejDeltaHours+"','0',now(),"+feeAmount+");";
					  mySQL.executeStatement(statement);
				  }else{
					  String statement = "INSERT INTO "+databaseName+".timely_reply_process (order_number,atlas_order_number,sending_shop_code,fulfilling_shop_code,"
						  		+ "message_type_id,order_created_date,order_created_time,delivery_date,order_value,dln_rej_date,dln_rej_date_time,rej_delta_hours,"
						  		+ "processed,created_date,total_fee) VALUES('"+orderNumber+"','"+atlasNumber+"','"+sendingShopCode+"','"+fulfillingShopCode+"','"+messageTypeId+"',STR_TO_DATE('"+orderDate+"','%c/%e/%Y')"
						  		+ ",'"+orderTime+"',STR_TO_DATE('"+deliveryDate+"','%c/%e/%Y'),'"+price+"',NULL,'"+dlnRejTimeDate+"','"+rejDeltaHours+"','0',now(),"+feeAmount+");";
						  mySQL.executeStatement(statement);
				  }
				  count++;
			  }catch(Exception e){
				  e.printStackTrace();
			  }
		  
		  }
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
	
	private void uploadRescindData(){
		
		String orderNumber,atlasNumber,sendingShopCode,fulfillingShopCode,
		messageType,messageDescription,orderDate,orderTime,deliveryDate,price,
		rescindDate,rescindTime,messageTypeId;
		
		FileInputStream fstream;
		
		String updateStatement = "UPDATE "+databaseName+".rescind_process SET processed = 1 WHERE processed = 0;";
		mySQL.executeStatement(updateStatement);
		
		try {
		  
		  if(fileName2 != null) fstream = new FileInputStream(fileName2);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  @SuppressWarnings("resource")
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  if((strLine = br.readLine()) != null){
			  System.out.println("File Good");
		  }
		  
		  while((strLine = br.readLine()) != null){
			  
			  try{
				  if(count%100 == 0){
					  System.out.println(count+" lines completed.");
				  }
					  
				  String[] lineItems = strLine.split(",");
				  orderNumber = lineItems[0];
				  atlasNumber = lineItems[1];
				  sendingShopCode = lineItems[2];
				  if(sendingShopCode != null && sendingShopCode.equals("00800000"))
					  sendingShopCode = "800000";
				  fulfillingShopCode = lineItems[3];
				  messageType = lineItems[4];
				  messageDescription = lineItems[6].replaceAll("'", "''");
				  orderDate = lineItems[7];
				  orderTime = lineItems[8];
				  deliveryDate = lineItems[9];
				  price = lineItems[10];
				  if(lineItems.length > 12){
					  rescindDate = lineItems[11];
					  rescindTime = lineItems[12];
				  }else{
					  rescindDate = "";
					  rescindTime = "";
				  }
				  messageTypeId = messageTypes.get(messageType);
				  
				  if(rescindDate != null && !rescindDate.equals("")){
					  String statement = "INSERT INTO "+databaseName+".rescind_process (order_number,atlas_order_number,sending_shop_code,fulfilling_shop_code,"
					  		+ "message_type_id,message_text,order_created_date,order_created_time,delivery_date,order_value,rescind_date,rescind_date_time,processed,created_date"
					  		+ ") VALUES('"+orderNumber+"','"+atlasNumber+"','"+sendingShopCode+"','"+fulfillingShopCode+"','"+messageTypeId+"','"+messageDescription+"',STR_TO_DATE('"+orderDate+"','%c/%e/%Y')"
					  		+ ",'"+orderTime+"',STR_TO_DATE('"+deliveryDate+"','%c/%e/%Y'),'"+price+"',STR_TO_DATE('"+rescindDate+"','%c/%e/%Y'),'"+rescindTime+"','0',now());";
					  mySQL.executeStatement(statement);
				  }else{
					  String statement = "INSERT INTO "+databaseName+".rescind_process (order_number,atlas_order_number,sending_shop_code,fulfilling_shop_code,"
						  		+ "message_type_id,message_text,order_created_date,order_created_time,delivery_date,order_value,rescind_date,rescind_date_time,processed,created_date"
						  		+ ") VALUES('"+orderNumber+"','"+atlasNumber+"','"+sendingShopCode+"','"+fulfillingShopCode+"','"+messageTypeId+"','"+messageDescription+"',STR_TO_DATE('"+orderDate+"','%c/%e/%Y')"
						  		+ ",'"+orderTime+"',STR_TO_DATE('"+deliveryDate+"','%c/%e/%Y'),'"+price+"',NULL,'"+rescindTime+"','0',now());";
						  mySQL.executeStatement(statement);
				  }
				  count++;
			  }catch(Exception e){
				  e.printStackTrace();
			  }
		  
		  }
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
}
