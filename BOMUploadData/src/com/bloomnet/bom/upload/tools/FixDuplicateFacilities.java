package com.bloomnet.bom.upload.tools;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import jxl.Sheet;
import jxl.Workbook;

public class FixDuplicateFacilities {
	
	public void parseFileAndCorrectData() throws SQLException{
				
		String id = "";
		String isDup = "";
		String phone = "";
		String otherId = "";
		
		SQLData mySQL = new SQLData();
		mySQL.login();
		mySQL.executeStatement("USE bloomnet;");
		
		Sheet sheet = openExcelSheet("data/Duplicates.xls");
		
		for (int ii = 0; ii < sheet.getRows(); ++ii){
			
			id = sheet.getCell(0,ii).getContents().toString();
			otherId = "";
			isDup = sheet.getCell(1,ii).getContents().toString();
			phone = sheet.getCell(3,ii).getContents().toString();
			
			try{
				
				if(isDup.equals("dup")){
					
					String query = "SELECT id FROM facility WHERE telephone_number = \""+phone+"\";";
					ResultSet results = mySQL.executeQuery(query);
					
					while(results.next()){
						if(!results.getString("id").equals(id)){
							otherId = results.getString("id");
						}
					}
					
					results.close();
					mySQL.closeStatement();
					if(!otherId.equals("")){
						String statement = "UPDATE shop_facility_xref SET facility_id = \""+otherId+"\" WHERE facility_id = \""+id+"\";";
						mySQL.executeStatement(statement);
						statement = "DELETE FROM facility WHERE id = \""+id+"\"";
						mySQL.executeStatement(statement);
					}
				}
				
				
			}catch(Exception ee){
				System.out.println(ee.getMessage()); 
				System.out.println("id: "+id);
				System.out.println("other id: "+otherId);
			}
			
		}
		
			
	}

	private Sheet openExcelSheet(String filename){
		
		File inputWorkbook = new File(filename);
		Workbook w = null;
		Sheet sheet = null;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			sheet = w.getSheet(0);
			return sheet;
		}catch(Exception ee){ee.printStackTrace();}
		return null;
	}
	
	public static void main(String[] args){
		try {
			new FixDuplicateFacilities().parseFileAndCorrectData();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
}
