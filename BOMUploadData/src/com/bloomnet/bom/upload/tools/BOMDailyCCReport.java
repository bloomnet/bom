package com.bloomnet.bom.upload.tools;

import java.io.*;
import java.sql.*;
import java.util.Calendar;

import org.apache.poi.xssf.usermodel.XSSFCell; 
import org.apache.poi.xssf.usermodel.XSSFRow; 
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook; 


public class BOMDailyCCReport {
	
	SQLData mySQL = new SQLData();

    public BOMDailyCCReport(){
    	
    	String year = String.valueOf(Calendar.getInstance().get(Calendar.YEAR));
  	
        mySQL.login();
        String statement = "USE bloomnetordermanagement;";
        mySQL.executeStatement(statement);
        System.out.println("INITIALIZED AND DATABASE SELECTED");
        String query = "SELECT (SELECT OrderNumber FROM bomorder bo WHERE BOMOrder_ID = bomorder.ParentOrder_ID) AS ParentOrderNumber,"
        		 + " SUBSTRING(OrderXML,(LOCATE('<recipientLastName>',OrderXML)+19),(LOCATE('</recipientLastName>',OrderXML))-LOCATE('<recipientLastName>',OrderXML)-19) AS LastName,"
        		 + " orderactivity.CreatedDate,"
        		 + " CONCAT(user.FirstName,\" \",user.LastName) AS UserName,"
        		 + " act_payment.PaymentAmount,"
        		 + " IFNULL((SELECT Price FROM orderactivity o2"
        		 + " INNER JOIN act_audit au2 ON o2.OrderActivity_ID = au2.OrderActivity_ID"
        		 + " WHERE o2.BOMOrder_ID = bomorder.ParentOrder_ID LIMIT 1), act_payment.PaymentAmount) AS OriginalAmount,"
        		 + " act_payment.PayTo,"
        		 + " paymenttype.Description,"
        		 + " \"TLO\" as Route, ShopAddress1, city.name AS CityName, state.Short_Name AS StateName, Zip_Code, ShopPhone"
        		 + " FROM bomorder"
        		 + " INNER JOIN orderactivity ON bomorder.BomOrder_ID = orderactivity.BomOrder_ID"
        		 + " INNER JOIN act_payment ON orderactivity.OrderActivity_ID = act_payment.OrderActivity_ID"
        		 + " INNER JOIN paymenttype ON act_payment.PaymentType_ID = paymenttype.PaymentType_ID"
        		 + " INNER JOIN shop ON bomorder.ReceivingShop_ID = shop.Shop_ID"
        		 + " INNER JOIN city ON shop.City_ID = city.City_ID"
        		 + " INNER JOIN state ON city.State_ID = state.State_ID"
        		 + " INNER JOIN zip ON shop.Zip_ID = zip.Zip_ID"
        		 + " INNER JOIN user ON orderactivity.User_ID = user.User_ID"
        		 + " WHERE bomorder.CreatedDate >= \""+year+"0101\" "
        		 + " AND bomorder.CreatedDate < now()"
        		 + " AND bomorder.OrderNumber LIKE \"BMT%\""
        		 + " AND paymenttype.Description = \"Credit Card\";";
        
        String filePath = "/root/automatedqueries/BOMCCReport.xlsx";
        
        createWorkbook(filePath, query);
    
    }
    
    private void createWorkbook(String filePath, String query){
    	try
        {
    		ResultSet queryResults = mySQL.executeQuery(query);
            @SuppressWarnings("resource")
			XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet("Sheet1");
            
            XSSFRow row = sheet.createRow(0);
            
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("Order Number");
            cell = row.createCell(1);
            cell.setCellValue("Last Name");
            cell = row.createCell(2);
            cell.setCellValue("Date");
            cell = row.createCell(3);
            cell.setCellValue("Agent");
            cell = row.createCell(4);
            cell.setCellValue("Amount Paid");
            cell = row.createCell(5);
            cell.setCellValue("Original Amount");
            cell = row.createCell(6);
            cell.setCellValue("Shop Name");
            cell = row.createCell(7);
            cell.setCellValue("Shop Address");
            cell = row.createCell(8);
            cell.setCellValue("Shop City");
            cell = row.createCell(9);
            cell.setCellValue("Shop State");
            cell = row.createCell(10);
            cell.setCellValue("Shop Zip");
            cell = row.createCell(11);
            cell.setCellValue("Shop Phone");
            cell = row.createCell(12);
            cell.setCellValue("Payment Type");
            cell = row.createCell(13);
            cell.setCellValue("Queue");

    		int ii = 1;
    				
            while(queryResults.next()){
            	
            	row = sheet.createRow(ii);
            	
                String orderNum = queryResults.getString("ParentOrderNumber");
                String lastName = queryResults.getString("LastName");
                String createdDate = queryResults.getString("CreatedDate");
                String userName = queryResults.getString("UserName");
                String paymentAmount = queryResults.getString("PaymentAmount");
                String originalAmount = queryResults.getString("OriginalAmount");
                String payTo = queryResults.getString("PayTo");
                String paymentType = queryResults.getString("Description");
                String vQueue = queryResults.getString("Route");
                String address = queryResults.getString("ShopAddress1");
                String city = queryResults.getString("CityName");
                String state = queryResults.getString("StateName");
                String zip = queryResults.getString("Zip_Code");
                String phone = queryResults.getString("ShopPhone");
                
                cell = row.createCell(0);
                cell.setCellValue(String.valueOf(orderNum).toString());
                cell = row.createCell(1);
                cell.setCellValue(String.valueOf(lastName).toString());
                cell = row.createCell(2);
                cell.setCellValue(String.valueOf(createdDate).toString());
                cell = row.createCell(3);
                cell.setCellValue(String.valueOf(userName).toString());
                cell = row.createCell(4);
                cell.setCellValue(String.valueOf(paymentAmount).toString());
                cell = row.createCell(5);
                cell.setCellValue(String.valueOf(originalAmount).toString());
                cell = row.createCell(6);
                cell.setCellValue(String.valueOf(payTo).toString());
                cell = row.createCell(7);
                cell.setCellValue(String.valueOf(address).toString());
                cell = row.createCell(8);
                cell.setCellValue(String.valueOf(city).toString());
                cell = row.createCell(9);
                cell.setCellValue(String.valueOf(state).toString());
                cell = row.createCell(10);
                cell.setCellValue(String.valueOf(zip).toString());
                cell = row.createCell(11);
                cell.setCellValue(String.valueOf(phone).toString());
                cell = row.createCell(12);
                cell.setCellValue(String.valueOf(paymentType).toString());
                cell = row.createCell(13);
                cell.setCellValue(String.valueOf(vQueue).toString());
               
                ii++;
            }

            queryResults.close();
            mySQL.closeStatement();
            
            FileOutputStream fileOut = new FileOutputStream(filePath);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close(); 
        }
        catch(IOException e){
            e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }


    public static void main(String args[]){
    	System.out.println("HITTING MAIN METHOD");
    	new BOMDailyCCReport();
    }
}
