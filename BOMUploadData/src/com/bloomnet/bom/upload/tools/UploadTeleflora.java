package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.LinkedList;

public class UploadTeleflora extends BaseUpload{
	
	private String fileName;
	private File file;
	
	
	public UploadTeleflora(File file, String databaseName) {
		super("logs/TFBadData.txt","logs/TFError.txt","logs/TFNewShopCodes.txt","logs/TFReUpload.txt", databaseName);
		this.file = file;
		setExistingShopCodes("2");
		parseTelefloraData();
		try {
			updateAvailability();
			updateCoverage();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public UploadTeleflora(String fileName, String databaseName){
		super("logs/TFBadData.txt","logs/TFError.txt","logs/TFNewShopCodes.txt","logs/TFReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("2");
		parseTelefloraData();
		try {
			updateAvailability();
			updateCoverage();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public UploadTeleflora(String fileName, String dummy, String databaseName){
		super("logs/TFBadData.txt","logs/TFError.txt","logs/TFNewShopCodes.txt","logs/TFReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("2");
		parseTelefloraData();
	}
	
	private String uploadShop(String shopName, String address, String phone, String shopContact, String fax, 
			String number800, String existingZipId, String existingCityId, String city, String state){
		
		String statement = "INSERT INTO "+databaseName+".shop " +
								"(ShopName," +
								"ShopAddress1," +
								"ShopAddress2," +
								"ShopPhone," +
								"ShopContact," +
								"ShopFax," +
								"Shop800," +
								"ShopEmail," +
								"Zip_ID," +
								"City_ID,"+
								"CreatedDate," +
								"CreatedUser_ID," +
								"ModifiedDate," +
								"ModifiedUser_ID) " +
								"VALUES (\""+shopName+"\"," +
								"\""+address+"\"," +
								"\"\"," +
								"\""+phone+"\"," +
								"\""+shopContact+"\"," +
								"\""+fax+"\"," +
								"\""+number800+"\"," +
								"\"\"," +
								""+Integer.valueOf(existingZipId)+"," +
								""+Integer.valueOf(existingCityId)+"," +
								"now()," +
								"1," +
								"now()," +
								"1);";

		mySQL.executeStatement(statement);
		String shopId = getId();
		
		shopsByPhone.put(phone,shopId);
		shopsByAddress.put(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase(), shopId);
		
		return shopId;
	}
	
	private void updateShop(String shopName, String address, String phone, String shopContact, String fax, 
			String number800, String existingZipId, String existingCityId, String existingShopId){
		
		String statement = "UPDATE "+databaseName+".shop SET " +
							  "ShopName = \""+shopName+"\"" +
							  ",ShopAddress1 = \""+address+"\"" +
							  ",ShopAddress2 = ''" +
							  ",ShopPhone = \""+phone+"\"" +
							  ",ShopContact = \""+shopContact+"\"" +
							  ",ShopFax = \""+fax+"\"" +
							  ",Shop800 = \""+number800+"\"" +
							  ",ShopEmail = ''" +
							  ",Zip_ID = "+Integer.valueOf(existingZipId)+"" +
							  ",City_ID = "+Integer.valueOf(existingCityId)+"" +
							  ",ModifiedDate = now()" +
							  ",ModifiedUser_ID = 1 " +
							  "WHERE Shop_ID = "+Integer.valueOf(existingShopId)+";";

		mySQL.executeStatement(statement);
	}
	
	private String uploadNetwork(String shopId, String shopCode, String commMethod, String openSunday){
		
		String statement = "INSERT INTO "+databaseName+".shopnetwork " +
						  		"(Shop_ID," +
						  		"Network_ID," +
						  		"ShopCode," +
						  		"ShopNetworkStatus_ID," +
						  		"CommMethod_ID," +
						  		"OpenSunday," +
						  		"CreatedDate," +
						  		"Created_User_ID," +
						  		"ModifiedDate," +
						  		"ModifiedUser_ID) " +
						  		"VALUES ("+
						  		""+Integer.valueOf(shopId)+"," +
						  		"2,\""+
						  		shopCode+"\"," +
						  		"1,";
  
	  if(commMethod.equals("X")) statement += "1,"+
	  									  Byte.valueOf(openSunday)+"," +
	  									  "now()," +
	  									  "1," +
	  									  "now()," +
	  									  "1);";
	 
	  else statement +=  "2,"+
	  				 Byte.valueOf(openSunday)+"," +
	  				 "now()," +
	  				 "1," +
	  				 "now()," +
	  				 "1);";
	 
	  mySQL.executeStatement(statement);
	 
	  String networkId = getId();
	 
	  shopNetwork.put(shopId+","+"2", networkId);
	  
	  existingShopCodes.put(shopCode, shopId);
	  
	  return networkId;
	}
	
	private void updateNetwork(String commMethod, String openSunday, String networkId){
		
		String statement = "UPDATE "+databaseName+".shopnetwork SET ";
		
	  if(commMethod.equals("X")) statement += "CommMethod_ID = 1," +
								  "OpenSunday = "+Byte.valueOf(openSunday)+"," +
								  "ModifiedDate = now()," +
								  "ModifiedUser_ID = 1 ";

	  else statement += "CommMethod_ID = 2," +
						  "OpenSunday = "+Byte.valueOf(openSunday)+"," +
						  "ModifiedDate = now()," +
						  "ModifiedUser_ID = 1 ";
	  
	  statement += "WHERE ShopNetwork_ID = "+Integer.valueOf(networkId)+";";
	  
	  mySQL.executeStatement(statement);
	}
	
	private void uploadCoverage(String listingCity, String listingState, String networkId){
		
		 LinkedList<String> zips = allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase());
		 
		if(zips != null){
			  
			 
			  for(int ii=0; ii<zips.size(); ii++){
				  
				  String zipId2 = zipId.get(zips.get(ii));
				  
				  if(zipId2 != null && !zipId2.equals("")){
					  if(shopNetworkCoverage.get(networkId+","+zipId2) == null){
						  String statement = "INSERT INTO "+databaseName+".shopnetworkcoverage " +
						  		"(ShopNetwork_ID," +
						  		"Zip_ID," +
						  		"CreatedDate," +
						  		"CreatedUser_ID," +
						  		"ModifiedDate," +
						  		"ModifiedUser_ID) " +
						  		"VALUES (" +
				  				""+Integer.valueOf(networkId)+","+
				  				Integer.valueOf(zipId2)+"" +
				  				",now()," +
				  				"1," +
				  				"now()," +
				  				"1);";
						  
						  mySQL.executeStatement(statement);
						  
						  shopNetworkCoverage.put(networkId+","+zipId2,1);
					  }
				  }
			  }
		}
	}
	
	@SuppressWarnings("unused")
	public void parseTelefloraData(){
		
		String shopCode,shopName,shopContact,address,city,state,listingCity,listingState,zip,commMethod,phone,fax,number800,
		cseq,zseq,colGroup,listingCountry,phCd1,phone2,phCd2,closePm1,closePm2,closePm3,closePm4,closePm5,closePm6,closePm7,
		close1,close2,close3,close4,close5,close6,close7,rrDelchInd,rrDelchG,delchGInd,delchG,merc,fto,mrk1,mrk2,mrk3,mrk4,mrk5,mrk6,mrk7,mrk8,mrk9,
		amt1,amt2,amt3,amt4,amt5,amt6,amt7,amt8,amt9,hol1,hol2,hol3,hol4,hol5,hol6,hol7,hol8,hol9,zipFacility,xMinOrd,areaTollFree,p800,listType,rtiInd,
		cityNote,delvTracking,hour24,sundayDeliv,productInd,intlPre,adSize,fontType,fontColor,hasDoveText,nameOfGif,openSunday;

		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else if(file != null)fstream = new FileInputStream(file);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  //String delete = "DELETE FROM florist.teleflora_parsed;";
		  //mySQL.executeStatement(delete);
		  
		  while((strLine = br.readLine()) != null){
			  
			  ++count;
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);
			  }
			  
			  cseq = strLine.substring(0,4).trim();
			  zseq = strLine.substring(4,8).trim();
			  shopCode = strLine.substring(8,16).trim();
			  colGroup = strLine.substring(16,17).trim();
			  listingState = strLine.substring(17,19).trim();
			  listingCity = strLine.substring(19,59).trim();
			  listingCountry = strLine.substring(59,89).trim();
			  shopName = strLine.substring(89,149).trim().replaceAll("\"","'");
			  phone = strLine.substring(149,159).trim();
			  phCd1 = strLine.substring(159,160).trim();
			  phone2 = strLine.substring(160,170).trim();
			  phCd2 = strLine.substring(170,171).trim();
			  shopContact = strLine.substring(171,221).trim().replaceAll("\"","'");
			  closePm1 = strLine.substring(221,222).trim();
			  closePm2 = strLine.substring(222,223).trim();
			  closePm3 = strLine.substring(223,224).trim();
			  closePm4 = strLine.substring(224,225).trim();
			  closePm5 = strLine.substring(225,226).trim();
			  closePm6 = strLine.substring(226,227).trim();
			  closePm7 = strLine.substring(227,228).trim();
			  close1 = strLine.substring(228,229).trim();
			  close2 = strLine.substring(229,230).trim();
			  close3 = strLine.substring(230,231).trim();
			  close4 = strLine.substring(231,232).trim();
			  close5 = strLine.substring(232,233).trim();
			  close6 = strLine.substring(233,234).trim();
			  close7 = strLine.substring(234,235).trim();
			  rrDelchInd = strLine.substring(235,236).trim();
			  rrDelchG = strLine.substring(236,242).trim();
			  delchGInd = strLine.substring(242,243).trim();
			  delchG = strLine.substring(243,249).trim();
			  merc = strLine.substring(249,250).trim();
			  fto = strLine.substring(250,251).trim();
			  mrk1 = strLine.substring(251,252).trim();
			  mrk2 = strLine.substring(252,253).trim();
			  mrk3 = strLine.substring(253,254).trim();
			  mrk4 = strLine.substring(254,255).trim();
			  mrk5 = strLine.substring(255,256).trim();
			  mrk6 = strLine.substring(256,257).trim();
			  mrk7 = strLine.substring(257,258).trim();
			  mrk8 = strLine.substring(258,259).trim();
			  mrk9 = strLine.substring(259,260).trim();
			  amt1 = strLine.substring(260,266).trim();
			  amt2 = strLine.substring(266,272).trim();
			  amt3 = strLine.substring(272,278).trim();
			  amt4 = strLine.substring(278,284).trim();
			  amt5 = strLine.substring(284,290).trim();
			  amt6 = strLine.substring(290,296).trim();
			  amt7 = strLine.substring(296,302).trim();
			  amt8 = strLine.substring(302,308).trim();
			  amt9 = strLine.substring(308,314).trim();
			  hol1 = strLine.substring(314,315).trim();
			  hol2 = strLine.substring(315,316).trim();
			  hol3 = strLine.substring(316,317).trim();
			  hol4 = strLine.substring(317,318).trim();
			  hol5 = strLine.substring(318,319).trim();
			  hol6 = strLine.substring(319,320).trim();
			  hol7 = strLine.substring(320,321).trim();
			  hol8 = strLine.substring(321,322).trim();
			  hol9 = strLine.substring(322,323).trim();
			  zip = strLine.substring(323,329).trim();
			  zipFacility = strLine.substring(329,335).trim();
			  city = strLine.substring(335,365).trim().replaceAll("\"","'");
			  state = strLine.substring(365,367).trim();
			  xMinOrd = strLine.substring(367,373).trim();
			  areaTollFree = strLine.substring(373,376).trim();
			  p800 = strLine.substring(376,383).trim();
			  fax = strLine.substring(383,393).trim();
			  listType = strLine.substring(393,394).trim();
			  number800 = strLine.substring(373,383).trim();
			  address = strLine.substring(394,424).trim().replaceAll("\"","'");
			  commMethod = strLine.substring(424,425).trim();
			  rtiInd = strLine.substring(425,426).trim();
			  cityNote =  strLine.substring(426,456).trim();
			  delvTracking =  strLine.substring(456,457).trim();
			  hour24 =  strLine.substring(457,458).trim();
			  sundayDeliv =  strLine.substring(458,459).trim();
			  productInd =  strLine.substring(459,460).trim();
			  intlPre =  strLine.substring(460,466).trim();
			  adSize =  strLine.substring(466,470).trim();
			  fontType =  strLine.substring(470,471).trim();
			  fontColor =  strLine.substring(471,472).trim();
			  hasDoveText =  strLine.substring(472,473).trim();
			  nameOfGif =  strLine.substring(473,489).trim();
			  
			  if(sundayDeliv.equals("X")) openSunday = "1";
			  else openSunday = "0";
			  
			  /*String insertTempData = "INSERT INTO florist.teleflora_parsed (CSEQ,ZSQE,PACT,COL_GROUP,STATE,CITY,COUNTRY,SHOP_NAME,PH1A,PH_CD1,PHONE2,PH_CD2,MANAGER,CLOSE_PM_1"+
		  		",CLOSE_PM_2,CLOSE_PM_3,CLOSE_PM_4,CLOSE_PM_5,CLOSE_PM_6,CLOSE_PM_7,CLOSE_1,CLOSE_2,CLOSE_3,CLOSE_4,CLOSE_5,CLOSE_6,CLOSE_7,RR_DELCH_IND"+
		  		",RR_DELCHG,DELCHG_IND,DELCHG,MERC,FTO,MRK1,MRK2,MRK3,MRK4,MRK5,MRK6,MRK7,MRK8,MRK9,AMT1,AMT2,AMT3,AMT4,AMT5,AMT6,AMT7,AMT8,AMT9,HOL1,HOL2"+
		  		",HOL3,HOL4,HOL5,HOL6,HOL7,HOL8,HOL9,SHOP_ZIP,ASBZIP_FACILITY,SHOP_CITY,SHOP_STATE,X_MIN_ORD,AREA_TOLLFREE,P800,FAXNBR,LIST_TYPE,ADDRESS,DOVE_IND"+
		  		",RTI_IND,CITY_NOTE,DELV_TRACKING,`24HOUR`,SUNDAY_DELIVERY,PRODUCT_IND,INTERNATIONAL_PREFIX,AD_SIZE,FONT_TYPE,FONT_COLOR,HAS_DOVE_TEXT,NAME_OF_GIF_FILE"+
		  		") VALUES (\""+cseq+"\",\""+zseq+"\",\""+shopCode+"\",\""+colGroup+"\",\""+listingState+"\",\""+listingCity+"\",\""+listingCountry+"\",\""+shopName+"\",\""+phone+"\",\""+phCd1+"\"" +
		  		",\""+phone2+"\",\""+phCd2+"\",\""+shopContact+"\",\""+closePm1+"\",\""+closePm2+"\",\""+closePm3+"\",\""+closePm4+"\",\""+closePm5+"\",\""+closePm6+"\",\""+closePm7+"\",\""+close1+"\",\""+close2+"\",\""+close3+"\",\""+close4+"\",\""+close5+"\",\""+close6+"\",\""+close7+"\",\""+rrDelchInd+"\",\""+rrDelchG+"\"," +
		  		"\""+delchGInd+"\",\""+delchG+"\",\""+merc+"\",\""+fto+"\",\""+mrk1+"\",\""+mrk2+"\",\""+mrk3+"\",\""+mrk4+"\",\""+mrk5+"\",\""+mrk6+"\",\""+mrk7+"\",\""+mrk8+"\",\""+mrk9+"\",\""+amt1+"\",\""+amt2+"\"" +
		  		",\""+amt3+"\",\""+amt4+"\",\""+amt5+"\",\""+amt6+"\",\""+amt7+"\",\""+amt8+"\",\""+amt9+"\",\""+hol1+"\",\""+hol2+"\",\""+hol3+"\",\""+hol4+"\",\""+hol5+"\",\""+hol6+"\",\""+hol7+"\",\""+hol8+"\"" +
		  		",\""+hol9+"\",\""+zip+"\",\""+zipFacility+"\",\""+city+"\",\""+state+"\",\""+xMinOrd+"\",\""+areaTollFree+"\",\""+p800+"\",\""+fax+"\",\""+listType+"\",\""+address+"\",\""+commMethod+"\"" +
		  		",\""+rtiInd+"\",\""+cityNote+"\",\""+delvTracking+"\",\""+hour24+"\",\""+sundayDeliv+"\",\""+productInd+"\",\""+intlPre+"\",\""+adSize+"\",\""+fontType+"\",\""+fontColor+"\",\""+hasDoveText+"\",\""+nameOfGif+"\");";
	  			
	  			mySQL.executeStatement(insertTempData);*/
			  
			  if(cityId.get(city.toUpperCase()+","+state.toUpperCase()) != null){
				  
				  if(cityId.get(listingCity.toUpperCase()+","+listingState.toUpperCase()) != null){
					  
					  if(zipId.get(zip) != null){
						  
						  String existingCityId = cityId.get(city.toUpperCase()+","+state.toUpperCase());
						  String existingZipId = zipId.get(zip);
						  allFileShopCodes.put(shopCode,1);
						  
						  if(shopCoverages.get(shopCode) != null){
							  LinkedList<String> coverages = shopCoverages.get(shopCode);
							  
							  if(allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase()) != null){
								
								  LinkedList<String> zips = allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase());
								 
								  for(int ii=0; ii<zips.size(); ii++){
									  coverages.add(zips.get(ii));
								  }
								  shopCoverages.put(shopCode, coverages);
							  }
						  }else{
							  LinkedList<String> coverages = new LinkedList<String>();
							  
							  if(allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase()) != null){
									
								  LinkedList<String> zips = allCityZips.get(listingCity.toUpperCase()+","+listingState.toUpperCase());
								 
								  for(int ii=0; ii<zips.size(); ii++){
									  coverages.add(zips.get(ii));
								  }
								  shopCoverages.put(shopCode, coverages);
							  }
						  }
						  
						  String existingShopId = "";
						  
						  if(!phone.equals("") && shopsByPhone.get(phone)!= null) existingShopId = shopsByPhone.get(phone);
						  
						  else if(!address.equals("") && shopsByAddress.get(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase()) != null)
							  existingShopId = shopsByAddress.get(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase());
						  
						  else if(!phone.equals("") && FLD.get(phone) != null){
							  
							  String existingShopCode = FLD.get(phone);
							  
							  if(existingShopCodes.get(existingShopCode) != null){
								  
								  existingShopId = existingShopCodes.get(existingShopCode);
								  System.out.println("Got one!! "+shopCode+ " : "+existingShopCode);
							  }
							  
							  else errorOut.write("TF Shop Code: "+shopCode+" matched shop code: "+existingShopCode+" in FLD, but we do not have that BMT shop code in our database\n");
						  }
						  
						  if(existingShopCodes.get(shopCode) != null){
							  
							  try{
								  existingShopId = existingShopCodes.get(shopCode);
								  
								  if(shopNetwork.get(existingShopId+","+"1") != null){ 
									  updateShop(shopName, address, phone, shopContact, fax, number800, existingZipId, existingCityId, existingShopId);
								  }
								  
								  else if(uniqueShops.get(shopCode) == null) 
									  updateShop(shopName, address, phone, shopContact, fax, number800, existingZipId, existingCityId, existingShopId);
								  
								  String networkId = shopNetwork.get(existingShopId+","+"2");
								  
								  if(uniqueShops.get(shopCode) == null) 
									  updateNetwork(commMethod, openSunday, networkId);
								  
								  uploadCoverage(listingCity, listingState, networkId);
							  }catch(Exception ee){
								  
							  }
							  
						  }else if(!existingShopId.equals("") && shopNetwork.get(existingShopId+","+"2") != null){
							  
							  if(uniqueShops.get(shopCode) == null){
								  out.write("There is a new shop code for shop ID: "+existingShopId+": "+shopCode+"\n");
							  	  shopCodesOut.write(existingShopId+","+shopCode+"\n");
							  }
						  }else if(!existingShopId.equals("")){
							  
							  if(shopNetwork.get(existingShopId+","+"1") != null) updateShop(shopName, address, phone, shopContact, fax, number800, existingZipId, existingCityId, existingShopId);
							  
							  else updateShop(shopName, address, phone, shopContact, fax, number800, existingZipId, existingCityId, existingShopId);
							  
							  String networkId = uploadNetwork(existingShopId, shopCode, commMethod, openSunday);
							  
							  uploadCoverage(listingCity, listingState, networkId);
							  
						  }else{
								  
							  String shopId = uploadShop(shopName, address, phone, shopContact, fax, number800, 
									  existingZipId, existingCityId, city, state);
							  
							  String networkId = uploadNetwork(shopId, shopCode, commMethod, openSunday);
							  
							  uploadCoverage(listingCity, listingState, networkId);
							  
						  }
				  	}else{
				  		if(!uniqueBadData.contains(zip)){
							  out.write("No zip was found for: "+zip+" for shop: "+shopCode+"\n");
							  uniqueBadData.add(zip);
						  }
				  	}
				 }else{
					 //Do Nothing
				  } 
			  }else{
				  if(uniqueShops.get(shopCode) == null){
					  reUploadOut.write(strLine+"\n");
				  }
				  if(!uniqueBadData.contains(city+","+state)){
					  out.write("No city was found for: "+city+", "+state+" for shop: "+shopCode+"\n");
					  uniqueBadData.add(city+","+state);
				  }
			  }  
			  uniqueShops.put(shopCode, 1);
		  }
		  
		  in.close();
		  out.close();
		  errorOut.close();
		  shopCodesOut.close();
		  reUploadOut.close();
		  mySQL.closeFile();
		  
		  
		}catch(Exception ee){
			ee.printStackTrace();
			
			try {
				errorOut.write(ee.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		try {
			setShopsToUpdate("2");
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void updateAvailability(){
		
		System.out.println("Updating shop active/inactive statuses");
		
		for(int ii=0; ii<shopsToDeactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
							"SET ShopNetworkStatus_ID = \"2\" " +
							"WHERE ShopCode = \""+shopsToDeactivate.get(ii)+"\"";
			mySQL.executeStatement(statement);
		}
		
		for(int ii=0; ii<shopsToReactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
						   "SET ShopNetworkStatus_ID = \"1\" " +
						   "WHERE ShopCode = \""+shopsToReactivate.get(ii)+"\"";
			
			mySQL.executeStatement(statement);
		}
		
		System.out.println("Finished updating shop active/inactive statuses");
	}
	
}
