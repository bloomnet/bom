package com.bloomnet.bom.upload.tools;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.LinkedList;

import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

public class FTDDataUpload extends BaseUpload{
	
	@SuppressWarnings("unused")
	private String fileName;
	@SuppressWarnings("unused")
	private File file;
	
	private int count = 0;
	
	public FTDDataUpload(File file, String databaseName) {
		super("logs/FTDBadData.txt","logs/FTDError.txt","logs/FTDNewShopCodes.txt","logs/FTDReUpload.txt",databaseName);
		this.file = file;
		setExistingShopCodes("3");
		try {
			parseFTD();
		} catch (BiffException e2) {
			e2.printStackTrace();
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		try {
			updateAvailability();
			updateCoverage();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public FTDDataUpload(String fileName, String databaseName){
		super("logs/FTDBadData.txt","logs/FTDError.txt","logs/FTDNewShopCodes.txt","logs/FTDReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("3");
		//useTheseShops();
		try {
			parseFTD();
		} catch (BiffException e1) {
			e1.printStackTrace();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		try {
			updateCoverage();
			updateAvailability();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public FTDDataUpload(String fileName, String dummy, String databaseName){
		super("logs/FTDBadData.txt","logs/FTDError.txt","logs/FTDNewShopCodes.txt","logs/FTDReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("3");
		try {
			parseFTD();
		} catch (BiffException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private String uploadShop(String shopName, String address, String phone, String shopContact,
			String existingZipId, String existingCityId, String city, String state){
		
		String statement = "INSERT INTO "+databaseName+".shop " +
								"(ShopName," +
								"ShopAddress1," +
								"ShopPhone," +
								"ShopContact," +
								"Zip_ID," +
								"City_ID,"+
								"CreatedDate," +
								"CreatedUser_ID," +
								"ModifiedDate," +
								"ModifiedUser_ID) " +
								"VALUES (\""+shopName+"\"," +
								"\""+address+"\"," +
								"\""+phone+"\"," +
								"\""+shopContact+"\"," +
								""+Integer.valueOf(existingZipId)+"," +
								""+Integer.valueOf(existingCityId)+"," +
								"now()," +
								"1," +
								"now()," +
								"1);";

		mySQL.executeStatement(statement);
		String shopId = getId();
		
		shopsByPhone.put(phone,shopId);
		shopsByAddress.put(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase(), shopId);
		
		return shopId;
	}
	
	private void updateShop(String shopName, String address, String phone, String shopContact,
			String existingZipId, String existingCityId, String existingShopId){
		
		String statement = "UPDATE "+databaseName+".shop SET " +
							  "ShopName = \""+shopName+"\"" +
							  ",ShopAddress1 = \""+address+"\"" +
							  ",ShopPhone = \""+phone+"\"" +
							  ",ShopContact = \""+shopContact+"\"" +
							  ",Zip_ID = "+Integer.valueOf(existingZipId)+"" +
							  ",City_ID = "+Integer.valueOf(existingCityId)+"" +
							  ",ModifiedDate = now()" +
							  ",ModifiedUser_ID = 1 " +
							  "WHERE Shop_ID = "+Integer.valueOf(existingShopId)+";";

		mySQL.executeStatement(statement);
	}
	
	private String uploadNetwork(String shopId, String shopCode, String openSunday){
		
		String statement = "INSERT INTO "+databaseName+".shopnetwork " +
						  		"(Shop_ID," +
						  		"Network_ID," +
						  		"ShopCode," +
						  		"ShopNetworkStatus_ID," +
						  		"CommMethod_ID," +
						  		"OpenSunday," +
						  		"CreatedDate," +
						  		"Created_User_ID," +
						  		"ModifiedDate," +
						  		"ModifiedUser_ID) " +
						  		"VALUES ("+
						  		""+Integer.valueOf(shopId)+"," +
						  		"3,\""+
						  		shopCode+"\"," +
						  		"1," +
						  		"2,"+
								Byte.valueOf(openSunday)+"," +
								"now()," +
								"1," +
								"now()," +
								"1);";
	 
	  mySQL.executeStatement(statement);
	 
	  String networkId = getId();
	 
	  shopNetwork.put(shopId+","+"3", networkId);
	  
	  existingShopCodes.put(shopCode, shopId);
	  
	  return networkId;
	}
	
	private void updateNetwork(String openSunday, String networkId){
		
		String statement = "UPDATE "+databaseName+".shopnetwork SET " +
							"CommMethod_ID = 2," +
							"OpenSunday = "+Byte.valueOf(openSunday)+"," +
							"ModifiedDate = now()," +
							"ModifiedUser_ID = 1 ";

	  
	  statement += "WHERE ShopNetwork_ID = "+Integer.valueOf(networkId)+";";
	  
	  mySQL.executeStatement(statement);
	}
	
	@SuppressWarnings("unused")
	private void uploadCoverage(String listingZip, String networkId){
		
		  String zip_id = zipId.get(listingZip);	
		  
		  if(zip_id != null){
				  
			  if(shopNetworkCoverage.get(networkId+","+zip_id) == null){
					  
				  String statement = "INSERT INTO "+databaseName+".shopnetworkcoverage " +
				  		"(ShopNetwork_ID," +
				  		"Zip_ID," +
				  		"CreatedDate," +
				  		"CreatedUser_ID," +
				  		"ModifiedDate," +
				  		"ModifiedUser_ID) " +
				  		"VALUES (" +
		  				""+Integer.valueOf(networkId)+","+
		  				zip_id+"" +
		  				",now()," +
		  				"1," +
		  				"now()," +
		  				"1);";
				  
				  mySQL.executeStatement(statement);
				  
				  shopNetworkCoverage.put(networkId+","+zip_id,1);
			  }
		  }
	}
	
	@SuppressWarnings("unused")
	private void parseFTD() throws IOException, BiffException{
		
		String shopCode,shopName,phone,address,zip,city,state,listingZip;
		
		File inputWorkbook = new File("data/ftd.xls");
		Workbook w;
		w = Workbook.getWorkbook(inputWorkbook);
		Sheet sheet = w.getSheet(0);
		
		for (int ii = 0; ii < sheet.getRows(); ++ii){
			  
			  ++count;
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);
			  } 
			  
			  shopCode = (sheet.getCell(0, ii).getContents().toString());
			  shopName = (sheet.getCell(1, ii).getContents().toString()).replaceAll("\\'", "''");
			  zip = sheet.getCell(5, ii).getContents().toString();
			  if(zip.length() == 3) zip = "00" + zip;
			  else if(zip.length() == 4) zip = "0" + zip;
			  phone = (sheet.getCell(6, ii).getContents().toString()).replaceAll("-","").replaceAll(" ","").replaceAll("\\(","").replaceAll("\\)","");
			  address = sheet.getCell(2, ii).getContents().toString();
			  city = sheet.getCell(3, ii).getContents().toString();
			  state = sheet.getCell(4, ii).getContents().toString();
		  	 
			  listingZip = "";
			  
			  if(!shopCode.equals("90-0266")){
				  
				  if(cityId.get(city.toUpperCase()+","+state.toUpperCase()) != null){
					  					  
					  if(zipId.get(zip) != null){
						  
						  String existingCityId = cityId.get(city.toUpperCase()+","+state.toUpperCase());
						  String existingZipId = zipId.get(zip);
						  allFileShopCodes.put(shopCode,1);
						  
						  if(shopCoverages.get(shopCode) != null){
							  
							  LinkedList<String> coverages = shopCoverages.get(shopCode);
							  coverages.add(listingZip);
							  shopCoverages.put(shopCode, coverages);
							  
						  }else{
							  
							  LinkedList<String> coverages = new LinkedList<String>();
							  coverages.add(listingZip);
							  shopCoverages.put(shopCode, coverages);
						  }
						  
						  String existingShopId = "";
						  
						  if(!phone.equals("") && shopsByPhone.get(phone)!= null) existingShopId = shopsByPhone.get(phone);
						  
						  else if(!address.equals("") && shopsByAddress.get(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase()) != null)
							  existingShopId = shopsByAddress.get(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase());
						  
						  else if(!phone.equals("") && FLD.get(phone) != null){
							  
							  String existingShopCode = FLD.get(phone);
							  
							  if(existingShopCodes.get(existingShopCode) != null){
								  
								  existingShopId = existingShopCodes.get(existingShopCode);
								  System.out.println("Got one!! "+shopCode+ " : "+existingShopCode);
							  }
							  
							  else errorOut.write("FTD Shop Code: "+shopCode+" matched shop code: "+existingShopCode+" in FLD, but we do not have that BMT shop code in our database\n");
						  }
						  
						  if(existingShopCodes.get(shopCode) != null){
							  
							  existingShopId = existingShopCodes.get(shopCode);
							  
							  if((shopNetwork.get(existingShopId+","+"1") != null) || (shopNetwork.get(existingShopId+","+"2") != null)){ /*do nothing*/ }
							  
							  else if(uniqueShops.get(shopCode) == null) 
								  updateShop(shopName, address, phone, "", existingZipId, existingCityId, existingShopId);
							  
							  String networkId = shopNetwork.get(existingShopId+","+"3");
							  
							  if(uniqueShops.get(shopCode) == null) 
								  updateNetwork("0", networkId);
							  
							  //uploadCoverage(listingZip, networkId);
							  
						  }else if(!existingShopId.equals("") && shopNetwork.get(existingShopId+","+"3") != null){
							  
							  if(uniqueShops.get(shopCode) == null){
								  out.write("There is a new shop code for shop ID: "+existingShopId+": "+shopCode+"\n");
							  	  shopCodesOut.write(existingShopId+","+shopCode+"\n");
							  }
						  }else if(!existingShopId.equals("")){
							  
							  if((shopNetwork.get(existingShopId+","+"1") != null) || (shopNetwork.get(existingShopId+","+"2") != null)){ /*do nothing*/ }
							  
							  else updateShop(shopName, address, phone, "", existingZipId, existingCityId, existingShopId);
							  
							  String networkId = uploadNetwork(existingShopId, shopCode, "0");
							  
							  //uploadCoverage(listingZip, networkId);
							  
						  }else{
								  
							  String shopId = uploadShop(shopName, address, phone, "", 
									  existingZipId, existingCityId, city, state);
							  
							  String networkId = uploadNetwork(shopId, shopCode, "0");
							  
							  //uploadCoverage(listingZip, networkId);
							  
						  }
				  	}else{
				  		if(!uniqueBadData.contains(zip)){
							  out.write("No zip was found for: "+zip+" for shop: "+shopCode+"\n");
							  uniqueBadData.add(zip);
						  }
				  	}
					 }else{
						 if(uniqueShops.get(shopCode) == null){
							  //reUploadOut.write(strLine+"\n");
						  }
						  if(!uniqueBadData.contains(city+","+state)){
							  out.write("No city was found for: "+city+", "+state+" for shop: "+shopCode+"\n");
							  uniqueBadData.add(city+","+state);
						  }
					 }  
			  }
			  uniqueShops.put(shopCode, 1);
		  }
	}
	
	private void updateAvailability(){
		
		System.out.println("Updating shop active/inactive statuses");
		
		for(int ii=0; ii<shopsToDeactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
							"SET ShopNetworkStatus_ID = \"2\" " +
							"WHERE ShopCode = \""+shopsToDeactivate.get(ii)+"\"";
			mySQL.executeStatement(statement);
		}
		
		for(int ii=0; ii<shopsToReactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
						   "SET ShopNetworkStatus_ID = \"1\" " +
						   "WHERE ShopCode = \""+shopsToReactivate.get(ii)+"\"";
			
			mySQL.executeStatement(statement);
		}
		
		System.out.println("Finished updating shop active/inactive statuses");
	}
	
}
