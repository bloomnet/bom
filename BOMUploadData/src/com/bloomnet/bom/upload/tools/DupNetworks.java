package com.bloomnet.bom.upload.tools;

import java.sql.ResultSet;
import java.sql.SQLException;

public class DupNetworks {
	private SQLData mySQL;
	
	public DupNetworks() throws SQLException{
		
		mySQL = new SQLData();
		mySQL.login();
		String useStatement = "USE bloomnetordermanagement";
		mySQL.executeStatement(useStatement);
		String query = "SELECT ShopCode FROM shopnetwork WHERE Network_ID IN (1,2) GROUP BY ShopCode HAVING COUNT(ShopCode) > 1;";
		ResultSet results = mySQL.executeQuery(query);
		int ii = 0;
		while(results.next()){
			ii++;
			String shopCode = results.getString(1);
			query = "SELECT ShopNetwork_ID FROM shopnetwork WHERE ShopCode = \""+shopCode+"\";";
			ResultSet results2 = mySQL.executeQuery(query);
			String initialId = "";
			results2.next();
			initialId = results2.getString(1);
			results2.next();
			String otherId = results2.getString(1);
			
			String statement = "UPDATE shopnetworkcoverage SET ShopNetwork_ID = "+otherId+" WHERE ShopNetwork_ID = "+initialId+";";
			mySQL.executeStatement(statement);
			statement = "DELETE FROM shopnetwork WHERE ShopNetwork_ID = "+initialId+";";
			mySQL.executeStatement(statement);

			mySQL.closeStatement();
		}
		mySQL.closeStatement();
		System.out.println(ii);
	}
	
	public static void main(String[] args){
		try {
			new DupNetworks();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
	}
}
