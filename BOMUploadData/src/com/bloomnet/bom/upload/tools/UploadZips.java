package com.bloomnet.bom.upload.tools;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;


public class UploadZips {
	
	HashMap<String,String> existingStates = new HashMap<String,String>();
	HashMap<String,String> existingCities = new HashMap<String,String>();
	HashMap<String, String> existingZips = new HashMap<String,String>();
	HashMap<String, String> existingCityZips = new HashMap<String,String>();
	
	Map<String,String> fileStates = new HashMap<String,String>();
	Map<String,String> fileCities = new HashMap<String,String>();
	Map<String,String> fileZips = new HashMap<String,String>();
	Map<String,String> fileCityZips = new HashMap<String,String>();
	
	Map<String,Integer> shortZipsSeen = new HashMap<String,Integer>();
	
	private FileOutputStream fos;
	private OutputStreamWriter out;
	
	private FileOutputStream errorFos;
	private OutputStreamWriter errorOut;
	private String databaseName;
	
	SQLData mySQL;
	
	public UploadZips(String databaseName){
		
		this.databaseName = databaseName;
		try {
			errorFos = new FileOutputStream("data/zipErrors.txt");
			errorOut = new OutputStreamWriter(errorFos);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}
		
		mySQL = new SQLData();
		mySQL.login();
		try {
			setExistingStates();
			setExistingCities();
			setExistingZips();
			setExistingCityZips();
			uploadZips("data/usa.csv", false);
			uploadZips("data/canada.csv", true);
			outputExistingToRemove();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void setExistingStates() throws SQLException{
		
		System.out.println("Setting State IDs");
		
		String query = "SELECT state.State_ID,state.Short_Name FROM "+databaseName+".state;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingStates.put(results.getString("Short_Name"), results.getString("State_ID"));
		}
		results.close();
		mySQL.closeStatement();
		
		System.out.println("Finished Setting State IDs");
	}
	
	private void setExistingCities() throws SQLException{
		
		System.out.println("Setting City IDs");
		
		String query = "SELECT city.City_ID,city.Name,state.Short_Name FROM "+databaseName+".city " +
					   "INNER JOIN "+databaseName+".state ON city.State_ID = state.State_ID;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			try{
				existingCities.put(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase(), results.getString("City_ID"));
			}catch(Exception ee){
				System.err.println(results.getString("Name"));
				try {
					errorOut.write(ee.getMessage()+"\n");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
		results.close();
		mySQL.closeStatement();
		
		System.out.println("Finished Setting City IDs");
	}
	
	private void setExistingZips() throws SQLException{
		
		System.out.println("Setting Zip IDs");
		
		String query = "SELECT zip.Zip_ID,zip.Zip_Code FROM "+databaseName+".zip;";

		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingZips.put(results.getString("Zip_Code"), results.getString("Zip_ID"));
		}
		results.close();
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Zip IDs");
	}
	
	private void setExistingCityZips() throws SQLException{
		
		System.out.println("Setting City to Zip IDs");
		
		String query = "SELECT zip.Zip_Code, city.Name, state.Short_Name FROM "+databaseName+".city_zip_xref " +
					   "INNER JOIN "+databaseName+".city ON city_zip_xref.City_ID = city.City_ID " +
					   "INNER JOIN "+databaseName+".zip ON city_zip_xref.Zip_ID = zip.Zip_ID " +
					   "INNER JOIN "+databaseName+".state ON city.State_ID = state.State_ID;";

		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingCityZips.put(results.getString("Name").toUpperCase()+","+results.getString("Short_Name").toUpperCase()+","+results.getString("Zip_Code"), "1");
		}
		results.close();
		mySQL.closeStatement();
		
		System.out.println("Finished Setting City to Zip IDs");
	}

	private String getId() throws SQLException{
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		results.next();
		String id = results.getString("LAST_INSERT_ID()");
		results.close();
		mySQL.closeStatement();
		return id;
	}
	
	private void outputExistingToRemove(){
		
		System.out.println("Updating City to Zip References");
		
		try {
			fos = new FileOutputStream("data/existingToRemove"+new Date().getTime()+".txt");
			out = new OutputStreamWriter(fos);
			
			Set<String> keys = existingStates.keySet();
			Iterator<String> itr = keys.iterator();
			String key;
			
			while(itr.hasNext()){
				key = itr.next().toString();
				if(fileStates.get(key) == null){
					out.write("State: " + key + "\n");
				}
			}
			keys = existingCities.keySet();
			itr = keys.iterator();
			
			while(itr.hasNext()){
				key = itr.next().toString();
				if(fileCities.get(key) == null){
					out.write("City: " + key + "\n");
				}
			}
			
			keys = existingZips.keySet();
			itr = keys.iterator();
			while(itr.hasNext()){
				key = itr.next().toString();
				if(fileZips.get(key) == null){
					out.write("Zip: " + key + "\n");
				}
			}

			keys = existingCityZips.keySet();
			itr = keys.iterator();
			while(itr.hasNext()){
				key = itr.next().toString();
				if(fileCityZips.get(key) == null){
					String[] keySplit = key.split(","); 
					String cityId = existingCities.get(keySplit[0]+","+keySplit[1]);
					String zipId = existingZips.get(keySplit[2]);
					String statement = "DELETE FROM "+databaseName+".city_zip_xref WHERE Zip_ID = "+zipId+" AND City_ID = "+cityId+";";
					mySQL.executeStatement(statement);
					System.out.println("Removed city_zip_xref: "+cityId+","+zipId);
					out.write("Removed city_zip_xref: "+cityId+","+zipId);
				}
			}

			out.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Finished Updating City to Zip References");
		System.out.println("All done.");
	}
	
	private void updateZip(String zipCode, String lat, String longi, String timeZone, String areaCode, String existingZipId){
		String statement = "UPDATE "+databaseName+".zip SET " +
		   "zip.Zip_Code = '"+zipCode+"'" +
		   ",zip.Latitude = "+Double.valueOf(lat)+"" +
		   ",zip.Longitude = "+Double.valueOf(longi)+"" +
		   ",zip.TimeZone = '"+Double.valueOf(timeZone)+"'" +
		   ",zip.AreaCodes = '"+areaCode+"'" +
		   ",zip.ModifiedDate = now()" +
		   ",zip.ModifiedUser_ID = 1 " +
		   "WHERE zip.Zip_ID = "+Long.valueOf(existingZipId)+"";

		mySQL.executeStatement(statement);
	}
	
	private void uploadZip(String zipCode, String lat, String longi, String timeZone, String areaCode){
		 String statement = "INSERT INTO "+databaseName+".zip ( " +
		  "Zip_Code" +
		  ",Latitude" +
		  ",Longitude" +
		  ",TimeZone" +
		  ",AreaCodes" +
		  ",CreatedDate" +
		  ",CreatedUser_ID" +
		  ",ModifiedDate" +
		  ",ModifiedUser_ID" +
		  ") VALUES (" +
		  "'"+zipCode+"'" +
		  ","+Double.valueOf(lat)+"" +
		  ","+Double.valueOf(longi)+"" +
		  ",'"+Double.valueOf(timeZone)+"'" +
		  ",'"+areaCode+"'" +
		  ",now()" +
		  ",1" +
		  ",now()" +
		  ",1" +
		  ");";

		 mySQL.executeStatement(statement);
	}
	
	private void updateState(String existingStateId){
		String statement = "UPDATE "+databaseName+".state SET " +
		 "state.ModifiedDate = now() " +
		 ",state.ModifiedUser_ID = '4' " +
		 "WHERE state.State_ID = "+Long.valueOf(existingStateId)+";";

		mySQL.executeStatement(statement);
	}
	
	private void uploadState(String stateName, int country, String stateShortName){
		String statement = "INSERT INTO "+databaseName+".state ( " +
	       "Name" +
		  ",Country_ID" +
		  ",Short_Name" +
		  ",CreatedDate" +
		  ",CreatedUser_ID" +
		  ",ModifiedDate" +
		  ",ModifiedUser_ID" +
		") VALUES (" +
		   "\""+stateName.toUpperCase()+"\"" +
		  ","+country+"" +
		  ",\""+stateShortName.toUpperCase()+"\"" +
		  ",now()" +
		  ",1" +
		  ",now()" +
		  ",1" +
		  ");";

		mySQL.executeStatement(statement);
	}
	
	private void updateCity(String cityType, String existingCityId){
		String statement = "UPDATE "+databaseName+".city SET " +
		 "city.City_Type = '"+cityType+"' " +
		 ",city.ModifiedDate = now() " +
		 ",city.ModifiedUser_ID = '1' " +
		 "WHERE city.City_ID = "+Long.valueOf(existingCityId)+";";

		mySQL.executeStatement(statement);
	}
	
	private void uploadCity(String cityName, String existingStateId, String cityType){
		String statement = "INSERT INTO "+databaseName+".city (" +
		   "Name" +
		  ",State_ID" +
		  ",Major_City" +
		  ",City_Type" +
		  ",CreatedDate" +
		  ",CreatedUser_ID" +
		  ",ModifiedDate" +
		  ",ModifiedUser_ID" +
		") VALUES (" +
		   "\""+cityName.toUpperCase()+"\"" +
		  ","+Long.valueOf(existingStateId)+"" +
		  ",0" +
		  ",\""+cityType.toUpperCase()+"\"" +
		  ",now()" +
		  ",1" +
		  ",now()" +
		  ",1" +
		")";

		mySQL.executeStatement(statement);
	}
	
	private void updateCityZipXref(String existingCityId, String existingZipId, String existingCityZipId){
		String statement = "UPDATE "+databaseName+".city_zip_xref SET " +
		 "City_ID = "+existingCityId+"" +
		 ",Zip_ID = "+existingZipId+"" +
		 ",CreatedDate = now() " +
		 ",CreatedUser_ID = '1' " +
		 ",ModifiedDate = now() " +
		 ",ModifiedUser_ID = '1' " +
		 "WHERE city_zip_xref_id = "+Long.valueOf(existingCityZipId)+";";

		mySQL.executeStatement(statement);
	}
	
	private void uploadCityZipXref(String existingCityId, String existingZipId){
		String statement = "INSERT INTO "+databaseName+".city_zip_xref ( " +
		  "City_ID" +
		  ",Zip_ID" +
		  ",CreatedDate" +
		  ",CreatedUser_ID" +
		  ",ModifiedDate" +
		  ",ModifiedUser_ID" +
		  ") VALUES (" +
		  ""+existingCityId+"" +
		  ","+existingZipId+"" +
		  ",now()" +
		  ",1" +
		  ",now()" +
		  ",1" +
		  ");";

		  mySQL.executeStatement(statement);
	}
	
	private void uploadZip(String zipCode, String lat, String longi, String timeZone, String areaCode, String existingCityId, 
			String cityName, String stateShortName, String existingCityZipId, boolean canadaCheck) throws Exception{
		
		String existingZipId = "";
		
		if(existingZips.get(zipCode) != null){
			
			  existingZipId = existingZips.get(zipCode);
			  updateZip(zipCode, lat, longi, timeZone, areaCode, existingZipId);
			  String shortZip = zipCode.substring(0,3);
			  
			  if(canadaCheck && shortZipsSeen.get(shortZip) == null && existingZips.get(shortZip) != null){
				  
				  fileCityZips.put(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+shortZip,"1");
			   
				  existingZipId = existingZips.get(shortZip);
				  updateZip(shortZip, lat, longi, timeZone, areaCode, existingZipId);
				  
				  if(existingCityZips.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+shortZip) != null){
					  existingCityZipId = existingCityZips.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+shortZip);
					  updateCityZipXref(existingCityId, existingZipId, existingCityZipId);
				  }else{
					  uploadCityZipXref(existingCityId, existingZipId);
					  existingCityZipId = getId();
					  existingCityZips.put(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+shortZip, existingCityZipId);
				  }
				  shortZipsSeen.put(shortZip,1);
			  }
		}else{
			  uploadZip(zipCode, lat, longi, timeZone, areaCode);
			  existingZipId = getId();
			  existingZips.put(zipCode,existingZipId);
			  String shortZip = zipCode.substring(0,3);
			  
			  if(canadaCheck && existingZips.get(shortZip) == null){
				  
				  uploadZip(shortZip, lat, longi, timeZone, areaCode);
				  existingZipId = getId();
				  existingZips.put(shortZip,existingZipId);
				  
				  if(existingCityZips.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode) != null){
					  existingCityZipId = existingCityZips.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode);
					  updateCityZipXref(existingCityId, existingZipId, existingCityZipId);
				  }else{
					  uploadCityZipXref(existingCityId, existingZipId);
					  existingCityZipId = getId();
					  existingCityZips.put(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode, existingCityZipId);
				  }
				  shortZipsSeen.put(shortZip,1);
			  }
		}
	}
	
	private String convertTimeZoneToNumerical(String value){
		
		if(value.toLowerCase().equals("samoa")) return "-11";
		else if(value.toLowerCase().equals("hawaii")) return "-10";
		else if(value.toLowerCase().equals("alaska")) return "-9";
		else if(value.toLowerCase().equals("pacific")) return "-8";
		else if(value.toLowerCase().equals("mountain")) return "-7";
		else if(value.toLowerCase().equals("central")) return "-6";
		else if(value.toLowerCase().equals("eastern")) return "-5";
		else if(value.toLowerCase().equals("atlantic")) return "-4";
		else if(value.toLowerCase().equals("newfoundland")) return "-3.5";
		else if(value.toLowerCase().equals("utc+9")) return "9";
		else if(value.toLowerCase().equals("utc+10")) return "10";
		else if(value.toLowerCase().equals("utc+11")) return "11";
		else if(value.toLowerCase().equals("utc+12")) return "12";
		
		return "";
	}
	
	private void uploadZips(String fileName, boolean canadaCheck){
		
		System.out.println("Updating City/State/Zip data");

		try {
			
		  BigFile file = new BigFile(fileName);
		  
		  int ii=0;
		  
		  String existingStateId = "";
		  int country=0;
		  String existingCityId="";
		  String existingZipId="";
		  String existingCityZipId="";
		  
		  for (String strLine : file){
			  if(ii!=0){
				  String[] zipArray = strLine.split(",");
				  String zipCode = "";
				  String cityName = "";
				  String cityType = "";
				  String stateName = "";
				  String stateShortName = "";
				  String lat = "";
				  String longi = "";
				  String timeZone = "";
				  String areaCode = "";
				  if(canadaCheck){
					  zipCode = zipArray[0].replaceAll(" ", "");
					  cityName = zipArray[1];
					  cityType = zipArray[2].replaceAll(" ", "");
					  stateName = zipArray[3];
					  stateShortName = zipArray[4].replaceAll(" ", "");
					  lat = zipArray[8].replaceAll(" ", "");
					  if (lat.equals("")) lat = "0";
					  longi = zipArray[9].replaceAll(" ", "");
					  if (longi.equals("")) longi = "0";
					  areaCode = zipArray[5].replaceAll(" ", "");
					  timeZone = convertTimeZoneToNumerical(zipArray[6].replaceAll(" ", ""));
					  if (timeZone.equals("")) timeZone = "0";
				  }else{
					  zipCode = zipArray[0].replaceAll(" ", "");
					  cityName = zipArray[2];
					  cityType = zipArray[3].replaceAll(" ", "");
					  stateName = zipArray[6];
					  stateShortName = zipArray[7].replaceAll(" ", "");
					  lat = zipArray[14].replaceAll(" ", "");
					  if (lat.equals("")) lat = "0";
					  longi = zipArray[15].replaceAll(" ", "");
					  if (longi.equals("")) longi = "0";
					  areaCode = zipArray[10].replaceAll(" ", "");
					  timeZone = convertTimeZoneToNumerical(zipArray[11].replaceAll(" ", ""));
					  if (timeZone.equals("")) timeZone = "0";
				  }
				  if(cityType != null && cityType.equalsIgnoreCase("D")){
					  fileStates.put(stateShortName.toUpperCase(),"1");
					  fileCities.put(cityName.toUpperCase()+","+stateShortName.toUpperCase(),"1");
					  fileZips.put(zipCode,"1");
					  fileCityZips.put(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode,"1");
					  
					  if(existingStates.get(stateShortName) != null){
						  existingStateId = existingStates.get(stateShortName);
						  updateState(existingStateId);
						  
						  if(existingCities.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()) != null){
							  existingCityId = existingCities.get(cityName.toUpperCase()+","+stateShortName.toUpperCase());
							  updateCity(cityType, existingCityId);
							  uploadZip(zipCode, lat, longi, timeZone, areaCode, existingCityId, cityName, stateShortName, existingCityZipId, canadaCheck);
						  }else{
							  uploadCity(cityName, existingStateId, cityType);
							  existingCityId = getId();
							  existingCities.put(cityName.toUpperCase()+","+stateShortName.toUpperCase(),existingCityId);
							  uploadZip(zipCode, lat, longi, timeZone, areaCode, existingCityId, cityName, stateShortName, existingCityZipId, canadaCheck);
						  }
					  }else{
						  if(stateShortName.equals("PR")) country = 3;
						  else if(canadaCheck) country = 2;
						  else country =1;
						  
						  uploadState(stateName, country, stateShortName);
						  existingStateId = getId();
						  existingStates.put(stateShortName, existingStateId);
			
						  uploadCity(cityName, existingStateId, cityType);
						  existingCityId = getId();
						  existingCities.put(cityName.toUpperCase()+","+stateShortName,existingCityId);
						  uploadZip(zipCode, lat, longi, timeZone, areaCode, existingCityId, cityName, stateShortName, existingCityZipId, canadaCheck);
					  }
					  existingZipId = existingZips.get(zipCode);
					  if(existingCityZips.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode) != null){
						  existingCityZipId = existingCityZips.get(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode);
						  updateCityZipXref(existingCityId, existingZipId, existingCityZipId);
					  }else{
						  uploadCityZipXref(existingCityId, existingZipId);
						  existingCityZipId = getId();
						  existingCityZips.put(cityName.toUpperCase()+","+stateShortName.toUpperCase()+","+zipCode, existingCityZipId);
					  }
				  }
			  }
			  ++ii;
			  if(ii%1000 == 0) System.out.println("done with " + ii + " rows.");
		  }
		  System.out.println("all done.");
		  file.Close();
		}catch(Exception ee){
			ee.printStackTrace();
			try {
				errorOut.write(ee.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		System.out.println("Finished Updating City/State/Zip data");
	}
	
}
