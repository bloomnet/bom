package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import jxl.Sheet;
import jxl.Workbook;

public class UploadParticipationIndex {
	
	private SQLData mySQL;
	private String databaseName;
	private String fileName;
	private HashMap<String,String> existingShopCodes = new HashMap<String,String>();
	private HashMap<String,Boolean> bloomnetOnlyShops = new HashMap<String,Boolean>();
	private HashMap<String,Boolean> bmsShops = new HashMap<String,Boolean>();
	private HashMap<String,String> shopInfo = new HashMap<String,String>();
	private HashMap<String,Boolean> pastDueShops = new HashMap<String,Boolean>();
	private HashMap <String,String> napcoPurchasing = new HashMap<String,String>();
	
	
	public UploadParticipationIndex(String fileName, String databaseName){

		this.databaseName = databaseName;
		this.fileName = fileName;
		mySQL = new SQLData();
		mySQL.login();
		
		setShopReferences();
		uploadFLDData();
		uploadCRMData();
		uploadPastDueShops();
		uploadNapcoPurchasing();
		uploadParticipationIndex();
	}
	
	private void uploadNapcoPurchasing() {
		try {
			System.out.println("Setting Napco Data");
			Sheet sheet = openExcelSheet("data/napco.xls");
			
			for (int ii = 0; ii < sheet.getRows(); ++ii){
				
				String shopCode = sheet.getCell(2,ii).getContents().toString();
				String amount = sheet.getCell(3,ii).getContents().toString();
				
				if(!amount.equals(""))
					napcoPurchasing.put(shopCode, amount);

			}
			System.out.println("Finished setting Napco Data");
		}catch(Exception ee){ee.printStackTrace();}
		
	}

	private void setShopReferences(){
		System.out.println("Setting all existing shop codes to ids");
		
		String query = "SELECT shop.ShopCode,shop.id " +
				"FROM "+databaseName+".shop;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		try {
			while(results.next()){
				existingShopCodes.put(results.getString("ShopCode"),results.getString("id"));
			}
			results.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting all existing shop codes to ids");
	}
	
	private void uploadDirectoryAdvertising(String shopId,String totalValue, String totalPoints){
		
		String statement = "INSERT INTO "+databaseName+".directory_advertising(Shop_ID,TotalValue,TotalPoints,Date) VALUES("
				+ ""+shopId+",\""+totalValue+"\",\""+totalPoints+"\",now());";
		mySQL.executeStatement(statement);
		
	}
	
	
	private void uploadNetworkEngagement(String shopId,String bloomnetOnly, String totalPoints){
		
		String statement = "INSERT INTO "+databaseName+".network_engagement(Shop_ID,BloomnetOnly,TotalPoints,Date) VALUES("
				+ ""+shopId+",\""+bloomnetOnly+"\",\""+totalPoints+"\",now());";
		mySQL.executeStatement(statement);
		
	}
	
	private void uploadProductPurchasing(String shopId,String totalPurchasing, String totalPoints){
		
		String statement = "INSERT INTO "+databaseName+".product_purchasing(Shop_ID,TotalPurchasing,TotalPoints,Date) VALUES("
				+ ""+shopId+",\""+totalPurchasing+"\",\""+totalPoints+"\",now());";
		mySQL.executeStatement(statement);
		
	}
	
	
	private void uploadSendingParticipation(String shopId,String actualSent, String totalPoints){
		
		String statement = "INSERT INTO "+databaseName+".sending_participation(Shop_ID,ActualSent,TotalPoints,Date) VALUES("
				+ ""+shopId+",\""+actualSent+"\",\""+totalPoints+"\",now());";
		mySQL.executeStatement(statement);
		
	}
	
	private void uploadTechnologyParticipation(String shopId,String bms, String webSites, String totalPoints){
		
		String statement = "INSERT INTO "+databaseName+".technology_participation(Shop_ID,BMS,WebSites,TotalPoints,Date) VALUES("
				+ ""+shopId+",\""+bms+"\",\""+webSites+"\",\""+totalPoints+"\",now());";
		mySQL.executeStatement(statement);
		
	}
	
	private void uploadTotalParticipation(String shopId,String sendingScore, String directoryAdvertisingScore, String productPurchasingScore, String technologyParticipationScore,String networkEngagementScore, String totalScore, String letterGrade){
		
		String statement = "INSERT INTO "+databaseName+".total_participation_score(Shop_ID,SendingScore,DirectoryAdvertisingScore,ProductPurchasingScore,TechnologyParticipationScore,NetworkEngagementScore,TotalScore,LetterGrade,Date) VALUES("
				+ ""+shopId+",\""+sendingScore+"\",\""+directoryAdvertisingScore+"\",\""+productPurchasingScore+"\",\""+technologyParticipationScore+"\",\""+networkEngagementScore+"\",\""+totalScore+"\",\""+letterGrade+"\",now());";
		mySQL.executeStatement(statement);
		
	}
	
	private void uploadNewShop(String shopCode, String shopName, String address, String city, String state, String zip, String phone){
		String statement = "INSERT INTO "+databaseName+".shop(ShopCode,ShopName,ShopPhone,ShopAddress1,ShopCity,ShopState,ShopZip) VALUES("
				+ "\""+shopCode+"\",\""+shopName.replaceAll("'", "''")+"\",\""+phone+"\",\""+address+"\",\""+city+"\",\""+state+"\",\""+zip+"\");";
		mySQL.executeStatement(statement);
	}
	
	private String getId(){
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		String id = "";
		try {
			results.next();
			id = results.getString("LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mySQL.closeStatement();
		return id;
	}

	
	private void uploadFLDData(){
		
		FileInputStream fstream;
		
		System.out.println("Setting FLD data");
		
		try {
		  
		  fstream = new FileInputStream("data/fld.txt");
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  while((strLine = br.readLine()) != null){
			  String[] items = strLine.split("\t");
			  String shopCode = items[9];
			  if(items.length > 30){
				  String shopName = items[1];
				  String shopAddress1 = items[2];
				  String city = items[4];
				  String state = items[5];
				  String zip = items[6];
				  String phone = items[30];
				  shopInfo.put(shopCode, shopName+","+shopAddress1+","+city+","+state+","+zip+","+phone);
			  }
		  }
		  
		  br.close();
		  fstream.close();
		  
		}catch(Exception ee){ee.printStackTrace();}
		
		System.out.println("Finished setting FLD data");
	}
	
	
	private void uploadCRMData(){
		
		try {
			System.out.println("Setting CRM Data");
			Sheet sheet = openExcelSheet("data/crm.xls");
			
			for (int ii = 0; ii < sheet.getRows(); ++ii){
				
				String shopCode = sheet.getCell(0,ii).getContents().toString();
				String bmtOnly = sheet.getCell(4,ii).getContents().toString();
				String bms = sheet.getCell(8,ii).getContents().toString();
				
				if(!bmtOnly.equals(""))
					bloomnetOnlyShops.put(shopCode, true);
				if(!bms.equals(""))
					bmsShops.put(shopCode,true);

			}
			System.out.println("Finished setting CRM Data");
		}catch(Exception ee){ee.printStackTrace();}
		
	}
	
	private void uploadPastDueShops(){
		
		try {
			System.out.println("Setting Past Due Shop Data");
			Sheet sheet = openExcelSheet("data/lateshops.xls");
			
			for (int ii = 0; ii < sheet.getRows(); ++ii){
				
				String shopCode = sheet.getCell(1,ii).getContents().toString();
				pastDueShops.put(shopCode,true);
			}
			System.out.println("Finished setting Past Due Shop Data");
		}catch(Exception ee){ee.printStackTrace();}
		
	}
	
	private synchronized Sheet openExcelSheet(String filename){
		
		File inputWorkbook = new File(filename);
		Workbook w = null;
		Sheet sheet = null;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			sheet = w.getSheet(0);
			return sheet;
		}catch(Exception ee){ee.printStackTrace();}
		return null;
	}
	
	private void uploadParticipationIndex(){
		
		File inputWorkbook = new File(fileName);
		Workbook w;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			
			for (int ii = 0; ii < sheet.getRows(); ++ii){
				
				if(ii%100 == 0)
					System.out.println("Completed "+ii+" Lines");
				
				String shopCode = sheet.getCell(0, ii).getContents().toString();
				String shopId = existingShopCodes.get(shopCode);
				
				if(shopId == null){
					if(shopInfo.get(shopCode) != null){
						String shopInfoString = shopInfo.get(shopCode);
						String[] shopInfoItems = shopInfoString.split(",");
						if(shopInfoItems.length == 6){
							String shopName = shopInfoItems[0];
							String address = shopInfoItems[1];
							String city = shopInfoItems[2];
							String state = shopInfoItems[3];
							String zip = shopInfoItems[4];
							String phone = shopInfoItems[5];
							
							uploadNewShop(shopCode,shopName,address,city,state,zip,phone);
							existingShopCodes.put(shopCode, getId());
						}
						
					}
				}
				
				shopId = existingShopCodes.get(shopCode);
				
				if(shopId != null && !shopId.equals("")){
					
					boolean pastDue60Days = false;
					
					if(pastDueShops.get(shopCode) != null && pastDueShops.get(shopCode) == true)
						pastDue60Days = true;
					
					Double sendingPoints = 0.00;
					Double advertisingPoints = 0.00;
					Double purchasingPoints = 0.00;
					Double techPoints = 0.00;
					Double networkEngagementPoints = 0.00;
					Double totalScore = 0.00;
					Integer bmsFlag = 0;
					Integer websiteFlag = 0;
					Integer bloomnetOnlyFlag = 0;
					
					Integer actualSent = 0;
					if(!sheet.getCell(12, ii).getContents().toString().equals(""));
						actualSent = Integer.valueOf(sheet.getCell(12, ii).getContents().toString());
					Double advertisingTotal = 0.00;
					if(!sheet.getCell(20, ii).getContents().toString().equals(""))
						advertisingTotal = Double.valueOf(sheet.getCell(20, ii).getContents().toString());
					Double purchasingTotal = 0.00;
					if(!sheet.getCell(26, ii).getContents().toString().equals(""))
						purchasingTotal = Double.valueOf(sheet.getCell(26, ii).getContents().toString());
					if(napcoPurchasing.get(shopCode) != null)
						purchasingTotal += Double.valueOf(napcoPurchasing.get(shopCode).replaceAll(",", ""));
					Double website = 0.00;
					if(!sheet.getCell(196, ii).getContents().toString().equals(""))
						website = Double.valueOf(sheet.getCell(196, ii).getContents().toString());
					
					
					if(actualSent >= 100)
						sendingPoints = 30.00;
					else if(actualSent >= 25)
						sendingPoints = 20.00;
					else if(actualSent >= 15)
						sendingPoints = 15.00;
					else if(actualSent >= 10)
						sendingPoints = 6.00;
					else if(actualSent >= 5)
						sendingPoints = 3.00;
					
					uploadSendingParticipation(shopId, actualSent.toString(), sendingPoints.toString());
					
					if(advertisingTotal >= 800.00)
						advertisingPoints = 25.00;
					else if (advertisingTotal >= 600.00)
						advertisingPoints = 20.00;
					else if (advertisingTotal >= 450.00)
						advertisingPoints = 15.00;
					else if (advertisingTotal >= 350.00)
						advertisingPoints = 10.00;
					else if (advertisingTotal >= 150.00)
						advertisingPoints = 5.00;
					else if (advertisingTotal >= 80.00)
						advertisingPoints = 2.00;
					
					uploadDirectoryAdvertising(shopId, advertisingTotal.toString(), advertisingPoints.toString());
					
					if(purchasingTotal >= 3000.00)
						purchasingPoints = 20.00;
					else if(purchasingTotal >= 1501.00)
						purchasingPoints = 15.00;
					else if(purchasingTotal >= 700.00)
						purchasingPoints = 10.00;
					else if(purchasingTotal >= 501.00)
						purchasingPoints = 5.00;
					else if(purchasingTotal >= 200.00)
						purchasingPoints = 1.00;
					
					uploadProductPurchasing(shopId, purchasingTotal.toString(), purchasingPoints.toString());
					
					if(bmsShops.get(shopCode) != null){
						techPoints += 25;
						bmsFlag = 1;
					}
					if(website > 0.00){
						techPoints += 15;
						websiteFlag = 1;
					}
					
					uploadTechnologyParticipation(shopId, bmsFlag.toString(), websiteFlag.toString(), techPoints.toString());
					
					
					if(bloomnetOnlyShops.get(shopCode) != null){
						networkEngagementPoints += 25;
						bloomnetOnlyFlag = 1;
					}
					
					uploadNetworkEngagement(shopId, bloomnetOnlyFlag.toString(), networkEngagementPoints.toString());
					
					totalScore = sendingPoints+advertisingPoints+purchasingPoints+techPoints+networkEngagementPoints;
					if(pastDue60Days)
						totalScore = 0.00;
					
					String letterGrade = "";
					if(totalScore > 60.00)
						letterGrade = "A";
					else if(totalScore > 45.00)
						letterGrade = "B";
					else if(totalScore > 30.00)
						letterGrade = "C";
					else if(totalScore > 14.00)
						letterGrade = "D";
					else
						letterGrade = "F";
					
					uploadTotalParticipation(shopId, sendingPoints.toString(), advertisingPoints.toString(), purchasingPoints.toString(), techPoints.toString(), networkEngagementPoints.toString(), totalScore.toString(), letterGrade);
					
				}
				
			}
			
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}

}
