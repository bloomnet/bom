package com.bloomnet.bom.upload.tools;

import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.Date;

import jxl.Sheet;
import jxl.Workbook;

public class UploadBN extends BaseUpload{
	
	private String fileName;
	@SuppressWarnings("unused")
	private File file;
	
	private int count = 0;
	
	public UploadBN(File file, String databaseName) {
		super("logs/BNBadData.txt","logs/BNError.txt","logs/BNNewShopCodes.txt","logs/BNReUpload.txt",databaseName);
		this.file = file;
		setExistingShopCodes("5");
		parseBN();
		//updateAvailability();
	}
	
	public UploadBN(String fileName, String databaseName){
		super("logs/BNBadData.txt","logs/BNError.txt","logs/BNNewShopCodes.txt","logs/BNReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("5");
		parseBN();
		updateAvailability();
	}
	
	public UploadBN(String fileName, String dummy, String databaseName){
		super("logs/BNBadData.txt","logs/BNError.txt","logs/BNNewShopCodes.txt","logs/BNReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("5");
		parseBN();
	}
	
	private String uploadShop(String shopName, String address, String phone, String shopContact,
			String existingZipId, String existingCityId, String city, String state){
		
		String statement = "INSERT INTO "+databaseName+".shop " +
								"(ShopName," +
								"ShopAddress1," +
								"ShopPhone," +
								"ShopContact," +
								"Zip_ID," +
								"City_ID,"+
								"CreatedDate," +
								"CreatedUser_ID," +
								"ModifiedDate," +
								"ModifiedUser_ID) " +
								"VALUES (\""+shopName+"\"," +
								"\""+address+"\"," +
								"\""+phone+"\"," +
								"\""+shopContact+"\"," +
								""+Integer.valueOf(existingZipId)+"," +
								""+Integer.valueOf(existingCityId)+"," +
								"now()," +
								"1," +
								"now()," +
								"1);";

		mySQL.executeStatement(statement);
		String shopId = getId();
		
		shopsByPhone.put(phone,shopId);
		shopsByAddress.put(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase(), shopId);
		
		return shopId;
	}
	
	private void updateShop(String shopName, String address, String phone, String shopContact,
			String existingZipId, String existingCityId, String existingShopId){
		
		String statement = "UPDATE "+databaseName+".shop SET " +
							  "ShopName = \""+shopName+"\"" +
							  ",ShopAddress1 = \""+address+"\"" +
							  ",ShopPhone = \""+phone+"\"" +
							  ",ShopContact = \""+shopContact+"\"" +
							  ",Zip_ID = "+Integer.valueOf(existingZipId)+"" +
							  ",City_ID = "+Integer.valueOf(existingCityId)+"" +
							  ",ModifiedDate = now()" +
							  ",ModifiedUser_ID = 1 " +
							  "WHERE Shop_ID = "+Integer.valueOf(existingShopId)+";";

		mySQL.executeStatement(statement);
	}
	
	private String uploadNetwork(String shopId, String shopCode, String openSunday){
		
		String statement = "INSERT INTO "+databaseName+".shopnetwork " +
						  		"(Shop_ID," +
						  		"Network_ID," +
						  		"ShopCode," +
						  		"ShopNetworkStatus_ID," +
						  		"CommMethod_ID," +
						  		"OpenSunday," +
						  		"CreatedDate," +
						  		"Created_User_ID," +
						  		"ModifiedDate," +
						  		"ModifiedUser_ID) " +
						  		"VALUES ("+
						  		""+Integer.valueOf(shopId)+"," +
						  		"5,\""+
						  		shopCode+"\"," +
						  		"1," +
						  		"2,"+
								Byte.valueOf(openSunday)+"," +
								"now()," +
								"1," +
								"now()," +
								"1);";
	 
	  mySQL.executeStatement(statement);
	 
	  String networkId = getId();
	 
	  shopNetwork.put(shopId+","+"5", networkId);
	  
	  existingShopCodes.put(shopCode, shopId);
	  
	  return networkId;
	}
	
	private void updateNetwork(String openSunday, String networkId){
		
		String statement = "UPDATE "+databaseName+".shopnetwork SET " +
							"CommMethod_ID = 2," +
							"OpenSunday = "+Byte.valueOf(openSunday)+"," +
							"ModifiedDate = now()," +
							"ModifiedUser_ID = 1 ";

	  
	  statement += "WHERE ShopNetwork_ID = "+Integer.valueOf(networkId)+";";
	  
	  mySQL.executeStatement(statement);
	}
	
	@SuppressWarnings("unused")
	private void parseBNCont(Sheet sheet) throws IOException{
		
		String shopName,phone,address,zip,city,state;
		
		for (int ii = 0; ii < sheet.getRows(); ++ii){
			  
			  ++count;
			  
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);			  
			  shopName = sheet.getCell(1, ii).getContents().toString();
			  zip = sheet.getCell(5, ii).getContents().toString();
			  if(zip.length() == 3) zip = "00" + zip;
			  else if(zip.length() == 4) zip = "0" + zip;
			  phone = sheet.getCell(6, ii).getContents().toString();
			  address = sheet.getCell(2, ii).getContents().toString();
			  city = sheet.getCell(3, ii).getContents().toString();
			  state = sheet.getCell(4, ii).getContents().toString();
			  
		  	  String openSunday = "0";
		  	  String shopCode = "BN"+ new Date().getTime();
				  
			  if(cityId.get(city.toUpperCase()+","+state.toUpperCase()) != null){
				  					  
				  if(zipId.get(zip) != null){
					  
					  String existingCityId = cityId.get(city.toUpperCase()+","+state.toUpperCase());
					  String existingZipId = zipId.get(zip);
					  
					  String existingShopId = "";
					  
					  if(!phone.equals("") && shopsByPhone.get(phone)!= null) existingShopId = shopsByPhone.get(phone);
					  
					  else if(!address.equals("") && shopsByAddress.get(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase()) != null)
						  existingShopId = shopsByAddress.get(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase());
					  
					  else if(!phone.equals("") && FLD.get(phone) != null){
						  
						  String existingShopCode = FLD.get(phone);
						  
						  if(existingShopCodes.get(existingShopCode) != null){
							  
							  existingShopId = existingShopCodes.get(existingShopCode);
							  System.out.println("Got one!! "+shopCode+ " : "+existingShopCode);
						  }
						  
						  else errorOut.write("BN Shop Code: "+shopCode+" matched shop code: "+existingShopCode+" in FLD, but we do not have that BMT shop code in our database\n");
					  }
					  
					  if(existingShopCodes.get(shopCode) != null){
						  
						  existingShopId = existingShopCodes.get(shopCode);
						  
						  if((shopNetwork.get(existingShopId+","+"1") != null) || (shopNetwork.get(existingShopId+","+"2") != null)){ /*do nothing*/ }
						  
						  else if(uniqueShops.get(shopCode) == null) 
							  updateShop(shopName, address, phone, "", existingZipId, existingCityId, existingShopId);
						  
						  String networkId = shopNetwork.get(existingShopId+","+"5");
						  
						  if(uniqueShops.get(shopCode) == null) 
							  updateNetwork(openSunday, networkId);
						  
					  }else if(!existingShopId.equals("") && shopNetwork.get(existingShopId+","+"5") != null){
						  
						  if(uniqueShops.get(shopCode) == null){
							  out.write("There is a new shop code for shop ID: "+existingShopId+": "+shopCode+"\n");
						  	  shopCodesOut.write(existingShopId+","+shopCode+"\n");
						  }
					  }else if(!existingShopId.equals("")){
						  
						  if((shopNetwork.get(existingShopId+","+"1") != null) || (shopNetwork.get(existingShopId+","+"2") != null)){ /*do nothing*/ }
						  
						  else updateShop(shopName, address, phone, "", existingZipId, existingCityId, existingShopId);
						  
						  String networkId = uploadNetwork(existingShopId, shopCode, openSunday);
						  
						  
					  }else{
							  
						  String shopId = uploadShop(shopName, address, phone, "", existingZipId, existingCityId, city, state);
						  
						  String networkId = uploadNetwork(shopId, shopCode, openSunday);
						  
						  
					  }
			  	}else{
			  		if(!uniqueBadData.contains(zip)){
						  out.write("No zip was found for: "+zip+" for shop: "+shopCode+"\n");
						  uniqueBadData.add(zip);
					  }
			  	}
				 }else{
					 if(uniqueShops.get(shopCode) == null){
						  //reUploadOut.write(strLine+"\n");
					  }
					  if(!uniqueBadData.contains(city+","+state)){
						  out.write("No city was found for: "+city+", "+state+" for shop: "+shopCode+"\n");
						  uniqueBadData.add(city+","+state);
					  }
				 }  
			  uniqueShops.put(shopCode, 1);
		  }
	}
	
	public void parseBN(){
		
		try{
			 
			count=0;
			  
			File inputWorkbook = new File(fileName);
			Workbook w;
			w = Workbook.getWorkbook(inputWorkbook);
			Sheet sheet = w.getSheet(0);
			
			parseBNCont(sheet);
			 
		}catch(Exception ee){
			System.out.println(ee.getMessage());
			
			try {
				errorOut.write(ee.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}finally{
			try {
				out.close();
				errorOut.close();
				shopCodesOut.close();
				reUploadOut.close();
				mySQL.closeFile();
				setShopsToUpdate("5");
			} catch (IOException e) {
				e.printStackTrace();
			} catch (SQLException e) {
				try {
					errorOut.write(e.getMessage()+"\n");
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	private void updateAvailability(){
		
		System.out.println("Updating shop active/inactive statuses");
		
		for(int ii=0; ii<shopsToDeactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
							"SET ShopNetworkStatus_ID = \"2\" " +
							"WHERE ShopCode = \""+shopsToDeactivate.get(ii)+"\"";
			mySQL.executeStatement(statement);
		}
		
		for(int ii=0; ii<shopsToReactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
						   "SET ShopNetworkStatus_ID = \"1\" " +
						   "WHERE ShopCode = \""+shopsToReactivate.get(ii)+"\"";
			
			mySQL.executeStatement(statement);
		}
		
		System.out.println("Finished updating shop active/inactive statuses");
	}
	
}
