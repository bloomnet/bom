package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class UploadPopulationData {
	
	HashMap<String,String> existingStates = new HashMap<String,String>();
	SQLData mySQL;
	String databaseName;
	String fileName;
	
	public UploadPopulationData(String fileName, String databaseName){
		
		this.fileName = fileName;
		this.databaseName = databaseName;
		mySQL = new SQLData();
		mySQL.login();
		
		try {
			setExistingStates();
			uploadPopulationData();
		} catch (SQLException e) {
		}
	}
	
	
	private void setExistingStates() throws SQLException{
		
		System.out.println("Setting State IDs");
		
		String query = "SELECT state.State_ID,state.Name FROM "+databaseName+".state;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			existingStates.put(results.getString("Name").toUpperCase(), results.getString("State_ID"));
		}
		results.close();
		mySQL.closeStatement();
		
		System.out.println("Finished Setting State IDs");
	}
	
	private void uploadPopulationData(){
		
		String city,state,population;
		
		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  @SuppressWarnings("resource")
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  if((strLine = br.readLine()) != null){
			  System.out.println("File Good");
		  }
		  
		  while((strLine = br.readLine()) != null){
			  
			  if(count%100 == 0){
				  System.out.println(count+" lines completed.");
			  }
				  
			  String[] lineItems = strLine.split(",");
			  city = lineItems[6];
			  state = lineItems[7];
			  population = lineItems[12];
			  
			  String statement = "UPDATE "+databaseName+".city SET Population = "+population+" WHERE Name = \""+city+"\" AND State_ID = "+existingStates.get(state.toUpperCase())+";";
			  mySQL.executeStatement(statement);
			  count++;
		  
		  }
		  
		}catch(Exception ee){
			ee.printStackTrace();
		}
	}
}
