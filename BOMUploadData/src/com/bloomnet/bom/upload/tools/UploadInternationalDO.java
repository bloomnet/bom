package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class UploadInternationalDO {
	
	SQLData mySQL;
	private File countriesFile;
	private File associationFile;
	private File internationalShops;
	
	private HashMap<String,String> cityId = new HashMap<String,String>();
	private HashMap<String,String> countryId = new HashMap<String,String>();
	private HashMap<String,String> stateId = new HashMap<String,String>();
	private HashMap<String,String> countryTwoToThree = new HashMap<String,String>();
	private HashMap<String,String> countryFromCode = new HashMap<String,String>();
	private HashMap<String,String> shopCountries = new HashMap<String,String>();
	private HashMap<String,String> shopId = new HashMap<String,String>();
	
	public UploadInternationalDO(String file, String file2, String file3, String databaseName){
		this.countriesFile = new File(file);
		this.associationFile = new File(file2);
		this.internationalShops = new File(file3);
		mySQL = new SQLData();
		mySQL.login();
		mySQL.executeStatement("USE bloomnet;");
		try {
			setCountryId();
			setCityId();
			setStateId();
			setShopId();
			uploadInternationalCountries();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setCountryId() throws SQLException{
		
		System.out.println("Setting Country IDs");
		
		String query = "SELECT idcountry,short_name FROM country;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			countryId.put(results.getString("short_name").toUpperCase(), results.getString("idcountry"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Country IDs");
	}
	
	private void setStateId() throws SQLException{
		
		System.out.println("Setting State IDs");
		
		String query = "SELECT id,short_name FROM state;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			stateId.put(results.getString("short_name").toUpperCase(), results.getString("id"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting State IDs");
	}

	private void setCityId() throws SQLException{
		
		System.out.println("Setting City IDs");
		
		String query = "SELECT city.id,city.name,state.short_name " +
					  "FROM city " +
					  "INNER JOIN state ON city.state_id = state.id;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			try{
				cityId.put(results.getString("name").toUpperCase()+","+results.getString("short_name").toUpperCase(), results.getString("id"));
			}catch(Exception ee){
				System.out.println("Error Alert!");
			}
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting City IDs");
	}
	
	private void setShopId() throws SQLException{
		
		System.out.println("Setting Shop IDs");
		
		String query = "SELECT shop_code,shop_id FROM shop;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			shopId.put(results.getString("shop_code").toUpperCase(), results.getString("shop_id"));
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Setting Shop IDs");
	}
	
	protected String getId(){
		String query = "SELECT LAST_INSERT_ID()";
		ResultSet results = mySQL.executeQuery(query);
		String id = "";
		try {
			results.next();
			id = results.getString("LAST_INSERT_ID()");
		} catch (SQLException e) {
			e.printStackTrace();
		}
		mySQL.closeStatement();
		return id;
	}
	
	private void insertCountry(String name,String shortName){
		
		String statement = "INSERT INTO country(name,short_name) VALUES(\""+name+"\",\""+shortName+"\");";
		mySQL.executeStatement(statement);
		
	}
	
	private void insertState(String name,String countryId,String shortName){
		
		String statement = "INSERT INTO state(name,country_idcountry,short_name) VALUES(\""+name+"\",\""+countryId+"\",\""+shortName+"\");";
		mySQL.executeStatement(statement);
		
	}
	
	private void insertCity(String name,String stateId){
		
		String statement = "INSERT INTO city(name,state_id,major_city,city_type) VALUES(\""+name+"\",\""+stateId+"\",\"0\",\"D\");";
		mySQL.executeStatement(statement);
		
	}
	
	private void insertAddress(String address,String cityId,String zip){
		
		String statement = "INSERT INTO address(street_address1,city_id,postal_code) VALUES(\""+address+"\",\""+cityId+"\",\""+zip+"\");";
		mySQL.executeStatement(statement);
		
	}
	
	private void insertShop(String shopCode,String name,String phone,String contact,String addressId){
		
		String statement = "INSERT INTO shop(shop_code,shop_name,telephone_number,contact_person,address_id,open_sunday,bloomlink_indicator,new_shop,florist_for_forrests,latitude,longitude,date_created,user_created_id) "
				+ "VALUES(\""+shopCode+"\",\""+name+"\",\""+phone+"\",\""+contact+"\",\""+addressId+"\",0,1,0,0,0.00,0.00,now(),1);";
		mySQL.executeStatement(statement);
		
	}
	
	private void insertListing(String shopCode,String entryCode,String cityId){
		
		String statement = "INSERT INTO listing(shop_code,entry_code,listing_type_id,rotate,rev_listing,cl8,ca9,status,city_id,date_created,user_created_id) "
				+ "VALUES(\""+shopCode+"\",\""+entryCode+"\",2,0,0,0,0,1,"+cityId+",now(),1);";
		mySQL.executeStatement(statement);
		
	}
	
	@SuppressWarnings({ "resource", "unused" })
	private void uploadInternationalCountries() throws IOException, SQLException{
		
		  FileInputStream fstream = new FileInputStream(associationFile);
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  while((strLine = br.readLine()) != null){
			  String[] lineItems = strLine.split(",");
			  if(lineItems.length == 3){
				  String country = lineItems[0];
				  String countryCodeTwo = lineItems[1];
				  String countryCodeThree = lineItems[2];
				  countryTwoToThree.put(countryCodeTwo,countryCodeThree);
				  countryFromCode.put(countryCodeThree, country);
			  }
		  }
		  
		  fstream = new FileInputStream(internationalShops);
		  in = new DataInputStream(fstream);
		  br = new BufferedReader(new InputStreamReader(in));
		  
		  while((strLine = br.readLine()) != null){
			  String[] lineItems = strLine.split(",");
			  if(lineItems.length >= 3){
				  String countryCodeThree = lineItems[0];
				  String shopCode = lineItems[2];
				  shopCountries.put(countryCodeThree,shopCode);
			  }
		  }
		  
		  fstream = new FileInputStream(countriesFile);
		  in = new DataInputStream(fstream);
		  br = new BufferedReader(new InputStreamReader(in));
		  
		  int lineNumber = 0;
		  while((strLine = br.readLine()) != null){
			  try{
				  lineNumber++;
				  String[] lineItems = strLine.split(",");
				  if(lineNumber%1000 == 0){
					  System.out.println(lineNumber);
				  }
				  if(lineItems.length >= 2){
					  String countryCodeTwo = lineItems[0].toUpperCase();
					  String city = lineItems[1].toUpperCase();
					  String countryCodeThree = countryTwoToThree.get(countryCodeTwo);
					  if(countryCodeThree != null){
						  if(shopCountries.get(countryCodeThree) != null){
							  String country = countryId.get(countryCodeThree);
							  if(country == null){
								  country = countryFromCode.get(countryCodeThree);
								  if(country == null){
									  System.out.println("Could not get country name from two character country code...");
									  continue;
								  }
								  country = country.toUpperCase();
								  insertCountry(country, countryCodeThree);
								  countryFromCode.put(countryCodeThree, country);
								  String insertId = getId();
								  countryId.put(countryCodeThree, insertId);
								  
								  insertState(country, insertId, countryCodeThree);
								  stateId.put(countryCodeThree, getId());
							  }
							  
							  String state = stateId.get(countryCodeThree);
							  
							  /*if(cityId.get(city+","+countryCodeThree) == null){
								  insertCity(city, state);
								  cityId.put(city+","+countryCodeThree, getId());
							  }*/ //use this when we decide to use actual cities with actual coverage
							  if(cityId.get(country+","+countryCodeThree) == null){
								  insertCity(countryFromCode.get(countryCodeThree).toUpperCase(), state);
								  cityId.put(country+","+countryCodeThree, getId());
							  }
						  }else{
							  //System.out.println("No shops in: "+countryCodeThree);
						  }
						  
					  }else{
						  //System.out.println("Could not get Three Character Country Code From Two Character Coutnry Code...");
					  }
				  }
			  }catch(Exception ee){
				System.out.println(ee.getMessage());  
			  }
		  }
		  
		  fstream = new FileInputStream(internationalShops);
		  in = new DataInputStream(fstream);
		  br = new BufferedReader(new InputStreamReader(in));
		  
		  while((strLine = br.readLine()) != null){
			  try{
				  String[] lineItems = strLine.split(",");
				  if(lineItems.length >= 13){
					  String countryCodeThree = lineItems[0];
					  String shopCode = lineItems[2];
					  String shopName = lineItems[5];
					  String shopContact = lineItems[6];
					  String shopAddress = lineItems[8];
					  String shopZip = lineItems[11];
					  String shopPhone = lineItems[12];
					  
					  if(shopId.get(shopCode) == null){
						  String city = cityId.get("INTERNATIONAL,INT");
						  insertAddress(shopAddress, city, shopZip);
						  String addressId = getId();
						  insertShop(shopCode, shopName, shopPhone, shopContact, addressId);
						  shopId.put(shopCode, getId());
					  }
					  
					  String state = stateId.get(countryCodeThree);
					  if(state != null){
						  String query = "SELECT id FROM city WHERE state_id = "+state+";";
						  ResultSet results = mySQL.executeQuery(query);
						  int ii=0;
						  while(results.next()){
							  ii++;
							  String cityId = results.getString("id");
							  insertListing(shopCode, String.valueOf(ii), cityId);
						  }
					  }else{
						  System.out.println("No Country Found for Shop For: "+countryCodeThree);
					  }
				  }
			  }catch(Exception ee){
				System.out.println(ee.getMessage());  
			  }
		  }
	}
	
}
