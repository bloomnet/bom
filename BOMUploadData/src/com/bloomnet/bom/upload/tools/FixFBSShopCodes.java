package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

public class FixFBSShopCodes {
	FixFBSShopCodes() throws IOException, SQLException{
		SQLData mySQL = new SQLData();
		mySQL.login();
		String statement = "USE fbs;";
		mySQL.executeStatement(statement);
		File file = new File("data/shopstofix.txt");
		FileReader fileReader = new FileReader(file);
		@SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(fileReader);
		
		String line = "";
		int num = 0;
		while((line = br.readLine()) != null){
			num++;
			if(num % 100 == 0)
				System.out.println(num+ " Completed...");
			String shopCode = line;
			String query = "SELECT FLORIST_VEND_REF_ID FROM fbs_florist_prod WHERE FLORIST_CODE LIKE '%"+shopCode+"';";
			ResultSet results = mySQL.executeQuery(query);
			
			while(results.next()){
				String id = results.getString("FLORIST_VEND_REF_ID");
				statement = "UPDATE fbs_florist_prod SET FLORIST_CODE = '"+shopCode.toUpperCase()+"' WHERE FLORIST_VEND_REF_ID = "+id+";";
				mySQL.executeStatement(statement);
				
			}
			results.close();
			mySQL.closeStatement();
		}
	}
	
	public static void main(String[] args){
		try {
			new FixFBSShopCodes();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
