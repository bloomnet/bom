package com.bloomnet.bom.upload.tools;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class BMSRetailReport {
	
	Map<String,String> payments = new HashMap<String,String>();
	Map<String,String> orderRefundsToProcess = new HashMap<String,String>();
	
	static String start_d = "";
	static String end_d = "";
	
	String connectionUrl = "jdbc:sqlserver://172.24.16.79:1433;"
			+ "databaseName=BMS_PRD;integratedSecurity=false;user=bmtfsi;password=BmtFsi1234";
	
	private void setHouseAccounts() throws IOException{
		System.out.println("Writing House Accounts File");
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(connectionUrl);
			String query = "select HA.HouseAccountID, HA.Name, FF.BloomlinkCustomerNumber, 0, ADDR.AddressLine1, ADDR.AddressLine2, ADDR.City, ADDR.Region, ADDR.Country, ADDR.PostalCode, ADDR.Phone, ADDR.FirstName + ' ' + ADDR.LastName AS FullName " +
			"From FlowersHouseAccounts HA " + 
			"INNER JOIN FlowersFlorists FF ON FF.PlatformID = HA.PlatformID " +
			"INNER JOIN Addresses ADDR ON ADDR.AddressID = HA.BillingAddressID " +
			"where HA.PlatformID IN (1435, 1436, 2716, 15678);";
			Statement stmt = con.createStatement();
			ResultSet results = stmt.executeQuery(query);
			
			File file = new File("output/HouseAccounts.csv");
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			
			String line = "CUSTOMER_NUMBER,CUSTOMER_NAME,STORE_NUMBER,CREDIT_LIMIT,ADDRESS_LINE1,ADDRESS_LINE2,CITY,STATE,"
					+ "COUNTRY,POSTAL_CODE,PHONE_NUMBER,CONTACT_NAME";
			writer.write(line + "\n");
			
			while(results.next()){
				
				String cNum = results.getString(1);
				if(cNum != null)
					cNum = cNum.replaceAll(",", " ");
				String cName = results.getString(2);
				if(cName != null)
					cName = cName.replaceAll(",", " ");
				String shopCode = results.getString(3);
				if(shopCode != null)
					shopCode = shopCode.replaceAll(",", " ");
				String creditLimit = results.getString(4);
				if(creditLimit != null)
					creditLimit = creditLimit.replaceAll(",", " ");
				String address = results.getString(5);
				if(address != null)
					address = address.replaceAll(",", " ");
				String address2  = results.getString(6);
				if(address2 != null)
					address2 = address2.replaceAll(","," ");
				String city = results.getString(7);
				if(city != null)
					city = city.replaceAll(",", " ");
				String state = results.getString(8);
				if(state != null)
					state = state.replaceAll(",", " ");
				String country = results.getString(9);
				if(country != null)
					country = country.replaceAll(",", " ");
				String zip = results.getString(10);
				if(zip != null)
					zip = zip.replaceAll(",", " ");
				String phone = results.getString(11);
				if(phone != null)
					phone = phone.replaceAll(",", " ");
				String contactName = results.getString(12);
				if(contactName != null)
					contactName = contactName.replaceAll(",", " ");
				
				line = cNum+","+cName+","+shopCode+","+creditLimit+","+address+","+address2+","+city+","+state
						+","+country+","+zip+","+phone+","+contactName;
				writer.write(line + "\n");
				writer.flush();
				
			}
			
			writer.close();
			results.close();
			stmt.close();
			con.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			System.out.println("Finished Writing House Accounts File");
		}
	}
	
	private void setPayments() throws IOException{
		System.out.println("Setting Payments");
		try {
			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(connectionUrl);
			String query = "SELECT OP.OrderID, ORD.OrderDate, PayType.EnumName AS [Value], IsNull(OP.AccountNumber, '') AS AccountNumber, OP.Amount "+
					"FROM OrderPayments OP "+
					"INNER JOIN dbo.PaymentTypeEnum PayType ON PayType.EnumId = OP.PaymentType_CodeID "+
					"INNER JOIN Orders ORD ON ORD.OrderID = OP.OrderID "+
					"WHERE OP.OrderID IN (select LEDGE.OrderID "+
					"from FlowersHouseAccountLedger LEDGE "+
					"INNER JOIN FlowersHouseAccounts FHA ON FHA.HouseAccountID = LEDGE.HouseAccountID "+
					"WHERE FHA.PlatformID in (1436, 15678)) "+
					"AND ORD.OrderDate >= '"+start_d+"' AND ORD.OrderDate < '"+end_d+"' "+
					"ORDER BY OP.OrderID;";
			Statement stmt = con.createStatement();
			ResultSet results = stmt.executeQuery(query);
			
			while(results.next()){
				String orderId = results.getString("OrderID");
				String houseAccount = results.getString("AccountNumber");
				String amount = results.getString("Amount");
				
				payments.put(orderId, houseAccount);
				if(Double.valueOf(amount) < 0.00)
					orderRefundsToProcess.put(orderId, amount);
			}
			
			results.close();
			stmt.close();
			con.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			System.out.println("Finished Setting Payments");
		}
	}
	
	private void createDoc(){
		System.out.println("Generating Report");
		try {

			Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
			Connection con = DriverManager.getConnection(connectionUrl);
			
			File file = new File("output/BMT_Corp_HouseAccount_Data_"+end_d.replaceAll("/","_")+".csv");
			BufferedWriter writer = new BufferedWriter(new FileWriter(file));
			
			String line = "SOURCE,CUST_TRX_TYPE,CUST_TRX_NUMBER,TRX_DATE,ORDER_NUMBER,CUSTOMER_NUMBER,STORE_NUMBER,LINE_TYPE,QUANTITY,"
					+ "UNIT_SELLING_PRICE,TOTAL_AMOUNT,PAID_AMOUNT,SALES_CODE,STATUS,CREATION_DATE,PROCESS_FLAG,"
					+ "PROCESS_DATE,REFERENCE1,LINE_NUMBER,DESCRIPTION,ORIG_ID";
			writer.write(line + "\n");
			
			String query = "SELECT OLI.OrderID, OLI.OrderLineItemID, ORD.OrderDate, OLI.Quantity, OLI.UnitPrice, OLI.DeliveryFee, OLI.ServiceCharge, OLI.Taxes, FHA.PlatformID "+
							"FROM OrderLineItems OLI "+
							"INNER JOIN Orders ORD ON ORD.OrderID = OLI.OrderID "+
							"INNER JOIN FlowersHouseAccountLedger LEDGE ON OLI.OrderID = LEDGE.OrderID " +
							"INNER JOIN FlowersHouseAccounts FHA ON FHA.HouseAccountID = LEDGE.HouseAccountID " +
							"WHERE OLI.OrderID IN (select LEDGE.OrderID "+
							"from FlowersHouseAccountLedger LEDGE "+
							"INNER JOIN FlowersHouseAccounts FHA ON FHA.HouseAccountID = LEDGE.HouseAccountID "+
							"WHERE FHA.PlatformID in (1436, 15678)) "+
							"AND ORD.OrderDate >= '"+start_d+"' AND ORD.OrderDate < '"+end_d+"' "+
							"ORDER BY OLI.OrderID, OLI.OrderLineItemID;";
			Statement stmt = con.createStatement();
			ResultSet results = stmt.executeQuery(query);
			
			while(results.next()){
				String source = "RETAIL";
				String trxType = "1";
				String date = results.getString("OrderDate");
				String platformID = results.getString("PlatformID");
				String orderNum = results.getString("OrderID");
				String customerNumber = payments.get(orderNum);
				String storeNum = "";
				if(platformID.equals("1436"))
					storeNum = "313";
				else if(platformID.equals("15678"))
					storeNum = "417";
				String lineType = "60";
				String quantity = "1";
				String tax = results.getString("Taxes");
				String price = results.getString("UnitPrice");
				String delivery = results.getString("DeliveryFee");
				String serviceCharge = results.getString("ServiceCharge");
				String processFlag = "N";
				String lineNum = "1";
				String origId = "101";
				
				Integer quantityInt = Integer.valueOf(results.getString("Quantity"));
				Integer lineNumInt = Integer.valueOf(results.getString("OrderLineItemID"));
				Double priceInt = Double.valueOf(price);
				Double deliveryInt = Double.valueOf(delivery); 
				Double serviceChargeInt = Double.valueOf(serviceCharge);
				
				String lineNum2 = String.valueOf(lineNumInt + 1);
				String lineType2 = "30";
				String totalPrice = String.valueOf((quantityInt * priceInt) + deliveryInt + serviceChargeInt);
				
				line = source+","+trxType+",,"+date+","+orderNum+","+customerNumber+","+storeNum+","+lineType+","+quantity+","+tax+","+tax+",,,,,"+processFlag+",,,"+lineNum+",,"+origId;
				writer.write(line+"\n");
				
				line = source+","+trxType+",,"+date+","+orderNum+","+customerNumber+","+storeNum+","+lineType2+","+quantity+","+totalPrice+","+totalPrice+",,,,,"+processFlag+",,,"+lineNum2+",,"+origId;
				writer.write(line+"\n");
				
				if(orderRefundsToProcess.get(orderNum) != null){
					
					String amount = orderRefundsToProcess.get(orderNum);
					
					line = source+","+trxType+",,"+date+","+orderNum+","+customerNumber+","+storeNum+",90,0,0.00,"+amount+",,,,,"+processFlag+",,,,,"+origId;
					writer.write(line+"\n");
					
					orderRefundsToProcess.remove(orderNum);
				}
				writer.flush();
			}
			
			results.close();
			stmt.close();
			con.close();
			
			writer.close();
			
		}catch (Exception e) {
			e.printStackTrace();
		}finally{
			System.out.println("Finished Generating Report");
		}
		
	}

	public static void main(String[] args) {
		
		BMSRetailReport create = new BMSRetailReport();
		
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Please Enter a Start Date in the Format MM/dd/YYYY: ");
		start_d = in.nextLine();
		System.out.println("Please Enter an End Date in the Format MM/dd/YYYY: ");
		end_d = in.nextLine();
		
		try {
			create.setHouseAccounts();
			create.setPayments();
			create.createDoc();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}
}
