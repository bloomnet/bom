package com.bloomnet.bom.upload.tools;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;

public class TelefloraBOMReport {
	
	String orderNumbers = "";
	
	Map<String,String> statuses = new HashMap<String,String>();
	
	SQLData mySQL = new SQLData();
	
	public TelefloraBOMReport(){
		mySQL.login();
		String statement = "USE bloomnetordermanagement;";
		mySQL.executeStatement(statement);
	}
	
    private void getFTPFile(){
    	String server = "sshftp.teleflora.com";
        int port = 22;
        String user = "bloomnet";
        String pass = "flower5";
        
        try {
	        	JSch jsch = new JSch();
	            Session session = jsch.getSession(user, server, port);
	            session.setPassword(pass);
	            session.setConfig("StrictHostKeyChecking", "no");
	            
	            System.out.println("Establishing Connection...");
	            session.connect();
	           
	            System.out.println("Connection established.");
	            System.out.println("Crating SFTP Channel.");
	            ChannelSftp sftpChannel = (ChannelSftp) session.openChannel("sftp");
	            sftpChannel.connect();
	            System.out.println("SFTP Channel created.");
     
                String billingFile = "/html/430306.htm";
                File downloadFile = new File("/TelefloraBOMReport/data/43030600.html");
                OutputStream outputStream = new BufferedOutputStream(new FileOutputStream(downloadFile));
                InputStream in = sftpChannel.get(billingFile);
                copy(in, outputStream);
     
                System.out.println("43030600.html has been downloaded successfully.");
                
                sftpChannel.disconnect();
                session.disconnect();
                
                
        } catch (Exception ex) {
            System.out.println("Oops! Something wrong happened");
            ex.printStackTrace();
        }
    }
    
    private static final int BUFFER_SIZE = 2 * 1024 * 1024;
    private void copy(InputStream input, OutputStream output) throws IOException {
        try {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead = input.read(buffer);
            while (bytesRead != -1) {
                output.write(buffer, 0, bytesRead);
                bytesRead = input.read(buffer);
            }
        } finally {
            input.close();
            output.close();
        }
    }
    
    private void executePerlScript(String date){
    	try {
            // input the file content to the String "input"
            BufferedReader file = new BufferedReader(new FileReader("/TelefloraBOMReport/data/parseTFStatement.pl"));
            String line;
            String input = "";

            while ((line = file.readLine()) != null){
            	if(line.startsWith("$myMonth ="))
            		line = "$myMonth = \""+date+"\";";
            	input += line + '\n';
            }

            file.close();


            FileOutputStream fileOut = new FileOutputStream("/TelefloraBOMReport/data/parseTFStatement.pl");
            fileOut.write(input.getBytes());
            fileOut.close();
            
            Process p = Runtime.getRuntime().exec(new String[] {"perl", "/TelefloraBOMReport/data/parseTFStatement.pl"});
            p.waitFor();
            
            

        } catch (Exception e) {
            System.out.println("Problem reading file.");
        }

    }
    
    private void createReport(String date){
    	
    	BufferedReader file;
		try {
			file = new BufferedReader(new FileReader("data/TF_"+date+"_statement.txt"));
			String line;
			int count = 0;
	        while ((line = file.readLine()) != null){
	        	String oNum = line.split("\t")[7];
	        	if(count == 0)
	        		orderNumbers += "\""+oNum+"\"";
	        	else
	        		orderNumbers += "," + "\""+oNum+"\"";
	        	count ++;
	        }
	        
	        String query = "SELECT Status,ChildOrderNumber FROM bomorderview_v2 WHERE Route = 'TFSI' AND ChildOrderNumber IN("+orderNumbers+")";
	        ResultSet results = mySQL.executeQuery(query);
	        
	        while(results.next()){
	        	
	        	String status = results.getString(1);
	        	String oNum = results.getString(2);
	        	
	        	statuses.put(oNum, status);
	        }
	        
	        File outFile = new File("/TelefloraBOMReport/data/TF_"+date+"_statement.csv");
			BufferedWriter writer = new BufferedWriter(new FileWriter(outFile));
	        String outLine ="a,b,Shop#,Teleflora-Shop,City,Date,Recipient,Reference#,Form#-Seq#,Amount,Status in BOM";
	        
	        writer.write(outLine+"\n");
	        writer.flush();
	        
	        file = new BufferedReader(new FileReader("data/TF_"+date+"_statement.txt"));
			count = 0;
	        while ((line = file.readLine()) != null){
	        	
	        	if(count != 0){

		        	String[] lineArray = line.split("\t");
		        	String a = lineArray[0];
		        	String b = lineArray[1];
		        	String shopCode = lineArray[2];
		        	String shopName = lineArray[3].replaceAll(",","-");
		        	String city = lineArray[4].replaceAll(",","-");
		        	String oDate = lineArray[5];
		        	String recipient = lineArray[6].replaceAll(",","-");
		        	String orderNum = lineArray[7];
		        	String seqNum = lineArray[8];
		        	String amount = lineArray[9];
		        	String status = "";
		        	
		        	if(Double.valueOf(amount) < 0.00)
		        		status = "Credit";
		        	else if(orderNum.length() == 0)
		        		status = "May Dispute - No Order Number Provided";
		        	else if(orderNum.length() != 6)
		        		status = "May Dispute - Invalid Order Number";
		        	else if(statuses.get(orderNum) != null)
		        		status = statuses.get(orderNum);
		        	else
		        		status = "May Dispute - Order Not in BOM";
		        	
		        	if(status.equals("Cancel Confirmed by BloomNet"))
		        		status = "May Dispute - Cancelled Order";
		        	
		        	outLine = a+","+b+","+shopCode+","+shopName+","+city+","+oDate+","+recipient+","+orderNum+","+seqNum+","+amount+","+status;	        	
		        	writer.write(outLine+"\n");
		        	writer.flush();
	        	}
		        count ++;
	        }
	        writer.close();
	        zipFiles("/TelefloraBOMReport/data/43030600.html","/TelefloraBOMReport/data/TF_"+date+"_statement.csv");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
    
    private static void zipFiles(String filePath1, String filePath2) {
        try {
            File firstFile = new File(filePath1);
            String zipFileName = firstFile.getName().concat(".zip");
 
            FileOutputStream fos = new FileOutputStream("/root/tomcat7/webapps/files/"+zipFileName);
            ZipOutputStream zos = new ZipOutputStream(fos);
 
            zos.putNextEntry(new ZipEntry(new File(filePath1).getName()));
            byte[] bytes = Files.readAllBytes(Paths.get(filePath1));
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();
            
            zos.putNextEntry(new ZipEntry(new File(filePath2).getName()));
            bytes = Files.readAllBytes(Paths.get(filePath2));
            zos.write(bytes, 0, bytes.length);
            zos.closeEntry();
 
            zos.close();
 
        } catch (FileNotFoundException ex) {
            System.err.println("A file does not exist: " + ex);
        } catch (IOException ex) {
            System.err.println("I/O error: " + ex);
        }
    }
    
	public static void main(String[] args) {
		
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		System.out.println("Please Enter a Start Date in the Format YYYY-MM: ");
		String date = in.nextLine();
		TelefloraBOMReport tfbr = new TelefloraBOMReport();
		tfbr.getFTPFile();
		tfbr.executePerlScript(date);
		tfbr.createReport(date);
    }
}
