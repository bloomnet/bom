package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class UploadBMTShops extends BaseUpload{
	
	private String fileName;
	private File file;
	private Map<String,Integer> phoneNumbersSeen = new HashMap<String,Integer>();
	
	public UploadBMTShops(File file, String databaseName) {
		super("logs/BMTBadData.txt","logs/BMTError.txt","logs/BMTNewShopCodes.txt","logs/BMTReUpload.txt",databaseName);
		this.file = file;
		setExistingShopCodes("1");
		parseBMTData();
		try {
			updateAvailability();
			updateCoverage();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public UploadBMTShops(String fileName, String databaseName){
		super("logs/BMTBadData.txt","logs/BMTError.txt","logs/BMTNewShopCodes.txt","logs/BMTReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("1");
		parseBMTData();
		try {
			updateAvailability();
			updateCoverage();
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	public UploadBMTShops(String fileName, String dummy, String databaseName){
		super("logs/BMTBadData.txt","logs/BMTError.txt","logs/BMTNewShopCodes.txt","logs/BMTReUpload.txt",databaseName);
		this.fileName = fileName;
		setExistingShopCodes("1");
		parseBMTData();
	}
	
	private String uploadShop(String shopName, String address, String address2, String phone, String shopContact, String fax, 
			 String existingZipId, String existingCityId, String city, String state, String email){
		
		String statement = "INSERT INTO "+databaseName+".shop " +
								"(ShopName," +
								"ShopAddress1," +
								"ShopAddress2," +
								"ShopPhone," +
								"ShopContact," +
								"ShopFax," +
								"ShopEmail," +
								"Zip_ID," +
								"City_ID,"+
								"CreatedDate," +
								"CreatedUser_ID," +
								"ModifiedDate," +
								"ModifiedUser_ID) " +
								"VALUES (\""+shopName.replaceAll("\"", "'")+"\"," +
								"\""+address.replaceAll("\"", "'")+"\",";
		
					if(address2 != null && !address2.equals("")) statement += "\""+address2.replaceAll("\"", "'")+"\",";
					
					else statement += "\"\",";
					
					statement += "\""+phone+"\"," +
								"\""+shopContact.replaceAll("\"", "'")+"\"," +
								"\""+fax+"\"," +
								"\""+email.replaceAll("\"", "'")+"\"," +
								""+Integer.valueOf(existingZipId)+"," +
								""+Integer.valueOf(existingCityId)+"," +
								"now()," +
								"1," +
								"now()," +
								"1);";

		mySQL.executeStatement(statement);
		String shopId = getId();
		
		shopsByPhone.put(phone,shopId);
		shopsByAddress.put(address.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase(), shopId);
		
		return shopId;
	}
	
	private void updateShop(String shopName, String address, String address2, String phone, String shopContact, String fax, 
			 String existingZipId, String existingCityId, String existingShopId, String email){
		
		String statement = "UPDATE "+databaseName+".shop SET " +
							  "ShopName = \""+shopName.replaceAll("\"", "'")+"\"" +
							  ",ShopAddress1 = \""+address.replaceAll("\"", "'")+"\"";
		
				 if(address2 != null && !address2.equals(""))statement += ",ShopAddress2 = \""+address2.replaceAll("\"", "'")+"\"";
				 
				 statement += ",ShopPhone = \""+phone+"\"" +
				 
							  ",ShopContact = \""+shopContact.replaceAll("\"", "'")+"\"" +
							  ",ShopFax = \""+fax+"\"" +
							  ",ShopEmail = \""+email.replaceAll("\"", "'")+"\"" +
							  ",Zip_ID = "+Integer.valueOf(existingZipId)+"" +
							  ",City_ID = "+Integer.valueOf(existingCityId)+"" +
							  ",ModifiedDate = now()" +
							  ",ModifiedUser_ID = 1 " +
							  "WHERE Shop_ID = "+Integer.valueOf(existingShopId)+";";

		mySQL.executeStatement(statement);
	}
	
	private String uploadNetwork(String shopId, String shopCode, String commMethod, String status, String openSunday){
		
		String statement = "INSERT INTO "+databaseName+".shopnetwork " +
						  		"(Shop_ID," +
						  		"Network_ID," +
						  		"ShopCode," +
						  		"ShopNetworkStatus_ID," +
						  		"CommMethod_ID," +
						  		"OpenSunday," +
						  		"CreatedDate," +
						  		"Created_User_ID," +
						  		"ModifiedDate," +
						  		"ModifiedUser_ID) " +
						  		"VALUES ("+
						  		""+Integer.valueOf(shopId)+"," +
						  		"1,\""+
						  		shopCode+"\",";
						  if(status.equals("A"))		
							  statement += "1,";
						  else
							  statement+= "2,";
  
						  if(commMethod.equals("1")){ //email
							  statement += "4,";
						  }
						  else if (commMethod.equals("3") || commMethod.equals("7")){ //fax or temp fax
							  statement += "3,";
							  						  }
						  else if (commMethod.equals("4")){ // FSI
							  statement += "1,";
							  						  }
						  else if (commMethod.equals("10")){ // TLO
							  statement += "2,";
							  						  }
						  else {
							  statement +=  "5,";
						  }
						  if(openSunday.equals("Y")){
							  statement += "1,";
						  }
						  else {
							  statement +=  "0,";
						  }
	  									  
	  									  
	  					  statement +=  "now()," +
	  									"1," +
	  									"now()," +
	  									"1);";
	 
	  mySQL.executeStatement(statement);
	 
	  String networkId = getId();
	 
	  shopNetwork.put(shopId+","+"1", networkId);
	  
	  existingShopCodes.put(shopCode, shopId);
	  
	  return networkId;
	}
	
	private void updateNetwork(String shopCode, String commMethod, String openSunday, String status, String networkId){
		
		String statement = "UPDATE "+databaseName+".shopnetwork SET " +
							"CommMethod_ID=";
		
							if(commMethod.equals("1")){ //email
								  statement += "4,";
							  }
							  else if (commMethod.equals("3") || commMethod.equals("7")){ //fax or temp fax
								  statement += "3,";
								  						  }
							  else if (commMethod.equals("4")){ // FSI
								  statement += "1,";
								  						  }
							  else if (commMethod.equals("10")){ // TLO
								  statement += "2,";
								  						  }
							  else {
								  statement +=  "5,";
							  }
							
							  statement += "OpenSunday=";
							  
							  if(openSunday.equals("Y")){
								  statement += "1,";
							  }
							  else {
								  statement +=  "0,";
							  }
							  
							  statement += "ShopNetworkStatus_ID=";
							  
							  if(status.equals("A"))
								  statement += "1,";
							  else
								  statement += "2,";
								  
							  statement += "ModifiedDate = now()," +
								  		   "ModifiedUser_ID = 1, shopCode = \""+shopCode+"\" ";
	  
							  statement += "WHERE ShopNetwork_ID = "+Integer.valueOf(networkId)+";";
	  
	  mySQL.executeStatement(statement);
	}
	
	private void uploadCoverage(ArrayList<String> listCoverages, String networkId){
			  	 
	  for(int ii=0; ii<listCoverages.size(); ii++){
		  String zip_id = zipId.get(listCoverages.get(ii));	
		  
		  if(zip_id != null){
			  
			  if(shopNetworkCoverage.get(networkId+","+zip_id) == null){
				  
				  String statement = "INSERT INTO "+databaseName+".shopnetworkcoverage " +
				  		"(ShopNetwork_ID," +
				  		"Zip_ID," +
				  		"CreatedDate," +
				  		"CreatedUser_ID," +
				  		"ModifiedDate," +
				  		"ModifiedUser_ID) " +
				  		"VALUES (" +
		  				""+Integer.valueOf(networkId)+","+
		  				zip_id+"" +
		  				",now()," +
		  				"1," +
		  				"now()," +
		  				"1);";
				  
				  mySQL.executeStatement(statement);
				  
				  shopNetworkCoverage.put(networkId+","+zip_id,1);
			  }
		  }
	  }
	  
	}
	
	public void parseBMTData(){
		
		String shopCode,
		shopName,
		shopContact,
		address1,address2,
		city,state,zip,
		commMethod,phone,fax,email,status,
		coveredZips,openSunday;
		
		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else if(file != null)fstream = new FileInputStream(file);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  while((strLine = br.readLine()) != null){
			  
			  ++count;
			  
			  ArrayList<String> listCoveredZips = new ArrayList<String>();
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);
			  }
			  
			  String myArray[] = new String[16];
			  myArray = strLine.split("\t");
			  shopCode = myArray[0];
			  shopName = myArray[1];
			  shopContact = myArray[2];
			  address1 = myArray[3];
			  address2 = myArray[4];
			  city = myArray[5];
			  state = myArray[6];
			  zip = myArray[7].replaceAll(" ", "");
			  phone = myArray[9];
			  fax = myArray[10];
			  email = myArray[11];
			  status = myArray[12];
			  commMethod = myArray[13];
			  openSunday = myArray[14];
			  if(myArray.length == 16) {
				coveredZips = myArray[15];
				String[] temp = coveredZips.split(",");
				for(int ii=0; ii<temp.length; ++ii)	listCoveredZips.add(temp[ii]);
			  }
			  
			  if(cityId.get(city.toUpperCase()+","+state.toUpperCase()) != null){
					  
				  if(zipId.get(zip) != null){
					  
					  String existingCityId = cityId.get(city.toUpperCase()+","+state.toUpperCase());
					  String existingZipId = zipId.get(zip);
					  allFileShopCodes.put(shopCode,1);
					    
					  LinkedList<String> coverages = new LinkedList<String>();
						 
					  for(int ii=0; ii<listCoveredZips.size(); ii++){
						  coverages.add(listCoveredZips.get(ii));
					  }
					  
					  shopCoverages.put(shopCode, coverages);

					  String existingShopId = "";
					  
					  if(!phone.equals("") && shopsByPhone.get(phone)!= null) existingShopId = shopsByPhone.get(phone);
					  
					  else if(!address1.equals("") && shopsByAddress.get(address1.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase()) != null)
						  existingShopId = shopsByAddress.get(address1.toUpperCase()+","+city.toUpperCase()+","+state.toUpperCase());
					 
					  
					  if(existingShopCodes.get(shopCode) != null){
						  
						  existingShopId = existingShopCodes.get(shopCode);
						  String networkId = "";
						  if(uniqueShops.get(shopCode) == null){
							  updateShop(shopName, address1, address2, phone, shopContact, fax, existingZipId, existingCityId, existingShopId, email);
						  	  networkId = shopNetwork.get(existingShopId+","+"1");
						  	  try{
						  		  if(networkId != null && !networkId.equals("")){
						  			  if(phoneNumbersSeen.get(phone) != null)
						  				  uploadNetwork(existingShopId, shopCode, commMethod, status, openSunday);
						  			  else
						  				  updateNetwork(shopCode, commMethod, openSunday, status, networkId);
						  			  uploadCoverage(listCoveredZips, networkId);
						  		  }
						  		  
						  	  }catch(Exception ee){
						  		  System.out.println("Error Occurred");
						  		 // ee.printStackTrace();
						  	  }
						  }
						  else{
							  networkId = shopNetwork.get(existingShopId+","+"1");
							  if(phoneNumbersSeen.get(phone) != null)
				  				  uploadNetwork(existingShopId, shopCode, commMethod, status, openSunday);
				  			  else
				  				  updateNetwork(shopCode, commMethod, openSunday, status, networkId);
							  uploadCoverage(listCoveredZips, networkId);
						  }
						  
						  
					  }else if(!existingShopId.equals("") && shopNetwork.get(existingShopId+","+"1") != null){
						  
						  updateShop(shopName, address1, address2, phone, shopContact, fax, existingZipId, existingCityId, existingShopId, email);
					  	  String networkId = shopNetwork.get(existingShopId+","+"1");
					  	  if(phoneNumbersSeen.get(phone) != null)
			  				  uploadNetwork(existingShopId, shopCode, commMethod, status, openSunday);
			  			  else
			  				  updateNetwork(shopCode, commMethod, openSunday, status, networkId);
					  	  uploadCoverage(listCoveredZips, networkId);
						  
					  }else if(!existingShopId.equals("")){
						  
						  updateShop(shopName, address1, address2, phone, shopContact, fax, existingZipId, existingCityId, existingShopId, email);
						  
						  String networkId = uploadNetwork(existingShopId, shopCode, commMethod, status, openSunday);
						  
						  uploadCoverage(listCoveredZips, networkId);
						  
					  }else{
							  
						  String shopId = uploadShop(shopName, address1, address2, phone, shopContact, fax, 
								  existingZipId, existingCityId, city, state, email);
						  
						  String networkId = uploadNetwork(shopId, shopCode, commMethod, status, openSunday);
						  
						  uploadCoverage(listCoveredZips, networkId);
						  
						  }
			  	}else{
			  		if(!uniqueBadData.contains(zip)){
						  out.write("No zip was found for: "+zip+" for shop: "+shopCode+"\n");
						  uniqueBadData.add(zip);
					  }
			  	}
			  }else{
				  if(uniqueShops.get(shopCode) == null){
					  reUploadOut.write(strLine+"\n");
				  }
				  if(!uniqueBadData.contains(city+","+state)){
					  out.write("No city was found for: "+city+", "+state+" for shop: "+shopCode+"\n");
					  uniqueBadData.add(city+","+state);
				  }
			  }  
			  uniqueShops.put(shopCode, 1);
			  phoneNumbersSeen.put(phone, 1);
		  }
		  
		  in.close();
		  out.close();
		  errorOut.close();
		  shopCodesOut.close();
		  reUploadOut.close();
		  mySQL.closeFile();
		  
		}catch(Exception ee){
			ee.printStackTrace();
			
			try {
				errorOut.write(ee.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
		
		try {
			setShopsToUpdate("1");
		} catch (SQLException e) {
			e.printStackTrace();
			try {
				errorOut.write(e.getMessage()+"\n");
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
	
	private void updateAvailability(){
		
		System.out.println("Updating shop active/inactive statuses");
		
		for(int ii=0; ii<shopsToDeactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
							"SET ShopNetworkStatus_ID = \"2\" " +
							"WHERE ShopCode = \""+shopsToDeactivate.get(ii)+"\"";
			mySQL.executeStatement(statement);
		}
		
		for(int ii=0; ii<shopsToReactivate.size(); ++ii){
			
			String statement = "UPDATE "+databaseName+".shopnetwork " +
						   "SET ShopNetworkStatus_ID = \"1\" " +
						   "WHERE ShopCode = \""+shopsToReactivate.get(ii)+"\"";
			
			mySQL.executeStatement(statement);
		}
		
		System.out.println("Finished updating shop active/inactive statuses");
	}
	
}
