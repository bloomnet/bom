package com.bloomnet.bom.upload.tools;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import org.apache.http.client.ClientProtocolException;
import jxl.Sheet;
import jxl.Workbook;

public class GetIPAddressesAndStatus {
	
	
	private FileOutputStream fos;
	private OutputStreamWriter out;
	private Map<String,String> knownIPs = new HashMap<String,String>();
	private List<String> sitesRetried = new ArrayList<String>();
	
	private static String getIPAddress(String URL) throws UnknownHostException {
	    try{
	    	InetAddress inetAddress = InetAddress.getByName(URL);
	    	return inetAddress.getHostAddress();
	    }catch(Exception ee){
	    	return "No IP Found";
	    }
	}
	
	private boolean[] isSiteUp(String site) throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, ClientProtocolException, IOException {

		try {
			site = site.toLowerCase();
			URL website = new URL("http://"+site);
			HttpURLConnection conn = (HttpURLConnection) website.openConnection();
			conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
            String redirect = conn.getHeaderField("Location");
            int count = 0;
            while(redirect != null){
            	site = redirect.replaceAll("http://", "").replaceAll("https://", "");
            	if(site.contains(".com/")) site = site.split(".com/")[0]+".com";
            	if(site.contains(".net/")) site = site.split(".net/")[0]+".net";
            	redirect = site;
            	website = new URL(redirect);
            	conn = (HttpURLConnection) website.openConnection();
            	try{
    				conn.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
    			}catch(Exception ee){}
            	
            	if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                    return new boolean[] {true,false};
                }
            	count++;
            	if(count == 10) break;
            }
            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                return new boolean[] {true,false};
            }
		}catch(Exception ee){}
            try{
            	TrustManager[] trustAllCerts = new TrustManager[]{
            		    new X509TrustManager() {
            		        public java.security.cert.X509Certificate[] getAcceptedIssuers() {
            		            return null;
            		        }
            		        public void checkClientTrusted(
            		            java.security.cert.X509Certificate[] certs, String authType) {
            		        }
            		        public void checkServerTrusted(
            		            java.security.cert.X509Certificate[] certs, String authType) {
            		        }
            		    }
            		};
        		try {
        		    SSLContext sc = SSLContext.getInstance("TLSv1.2");
        		    sc.init(null, trustAllCerts, new java.security.SecureRandom());
        		    HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        		} catch (Exception e) {
        		}
        		URL website = new URL("https://"+site);
        		HttpURLConnection conn2 = (HttpURLConnection) website.openConnection();
        		
    			try{
    				conn2.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
    			}catch(Exception ee){}
        		String redirect = conn2.getHeaderField("Location");
        		if(redirect != null && redirect.contains(".com/")) redirect = redirect.split(".com/")[0]+".com";
            	if(redirect != null && redirect.contains(".net/")) redirect = redirect.split(".net/")[0]+".net";
        		BufferedReader br = null;
        		StringBuilder sb = null;
        		
        		 int count = 0;
        		while (redirect != null){
        			HttpURLConnection conn3 = (HttpURLConnection) new URL(redirect.toLowerCase()).openConnection();
        			try{
        				conn3.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        			}catch(Exception ee){}
        			if (conn3.getResponseCode() == HttpURLConnection.HTTP_OK) {
        				br = new BufferedReader(new InputStreamReader((conn3.getInputStream())));
        				sb = new StringBuilder();
        				String output;
        				while ((output = br.readLine()) != null) {
        				  sb.append(output);
        				}
        				if(!sb.toString().contains("Sorry, the page you requested was not found on our server")) return new boolean[] {true,true};
                    }
        			redirect = conn3.getHeaderField("Location").toLowerCase();
        			if(redirect.contains(".com/")) redirect = redirect.split(".com/")[0]+".com";
                	if(redirect.contains(".net/")) redirect = redirect.split(".net/")[0]+".net";
                	
                	count++;
                	
                	if(count == 10) break;
        		}

                if (conn2.getResponseCode() == HttpURLConnection.HTTP_OK) {
                	br = new BufferedReader(new InputStreamReader((conn2.getInputStream())));
                	sb = new StringBuilder();
                	String output;
                	while ((output = br.readLine()) != null) {
                	  sb.append(output);
                	}
                    if(!sb.toString().contains("Sorry, the page you requested was not found on our server")) return new boolean[] {true,true};
                }else{
                	if(!sitesRetried.contains(site)){
                		sitesRetried.add(site);
                		if(!site.contains("www.")) return isSiteUp("www."+site);
                	}
                }
            }catch(Exception ee){
            	ee.printStackTrace();
            	if(!sitesRetried.contains(site)){
            		sitesRetried.add(site);
            		if(site.contains("www.")) return isSiteUp(site.replaceAll("www.", ""));
            	}
            }
            return new boolean[] {false,false};
      }
	
	public void parseFileAndFindData() throws KeyManagementException, NoSuchAlgorithmException, KeyStoreException, ClientProtocolException, IOException{
		
		setKnownIPs();
		
		String shopCode = "";
		String site = "";
		String ipAddress = "";
		String hostingCompany = "";
		boolean isUp = false;
		boolean hasSSL = false;
		
		fos = new FileOutputStream("data/results.csv");
		out = new OutputStreamWriter(fos);
		
		out.write("Shop Code,Site,IP Address,Site up?,Has SSL?,Hosting Company\r\n");
		out.flush();
		
		Sheet sheet = openExcelSheet("data/sites.xls");
		
		for (int ii = 0; ii < sheet.getRows(); ++ii){
			
			shopCode = sheet.getCell(1,ii).getContents().toString();
			site = sheet.getCell(2,ii).getContents().toString().replaceAll("https://", "").replaceAll("http://", "").replaceAll("/", "");
			if(!site.contains("www.") && site.split("\\.").length < 3) site = "www." + site;
			ipAddress = getIPAddress(site);
		    boolean[] isUpHasSSL = isSiteUp(site);
			isUp = isUpHasSSL[0];
			hasSSL = isUpHasSSL[1];
			
			if(knownIPs.get(ipAddress) != null )
				hostingCompany = knownIPs.get(ipAddress);
			else if(knownIPs.get(ipAddress.substring(0,10)) != null)
				hostingCompany = knownIPs.get(ipAddress.substring(0,10));
			else if(knownIPs.get(ipAddress.substring(0,9)) != null)
				hostingCompany = knownIPs.get(ipAddress.substring(0,9));
			else if(knownIPs.get(ipAddress.substring(0,8)) != null)
				hostingCompany = knownIPs.get(ipAddress.substring(0,8));
			else
				hostingCompany = "unknown";
			
			out.write(shopCode + "," + site + "," + ipAddress + "," + isUp + "," + hasSSL + "," + hostingCompany + "\r\n");
			out.flush();
		}
		
		out.close();
			
	}
	
	private Sheet openExcelSheet(String filename){
		
		File inputWorkbook = new File(filename);
		Workbook w = null;
		Sheet sheet = null;
		try {
			w = Workbook.getWorkbook(inputWorkbook);
			sheet = w.getSheet(0);
			return sheet;
		}catch(Exception ee){ee.printStackTrace();}
		return null;
	}
	
	private void setKnownIPs(){
		knownIPs.put("184.168.131.241","BloomNation");
		knownIPs.put("52.53.58.195","BloomNation");
		knownIPs.put("52.9.226.41","BloomNation");
		knownIPs.put("108.166.35.248","BloomNet Commerce");
		knownIPs.put("161.47.101","BloomNet Commerce");
		knownIPs.put("161.47.108","BloomNet Commerce");
		knownIPs.put("165.160.13.20","BloomNet Commerce");
		knownIPs.put("166.78.235.117","BloomNet Commerce");
		knownIPs.put("184.106.34.28","BloomNet Commerce");
		knownIPs.put("184.106.42.192","BloomNet Commerce");
		knownIPs.put("198.61.140.79","BloomNet Commerce");
		knownIPs.put("50.63.202","BloomNet Commerce");
		knownIPs.put("13.35.78","Floranext");
		knownIPs.put("12.228.94","FSN");
		knownIPs.put("172.217.12.179","FSN");
		knownIPs.put("64.233.146","FSN");
		knownIPs.put("65.202.58","FTD");
		knownIPs.put("13.35.78","Lovingly");
		knownIPs.put("198.185.159.145","SquareSpace");
		knownIPs.put("167.224.66.16","Teleflora");
		knownIPs.put("64.131.64.148","websystems");
		knownIPs.put("199.34.228.191","weebly");
		
			
	}
	
	public static void main(String[] args){
		try {
			//System.out.println(new GetIPAddressesAndStatus().isSiteUp("1800flowerswaterloo.flowerama.com")[0]);
			new GetIPAddressesAndStatus().parseFileAndFindData();
		}catch(Exception ee){System.out.println(ee.getMessage()); ee.printStackTrace();}
	}
}
