package com.bloomnet.bom.upload.tools;

import java.io.*;
import java.sql.*;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class FOMDailyReport {
	
	SQLData mySQL = new SQLData();
	
	public FOMDailyReport(){
        mySQL.login();
        String statement = "USE bloomnetordermanagement;";
        mySQL.executeStatement(statement);
        System.out.println("INITIALIZED AND DATABASE SELECTED");
        String query = "SELECT ONumber, ShopCode,FirstName,LastName,CreatedDate,DeliveryDate,Occassion,Price,Sts,VQueue FROM ( SELECT SendingShop_ID as shopid, OrderNumber as ONumber, (SELECT shopnetwork.ShopCode FROM shopnetwork WHERE Shop_ID = shopid AND Network_ID = 1) as ShopCode, SUBSTRING(OrderXML,(LOCATE('<recipientFirstName>',OrderXML)+20),(LOCATE('</recipientFirstName>',OrderXML))-LOCATE('<recipientFirstName>',OrderXML)-20) AS FirstName, SUBSTRING(OrderXML,(LOCATE('<recipientLastName>',OrderXML)+19),(LOCATE('</recipientLastName>',OrderXML))-LOCATE('<recipientLastName>',OrderXML)-19) AS LastName, CreatedDate, DeliveryDate, SUBSTRING(OrderXML,(LOCATE('<occasionCode>',OrderXML)+14),(LOCATE('</occasionCode>',OrderXML))-LOCATE('<occasionCode>',OrderXML)-14) AS Occassion, Price, (SELECT Sts_Routing_ID FROM orderactivity INNER JOIN act_routing ON orderactivity.OrderActivity_ID = act_routing.OrderActivity_ID INNER JOIN bomorder ON orderactivity.BOMOrder_ID = bomorder.BOMOrder_ID WHERE bomorder.OrderNumber = ONumber ORDER BY orderactivity.CreatedDate DESC LIMIT 1) AS Sts, (SELECT VirtualQueue FROM orderactivity INNER JOIN act_routing ON orderactivity.OrderActivity_ID = act_routing.OrderActivity_ID INNER JOIN bomorder ON orderactivity.BOMOrder_ID = bomorder.BOMOrder_ID WHERE bomorder.OrderNumber = ONumber ORDER BY orderactivity.CreatedDate DESC LIMIT 1) AS VQueue FROM bomorder WHERE bomorder.CreatedDate >= '20170101' AND bomorder.CreatedDate < CURDATE() + 1 AND bomorder.BOMOrder_ID = bomorder.ParentOrder_ID ) AS FYFOrders;";
        String filePath = "/root/queryautomation/FOMOrdersAll.xlsx";
        
        createWorkbook(filePath, query );
    }
	
	private void createWorkbook(String filePath, String query){
    	try
        {
    		ResultSet queryResults = mySQL.executeQuery(query);
            @SuppressWarnings("resource")
			XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet("Sheet1");
            
            XSSFRow row = sheet.createRow(0);
            
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("Order Number");
            cell = row.createCell(1);
            cell.setCellValue("Shop Code");
            cell = row.createCell(2);
            cell.setCellValue("First Name");
            cell = row.createCell(3);
            cell.setCellValue("Last Name");
            cell = row.createCell(4);
            cell.setCellValue("Created Date");
            cell = row.createCell(5);
            cell.setCellValue("Delivery Date");
            cell = row.createCell(6);
            cell.setCellValue("Occassion");
            cell = row.createCell(7);
            cell.setCellValue("Price");
            cell = row.createCell(8);
            cell.setCellValue("Status");
            cell = row.createCell(9);
            cell.setCellValue("Queue");

    		int ii = 1;
    				
            while(queryResults.next())
            {
            	row = sheet.createRow(ii);
            	
                String orderNum = queryResults.getString("ONumber");
                String shopCode = queryResults.getString("ShopCode");
                String firstName = queryResults.getString("FirstName");
                String lastName = queryResults.getString("LastName");
                String createdDate = queryResults.getString("CreatedDate");
                String deliveryDate = queryResults.getString("DeliveryDate");
                String occassion = queryResults.getString("Occassion");
                String price = queryResults.getString("Price");
                String sts = queryResults.getString("Sts");
                String vQueue = queryResults.getString("VQueue");
                
                String[] createdDateArray = createdDate.split(" ")[0].split("-");
                String[] deliveryDateArray = deliveryDate.split(" ")[0].split("-");
                
                cell = row.createCell(0);
                cell.setCellValue(String.valueOf(orderNum).toString());
                cell = row.createCell(1);
                cell.setCellValue(String.valueOf(shopCode).toString());
                cell = row.createCell(2);
                cell.setCellValue(String.valueOf(firstName).toString());
                cell = row.createCell(3);
                cell.setCellValue(String.valueOf(lastName).toString());
                cell = row.createCell(4);
                cell.setCellValue(createdDateArray[1]+"/"+createdDateArray[2]+"/"+createdDateArray[0]);
                cell = row.createCell(5);
                cell.setCellValue(deliveryDateArray[1]+"/"+deliveryDateArray[2]+"/"+deliveryDateArray[0]);
                cell = row.createCell(6);
                cell.setCellValue(String.valueOf(occassion).toString());
                cell = row.createCell(7);
                cell.setCellValue(String.valueOf(price).toString());
                cell = row.createCell(8);
                cell.setCellValue(String.valueOf(sts).toString());
                cell = row.createCell(9);
                cell.setCellValue(String.valueOf(vQueue).toString());
               
                ii++;
            }
            
            FileOutputStream fileOut = new FileOutputStream(filePath);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();

        }
        catch(IOException e){
            e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally{
			try{
				mySQL.closeFile();
			}catch(Exception ee){}
		}
    }

    public static void main(String args[]){
        System.out.println("HITTING MAIN METHOD");
        new FOMDailyReport();
    }
}
