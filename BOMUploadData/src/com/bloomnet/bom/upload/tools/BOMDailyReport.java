package com.bloomnet.bom.upload.tools;

import java.io.*;
import java.sql.*;

import org.apache.poi.xssf.usermodel.XSSFCell; 
import org.apache.poi.xssf.usermodel.XSSFRow; 
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook; 


public class BOMDailyReport {
	
	SQLData mySQL = new SQLData();

    public BOMDailyReport(){
        mySQL.login();
        String statement = "USE bloomnetordermanagement;";
        mySQL.executeStatement(statement);
        System.out.println("INITIALIZED AND DATABASE SELECTED");
        String query = "SELECT ONumber, ShopCode,FirstName,LastName,CreatedDate,DeliveryDate,Occassion,Price,Sts,VQueue,ReceivingShop,CityName,StateName FROM ( SELECT rs.ShopName AS ReceivingShop,city.Name AS CityName,state.Short_name AS StateName,bomorder.SendingShop_ID as shopid, bomorder.OrderNumber as ONumber, (SELECT shopnetwork.ShopCode FROM shopnetwork WHERE Shop_ID = shopid AND Network_ID = 1 LIMIT 1) as ShopCode, SUBSTRING(bomorder.OrderXML,(LOCATE('<recipientFirstName>',bomorder.OrderXML)+20),(LOCATE('</recipientFirstName>',bomorder.OrderXML))-LOCATE('<recipientFirstName>',bomorder.OrderXML)-20) AS FirstName, SUBSTRING(bomorder.OrderXML,(LOCATE('<recipientLastName>',bomorder.OrderXML)+19),(LOCATE('</recipientLastName>',bomorder.OrderXML))-LOCATE('<recipientLastName>',bomorder.OrderXML)-19) AS LastName, bomorder.CreatedDate, bomorder.DeliveryDate, SUBSTRING(bomorder.OrderXML,(LOCATE('<occasionCode>',bomorder.OrderXML)+14),(LOCATE('</occasionCode>',bomorder.OrderXML))-LOCATE('<occasionCode>',bomorder.OrderXML)-14) AS Occassion, bomorder.Price, (SELECT Sts_Routing_ID FROM orderactivity INNER JOIN act_routing ON orderactivity.OrderActivity_ID = act_routing.OrderActivity_ID INNER JOIN bomorder ON orderactivity.BOMOrder_ID = bomorder.BOMOrder_ID WHERE bomorder.OrderNumber = ONumber ORDER BY orderactivity.CreatedDate DESC LIMIT 1) AS Sts, (SELECT VirtualQueue FROM orderactivity INNER JOIN act_routing ON orderactivity.OrderActivity_ID = act_routing.OrderActivity_ID INNER JOIN bomorder ON orderactivity.BOMOrder_ID = bomorder.BOMOrder_ID WHERE bomorder.OrderNumber = ONumber ORDER BY orderactivity.CreatedDate DESC LIMIT 1) AS VQueue FROM bomorder LEFT JOIN city ON bomorder.City_ID = city.City_ID LEFT JOIN state ON city.State_ID = state.State_ID LEFT JOIN bomorder_sts ON bomorder.ParentOrder_ID = bomorder_sts.BOMOrder_ID LEFT JOIN bomorder bos ON bomorder_sts.activechild_id = bos.BOMOrder_ID LEFT JOIN shop rs ON bos.ReceivingShop_ID = rs.Shop_ID WHERE bomorder.CreatedDate >= CONCAT(Year(CURDATE()),'-01-01') AND bomorder.BOMOrder_ID = bomorder.ParentOrder_ID AND bomorder.SendingShop_ID != '19612' ) AS FYFOrders;";
        String queryEverything = "SELECT ONumber, ShopCode,FirstName,LastName,CreatedDate,DeliveryDate,Occassion,Price,Sts,VQueue,ReceivingShop,CityName,StateName FROM ( SELECT rs.ShopName AS ReceivingShop,city.Name AS CityName,state.Short_name AS StateName,bomorder.SendingShop_ID as shopid, bomorder.OrderNumber as ONumber, (SELECT shopnetwork.ShopCode FROM shopnetwork WHERE Shop_ID = shopid AND Network_ID = 1 LIMIT 1) as ShopCode, SUBSTRING(bomorder.OrderXML,(LOCATE('<recipientFirstName>',bomorder.OrderXML)+20),(LOCATE('</recipientFirstName>',bomorder.OrderXML))-LOCATE('<recipientFirstName>',bomorder.OrderXML)-20) AS FirstName, SUBSTRING(bomorder.OrderXML,(LOCATE('<recipientLastName>',bomorder.OrderXML)+19),(LOCATE('</recipientLastName>',bomorder.OrderXML))-LOCATE('<recipientLastName>',bomorder.OrderXML)-19) AS LastName, bomorder.CreatedDate, bomorder.DeliveryDate, SUBSTRING(bomorder.OrderXML,(LOCATE('<occasionCode>',bomorder.OrderXML)+14),(LOCATE('</occasionCode>',bomorder.OrderXML))-LOCATE('<occasionCode>',bomorder.OrderXML)-14) AS Occassion, bomorder.Price, (SELECT Sts_Routing_ID FROM orderactivity INNER JOIN act_routing ON orderactivity.OrderActivity_ID = act_routing.OrderActivity_ID INNER JOIN bomorder ON orderactivity.BOMOrder_ID = bomorder.BOMOrder_ID WHERE bomorder.OrderNumber = ONumber ORDER BY orderactivity.CreatedDate DESC LIMIT 1) AS Sts, (SELECT VirtualQueue FROM orderactivity INNER JOIN act_routing ON orderactivity.OrderActivity_ID = act_routing.OrderActivity_ID INNER JOIN bomorder ON orderactivity.BOMOrder_ID = bomorder.BOMOrder_ID WHERE bomorder.OrderNumber = ONumber ORDER BY orderactivity.CreatedDate DESC LIMIT 1) AS VQueue FROM bomorder LEFT JOIN city ON bomorder.City_ID = city.City_ID LEFT JOIN state ON city.State_ID = state.State_ID LEFT JOIN bomorder_sts ON bomorder.ParentOrder_ID = bomorder_sts.BOMOrder_ID LEFT JOIN bomorder bos ON bomorder_sts.activechild_id = bos.BOMOrder_ID LEFT JOIN shop rs ON bos.ReceivingShop_ID = rs.Shop_ID WHERE bomorder.CreatedDate >= CONCAT(Year(CURDATE()),'-01-01') AND bomorder.BOMOrder_ID = bomorder.ParentOrder_ID ) AS AllOrders;";
        String filePath = "/root/automatedqueries/FYFqueryresults.xlsx";
        String filePath2 = "/root/automatedqueries/EVERYTHINGqueryresults.xlsx";
        
        createWorkbook(filePath, query);
        createWorkbook(filePath2, queryEverything);
    }
    
    private void createWorkbook(String filePath, String query){
    	try
        {
    		ResultSet queryResults = mySQL.executeQuery(query);
            @SuppressWarnings("resource")
			XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet("Sheet1");
            
            XSSFRow row = sheet.createRow(0);
            
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("Order Number");
            cell = row.createCell(1);
            cell.setCellValue("Shop Code");
            cell = row.createCell(2);
            cell.setCellValue("First Name");
            cell = row.createCell(3);
            cell.setCellValue("Last Name");
            cell = row.createCell(4);
            cell.setCellValue("Created Date");
            cell = row.createCell(5);
            cell.setCellValue("Delivery Date");
            cell = row.createCell(6);
            cell.setCellValue("Occassion");
            cell = row.createCell(7);
            cell.setCellValue("Price");
            cell = row.createCell(8);
            cell.setCellValue("Status");
            cell = row.createCell(9);
            cell.setCellValue("Queue");
            cell = row.createCell(10);
            cell.setCellValue("Receiving Shop");
            cell = row.createCell(11);
            cell.setCellValue("Recipient City");
            cell = row.createCell(12);
            cell.setCellValue("Recipient State");

    		int ii = 1;
    				
            while(queryResults.next())
            {
            	row = sheet.createRow(ii);
            	
                String orderNum = queryResults.getString("ONumber");
                String shopCode = queryResults.getString("ShopCode");
                String firstName = queryResults.getString("FirstName");
                String lastName = queryResults.getString("LastName");
                String createdDate = queryResults.getString("CreatedDate");
                String deliveryDate = queryResults.getString("DeliveryDate");
                String occassion = queryResults.getString("Occassion");
                String price = queryResults.getString("Price");
                String sts = queryResults.getString("Sts");
                String vQueue = queryResults.getString("VQueue");
                String receivingShop = queryResults.getString("ReceivingShop");
                String city = queryResults.getString("CityName");
                String state = queryResults.getString("StateName");
                
                String[] createdDateArray = createdDate.split(" ")[0].split("-");
                String[] deliveryDateArray = deliveryDate.split(" ")[0].split("-");
                
                cell = row.createCell(0);
                cell.setCellValue(String.valueOf(orderNum).toString());
                cell = row.createCell(1);
                cell.setCellValue(String.valueOf(shopCode).toString());
                cell = row.createCell(2);
                cell.setCellValue(String.valueOf(firstName).toString());
                cell = row.createCell(3);
                cell.setCellValue(String.valueOf(lastName).toString());
                cell = row.createCell(4);
                cell.setCellValue(createdDateArray[1]+"/"+createdDateArray[2]+"/"+createdDateArray[0]);
                cell = row.createCell(5);
                cell.setCellValue(deliveryDateArray[1]+"/"+deliveryDateArray[2]+"/"+deliveryDateArray[0]);
                cell = row.createCell(6);
                cell.setCellValue(String.valueOf(occassion).toString());
                cell = row.createCell(7);
                cell.setCellValue(String.valueOf(price).toString());
                cell = row.createCell(8);
                cell.setCellValue(String.valueOf(sts).toString().replaceAll("13", "Waiting For Response").replaceAll("33","Waiting for Time Zone / DLC").replaceAll("16", "Order Completed").replaceAll("12", "Cancel Denied By BloomNet").replaceAll("15", "Order Worked Outside Of BOM").replaceAll("18", "Message To Be Worked").replaceAll("8", "Rejected By Shop").replaceAll("7", "Delivery Attempted By Shop").replaceAll("2", "Actively Being Worked").replaceAll("3", "Sent To Shop").replaceAll("4", "Accepted By Shop").replaceAll("6", "Delivered By Shop").replaceAll("9", "Rejected By BloomNet").replaceAll("11", "Cancelled Confirmed By BloomNet").replaceAll("10", "Cancelled By Sending Shop").replaceAll("1", "To Be Worked"));
                cell = row.createCell(9);
                cell.setCellValue(String.valueOf(vQueue).toString());
                cell = row.createCell(10);
                cell.setCellValue(String.valueOf(receivingShop).toString().replaceAll("null", "N/A"));
                cell = row.createCell(11);
                cell.setCellValue(String.valueOf(city).toString());
                cell = row.createCell(12);
                cell.setCellValue(String.valueOf(state).toString());
               
                ii++;
            }
            
            FileOutputStream fileOut = new FileOutputStream(filePath);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();
        }
        catch(IOException e){
            e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }


    public static void main(String args[]){
    	System.out.println("HITTING MAIN METHOD");
    	new BOMDailyReport();
    }
}
