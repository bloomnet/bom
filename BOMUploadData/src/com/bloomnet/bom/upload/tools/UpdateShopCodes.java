package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;

public class UpdateShopCodes {
	
	private SQLData mySQL;
	private String fileName;
	private FileInputStream fstream;
	private String databaseName;
	
	public UpdateShopCodes(String fileName, String network, String databaseName){
		
		this.databaseName = databaseName;
		
		this.fileName = fileName;
		
		mySQL = new SQLData();
		mySQL.login();
		
		updateShopCodes(network);
	}
	
	private void updateShopCodes(String network){
		

		if(fileName != null){
			try {
				fstream = new FileInputStream(fileName);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}else{
			 System.err.println("No file to parse");
			 return;
		 }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  System.out.println("Updating Shop Codes");
		  try {
			while((strLine = br.readLine()) != null){
				
			  String shopId = strLine.split(",")[0];
			  String shopCode = strLine.split(",")[1];
			  
			  String statement = "UPDATE "+databaseName+".shopnetwork SET ShopCode = '"+shopCode+"', ShopNetworkStatus_ID = '1' " +
			  		"WHERE Shop_ID = '"+shopId+"' AND Network_ID = '"+network+"'";
			  
			  mySQL.executeStatement(statement);
			  
			  }
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.out.println("Finished Updating Shop Codes");
	}
}
