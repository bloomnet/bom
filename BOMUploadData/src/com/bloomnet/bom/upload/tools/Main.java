package com.bloomnet.bom.upload.tools;
import java.util.Scanner;

public class Main {
	
	String input = "";
	Scanner user_input = new Scanner( System.in );
	
	public static void main(String[] args){
		Main main = new Main();
		main.UI();
	}
	
	private void UI(){
		
		System.out.println("\nPlease enter the Database Name: \n");
		String databaseName = user_input.next();
		
		doNext();
		
		while(true){
			try{
				switch (Integer.valueOf(input)){
					
					case 0: input = "0";
							System.out.println("Exiting!");
							System.exit(0);
				 
					case 1: input = "1";
							new UploadZips(databaseName);
							new UploadBMTShops("data/bmt.txt", databaseName);
							new UploadTeleflora("data/combodod.dat",databaseName);
							new UploadFTD("data/ftd.csv",databaseName);
							doNext();
							break;
							
					case 2: input = "2";
							new UploadZips(databaseName);
							doNext();
							break;
							
					case 3: input = "3";
							new UploadBMTShops("data/bmt.txt",databaseName);
							new UploadTeleflora("data/combodod.dat",databaseName);
							new UploadFTD("data/ftd.csv",databaseName);
							doNext();
							break;
							
					case 4: input = "4";
							new UploadBMTShops("data/bmt.txt",databaseName);
							doNext();
							break;
							
					case 5: input = "5";
							new UploadTeleflora("data/combodod.dat",databaseName);
							doNext();
							break;
							
					case 6: input = "6";
							new UploadFTD("data/ftd.csv",databaseName);
							doNext();
							break;
					
					case 7: input = "7";
							new UploadBMTShops("data/BMTReUpload.txt","", databaseName);
							doNext();
							break;
							
					case 8: input = "8";
							new UpdateShopCodes("data/BMTNewShopCodes.txt","1", databaseName);
							doNext();
							break;
					
					case 9: input = "9";
							new UploadTeleflora("data/TFReUpload.txt", "", databaseName);
							doNext();
							break;
							
					case 10: input = "10";
							new UpdateShopCodes("data/TFNewShopCodes.txt","2", databaseName);
							doNext();
							break;
							
					case 11: input = "11";
							new UploadFTD("data/FTDReUpload.txt", "", databaseName);
							doNext();
							break;
					
					case 12: input = "12";
							new UpdateShopCodes("data/FTDNewShopCodes.txt","2", databaseName);
							doNext();
							break;
					
					case 13: input = "13";
					 		 new UploadPopulationData("data/populationData.csv", databaseName);
					 		 doNext();
					 		 break;
					 		 
					case 14: input = "14";
					 		 new UploadExistingLeads(databaseName);
					 		 doNext();
					 		 break;
					 		 
					case 15: input = "15";
							new UploadBN("data/BN.xls",databaseName);
							doNext();
							break;
							
					case 16: input = "16";
							new UploadFSN("data/FSN.xls",databaseName);
							doNext();
							break;
							
					case 17: input = "17";
						new UploadParticipationIndex("data/participation.xls",databaseName);
						doNext();
					break;
					
					case 18: input = "18";
						new UploadNewFBSValues("data/fbs.xls",databaseName);
						doNext();
						break;
						
					case 19: input = "19";
						new UploadTRPData("data/trp.csv", "data/rescind.csv", databaseName);
						doNext();
						break;
						
					case 20: input = "20";
						new UploadTRPCompiledData("data/trpCompiled.xls", "data/rescindCompiled.xls", databaseName);
						doNext();
						break;
						
					case 21: input = "21";
						new UploadTRPBilling(databaseName);
						doNext();
						break;
						
					case 22: input = "22";
						new UploadInternationalDO("data/worldcitiespop.csv", "data/countryCodes.csv", "data/internationalShops.csv", databaseName);
						doNext();
						break;
						
					case 23: input = "23";
					new UploadInternationalBOM("data/worldcitiespop.csv", "data/countryCodes.csv", "data/internationalShops.csv", databaseName);
					doNext();
					break;
					
					case 24: input = "24";
					new GetTelefloraCoverageZips("data/combodod.dat", databaseName);
					doNext();
					break;
					
					case 25: input = "25";
					new GetFTDCoverageZips("data/FTDData.csv", databaseName);
					doNext();
					break;
					
					
					default: System.out.println("Invalid Selection. Please choose a valid selection...");
							 input = user_input.next();
							 break;
							
				}
			}catch(Exception ee){
				System.err.println("\n\nFailed to proccess your request because: " + ee.getStackTrace()[0].toString());
				doNext();
			}
		}
	}
	
	private void doNext(){
		System.out.print("\n\nPlease select what you want to do: \n \n" +
				"    Enter 1 to upload all city/state/zip data as well as BMT, TF, and FTD shops \n" +
				"    Enter 2 to upload only city/state/zip data \n" +
				"    Enter 3 to upload only BMT, TF, and FTD shops \n" +
				"    Enter 4 to upload only BMT shops \n" +
				"    Enter 5 to upload only TF shops \n" +
				"    Enter 6 to upload only FTD shops \n" +
				"    Enter 7 to re-upload erroneous BMT data \n" +
				"    Enter 8 to update BMT shop codes \n" +
				"    Enter 9 to re-upload erroneous TF data \n" +
				"    Enter 10 to update TF shop codes \n" +
				"    Enter 11 to re-upload erroneous FTD data \n" +
				"    Enter 12 to update FTD shop codes \n" +
				"    Enter 13 to upload population data \n" +
				"    Enter 14 to upload existing leads data \n" +
				"    Enter 15 to upload BN Shop Data \n" +
				"    Enter 16 to upload FSN Shop Data \n" +
				"    Enter 17 to upload Participation Index Shop Data \n" +
				"    Enter 18 to upload new FBS values \n" +
				"    Enter 19 to upload TRP Data \n" +
				"    Enter 20 to upload Compiled TRP Data \n" +
				"    Enter 21 to process outstanding TRP data \n" +
				"    Enter 22 to upload International DO Data \n" +
				"    Enter 23 to upload International BOM Data \n" +
				"    Enter 24 to write a Teleflora zip coverage file \n" +
				"    Enter 25 to write an FTD zip coverage file \n" +
				"    Enter 0 to Exit \n\n");
		input = user_input.next();
	}
}


