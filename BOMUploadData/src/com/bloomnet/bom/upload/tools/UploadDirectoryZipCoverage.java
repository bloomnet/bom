package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;

public class UploadDirectoryZipCoverage {
	
	SQLData mySQL = new SQLData();
	
	public UploadDirectoryZipCoverage(){
		uploadDirectoryZipCoverage("data/bmt.txt");
	}
	
	private void insertShopZip(String shopCode, String zip){
		String statement = "INSERT INTO zip_listing(shop_code,zip_code,active) VALUES(\""+shopCode+"\",\""+zip+"\",1)";
		mySQL.executeStatement(statement);
	}
	
	private void uploadDirectoryZipCoverage(String fileName){
		
		mySQL.login();
		
		String statement = "USE bloomnet;";
		mySQL.executeStatement(statement);
		
		String shopCode, coveredZips;
		
		FileInputStream fstream;
		
		try {
		  
		  if(fileName != null) fstream = new FileInputStream(fileName);
		  
		  else{
			  System.err.println("No file to parse");
			  return;
		  }
		 
		  DataInputStream in = new DataInputStream(fstream);
		  @SuppressWarnings("resource")
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		  String strLine;
		  
		  int count=0;
		  
		  while((strLine = br.readLine()) != null){
			  
			  ++count;
			  
			  if(count%100 == 0){
				  String lineNumber = String.valueOf(count);
			  
				  System.out.println("Uploading data line "+lineNumber);
			  }
			  
			  String myArray[] = new String[16];
			  myArray = strLine.split("\t");
			  shopCode = myArray[0];
			  if(myArray.length == 16) {
				coveredZips = myArray[15];
				String[] temp = coveredZips.split(",");
				for(int ii=0; ii<temp.length; ++ii){
					insertShopZip(shopCode, temp[ii]);
				}
			  }
		  }
		}catch(Exception ee){
			ee.printStackTrace();
		}
		
	}
	
	public static void main(String[] args){
		new UploadDirectoryZipCoverage();
	}
	
}
