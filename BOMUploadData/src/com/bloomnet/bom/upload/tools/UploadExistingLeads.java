package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class UploadExistingLeads {
	
	private HashMap<String,String> shopsByPhone = new HashMap<String,String>();
	private SQLData mySQL;
	private String databaseName = "";
	
	public UploadExistingLeads(String databaseName){
		
		this.databaseName = databaseName;
		mySQL = new SQLData();
		mySQL.login();
		
		try {
			setShopsByPhone();
			uploadExistingLeads();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void setShopsByPhone() throws SQLException{
		
		System.out.println("Seting Shop to Phone References");
		
		String query = "SELECT shop.Shop_ID," +
					   "shop.ShopName," +
					   "shop.ShopPhone " +
					   "FROM "+databaseName+".shop;";
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			if(results.getString("ShopPhone") != null){
				shopsByPhone.put(results.getString("ShopPhone"),results.getString("Shop_ID"));
			}
		}
		
		mySQL.closeStatement();
		
		System.out.println("Finished Seting Shop to Phone References");
	}
	
	private void uploadExistingLeads() throws IOException{
		
	  FileInputStream fstream = new FileInputStream("data/leads.csv");
	 
	  DataInputStream in = new DataInputStream(fstream);
	  BufferedReader br = new BufferedReader(new InputStreamReader(in));
	  String strLine;
	  
	  if((strLine = br.readLine()) != null){
	  }
	  
	  String statement = "USE "+databaseName+";";
	  mySQL.executeStatement(statement);
	  
	  int errorCount = 0;
	  int lineCount = 0;
	  
	  while((strLine = br.readLine()) != null){
		  
		  lineCount++;
		  
		  if(lineCount%100 == 0){
			  System.out.println("Processing Line: "+lineCount);
		  }
		  
		  String[] lineItems = strLine.split(",");
		  String leadId = lineItems[0];
		  String shopPhone = lineItems[20].replaceAll("1 ", "").replaceAll("\\(","").replaceAll("\\)", "").replaceAll("-","").replaceAll(" ", "");
		  String shopId = shopsByPhone.get(shopPhone);
		  if(shopId != null){
			  statement = "INSERT INTO assigned_leads(Shop_ID,CRM_ID) VALUES("+shopId+",\""+leadId+"\");";
			  mySQL.executeStatement(statement);
		  }else{
			  errorCount++;
		  }
		  
	  }
	  br.close();
	  System.out.println(errorCount);
	}
}
