package com.bloomnet.bom.upload.tools;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

public class GetCitiesToFix {
	
	private SQLData mySQL;
	private FileOutputStream fos;
	private OutputStreamWriter out;
	
	public GetCitiesToFix(String[] args){
		mySQL = new SQLData();
		mySQL.login();
		try {
			fixCities();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void fixCities() throws SQLException, IOException{
		try {
			fos = new FileOutputStream("logs/fixTheseCities.txt");
			out = new OutputStreamWriter(fos);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		for(int ii=0; ii<76; ++ii){
			String query = "SELECT * FROM bloomnetordermanagement.city WHERE State_ID = '"+String.valueOf(ii)+"' GROUP BY Name HAVING count(Name) > 1;";
			ResultSet results = mySQL.executeQuery(query);
			
			while(results.next()){
				String cityName = results.getString("Name");
				query = "SELECT * FROM bloomnetordermanagement.city WHERE Name = '"+cityName+"' AND State_ID = '"+String.valueOf(ii)+"';";
				ResultSet results2 = mySQL.executeQuery(query);
				String id1 = "";
				String id2 = "";
				String zip1 = "";
				String zip2 = "";
				String state = "";
				for(int xx=0; xx<2; ++xx){
					results2.next();
					if(xx == 0) id1 = results2.getString("City_ID");
					else id2 = results2.getString("City_ID");
				}
				query = "SELECT zip.Zip_Code FROM bloomnetordermanagement.city_zip_xref INNER JOIN bloomnetordermanagement.zip ON city_zip_xref.Zip_ID = zip.Zip_ID WHERE City_ID = "+id1+";";
				ResultSet results3 = mySQL.executeQuery(query);
				while(results3.next()){
					if(zip1.equals("")) zip1 = results3.getString(1);
				}
				mySQL.closeStatement();
				query = "SELECT zip.Zip_Code FROM bloomnetordermanagement.city_zip_xref INNER JOIN bloomnetordermanagement.zip ON city_zip_xref.Zip_ID = zip.Zip_ID WHERE City_ID = "+id2+";";
				ResultSet results4 = mySQL.executeQuery(query);
				while(results4.next()){
					if(zip2.equals("")) zip2 = results4.getString(1);
				}
				query = "SELECT * FROM bloomnetordermanagement.state WHERE State_ID = "+ii+"";
				ResultSet results5 = mySQL.executeQuery(query);
				while(results5.next()){
					state = results5.getString("Name");
				}
				
				if(zip1.equals("")){
					query = "SELECT zip.Zip_Code FROM bloomnetordermanagement.shop INNER JOIN bloomnetordermanagement.zip ON shop.Zip_ID = zip.Zip_ID WHERE City_ID = "+id1+";";
					ResultSet results6 = mySQL.executeQuery(query);
					while(results6.next()){
						if(zip1.equals("")) zip1 = results6.getString(1);
					}
				}
				if(zip2.equals("")){
					query = "SELECT zip.Zip_Code FROM bloomnetordermanagement.shop INNER JOIN bloomnetordermanagement.zip ON shop.Zip_ID = zip.Zip_ID WHERE City_ID = "+id2+";";
					ResultSet results6 = mySQL.executeQuery(query);
					while(results6.next()){
						if(zip2.equals("")) zip2 = results6.getString(1);
					}
				}
				
				if(zip1.equals("")){
					query = "SELECT zip.Zip_Code FROM bloomnetordermanagement.bomorder INNER JOIN bloomnetordermanagement.zip ON bomorder.Zip_ID = zip.Zip_ID WHERE City_ID = "+id1+";";
					ResultSet results6 = mySQL.executeQuery(query);
					while(results6.next()){
						if(zip1.equals("")) zip1 = results6.getString(1);
					}
				}
				if(zip2.equals("")){
					query = "SELECT zip.Zip_Code FROM bloomnetordermanagement.bomorder INNER JOIN bloomnetordermanagement.zip ON bomorder.Zip_ID = zip.Zip_ID WHERE City_ID = "+id2+";";
					ResultSet results6 = mySQL.executeQuery(query);
					while(results6.next()){
						if(zip2.equals("")) zip2 = results6.getString(1);
					}
				}
				mySQL.closeStatement();
				out.write(id1+","+cityName+","+state+","+zip1+"\n");
				out.write(id2+","+cityName+","+state+","+zip2+"\n");
			}
		}
		out.close();
		fos.close();
	}

}
