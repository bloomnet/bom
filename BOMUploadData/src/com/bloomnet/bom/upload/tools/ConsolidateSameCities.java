package com.bloomnet.bom.upload.tools;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ConsolidateSameCities {
	
private SQLData mySQL;
	
	public ConsolidateSameCities(){
		mySQL = new SQLData();
		mySQL.login();
		try {
			consolidateSameCities();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void consolidateSameCities() throws IOException, SQLException{
		int completed = 0;
		
		String statement = "USE bloomnet;";
		mySQL.executeStatement(statement);
		
		String query = "SELECT id, name, state_id FROM city GROUP BY name,state_id HAVING COUNT(state_id) > 1 LIMIT 10000;";
		
		String cityName = "";
		String stateId = "";
		String idToUse = "";
		List<String> idsToRemove = new ArrayList<String>();
		
		ResultSet results = mySQL.executeQuery(query);
		
		while(results.next()){
			completed++;
			if(completed % 100 == 0){
				System.out.println(completed + " Lines Completed...");
			}
			idToUse = results.getString("id");
			cityName = results.getString("name").replaceAll("'", "''");
			stateId = results.getString("state_id");
			
			query = "SELECT id FROM city WHERE name = '"+cityName+"' AND state_id = "+stateId+" AND city_type = 'D';";
			ResultSet results2 = mySQL.executeQuery(query);
			if(results2.next()){
				idToUse = results2.getString("id");
			}
			results2.close();
			mySQL.closeStatement();
			
			query = "SELECT id FROM city WHERE name = '"+cityName+"' AND state_id = "+stateId+";";
			ResultSet results3 = mySQL.executeQuery(query);
			while(results3.next()){
				String id = results3.getString("id");
				if(!id.equals(idToUse))
					idsToRemove.add(id);
			}
			results3.close();
			mySQL.closeStatement();
			
			for(int ii = 0; ii < idsToRemove.size(); ++ii){
				String id = idsToRemove.get(ii);
				statement = "UPDATE address SET city_id = "+idToUse+" WHERE city_id = "+id+";";
				mySQL.executeStatement(statement);
				statement = "UPDATE listing SET city_id = "+idToUse+" WHERE city_id = "+id+";";
				mySQL.executeStatement(statement);
				statement = "UPDATE city_zip_xref SET id_city = "+idToUse+" WHERE id_city = "+id+";";
				mySQL.executeStatement(statement);
				statement = "DELETE FROM city WHERE id = "+id+";";
				mySQL.executeStatement(statement);
			}
			
		}
		results.close();
		mySQL.closeStatement();
		
	}
	
	public static void main(String[] args){
		new ConsolidateSameCities();
	}
	
}
