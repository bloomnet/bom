package com.bloomnet.bom.upload.tools;

import java.io.BufferedReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;

public class FTDWebCrawler {

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException, NoSuchAlgorithmException, KeyManagementException{
		
		  FileWriter myWriter = new FileWriter("FTDData.csv");
		  myWriter.write("Shop Code,Shop Name,Shop Phone,Shop Address,City,State,Zip,Covered City,Covered State\n");
		  myWriter.flush();
		  
		  Map<String, String> phoneCode = new HashMap<String, String>();
		
		  String baseURL = "https://www.ftdflorists.com/states";
		  
		  SSLContext ssl = SSLContext.getInstance("TLSv1.2"); 
		  ssl.init(null, null, new SecureRandom());
		  
	      URL url = new URL(baseURL);
	      HttpsURLConnection connection = (HttpsURLConnection) url.openConnection();
	      HttpsURLConnection.setDefaultSSLSocketFactory(ssl.getSocketFactory());
	      connection.setSSLSocketFactory(ssl.getSocketFactory());
	      
	      Scanner sc = new Scanner(connection.getInputStream());
	      StringBuffer sb = new StringBuffer();
	      while(sc.hasNext()) {
	         sb.append(sc.next());
	      }
	      String result = sb.toString();
	      
	      String[] parentLinks = result.split("href=\"/states");
	      
	      String state = "";
	      
	      for(int ii=2; ii < parentLinks.length - 1; ++ii){
	    	  state = parentLinks[ii].split("\"")[0].split("/")[1];
	    	  URL urlChild = new URL(baseURL+"/"+state);
	    	  Scanner scChild = new Scanner(urlChild.openStream());
		      StringBuffer sbChild = new StringBuffer();
		      while(scChild.hasNext()) {
		         sbChild.append(scChild.next());
		      }
		      String resultChild = sbChild.toString();
		      
		      String[] childParentLinksTemp = resultChild.split("href=\"/states");
		      List<String> childParentLinks = new ArrayList<String>();
		      
		      for(int xx=3; xx<childParentLinksTemp.length; ++xx){
		    	  if(!childParentLinks.contains(childParentLinksTemp[xx].split("\"")[0].toLowerCase()))
		    		  childParentLinks.add(childParentLinksTemp[xx].split("\"")[0].toLowerCase());
		      }
		      
		      String city = "";
		      
		      state = state.replaceAll("%20", " ");
		     
		      for(int xx=0; xx < childParentLinks.size(); ++xx){
		    	  
	    		  city = childParentLinks.get(xx);
	    		  
	    		  if(city == null || city.equals("")) continue;
	    	  
		    	  URL urlChild2 = new URL(baseURL+city);
		    	  BufferedReader br = new BufferedReader(new InputStreamReader(urlChild2.openStream()));
		    	  StringBuffer sbChild2 = new StringBuffer();
		    	  String line;
			      while((line = br.readLine()) != null) {
			         sbChild2.append(line);
			      }
			      String resultChild2 = sbChild2.toString();
			      if(resultChild2.contains("<p style='text-align: center;'>We're Sorry!</p>")) continue;
			      String addressLine = resultChild2.split("class=\"address\">")[1].split("<")[0];
			      String[] addressItems = addressLine.split(",");
	      
			      String shopName = resultChild2.split("<!-- -->")[2];
			      String shopAddress = addressItems[0].trim();
			      String shopCity = addressItems[1].trim();
			      String shopState = state;
			      String shopZip = "";
			      if(addressItems.length == 3)
			    	  shopZip = addressItems[2].trim();
			      String shopPhone = resultChild2.split("tel:")[1].split("\"")[0];
			      String shopCode = "";
			      if(phoneCode.get(shopPhone) != null)
			    	  shopCode = phoneCode.get(shopPhone);
			      else{
			    	  shopCode = String.valueOf(ii + "-" + xx);
			    	  phoneCode.put(shopPhone, shopCode);
			      }
			      
			      String writeLine = "\"" + shopCode + "\",\"" + shopName + "\",\"" + shopPhone + "\",\"" + shopAddress + "\",\"" + shopCity + "\",\"" + shopState + "\",\"" + shopZip + "\",\"" + city.split("/")[2] + "\",\"" + state + "\"\n";
			      writeLine = writeLine.replaceAll("&#x27;","'").replaceAll("&amp;","&");
			      
		    	  myWriter.write(writeLine);
		    	  myWriter.flush();
		    	}
		     }
	      }
}
