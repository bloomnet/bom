package com.bloomnet.bom.upload.tools;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class UploadTRPBilling {
	
	private SQLData mySQL;
	private String databaseName;
	
	private final boolean ASC = true;
	private final boolean DESC = false;
	
	private Map<String,ShopTRPCharges> shopTRP = new HashMap<String,ShopTRPCharges>();
	
	private FileWriter writer;
	private FileWriter writer2;	
	
	public UploadTRPBilling(String databaseName){

		this.databaseName = databaseName;
		mySQL = new SQLData();
		mySQL.login();
		try {
			writer = new FileWriter("data/billings.csv");
			writer2 = new FileWriter("data/summary.csv");
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			calculateBillings();
			calcultaeRescinds();
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		uploadData();
		
	}
	private void updateAsProcessed(String id, String table){
		String statement = "UPDATE "+databaseName+"."+table+" SET processed = 1 WHERE id = "+id+";";
		mySQL.executeStatement(statement);
	}
	
	private void calculateBillings() throws SQLException, ParseException, IOException{
		
		
		writer.append("shop,start,end,product code,description,reference,amount,city,state,user,Notes,SR Number");
		writer.append("\n");
		
		writer2.append("Top 10 Shops for TRP Credits");
		writer2.append("\n");
		
		String query = "SELECT * FROM "+databaseName+".timely_reply_process WHERE processed = 0;";
		ResultSet results = mySQL.executeQuery(query);
		
		System.out.println("Processing TRP Billings...");
		
		Map<String,Double> topTRP = new HashMap<String,Double>();
		
		int ii = 0;
		while(results.next()){
			ii++;
			if(ii%100 == 0){
				writer.flush();
				System.out.println(ii+" processed");
			}
			String id = results.getString("id");
			String sendingShopCode = results.getString("sending_shop_code");
			String fulfillingShopCode = results.getString("fulfilling_shop_code");
			String messageType = results.getString("message_type_id");
			String orderNumber = results.getString("order_number");
			String deliveryDate = results.getString("delivery_date");
			Date d1 = new Date();
			if(deliveryDate != null && !deliveryDate.equals("") && !deliveryDate.equals("0000-00-00"))
				d1 = new SimpleDateFormat("yyyy-MM-dd").parse(deliveryDate);
			String rejDate = results.getString("dln_rej_date");
			Date d2 = new Date();
			if(rejDate != null && !rejDate.equals("") && !rejDate.equals("0000-00-00"))
				d2 = new SimpleDateFormat("yyyy-MM-dd").parse(rejDate);
			String createdDate = results.getString("created_date");
			Integer billingMonth = Integer.valueOf(createdDate.split("-")[1]) - 1;
			if(billingMonth == 0)
				billingMonth = 12;
			String billingMonthString = billingMonth.toString();
			if(billingMonthString.length() == 1)
				billingMonthString = "0" + billingMonthString;
			String usableDate = createdDate.split("-")[0]+"-"+billingMonthString+"-01";
			Double elapsedHours = 0.00;
			String elapsedHoursString = results.getString("rej_delta_hours");
			if(elapsedHoursString != null && !elapsedHoursString.equals("")){
				elapsedHours = Double.valueOf(elapsedHoursString);
			}
			String rejectTime = results.getString("dln_rej_date_time");
			Integer rejectTimeInt = 0;
			if(rejectTime != null && !rejectTime.equals("")){
				rejectTime = rejectTime.replaceAll(":", "");
				rejectTimeInt = Integer.valueOf(rejectTime);
				
			}
			Double feeAmount = Double.valueOf(results.getString("total_fee"));
			
			String billingCode = "";
			String notes = "";
			if(messageType.equals("1")){ //No Delivery Notification 
				if(sendingShopCode.equals("800000"))
					billingCode = "1001.DLN18F001";
				else
					billingCode = "1001.DLNS2S001";
			}else if(messageType.equals("2")){ //Late delivery Notification
				if(sendingShopCode.equals("800000"))
					billingCode = "1001.DLN18F001";
				else
					billingCode = "1001.DLNS2S001";
			}else{ //Late response
				if(sendingShopCode.equals("800000"))
					billingCode = "1001.LRP18F001";
				else
					billingCode = "1001.LRPS2S001";
				if(((d2.getTime() > d1.getTime()) || (d2.getTime() == d1.getTime() && rejectTimeInt > 150000 ))){//2:00PM EST (Cut-off Time)
			    	notes = "TRP Fee response after delivery window";
				}else if(elapsedHours > 72.00){
					notes = "TRP Fee 72 hours or more";
				}else if(elapsedHours > 24.00){
					notes = "TRP Fee 24 hours to 72 hours";
				}else if(elapsedHours > 2.00){
					notes = "TRP Fee 2 hours to 24 hours";
				}
			}
			
			ShopTRPCharges trp = shopTRP.get(fulfillingShopCode+","+usableDate);
			if(trp != null){//Charge the fulfilling shop the TRP fee
				if(trp.getTotalTRPOwed() != null)
					trp.setTotalTRPOwed(trp.getTotalTRPOwed()+feeAmount);
				else
					trp.setTotalTRPOwed(feeAmount);
			}else{
				trp = new ShopTRPCharges();
				trp.setTotalTRPOwed(feeAmount);
				shopTRP.put(fulfillingShopCode+","+usableDate, trp);
			}
			
			trp = shopTRP.get(sendingShopCode+","+usableDate);
			if(trp != null){//Credit the sending shop the TRP fee
				if(trp.getTotalTRPDue() != null)
					trp.setTotalTRPDue(trp.getTotalTRPDue()+feeAmount);
				else
					trp.setTotalTRPDue(feeAmount);
			}else{
				trp = new ShopTRPCharges();
				trp.setTotalTRPDue(feeAmount);
				shopTRP.put(sendingShopCode+","+usableDate, trp);
			}
			
			if(topTRP.get(sendingShopCode) != null){
				Double currentValue = topTRP.get(sendingShopCode);
				Double newValue = currentValue + (feeAmount*-1);
				topTRP.put(sendingShopCode, newValue);
			}else{
				topTRP.put(sendingShopCode, (feeAmount*-1));
			}
			
			writer.append(sendingShopCode+","+usableDate+","+usableDate+","+billingCode+",,"+orderNumber+","+String.valueOf(feeAmount*-1)+",,,msilver,"+notes+",");//Sending shop is credited the TRP charge
			writer.append("\n");
			writer.append(fulfillingShopCode+","+usableDate+","+usableDate+","+billingCode+",,"+orderNumber+","+String.valueOf(feeAmount)+",,,msilver,"+notes+",");//Fulfilling shop is charged for the TRP charge.
			writer.append("\n");
			
			updateAsProcessed(id,"timely_reply_process");
		}
		results.close();
		mySQL.closeStatement();
		
		topTRP = sortByComparator(topTRP, ASC);
		Set<String> keys = topTRP.keySet();
		Iterator<String> it = keys.iterator();
		for(int xx=0; xx<10; xx++){
			try{
				String key = (String) it.next();
				writer2.append(key+","+topTRP.get(key)+"\n");
			}catch(Exception ee){
				
			}
		}
		
		writer2.flush();
		System.out.println("Finished Processing TRP Billings...");
		
	}
	
	private void calcultaeRescinds() throws SQLException, ParseException, IOException{
		
		String query = "SELECT * FROM "+databaseName+".rescind_process WHERE processed = 0;";
		ResultSet results = mySQL.executeQuery(query);
		
		System.out.println("Processing TRP Rescinds...");
		int ii=0;
		
		writer2.append("\nTop 5 Shops for Resind Debits");
		writer2.append("\n");
		
		Map<String,Double> topRescind = new HashMap<String,Double>();
		
		while(results.next()){
			ii++;
			if(ii%100 == 0){
				writer.flush();
				System.out.println(ii+" processed");
			}
			String orderNumber = results.getString("order_number");
			
			String query2 = "SELECT * FROM "+databaseName+".timely_reply_process WHERE order_number = '"+orderNumber+"';";
			ResultSet results2 = mySQL.executeQuery(query2);
			while(results2.next()){
				String id = results.getString("id");
				String sendingShopCode = results2.getString("sending_shop_code");
				String fulfillingShopCode = results2.getString("fulfilling_shop_code");
				String messageType = results2.getString("message_type_id");
				String createdDate = results.getString("created_date");
				Integer billingMonth = Integer.valueOf(createdDate.split("-")[1]) - 1;
				if(billingMonth == 0)
					billingMonth = 12;
				String billingMonthString = billingMonth.toString();
				if(billingMonthString.length() == 1)
					billingMonthString = "0" + billingMonthString;
				String usableDate = createdDate.split("-")[0]+"-"+billingMonthString+"-01";
				Double feeAmount = Double.valueOf(results2.getString("total_fee"));
				
				String billingCode = "";
				String notes = "TRP Credit back due to Rescind";
				if(messageType.equals("1")){ //No Delivery Notification 
					if(sendingShopCode.equals("800000"))
						billingCode = "1001.DLN18F001";
					else
						billingCode = "1001.DLNS2S001";
				}else if(messageType.equals("2")){ //Late delivery Notification
					if(sendingShopCode.equals("800000"))
						billingCode = "1001.DLN18F001";
					else
						billingCode = "1001.DLNS2S001";
				}else{ //Late response
					if(sendingShopCode.equals("800000"))
						billingCode = "1001.LRP18F001";
					else
						billingCode = "1001.LRPS2S001";
				}
				
				ShopTRPCharges trp = shopTRP.get(fulfillingShopCode+","+usableDate);
				if(trp != null){//Credit the fulfilling shop the TRP fee back as a rescind credit
					if(trp.getTotalRescindDue() != null)
						trp.setTotalRescindDue(trp.getTotalRescindDue()+feeAmount);
					else
						trp.setTotalRescindDue(feeAmount);
				}else{
					trp = new ShopTRPCharges();
					trp.setTotalRescindDue(feeAmount);
					shopTRP.put(fulfillingShopCode+","+usableDate, trp);
				}
				
				trp = shopTRP.get(sendingShopCode+","+usableDate);
				if(trp != null){//Charge the sending shop the TRP fee as a rescind charge
					if(trp.getTotalRescindOwed() != null)
						trp.setTotalRescindOwed(trp.getTotalRescindOwed()+feeAmount);
					else
						trp.setTotalRescindOwed(feeAmount);
				}else{
					trp = new ShopTRPCharges();
					trp.setTotalRescindOwed(feeAmount);
					shopTRP.put(sendingShopCode+","+usableDate, trp);
				}
				
				if(topRescind.get(sendingShopCode) != null){
					Double currentValue = topRescind.get(sendingShopCode);
					Double newValue = currentValue + feeAmount;
					topRescind.put(sendingShopCode, newValue);
				}else{
					topRescind.put(sendingShopCode, feeAmount);
				}
				
				writer.append(sendingShopCode+","+usableDate+","+usableDate+","+billingCode+",,"+orderNumber+","+String.valueOf(feeAmount)+",,,msilver,"+notes+",");//Sending shop gets charged the fee they received
				writer.append("\n");
				writer.append(fulfillingShopCode+","+usableDate+","+usableDate+","+billingCode+",,"+orderNumber+","+String.valueOf(feeAmount*-1)+",,,msilver,"+notes+",");//Fulfilling shop receives credit back for the fee they were charged
				writer.append("\n");
				
				updateAsProcessed(id,"rescind_process");
			}
			results2.close();
			mySQL.closeStatement();
		}
		results.close();
		mySQL.closeStatement();
		
		topRescind = sortByComparator(topRescind, DESC);
		Set<String> keys = topRescind.keySet();
		Iterator<String> it = keys.iterator();
		for(int xx=0; xx<5; xx++){
			try{
				String key = (String) it.next();
				writer2.append(key+","+topRescind.get(key)+"\n");
			}catch(Exception ee){
				
			}
		}
		
		System.out.println("Finished Processing TRP Rescinds...");
		
		writer.flush();
		writer.close();
		writer2.flush();
		writer2.close();
	}
	
	private void uploadData(){
		
		System.out.println("Uploading Data...");
		
		Iterator<String> it = shopTRP.keySet().iterator();
		while(it.hasNext()){
			String key = it.next();
			String dateComponent = key.split(",")[1];
			String shopCode = key.split(",")[0];
			ShopTRPCharges trp = shopTRP.get(key);
			Double totalTRPOwed = trp.getTotalTRPOwed();
			Double totalTRPDue = trp.getTotalTRPDue();
			Double totalNetTRP = totalTRPOwed - totalTRPDue;
			Double totalRescindOwed = trp.getTotalRescindOwed();
			Double totalRescindDue = trp.getTotalRescindDue();
			Double totalNetRescind = totalRescindOwed - totalRescindDue;
			Double netTotal = totalNetTRP + totalNetRescind;
			
			String statement = "INSERT INTO "+databaseName+".timely_reply_billing(billing_cycle,shop_code,total_trp_owed,"
					+ "total_trp_due,net_trp,total_rescind_owed,total_rescind_due,net_rescind,net_total,created_date) VALUES("
					+ "STR_TO_DATE('"+dateComponent+"','%Y-%c-%e'),'"+shopCode+"',"+totalTRPOwed+","+totalTRPDue+","+totalNetTRP+","+totalRescindOwed+","
							+ ""+totalRescindDue+","+totalNetRescind+","+netTotal+",now());";
			mySQL.executeStatement(statement);
		}
		System.out.println("Finsihed Uploading Data...");
	}
	
	private Map<String, Double> sortByComparator(Map<String, Double> unsortMap, final boolean order){ //true for ASC, false for DESC

        List<Entry<String, Double>> list = new LinkedList<Entry<String, Double>>(unsortMap.entrySet());

        Collections.sort(list, new Comparator<Entry<String, Double>>()
        {
            public int compare(Entry<String, Double> o1,
                    Entry<String, Double> o2)
            {
                if (order)
                {
                    return o1.getValue().compareTo(o2.getValue());
                }
                else
                {
                    return o2.getValue().compareTo(o1.getValue());

                }
            }
        });

        // Maintaining insertion order with the help of LinkedList
        Map<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        for (Entry<String, Double> entry : list)
        {
            sortedMap.put(entry.getKey(), entry.getValue());
        }

        return sortedMap;
    }

}
