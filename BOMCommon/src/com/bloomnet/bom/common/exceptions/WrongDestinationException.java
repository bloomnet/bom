package com.bloomnet.bom.common.exceptions;

public class WrongDestinationException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public WrongDestinationException(String nextDestination){
		super("Virtual queue should be "  + nextDestination + "now");
	}

}
