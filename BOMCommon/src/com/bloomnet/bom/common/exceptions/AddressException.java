package com.bloomnet.bom.common.exceptions;

import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.State;
import com.bloomnet.bom.common.entity.Zip;

/**
 * @author Danil svirchtchev
 *
 */
public class AddressException extends Exception {

	private static final long serialVersionUID = 1L;
	
	/**
	 * @param e
	 */
	public AddressException( Throwable e ) {
		
		super(e);
	}
	
	public AddressException( Zip zip ) {
		
		super( "Zip code "+zip.getZipCode()+" is not valid" );
	}
	
	public AddressException( City city ) {
		
		super( "City "+city.getName()+" is not valid" );
	}
	
	public AddressException( Zip zip, City city ) {
		
		super( "City "+city.getName()+" is not in the zip code "+zip.getZipCode() );
	}
	
	public AddressException( State state, City city ) {
		
		super( "City "+city.getName()+" is not in the state "+state.getShortName() );
	}
}
