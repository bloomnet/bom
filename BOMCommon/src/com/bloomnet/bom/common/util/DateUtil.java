package com.bloomnet.bom.common.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import com.bloomnet.bom.common.constants.BOMConstants;

public class DateUtil {
	private static SimpleDateFormat format = new SimpleDateFormat("MM/dd/yyyy");
	private static SimpleDateFormat xmlFormat = new SimpleDateFormat("yyyyMMddhhmmss");
	private static SimpleDateFormat xmlDateFormat = new SimpleDateFormat("yyyyMMdd");
	private static SimpleDateFormat xmlDashFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	private static SimpleDateFormat dashFormat = new SimpleDateFormat("MM-dd-yyyy");
	private static SimpleDateFormat dashFormat2 = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat dashTimeFormat = new SimpleDateFormat("MM-dd-yyyy hh:mm aa");
	private static SimpleDateFormat dashTimeFormat2 = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss aa");
	private static SimpleDateFormat formatTime = new SimpleDateFormat("M/dd/yyyy hh:mm aa");
	private static SimpleDateFormat xml24Format = new SimpleDateFormat("yyyyMMddkkmmss");
	private static SimpleDateFormat xmlNoTimeFormat = new SimpleDateFormat("yyyyMMdd000000");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("kkmmss");
	//private static SimpleDateFormat unixFormat = new SimpleDateFormat("yyyyMMdd");
	private static SimpleDateFormat hourFormat = new SimpleDateFormat("kk");
	private static SimpleDateFormat weekFormat = new SimpleDateFormat("EEEEEEEEE MMMM dd, yyyy");



	
	public static long convertDateToLong() throws ParseException{
		
		return new Date().getTime()/1000L;
		
	}

	
	@SuppressWarnings("deprecation")
	public static String getCurrentDate(int hours) {
		Date date = new Date();
		if(hours > 13) date.setDate(date.getDate()+1);
		return toString(date);
	}
	/**
	 * Supports date format yyyyMMddhhmmss
	 * @param dateStr
	 * @return
	 */
	public static Date toDate(String dateStr) {
		//TODO: Verify if it's necessary to throw the ParseException
		try { 
			return xmlFormat.parse(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	
	public static String toDate(Date dateStr) {
		//TODO: Verify if it's necessary to throw the ParseException
		try { 
			return xmlFormat.format(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	/**
	 * Supports date format MM/dd/yyyy
	 * @param dateStr
	 * @return
	 */
	public static Date toDateFormat(String dateStr) {
		//TODO: Verify if it's necessary to throw the ParseException
		try { 
			return format.parse(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	
	public static String toDateStringFormat(Date dateStr) {
		//TODO: Verify if it's necessary to throw the ParseException
		try { 
			return format.format(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	
	/**
	 * Format MM-dd-yyyy
	 * @param dateStr
	 * @return
	 */
	public static Date toDateDashFormat(String dateStr) {
		//TODO: Verify if it's necessary to throw the ParseException
		try { 
			return dashFormat.parse(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	
	public static Date toDateDashTimeFormat(String dateStr) {
		//TODO: Verify if it's necessary to throw the ParseException
		try { 
			return dashTimeFormat.parse(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	
	public static Date toDateDashTimeFormat2(String dateStr) {
		//TODO: Verify if it's necessary to throw the ParseException
		try { 
			return dashTimeFormat2.parse(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	
	public static String formatDateString(String in) {
		if(in.length() == 8) return in.substring(0,2)+"/"+in.substring(2,4)+"/"+in.substring(4);
		if( in.length() == 10)  return in.replace("-", "/");
		return in;
	}
	public static String toString(Date date) {
		
		final String result;
		
		if( date != null ) {
			result = format.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static String toStringFormatTime(Date date) {
		
		final String result;
		
		if( date != null ) {
			result = formatTime.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}

public static Date toDateTime(String dateStr) {
	try { 
		return timeFormat.parse(dateStr);
	}
	catch (Exception e) {
		return null;  
	}
}



	public static String toXmlFormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = xmlFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static String toXmlDateFormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = xmlDateFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static String toXmlDashFormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = xmlDashFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	
	public static String toXml24FormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = xml24Format.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static String toTimeFormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = timeFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static String toHourFormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = hourFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static String getCurrentTimeString(Date date) {
		final String result;
		
		if( date != null ) {
			result = timeFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static String tomorrowsDate(){
		
		GregorianCalendar calendar = new GregorianCalendar();

		//Display the date now:
		Date now = calendar.getTime();
		String formattedDate = format.format(now);
		//System.out.println("todays date: " + formattedDate);

		//Advance the calendar one day:
		calendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
		Date tomorrow = calendar.getTime();
		formattedDate = format.format(tomorrow);
		//System.out.println("tomorrows date: " +formattedDate);

		
		return formattedDate;
	}
	
	public static String todashFormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = dashFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static Date toDateDashFormat2(String date) {
		Date result = null;
		
		if( date != null ) {
			try {
				result = dashFormat2.parse(date);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	
	public static String toXmlNoTimeFormatString(Date date) {
		final String result;
		
		if( date != null ) {
			result = xmlNoTimeFormat.format(date);
		}
		else {
			result = "";
		}
		
		return result;
	}
	
	public static Date toDateXmlFormat(String dateStr) {
		try { 
			return xmlNoTimeFormat.parse(dateStr);
		}
		catch (Exception e) {
			return null;  
		}
	}
	
	public static Date toDateUnixFormat(long time) {
		
		Date d = new Date(time * 1000); 
		
		return d;
	}
	
public static String tomorrowsNoTimeDate(){
		
		GregorianCalendar calendar = new GregorianCalendar();

		//Display the date now:
		Date now = calendar.getTime();
		String formattedDate = format.format(now);
		//System.out.println("todays date: " + formattedDate);

		//Advance the calendar one day:
		calendar.add(GregorianCalendar.DAY_OF_MONTH, 1);
		Date tomorrow = calendar.getTime();
		formattedDate = xmlNoTimeFormat.format(tomorrow);
		//System.out.println("tomorrows date: " +formattedDate);

		
		return formattedDate;
	}
	
		public static String yesterdaysDate(){
		
		GregorianCalendar calendar = new GregorianCalendar();

		//Display the date now:
		Date now = calendar.getTime();
		String formattedDate = format.format(now);
		//System.out.println("todays date: " + formattedDate);

		//Advance the calendar one day:
		calendar.add(GregorianCalendar.DAY_OF_MONTH, -1);
		Date tomorrow = calendar.getTime();
		formattedDate = format.format(tomorrow);
		//System.out.println("tomorrows date: " +formattedDate);

		
		return formattedDate;
	}
	
	public static List<String> lastFiveDays(){
		
		List<String> lastFiveDays = new ArrayList<String>();
	

		for (int i =-1; i>-6; i--){
			GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(GregorianCalendar.DAY_OF_MONTH, i);
		Date tomorrow = calendar.getTime();
		String formattedDate = format.format(tomorrow);
		lastFiveDays.add(formattedDate);
		}
		
		return lastFiveDays;
	}
	
	public static List<String> lastThreeDays(){
		
		List<String> lastThreeDays = new ArrayList<String>();
	

		for (int i =-1; i>-4; i--){
			GregorianCalendar calendar = new GregorianCalendar();
		calendar.add(GregorianCalendar.DAY_OF_MONTH, i);
		Date tomorrow = calendar.getTime();
		String formattedDate = format.format(tomorrow);
		lastThreeDays.add(formattedDate);
		}
		
		return lastThreeDays;
	}
	
public static String EasternTime(){
		
		GregorianCalendar calendar = new GregorianCalendar();

		//Display the date now:
		Date now = calendar.getTime();
		String formattedDate = formatTime.format(now);
		//System.out.println("todays date: " + formattedDate);

		//Advance the calendar one day:
		calendar.add(GregorianCalendar.HOUR, +1);
		Date tomorrow = calendar.getTime();
		formattedDate = formatTime.format(tomorrow);
		//System.out.println("tomorrows date: " +formattedDate);

		
		return formattedDate;
	}
	

public static Date previousDate(Date now){
	
	GregorianCalendar calendar = new GregorianCalendar();

	//Display the date now:
	//now = calendar.getTime();
	String formattedDate = format.format(now);
	//System.out.println("todays date: " + formattedDate);

	//Advance the calendar one day:
	calendar.add(GregorianCalendar.DAY_OF_MONTH, -1);
	Date previous = calendar.getTime();
	formattedDate = format.format(previous);
	//System.out.println("tomorrows date: " +formattedDate);

	
	return previous;
}

public static String nextDate(Date now){
	
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(now);  

	//Display the date now:
	//now = calendar.getTime();
	String formattedDate = format.format(now);
	//System.out.println("todays date: " + formattedDate);

	//Advance the calendar one day:
	calendar.add(GregorianCalendar.DAY_OF_MONTH, +1);
	Date tomorrow = calendar.getTime();
	formattedDate = format.format(tomorrow);
	//System.out.println("tomorrows date: " +formattedDate);

	
	return formattedDate;
}

public static Date childOrderDate(Date now){
	
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(now);  

	System.out.println("todays date: " + now);

	//Advance the calendar 1 minute:
	calendar.add(GregorianCalendar.MINUTE, +1);
	Date newDate = calendar.getTime();
	System.out.println("child date: " +newDate);

	
	return newDate;
}

public static Date getNextDate(Date date){
	
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);  

	//Advance the calendar one day:
	calendar.add(GregorianCalendar.DAY_OF_MONTH, +1);
	Date next = calendar.getTime();

	
	return next;
}

public static Date getFirstOfMonth(Date date){
	
	Calendar calendar = Calendar.getInstance();
	calendar.setTime(date);  
	calendar.set(Calendar.DAY_OF_MONTH, 1); 

	Date first = calendar.getTime();

	return first;
}

public static String toWeekString(Date date) {

	final String result;

	if( date != null ) {
		result = weekFormat.format(date);
	}
	else {
		result = "";
	}
	return result;

}

public static String getMonthDescription( int month ){
	
	String description = new String();
	
	if ( month != 0 ) {
		
		if (month==1){
			description = "January";
		}
		else if (month==2){
			description = "February";
		}
		else if (month==3){
			description = "March";
		}
		else if (month==4){
			description = "April";
		}
		else if (month==5){
			description = "May";
		}
		else if (month==6){
			description = "June";
		}
		else if (month==7){
			description = "July";
		}
		else if (month==8){
			description = "August";
		}
		else if (month==9){
			description = "September";
		}
		else if (month==10){
			description = "October";
		}
		else if (month==11){
			description = "November";
		}
		else if (month==12){
			description = "December";
		}
		
	}
	
	return description;
}


}
