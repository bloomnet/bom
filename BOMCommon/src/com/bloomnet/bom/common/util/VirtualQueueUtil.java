package com.bloomnet.bom.common.util;

import java.util.HashMap;
import java.util.Map;

import com.bloomnet.bom.common.constants.BOMConstants;

public class VirtualQueueUtil {
	

	
	public static byte getVirtualQueueId( String destination ){
		
		byte vqueue = 0;
		
		if ( destination != null ) {
			
			if (destination.equals(BOMConstants.FSI_QUEUE)){
				vqueue = 1;
			}
			else if (destination.equals(BOMConstants.TFSI_QUEUE)){
				vqueue = 2;
			}
			else if (destination.equals(BOMConstants.TLO_QUEUE)){
				vqueue = 3;
			}
			else if (destination.equals(BOMConstants.FAX_OUTBOUND_ORDER_QUEUE)){
				vqueue = 4;
			}
			else if (destination.equals(BOMConstants.MESSAGES_QUEUE)){
				vqueue = 5;
			}
			
		}
		
		return vqueue;
	}


}
