package com.bloomnet.bom.common.util;

import java.util.HashMap;
import java.util.Map;

import com.bloomnet.bom.common.constants.BOMConstants;

public class OccassionUtil {
	
	public static String[] OCCASSIONS_ARRAY = { "Funeral", "Illness", "Birthday", "Business Gifts", "Holiday", "Maternity", "Anniversary", "Others"};
	
	private Map<String, String> occassions;
	
	public OccassionUtil() {
		
		this.occassions = new HashMap<String, String>();
		
		occassions.put( "1", "Funeral");
		occassions.put( "2", "Illness");
		occassions.put( "3", "Birthday");
		occassions.put( "4", "Business Gifts");
		occassions.put( "5", "Holiday");
		occassions.put( "6", "Maternity");
		occassions.put( "7", "Anniversary");
		occassions.put( "8", "Others");
	}
	
	public static String getOccassionDescription( String occassionId ){
		
		String occassion = "";
		
		if ( occassionId != null ) {
			
			if (occassionId.equals(BOMConstants.Funeral)){
				occassion = "Funeral";
			}
			else if (occassionId.equals(BOMConstants.Illness)){
				occassion = "Illness";
			}
			else if (occassionId.equals(BOMConstants.Birthday)){
				occassion = "Birthday";
			}
			else if (occassionId.equals(BOMConstants.BusinessGifts)){
				occassion = "Business Gifts";
			}
			else if (occassionId.equals(BOMConstants.Holiday)){
				occassion = "Holiday";
			}
			else if (occassionId.equals(BOMConstants.Maternity)){
				occassion = "Maternity";
			}
			else if (occassionId.equals(BOMConstants.Anniversary)){
				occassion = "Anniversary";
			}
			else if (occassionId.equals(BOMConstants.Others)){
				occassion = "Others";
			}
		}
		
		return occassion;
	}

	public void setOccassions(Map<String, String> occassions) {
		this.occassions = occassions;
	}
	public Map<String, String> getOccassions() {
		return occassions;
	}
}
