package com.bloomnet.bom.common.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.log4j.Logger;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;


public class XMLValidate {
	
	// Define a static logger variable
	static final Logger logger = Logger.getLogger(XMLValidate.class);

	/**
	 * DOM Parser
	 * 
	 * @param xml
	 * @throws Exception
	 */
	public void checkXmlForm(String xml) throws Exception {

		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		factory.setValidating(false);
		factory.setNamespaceAware(true);

		DocumentBuilder builder;
		
		try {
			
			builder = factory.newDocumentBuilder();

			builder.setErrorHandler(new SimpleErrorHandler());

			builder.parse(new InputSource(new ByteArrayInputStream(xml.getBytes("utf-8"))));
			
			if (logger.isDebugEnabled()){
				logger.debug("well formed xml");
			}

		} catch (ParserConfigurationException e) {
			logger.error(e);
			throw new Exception(e);
		} catch (SAXException e) {
			logger.error(e);
			throw new Exception(e);
		} catch (IOException e) {
			logger.error(e);
			throw new Exception(e);
		}
	}
}
