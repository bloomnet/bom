package com.bloomnet.bom.common.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import org.springframework.stereotype.Service;


import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.audit.AuditInterface;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.DeliveryDetails;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckf;
import com.bloomnet.bom.common.jaxb.fsi.MessageCanc;
import com.bloomnet.bom.common.jaxb.fsi.MessageConf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDeni;
import com.bloomnet.bom.common.jaxb.fsi.MessageDisp;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlca;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlcf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlou;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspc;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspd;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspr;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspu;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.jaxb.fsi.MessageInqr;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagePchg;
import com.bloomnet.bom.common.jaxb.fsi.MessageResp;
import com.bloomnet.bom.common.jaxb.fsi.MessageRjct;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductsInfo;
import com.bloomnet.bom.common.jaxb.fsi.Recipient;
import com.bloomnet.bom.common.jaxb.fsi.RedeliveryDetails;
import com.bloomnet.bom.common.jaxb.fsi.Security;
import com.bloomnet.bom.common.jaxb.fsi.ShippingDetails;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectoryInterface;
import com.bloomnet.bom.common.jaxb.recipe.RecipeInfoInterface;
import com.bloomnet.bom.common.tfsi.TFSIUtil;
import com.bloomnet.bom.common.tfsi.Transaction;

@Service("transformService")
public class Transform implements TransformService {
	
	static final Logger logger = Logger.getLogger(Transform.class);

	public static Properties PROPERTIES;
	

	
	@Autowired private Properties bomProperties;
	@Autowired private Properties fsiProperties;


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#convertToFSIXML(com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface)
	 */
	@Override
	public  String convertToFSIXML(ForeignSystemInterface foreignSystemInterface){ 
		  StringWriter xml = new StringWriter();

		  try {
			JAXBContext jc = JAXBContext.newInstance(BOMConstants.FSI_JAXB_PACKAGE );
			Marshaller m = jc.createMarshaller();
			//m.marshal( foreignSystemInterface, System.out );
			m.marshal( foreignSystemInterface,xml);
			m.setProperty("jaxb.encoding", "US-ASCII"); 
			
			if(logger.isDebugEnabled()){
				logger.debug("FSI XML: " + xml.toString());
			}

			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml.toString();
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#convertToJavaFSI(java.lang.String)
	 */
	@Override
	public  ForeignSystemInterface convertToJavaFSI(String xmlString)//
	{

		ForeignSystemInterface foreignSystemInterface = new ForeignSystemInterface();
		try{
			JAXBContext jc = JAXBContext.newInstance( BOMConstants.FSI_JAXB_PACKAGE );
			Unmarshaller unmarshaller = jc.createUnmarshaller();
			JAXBElement<ForeignSystemInterface> jaxbElem  = unmarshaller.unmarshal( new StreamSource( new StringReader( xmlString ) ),ForeignSystemInterface.class );

			foreignSystemInterface = jaxbElem.getValue();
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return foreignSystemInterface;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#convertToAuditXML(com.bloomnet.bom.common.jaxb.audit.AuditInterface)
	 */
	@Override
	public  String convertToAuditXML(AuditInterface auditInterface){ 
		  StringWriter xml = new StringWriter();

		  try {
			JAXBContext jc = JAXBContext.newInstance(BOMConstants.AUDIT_JAXB_PACKAGE );
			Marshaller m = jc.createMarshaller();
			m.marshal( auditInterface,xml);
			m.setProperty("jaxb.encoding", "US-ASCII"); 
			
			if(logger.isDebugEnabled()){
				logger.debug("AUDIT XML: " + xml.toString());
			}

			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml.toString();
	}
	
	@Override
	public  String convertToRecipeXML(RecipeInfoInterface recipeInterface){ 
		  StringWriter xml = new StringWriter();

		  try {
			JAXBContext jc = JAXBContext.newInstance(BOMConstants.RECIPE_JAXB_PACKAGE );
			Marshaller m = jc.createMarshaller();
			m.marshal( recipeInterface,xml);
			m.setProperty("jaxb.encoding", "US-ASCII"); 
			
			if(logger.isDebugEnabled()){
				logger.debug("RECIPE XML: " + xml.toString());
			}

			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml.toString();
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#convertToJavaAudit(java.lang.String)
	 */
	@Override
	public  AuditInterface convertToJavaAudit(String xmlString)//
	{

		AuditInterface auditInterface = new AuditInterface();
		try{
			JAXBContext jc = JAXBContext.newInstance( BOMConstants.AUDIT_JAXB_PACKAGE );
			Unmarshaller unmarshaller = jc.createUnmarshaller();

			JAXBElement<AuditInterface> jaxbElem  = unmarshaller.unmarshal( new StreamSource( new StringReader( xmlString ) ),AuditInterface.class );

			auditInterface = jaxbElem.getValue();
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return auditInterface;
	}
	
	@Override
	public  RecipeInfoInterface convertToJavaRecipe(String xmlString)//
	{

		RecipeInfoInterface recipeInterface = new RecipeInfoInterface();
		try{
			JAXBContext jc = JAXBContext.newInstance( BOMConstants.RECIPE_JAXB_PACKAGE );
			Unmarshaller unmarshaller = jc.createUnmarshaller();

			JAXBElement<RecipeInfoInterface> jaxbElem  = unmarshaller.unmarshal( new StreamSource( new StringReader( xmlString ) ),RecipeInfoInterface.class );

			recipeInterface = jaxbElem.getValue();
		
		}
		catch(Exception e){
			e.printStackTrace();
		}
		return recipeInterface;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#createForeignSystemInterface(com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder, com.bloomnet.bom.common.jaxb.fsi.Security)
	 */
	@Override
	public  String createForeignSystemInterface(MessagesOnOrder aMessageOnOrder, Security security) {

		String fsi = "";

		ForeignSystemInterface foreignSystemInterface = new ForeignSystemInterface();
		List<MessagesOnOrder> messagesOnOrderList = new ArrayList<MessagesOnOrder>();

		aMessageOnOrder.setMessageCount(1);

		messagesOnOrderList.add(aMessageOnOrder);

		foreignSystemInterface.getMessagesOnOrder().add(aMessageOnOrder);

		foreignSystemInterface.setSecurity(security);

		fsi = convertToFSIXML(foreignSystemInterface);

		return fsi;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#createForeignSystemInterface(java.lang.String, com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage)
	 */
	@Override
	public  String createForeignSystemInterface(String messageType,BloomNetMessage message) throws Exception {
		
		Properties props  = new Properties();
		
		if( bomProperties != null ){
			props.putAll( new HashMap<String, String>( (Map) bomProperties ) );
		}
		
		if( fsiProperties != null ) {
			props.putAll( new HashMap<String, String>( (Map) fsiProperties ) );
		}
		
		Transform.PROPERTIES = props;
		

		MessagesOnOrder aMessageOnOrder = new MessagesOnOrder();
		Security security = new Security();
		String fsi = new String();

		String fulfillingshopcode = new String();
		String sendingshopcode = new String();
		String recievingshopcode = new String();

		Identifiers identifiers = new Identifiers();
		GeneralIdentifiers generalIdentifiers = new GeneralIdentifiers();

		generalIdentifiers.setBmtOrderNumber(message.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber());
		generalIdentifiers.setBmtSeqNumberOfMessage(message.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage());
		generalIdentifiers.setBmtSeqNumberOfOrder(message.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfOrder());
		 
		 //TODO WHAT DOES THIS HAVE TO BE
		generalIdentifiers.setExternalShopMessageNumber("1001");
		
		generalIdentifiers.setExternalShopOrderNumber(message.getIdentifiers().getGeneralIdentifiers().getExternalShopOrderNumber());
		generalIdentifiers.setInwireSequenceNo(message.getIdentifiers().getGeneralIdentifiers().getInwireSequenceNo());
		 
		identifiers.setGeneralIdentifiers(generalIdentifiers);
		
		fulfillingshopcode = message.getFulfillingShopCode();
		sendingshopcode = message.getSendingShopCode(); 
		recievingshopcode = message.getReceivingShopCode();
		
		String orderNumber = message.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
		
		if (messageType.equals(BOMConstants.ACKF_MESSAGE_DESC)) {
			

			MessageAckf messageAckf = new MessageAckf();

			messageAckf.setFulfillingShopCode(fulfillingshopcode);
			messageAckf.setIdentifiers(identifiers);
			messageAckf.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			messageAckf.setMessageType(BOMConstants.ACKF_MESSAGE_TYPE);
			
			
			if(sendingshopcode.equals(recievingshopcode))
			{
				messageAckf.setReceivingShopCode(fulfillingshopcode);

				
			}
			else{
			messageAckf.setReceivingShopCode(sendingshopcode);
			}
			messageAckf.setSendingShopCode(recievingshopcode);
			messageAckf.setSystemType(BOMConstants.SYSTEM_TYPE);
			
			aMessageOnOrder.getMessageAckf().add(messageAckf);

		} else if (messageType.equals(BOMConstants.CANC_MESSAGE_DESC)) {
			

			MessageCanc messageCanc = new MessageCanc();
			
		 	MessageCanc canc = (MessageCanc)message;
		 	
			messageCanc.setFulfillingShopCode(fulfillingshopcode);
			messageCanc.setIdentifiers(identifiers);
			messageCanc.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			messageCanc.setMessageStatus(canc.getMessageStatus());
			messageCanc.setMessageText(canc.getMessageText());
			messageCanc.setMessageType(canc.getMessageType());
			messageCanc.setReceivingShopCode(recievingshopcode);
			messageCanc.setSendingShopCode(sendingshopcode);
			messageCanc.setSystemType(BOMConstants.SYSTEM_TYPE);

			aMessageOnOrder.getMessageCanc().add(messageCanc);
			

		}
	 else if (messageType.equals(BOMConstants.CONF_MESSAGE_DESC)) {
		
		MessageConf messageConf = new MessageConf();
		
	 	MessageConf conf = (MessageConf)message;
	 	
		messageConf.setFulfillingShopCode(fulfillingshopcode);
		messageConf.setIdentifiers(identifiers);
		messageConf.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
		messageConf.setMessageStatus(conf.getMessageStatus());
		messageConf.setMessageText(conf.getMessageText());
		messageConf.setMessageType(BOMConstants.CONF_MESSAGE_TYPE);
		messageConf.setReceivingShopCode(recievingshopcode);
		messageConf.setSendingShopCode(sendingshopcode);
		messageConf.setSystemType(BOMConstants.SYSTEM_TYPE);

		aMessageOnOrder.getMessageConf().add(messageConf);
	

	}
		
		 else if (messageType.equals(BOMConstants.DENI_MESSAGE_DESC)) {
			 

				MessageDeni messageDeni = new MessageDeni();
			 	MessageDeni deni = (MessageDeni)message;

			 	
				messageDeni.setFulfillingShopCode(fulfillingshopcode);
				messageDeni.setIdentifiers(identifiers);
				messageDeni.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDeni.setMessageStatus(deni.getMessageStatus());
				messageDeni.setMessageText(deni.getMessageText());
				messageDeni.setMessageType(deni.getMessageType());
				messageDeni.setReceivingShopCode(recievingshopcode);
				messageDeni.setSendingShopCode(sendingshopcode);
				messageDeni.setSystemType(BOMConstants.SYSTEM_TYPE);
				
				aMessageOnOrder.getMessageDeni().add(messageDeni);


			}
		
		 else if (messageType.equals(BOMConstants.DLCA_MESSAGE_DESC)) {
			 

				MessageDlca messageDlca = new MessageDlca();
			 	MessageDlca dlca = (MessageDlca)message;

				messageDlca.setFulfillingShopCode(fulfillingshopcode);
				messageDlca.setIdentifiers(identifiers);
				messageDlca.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDlca.setMessageStatus(dlca.getMessageStatus());
				messageDlca.setMessageText(dlca.getMessageText());
				messageDlca.setMessageType(dlca.getMessageType());
				messageDlca.setReceivingShopCode(recievingshopcode);
				messageDlca.setSendingShopCode(sendingshopcode);
				messageDlca.setSystemType(BOMConstants.SYSTEM_TYPE);
				
				aMessageOnOrder.getMessageDlca().add(messageDlca);
				

			}
		 else if (messageType.equals(BOMConstants.DLCF_MESSAGE_DESC)) {
			 

				MessageDlcf messageDlcf = new MessageDlcf();
				
			 	MessageDlcf dlcf = (MessageDlcf)message;

			 	
				messageDlcf.setFulfillingShopCode(fulfillingshopcode);
				messageDlcf.setIdentifiers(identifiers);
				messageDlcf.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDlcf.setMessageStatus(dlcf.getMessageStatus());
				messageDlcf.setDateOrderDelivered(dlcf.getDateOrderDelivered());
				messageDlcf.setSignature(dlcf.getSignature());
				messageDlcf.setMessageType(BOMConstants.DLCF_MESSAGE_TYPE);
				messageDlcf.setReceivingShopCode(recievingshopcode);
				messageDlcf.setSendingShopCode(sendingshopcode);
				messageDlcf.setSystemType(BOMConstants.SYSTEM_TYPE);
				
				aMessageOnOrder.getMessageDlcf().add(messageDlcf);
				

			}
		
		 else if (messageType.equals(BOMConstants.DLOU_MESSAGE_DESC)) {
			 

				MessageDlou messageDlou = new MessageDlou();
				
			 	MessageDlou dlou = (MessageDlou)message;

			 
			 	messageDlou.setReceivingShopCode(recievingshopcode);
			 	messageDlou.setFulfillingShopCode(fulfillingshopcode);
			 	messageDlou.setSendingShopCode(sendingshopcode);
				messageDlou.setIdentifiers(identifiers);
				messageDlou.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDlou.setMessageStatus(dlou.getMessageStatus());
				messageDlou.setMessageType(dlou.getMessageType());
				messageDlou.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDlou.setLoadedDate(dlou.getLoadedDate());
				
				aMessageOnOrder.getMessageDlou().add(messageDlou);

			}
		 else if (messageType.equals(BOMConstants.DISP_MESSAGE_DESC)) {
			 

				MessageDisp messageDisp = new MessageDisp();
				
				MessageDisp disp = (MessageDisp)message;

			 
				messageDisp.setReceivingShopCode(recievingshopcode);
				messageDisp.setFulfillingShopCode(fulfillingshopcode);
				messageDisp.setSendingShopCode(sendingshopcode);
				messageDisp.setIdentifiers(identifiers);
				messageDisp.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDisp.setMessageStatus(disp.getMessageStatus());
				messageDisp.setMessageType(disp.getMessageType());
				messageDisp.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDisp.setReasonForDispute(disp.getReasonForDispute());
				messageDisp.setAdditionalReasonForDispute(disp.getAdditionalReasonForDispute());
				messageDisp.setMessageText(disp.getMessageText());
				
				
				aMessageOnOrder.getMessageDisp().add(messageDisp);

			}
		 else if (messageType.equals(BOMConstants.DSPC_MESSAGE_DESC)) {
			 

				MessageDspc messageDspc = new MessageDspc();
				
				MessageDspc dspc = (MessageDspc)message;

			 
				messageDspc.setReceivingShopCode(recievingshopcode);
				messageDspc.setFulfillingShopCode(fulfillingshopcode);
				messageDspc.setSendingShopCode(sendingshopcode);
				messageDspc.setIdentifiers(identifiers);
				messageDspc.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDspc.setMessageStatus(dspc.getMessageStatus());
				messageDspc.setMessageType(dspc.getMessageType());
				messageDspc.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDspc.setMessageText(dspc.getMessageText());
				
				aMessageOnOrder.getMessageDspc().add(messageDspc);

			}
		 else if (messageType.equals(BOMConstants.DSPD_MESSAGE_DESC)) {
			 

				MessageDspd messageDspd = new MessageDspd();
				
				MessageDspd dspd = (MessageDspd)message;

			 
				messageDspd.setReceivingShopCode(recievingshopcode);
				messageDspd.setFulfillingShopCode(fulfillingshopcode);
				messageDspd.setSendingShopCode(sendingshopcode);
				messageDspd.setIdentifiers(identifiers);
				messageDspd.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDspd.setMessageStatus(dspd.getMessageStatus());
				messageDspd.setMessageType(dspd.getMessageType());
				messageDspd.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDspd.setMessageText(dspd.getMessageText());
				
				aMessageOnOrder.getMessageDspd().add(messageDspd);

			}
		 else if (messageType.equals(BOMConstants.DSPR_MESSAGE_DESC)) {
			 

				MessageDspr messageDspr = new MessageDspr();
				
				MessageDspr dspr = (MessageDspr)message;

			 
				messageDspr.setReceivingShopCode(recievingshopcode);
			 	messageDspr.setFulfillingShopCode(fulfillingshopcode);
			 	messageDspr.setSendingShopCode(sendingshopcode);
			 	messageDspr.setIdentifiers(identifiers);
			 	messageDspr.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 	messageDspr.setMessageStatus(dspr.getMessageStatus());
				messageDspr.setMessageType(dspr.getMessageType());
				messageDspr.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDspr.setMessageText(dspr.getMessageText());
				
				aMessageOnOrder.getMessageDspr().add(messageDspr);

			}
		
		 else if (messageType.equals(BOMConstants.DSPU_MESSAGE_DESC)) {
			 

				MessageDspu messageDspu = new MessageDspu();
				
				MessageDspu dspu = (MessageDspu)message;

			 
				messageDspu.setReceivingShopCode(recievingshopcode);
			 	messageDspu.setFulfillingShopCode(fulfillingshopcode);
			 	messageDspu.setSendingShopCode(sendingshopcode);
			 	messageDspu.setIdentifiers(identifiers);
			 	messageDspu.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 	messageDspu.setMessageStatus(dspu.getMessageStatus());
				messageDspu.setMessageType(dspu.getMessageType());
				messageDspu.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDspu.setMessageText(dspu.getMessageText());
				
				aMessageOnOrder.getMessageDspu().add(messageDspu);

			}
		
		 else if (messageType.equals(BOMConstants.INFO_MESSAGE_DESC)) {
			 

				MessageInfo messageInfo = new MessageInfo();
				
			 	MessageInfo info = (MessageInfo)message;


	
				messageInfo.setReceivingShopCode(recievingshopcode);
			    messageInfo.setSendingShopCode(sendingshopcode);
				messageInfo.setFulfillingShopCode(fulfillingshopcode);
			 	
			 	
				messageInfo.setIdentifiers(identifiers);
				messageInfo.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageInfo.setMessageStatus(info.getMessageStatus());
				messageInfo.setMessageType(info.getMessageType());
				messageInfo.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageInfo.setMessageText(info.getMessageText());
				
				aMessageOnOrder.getMessageInfo().add(messageInfo);
				

			}
		
		 else if (messageType.equals(BOMConstants.INQR_MESSAGE_DESC)) {
			 

				MessageInqr messageInqr = new MessageInqr();
				
			 	MessageInqr inqr = (MessageInqr)message;

			 	messageInqr.setReceivingShopCode(recievingshopcode);
			    messageInqr.setSendingShopCode(sendingshopcode);
				messageInqr.setFulfillingShopCode(fulfillingshopcode);
			 	
			 	
				messageInqr.setIdentifiers(identifiers);
				messageInqr.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageInqr.setMessageStatus(inqr.getMessageStatus());
				messageInqr.setMessageType(inqr.getMessageType());
				messageInqr.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageInqr.setMessageText(inqr.getMessageText());
				
				aMessageOnOrder.getMessageInqr().add(messageInqr);

			}
		
		 else if (messageType.equals(BOMConstants.PCHG_MESSAGE_DESC)) {
				 

			 MessagePchg messagePchg = new MessagePchg();

			 MessagePchg pchg = (MessagePchg)message;


			 messagePchg.setFulfillingShopCode(fulfillingshopcode);
			 messagePchg.setIdentifiers(identifiers);
			 messagePchg.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 messagePchg.setMessageStatus(pchg.getMessageStatus());
			 messagePchg.setMessageType(pchg.getMessageType());
			 messagePchg.setReceivingShopCode(recievingshopcode);
			 messagePchg.setSendingShopCode(sendingshopcode);
			 messagePchg.setSystemType(BOMConstants.SYSTEM_TYPE);
			 messagePchg.setMessageText(pchg.getMessageText());
			 messagePchg.setPrice(pchg.getPrice());

			 aMessageOnOrder.getMessagePchg().add(messagePchg);
		
			}
		
		 else if (messageType.equals(BOMConstants.RESP_MESSAGE_DESC)) {
			 
			 MessageResp messageResp = new MessageResp();

			 MessageResp resp = (MessageResp)message;

			 messageResp.setReceivingShopCode(recievingshopcode);
			 messageResp.setSendingShopCode(sendingshopcode);
			 messageResp.setFulfillingShopCode(fulfillingshopcode);
			 	
			 messageResp.setIdentifiers(identifiers);
			 messageResp.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 messageResp.setMessageStatus(resp.getMessageStatus());
			 messageResp.setMessageType(BOMConstants.RESP_MESSAGE_TYPE);
			 messageResp.setSystemType(BOMConstants.SYSTEM_TYPE);
			 messageResp.setMessageText(resp.getMessageText());

			 aMessageOnOrder.getMessageResp().add(messageResp);
		
			}
		
	 else if (messageType.equals(BOMConstants.RJCT_MESSAGE_DESC)) {
			 

			 MessageRjct messageRjct = new MessageRjct();

			 MessageRjct rjct = (MessageRjct)message;

			 messageRjct.setFulfillingShopCode(fulfillingshopcode);
			 messageRjct.setIdentifiers(identifiers);
			 messageRjct.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 messageRjct.setMessageStatus(rjct.getMessageStatus());
			 messageRjct.setMessageType(rjct.getMessageType());
			 
			 messageRjct.setReceivingShopCode(recievingshopcode);
			 messageRjct.setSendingShopCode(sendingshopcode);
			 messageRjct.setSystemType(BOMConstants.SYSTEM_TYPE);
			 messageRjct.setMessageText(rjct.getMessageText());

			 aMessageOnOrder.getMessageRjct().add(messageRjct);
		
			}
		
	 else if (messageType.equals(BOMConstants.ORDER_MESSAGE_DESC)) {
		 
		 MessageOrder messageOrder = new MessageOrder();
		 Recipient recipient = new Recipient();
			DeliveryDetails deliveryDetails = new DeliveryDetails();
			
			ShippingDetails shippingDetails = new ShippingDetails();
			OrderDetails orderDetails = new OrderDetails();
			OrderProductsInfo orderproductsInfo = new OrderProductsInfo();
			OrderProductInfoDetails aOrderProductInfoDetails = new OrderProductInfoDetails();

		 MessageOrder order = (MessageOrder)message;

			 recipient.setRecipientAddress1(order.getRecipient().getRecipientAddress1());
			 recipient.setRecipientAddress2(order.getRecipient().getRecipientAddress2());
				recipient.setRecipientCity(order.getRecipient().getRecipientCity());
				recipient.setRecipientCountryCode(order.getRecipient().getRecipientCountryCode());
				recipient.setRecipientFirstName(order.getRecipient().getRecipientFirstName());
				recipient.setRecipientLastName(order.getRecipient().getRecipientLastName());
				recipient.setRecipientPhoneNumber(order.getRecipient().getRecipientPhoneNumber());
				recipient.setRecipientState(order.getRecipient().getRecipientState());
				recipient.setRecipientZipCode(order.getRecipient().getRecipientZipCode());
		 			
				messageOrder.setRecipient(recipient);
			
			
			deliveryDetails.setDeliveryDate(order.getDeliveryDetails().getDeliveryDate());
			deliveryDetails.setFlexDeliveryDate(order.getDeliveryDetails().getFlexDeliveryDate());
			deliveryDetails.setSpecialInstruction(order.getDeliveryDetails().getSpecialInstruction());
			messageOrder.setDeliveryDetails(deliveryDetails);
			
			messageOrder.setShippingDetails(shippingDetails);
			
			
			orderDetails.setOccasionCode(order.getOrderDetails().getOccasionCode());
			orderDetails.setOrderCardMessage(order.getOrderDetails().getOrderCardMessage());
			orderDetails.setOrderType(order.getOrderDetails().getOrderType());
			orderDetails.setTotalCostOfMerchandise(order.getOrderDetails().getTotalCostOfMerchandise());

			for (OrderProductInfoDetails product : order.getOrderDetails().getOrderProductsInfo().getOrderProductInfoDetails()) {
				aOrderProductInfoDetails.setCostOfSingleProduct(product.getCostOfSingleProduct());
				aOrderProductInfoDetails.setProductDescription(product.getProductDescription());
				aOrderProductInfoDetails.setUnits(product.getUnits());
				aOrderProductInfoDetails.setProductCode(product.getProductCode());
				aOrderProductInfoDetails.setRecipe(product.getRecipe());

				orderproductsInfo.getOrderProductInfoDetails().add(aOrderProductInfoDetails);

			}

			orderDetails.setOrderProductsInfo(orderproductsInfo);

			messageOrder.setOrderDetails(orderDetails);

			aMessageOnOrder.setMessageCount(1);
			aMessageOnOrder.setMessageOrder(messageOrder);

			messageOrder.setFulfillingShopCode(fulfillingshopcode);
			///----------
			
			Identifiers newIdentifiers = new Identifiers();
			GeneralIdentifiers newGeneralIdentifiers = new GeneralIdentifiers();
			 
			 //TODO WHAT DOES THIS HAVE TO BE
			newGeneralIdentifiers.setExternalShopMessageNumber("1001");
			
			newGeneralIdentifiers.setExternalShopOrderNumber(message.getIdentifiers().getGeneralIdentifiers().getExternalShopOrderNumber());
			 
			newIdentifiers.setGeneralIdentifiers(newGeneralIdentifiers);
			
			
			//----------
			messageOrder.setIdentifiers(newIdentifiers);

			messageOrder.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			messageOrder.setMessageStatus(order.getMessageStatus());
			messageOrder.setMessageType(order.getMessageType());
			messageOrder.setReceivingShopCode(recievingshopcode);
			messageOrder.setSendingShopCode(sendingshopcode);
			messageOrder.setSystemType(BOMConstants.SYSTEM_TYPE);
			
			messageOrder.setOrderCaptureDate(order.getOrderCaptureDate());
			messageOrder.setWireServiceCode(order.getWireServiceCode());

		aMessageOnOrder.setMessageOrder(messageOrder);


		}
			
		security.setPassword(PROPERTIES.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(PROPERTIES.getProperty(BOMConstants.FSI_USERNAME));

		fsi = createForeignSystemInterface(aMessageOnOrder, security);

		return fsi;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#createOutboundMessage(java.lang.String, com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public  String createOutboundMessage(String messageType,BloomNetMessage message, String originalSendingShopCode,String currentFulfillingShopCode,
			String parentOrderNumber, String bmtSeqNumberOfOrder, String bmtSeqNumberOfMessage) throws Exception {
		
		if(PROPERTIES.isEmpty()){
			throw new Exception("Empty properties");
		}

		MessagesOnOrder aMessageOnOrder = new MessagesOnOrder();
		Security security = new Security();
		String fsi = new String();
		

		String fulfillingshopcode = new String();
		String sendingshopcode = new String();
		String recievingshopcode = new String();

		Identifiers identifiers = new Identifiers();
		GeneralIdentifiers generalIdentifiers = new GeneralIdentifiers();
		

		 generalIdentifiers.setBmtOrderNumber(parentOrderNumber);
		generalIdentifiers.setBmtSeqNumberOfMessage(bmtSeqNumberOfMessage);
		generalIdentifiers.setBmtSeqNumberOfOrder(bmtSeqNumberOfOrder);
		 
		 //TODO WHAT DOES THIS HAVE TO BE
		generalIdentifiers.setExternalShopMessageNumber("1001");
		
		 generalIdentifiers.setExternalShopOrderNumber(message.getIdentifiers().getGeneralIdentifiers().getExternalShopOrderNumber());
		 generalIdentifiers.setInwireSequenceNo(message.getIdentifiers().getGeneralIdentifiers().getInwireSequenceNo());
		 
		identifiers.setGeneralIdentifiers(generalIdentifiers);

		fulfillingshopcode = message.getFulfillingShopCode();
		sendingshopcode = message.getSendingShopCode(); 
		recievingshopcode = message.getReceivingShopCode();
		
		String orderNumber = message.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
		

		if (messageType.equals(BOMConstants.ACKF_MESSAGE_DESC)) {

			MessageAckf messageAckf = new MessageAckf();
		 	MessageAckf ackf = (MessageAckf)message;

			
			messageAckf.setFulfillingShopCode(fulfillingshopcode);
			messageAckf.setIdentifiers(identifiers);
			messageAckf.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			messageAckf.setMessageType(BOMConstants.ACKF_MESSAGE_TYPE);
			
		 	// message from current fulfilling shop, pass to original sending shop
		 	if (sendingshopcode.equals(fulfillingshopcode)){
		 		messageAckf.setReceivingShopCode(originalSendingShopCode);
		 		messageAckf.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
		 		messageAckf.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
		 	}//message from original sending shop
		 	else if (recievingshopcode.equals(fulfillingshopcode)){
		 		messageAckf.setReceivingShopCode(currentFulfillingShopCode);
		 		messageAckf.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
		 		messageAckf.setFulfillingShopCode(currentFulfillingShopCode);
		 	}
			 
			 
			messageAckf.setSystemType(BOMConstants.SYSTEM_TYPE);
			messageAckf.setMessageType(ackf.getMessageType());
			aMessageOnOrder.getMessageAckf().add(messageAckf);
			

		} else if (messageType.equals(BOMConstants.CANC_MESSAGE_DESC)) {
			

			MessageCanc messageCanc = new MessageCanc();
			
		 	MessageCanc canc = (MessageCanc)message;

		 	//Inbound message is from sending shop
		 	//TODO If no shop actively has the order, order needs to be canceled in BOM
		 	 if (recievingshopcode.equals(fulfillingshopcode)){
		 		messageCanc.setReceivingShopCode(currentFulfillingShopCode);
				messageCanc.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				messageCanc.setFulfillingShopCode(currentFulfillingShopCode);
		 	}
		 	
			messageCanc.setIdentifiers(identifiers);
			messageCanc.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			messageCanc.setMessageStatus(canc.getMessageStatus());
			messageCanc.setMessageText(canc.getMessageText());
			messageCanc.setMessageType(canc.getMessageType());
			messageCanc.setSystemType(BOMConstants.SYSTEM_TYPE);

			aMessageOnOrder.getMessageCanc().add(messageCanc);
	

		}else if (messageType.equals(BOMConstants.CONF_MESSAGE_DESC)) {
			

			MessageConf messageConf = new MessageConf();
			
		 	MessageConf conf = (MessageConf)message;

		 	//Inbound message is from fulfilling shop
		 	//TODO Order needs to be canceled in BOM.
			 if (sendingshopcode.equals(fulfillingshopcode)){
			 	messageConf.setReceivingShopCode(originalSendingShopCode);
			 	messageConf.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 	messageConf.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 	}
		 	
			messageConf.setIdentifiers(identifiers);
			messageConf.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			messageConf.setMessageStatus(conf.getMessageStatus());
			messageConf.setMessageText(conf.getMessageText());
			messageConf.setMessageType(conf.getMessageType());
			messageConf.setSystemType(BOMConstants.SYSTEM_TYPE);

			aMessageOnOrder.getMessageConf().add(messageConf);
		
		}
		
		 else if (messageType.equals(BOMConstants.DENI_MESSAGE_DESC)) {
			 

				MessageDeni messageDeni = new MessageDeni();
			 	MessageDeni deni = (MessageDeni)message;


			 	//Inbound message is from fulfilling shop
				 if (sendingshopcode.equals(fulfillingshopcode)){
				 	messageDeni.setReceivingShopCode(originalSendingShopCode);
				 	messageDeni.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	messageDeni.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	}
				messageDeni.setIdentifiers(identifiers);
				messageDeni.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDeni.setMessageStatus(deni.getMessageStatus());
				messageDeni.setMessageText(deni.getMessageText());
				messageDeni.setMessageType(deni.getMessageType());
				messageDeni.setSystemType(BOMConstants.SYSTEM_TYPE);
				
				aMessageOnOrder.getMessageDeni().add(messageDeni);
			

			}
		
		 else if (messageType.equals(BOMConstants.DLCA_MESSAGE_DESC)) {
			 

				MessageDlca messageDlca = new MessageDlca();
			 	MessageDlca dlca = (MessageDlca)message;


			 	//Inbound message is from fulfilling shop
				 if (sendingshopcode.equals(fulfillingshopcode)){
				 	messageDlca.setReceivingShopCode(originalSendingShopCode);
				 	messageDlca.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	messageDlca.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	}
				 
				String reason = new String();
				String additionalReason = new String();
				 
			    messageDlca.setIdentifiers(identifiers);
				messageDlca.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDlca.setMessageStatus(dlca.getMessageStatus());
				messageDlca.setMessageType(dlca.getMessageType());
				
				String messageText = dlca.getMessageText();
				messageDlca.setDeliveryAttemptedDate(dlca.getDeliveryAttemptedDate());//FIXME date is coming back like garbage neeed to pull from text		
				//String  deliveryDate = parseDeliveryDate(messageText);
				//messageDlca.setDeliveryAttemptedDate(deliveryDate);
				messageDlca.setMessageText(dlca.getMessageText());
				messageDlca.setSystemType(BOMConstants.SYSTEM_TYPE);
				
				RedeliveryDetails redeliveryDetails = new RedeliveryDetails();
				
				/*if (redeliveryDetails==null){//FIXME redelivery details will always be null
					int index = messageText.indexOf(",");
					String notes = messageText.substring(index+1);
					redeliveryDetails = new RedeliveryDetails();
					redeliveryDetails.setNotes(notes);
				}*/
			
				
				reason = dlca.getReasonForNonDelivery();
				additionalReason = dlca.getAdditionalReasonForNonDelivery();

				 if(reason.equals("Need Additional Information")){
					 messageDlca.setReasonForNonDelivery(BOMConstants.DLCA_NEED_INFO);
					 /*int index = messageText.indexOf(",");
					 String notes = messageText.substring(index+1);
					 redeliveryDetails.setNotes(notes);*/
					 String notes = parseNotesAdditionalInfo(messageText);
					 redeliveryDetails.setNotes(notes);

				 }
				 else  if(reason.equals("Recipient Not Available")){
					 boolean b = messageText.indexOf("Will attempt Delivery") >0;
					 boolean c = messageText.indexOf("Will attempt delivery when contacted by recipient") >0;

					 //if(additionalReason!=null){//FIXME additonal reason will always be null
						 if (b){
							 messageDlca.setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
							 String redeliverydate = parseRedeliveryDate(messageText);
							 redeliveryDetails.setDeliveryDate(redeliverydate);
						 }
						 else if (c){
							 messageDlca.setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT_CONT);
							 String notes = parseNotes(messageText);
							 redeliveryDetails.setNotes(notes);


						 }
					 
					 else{
						 messageDlca.setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
					 }
					 messageDlca.setReasonForNonDelivery(BOMConstants.DLCA_NOT_AVAIL);
				 }
				 else if(reason.equals("Incorrect Delivery Date")){
					 messageDlca.setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
					 messageDlca.setReasonForNonDelivery(BOMConstants.DLCA_INC_DATE);
					 String redeliverydate = parseRedeliveryDate(messageText);
					 redeliveryDetails.setDeliveryDate(redeliverydate);
				 }
				 else if(reason.equals("Other")){
					 boolean b = messageText.indexOf("Will attempt Delivery") >0;
					 boolean c = messageText.indexOf("Will attempt delivery when contacted by recipient") >0;

					 //if(additionalReason!=null){//FIXME additonal reason will always be null
						 if (b){
							 messageDlca.setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
							 String redeliverydate = parseRedeliveryDate(messageText);
							 redeliveryDetails.setDeliveryDate(redeliverydate);
							 }
						 else if (c){
							 messageDlca.setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT_CONT);
							 String notes = parseNotes(messageText);
							 redeliveryDetails.setNotes(notes);


						 }
					 
					 else{
						 messageDlca.setAdditionalReasonForNonDelivery(BOMConstants.DLCA_WILL_ATTEMPT);
					 }
					 messageDlca.setReasonForNonDelivery(BOMConstants.DLCA_NOT_AVAIL);
				 }
				 
					messageDlca.setRedeliveryDetails(redeliveryDetails);
					//FIXME need to parse message text for additional reason, redeliverydetails (notes and delivery date)
				
					aMessageOnOrder.getMessageDlca().add(messageDlca);
			

			}
		 else if (messageType.equals(BOMConstants.DLCF_MESSAGE_DESC)) {
			 

				MessageDlcf messageDlcf = new MessageDlcf();
				
			 	MessageDlcf dlcf = (MessageDlcf)message;

			 	//Inbound message is from fulfilling shop
				 if (sendingshopcode.equals(fulfillingshopcode)){
				 	 messageDlcf.setReceivingShopCode(originalSendingShopCode);
					 messageDlcf.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					 messageDlcf.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	}
				 
				messageDlcf.setIdentifiers(identifiers);
				messageDlcf.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDlcf.setMessageStatus(dlcf.getMessageStatus());
				if(dlcf.getDateOrderDelivered() != null)
					messageDlcf.setDateOrderDelivered(dlcf.getDateOrderDelivered());
				else
					messageDlcf.setDateOrderDelivered(DateUtil.toDateStringFormat(new Date()));
				messageDlcf.setSignature(dlcf.getSignature());
				messageDlcf.setMessageType(dlcf.getMessageType());
				messageDlcf.setSystemType(BOMConstants.SYSTEM_TYPE);
				//TODO Implement and test
				//String signature = "BloomNet " +dlcf.getSignature();
			//	messageDlcf.setSignature(signature.substring(0, 29));
				messageDlcf.setSignature("BloomNet " +dlcf.getSignature());
				
				aMessageOnOrder.getMessageDlcf().add(messageDlcf);
				

			}
		
		 else if (messageType.equals(BOMConstants.DLOU_MESSAGE_DESC)) {
			 

				MessageDlou messageDlou = new MessageDlou();
				
			 	MessageDlou dlou = (MessageDlou)message;

			 	//Inbound message is from fulfilling shop
				 if (sendingshopcode.equals(fulfillingshopcode)){
				 	 messageDlou.setReceivingShopCode(originalSendingShopCode);
					 messageDlou.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					 messageDlou.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	}
				messageDlou.setIdentifiers(identifiers);
				messageDlou.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDlou.setMessageStatus(dlou.getMessageStatus());
				messageDlou.setMessageType(dlou.getMessageType());
				messageDlou.setSystemType(BOMConstants.SYSTEM_TYPE);
				if(messageDlou.getLoadedDate() != null)
					messageDlou.setLoadedDate(dlou.getLoadedDate());
				else
					messageDlou.setLoadedDate(DateUtil.toDate(new Date()));
				
				aMessageOnOrder.getMessageDlou().add(messageDlou);
				

			}
			
		
		 else if (messageType.equals(BOMConstants.DISP_MESSAGE_DESC)) {
			 

				MessageDisp messageDisp = new MessageDisp();
				
				MessageDisp disp = (MessageDisp)message;

			 
				//Inbound message is from sending shop
			 	 if (recievingshopcode.equals(fulfillingshopcode)){
			 		messageDisp.setReceivingShopCode(currentFulfillingShopCode);
					messageDisp.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					messageDisp.setFulfillingShopCode(currentFulfillingShopCode);
			 	}
				messageDisp.setIdentifiers(identifiers);
				messageDisp.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDisp.setMessageStatus(disp.getMessageStatus());
				messageDisp.setMessageType(disp.getMessageType());
				messageDisp.setSystemType(BOMConstants.SYSTEM_TYPE);
				String incomingReasonForDispute = disp.getReasonForDispute();
				if (incomingReasonForDispute.equals("103")){
					messageDisp.setReasonForDispute("0");
					messageDisp.setAdditionalReasonForDispute("103");
				}
				else if (incomingReasonForDispute.equals("104")){
					messageDisp.setReasonForDispute("0");
					messageDisp.setAdditionalReasonForDispute("104");
				}
				else if (incomingReasonForDispute.equals("105")){
					messageDisp.setReasonForDispute("0");
					messageDisp.setAdditionalReasonForDispute("105");
				}
				else if (incomingReasonForDispute.equals("106")){
					messageDisp.setReasonForDispute("0");
					messageDisp.setAdditionalReasonForDispute("106");
					
				}
				else if (incomingReasonForDispute.equals("107")){
					messageDisp.setReasonForDispute("103");
					messageDisp.setAdditionalReasonForDispute("107");
					
				}
				else if (incomingReasonForDispute.equals("108")){
					messageDisp.setReasonForDispute("103");
					messageDisp.setAdditionalReasonForDispute("108");
					
				}
				else if (incomingReasonForDispute.equals("109")){
					messageDisp.setReasonForDispute("103");
					messageDisp.setAdditionalReasonForDispute("109");
					
				}
				else if (incomingReasonForDispute.equals("110")){
					messageDisp.setReasonForDispute("103");
					messageDisp.setAdditionalReasonForDispute("110");
					
				}
				else if (incomingReasonForDispute.equals("111")){
					messageDisp.setReasonForDispute("104");
					messageDisp.setAdditionalReasonForDispute("111");
				}
				else if (incomingReasonForDispute.equals("112")){
					messageDisp.setReasonForDispute("104");
					messageDisp.setAdditionalReasonForDispute("112");
					
				}
				else if (incomingReasonForDispute.equals("113")){
					messageDisp.setReasonForDispute("104");
					messageDisp.setAdditionalReasonForDispute("113");
					
				}
				else if (incomingReasonForDispute.equals("114")){
					messageDisp.setReasonForDispute("104");
					messageDisp.setAdditionalReasonForDispute("114");
					
				}
				else if (incomingReasonForDispute.equals("115")){
					messageDisp.setReasonForDispute("0");
					messageDisp.setAdditionalReasonForDispute("115");
					
				}
				else if (incomingReasonForDispute.equals("116")){ // timely response - no delivery
					messageDisp.setReasonForDispute("103");
					messageDisp.setAdditionalReasonForDispute("107");
					
				}
				else if (incomingReasonForDispute.equals("117")){ // timely response - late delivery
					messageDisp.setReasonForDispute("103");
					messageDisp.setAdditionalReasonForDispute("109");
					
				}
				else if (incomingReasonForDispute.equals("118")){ // timely response - delayed response
					messageDisp.setReasonForDispute("0");
					messageDisp.setAdditionalReasonForDispute("106");
				}
				
				//messageDisp.setReasonForDispute(disp.getReasonForDispute());
				//messageDisp.setAdditionalReasonForDispute(disp.getAdditionalReasonForDispute());
				messageDisp.setMessageText(disp.getMessageText());
				
				
				aMessageOnOrder.getMessageDisp().add(messageDisp);

			}
		 else if (messageType.equals(BOMConstants.DSPC_MESSAGE_DESC)) {
			 

				MessageDspc messageDspc = new MessageDspc();
				
				MessageDspc dspc = (MessageDspc)message;

			 
			 	//Inbound message is from fulfilling shop
				 if (sendingshopcode.equals(fulfillingshopcode)){
				 	 messageDspc.setReceivingShopCode(originalSendingShopCode);
					 messageDspc.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					 messageDspc.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	}
				messageDspc.setIdentifiers(identifiers);
				messageDspc.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDspc.setMessageStatus(dspc.getMessageStatus());
				messageDspc.setMessageType(dspc.getMessageType());
				messageDspc.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDspc.setMessageText(dspc.getMessageText());
				
				aMessageOnOrder.getMessageDspc().add(messageDspc);

			}
		 else if (messageType.equals(BOMConstants.DSPD_MESSAGE_DESC)) {
			 

				MessageDspd messageDspd = new MessageDspd();
				
				MessageDspd dspd = (MessageDspd)message;

			 
			 	//Inbound message is from fulfilling shop
				 if (sendingshopcode.equals(fulfillingshopcode)){
				 	 messageDspd.setReceivingShopCode(originalSendingShopCode);
					 messageDspd.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					 messageDspd.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 	}
				messageDspd.setIdentifiers(identifiers);
				messageDspd.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageDspd.setMessageStatus(dspd.getMessageStatus());
				messageDspd.setMessageType(dspd.getMessageType());
				messageDspd.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDspd.setMessageText(dspd.getMessageText());
				
				aMessageOnOrder.getMessageDspd().add(messageDspd);

			}
		 else if (messageType.equals(BOMConstants.DSPR_MESSAGE_DESC)) {
			 

				MessageDspr messageDspr = new MessageDspr();
				
				MessageDspr dspr = (MessageDspr)message;

			 
				//Inbound message is from sending shop
			 	 if (recievingshopcode.equals(fulfillingshopcode)){
			 		messageDspr.setReceivingShopCode(currentFulfillingShopCode);
					messageDspr.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					messageDspr.setFulfillingShopCode(currentFulfillingShopCode);
			 	}
			 	messageDspr.setIdentifiers(identifiers);
			 	messageDspr.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 	messageDspr.setMessageStatus(dspr.getMessageStatus());
				messageDspr.setMessageType(dspr.getMessageType());
				messageDspr.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageDspr.setMessageText(dspr.getMessageText());
				
				aMessageOnOrder.getMessageDspr().add(messageDspr);

			}
		
		 else if (messageType.equals(BOMConstants.DSPU_MESSAGE_DESC)) {
			 

				MessageDspu messageDspu = new MessageDspu();
				
				MessageDspu dspu = (MessageDspu)message;

			 
				//Inbound message is from sending shop
			 	 if (recievingshopcode.equals(fulfillingshopcode)){
			 		messageDspu.setReceivingShopCode(currentFulfillingShopCode);
			 		messageDspu.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageDspu.setFulfillingShopCode(currentFulfillingShopCode);
			 	}
			 	messageDspu.setIdentifiers(identifiers);
			 	messageDspu.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 	messageDspu.setMessageStatus(dspu.getMessageStatus());
			 	messageDspu.setMessageType(dspu.getMessageType());
			 	messageDspu.setSystemType(BOMConstants.SYSTEM_TYPE);
			 	messageDspu.setMessageText(dspu.getMessageText());
				
				aMessageOnOrder.getMessageDspu().add(messageDspu);

			}
		
		
			
		 else if (messageType.equals(BOMConstants.INFO_MESSAGE_DESC)) {
			 

				MessageInfo messageInfo = new MessageInfo();
				
			 	MessageInfo info = (MessageInfo)message;

			 	//from inbound message, if sending and fulfilling shop is equal 
			 	//then message is from fulfilling shop
			 	//on outbound, message receiving shop has to be set to original sending shop
			 	//sending and fulfilling shop will be set to z999
			 	
			 	//else if from inbound message, receiving and fulfilling shop is equal
			 	//then message is from sending shop
			 	//on outbound message receiving and fulfilling shop is set to current fulfilling shop
			 	//sending will be set to z999
				if (sendingshopcode.equals(fulfillingshopcode)){
					messageInfo.setReceivingShopCode(originalSendingShopCode);
					messageInfo.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					messageInfo.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 	}//message from original sending shop
			 	else if (recievingshopcode.equals(fulfillingshopcode)){
			 		messageInfo.setReceivingShopCode(currentFulfillingShopCode);
			 		messageInfo.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageInfo.setFulfillingShopCode(currentFulfillingShopCode);
			 	}//message from original sending shop,pass it to current fulfilling shop
			 	else {
			 		messageInfo.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageInfo.setFulfillingShopCode(currentFulfillingShopCode);
			 		messageInfo.setReceivingShopCode(currentFulfillingShopCode);
			 		
			 	}
			 	
				messageInfo.setIdentifiers(identifiers);
				messageInfo.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageInfo.setMessageStatus(info.getMessageStatus());
				messageInfo.setMessageType(info.getMessageType());
				messageInfo.setSystemType(BOMConstants.SYSTEM_TYPE);
				messageInfo.setMessageText(info.getMessageText());
				
				aMessageOnOrder.getMessageInfo().add(messageInfo);
		

			}
		
		 else if (messageType.equals(BOMConstants.INQR_MESSAGE_DESC)) {
			 

				MessageInqr messageInqr = new MessageInqr();
				
			 	MessageInqr inqr = (MessageInqr)message;

			 	// message from current fulfilling shop, pass to original sending shop
			 	if (sendingshopcode.equals(fulfillingshopcode)){
					messageInqr.setReceivingShopCode(originalSendingShopCode);
			 		messageInqr.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageInqr.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 	}//message from original sending shop
			 	else if (recievingshopcode.equals(fulfillingshopcode)){
					 messageInqr.setReceivingShopCode(currentFulfillingShopCode);
			 		messageInqr.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageInqr.setFulfillingShopCode(currentFulfillingShopCode);
			 	}//message from original sending shop,pass it to current fulfilling shop
			 	else {
			 		messageInqr.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageInqr.setFulfillingShopCode(currentFulfillingShopCode);
			 		messageInqr.setReceivingShopCode(currentFulfillingShopCode);
			 		
			 	}
				messageInqr.setIdentifiers(identifiers);
				messageInqr.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
				messageInqr.setMessageStatus(inqr.getMessageStatus());
				messageInqr.setMessageType(inqr.getMessageType());
				messageInqr.setMessageText(inqr.getMessageText());
				messageInqr.setSystemType(BOMConstants.SYSTEM_TYPE);
				
				aMessageOnOrder.getMessageInqr().add(messageInqr);
			

			}
		
		 else if (messageType.equals(BOMConstants.PCHG_MESSAGE_DESC)) {
				 

			 MessagePchg messagePchg = new MessagePchg();

			 MessagePchg pchg = (MessagePchg)message;

			//Inbound message is from sending shop
		 	 if (recievingshopcode.equals(fulfillingshopcode)){
		 		messagePchg.setReceivingShopCode(currentFulfillingShopCode);
				messagePchg.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				messagePchg.setFulfillingShopCode(currentFulfillingShopCode);
		 	}
		 	 /*else{
		 		messagePchg.setReceivingShopCode(currentFulfillingShopCode);
				messagePchg.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				messagePchg.setFulfillingShopCode(currentFulfillingShopCode);
		 	 }*/
			 messagePchg.setIdentifiers(identifiers);
			 messagePchg.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 messagePchg.setMessageStatus(pchg.getMessageStatus());
			 messagePchg.setMessageType(pchg.getMessageType());
			 messagePchg.setSystemType(BOMConstants.SYSTEM_TYPE);
			 messagePchg.setMessageText(pchg.getMessageText());
			 messagePchg.setPrice(pchg.getPrice());

			 aMessageOnOrder.getMessagePchg().add(messagePchg);
			


			}
		
		 else if (messageType.equals(BOMConstants.RESP_MESSAGE_DESC)) {
			 

			 MessageResp messageResp = new MessageResp();

			 MessageResp resp = (MessageResp)message;

				// message from current fulfilling shop, pass to original sending shop
			 	if (sendingshopcode.equals(fulfillingshopcode)){
					messageResp.setReceivingShopCode(originalSendingShopCode);
					messageResp.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
					messageResp.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 	}//message from original sending shop
			 	else if (recievingshopcode.equals(fulfillingshopcode)){
			 		messageResp.setReceivingShopCode(currentFulfillingShopCode);
			 		messageResp.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageResp.setFulfillingShopCode(currentFulfillingShopCode);
			 	}//message from original sending shop,pass it to current fulfilling shop
			 	else {
			 		messageResp.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 		messageResp.setFulfillingShopCode(currentFulfillingShopCode);
			 		messageResp.setReceivingShopCode(currentFulfillingShopCode);
			 		
			 	}
			 messageResp.setIdentifiers(identifiers);
			 messageResp.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 messageResp.setMessageStatus(resp.getMessageStatus());
			 messageResp.setMessageType(resp.getMessageType());
			 messageResp.setSystemType(BOMConstants.SYSTEM_TYPE);
			 messageResp.setMessageText(resp.getMessageText());

			 
			 aMessageOnOrder.getMessageResp().add(messageResp);
			
			}
		
	 else if (messageType.equals(BOMConstants.RJCT_MESSAGE_DESC)) {
			 

			 MessageRjct messageRjct = new MessageRjct();

			 MessageRjct rjct = (MessageRjct)message;

			//Inbound message is from fulfilling shop
			 if (sendingshopcode.equals(fulfillingshopcode)){
				 messageRjct.setReceivingShopCode(originalSendingShopCode);
				 messageRjct.setSendingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
				 messageRjct.setFulfillingShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
			 	}
			 messageRjct.setFulfillingShopCode(fulfillingshopcode);
			 messageRjct.setIdentifiers(identifiers);
			 messageRjct.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
			 messageRjct.setMessageStatus(rjct.getMessageStatus());
			 messageRjct.setMessageType(rjct.getMessageType());
			 messageRjct.setSystemType(BOMConstants.SYSTEM_TYPE);
			 messageRjct.setMessageText(rjct.getMessageText());

			 aMessageOnOrder.getMessageRjct().add(messageRjct);
			
			}
		
else if (messageType.equals(BOMConstants.ORDER_MESSAGE_DESC)) {
		 
		 MessageOrder messageOrder = new MessageOrder();
		 Recipient recipient = new Recipient();
			DeliveryDetails deliveryDetails = new DeliveryDetails();
			
			ShippingDetails shippingDetails = new ShippingDetails();
			OrderDetails orderDetails = new OrderDetails();
		
		 MessageOrder order = (MessageOrder)message;

			 recipient.setRecipientAddress1(order.getRecipient().getRecipientAddress1());
			 recipient.setRecipientAddress2(order.getRecipient().getRecipientAddress2());
				recipient.setRecipientCity(order.getRecipient().getRecipientCity());
				recipient.setRecipientCountryCode(order.getRecipient().getRecipientCountryCode());
				recipient.setRecipientFirstName(order.getRecipient().getRecipientFirstName());
				recipient.setRecipientLastName(order.getRecipient().getRecipientLastName());
				recipient.setRecipientPhoneNumber(order.getRecipient().getRecipientPhoneNumber());
				recipient.setRecipientState(order.getRecipient().getRecipientState());
				recipient.setRecipientZipCode(order.getRecipient().getRecipientZipCode());
				recipient.setRecipientAttention(order.getRecipient().getRecipientAttention());
		 			
				messageOrder.setRecipient(recipient);
			
			
			deliveryDetails.setDeliveryDate(order.getDeliveryDetails().getDeliveryDate());
			deliveryDetails.setFlexDeliveryDate(order.getDeliveryDetails().getFlexDeliveryDate());
			deliveryDetails.setSpecialInstruction(order.getDeliveryDetails().getSpecialInstruction());
			messageOrder.setDeliveryDetails(deliveryDetails);
			
			messageOrder.setShippingDetails(shippingDetails);
			
			
			orderDetails = order.getOrderDetails();


			messageOrder.setOrderDetails(orderDetails);

			aMessageOnOrder.setMessageCount(1);
			aMessageOnOrder.setMessageOrder(messageOrder);

			messageOrder.setFulfillingShopCode(fulfillingshopcode);
			
			messageOrder.setIdentifiers(identifiers);

			messageOrder.setMessageCreateTimestamp(order.getMessageCreateTimestamp());
			messageOrder.setMessageStatus(order.getMessageStatus());
			messageOrder.setMessageType(order.getMessageType());
			messageOrder.setReceivingShopCode(recievingshopcode);
			messageOrder.setSendingShopCode(sendingshopcode);
			messageOrder.setSystemType(BOMConstants.SYSTEM_TYPE);
			
			messageOrder.setOrderCaptureDate(order.getOrderCaptureDate());
			messageOrder.setWireServiceCode(order.getWireServiceCode());
			messageOrder.setContainsPerishables(order.getContainsPerishables());
			messageOrder.setPickupCode(order.getPickupCode());

		aMessageOnOrder.setMessageOrder(messageOrder);


		}
			
		security.setPassword(PROPERTIES.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(PROPERTIES.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(PROPERTIES.getProperty(BOMConstants.FSI_USERNAME));


		fsi = createForeignSystemInterface(aMessageOnOrder, security);

		return fsi;
	}
	
	
	
	private  String parseNotes(String messageText) {
		
		String notes = new String();
		
		 int notesIndex = messageText.indexOf("Will attempt delivery when contacted by recipient");
		 notes = messageText.substring(notesIndex+53);
		 
		 return notes;
	}
	
	private  String parseNotesAdditionalInfo(String messageText) {
		
		String notes = new String();
		
		 int notesIndex = messageText.indexOf(",");
		 notes = messageText.substring(notesIndex+1);
		 
		 return notes;
	}
	


	private  String parseDeliveryDate(String messageText) {
		String formattedStr  = new String();
		
		int deliveryIndex = messageText.indexOf("Delivery Attempted on ");
		int enddeliveryIndex = messageText.indexOf(",");
		String deliveryDate = messageText.substring(deliveryIndex+22, enddeliveryIndex);
		 System.out.println(deliveryDate);
		 Date formatted = DateUtil.toDateDashTimeFormat2(deliveryDate);
		 System.out.println(formatted);
		  formattedStr = DateUtil.toXml24FormatString(formatted);
		 System.out.println(formattedStr);
		 
		 return formattedStr;
	}


	private  String parseRedeliveryDate(String messageText) {
		
		String formattedStr = new String();
		try{
			int deliveryIndex = messageText.indexOf("Will attempt Delivery on : ");
			String deliveryDate = messageText.substring(deliveryIndex+27);
			System.out.println(deliveryDate);
			Date formatted = DateUtil.toDateDashTimeFormat2(deliveryDate);
			System.out.println(formatted);
			formattedStr = DateUtil.toXml24FormatString(formatted);
			System.out.println(formattedStr);
			 
		 }catch(Exception ee){}
		 
		 if(formattedStr == null || formattedStr.equals("")){
			 try{
				int deliveryIndex = messageText.indexOf("Will attempt Delivery : "); 
				String deliveryDate = messageText.substring(deliveryIndex+24);
				Date formatted = DateUtil.toDateDashFormat2 (deliveryDate);
				formattedStr = DateUtil.toXml24FormatString(formatted);
			 }catch(Exception ee){}
		 }
		
		return formattedStr;
	}


	//XXX
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#convertToTFSIXML(com.bloomnet.bom.common.tfsi.Transaction)
	 */
	@Override
	public  String convertToTFSIXML(Transaction transaction){ //marshall
		 
		 StringWriter xml = new StringWriter();

		  try {
			JAXBContext jc = JAXBContext.newInstance( BOMConstants.TFSI_JAXB_PACKAGE );
			Marshaller m = jc.createMarshaller();
			//m.marshal( foreignSystemInterface, System.out );
			m.marshal( transaction,xml);
			
		  } catch (JAXBException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		return xml.toString();

		
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#convertToMDIXML(com.bloomnet.bom.common.jaxb.mdi.MemberDirectoryInterface)
	 */
	@Override
	public  String convertToMDIXML(MemberDirectoryInterface memberDirectoryInterface){ //marshall
		  StringWriter xml = new StringWriter();

		  try {
			JAXBContext jc = JAXBContext.newInstance( BOMConstants.MDI_JAXB_PACKAGE);
			Marshaller m = jc.createMarshaller();
			//m.marshal( foreignSystemInterface, System.out );
			m.marshal( memberDirectoryInterface,xml);
			
			
			System.out.println(xml.toString());

			
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return xml.toString();
		
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#convertToJavaMDI(java.lang.String)
	 */
	@Override
	public  MemberDirectoryInterface convertToJavaMDI(String xmlString) {

		MemberDirectoryInterface memberDirectoryInterface = new MemberDirectoryInterface();
		try {
			JAXBContext jc = JAXBContext
					.newInstance(BOMConstants.MDI_JAXB_PACKAGE);
			Unmarshaller unmarshaller = jc.createUnmarshaller();

			JAXBElement<MemberDirectoryInterface> jaxbElem = unmarshaller
					.unmarshal(new StreamSource(new StringReader(xmlString)),
							MemberDirectoryInterface.class);

			memberDirectoryInterface = jaxbElem.getValue();

		} catch (Exception e) {
			e.printStackTrace();
		}
		return memberDirectoryInterface;
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#createBeanFromFSI(com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface, com.bloomnet.bom.common.entity.Shop, com.bloomnet.bom.common.entity.Shop, com.bloomnet.bom.common.entity.Shop)
	 */
	@Override
	public  MessageOnOrderBean createBeanFromFSI(ForeignSystemInterface fsi,Shop receivingShop, Shop sendingShop, Shop fulfillingShop) throws Exception{

		MessageOnOrderBean orderbean = new MessageOnOrderBean();

		MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();
		
		long bmtSeqNumberOfMessage = 0;
		long bmtSeqNumberOfOrder = 0;
		
		String bmtOrderNumberStr = messageOrder.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
		
		String bmtSeqNumberOfMessageStr = messageOrder.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();
		
		if(bmtSeqNumberOfMessageStr!= null){
			if (bmtSeqNumberOfMessageStr.isEmpty()){
				bmtSeqNumberOfMessageStr="0";
			}else{
			bmtSeqNumberOfMessage = Long.parseLong(bmtSeqNumberOfMessageStr);
			}
		}
		String bmtSeqNumberOfOrderStr = messageOrder.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfOrder();
		
		if(bmtSeqNumberOfOrderStr!= null){
			if (bmtSeqNumberOfOrderStr.isEmpty()){
				bmtSeqNumberOfOrderStr="0";
			}else{
			bmtSeqNumberOfOrder = Long.parseLong(bmtSeqNumberOfOrderStr);
			}
		}
		
		orderbean.setBmtOrderNumber(bmtOrderNumberStr);
		orderbean.setBmtSeqNumberOfMessage(bmtSeqNumberOfMessage);
		orderbean.setBmtSeqNumberOfOrder(bmtSeqNumberOfOrder);
		
		orderbean.setRecipientAttention(messageOrder.getRecipient().getRecipientAttention());
		orderbean.setRecipientAddress1(messageOrder.getRecipient().getRecipientAddress1());
		orderbean.setRecipientAddress2(messageOrder.getRecipient().getRecipientAddress2());
		orderbean.setRecipientCity(messageOrder.getRecipient().getRecipientCity());
		orderbean.setRecipientCountryCode(messageOrder.getRecipient().getRecipientCountryCode());
		orderbean.setRecipientFirstName(messageOrder.getRecipient().getRecipientFirstName());
		orderbean.setRecipientLastName(messageOrder.getRecipient().getRecipientLastName());
		orderbean.setRecipientPhoneNumber(messageOrder.getRecipient().getRecipientPhoneNumber());
		orderbean.setRecipientState(messageOrder.getRecipient().getRecipientState());
		orderbean.setRecipientZipCode(messageOrder.getRecipient().getRecipientZipCode());
		
		orderbean.setReceivingShop(receivingShop);
		orderbean.setSendingShop(sendingShop);
		orderbean.setFulfillingShop(fulfillingShop);
		
		orderbean.setDeliveryDate(messageOrder.getDeliveryDetails().getDeliveryDate());
		orderbean.setFlexDeliveryDate(messageOrder.getDeliveryDetails().getFlexDeliveryDate());
		String specialInstructions = messageOrder.getDeliveryDetails().getSpecialInstruction();
		String decodedInstr = URLDecoder.decode(specialInstructions, "UTF-8");
		orderbean.setSpecialInstruction(decodedInstr);
		
		orderbean.setCaptureDate(DateUtil.toDate(messageOrder.getOrderCaptureDate()));
		orderbean.setOrderNumber(messageOrder.getOrderDetails().getOrderNumber());
		if(orderbean.getOrderNumber() == null){
			orderbean.setOrderNumber(messageOrder.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber());
		}
		
		orderbean.setOccasionId(messageOrder.getOrderDetails().getOccasionCode());
		String cardMessage = messageOrder.getOrderDetails().getOrderCardMessage();
		String decodedCrd = URLDecoder.decode(cardMessage, "UTF-8");
		orderbean.setCardMessage(decodedCrd);
		
		orderbean.setTotalCost(messageOrder.getOrderDetails().getTotalCostOfMerchandise());
		
		orderbean.setOrderMessageType(messageOrder.getOrderDetails().getOrderTypeMessage());
		
		orderbean.setShipperCode(messageOrder.getShippingDetails().getShipperCode());
		
		orderbean.setShippingDate(DateUtil.toDate(messageOrder.getShippingDetails().getShippingDate()));
		orderbean.setShippingMethod(messageOrder.getShippingDetails().getShippingMethod());
		orderbean.setTrackingNumber(messageOrder.getShippingDetails().getTrackingNumber());
		
		List<OrderProductInfoDetails> products = new ArrayList<OrderProductInfoDetails>();
		
		for (OrderProductInfoDetails product : messageOrder.getOrderDetails().getOrderProductsInfo().getOrderProductInfoDetails()) {
			OrderProductInfoDetails aOrderProductInfoDetails = new OrderProductInfoDetails();

			String description = product.getProductDescription();
			String decodedDesc = URLDecoder.decode(description, "UTF-8");
			aOrderProductInfoDetails.setProductDescription(decodedDesc);
			aOrderProductInfoDetails.setCostOfSingleProduct(product.getCostOfSingleProduct());
			aOrderProductInfoDetails.setUnits(product.getUnits());
			aOrderProductInfoDetails.setProductCode(product.getProductCode());
			aOrderProductInfoDetails.setRecipe(product.getRecipe());


			products.add(aOrderProductInfoDetails);
		}
		
		orderbean.setProducts(products);
		orderbean.setWireServiceCode(messageOrder.getWireServiceCode().value());
		orderbean.setOrderType( BOMConstants.ORDER_TYPE_BMT);
		orderbean.setMessageType(Integer.parseInt(messageOrder.getMessageType()));
		
		return orderbean;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.util.TransformService#createBeanFromTFSI(java.lang.String, com.bloomnet.bom.common.entity.Shop, com.bloomnet.bom.common.entity.Shop)
	 */
	@Override
	@SuppressWarnings("unchecked")
	public  MessageOnOrderBean createBeanFromTFSI(String tfsiXML, Shop sendingShop, Shop receivingShop) throws JDOMException, IOException{

		TFSIUtil tfsiUtil = new TFSIUtil();

		MessageOnOrderBean moob = new MessageOnOrderBean();

		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" + tfsiXML.split("<TFETransactionHeader>")[1].split("</TFETransactionHeader>")[0] + "</TFETransactionHeader>";
		String body = tfsiXML.split("</TFETransactionHeader>")[1];

		SAXBuilder builder = new SAXBuilder();

		Document document = builder.build(new ByteArrayInputStream(header.getBytes()));

		Element rootElement = document.getRootElement();

		String seqNumber = rootElement.getChild("SenderSequenceNumber").getText();

		document = builder.build(new ByteArrayInputStream(body.getBytes()));

		rootElement = document.getRootElement();

		String occassionCode = rootElement.getAttributeValue("OccassionCode");
		String recipientFirstName = rootElement.getChild("Recipient").getChild("FirstName").getText();
		String recipientLastName = rootElement.getChild("Recipient").getChild("LastName").getText();
		List<Element> addressLines = rootElement.getChild("Recipient").getChild("AddressLines").getChildren("AddressLine");
		String recipientCity = rootElement.getChild("Recipient").getChild("City").getText();
		String recipientState = rootElement.getChild("Recipient").getChild("State").getText();
		String recipientZip = rootElement.getChild("Recipient").getChild("PostalCode").getText();
		String recipientPhone = ((Element) rootElement.getChild("Recipient").getChild("Phones").getChildren("Phone").get(0)).getText();

		List<Element> messageLines = rootElement.getChild("CardMessageLines").getChildren("CardMessage");
		List<Element> products = rootElement.getChild("Products").getChildren("Product");
		List<Element> instructionLines = rootElement.getChild("Instructions").getChildren("Instruction");

		String message = "";
		for(int ii=0; ii<messageLines.size(); ++ii){
			message += messageLines.get(ii).getText();
		}

		String instructions = "";
		for(int ii=0; ii<instructionLines.size(); ++ii){
			instructions += instructionLines.get(ii).getText();
		}

		List<OrderProductInfoDetails> productList = new ArrayList<OrderProductInfoDetails>();
		List<Element> productDescriptions;

		if(products.size() == 1){
			productDescriptions = products.get(0).getChild("DescLine").getChildren("DescLine");
		}else{
			productDescriptions = products.get(0).getChild("DescLine").getChildren("DescLine");
			productDescriptions.addAll(products.get(1).getChild("DescLine").getChildren("DescLine"));
		}

		for(int ii=0; ii<productDescriptions.size(); ++ii){
			OrderProductInfoDetails details = new OrderProductInfoDetails();
			details.setCostOfSingleProduct("0.00");
			details.setProductDescription(productDescriptions.get(ii).getText().split("of")[1]);
			details.setUnits(Integer.valueOf(productDescriptions.get(ii).getText().split(" ")[0]));
			productList.add(details);
		}

		String deliveryDate = rootElement.getChild("DeliveryDate").getText();
		String flexDeliveryDate = rootElement.getChild("FlexDeliveryDate").getText();
		String price = rootElement.getChild("Price").getText();
		String orderDate = rootElement.getChild("OrderDate").getText();

		moob.setBmtSeqNumberOfOrder(Integer.valueOf(seqNumber));
		moob.setCommMethod(Byte.valueOf("1"));
		moob.setDeliveryDate(deliveryDate);
		if (flexDeliveryDate!=null){
		moob.setFlexDeliveryDate(flexDeliveryDate);
		}
		moob.setFulfillingShop(receivingShop);
		moob.setReceivingShop(receivingShop);
		moob.setSendingShop(sendingShop);
		moob.setOccasionId(occassionCode);
		moob.setRecipientAddress1(addressLines.get(0).getText());
		if(addressLines.size()>1) moob.setRecipientAddress2(addressLines.get(1).getText());
		moob.setRecipientCity(recipientCity);
		moob.setRecipientFirstName(recipientFirstName);
		moob.setRecipientLastName(recipientLastName);
		moob.setRecipientPhoneNumber(recipientPhone);
		moob.setRecipientState(recipientState);
		moob.setRecipientZipCode(recipientZip);
		moob.setCardMessage(message);
		moob.setSpecialInstruction(instructions);
		moob.setTotalCost(price);
		moob.setDeliveryDate(deliveryDate);
		moob.setCaptureDate(tfsiUtil.stringDateToDateDate(orderDate));
		moob.setProducts(productList);

		return moob;
	}
}
