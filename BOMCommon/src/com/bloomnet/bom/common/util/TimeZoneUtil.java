package com.bloomnet.bom.common.util;

import com.bloomnet.bom.common.constants.BOMConstants;

public class TimeZoneUtil {
	
	public static String getTimeZoneDescription(String timeZoneId){
		
		String timeZone="";
		
		if (timeZoneId.equals(BOMConstants.SAMOA_TIME)){
			timeZone="SAMOA";
		}
		else if (timeZoneId.equals(BOMConstants.HAWAII_TIME)){
			timeZone="HAWAII";
		}
		else if (timeZoneId.equals(BOMConstants.ALASKA_TIME)){
			timeZone="ALASKA";
		}
		else if (timeZoneId.equals(BOMConstants.PACIFIC_TIME)){
			timeZone="PACIFIC";
		}
		else if (timeZoneId.equals(BOMConstants.MTN_TIME)){
			timeZone="MOUNTAIN";
		}
		else if (timeZoneId.equals(BOMConstants.CENTRAL_TIME)){
			timeZone="CENTRAL";
		}
		else if (timeZoneId.equals(BOMConstants.EAST_TIME)){
			timeZone="EASTERN";
		}
		else if (timeZoneId.equals(BOMConstants.ATLANTIC_TIME)){
			timeZone="ATLANTIC";
		}
		else if (timeZoneId.equals(BOMConstants.NEWFOUNDLAND_TIME)){
			timeZone="NEWFOUNDLAND";
		}
		else if (timeZoneId.equals(BOMConstants.UTC9_TIME)){
			timeZone="UTC+9";
		}
		else if (timeZoneId.equals(BOMConstants.UTC10_TIME)){
			timeZone="UTC+10";
		}
		else if (timeZoneId.equals(BOMConstants.UTC11_TIME)){
			timeZone="UTC+11";
		}
		else if (timeZoneId.equals(BOMConstants.UTC12_TIME)){
			timeZone="UTC+12";
		}
		
		
		return timeZone;
		
	}

}
