package com.bloomnet.bom.common.bean;

public class OutstandingOrderBean {
	
	private String orderNumber;
	private String orderDate;
	private String deliveryDate;
	private String sendingShop;
	private String city;
	private String state;
	private String zip;
	private String user;
	private String note;
	private String timeOfNote;
	private String disposition;
	private String dispositionText;
	private String timeOfDisposition;
	
	
	public OutstandingOrderBean(String orderNumber, String orderDate,
			String deliveryDate, String sendingShop, String city, String state,
			String zip, String user, String note, String timeOfNote,
			String disposition, String dispositionText, String timeOfDisposition) {
		super();
		this.orderNumber = orderNumber;
		this.orderDate = orderDate;
		this.deliveryDate = deliveryDate;
		this.sendingShop = sendingShop;
		this.city = city;
		this.state = state;
		this.zip = zip;
		this.user = user;
		this.note = note;
		this.timeOfNote = timeOfNote;
		this.disposition = disposition;
		this.dispositionText = dispositionText;
		this.timeOfDisposition = timeOfDisposition;
	}


	public String getOrderNumber() {
		return orderNumber;
	}


	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}


	public String getOrderDate() {
		return orderDate;
	}


	public void setOrderDate(String orderDate) {
		this.orderDate = orderDate;
	}


	public String getDeliveryDate() {
		return deliveryDate;
	}


	public void setDeliveryDate(String deliveryDate) {
		this.deliveryDate = deliveryDate;
	}


	public String getSendingShop() {
		return sendingShop;
	}


	public void setSendingShop(String sendingShop) {
		this.sendingShop = sendingShop;
	}


	public String getCity() {
		return city;
	}


	public void setCity(String city) {
		this.city = city;
	}


	public String getState() {
		return state;
	}


	public void setState(String state) {
		this.state = state;
	}


	public String getZip() {
		return zip;
	}


	public void setZip(String zip) {
		this.zip = zip;
	}


	public String getUser() {
		return user;
	}


	public void setUser(String user) {
		this.user = user;
	}


	public String getNote() {
		return note;
	}


	public void setNote(String note) {
		this.note = note;
	}


	public String getTimeOfNote() {
		return timeOfNote;
	}


	public void setTimeOfNote(String timeOfNote) {
		this.timeOfNote = timeOfNote;
	}


	public String getDisposition() {
		return disposition;
	}


	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}


	public String getDispositionText() {
		return dispositionText;
	}


	public void setDispositionText(String dispositionText) {
		this.dispositionText = dispositionText;
	}


	public String getTimeOfDisposition() {
		return timeOfDisposition;
	}


	public void setTimeOfDisposition(String timeOfDisposition) {
		this.timeOfDisposition = timeOfDisposition;
	}
	
	
	

}
