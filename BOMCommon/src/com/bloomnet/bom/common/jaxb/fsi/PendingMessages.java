//
// This file was generated by the JavaTM Architecture for XML Binding(JAXB) Reference Implementation, vJAXB 2.1.10 in JDK 6 
// See <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Any modifications to this file will be lost upon recompilation of the source schema. 
// Generated on: 2011.09.08 at 03:23:15 PM EDT 
//


package com.bloomnet.bom.common.jaxb.fsi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for pendingMessages complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="pendingMessages">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="total" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numOfOrderMessages" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numOfAckfMessages" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="numOfGeneralMessages" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "pendingMessages", propOrder = {
    "total",
    "numOfOrderMessages",
    "numOfAckfMessages",
    "numOfGeneralMessages"
})
public class PendingMessages {

    @XmlElement(required = true)
    protected String total;
    @XmlElement(required = true)
    protected String numOfOrderMessages;
    @XmlElement(required = true)
    protected String numOfAckfMessages;
    protected String numOfGeneralMessages;

    /**
     * Gets the value of the total property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTotal() {
        return total;
    }

    /**
     * Sets the value of the total property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTotal(String value) {
        this.total = value;
    }

    /**
     * Gets the value of the numOfOrderMessages property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfOrderMessages() {
        return numOfOrderMessages;
    }

    /**
     * Sets the value of the numOfOrderMessages property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfOrderMessages(String value) {
        this.numOfOrderMessages = value;
    }

    /**
     * Gets the value of the numOfAckfMessages property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfAckfMessages() {
        return numOfAckfMessages;
    }

    /**
     * Sets the value of the numOfAckfMessages property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfAckfMessages(String value) {
        this.numOfAckfMessages = value;
    }

    /**
     * Gets the value of the numOfGeneralMessages property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumOfGeneralMessages() {
        return numOfGeneralMessages;
    }

    /**
     * Sets the value of the numOfGeneralMessages property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumOfGeneralMessages(String value) {
        this.numOfGeneralMessages = value;
    }

}
