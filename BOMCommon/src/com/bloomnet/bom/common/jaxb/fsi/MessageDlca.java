package com.bloomnet.bom.common.jaxb.fsi;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for messageDlca complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="messageDlca">
 *   &lt;complexContent>
 *     &lt;extension base="{}bloomNetMessage">
 *       &lt;sequence>
 *         &lt;element name="deliveryAttemptedDate" type="{}timestampFormat"/>
 *         &lt;element name="reasonForNonDelivery" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="additionalReasonForNonDelivery" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="redeliveryDetails" type="{}redeliveryDetails" minOccurs="0"/>
 *         &lt;element name="messageText" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "messageDlca", propOrder = {
    "deliveryAttemptedDate",
    "reasonForNonDelivery",
    "additionalReasonForNonDelivery",
    "redeliveryDetails",
    "messageText"
})
public class MessageDlca
    extends BloomNetMessage
{

    @XmlElement(required = true)
    protected String deliveryAttemptedDate;
    @XmlElement(required = true)
    protected String reasonForNonDelivery;
    protected String additionalReasonForNonDelivery;
    protected RedeliveryDetails redeliveryDetails;
    protected String messageText;

    /**
     * Gets the value of the deliveryAttemptedDate property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDeliveryAttemptedDate() {
        return deliveryAttemptedDate;
    }

    /**
     * Sets the value of the deliveryAttemptedDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDeliveryAttemptedDate(String value) {
        this.deliveryAttemptedDate = value;
    }

    /**
     * Gets the value of the reasonForNonDelivery property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getReasonForNonDelivery() {
        return reasonForNonDelivery;
    }

    /**
     * Sets the value of the reasonForNonDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setReasonForNonDelivery(String value) {
        this.reasonForNonDelivery = value;
    }

    /**
     * Gets the value of the additionalReasonForNonDelivery property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalReasonForNonDelivery() {
        return additionalReasonForNonDelivery;
    }

    /**
     * Sets the value of the additionalReasonForNonDelivery property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalReasonForNonDelivery(String value) {
        this.additionalReasonForNonDelivery = value;
    }

    /**
     * Gets the value of the redeliveryDetails property.
     * 
     * @return
     *     possible object is
     *     {@link RedeliveryDetails }
     *     
     */
    public RedeliveryDetails getRedeliveryDetails() {
        return redeliveryDetails;
    }

    /**
     * Sets the value of the redeliveryDetails property.
     * 
     * @param value
     *     allowed object is
     *     {@link RedeliveryDetails }
     *     
     */
    public void setRedeliveryDetails(RedeliveryDetails value) {
        this.redeliveryDetails = value;
    }

    /**
     * Gets the value of the messageText property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessageText() {
        return messageText;
    }

    /**
     * Sets the value of the messageText property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessageText(String value) {
        this.messageText = value;
    }

}
