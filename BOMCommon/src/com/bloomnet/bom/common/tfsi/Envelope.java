
package com.bloomnet.bom.common.tfsi;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for Envelope complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="Envelope">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Transactions" type="{http://www.teleflora.com/webservices/}ArrayOfTransaction" minOccurs="0"/>
 *         &lt;element name="OverallResult" type="{http://www.teleflora.com/webservices/}Result" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="GUID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="UserID" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Password" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TransactionCount" use="required" type="{http://www.w3.org/2001/XMLSchema}int" />
 *       &lt;attribute name="WaitTimeBeforeNextCall" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="TWSXMLVersion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CommsSoftwareVersion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="CommsVendor" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SoftwareVendor" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SoftwareVersion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="DatabaseVersion" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="ConnectionInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="EnvironmentInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MachineInfo" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "Envelope", propOrder = {
    "transactions",
    "overallResult"
})
public class Envelope {

    @XmlElement(name = "Transactions")
    protected ArrayOfTransaction transactions;
    @XmlElement(name = "OverallResult")
    protected Result overallResult;
    @XmlAttribute(name = "GUID")
    protected String guid;
    @XmlAttribute(name = "UserID")
    protected String userID;
    @XmlAttribute(name = "Password")
    protected String password;
    @XmlAttribute(name = "TransactionCount", required = true)
    protected int transactionCount;
    @XmlAttribute(name = "WaitTimeBeforeNextCall")
    protected String waitTimeBeforeNextCall;
    @XmlAttribute(name = "TWSXMLVersion")
    protected String twsxmlVersion;
    @XmlAttribute(name = "CommsSoftwareVersion")
    protected String commsSoftwareVersion;
    @XmlAttribute(name = "CommsVendor")
    protected String commsVendor;
    @XmlAttribute(name = "SoftwareVendor")
    protected String softwareVendor;
    @XmlAttribute(name = "SoftwareVersion")
    protected String softwareVersion;
    @XmlAttribute(name = "DatabaseVersion")
    protected String databaseVersion;
    @XmlAttribute(name = "ConnectionInfo")
    protected String connectionInfo;
    @XmlAttribute(name = "EnvironmentInfo")
    protected String environmentInfo;
    @XmlAttribute(name = "MachineInfo")
    protected String machineInfo;

    /**
     * Gets the value of the transactions property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfTransaction }
     *     
     */
    public ArrayOfTransaction getTransactions() {
        return transactions;
    }

    /**
     * Sets the value of the transactions property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfTransaction }
     *     
     */
    public void setTransactions(ArrayOfTransaction value) {
        this.transactions = value;
    }

    /**
     * Gets the value of the overallResult property.
     * 
     * @return
     *     possible object is
     *     {@link Result }
     *     
     */
    public Result getOverallResult() {
        return overallResult;
    }

    /**
     * Sets the value of the overallResult property.
     * 
     * @param value
     *     allowed object is
     *     {@link Result }
     *     
     */
    public void setOverallResult(Result value) {
        this.overallResult = value;
    }

    /**
     * Gets the value of the guid property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getGUID() {
        return guid;
    }

    /**
     * Sets the value of the guid property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setGUID(String value) {
        this.guid = value;
    }

    /**
     * Gets the value of the userID property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUserID() {
        return userID;
    }

    /**
     * Sets the value of the userID property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUserID(String value) {
        this.userID = value;
    }

    /**
     * Gets the value of the password property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the value of the password property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Gets the value of the transactionCount property.
     * 
     */
    public int getTransactionCount() {
        return transactionCount;
    }

    /**
     * Sets the value of the transactionCount property.
     * 
     */
    public void setTransactionCount(int value) {
        this.transactionCount = value;
    }

    /**
     * Gets the value of the waitTimeBeforeNextCall property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaitTimeBeforeNextCall() {
        return waitTimeBeforeNextCall;
    }

    /**
     * Sets the value of the waitTimeBeforeNextCall property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaitTimeBeforeNextCall(String value) {
        this.waitTimeBeforeNextCall = value;
    }

    /**
     * Gets the value of the twsxmlVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTWSXMLVersion() {
        return twsxmlVersion;
    }

    /**
     * Sets the value of the twsxmlVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTWSXMLVersion(String value) {
        this.twsxmlVersion = value;
    }

    /**
     * Gets the value of the commsSoftwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommsSoftwareVersion() {
        return commsSoftwareVersion;
    }

    /**
     * Sets the value of the commsSoftwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommsSoftwareVersion(String value) {
        this.commsSoftwareVersion = value;
    }

    /**
     * Gets the value of the commsVendor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCommsVendor() {
        return commsVendor;
    }

    /**
     * Sets the value of the commsVendor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCommsVendor(String value) {
        this.commsVendor = value;
    }

    /**
     * Gets the value of the softwareVendor property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftwareVendor() {
        return softwareVendor;
    }

    /**
     * Sets the value of the softwareVendor property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftwareVendor(String value) {
        this.softwareVendor = value;
    }

    /**
     * Gets the value of the softwareVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSoftwareVersion() {
        return softwareVersion;
    }

    /**
     * Sets the value of the softwareVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSoftwareVersion(String value) {
        this.softwareVersion = value;
    }

    /**
     * Gets the value of the databaseVersion property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDatabaseVersion() {
        return databaseVersion;
    }

    /**
     * Sets the value of the databaseVersion property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDatabaseVersion(String value) {
        this.databaseVersion = value;
    }

    /**
     * Gets the value of the connectionInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getConnectionInfo() {
        return connectionInfo;
    }

    /**
     * Sets the value of the connectionInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setConnectionInfo(String value) {
        this.connectionInfo = value;
    }

    /**
     * Gets the value of the environmentInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEnvironmentInfo() {
        return environmentInfo;
    }

    /**
     * Sets the value of the environmentInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEnvironmentInfo(String value) {
        this.environmentInfo = value;
    }

    /**
     * Gets the value of the machineInfo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMachineInfo() {
        return machineInfo;
    }

    /**
     * Sets the value of the machineInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMachineInfo(String value) {
        this.machineInfo = value;
    }

}
