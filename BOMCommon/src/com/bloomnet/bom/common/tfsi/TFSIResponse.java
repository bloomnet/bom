package com.bloomnet.bom.common.tfsi;

import java.util.ArrayList;
import java.util.List;


public class TFSIResponse {
	private boolean isSuccess;
	private String message;
	private List<TFSIMessage> tfsiMessages = new ArrayList<TFSIMessage>();
	
	public void setSuccess(boolean isSuccess) {
		this.isSuccess = isSuccess;
	}
	public boolean isSuccess() {
		return isSuccess;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getMessage() {
		return message;
	}
	public void setTfsiMessages(TFSIMessage tfsiMessage) {
		tfsiMessages.add(tfsiMessage);
	}
	public List<TFSIMessage> getTfsiMessages() {
		return tfsiMessages;
	}

}
