package com.bloomnet.bom.common.tfsi;

import java.text.CharacterIterator;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.StsMessage;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Zip;

public class TFSIUtil {
	
	public String getDate(Date date){
		String stringDate;
		String DATE_FORMAT = "yyyyMMdd";
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
	    Date d1 = date;
	    stringDate = sdf.format(d1.getTime());
	    return stringDate;
	}
	
	public String getDate(String date){
		String stringDate;
		String DATE_FORMAT = "MM/dd/yyyy";
		String END_DATE_FORMAT = "yyyyMMdd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date d1;
		try {
			d1 = sdf.parse(date);
			SimpleDateFormat sdf2 = new SimpleDateFormat(END_DATE_FORMAT);
			stringDate = sdf2.format(d1);
			return stringDate;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.err.println("error processing a Date date to a String date in the format of yyyyMMdd");
		return null;
	}
	
	public Date stringDateToDateDate(String date){
		String DATE_FORMAT  = "yyyyMMdd";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		try {
			Date d1 = sdf.parse(date);
			return d1;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.err.println("error processing a String date to a Date date");
		return null;
	}
	
	public Date stringDateToDateDate2(String date){
		String DATE_FORMAT  = "MM/dd/yyyy";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		try {
			Date d1 = sdf.parse(date);
			return d1;
		} catch (ParseException e) {
			e.printStackTrace();
		}
		System.err.println("error processing a String date to a Date date");
		return null;
	}
	
	public List<String> splitMessageIntoLines(String message){
		
		List<String> lines = new ArrayList<String>();
		
		if(message.length() == 0){
			
			lines.add("");
			return lines;
			
		}
		
		if(message.split(" ").length == 1 && message.length() > 50 ){
			
			lines.add("Card Message on File");
			return lines;
		}
		
		final StringCharacterIterator iterator = new StringCharacterIterator(message);
		
		StringBuilder result = new StringBuilder();
		
	    char character =  iterator.current();
	    
	    int numCurrChar= 0;
	    int numTotalChar = 0;
	    int lastWhiteSpace = 0;
	    int messageLength = message.length();
	    
	    String leftovers = "";
	    
	    while (character != CharacterIterator.DONE ){
	    	
	    	if((numTotalChar + 1) == messageLength){
	    		
	    		result.append(character);
    			lines.add(result.toString());
	    		
	    	}else if(Double.valueOf(result.toString().length()/59) == 1.00){
	    		
	    		if(character == ' '){
	    			result.append(character);
	    			lines.add(result.toString());
	    			result = new StringBuilder();
	    			lastWhiteSpace = 0;
	    			numCurrChar = 0;
	    		}else{
	    			lines.add(result.toString().substring(0,60-(60-lastWhiteSpace)));
	    			leftovers = result.toString().substring(60-(60-lastWhiteSpace), result.toString().length());
	    			result = new StringBuilder();
	    			result.append(leftovers);
	    			result.append(character);
	    			lastWhiteSpace = leftovers.length();
	    			numCurrChar = leftovers.length();
	    		}
	    		
	    	}else{
	    		if(character == ' ') lastWhiteSpace = numCurrChar;
	    		result.append(character);
	    	}
	    	numCurrChar++;
	    	numTotalChar++;
	    	character = iterator.next();
	    }
	    
		return lines;
	}
	
	public int getNumberOfLinesForMessageLines(String message){
		int length = message.length();
		int one = (length/60);
		if(one == 0) return 1;
		int two = (length-(60*one));
		int three = two%60;
		int finalNumber = one;
		if(three == 0) return finalNumber;
		else return finalNumber+1;
	}
	
	public void saveOrder(OrderDAO orderDAO, MessageOnOrderBean moob, String tempOrderNumber, String header, String body, 
			long orderSequenceNumber, TFSIResponse response, Properties bomProperties){
		byte commid = moob.getCommMethod();
		final Commmethod commmethod = orderDAO.getCommmethodById(commid);
		City city = new City();
		try {
			city = orderDAO.getCityByNameAndState(moob.getRecipientCity(), moob.getRecipientState());
		} catch (Exception ee) {
			ee.printStackTrace();
		}
		if(city == null || city.getCityId() == null){
			try{
				city = orderDAO.getCityByZip(moob.getRecipientZipCode(), moob.getRecipientCity());
			}catch(Exception ee){
				ee.printStackTrace();
			}
		}
		Zip zip = orderDAO.getZipByCode(moob.getRecipientZipCode());
		Bomorder bo = new Bomorder(tempOrderNumber,
				BOMConstants.ORDER_TYPE_TEL,
				header+body,
				city,
				zip,
				commmethod,
				new Date(),
				stringDateToDateDate(getDate(moob.getDeliveryDate())),
				Double.valueOf(moob.getTotalCost()),
				moob.getReceivingShop(),
				moob.getSendingShop(),
				String.valueOf(orderSequenceNumber),
				"");
		if(response.isSuccess() == true){
			
			try {
				
				orderDAO.persistChildOrderAndActivity(bo, 
						moob.getOrderNumber(),
						"TFSI", 
					    BOMConstants.ACT_ROUTING, 
						bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP), 
						BOMConstants.TFSI_QUEUE);
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			System.err.println(response.getMessage());
		}
	}
	
	public void saveMessage(MessageDAO messageDAO, UserDAO userDAO, OrderDAO orderDAO, MessageOnOrderBean moob, String messageType, 
			long messageSequenceNumber, String messageXML, String messageTxt, byte status){
		
			ActMessage message = new ActMessage();
			Shop sendingShop = moob.getSendingShop();
		    Shop receivingShop = moob.getReceivingShop();
		    StsMessage stsMessage = messageDAO.getStsById(status);
		    Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc(BOMConstants.ACT_MESSAGE);
			Orderactivity orderactivity = new Orderactivity(oactivitytype);
			User user = userDAO.getUserByUserName("TFSI");
			orderactivity.setActMessage(message);
			orderactivity.setUser(user);
			List<Bomorder> orders = orderDAO.getOrdersByOrderNumber(moob.getOrderNumber());
			Long bomorderId = orders.get(0).getBomorderId();
			Bomorder bomorder = orderDAO.getOrderByOrderId(bomorderId);
			orderactivity.setBomorder(bomorder);
			orderactivity.setCreatedDate(new Date());
			Messagetype messagetype = messageDAO.getMessageTypeByShortDesc(messageType);
			final Commmethod commmethod = messageDAO.getCommethodByCommId(moob.getCommMethod());
			byte messageFormat = BOMConstants.ORDER_TYPE_TEL;
			message.setOrderactivity(orderactivity);
			message.setMessagetype(messagetype);
			message.setCommmethod(commmethod);
			message.setMessageXml(messageXML);
			message.setShopByReceivingShopId(receivingShop);
			message.setShopBySendingShopId(sendingShop);
			message.setMessageFormat(messageFormat);
			message.setStsMessage(stsMessage);
			message.setMessageText(messageTxt);
			message.setBmtMessageSequenceNumber(String.valueOf(messageSequenceNumber));
			messageDAO.persistMessage(message);
	}
}
