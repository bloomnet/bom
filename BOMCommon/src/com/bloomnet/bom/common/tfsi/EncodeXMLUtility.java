package com.bloomnet.bom.common.tfsi;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;

public class EncodeXMLUtility {
	
	public String encode(String xml){
		
		
		final StringBuilder result = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(xml);
	    char character =  iterator.current();
	    while (character != CharacterIterator.DONE ){
	      if (character == '<') {
	        result.append("&lt;");
	      }
	      else if (character == '>') {
	        result.append("&gt;");
	      }
	     else if (character == '\"') {
	        result.append("&quot;");
	      }
	      else if (character == '\'') {
	        result.append("&#039;");
	      }
	      else if (character == '&') {
	         result.append("&amp;");
	      }else if(character == '|'){
	    	  result.append("%A6");
	      }else if(character == '^'){
	    	  result.append("%5D");
	      }
	      else {
	        //the char is not a special one
	        //add it to the result as is
	        result.append(character);
	      }
	      character = iterator.next();
	    }

		
		
	    return result.toString();
	}



}
