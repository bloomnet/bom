package com.bloomnet.bom.common.tfsi;

public class TFSIXML {
	private String header;
	private String body;
	
	public void setHeader(String header) {
		this.header = header;
	}
	public String getHeader() {
		return header;
	}
	public void setBody(String body) {
		this.body = body;
	}
	public String getBody() {
		return body;
	} 
}
