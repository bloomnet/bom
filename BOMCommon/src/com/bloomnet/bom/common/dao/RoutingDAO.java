package com.bloomnet.bom.common.dao;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.RoutingInterface;
import com.bloomnet.bom.common.entity.StsRouting;

public interface RoutingDAO {
	
	StsRouting getStatusById(byte id) throws HibernateException;
	
	StsRouting getStatusById(String id) throws HibernateException;
	
	String getVirtualQueue(String orderNumber) throws HibernateException;

	String persistRouting(ActRouting route) throws HibernateException;
	
	void persistRouting( RoutingInterface route ) throws HibernateException;
}
