package com.bloomnet.bom.common.dao;

import java.util.List;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.LogacallInterface;
import com.bloomnet.bom.common.entity.Shop;

public interface CallDispDAO {
	
	static final String NO_CONTACT_DEFAULT_SHOPNETWORK_STATUS_ID="3";
	
	List<Calldisp> getAllCallDisp() throws HibernateException;
	
	void persistCallDisp( Calldisp callDisposition ) throws HibernateException;
	
	void deleteCallDisp( String description ) throws HibernateException;
	
	Calldisp getCallDispByDesc( String description ) throws HibernateException;
	
	List<ActLogacall> getCallsByShop( Shop shop ) throws HibernateException;
	
	void persistCall(ActLogacall call) throws HibernateException;
	
	ActLogacall persistCall(LogacallInterface call) throws HibernateException;

	Calldisp getCallDispById(byte id) throws HibernateException;

	Calldisp getCallDispById(String id) throws HibernateException;
}
