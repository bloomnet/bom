package com.bloomnet.bom.common.test;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;

public class NonSpringListener implements MessageListener{

	public static void main(String[] args) {
		ConnectionFactory cf = new ActiveMQConnectionFactory(
				"tcp://localhost:61616");
		Connection conn = null;
		Session session = null;
		try {
			conn = cf.createConnection();
			conn.start();
			session = conn.createSession(false, Session.AUTO_ACKNOWLEDGE);
			Destination destination = new ActiveMQQueue("spitter.queue");
			MessageConsumer consumer = session.createConsumer(destination);
			Message message = consumer.receive();
			TextMessage textMessage = (TextMessage) message;
			System.out.println("GOTAMESSAGE:" + textMessage.getText());
			conn.start();
		} catch (JMSException e) {
			// handleexception?
		} finally {
			try {
				if (session != null) {
					session.close();
				}
				if (conn != null) {
					conn.close();
				}
			} catch (JMSException ex) {
			}
		}
	}

	@Override
	public void onMessage(Message message) {
		// TODO Auto-generated method stub
		try {
		      TextMessage textMessage = (TextMessage) message;

		     System.out.println("received: " + textMessage.getText());

		    //  _lastMessage = textMessage.getText();
		    } catch (Throwable e) {
		     // log.log(Level.WARNING, e.toString(), e);
		    }
	}
		  
}
		
	


