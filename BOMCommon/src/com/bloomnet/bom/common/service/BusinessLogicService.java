package com.bloomnet.bom.common.service;


public interface BusinessLogicService {

	String determineRoute(String orderNumber, String sendingShopCode) throws Exception;

	String getPreviousDestination(String orderNumber) throws Exception;
}
