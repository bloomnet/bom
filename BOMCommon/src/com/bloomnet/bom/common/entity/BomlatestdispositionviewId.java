package com.bloomnet.bom.common.entity;

// default package
// Generated Feb 16, 2012 4:13:21 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Embeddable;

/**
 * BomlatestdispositionviewId generated by hbm2java
 */
@Embeddable
public class BomlatestdispositionviewId implements java.io.Serializable {

	private Long parentBomid;
	private String parentOrderNumber;
	private Date parentOrderDate;
	private Date deliveryDate;
	private Date timeLogged;
	private long orderActivityId;
	private String disposition;
	private String text;

	public BomlatestdispositionviewId() {
	}

	public BomlatestdispositionviewId(Date timeLogged, long orderActivityId,
			String disposition) {
		this.timeLogged = timeLogged;
		this.orderActivityId = orderActivityId;
		this.disposition = disposition;
	}

	public BomlatestdispositionviewId(Long parentBomid,
			String parentOrderNumber, Date parentOrderDate, Date deliveryDate,
			Date timeLogged, long orderActivityId, String disposition,
			String text) {
		this.parentBomid = parentBomid;
		this.parentOrderNumber = parentOrderNumber;
		this.parentOrderDate = parentOrderDate;
		this.deliveryDate = deliveryDate;
		this.timeLogged = timeLogged;
		this.orderActivityId = orderActivityId;
		this.disposition = disposition;
		this.text = text;
	}

	@Column(name = "ParentBOMid")
	public Long getParentBomid() {
		return this.parentBomid;
	}

	public void setParentBomid(Long parentBomid) {
		this.parentBomid = parentBomid;
	}

	@Column(name = "ParentOrderNumber", length = 30)
	public String getParentOrderNumber() {
		return this.parentOrderNumber;
	}

	public void setParentOrderNumber(String parentOrderNumber) {
		this.parentOrderNumber = parentOrderNumber;
	}

	@Column(name = "ParentOrderDate", length = 19)
	public Date getParentOrderDate() {
		return this.parentOrderDate;
	}

	public void setParentOrderDate(Date parentOrderDate) {
		this.parentOrderDate = parentOrderDate;
	}

	@Column(name = "DeliveryDate", length = 19)
	public Date getDeliveryDate() {
		return this.deliveryDate;
	}

	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}

	@Column(name = "TimeLogged", nullable = false, length = 19)
	public Date getTimeLogged() {
		return this.timeLogged;
	}

	public void setTimeLogged(Date timeLogged) {
		this.timeLogged = timeLogged;
	}

	@Column(name = "OrderActivity_ID", nullable = false)
	public long getOrderActivityId() {
		return this.orderActivityId;
	}

	public void setOrderActivityId(long orderActivityId) {
		this.orderActivityId = orderActivityId;
	}

	@Column(name = "Disposition", nullable = false, length = 30)
	public String getDisposition() {
		return this.disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	@Column(name = "Text", length = 65535)
	public String getText() {
		return this.text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public boolean equals(Object other) {
		if ((this == other))
			return true;
		if ((other == null))
			return false;
		if (!(other instanceof BomlatestdispositionviewId))
			return false;
		BomlatestdispositionviewId castOther = (BomlatestdispositionviewId) other;

		return ((this.getParentBomid() == castOther.getParentBomid()) || (this
				.getParentBomid() != null && castOther.getParentBomid() != null && this
				.getParentBomid().equals(castOther.getParentBomid())))
				&& ((this.getParentOrderNumber() == castOther
						.getParentOrderNumber()) || (this
						.getParentOrderNumber() != null
						&& castOther.getParentOrderNumber() != null && this
						.getParentOrderNumber().equals(
								castOther.getParentOrderNumber())))
				&& ((this.getParentOrderDate() == castOther
						.getParentOrderDate()) || (this.getParentOrderDate() != null
						&& castOther.getParentOrderDate() != null && this
						.getParentOrderDate().equals(
								castOther.getParentOrderDate())))
				&& ((this.getDeliveryDate() == castOther.getDeliveryDate()) || (this
						.getDeliveryDate() != null
						&& castOther.getDeliveryDate() != null && this
						.getDeliveryDate().equals(castOther.getDeliveryDate())))
				&& ((this.getTimeLogged() == castOther.getTimeLogged()) || (this
						.getTimeLogged() != null
						&& castOther.getTimeLogged() != null && this
						.getTimeLogged().equals(castOther.getTimeLogged())))
				&& (this.getOrderActivityId() == castOther.getOrderActivityId())
				&& ((this.getDisposition() == castOther.getDisposition()) || (this
						.getDisposition() != null
						&& castOther.getDisposition() != null && this
						.getDisposition().equals(castOther.getDisposition())))
				&& ((this.getText() == castOther.getText()) || (this.getText() != null
						&& castOther.getText() != null && this.getText()
						.equals(castOther.getText())));
	}

	public int hashCode() {
		int result = 17;

		result = 37
				* result
				+ (getParentBomid() == null ? 0 : this.getParentBomid()
						.hashCode());
		result = 37
				* result
				+ (getParentOrderNumber() == null ? 0 : this
						.getParentOrderNumber().hashCode());
		result = 37
				* result
				+ (getParentOrderDate() == null ? 0 : this.getParentOrderDate()
						.hashCode());
		result = 37
				* result
				+ (getDeliveryDate() == null ? 0 : this.getDeliveryDate()
						.hashCode());
		result = 37
				* result
				+ (getTimeLogged() == null ? 0 : this.getTimeLogged()
						.hashCode());
		result = 37 * result + (int) this.getOrderActivityId();
		result = 37
				* result
				+ (getDisposition() == null ? 0 : this.getDisposition()
						.hashCode());
		result = 37 * result
				+ (getText() == null ? 0 : this.getText().hashCode());
		return result;
	}

}
