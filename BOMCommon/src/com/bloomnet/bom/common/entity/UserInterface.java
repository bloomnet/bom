package com.bloomnet.bom.common.entity;

import java.util.Set;

public interface UserInterface {
	
	Long getUserId();
	
	User getUserByCreatedUserId();
	
	String getUserName();
	void setUserName(String firstName);

	String getFirstName();
	void setFirstName(String firstName);

	String getLastName();
	void setLastName(String lastName);

	String getEmail();
	void setEmail(String email);

	String getPassword();
	void setPassword(String password);

	String getCreatedDate();
	void setCreatedDate(String createdDate);

	String getModifiedDate();
	void setModifiedDate(String modifiedDate);

	Set<Userrole> getUserrolesForUserId();
	void setUserrolesForUserId(Set<Userrole> userrolesForUserId);
	
	Set<Useractivity> getUseractivities();	
	void setUseractivities(Set<Useractivity> useractivities);

	String getActive();
	void setActive(String active);
}