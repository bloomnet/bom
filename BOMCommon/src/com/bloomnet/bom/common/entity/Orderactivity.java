package com.bloomnet.bom.common.entity;

// default package
// Generated Oct 3, 2011 5:44:13 PM by Hibernate Tools 3.4.0.CR1

import java.io.Serializable;
import java.util.Date;

/**
 * Orderactivity generated by hbm2java
 */
public class Orderactivity implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long orderActivityId;
	private Oactivitytype oactivitytype;
	private User user;
	private Bomorder bomorder;
	private Date createdDate;
	private ActLogacall actLogacall;
	private ActRouting actRouting;
	private ActPayment actPayment;
	private ActMessage actMessage;
	private ActAudit actAudit;

	public Orderactivity() {} 

	public Orderactivity( Oactivitytype oactivitytype ) {
		this.oactivitytype = oactivitytype;
		this.createdDate = new Date();
	}
	
	public Orderactivity( Oactivitytype oactivitytype, 
			              User user,
						  Bomorder bomorder, 
						  Date createdDate ) {
		
		this.oactivitytype = oactivitytype;
		this.user = user;
		this.bomorder = bomorder;
	}

	public Long getOrderActivityId() {
		return this.orderActivityId;
	}

	protected void setOrderActivityId(Long orderActivityId) {
		this.orderActivityId = orderActivityId;
	}

	public Oactivitytype getOactivitytype() {
		return this.oactivitytype;
	}

	public void setOactivitytype(Oactivitytype oactivitytype) {
		this.oactivitytype = oactivitytype;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Bomorder getBomorder() {
		return this.bomorder;
	}

	public void setBomorder(Bomorder bomorder) {
		this.bomorder = bomorder;
	}

	public Date getCreatedDate() {
		return this.createdDate;
	}

	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}

	public ActLogacall getActLogacall() {
		return this.actLogacall;
	}

	public void setActLogacall(ActLogacall actLogacall) {
		this.actLogacall = actLogacall;
	}

	public ActRouting getActRouting() {
		return this.actRouting;
	}

	public void setActRouting(ActRouting actRouting) {
		this.actRouting = actRouting;
	}

	public ActPayment getActPayment() {
		return this.actPayment;
	}

	public void setActPayment(ActPayment actPayment) {
		this.actPayment = actPayment;
	}

	public ActMessage getActMessage() {
		return this.actMessage;
	}

	public void setActMessage(ActMessage actMessage) {
		this.actMessage = actMessage;
	}

	public ActAudit getActAudit() {
		return this.actAudit;
	}

	public void setActAudit(ActAudit actAudit) {
		this.actAudit = actAudit;
	}
}
