package com.bloomnet.bom.common.entity;

// default package
// Generated Oct 3, 2011 5:44:13 PM by Hibernate Tools 3.4.0.CR1

import java.util.Date;


public class WebserviceMonitor implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long webserviceMonitorId;
	private Network network;
	private Date lastquery;
	private Date starttime;
	private Date finishtime;
	private Long numOfMessages;
	
	

	public WebserviceMonitor() {
	}
	
	public WebserviceMonitor(Network network, Date lastquery, Date starttime,
			Date finishtime, Long numOfMessages) {
		super();
		this.network = network;
		this.lastquery = lastquery;
		this.starttime = starttime;
		this.finishtime = finishtime;
		this.numOfMessages = numOfMessages;
	}
	
	public WebserviceMonitor(Network network, Date starttime,
			Long numOfMessages) {
		super();
		this.network = network;
		this.starttime = starttime;
		this.numOfMessages = numOfMessages;
	}
	
	



	public Long getWebserviceMonitorId() {
		return webserviceMonitorId;
	}



	public void setWebserviceMonitorId(Long webserviceMonitorId) {
		this.webserviceMonitorId = webserviceMonitorId;
	}



	public Network getNetwork() {
		return network;
	}



	public void setNetwork(Network network) {
		this.network = network;
	}



	public Date getLastquery() {
		return lastquery;
	}



	public void setLastquery(Date lastquery) {
		this.lastquery = lastquery;
	}



	public Date getStarttime() {
		return starttime;
	}



	public void setStarttime(Date starttime) {
		this.starttime = starttime;
	}



	public Date getFinishtime() {
		return finishtime;
	}



	public void setFinishtime(Date finishtime) {
		this.finishtime = finishtime;
	}



	public Long getNumOfMessages() {
		return numOfMessages;
	}



	public void setNumOfMessages(Long numOfMessages) {
		this.numOfMessages = numOfMessages;
	}

	
}
