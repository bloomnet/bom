package com.bloomnet.bom.common.entity;

import java.util.Date;

public interface AuditInterface extends OrderActivityCarrier {

	long getCityId();

	Date getDeliveryDate();

	String getOrderXml();

	double getPrice();

	long getZipId();
}