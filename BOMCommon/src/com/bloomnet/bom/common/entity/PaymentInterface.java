package com.bloomnet.bom.common.entity;

public interface PaymentInterface extends OrderActivityCarrier {

	StsPayment getStsPayment();
	void setStsPayment(StsPayment stsPayment);

	Paymenttype getPaymenttype();
	void setPaymenttype(Paymenttype paymenttype);

	double getPaymentAmount();

	String getPayTo();
	void setPayTo(String payTo);
}
