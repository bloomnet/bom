package com.bloomnet.bom.common.entity;

import java.io.Serializable;

public class SpecialQueues implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private long queueId;
	private String shopCode;
	private int tloFirst;
	private int tfsiFirst;
	private int automatedOnly;
	
	public SpecialQueues(){}
	
	public SpecialQueues(String shopCode, int tloFirst, int tfsiFirst, int automatedOnly) {
		this.setShopCode(shopCode);
		this.setTloFirst(tloFirst);
		this.setTfsiFirst(tfsiFirst);
		this.setAutomatedOnly(automatedOnly);
	}

	public long getQueueId() {
		return queueId;
	}

	public void setQueueId(long queueId) {
		this.queueId = queueId;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public int getTloFirst() {
		return tloFirst;
	}

	public void setTloFirst(int tloFirst) {
		this.tloFirst = tloFirst;
	}

	public int getTfsiFirst() {
		return tfsiFirst;
	}

	public void setTfsiFirst(int tfsiFirst) {
		this.tfsiFirst = tfsiFirst;
	}

	public int getAutomatedOnly() {
		return automatedOnly;
	}

	public void setAutomatedOnly(int automatedOnly) {
		this.automatedOnly = automatedOnly;
	}

}
