// default package
// Generated Oct 7, 2011 10:17:14 AM by Hibernate Tools 3.4.0.CR1
package com.bloomnet.bom.common.entity;

import java.io.Serializable;
import java.util.Date;


public class Internationalshops implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private Long internationalId;
	private String countryCode;
	private String shopCode;
	

	public Internationalshops() {
	}


	public Internationalshops(Long internationalId, String countryCode,
			String shopCode) {
		super();
		this.internationalId = internationalId;
		this.countryCode = countryCode;
		this.shopCode = shopCode;
	}

	

	public Long getInternationalId() {
		return internationalId;
	}


	public void setInternationalId(Long internationalId) {
		this.internationalId = internationalId;
	}


	public String getCountryCode() {
		return countryCode;
	}


	public void setCountryCode(String countryCode) {
		this.countryCode = countryCode;
	}


	public String getShopCode() {
		return this.shopCode;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}


}
