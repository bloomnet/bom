#!/bin/bash

CATALINA_LOG_DIR="/var/lib/tomcat6/logs/"
ACTIVEMQ_DIR="/home/activemq/apache-activemq-5.4.3"

echo "restarting activemq and tomcat"
cd $ACTIVEMQ_DIR
bin/activemq stop
bin/activemq status
bin/activemq start
bin/activemq status
service tomcat6 stop
service tomcat6 start
echo "restart activemq and tomcat completed"

cd $CATALINA_LOG_DIR
tail -f catalina.out


