/**
 * 
 */
package com.bloomnet.bom.webservice.service;

import static org.junit.Assert.fail;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.AfterTransaction;
import org.springframework.test.context.transaction.BeforeTransaction;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.webservice.exceptions.BloomLinkException;


/**
 * @author Danil Svirchtchev
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testWebServices.xml"})  
public class FSIServiceTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	@Autowired FSIClient fsiClient;
	@Autowired FSIService fsiService;
	@Autowired OrderDAO orderDAO;

	
	@BeforeTransaction
    public void verifyInitialDatabaseState() {
        // logic to verify the initial state before a transaction is started
		/*List<Bomorder> orders = orderDAO.getAllOrders();
		
		if (!orders.isEmpty()){
		for(Bomorder order:orders){
			System.out.println("order number: " + order.getOrderNumber());
			}
		}
		else{
			System.out.println("no orders");
		}*/
    }
	
	
	 @AfterTransaction
	    public void verifyFinalDatabaseState() {
	        // logic to verify the final state after transaction has rolled back
		/* List<Bomorder> orders = orderDAO.getAllOrders();
			
			if (!orders.isEmpty()){
			for(Bomorder order:orders){
				System.out.println("order number: " + order.getOrderNumber());
				}
			}
			else{
				System.out.println("no orders");
			}*/
	    }





	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#checkForMessages()}.
	 */
	@Test
	 // overrides the class-level defaultRollback setting
	@Rollback(true)
	public void testCheckForMessagesRollback() {
		
		try {
			
			fsiClient.checkForMessages();
		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print("\n\n\n-----> FINISHED...\n\n\n");
	}
	
	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#checkForMessages()}.
	 */
	@Test
	public void testCheckForMessages() {
		
		try {
			
			fsiClient.checkForMessages();
		
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.print("\n\n\n-----> FINISHED...\n\n\n");
	}


	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#sendMessageToBloomlink(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSendMessageToBloomlink() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#processMessage(java.lang.String)}.
	 */
	@Test
	public void testProcessMessage() {
		fail("Not yet implemented"); // TODO
	}
	
	@Test
	public void testProcessMessageRjct() {
		//TODO ***CHANGE ORDERNUMBER SENDING AND RECIEVING SHOP TO A VALID RECORD IN YOUR LOCAL DB FOR TESTING *** 
		//String msg =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>5</total><numOfOrderMessages>5</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3417555</bmtOrderNumber><bmtSeqNumberOfOrder>36262989</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267521</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3120</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383382</bmtOrderNumber><bmtSeqNumberOfOrder>36262988</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267519</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3118</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383381</bmtOrderNumber><bmtSeqNumberOfOrder>36262987</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267518</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3117</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383379</bmtOrderNumber><bmtSeqNumberOfOrder>36262719</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267251</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3116</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383380</bmtOrderNumber><bmtSeqNumberOfOrder>36262986</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267520</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3119</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383386</bmtOrderNumber><bmtSeqNumberOfOrder>36263258</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267789</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3122</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383385</bmtOrderNumber><bmtSeqNumberOfOrder>36263257</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267788</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3121</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder></foreignSystemInterface>";
		String msg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>5</total><numOfOrderMessages>5</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3417555</bmtOrderNumber><bmtSeqNumberOfOrder>36262989</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267521</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3120</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder></foreignSystemInterface>";
		try {
			fsiService.processMessage(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#sendMessageAckToBloomlink(java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSendMessageAckToBloomlink() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#sendMessageToQueue(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSendMessageToQueue() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#sendOrderToQueue(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSendOrderToQueue() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for {@link com.bloomnet.bom.webservice.service.impl.FSIServiceImpl#sendMessageToDestination(java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)}.
	 */
	@Test
	public void testSendMessageToDestination() {
		fail("Not yet implemented"); // TODO
	}
	
	@Test
	public void testProcessMessageDlou() {
		//TODO ***CHANGE ORDERNUMBER SENDING AND RECIEVING SHOP TO A VALID RECORD IN YOUR LOCAL DB FOR TESTING ***

 		String msg="<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>0</total><numOfOrderMessages>0</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageDlou><messageType>26</messageType><sendingShopCode>C2510000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>C2510000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419453</bmtOrderNumber><bmtSeqNumberOfOrder>38289266</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>38297235</bmtSeqNumberOfMessage><externalShopOrderNumber>10001</externalShopOrderNumber><inwireSequenceNo>9871</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20111108232517</messageCreateTimestamp><loadedDate>00101207220101</loadedDate></messageDlou></messagesOnOrder></foreignSystemInterface>";
		try {
			fsiService.processMessage(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test 
	public void testProcessMessageInqr(){
		String msg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>0</total><numOfOrderMessages>0</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageInqr><messageType>1</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>25493085</bmtOrderNumber><bmtSeqNumberOfOrder>91489413</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>38309247</bmtSeqNumberOfMessage><inwireSequenceNo>9900</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20111109104722</messageCreateTimestamp><messageText>jpursoo wrote: Please contact customer to verify recipient contact information or obtain alternate delivery address.</messageText></messageInqr></messagesOnOrder></foreignSystemInterface>";
		try {
			fsiService.processMessage(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testProcessMessageInfo(){
		String msg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>0</total><numOfOrderMessages>0</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageInfo><messageType>12</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419468</bmtOrderNumber><bmtSeqNumberOfOrder>38309246</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>38309515</bmtSeqNumberOfMessage><inwireSequenceNo>9901</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20111109110243</messageCreateTimestamp><messageText>jpursoo wrote: Delivery will be made as soon as permitted.</messageText></messageInfo></messagesOnOrder></foreignSystemInterface>";
		try {
			fsiService.processMessage(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testProcessMessagePchg(){
		String msg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>0</total><numOfOrderMessages>0</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messagePchg><messageType>18</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419468</bmtOrderNumber><bmtSeqNumberOfOrder>38309246</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>38309782</bmtSeqNumberOfMessage><externalShopOrderNumber>1001</externalShopOrderNumber><inwireSequenceNo>9902</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20111109111548</messageCreateTimestamp><messageText>jpursoo wrote: new price The total price you will be paid for this order has been changed. Previous Price: $65.00, New Price: $70.00. Please send a response acknowledging you have received and read this message.</messageText><price>70.00</price></messagePchg></messagesOnOrder></foreignSystemInterface>";
		try {
			fsiService.processMessage(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testProcessMessageCanc(){
		String msg = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>0</total><numOfOrderMessages>0</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageCanc><messageType>3</messageType><sendingShopCode>D7340000</sendingShopCode><receivingShopCode>J8860000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419468</bmtOrderNumber><bmtSeqNumberOfOrder>38309246</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>38310050</bmtSeqNumberOfMessage><inwireSequenceNo>9903</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20111109112747</messageCreateTimestamp><messageText>jpursoo wrote: I want to cancel this order</messageText></messageCanc></messagesOnOrder></foreignSystemInterface>";
		try {
			fsiService.processMessage(msg);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test 
	public void testProcessMessageInqrToSendingShop(){
		String msg  ="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?><foreignSystemInterface><security><username>J886</username><password></password><shopCode>J8860000</shopCode></security><messagesOnOrder><messageCount>1</messageCount><messageInqr><messageType>1</messageType><sendingShopCode>J8860000</sendingShopCode><receivingShopCode>D7340000</receivingShopCode><fulfillingShopCode>J8860000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3419468</bmtOrderNumber><bmtSeqNumberOfOrder>38309246</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>38309247</bmtSeqNumberOfMessage><externalShopMessageNumber>1001</externalShopMessageNumber><inwireSequenceNo>9900</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20111109105349</messageCreateTimestamp><messageText>jpursoo wrote: Please contact customer to verify recipient contact information or obtain alternate delivery address.</messageText></messageInqr></messagesOnOrder></foreignSystemInterface>";
		String dataEncoded = "";
		try {
			dataEncoded = URLEncoder.encode(msg, "UTF-8");
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("xml encoded: " + dataEncoded);

		//IN FLOWERS NETWORK ENDPOINT
		String post = BOMConstants.QA_ENDPOINT + BOMConstants.POST_MESSAGE	+ dataEncoded;
		//OUT FLOWERS NETWORK ENDPOINT
		//String post = BOMConstants.QA2_ENDPOINT + BOMConstants.POST_MESSAGE+ dataEncoded;

		System.out.println("post request " + post);
		
	try {
		String messages = fsiClient.sendRequest(post,"Order");
	} catch (BloomLinkException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	System.out.println("Order sent to bloomlink");
		
	}
	
	public void testDetermineRoute(){
	
		/*String currentDateStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date delDate = DateUtil.toDateFormat(deliveryDate);
		Date currentDate = DateUtil.toDate(currentDateStr);
		int eq = delDate.compareTo(currentDate);*/
		
	}

}
