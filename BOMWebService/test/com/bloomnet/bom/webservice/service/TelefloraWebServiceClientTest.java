  package com.bloomnet.bom.webservice.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.common.tfsi.TFSIMessage;
import com.bloomnet.bom.common.tfsi.TFSIResponse;
import com.bloomnet.bom.webservice.client.impl.TelefloraWebServiceClientImpl;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testWebServices.xml"})  
public class TelefloraWebServiceClientTest extends AbstractTransactionalJUnit4SpringContextTests {
	
	@Autowired TelefloraWebServiceClientImpl twsc;
	@Autowired private ShopDAO shopDAO;
	@Autowired OrderDAO orderDAO;
	String date;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetTransactions() {
		while(true){
			try{
				Thread.sleep(20000);
			}catch(Exception ee){}
			TFSIResponse response = twsc.getTransactions();
			ArrayList<TFSIMessage> messages = (ArrayList<TFSIMessage>) response.getTfsiMessages();
			for(int ii=0; ii<messages.size(); ++ii){
				System.out.println(messages.get(ii).getMessageText());
				twsc.ackTransaction(messages.get(ii).getBundleId());
				for(int xx=0;xx<20;++xx) System.out.println(messages.get(ii).getXmlMessage());
			}
		}
	}

	@Test
	public void testAckTransaction() {
		TFSIResponse response = twsc.ackTransaction("");
		System.out.println(response.isSuccess());
	}

	private String getDate(){
		String DATE_FORMAT = "yyyyMMdd";
	    SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
	    Date datee = new Date();
	    date = sdf.format(datee.getTime());
	    return date;
	}
	
	public MessageOnOrderBean test(){
		MessageOnOrderBean moob = new MessageOnOrderBean();
		moob.setCaptureDate(new Date());
		moob.setCardMessage("good luck!");
		moob.setDeliveryDate("03/16/2012");
		moob.setDateOrderDelivered(getDate());
		Shop shop = shopDAO.getShopByCode("14614600");
		Shop us = shopDAO.getShopByCode("43300100");
		moob.setFulfillingShop(us);
		moob.setMessageText("Final Testing Stages");
		moob.setReceivingShop(us);
		moob.setRecipientAddress1("2 old country rd");
		moob.setRecipientCity("beverly hills");
		moob.setRecipientCountryCode("USA");
		moob.setRecipientFirstName("Mark");
		moob.setRecipientLastName("Slvr");
		moob.setRecipientPhoneNumber("5162377080");
		moob.setRecipientState("CA");
		moob.setRecipientZipCode("90210");
		moob.setSendingShop(us);
		moob.setOrderNumber("DYK045");
		moob.setMessageCreateTimestamp(new Date());
		moob.setPrice("99.00");
		moob.setMessageText("Final Testing Stages");
		moob.setBmtOrderNumber("25518153");
		OrderProductInfoDetails opid = new OrderProductInfoDetails();
		opid.setProductCode("1");
		moob.setCommMethod((byte) 1);
		opid.setProductDescription("prod1");
		opid.setUnits(1);
		List<OrderProductInfoDetails> products = new ArrayList<OrderProductInfoDetails>();
		products.add(opid);
		moob.setProducts(products);
		moob.setTotalCost("5.99");
		moob.setSpecialInstruction("please leave at door");
		moob.setShippingDate(new Date());
		moob.setOccasionId("1");
		moob.setStatus(1);
		moob.setCommMethod(Byte.valueOf("1"));
		//twsc.removeDNRTransaction();
		return moob;
	}
	
	@Test
	public void testSendOrderTransaction() {
		TFSIResponse response= twsc.sendOrderTransaction("fsdfds", test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	
	@Test
	public void testResend(){
		
		Bomorder bomOrder = orderDAO.getOrderByOrderNumber("DWV923");
		
		String tfsiXML = bomOrder.getOrderXml();
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" + tfsiXML.split("<TFETransactionHeader>")[1].split("</TFETransactionHeader>")[0] + "</TFETransactionHeader>";
		String body = tfsiXML.split("</TFETransactionHeader>")[1];
		
		long originalSequenceNumber = Long.valueOf(bomOrder.getBmtOrderSequenceNumber());
		twsc.resendOrderTransaction(header, body, originalSequenceNumber);
	}


	@Test
	public void testRefusalTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.refusalTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testPriceChangeTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.priceChangeTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testCancelTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.cancelTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testConfirmCancelTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.confirmCancelTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testDenyCancelTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.denyCancelTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testMessageTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.messageTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testInquiryTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.inquiryTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testInquiryResponseTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.inquiryResponseTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testDNRTransaction(MessageOnOrderBean moob) {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.DNRTransaction(moob);
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testRemoveDNRTransaction() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.removeDNRTransaction(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

	@Test
	public void testDeliveryConfirmation() {
		twsc.setMessageSequenceNumber();
		TFSIResponse response = twsc.deliveryConfirmation(test());
		System.out.println(response.isSuccess()); 
		System.out.println(response.getMessage());
		System.out.println("\n");
	}

}
