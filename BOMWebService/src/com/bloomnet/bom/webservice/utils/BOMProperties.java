package com.bloomnet.bom.webservice.utils;

import java.io.FileInputStream;
import java.util.Properties;

public class BOMProperties {
	
	
	/**
	 * Load a properties file from the classpath
	 * 
	 * @param propsName
	 * @return Properties
	 * @throws Exception
	 */
	public static Properties load() throws Exception {
		Properties props = new Properties();
		props.load(new FileInputStream("/var/bloomnet/cfg/db.properties"));
		props.load(new FileInputStream("/var/bloomnet/cfg/fsi.properties"));
		props.load(new FileInputStream("/var/bloomnet/cfg/bom.properties"));
		props.load(new FileInputStream("/var/bloomnet/cfg/db.properties"));

		return props;

	}
}
