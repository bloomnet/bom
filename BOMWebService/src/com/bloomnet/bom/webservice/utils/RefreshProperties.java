package com.bloomnet.bom.webservice.utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;

public class RefreshProperties {
	
	@Autowired private Properties bomProperties;
	@Autowired private Properties fsiProperties;
	@Autowired private Properties tfsiProperties;
	
	
	public void refreshProperties(){
		try {
			bomProperties.load(new FileInputStream("/var/bloomnet/cfg/bom.properties"));
			fsiProperties.load(new FileInputStream("/var/bloomnet/cfg/fsi.properties"));
			tfsiProperties.load(new FileInputStream("/var/bloomnet/cfg/tfsi.properties"));
		} catch (FileNotFoundException e) {
			try {
				bomProperties.load(new FileInputStream("c:/var/bloomnet/cfg/bom.properties"));
				fsiProperties.load(new FileInputStream("c:/var/bloomnet/cfg/fsi.properties"));
				tfsiProperties.load(new FileInputStream("c:/var/bloomnet/cfg/tfsi.properties"));
			} catch (FileNotFoundException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {
				e1.printStackTrace();
			}
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
