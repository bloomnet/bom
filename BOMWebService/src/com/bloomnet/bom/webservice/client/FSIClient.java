package com.bloomnet.bom.webservice.client;

import com.bloomnet.bom.webservice.exceptions.BloomLinkException;

public interface FSIClient {

	/***
	 * Polls bloomLink for new messages
	 * @throws Exception 
	 */
	public abstract void checkForMessages() throws Exception;

	/**
	 * Send RESTFUL http post message to BloomLink and handle response
	 * @param outgoingMessage
	 * @return
	 * @throws BloomLinkException 
	 */
	public abstract String sendRequest(String outgoingMessage,
			String messageType) throws BloomLinkException;

}