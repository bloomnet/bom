package com.bloomnet.bom.webservice.client.impl;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.UUID;

import javax.jms.JMSException;
import javax.xml.ws.handler.Handler;
import javax.xml.ws.handler.HandlerResolver;
import javax.xml.ws.handler.PortInfo;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.tfsi.AckTransactions;
import com.bloomnet.bom.common.tfsi.AckTransactionsResponse;
import com.bloomnet.bom.common.tfsi.ArrayOfTransaction;
import com.bloomnet.bom.common.tfsi.EncodeXMLUtility;
import com.bloomnet.bom.common.tfsi.Envelope;
import com.bloomnet.bom.common.tfsi.ReceiveTransactions;
import com.bloomnet.bom.common.tfsi.ReceiveTransactionsResponse;
import com.bloomnet.bom.common.tfsi.SendTransactions;
import com.bloomnet.bom.common.tfsi.SendTransactionsResponse;
import com.bloomnet.bom.common.tfsi.TFSIMessage;
import com.bloomnet.bom.common.tfsi.TFSIResponse;
import com.bloomnet.bom.common.tfsi.TFSIUtil;
import com.bloomnet.bom.common.tfsi.TFSIXML;
import com.bloomnet.bom.common.tfsi.TelefloraWebService;
import com.bloomnet.bom.common.tfsi.TelefloraWebServiceSoap;
import com.bloomnet.bom.common.tfsi.Transaction;
import com.bloomnet.bom.common.tfsi.UUIDHandler;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.webservice.client.TelefloraWebServiceClient;
import com.bloomnet.bom.webservice.service.TFSIAgentService;

@Service("twsc")
@Transactional(propagation = Propagation.SUPPORTS)
public class TelefloraWebServiceClientImpl implements TelefloraWebServiceClient {
	
	@Autowired private ShopDAO shopDAO;
	@Autowired private OrderDAO orderDAO;
	@Autowired private UserDAO userDAO;
	@Autowired private MessageDAO messageDAO;
	
	@Autowired private Properties tfsiProperties;
	@Autowired private Properties bomProperties;
	
	@Autowired private TFSIAgentService tfsiAgent;
	
	@Autowired private TransformService transformService;	

	
	static Logger logger = Logger.getLogger( TelefloraWebServiceClientImpl.class );
	
	EncodeXMLUtility exu = new EncodeXMLUtility();
	
	Properties systemProps;
	
	TelefloraWebService service;
	TelefloraWebServiceSoap eif;
	TFSIUtil tfsiUtil = new TFSIUtil();
	
	long messageSequenceNumber;
	
	public TelefloraWebServiceClientImpl(){}
	
	@Override
	public void setMessageSequenceNumber(){
		this.messageSequenceNumber = Integer.valueOf(messageDAO.getLastMessageSequenceNumber());
	}
	
	private TFSIResponse sendSOAPMessage(String header, String body, String transactionType, long sequenceNumber, String messageType, MessageOnOrderBean moob){
		TFSIResponse response = createSendTransaction(header,body,transactionType);
		tfsiUtil.saveMessage(messageDAO, userDAO, orderDAO, moob, messageType, sequenceNumber, header+body, moob.getMessageText(), Byte.valueOf("3"));
		return response;
	}
	
	private TFSIResponse sendSOAPOrder(String tempOrderNumber, String header, String body, String transactionType, long sequenceNumber, MessageOnOrderBean moob){

		TFSIResponse response = createSendTransaction(header,body,transactionType);
		//String tempOrderNumber = "TFN"+new Date().getTime();
		logger.debug("Order number " + tempOrderNumber + " has been sent to teleflora with the sequence number: " + sequenceNumber);
		if(response.isSuccess()){
			tfsiUtil.saveOrder(orderDAO, moob, tempOrderNumber, header, body, sequenceNumber, response, bomProperties);
		}else{
			Bomorder bomOrder = orderDAO.getOrderByOrderNumber(moob.getBmtOrderNumber());
			ForeignSystemInterface fsi = transformService.convertToJavaFSI(bomOrder.getOrderXml());
			MessageOrder order = fsi.getMessagesOnOrder().get(0).getMessageOrder();
			try {
				if (moob.getRecipientCountryCode().equals("US") || moob.getRecipientCountryCode().equals("USA")){
					tfsiAgent.rejectOrder(moob, order, order.getSendingShopCode(), bomOrder.getOrderXml());
	        	}else {
	        		tfsiAgent.sendToTLOQueue(bomOrder.getOrderXml(), order);
	        	}
			} catch (Exception e) {}
		}
		return response;
	}
	
	private TFSIResponse resendSOAPOrder(String header, String body, String transactionType, long sequenceNumber){
		TFSIResponse response = createSendTransaction(header,body,transactionType);
		return response;
	}
	
	private void setResources(final String transactionType) throws MalformedURLException{

		if(tfsiProperties.getProperty("proxySet").equals("true")){
			systemProps = System.getProperties();
			systemProps.put("proxySet", "true");
			systemProps.put("proxyHost", tfsiProperties.getProperty("proxyAddress"));
			systemProps.put("proxyPort", tfsiProperties.getProperty("proxyPort"));
		}
		
		service = new TelefloraWebService(
				new URL(tfsiProperties.getProperty("endpoint")),
				tfsiProperties.getProperty("serviceName"),
				tfsiProperties.getProperty("serviceType"));
		
		service.setHandlerResolver(new HandlerResolver(){
			@SuppressWarnings("rawtypes")
			public List<Handler> getHandlerChain(PortInfo arg0) {
				List<Handler> hchain = new ArrayList<Handler>();
				hchain.add(new UUIDHandler(transactionType, 
											tfsiProperties.getProperty("BloomNetShopCode"), 
											tfsiProperties.getProperty("password")));
				return hchain;
			}
		});
		eif = service.getTelefloraWebServiceSoap();

	}

	private TFSIResponse createSendTransaction(String header, String body, String transactionType){
		try {
			setResources("SendTransactions");
		} catch (MalformedURLException e) {
		}
		
		UUID uuid = UUID.randomUUID();
		
		Transaction transaction = new Transaction();
		transaction.setSourceId("CLIENT");
		transaction.setDestinationId("TFE");
		transaction.setTransactionType(transactionType);
		transaction.setTransactionGuid(uuid.toString());
		transaction.setLocalTime(String.valueOf(new Date().getTime()));
		transaction.setSortIndex(1);
		transaction.setHeader(header);
		transaction.setBody(body);
		
		ArrayOfTransaction transactions = new ArrayOfTransaction();
		transactions.getTransaction().add(transaction);	
		
		Envelope envelope = new Envelope();
		envelope.setCommsSoftwareVersion(tfsiProperties.getProperty("commsSoftwareVersion"));
		envelope.setCommsVendor(tfsiProperties.getProperty("commsVendor"));
		envelope.setDatabaseVersion(tfsiProperties.getProperty("databaseVersion"));
		UUID guid = UUID.randomUUID();
		envelope.setGUID(String.valueOf(guid));
		envelope.setPassword(tfsiProperties.getProperty("password"));
		envelope.setSoftwareVendor(tfsiProperties.getProperty("softwareVendor"));
		envelope.setSoftwareVersion(tfsiProperties.getProperty("softwareVersion"));
		envelope.setTransactionCount(1);
		envelope.setTransactions(transactions);
		envelope.setTWSXMLVersion(tfsiProperties.getProperty("twsXMLVersion"));
		envelope.setUserID(tfsiProperties.getProperty("BloomNetShopCode"));
		
		SendTransactionsResponse transactionresponse = new SendTransactionsResponse(); 
		SendTransactions sendtransactions = new SendTransactions();
		sendtransactions.setIncomingEnvelope(envelope);
		try{
			transactionresponse.setSendTransactionsResult(eif.sendTransactions(sendtransactions.getIncomingEnvelope()));
		}catch(Exception ee) {
			ee.printStackTrace();
			}
		
		Envelope responseEnvelope = transactionresponse.getSendTransactionsResult();
		
		TFSIResponse response = new TFSIResponse();
		response.setSuccess(responseEnvelope.getTransactions().getTransaction().get(0).getResult().isStatus());
		response.setMessage(responseEnvelope.getTransactions().getTransaction().get(0).getResult().getMessage());
		return response;
	}
	
	private TFSIResponse createAckTransaction(String transactionType, String bundleId){
		try {
			setResources("AckTransactions");
		} catch (MalformedURLException e) {
		}
		
		UUID uuid = UUID.randomUUID();
		
		Transaction transaction = new Transaction();
		transaction.setSourceId("CLIENT");
		transaction.setDestinationId("TFE");
		transaction.setTransactionType(transactionType);
		transaction.setBundleId(bundleId);
		transaction.setTransactionGuid(uuid.toString());
		transaction.setLocalTime(String.valueOf(new Date().getTime()));
		transaction.setSortIndex(1);
		ArrayOfTransaction transactions = new ArrayOfTransaction();
		transactions.getTransaction().add(transaction);	
		
		Envelope envelope = new Envelope();
		envelope.setCommsSoftwareVersion(tfsiProperties.getProperty("commsSoftwareVersion"));
		envelope.setCommsVendor(tfsiProperties.getProperty("commsVendor"));
		envelope.setDatabaseVersion(tfsiProperties.getProperty("databaseVersion"));
		UUID guid = UUID.randomUUID();
		envelope.setGUID(String.valueOf(guid));
		envelope.setPassword(tfsiProperties.getProperty("password"));
		envelope.setSoftwareVendor(tfsiProperties.getProperty("softwareVendor"));
		envelope.setSoftwareVersion(tfsiProperties.getProperty("softwareVersion"));
		envelope.setTransactionCount(1);
		envelope.setTransactions(transactions);
		envelope.setTWSXMLVersion(tfsiProperties.getProperty("twsXMLVersion"));
		envelope.setUserID(tfsiProperties.getProperty("BloomNetShopCode"));
		
		AckTransactionsResponse transactionresponse = new AckTransactionsResponse(); 
		AckTransactions acktransactions = new AckTransactions();
		acktransactions.setIncomingEnvelope(envelope);
		transactionresponse.setAckTransactionsResult(eif.ackTransactions(acktransactions.getIncomingEnvelope()));
		
		Envelope responseEnvelope = transactionresponse.getAckTransactionsResult();
		
		TFSIResponse response = new TFSIResponse();
		response.setSuccess(responseEnvelope.getTransactions().getTransaction().get(0).getResult().isStatus());
		response.setMessage(responseEnvelope.getTransactions().getTransaction().get(0).getResult().getMessage());
		return response;
	}
	
	private TFSIResponse createReceiveTransaction(){
		

		try {
			setResources("ReceiveTransactions");
		} catch (MalformedURLException e) {
		}
		
		Envelope envelope = new Envelope();
		envelope.setCommsSoftwareVersion(tfsiProperties.getProperty("commsSoftwareVersion"));
		envelope.setCommsVendor(tfsiProperties.getProperty("commsVendor"));
		envelope.setDatabaseVersion(tfsiProperties.getProperty("databaseVersion"));
		UUID guid = UUID.randomUUID();
		envelope.setGUID(String.valueOf(guid));
		envelope.setPassword(tfsiProperties.getProperty("password"));
		envelope.setSoftwareVendor(tfsiProperties.getProperty("softwareVendor"));
		envelope.setSoftwareVersion(tfsiProperties.getProperty("softwareVersion"));
		envelope.setTWSXMLVersion(tfsiProperties.getProperty("twsXMLVersion"));
		envelope.setUserID(tfsiProperties.getProperty("BloomNetShopCode"));
		
		ReceiveTransactionsResponse transactionresponse = new ReceiveTransactionsResponse(); 
		ReceiveTransactions receivetransactions = new ReceiveTransactions();
		receivetransactions.setIncomingEnvelope(envelope);
		transactionresponse.setReceiveTransactionsResult(eif.receiveTransactions(receivetransactions.getIncomingEnvelope()));
		
		Envelope responseEnvelope = transactionresponse.getReceiveTransactionsResult();
		TFSIResponse response = new TFSIResponse();
		
		int numberOfTransaction = responseEnvelope.getTransactionCount();
		
		for(int ii=0; ii<numberOfTransaction; ++ii){
			TFSIMessage tfsiMessage = new TFSIMessage();
			tfsiMessage.setMessageType(responseEnvelope.getTransactions().getTransaction().get(ii).getTransactionType());
			tfsiMessage.setBundleId(responseEnvelope.getTransactions().getTransaction().get(ii).getBundleId());
			tfsiMessage.setXmlMessage(transformService.convertToTFSIXML(responseEnvelope.getTransactions().getTransaction().get(ii)));
			try{
				tfsiMessage.setSendingShopCode(responseEnvelope.getTransactions().getTransaction().get(ii).getHeader().split("<SenderVSC>")[1].substring(0,8));
			}catch(Exception ee){}
			try{
				tfsiMessage.setTIDNumber(responseEnvelope.getTransactions().getTransaction().get(ii).getHeader().split("<Tid>")[1].substring(0,6));
			}catch(Exception ee){}
			try{
				tfsiMessage.setValType(responseEnvelope.getTransactions().getTransaction().get(ii).getBody().split("<MessageText Line=\"1\">")[1].split(" ")[1].split(" ")[0]);
			}catch(Exception ee){}
			try{
				tfsiMessage.setOriginalTIDNumber(responseEnvelope.getTransactions().getTransaction().get(ii).getBody().split("OrigTID=")[1].replaceAll("\"","").substring(0,6));
			}catch(Exception ee){}
					
			response.setSuccess(responseEnvelope.getTransactions().getTransaction().get(ii).getResult().isStatus());
			response.setMessage(responseEnvelope.getTransactions().getTransaction().get(ii).getTransactionGuid());
			response.setTfsiMessages(tfsiMessage);
		}
		return response;
	}
	
	@Override
	public TFSIResponse getTransactions(){
		TFSIResponse response = createReceiveTransaction();
		return response;
	}
	
	@Override
	public TFSIResponse ackTransaction(String bundleId){
		TFSIResponse response = createAckTransaction("Acknowledgement",bundleId);
		return response;
	}
	
	private TFSIXML buildSendOrderTransaction(MessageOnOrderBean moob){
		
		String receivingShopCode = shopDAO.getTelefloraShopnetwork(moob.getReceivingShop().getShopId()).getShopCode();
		String sendingShopCode = shopDAO.getTelefloraShopnetwork(shopDAO.getShopByCode(tfsiProperties.getProperty("BloomNetShopCode")).getShopId()).getShopCode();
		String recipientCountry = moob.getRecipientCountryCode();
		Integer sequenceNumber = Integer.valueOf(BOMConstants.getTelSeqNum());
		moob.setBmtSeqNumberOfOrder(sequenceNumber);
		
		boolean isInternational = false;
		
		if(recipientCountry != null && !recipientCountry.equals("") && !recipientCountry.equalsIgnoreCase("USA")
				&& !recipientCountry.equalsIgnoreCase("US") && !recipientCountry.equalsIgnoreCase("CAN") && !recipientCountry.equalsIgnoreCase("CA"))
			isInternational = true;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<ReceiverVSC>"+receivingShopCode+"</ReceiverVSC>" +
		"<SenderPSC>"+sendingShopCode+"</SenderPSC>" +
		"<SenderVSC>"+sendingShopCode+"</SenderVSC>" +
		"<SenderSequenceNumber>"+sequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<Order xmlns:Dove=\"http://www.teleflora.com/Dove\" Confirmation=\"N\" AutoForward=\"Z\" Operator=\""+tfsiProperties.getProperty("operator")+"\" OccassionCode=\""+moob.getOccasionId()+"\">" +
					"<SendingFlorist>" +
						"<Name>"+moob.getSendingShop().getShopName()+"</Name>" +
						"<City>"+moob.getSendingShop().getCity().getName()+"</City>" +
						"<State>"+moob.getSendingShop().getCity().getState().getShortName()+"</State>" +
						"<Phones>" +
							"<Phone Preference=\"1\">"+moob.getSendingShop().getShopPhone()+"</Phone>" +
						"</Phones>" +
					"</SendingFlorist>" +
					"<Recipient>" +
						"<FirstName>"+exu.encode(moob.getRecipientFirstName())+"</FirstName>" +
						"<LastName>"+exu.encode(moob.getRecipientLastName())+"</LastName>" +
						"<AddressLines>" +
							"<AddressLine Line=\"1\">"+exu.encode(moob.getRecipientAddress1())+"</AddressLine>";
					if(moob.getRecipientAddress2() != null && !moob.getRecipientAddress2().equals("")){
							body += "<AddressLine Line=\"2\">"+exu.encode(moob.getRecipientAddress2())+"</AddressLine>";
							if(isInternational){
								body += "<AddressLine Line=\"3\"></AddressLine>";
								body += "<AddressLine Line=\"4\">"+exu.encode("Country Code: " + moob.getRecipientCountryCode())+"</AddressLine>";
								
							}
					}else{
						if(isInternational){
							body += "<AddressLine Line=\"2\"></AddressLine>";
							body += "<AddressLine Line=\"3\"></AddressLine>";
							body += "<AddressLine Line=\"4\">"+exu.encode("Country Code: " + moob.getRecipientCountryCode())+"</AddressLine>";
						}
					}
					body += "</AddressLines>" +
						"<City>"+exu.encode(moob.getRecipientCity())+"</City>";
					if(isInternational)
						body += "<State>..</State>";
					else
						body += "<State>"+exu.encode(moob.getRecipientState())+"</State>";
						
					body += "<PostalCode>"+moob.getRecipientZipCode()+"</PostalCode>" +
						"<Email/>" +
						"<Phones>" +
							"<Phone Preference=\"1\">"+moob.getRecipientPhoneNumber()+"</Phone>" +
						"</Phones>" +
					"</Recipient>";
		body += "<CardMessageLines>";
		List<String> lines = tfsiUtil.splitMessageIntoLines(exu.encode(moob.getCardMessage()).replaceAll("%0D", "").replaceAll("%0A", " "));
		for(int ii=0; ii<lines.size(); ++ii){
			body += "<CardMessage Line=\""+(ii+1)+"\">"+lines.get(ii)+"</CardMessage>";
		}
		body += "</CardMessageLines>";
		body += "<Products>";
		int totalLines = 0;
		int previousLines = 0;
		if(moob.getProducts().size() < 3){
			
			body += "<Product ProductCode=\"\" Index=\"1\">" +
					"<DescLine>";
			
			for(int ii=0; ii<moob.getProducts().size(); ++ii){
				
				String productDesc = moob.getProducts().get(ii).getUnits()+" orders of " + moob.getProducts().get(ii).getProductDescription();
				List<String> lineDescs = tfsiUtil.splitMessageIntoLines(productDesc);
				List<String> finalLineDescs = new ArrayList<String>();
				if(lineDescs.size() >= 3){
					totalLines = totalLines + 3;
					for(int xx=0; xx<3; ++xx){
						finalLineDescs.add(lineDescs.get(xx));
					}					
				}else{
					totalLines = totalLines + lineDescs.size();
					for(int xx=0; xx<lineDescs.size(); ++xx){
						finalLineDescs.add(lineDescs.get(xx));
					}		
				}
				int z = 0;
				for(int xx=previousLines; xx<totalLines; ++xx){				
					body += "<DescLine Sort=\""+(xx+1)+"\">"+exu.encode(finalLineDescs.get(z))+"</DescLine>";
					z++;
				}
				previousLines = totalLines;
			}
			body += "</DescLine>" +
					"</Product>";
			
		}else{
			body += "<Product ProductCode=\"\" Index=\"1\">" +
					"<DescLine>";
			for(int ii=0; ii<2; ++ii){
				
				String productDesc = moob.getProducts().get(ii).getUnits()+" orders of " + moob.getProducts().get(ii).getProductDescription();
				List<String> lineDescs = tfsiUtil.splitMessageIntoLines(productDesc);
				List<String> finalLineDescs = new ArrayList<String>();
				if(lineDescs.size() >= 3){
					totalLines = totalLines + 3;
					for(int xx=0; xx<3; ++xx){
						finalLineDescs.add(lineDescs.get(xx));
					}					
				}else{
					totalLines = totalLines + lineDescs.size();
					for(int xx=0; xx<lineDescs.size(); ++xx){
						finalLineDescs.add(lineDescs.get(xx));
					}		
				}
				int z = 0;
				for(int xx=previousLines; xx<totalLines; ++xx){				
					body += "<DescLine Sort=\""+(xx+1)+"\">"+exu.encode(finalLineDescs.get(z))+"</DescLine>";
					z++;
				}
				previousLines = totalLines;
			}
			body += "</DescLine>" +
					"</Product>" +
					"<Product ProductCode=\"\" Index=\"2\">" +
					"<DescLine>";
			
			previousLines = 0;
			totalLines = 0;
			
			for(int ii=2; ii<moob.getProducts().size(); ++ii){
				if(ii == 4) break;
				String productDesc = moob.getProducts().get(ii).getUnits()+" orders of " + moob.getProducts().get(ii).getProductDescription();
				List<String> lineDescs = tfsiUtil.splitMessageIntoLines(productDesc);
				List<String> finalLineDescs = new ArrayList<String>();
				if(lineDescs.size() >= 3){
					totalLines = totalLines + 3;
					for(int xx=0; xx<3; ++xx){
						finalLineDescs.add(lineDescs.get(xx));
					}					
				}else{
					totalLines = totalLines + lineDescs.size();
					for(int xx=0; xx<lineDescs.size(); ++xx){
						finalLineDescs.add(lineDescs.get(xx));
					}		
				}
				int z = 0;
				for(int xx=previousLines; xx<totalLines; ++xx){				
					body += "<DescLine Sort=\""+(xx+1)+"\">"+exu.encode(finalLineDescs.get(z))+"</DescLine>";
					z++;
				}
				previousLines = totalLines;
			}
			body += "</DescLine>" +
					"</Product>";
		}
		body += "</Products>";
		body += "<Instructions>";
		List<String> instructionLines = tfsiUtil.splitMessageIntoLines(exu.encode(moob.getSpecialInstruction()));
		for(int ii=0; ii<instructionLines.size(); ++ii){
			body += "<Instruction Line=\""+(ii+1)+"\">"+instructionLines.get(ii)+"</Instruction>";
		}
		body += "</Instructions>" +
					"<Price>"+moob.getTotalCost()+"</Price>" +
					"<DeliveryDate>"+tfsiUtil.getDate(moob.getDeliveryDate())+"</DeliveryDate>" +
					"<OrderDate>"+tfsiUtil.getDate(new Date())+"</OrderDate>" +
					"</Order>";
		
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.client.TelefloraWebServiceClient#sendOrderTransaction()
	 */
	@Override
	public TFSIResponse sendOrderTransaction(String tempOrderNumber, MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildSendOrderTransaction(moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPOrder(tempOrderNumber, header,body,"Order",moob.getBmtSeqNumberOfOrder(),moob);
		
		return response;
	}
	
	@Override
	public TFSIResponse resendOrderTransaction(String h, String b, long originalSequenceNumber){
		
		String header = h;
		String body = b;
		
		TFSIResponse response = resendSOAPOrder(header,body,"Order",originalSequenceNumber);
		
		return response;
	}
	
	
	private TFSIXML buildRefusalTransaction(MessageOnOrderBean moob){
		messageSequenceNumber++;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<SenderPSC>"+tfsiProperties.getProperty("PSC")+"</SenderPSC>" +
		"<SenderVSC>"+tfsiProperties.getProperty("VSC")+"</SenderVSC>" +
		"<SenderSequenceNumber>"+messageSequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<Refusal xmlns:Dove=\"http://www.teleflora.com/Dove\" OrigTID=\""+moob.getOrderNumber()+"\" Operator=\""+tfsiProperties.getProperty("operator")+"\">" +
					"<Recipient>" +
						"<FirstName>"+exu.encode(moob.getRecipientFirstName())+"</FirstName>" +
						"<LastName>"+exu.encode(moob.getRecipientLastName())+"</LastName>" +
						"<AddressLines>" +
						"<AddressLine Line=\"1\">"+exu.encode(moob.getRecipientAddress1())+"</AddressLine>";
					if(moob.getRecipientAddress2() != null && !moob.getRecipientAddress2().equals(""))
						body += "<AddressLine Line=\"2\">"+exu.encode(moob.getRecipientAddress2())+"</AddressLine>";
					body += "</AddressLines>"+exu.encode(moob.getRecipientCity())+"</City>" +
						"<State>"+exu.encode(moob.getRecipientState())+"</State>" +
						"<PostalCode>"+moob.getRecipientZipCode()+"</PostalCode>" +
						"<Email />" +
						"<Phones>" +
							"<Phone Preference=\"1\">"+moob.getRecipientPhoneNumber()+"</Phone>" +
						"</Phones>" +
					"</Recipient>";
		body += "<MessageLines>";
		List<String> lines = tfsiUtil.splitMessageIntoLines(exu.encode(moob.getMessageText()));
		for(int ii=0; ii<lines.size(); ++ii){
			body += "<MessageText Line=\""+(ii+1)+"\">"+lines.get(ii)+"</MessageText>";
		}
		body += "</MessageLines>" +
					"<DeliveryDate>"+tfsiUtil.getDate(moob.getDeliveryDate())+"</DeliveryDate>" +
					"<OrderDate>"+tfsiUtil.getDate(moob.getMessageCreateTimestamp())+"</OrderDate>" +
					"<SuggestedFlorist></SuggestedFlorist>" +
				"</Refusal>";
					
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	@Override
	public TFSIResponse refusalTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildRefusalTransaction(moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"RefuseOrder",messageSequenceNumber,"Rjct",moob);
		
		return response;
	}
	
	private TFSIXML buildPriceChangeTransaction(MessageOnOrderBean moob){
		messageSequenceNumber++;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<SenderPSC>"+tfsiProperties.getProperty("PSC")+"</SenderPSC>" +
		"<SenderVSC>"+tfsiProperties.getProperty("VSC")+"</SenderVSC>" +
		"<SenderSequenceNumber>"+messageSequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<PriceChange xmlns:Dove=\"http://www.teleflora.com/Dove\" OrigTID=\""+moob.getOrderNumber()+"\" Operator=\""+tfsiProperties.getProperty("operator")+"\">" +
					"<Recipient>" +
						"<FirstName>"+exu.encode(moob.getRecipientFirstName())+"</FirstName>" +
						"<LastName>"+exu.encode(moob.getRecipientLastName())+"</LastName>" +
						"<AddressLines>" +
						"<AddressLine Line=\"1\">"+exu.encode(moob.getRecipientAddress1())+"</AddressLine>";
					if(moob.getRecipientAddress2() != null && !moob.getRecipientAddress2().equals(""))
						body += "<AddressLine Line=\"2\">"+exu.encode(moob.getRecipientAddress2())+"</AddressLine>";
					body += "</AddressLines>" +
						"<City>"+exu.encode(moob.getRecipientCity())+"</City>" +
						"<State>"+exu.encode(moob.getRecipientState())+"</State>" +
						"<PostalCode>"+moob.getRecipientZipCode()+"</PostalCode>" +
						"<Email />" +
						"<Phones>" +
							"<Phone Preference=\"1\">"+moob.getRecipientPhoneNumber()+"</Phone>" +
						"</Phones>" +
					"</Recipient>";
		body += "<MessageLines>";
		List<String> lines = tfsiUtil.splitMessageIntoLines(exu.encode(moob.getMessageText()));
		for(int ii=0; ii<lines.size(); ++ii){
			body += "<MessageText Line=\""+(ii+1)+"\">"+lines.get(ii)+"</MessageText>";
		}
		body += "</MessageLines>" +
					"<DeliveryDate>"+tfsiUtil.getDate(moob.getDeliveryDate())+"</DeliveryDate>" +
					"<OrderDate>"+tfsiUtil.getDate(moob.getMessageCreateTimestamp())+"</OrderDate>" +
					"<Price>"+moob.getTotalCost()+"</Price>" +
					"<SuggestedFlorist></SuggestedFlorist>" +
					"</PriceChange>";
					
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	@Override
	public TFSIResponse priceChangeTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildPriceChangeTransaction(moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"PriceChange",messageSequenceNumber,"Pchg",moob);
		
		return response;
	}
	
	private TFSIXML buildCancelTransaction(String shortType, MessageOnOrderBean moob){
		messageSequenceNumber++;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<SenderPSC>"+tfsiProperties.getProperty("PSC")+"</SenderPSC>" +
		"<SenderVSC>"+tfsiProperties.getProperty("VSC")+"</SenderVSC>" +
		"<SenderSequenceNumber>"+messageSequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<Cancel xmlns:Dove=\"http://www.teleflora.com/Dove\" Type=\""+shortType+"\" OrigTID=\""+moob.getOrderNumber()+"\" Operator=\""+tfsiProperties.getProperty("operator")+"\">" +
					"<Recipient>" +
						"<FirstName>"+exu.encode(moob.getRecipientFirstName())+"</FirstName>" +
						"<LastName>"+exu.encode(moob.getRecipientLastName())+"</LastName>" +
						"<AddressLines>" +
							"<AddressLine Line=\"1\">"+exu.encode(moob.getRecipientAddress1())+"</AddressLine>";
					if(moob.getRecipientAddress2() != null && !moob.getRecipientAddress2().equals(""))
						body += "<AddressLine Line=\"2\">"+exu.encode(moob.getRecipientAddress2())+"</AddressLine>";
					body += "</AddressLines>" +
						"<City>"+exu.encode(moob.getRecipientCity())+"</City>" +
						"<State>"+exu.encode(moob.getRecipientState())+"</State>" +
						"<PostalCode>"+moob.getRecipientZipCode()+"</PostalCode>" +
						"<Email />" +
						"<Phones>" +
							"<Phone Preference=\"1\">"+moob.getRecipientPhoneNumber()+"</Phone>" +
						"</Phones>" +
					"</Recipient>";
		body += "<MessageLines>";
		List<String> lines = tfsiUtil.splitMessageIntoLines(exu.encode(moob.getMessageText()));
		for(int ii=0; ii<lines.size(); ++ii){
			body += "<MessageText Line=\""+(ii+1)+"\">"+lines.get(ii)+"</MessageText>";
		}
		body += "</MessageLines>" +
					"<DeliveryDate>"+tfsiUtil.getDate(moob.getDeliveryDate())+"</DeliveryDate>" +
					"<OrderDate>"+tfsiUtil.getDate(moob.getMessageCreateTimestamp())+"</OrderDate>" +
				"</Cancel>";
		
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	@Override
	public TFSIResponse cancelTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildCancelTransaction("", moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"CancelOrder",messageSequenceNumber,"Canc",moob);
		
		return response;
	}
	
	public TFSIResponse confirmCancelTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildCancelTransaction("C", moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"ConfirmDenial",messageSequenceNumber,"Conf",moob);
		
		return response;
	}
	
	public TFSIResponse denyCancelTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildCancelTransaction("D", moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"ConfirmDenial",messageSequenceNumber,"Deni",moob);
		
		return response;
	}
	
	private TFSIXML buildMessageTransaction(MessageOnOrderBean moob){
		messageSequenceNumber++;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<ReceiverVSC>"+shopDAO.getTelefloraShopnetwork(moob.getReceivingShop().getShopId()).getShopCode()+"</ReceiverVSC>" +
		"<SenderPSC>"+tfsiProperties.getProperty("PSC")+"</SenderPSC>" +
		"<SenderVSC>"+tfsiProperties.getProperty("VSC")+"</SenderVSC>" +
		"<SenderSequenceNumber>"+messageSequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<Message xmlns:Dove=\"http://www.teleflora.com/Dove\" Operator=\""+tfsiProperties.getProperty("operator")+"\">" +
					"<SendingFlorist PSC=\""+tfsiProperties.getProperty("BloomNetShopCode")+"\">" +
						"<Name>BloomNet</Name>" +
						"<City>Carle Place</City>" +
						"<State>NY</State>" +
						"<Phones>" +
							"<Phone Preference=\"1\">5162376000</Phone>" +
						"</Phones>" +
					"</SendingFlorist>";
		body += "<MessageLines>";
		List<String> lines = tfsiUtil.splitMessageIntoLines(exu.encode(moob.getMessageText()));
		for(int ii=0; ii<lines.size(); ++ii){
			body += "<MessageText Line=\""+(ii+1)+"\">"+lines.get(ii)+"</MessageText>";
		}
		body += "</MessageLines>" +
				"</Message>";
		
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	public TFSIResponse messageTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildMessageTransaction(moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"GeneralMessage",messageSequenceNumber,"Inqr",moob);
		
		return response;
	}
	
	private TFSIXML buildInquiryTransaction(String shortType, MessageOnOrderBean moob){
		messageSequenceNumber++;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<SenderPSC>"+tfsiProperties.getProperty("PSC")+"</SenderPSC>" +
		"<SenderVSC>"+tfsiProperties.getProperty("VSC")+"</SenderVSC>" +
		"<SenderSequenceNumber>"+messageSequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<Inquiry xmlns:Dove=\"http://www.teleflora.com/Dove\" Type=\""+shortType+"\" OrigTID=\""+moob.getOrderNumber()+"\" Operator=\""+tfsiProperties.getProperty("operator")+"\">" +
					"<Recipient>" +
						"<FirstName>"+exu.encode(moob.getRecipientFirstName())+"</FirstName>" +
						"<LastName>"+exu.encode(moob.getRecipientLastName())+"</LastName>" +
						"<AddressLines>" +
						"<AddressLine Line=\"1\">"+exu.encode(moob.getRecipientAddress1())+"</AddressLine>";
					if(moob.getRecipientAddress2() != null && !moob.getRecipientAddress2().equals(""))
						body += "<AddressLine Line=\"2\">"+exu.encode(moob.getRecipientAddress2())+"</AddressLine>";
					body += "</AddressLines>" +
						"<City>"+exu.encode(moob.getRecipientCity())+"</City>" +
						"<State>"+exu.encode(moob.getRecipientState())+"</State>" +
						"<PostalCode>"+moob.getRecipientZipCode()+"</PostalCode>" +
						"<Email />" +
						"<Phones>" +
							"<Phone Preference=\"1\">"+moob.getRecipientPhoneNumber()+"</Phone>" +
						"</Phones>" +
					"</Recipient>";
		body += "<MessageLines>";
		List<String> lines = tfsiUtil.splitMessageIntoLines(exu.encode(moob.getMessageText()));
		for(int ii=0; ii<lines.size(); ++ii){
			body += "<MessageText Line=\""+(ii+1)+"\">"+lines.get(ii)+"</MessageText>";
		}
		body += "</MessageLines>" +
					"<DeliveryDate>"+tfsiUtil.getDate(moob.getDeliveryDate())+"</DeliveryDate>" +
					"<OrderDate>"+tfsiUtil.getDate(moob.getMessageCreateTimestamp())+"</OrderDate>" +
				"</Inquiry>";
		
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	@Override
	public TFSIResponse inquiryTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildInquiryTransaction("I", moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"InquiryResponse",messageSequenceNumber,"Inqr",moob);
		
		return response;
	}
	
	@Override
	public TFSIResponse inquiryResponseTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildInquiryTransaction("R", moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"InquiryResponse",messageSequenceNumber,"Resp",moob);
		
		return response;
	}
	
	private TFSIXML buildDNRTransaction(String shortType, MessageOnOrderBean moob){
		messageSequenceNumber++;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<SenderPSC>"+tfsiProperties.getProperty("PSC")+"</SenderPSC>" +
		"<SenderVSC>"+tfsiProperties.getProperty("VSC")+"</SenderVSC>" +
		"<SenderSequenceNumber>"+messageSequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<DNR xmlns:Dove=\"http://www.teleflora.com/Dove\" Type=\""+shortType+"\" VSC=\""+shopDAO.getTelefloraShopnetwork(moob.getReceivingShop().getShopId()).getShopCode()+"\" Operator=\""+tfsiProperties.getProperty("operator")+"\">" +
				"<SendingFlorist PSC=\""+tfsiProperties.getProperty("BloomNetShopCode")+"\">" +
						"<Name>BloomNet</Name>" +
						"<City>Carle Place</City>" +
						"<State>NY</State>" +
						"<Phones>" +
							"<Phone Preference=\"1\">5162376000</Phone>" +
						"</Phones>" +
						"<SequenceNumber>00001</SequenceNumber>" +
					"</SendingFlorist>" +
				"</DNR>";
		
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	public TFSIResponse DNRTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildDNRTransaction("Add", moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"DNR",messageSequenceNumber,"Info",moob);
		
		return response;
	}
	
	public TFSIResponse removeDNRTransaction(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildDNRTransaction("Delete", moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"DNR",messageSequenceNumber,"Info",moob);
		
		return response;
	}
	
	public TFSIXML buildDeliveryConfirmation(MessageOnOrderBean moob){
		messageSequenceNumber++;
		
		String header = "<?xml version=\"1.0\"?><TFETransactionHeader>" +
		"<SenderPSC>"+tfsiProperties.getProperty("PSC")+"</SenderPSC>" +
		"<SenderVSC>"+tfsiProperties.getProperty("VSC")+"</SenderVSC>" +
		"<SenderSequenceNumber>"+messageSequenceNumber+"</SenderSequenceNumber>" +
		"</TFETransactionHeader>";
		
		String body = "<?xml version=\"1.0\"?>" +
				"<Inquiry xmlns:Dove=\"http://www.teleflora.com/Dove\" Type=\"I\" OrigTID=\""+moob.getOrderNumber()+"\" Operator=\""+tfsiProperties.getProperty("operator")+"\">" +
					"<Recipient>" +
						"<FirstName>"+exu.encode(moob.getRecipientFirstName())+"</FirstName>" +
						"<LastName>"+exu.encode(moob.getRecipientLastName())+"</LastName>" +
						"<AddressLines>" +
						"<AddressLine Line=\"1\">"+exu.encode(moob.getRecipientAddress1())+"</AddressLine>";
					if(moob.getRecipientAddress2() != null && !moob.getRecipientAddress2().equals(""))
						body += "<AddressLine Line=\"2\">"+exu.encode(moob.getRecipientAddress2())+"</AddressLine>";
					body += "</AddressLines>" +
						"<City>"+exu.encode(moob.getRecipientCity())+"</City>" +
						"<State>"+exu.encode(moob.getRecipientState())+"</State>" +
						"<PostalCode>"+moob.getRecipientZipCode()+"</PostalCode>" +
						"<Email />" +
						"<Phones>" +
							"<Phone Preference=\"1\">"+moob.getRecipientPhoneNumber()+"</Phone>" +
						"</Phones>" +
					"</Recipient>" +
					"<MessageLines>" +
						"<MessageText Line=\"1\">DELIVERY CONFIRMATION</MessageText>" +
						"<MessageText Line=\"1\">DATE DELIVERED: "+moob.getDateOrderDelivered()+"</MessageText>" +
						"<MessageText Line=\"1\">TIME DELIVERED: </MessageText>" +
						"<MessageText Line=\"1\">SIGNED BY: </MessageText>" +
					"</MessageLines>" +
					"<DeliveryDate>"+tfsiUtil.getDate(moob.getDeliveryDate())+"</DeliveryDate>" +
					"<OrderDate>"+tfsiUtil.getDate(moob.getMessageCreateTimestamp())+"</OrderDate>" +
				"</Inquiry>";
		
		TFSIXML tfsiXML = new TFSIXML();
		tfsiXML.setBody(body);
		tfsiXML.setHeader(header);
		return tfsiXML;
	}
	
	public TFSIResponse deliveryConfirmation(MessageOnOrderBean moob){
		TFSIXML tfsiXML = buildDeliveryConfirmation(moob);
		
		String header = tfsiXML.getHeader();
		String body = tfsiXML.getBody();
		
		TFSIResponse response = sendSOAPMessage(header,body,"InquiryResponse",messageSequenceNumber,"Dlcf",moob);
		
		return response;
	}
}


