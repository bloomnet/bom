package com.bloomnet.bom.webservice.test;


import java.io.File;
import java.io.UnsupportedEncodingException;

import javax.jms.TextMessage;

import org.easymock.EasyMock;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.core.JmsTemplate;

import com.bloomnet.bom.common.dao.BusinessLogicDAO;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.RoutingDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.StsMessage;
import com.bloomnet.bom.common.service.BusinessLogicService;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.webservice.service.FSIService;

public class FSIServiceTest {
	
	FSIService fsiService;
	MessageDAO messageDAO;
	MessageProducer producer;
	private JmsTemplate mockTemplate;
	RoutingDAO routingDAO;
	BusinessLogicDAO businessLogicDAO;
	private TextMessage mock;
	FSIClient client;
	BusinessLogicService businesslogicservice;
	OrderDAO orderDAO;
	ShopDAO shopDAO;


	
	
	@Before
	public void setup(){
//		fsiService = new FSIServiceImpl();
//		messageDAO = EasyMock.createMock(MessageDAO.class);
//		fsiService.setMessageDAO(messageDAO);
//		mock = EasyMock.createMock(TextMessage.class);
//		
//		producer = EasyMock.createNiceMock(MessageProducer.class);
//		
//		fsiService.setMessageProducer(producer);
//		client = EasyMock.createNiceMock(FSIClientImpl.class);
//		fsiService.setFsiClient(client);
//		
//		routingDAO = EasyMock.createNiceMock(RoutingDAO.class);
//		fsiService.setRoutingDAO(routingDAO);
//		
//		businessLogicDAO = EasyMock.createMock(BusinessLogicDAO.class);
//		//fsiService.setBusinessLogicDAO(businessLogicDAO);
//		
//		
//		businesslogicservice = EasyMock.createMock(BusinessLogicService.class);
//		businesslogicservice.setMessageDAO(messageDAO);
//		businesslogicservice.setBusinessLogicDAO(businessLogicDAO);
//		fsiService.setBusinessLogicService(businesslogicservice);
//
//		
//		orderDAO = EasyMock.createNiceMock(OrderDAO.class);
//		fsiService.setOrderDAO(orderDAO);
//		
//		shopDAO = EasyMock.createNiceMock(ShopDAO.class);
//		fsiService.setShopDAO(shopDAO);
	}
	
	/**
	 * 
	 */
/*	@Test
	public void testProcessMessageRjctChunkCommMethodFSI(){
		
		String msg =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>5</total><numOfOrderMessages>5</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383383</bmtOrderNumber><bmtSeqNumberOfOrder>36262989</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267521</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3120</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383382</bmtOrderNumber><bmtSeqNumberOfOrder>36262988</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267519</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3118</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383381</bmtOrderNumber><bmtSeqNumberOfOrder>36262987</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267518</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3117</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383379</bmtOrderNumber><bmtSeqNumberOfOrder>36262719</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267251</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3116</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383380</bmtOrderNumber><bmtSeqNumberOfOrder>36262986</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267520</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3119</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383386</bmtOrderNumber><bmtSeqNumberOfOrder>36263258</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267789</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3122</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383385</bmtOrderNumber><bmtSeqNumberOfOrder>36263257</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267788</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3121</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder></foreignSystemInterface>";

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageAckfSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_ackf_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageCancSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_canc_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

		EasyMock.expect(orderDAO.getCurrentFulfillingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageConfSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_conf_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageDeniSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_deni_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageDlcfSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_dlcf_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageDlouSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_dlou_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageInqrSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_inqr_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

		EasyMock.expect(orderDAO.getCurrentFulfillingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessagePchgSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_pchg_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

		EasyMock.expect(orderDAO.getCurrentFulfillingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageRespSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_resp_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

		EasyMock.expect(orderDAO.getCurrentFulfillingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	
	@Test
	public void testProcessMessageOrderSingleCommMethodFSI(){
		File file = new File("test-msgs/fsi_inbound_order_general_type.xml");
		FileReader fd = new FileReader();
		 String msg = fd.getFileAsString(file);
		System.out.print( msg);
		

		EasyMock.expect(orderDAO.getCurrentFulfillingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();

	    EasyMock.expect(messageDAO.getCommunicationMethod((String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    EasyMock.expect(orderDAO.getOriginalSendingShopCode((String) EasyMock.anyObject())).andReturn("Y833").anyTimes();
	    EasyMock.expect( messageDAO.getCommethodByDesc((String) EasyMock.anyObject())).andReturn(new Commmethod()).anyTimes();
	    EasyMock.expect( messageDAO.getMessageTypeByShortDesc((String) EasyMock.anyObject())).andReturn(new Messagetype()).anyTimes();
	    EasyMock.expect(orderDAO.getActivityTypeByDesc((String) EasyMock.anyObject())).andReturn(new Oactivitytype()).anyTimes();
	    EasyMock.expect(messageDAO.getStsById(EasyMock.anyByte())).andReturn(new StsMessage()).anyTimes();
	    
	    
	    //EasyMock.expect(messageDAO.persistMessage( (ActMessage) EasyMock.anyObject()));

	    
	   // EasyMock.expect(messageDAO.getPreviousDestination((String) EasyMock.anyObject())).andReturn("12334").anyTimes();
	    //EasyMock.expect(businessLogicDAO.isDefaultRoute()).andReturn(true).anyTimes();
	   // EasyMock.expect(businessLogicDAO.getCurrentQueueRoute()).andReturn("FSI,TLO,TFSI").anyTimes();
	    EasyMock.expect(businesslogicservice.DetermineRoute((String) EasyMock.anyObject(),(String) EasyMock.anyObject())).andReturn("BMTFSI").anyTimes();
	    
	   // EasyMock.expect(orderDAO.getCityByName((String) EasyMock.anyObject())).andReturn(new City()).anyTimes();
	    EasyMock.replay(messageDAO);
	    


	    EasyMock.replay(producer);
	    EasyMock.replay(client);
	    EasyMock.replay(routingDAO);
	    EasyMock.replay(businesslogicservice);
	    EasyMock.replay(orderDAO);
	    EasyMock.replay(shopDAO);







		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		EasyMock.verify(messageDAO);
		EasyMock.verify(producer);
		EasyMock.verify(client);
		EasyMock.verify(routingDAO);
	//	EasyMock.verify(businesslogicservice);
		EasyMock.verify(orderDAO);
		
		EasyMock.verify(shopDAO);

		
	}
	*/
	@Test
	public void testCheckForMessages(){
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-test.xml"));

		//ThreadPoolTaskScheduler task = (ThreadPoolTaskScheduler) factory.getBean("Scheduler");
	
		FSIClient ping = (FSIClient) factory.getBean("fsiClient");

		try {
			ping.checkForMessages();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//fsiService.checkForMessages();
	}

	
	/*public void testDetermineRoute(){
		
		String currentRoute = "FSI,TLO,TFSI";
		String previousDestination = "TLO";
		String nextDestination = "";
		
		StringTokenizer st = new StringTokenizer(currentRoute, ","); 
		while(st.hasMoreTokens()) { 
			String destination = st.nextToken(); 
			System.out.println(destination);
			if (destination.equals(previousDestination)){
				continue;
			}
			else{
				nextDestination = destination;
				break;
			}	
		}
		
		System.out.println("nextDestination is " + nextDestination);

	}
	
	
	public void searchShopCodeByDeliveryDateAndZipCodeTest(){
		
		String zipCode = "11550";
		String deliveryDate;
		deliveryDate = DateUtil.formatDateString("10012011");
		System.out.println(deliveryDate);
		String shops = fsiService.searchShopCodeByDeliveryDateAndZipCode(deliveryDate, zipCode);
		System.out.println(shops);

	}
	
public void processMessage() {
		
		String msg = new String();
		msg =  "<?xml version=\"1.0\" encoding=\"UTF-8\"?><foreignSystemInterface><errors/><pendingMessages><total>5</total><numOfOrderMessages>5</numOfOrderMessages><numOfAckfMessages>0</numOfAckfMessages><numOfGeneralMessages>0</numOfGeneralMessages></pendingMessages><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383383</bmtOrderNumber><bmtSeqNumberOfOrder>36262989</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267521</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3120</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383382</bmtOrderNumber><bmtSeqNumberOfOrder>36262988</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267519</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3118</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383381</bmtOrderNumber><bmtSeqNumberOfOrder>36262987</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267518</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3117</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383379</bmtOrderNumber><bmtSeqNumberOfOrder>36262719</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267251</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3116</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383380</bmtOrderNumber><bmtSeqNumberOfOrder>36262986</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267520</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3119</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383386</bmtOrderNumber><bmtSeqNumberOfOrder>36263258</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267789</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3122</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder><messagesOnOrder><messageCount>1</messageCount><messageRjct><messageType>4</messageType><sendingShopCode>M1770000</sendingShopCode><receivingShopCode>Q8830000</receivingShopCode><fulfillingShopCode>M1770000</fulfillingShopCode><systemType>GENERAL</systemType><identifiers><generalIdentifiers><bmtOrderNumber>3383385</bmtOrderNumber><bmtSeqNumberOfOrder>36263257</bmtSeqNumberOfOrder><bmtSeqNumberOfMessage>36267788</bmtSeqNumberOfMessage><externalShopOrderNumber>1004</externalShopOrderNumber><inwireSequenceNo>3121</inwireSequenceNo></generalIdentifiers></identifiers><messageCreateTimestamp>20110907164805</messageCreateTimestamp><messageText>Selected Florist is Not Available.  Please Select Another Florist.</messageText></messageRjct></messagesOnOrder></foreignSystemInterface>";
		try {
			fsiService.processMessage(msg);
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}
	*/
	




}
