package com.bloomnet.bom.webservice.test;
import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.easymock.EasyMock;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.core.JmsTemplate;

//import com.bloomnet.bom.webservice.jms.FSIOutMessageListener;
//import com.bloomnet.bom.webservice.jms.MessageConsumer;

public class FSIOutMessageListenerTest {
	
	private TextMessage textMsg;
	private Message message;

//	private FSIOutMessageListener listener;

	@Before
	public void setUp() {
		textMsg = EasyMock.createMock(TextMessage.class);
		message = EasyMock.createMock(Message.class);
		
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-test.xml"));

		//FSIOutMessageListener fsiOutMessageListener = (FSIOutMessageListener) factory.getBean("fsiOutMessageListener");

		//listener = fsiOutMessageListener;

		}

		@Test
		public void testOnMessage() throws JMSException {
			EasyMock.expect(textMsg.getText()).andReturn("").anyTimes();
			EasyMock.expect(textMsg.getStringProperty((String)EasyMock.anyObject())).andReturn("").anyTimes();
			EasyMock.replay(textMsg);
			EasyMock.replay(message);
		//listener.onMessage(textMsg);
		EasyMock.verify(textMsg);
		EasyMock.verify(message);
		}
	
}
