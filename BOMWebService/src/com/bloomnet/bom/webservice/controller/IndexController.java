package com.bloomnet.bom.webservice.controller;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Controller for the "index" web application screen.
 *
 * @author Danil Svirchtchev
 */
@Controller
@RequestMapping("/index.htm")
public class IndexController {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( IndexController.class );
    
    
	@RequestMapping(method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
								 ModelMap model ) {
		
		model.addAttribute( "message", "Working..." );
		
		return "index";
	}
}
