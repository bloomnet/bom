package com.bloomnet.bom.webservice.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.Session;
import javax.jms.TextMessage;


import org.apache.log4j.Logger;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.MessageCreator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;

@Service("bloomLinkMessageProducer")
@Transactional(propagation = Propagation.SUPPORTS)
public class BloomLinkMessageProducer {
	
	static final Logger logger = Logger.getLogger(BloomLinkMessageProducer.class);
	
	@Autowired private JmsTemplate jmsProducer;
	  



public void setJmsProducer(JmsTemplate jmsProducer) {
		this.jmsProducer = jmsProducer;
	}

public BloomLinkMessageProducer() {	}

/**
 * sends orders to  BMTFSI, TLO and TSI destinations
 * @param destination
 * @param message
 * @param orderNumber
 */
@Transactional(propagation=Propagation.REQUIRED)
public void produceMessage(String destination, String message, final String orderNumber,final String sendingShopCode,  final String deliveryDate,
		final String city,final String zip, final String occasionCode,final String skillset, final String messageType, final String orderType) {
	
	final String msg = message;

	jmsProducer.send(destination,new MessageCreator(){
		public Message createMessage(Session session)throws JMSException{


			String ORDERNUMBER = "";
			String SENDINGSHOP = "";
			String DELIVERYDATE="";
			String CITY="";
			String ZIP="";
			String OCCASSIONCODE="";
			String SKILLSET="";
			String MESSAGETYPE="";
			String ORDERTYPE = "";


			ORDERNUMBER = "ORDERNUMBER";
			String value1 = orderNumber;

			SENDINGSHOP = "SENDINGSHOP";
			String value2 = sendingShopCode;

			DELIVERYDATE = "DELIVERYDATE";
			String value3 = deliveryDate;

			CITY = "CITY";
			String value4 = city;

			ZIP = "ZIP";
			String value5 =zip;

			OCCASSIONCODE = "OCCASSIONCODE";
			String value6  = occasionCode;

			SKILLSET="SKILLSET";
			String value7 = skillset;

			MESSAGETYPE = "MESSAGETYPE";
			String value8 = messageType;
			
			ORDERTYPE ="ORDERTYPE";
			String value9 = orderType;


			TextMessage textMessage = session.createTextMessage(msg);
			textMessage.setStringProperty(ORDERNUMBER, value1);
			textMessage.setStringProperty(SENDINGSHOP, value2);
			textMessage.setStringProperty(DELIVERYDATE, value3);
			textMessage.setStringProperty(CITY, value4);
			textMessage.setStringProperty(ZIP, value5);
			textMessage.setStringProperty(OCCASSIONCODE, value6);
			textMessage.setStringProperty(SKILLSET, value7);
			textMessage.setStringProperty(MESSAGETYPE, value8);
			textMessage.setStringProperty(ORDERTYPE, value9);



			return textMessage;
		}
	});

	if(logger.isDebugEnabled()) {   	
		logger.debug("sending message to " + destination);

	}	
	  		}

/**
 * sends messageOnOrder to NEW-MESSAGES and MESSAGES destinations
 * @param destination
 * @param message
 * @param orderNumber
 */
@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
public void produceMessage(String destination, String message, final String orderNumber,final String sendingShopCode,final String messageType,final String skillset) {
	  final String msg = message;
	  		try {
	  			jmsProducer.send(destination,new MessageCreator(){
 public Message createMessage(Session session)throws JMSException{
						String ORDERNUMBER = "";
						String SENDINGSHOP = "";
						String MESSAGETYPE = "";
						String SKILLSET = "";

						ORDERNUMBER = "ORDERNUMBER";
						String value1 = orderNumber;
						
						SENDINGSHOP = "SENDINGSHOP";
						String value2 = sendingShopCode;
						
						MESSAGETYPE = "MESSAGETYPE";
						String value3 = messageType;
						
						SKILLSET="SKILLSET";
						String value4 = skillset;

						TextMessage textMessage = session.createTextMessage(msg);
						textMessage.setStringProperty(ORDERNUMBER, value1);
						textMessage.setStringProperty(SENDINGSHOP, value2);
						textMessage.setStringProperty(MESSAGETYPE, value3);
						textMessage.setStringProperty(SKILLSET, value4);


						return textMessage;
						}
						});
			} catch (JmsException e) {
				logger.error("JMS Exception: " + e.getMessage());
				e.printStackTrace();
			}
	    	
	  		if(logger.isDebugEnabled()) {   	
	  			logger.debug("sending message to " + destination);
	  			}	
	  		}
	
 
 
}

