package com.bloomnet.bom.webservice.jms;

import java.util.Date;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageProducer;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.command.ActiveMQQueue;
import org.apache.log4j.Logger;
import org.springframework.jms.listener.SessionAwareMessageListener;

import com.bloomnet.bom.common.exceptions.WrongDestinationException;
import com.bloomnet.bom.common.service.BusinessLogicService;
import com.bloomnet.bom.webservice.service.FSIAgentService;

public class FSIMessageListener implements SessionAwareMessageListener<Message> {

	// Define a static logger variable
	static final Logger logger = Logger.getLogger(FSIMessageListener.class);

	// Injected dependency
	private FSIAgentService fsiAgent;
	
	@Override
	public void onMessage(Message message, Session session) throws JMSException {

		String result = "";
		String order = "";
		String orderNumber = new String();

		TextMessage msg = (TextMessage) message;
		try {
			if (logger.isDebugEnabled()) {

				logger.debug("***************FSIMessageListener onMessage***************");
				logger.debug("received: " + msg.getText());
			}
			order = msg.getText();
			orderNumber = msg.getStringProperty("ORDERNUMBER");
			String orderType = msg.getStringProperty("ORDERTYPE");
			
			logger.debug("Processing order: "+ orderNumber);
			logger.debug("Start time: " + new Date());

			getFsiAgent().handleOrder(order, orderNumber, orderType);
			
			logger.debug("End time: " + new Date());

			

		}   catch (Exception e) {
			e.printStackTrace();
			//throw new JMSException("BMTFSI: Unable to process order " + orderNumber +  "\n Received exception: " + e.getMessage());
		}
	}


	


	public void setFsiAgent(FSIAgentService fsiAgent) {
		this.fsiAgent = fsiAgent;
	}


	public FSIAgentService getFsiAgent() {
		return fsiAgent;
	}


	
}
