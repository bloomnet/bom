package com.bloomnet.bom.webservice.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.SpecialQueues;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageCanc;
import com.bloomnet.bom.common.jaxb.fsi.MessageDisp;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspr;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.jaxb.fsi.MessageInqr;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagePchg;
import com.bloomnet.bom.common.jaxb.fsi.MessageResp;
import com.bloomnet.bom.common.jaxb.fsi.MessageRjct;
import com.bloomnet.bom.common.tfsi.TFSIUtil;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.webservice.service.TFSIAgentService;
import com.bloomnet.bom.webservice.service.TFSIService;

@Service("tfsiAgent")
@Transactional(propagation = Propagation.REQUIRED)
public class TFSIAgentServiceImpl implements TFSIAgentService {
	
	@Autowired private TFSIService tfsiService;
	
    static Logger logger = Logger.getLogger( TFSIAgentServiceImpl.class );
	
	@Autowired private ShopDAO shopDAO;
	@Autowired private OrderDAO orderDAO;
	@Autowired private MessageDAO messageDAO;
	@Autowired private UserDAO userDAO;
	
	@Autowired private Properties tfsiProperties;
	@Autowired private Properties fsiProperties;
	@Autowired private Properties bomProperties;
	
	@Autowired MessageProducer messageProducer;
	@Autowired private TransformService transformService;	

	
	public TFSIAgentServiceImpl(){}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private MessageOnOrderBean sendMessageOnOrder(Object obj, ForeignSystemInterface fsi, String messageText){
		
		Transform.PROPERTIES = fsiProperties;

		
		String orderNumber = ((BloomNetMessage) obj).getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
		
		MessageOnOrderBean moob = new MessageOnOrderBean();
		
		Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
		fsi = transformService.convertToJavaFSI(parentOrder.getOrderXml());
		MessageOrder order = fsi.getMessagesOnOrder().get(0).getMessageOrder();
		
		final Shop originalReceivingShop  = shopDAO.getShopByCode(((BloomNetMessage) obj).getReceivingShopCode());
        final Shop originalSendingShop    = shopDAO.getShopByCode(((BloomNetMessage) obj).getSendingShopCode());
        final Shop originalFulfillingShop = shopDAO.getShopByCode(((BloomNetMessage) obj).getFulfillingShopCode());
		
		try{
        	moob = transformService.createBeanFromFSI(fsi,originalReceivingShop,originalSendingShop,originalFulfillingShop);
        	moob.setFulfillingShop(new Shop());
        	moob.setReceivingShop(new Shop());
        }catch(Exception ee){
        	return null;
        }
        moob.setMessageText(messageText);
        moob.setCommMethod(Byte.valueOf("1"));
        moob.setMessageCreateTimestamp(DateUtil.toDate(order.getMessageCreateTimestamp()));
        moob.setStatus(Byte.valueOf("2"));
        
        Shop sendingShop = shopDAO.getShopByCode(tfsiProperties.getProperty("BloomNetShopCode"));
        moob.setSendingShop(sendingShop);
      
        Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
        
        moob.setOrderNumber(childOrder.getOrderNumber());        
        Shop receivingShop = childOrder.getShopByReceivingShopId();
		moob.setReceivingShop(receivingShop);
		
		return moob;
	}
	
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	private void sendOrderMessage(String xml) throws Exception{
		
		Transform.PROPERTIES = fsiProperties;

		/*SpecialQueues queue = new SpecialQueues();
		
		queue.setTfsiFirst(0);
		queue.setTloFirst(1);
		queue.setAutomatedOnly(0);
		
		String tloFirst = shopDAO.getSpecialQueues(queue);
		
		queue.setTfsiFirst(0);
		queue.setTloFirst(0);
		queue.setAutomatedOnly(1);
				
		String automatedOnly = shopDAO.getSpecialQueues(queue);*/
		
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageOrder order = fsi.getMessagesOnOrder().get(0).getMessageOrder();
		MessageOnOrderBean moob = new MessageOnOrderBean();
		
		final Shop originalReceivingShop  = shopDAO.getShopByCode(order.getReceivingShopCode());
        final Shop originalSendingShop    = shopDAO.getShopByCode(order.getSendingShopCode());
        final Shop originalFulfillingShop = shopDAO.getShopByCode(order.getFulfillingShopCode());
        
        try{
        	moob = transformService.createBeanFromFSI(fsi,originalReceivingShop,originalSendingShop,originalFulfillingShop);
        	moob.setFulfillingShop(new Shop());
        	moob.setReceivingShop(new Shop());
        }catch(Exception ee){
        	if (moob.getRecipientCountryCode().equals("US") || moob.getRecipientCountryCode().equals("USA")){
        		rejectOrder(moob, order, order.getSendingShopCode(), xml);
        	}else {
        		sendToTLOQueue(xml, order);
        	}
        	return;
        }
        
        boolean hasFullingshop = true;
		hasFullingshop = checkForActiveFullingShop(moob.getOrderNumber());
		
		if (hasFullingshop){
			return;
		}
		
		String originalSendingShopCode = order.getSendingShopCode();
	   
        if(moob.getOccasionId().equals("1"))moob.setOccasionId(BOMConstants.TFSI_FUNERAL);
        else if(moob.getOccasionId().equals("2"))moob.setOccasionId(BOMConstants.TFSI_ILLNESS);
        else if(moob.getOccasionId().equals("3"))moob.setOccasionId(BOMConstants.TFSI_BIRTHDAY);
        else if(moob.getOccasionId().equals("4"))moob.setOccasionId(BOMConstants.TFSI_BUSINESSGIFT);
        else if(moob.getOccasionId().equals("5"))moob.setOccasionId(BOMConstants.TFSI_HOLIDAY);
        else if(moob.getOccasionId().equals("6"))moob.setOccasionId(BOMConstants.TFSI_MATERNITY);
        else if(moob.getOccasionId().equals("7"))moob.setOccasionId(BOMConstants.TFSI_ANNIVERSARY);
        else if(moob.getOccasionId().equals("8"))moob.setOccasionId(BOMConstants.TFSI_OTHER);
        else moob.setOccasionId(BOMConstants.TFSI_OTHER);
        
        moob.setCommMethod(Byte.valueOf("1"));
        
        Shop sendingShop = shopDAO.getShopByCode(tfsiProperties.getProperty("BloomNetShopCode"));
        moob.setSendingShop(sendingShop);
        
		Shop receivingShop = searchForShops(moob, originalSendingShopCode);
		
		String currentDateStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date delDate = DateUtil.toDateFormat(moob.getDeliveryDate());
		Date currentDate = DateUtil.toDate(currentDateStr);
		int eq = delDate.compareTo(currentDate);
		
		if(eq < 0 || receivingShop == null || 
				   receivingShop.getShopId() == null
				|| receivingShop.getShopId().equals("") 
				|| order.getOrderDetails().getOrderProductsInfo().getOrderProductInfoDetails().size() > 12){
			
			if (moob.getRecipientCountryCode().equals("US") || moob.getRecipientCountryCode().equals("USA")){
				
				rejectOrder(moob, order, originalSendingShopCode, xml);
				return;
			}else{
				sendToTLOQueue(xml, order);
				return;
			}
		}
		moob.setReceivingShop(receivingShop);
		
		try {
			orderDAO.persistOrderactivityByUser(
					tfsiProperties.getProperty("userTFSI"),
					BOMConstants.ACT_ROUTING, 
					bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED), 
					moob.getBmtOrderNumber(), 
					BOMConstants.TFSI_QUEUE);
			
			long childId = 0;
			String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber());
			if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
				childId = orderDAO.getCurrentFulfillingOrder(moob.getOrderNumber()).getBomorderId();
			}
			
			byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TFSI_QUEUE );
			
			Bomorder m_order = orderDAO.getOrderByOrderNumber(moob.getBmtOrderNumber());
			BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(m_order.getBomorderId());
			bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
			bomorderSts.setStsRoutingId(Byte.parseByte(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)));
			bomorderSts.setVirtualqueueId(virtualqueueId);
			
			orderDAO.updateBomorderSts(bomorderSts, childId);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		tfsiService.sendOrder(moob);
	}
	
    public void sendInquiryMessage(String xml){
    	
    	Transform.PROPERTIES = fsiProperties;

    	
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageInqr inqr = fsi.getMessagesOnOrder().get(0).getMessageInqr().get(0);
		String messageText = inqr.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(inqr, fsi, messageText);
		
		tfsiService.sendInqr(moob);
    }
    
    public void sendDispMessage(String xml){
    	
    	Transform.PROPERTIES = fsiProperties;
    	
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageDisp disp = fsi.getMessagesOnOrder().get(0).getMessageDisp().get(0);
		String messageText = disp.getReasonForDispute() + " " + disp.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(disp, fsi, messageText);
		
		tfsiService.sendInqr(moob); //TF does not have a Dispute message type, so send it as an inquiry
    }

    public void sendDsprMessage(String xml){
	
		Transform.PROPERTIES = fsiProperties;
		
		ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageDspr dspr = fsi.getMessagesOnOrder().get(0).getMessageDspr().get(0);
		String messageText = dspr.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(dspr, fsi, messageText);
		
		tfsiService.sendInqr(moob); //TF does not have a Dispute Rescind message type, so send it as an inquiry
}
    
    public void sendResponseMessage(String xml){
    	
    	Transform.PROPERTIES = fsiProperties;

    	
    	ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageResp resp = fsi.getMessagesOnOrder().get(0).getMessageResp().get(0);
		String messageText = resp.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(resp, fsi, messageText);

		tfsiService.sendInqrResponse(moob);
    }
    
    public void sendRejectMessage(String xml){
    	
    	Transform.PROPERTIES = fsiProperties;

    	ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageRjct rjct = fsi.getMessagesOnOrder().get(0).getMessageRjct().get(0);
		String messageText = rjct.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(rjct, fsi, messageText);
		
		tfsiService.sendRjt(moob);
    }
    
    @Transactional(propagation = Propagation.REQUIRED)
    public void sendCancelMessage(String xml){
    	
    	Transform.PROPERTIES = fsiProperties;


    	ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageCanc canc = fsi.getMessagesOnOrder().get(0).getMessageCanc().get(0);
		String messageText = canc.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(canc, fsi, messageText);
		
		String parentOrderNumber = canc.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
		
		Bomorder parentOrder = orderDAO.getOrderByOrderNumber(parentOrderNumber);
		Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(parentOrderNumber);
		
		try {
			
			orderDAO.persistChildOrderactivityByUser(
					tfsiProperties.getProperty("userTFSI"), 
					BOMConstants.ACT_ROUTING,
					bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_BY_SSHOP),
					childOrder.getOrderNumber(), 
					BOMConstants.TFSI_QUEUE);
			
			orderDAO.persistOrderactivityByUser(
					tfsiProperties.getProperty("userTFSI"),
					BOMConstants.ACT_ROUTING, 
					bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_BY_SSHOP), 
					parentOrder.getOrderNumber(), 
					BOMConstants.TFSI_QUEUE);
		} catch (Exception e) {
		}
		
		tfsiService.sendCanc(moob);
    }
    
    public void sendInfoMessage(String xml){
    	
    	Transform.PROPERTIES = fsiProperties;


    	ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessageInfo info = fsi.getMessagesOnOrder().get(0).getMessageInfo().get(0);
		String messageText = info.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(info, fsi, messageText);
	
		tfsiService.sendInqr(moob);
    }
    
    public void sendPriceChangeMessage(String xml){
    	
    	Transform.PROPERTIES = fsiProperties;


    	ForeignSystemInterface fsi = transformService.convertToJavaFSI(xml);
		MessagePchg pchg = fsi.getMessagesOnOrder().get(0).getMessagePchg().get(0);
		String messageText = pchg.getMessageText();
		
		MessageOnOrderBean moob = sendMessageOnOrder(pchg, fsi, messageText);
		moob.setTotalCost(pchg.getPrice());
		
		tfsiService.sendPchg(moob);
    }
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.TFSIAgentService#sendMessage(java.lang.String, java.lang.String)
	 */
	@Override
	public void sendMessage ( String messageXML, String messageType ) throws Exception {
		//invoked by TFSIMessageListener
		Transform.PROPERTIES = fsiProperties;
		
		if(messageType.equals("ORDER")) sendOrderMessage(messageXML);
		else if(messageType.equals("INQR")) sendInquiryMessage(messageXML);
		else if(messageType.equals("RESP")) sendResponseMessage(messageXML);
		else if(messageType.equals("RJCT")) sendRejectMessage(messageXML);
		else if(messageType.equals("CANC")) sendCancelMessage(messageXML);
		else if(messageType.equals("INFO")) sendInfoMessage(messageXML);
		else if(messageType.equals("PCHG")) sendPriceChangeMessage(messageXML);
		else if(messageType.equals("DISP")) sendDispMessage(messageXML);
		else if(messageType.equals("DSPR")) sendDsprMessage(messageXML);
	}
	
	private String getDayOfTheWeek(String date){
		String stringDate;
		String DATE_FORMAT = "MM/dd/yyyy";
		String END_DATE_FORMAT = "E";
		SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
		Date d1;
		try {
			d1 = sdf.parse(date);
			SimpleDateFormat sdf2 = new SimpleDateFormat(END_DATE_FORMAT);
			stringDate = sdf2.format(d1);
			return stringDate;
		} catch (ParseException e) {
		}
		return null;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.TFSIAgentService#sendToTLOQueue(java.lang.String, com.bloomnet.bom.common.jaxb.fsi.MessageOrder)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void sendToTLOQueue(String xml, MessageOrder order) throws JMSException{
		
		try {
			String orderNumber = order.getOrderDetails().getOrderNumber();
			if(orderNumber == null) orderNumber = order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();

			orderDAO.persistOrderactivityByUser(
					tfsiProperties.getProperty("userTFSI"),
					BOMConstants.ACT_ROUTING, 
					bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED), 
					orderNumber, 
					BOMConstants.TLO_QUEUE);
			
			long childId = 0;
			String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber());
			if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
				childId = orderDAO.getCurrentFulfillingOrder(order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber()).getBomorderId();
			}
			
			byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TLO_QUEUE );
			BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(orderDAO.getOrderByOrderNumber(order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber()).getBomorderId());
			bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
			bomorderSts.setStsRoutingId(Byte.parseByte(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)));
			bomorderSts.setVirtualqueueId(virtualqueueId);
		
			orderDAO.updateBomorderSts(bomorderSts, childId);
			
		} catch (Exception e) {
		}
	}
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.webservice.service.impl.TFSIAgentService#searchForShops(com.bloomnet.bom.common.bean.MessageOnOrderBean, java.lang.String)
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public Shop searchForShops(MessageOnOrderBean moob, String originalSendingShopCode){

		logger.debug("Searching for shops for order Number: "+ moob.getOrderNumber());
		Shop receivingShop = null;
		try{
			String recipientCountry = moob.getRecipientCountryCode();
			
			boolean isInternational = false;
			
			if(recipientCountry != null && !recipientCountry.equals("") && !recipientCountry.equalsIgnoreCase("USA")
					&& !recipientCountry.equalsIgnoreCase("US") && !recipientCountry.equalsIgnoreCase("CAN") && !recipientCountry.equalsIgnoreCase("CA"))
				isInternational = true;
			
			if(isInternational)
				return shopDAO.getShopByCode("AFSI0000");
			
			Thread.sleep(1000);
			if(moob.getRecipientZipCode().length() > 5) moob.setRecipientZipCode(moob.getRecipientZipCode().substring(0,3));
			Zip zip = orderDAO.getZipByCode(moob.getRecipientZipCode());
			String dayOfTheWeek = getDayOfTheWeek(moob.getDeliveryDate());
			if(moob.getOrderNumber() == null || moob.getOrderNumber().length() < 5)
				moob.setOrderNumber(moob.getBmtOrderNumber());
			long parentOrderId = orderDAO.getOrderByOrderNumber(moob.getOrderNumber()).getBomorderId();
			
			if(parentOrderId == 0){
	
				return null;
				
			}
			
			List<Shop> shopsUsed = orderDAO.getTFShopsAlreadySentTo(parentOrderId);
			List<Long> shopsAlreadySentTo = new ArrayList<Long>();
			for(int ii=0; ii<shopsUsed.size(); ++ii){
				shopsAlreadySentTo.add(shopsUsed.get(ii).getShopId());
			}
			List<Shop> availableShops = shopDAO.getAvailableTFSIShops(dayOfTheWeek, zip);
			List<Shop> shopsToSendTo = new LinkedList<Shop>();
			
			TFSIUtil util = new TFSIUtil();
			
			Date currentDate = new Date();
			String currentDateString = util.getDate(currentDate);
			Date deliveryDate = null;
			try{
				deliveryDate = util.stringDateToDateDate2(moob.getDeliveryDate());
			}catch(Exception ee){
				deliveryDate = util.stringDateToDateDate(moob.getDeliveryDate());
			}
			String deliveryDateString = util.getDate(deliveryDate);
			
			int eq = deliveryDateString.compareTo(currentDateString);
			
			if(eq<0) //if it is after the delivery date, send to TLO
				return receivingShop;
			
			if (availableShops == null){
				logger.debug("No shops found for order : " + moob.getOrderNumber());
			}
			else{
				logger.debug("Number of shops found: " + availableShops.size());
				if (originalSendingShopCode.equals(BOMConstants.FROM_YOU_FLOWERS) || originalSendingShopCode.equals(BOMConstants.BLOOMS_TODAY)){
					for(Shop shop : availableShops){
						if(shop.getShopId() != null && !shopsAlreadySentTo.contains(shop.getShopId())){
							boolean BMT = false;
							if (shopDAO.getBloomNetShopnetwork(shop.getShopId()) != null) BMT = true;
							if(!BMT) shopsToSendTo.add(shop);//Only send to shops that are strictly TF
						}
					}
				}else{
					for(Shop shop : availableShops){
						if(shop.getShopId() != null && !shopsAlreadySentTo.contains(shop.getShopId())){
							shopsToSendTo.add(shop);
						}
					}
				}
			}
	
			if(shopsToSendTo.size() > 0){
				int totalShops = shopsToSendTo.size()-1;
				int randomNum = Integer.valueOf(String.valueOf(Math.round(Math.random() * totalShops)));
				Shop randomShop = shopsToSendTo.get(randomNum);
				receivingShop = randomShop;
				logger.debug("Sending order number "+moob.getOrderNumber()+" to shop ID: "+randomShop.getShopId());
			}
		}catch(Exception ee){
			return null;
		}
		return receivingShop;
	}
	private boolean checkForActiveFullingShop(String orderNumber) {
		
		boolean activeShop = false;
		String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderNumber);

		if (!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
			Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
			byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
			if ((Byte.toString(childOrderStatus).equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
					||(Byte.toString(childOrderStatus).equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))){
				
				return false;
			}
			else{
				logger.debug("order " +orderNumber + " already has a fulfilling shop " + currentFulfillingShop);
				return true;
			}
	

		}
		
		return activeShop;
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void rejectOrder(MessageOnOrderBean moob, MessageOrder order, String originalSendingShopCode, String xml) throws Exception {
		
		String messageText = "BloomNet: Unable to fulfill order ";
		String messageType = BOMConstants.RJCT_MESSAGE_TYPE;
		String orderNumber = order.getOrderDetails().getOrderNumber();
		if(orderNumber == null) orderNumber = order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();

		Bomorder bomOrder = orderDAO.getOrderByOrderNumber(orderNumber);
		String[] originalGeneralIdentifiers = tfsiService.getOriginalVars(bomOrder);
		
		Identifiers identifiers = new Identifiers();
		GeneralIdentifiers generalIdentifiers = new GeneralIdentifiers();
		
		generalIdentifiers.setExternalShopMessageNumber(originalGeneralIdentifiers[0]);
		generalIdentifiers.setExternalShopOrderNumber(originalGeneralIdentifiers[1]);
		generalIdentifiers.setInwireSequenceNo(originalGeneralIdentifiers[2]);
		generalIdentifiers.setBmtSeqNumberOfMessage(originalGeneralIdentifiers[3]);
		generalIdentifiers.setBmtSeqNumberOfOrder(originalGeneralIdentifiers[4]);
		generalIdentifiers.setBmtOrderNumber(originalGeneralIdentifiers[5]);
		
		identifiers.setGeneralIdentifiers(generalIdentifiers);

		MessageRjct messageRjct = new MessageRjct();
		messageRjct.setMessageText(messageText);
		messageRjct.setReceivingShopCode(originalSendingShopCode);
		messageRjct.setFulfillingShopCode(fsiProperties.getProperty("fsi.shopcode"));
		messageRjct.setSendingShopCode(fsiProperties.getProperty("fsi.shopcode"));
		messageRjct.setMessageType(messageType);
		messageRjct.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
		messageRjct.setSystemType(BOMConstants.SYSTEM_TYPE);
		messageRjct.setIdentifiers(identifiers);
		
		String fsiXML = transformService.createForeignSystemInterface(BOMConstants.RJCT_MESSAGE_DESC, messageRjct);
		
		messageDAO.persistMessageAndActivity(
				userDAO.getUserByUserName(tfsiProperties.getProperty("userTFSI")).getUserId(), 
				generalIdentifiers, 
				xml, 
				BOMConstants.RJCT_MESSAGE_DESC, 
				bomProperties.getProperty("mesage.sentshop"), 
				BOMConstants.COMMMETHOD_API, 
				fsiProperties.getProperty("fsi.shopcode"), 
				originalSendingShopCode, 
				messageText);

		tfsiService.sendMessageToBloomlink(fsiXML,messageType);
		
		orderDAO.persistOrderactivityByUser(
				tfsiProperties.getProperty("userTFSI"),
				BOMConstants.ACT_ROUTING, 
				bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT), 
				bomOrder.getOrderNumber(), 
				BOMConstants.TFSI_QUEUE);
		
		
		long childId = 0;
		String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber());
		if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
			childId = orderDAO.getCurrentFulfillingOrder(bomOrder.getOrderNumber()).getBomorderId();
		}
		
		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TFSI_QUEUE );
		
		BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(bomOrder.getBomorderId());
		bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
		bomorderSts.setStsRoutingId(Byte.parseByte(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)));
		bomorderSts.setVirtualqueueId(virtualqueueId);
		
		orderDAO.updateBomorderSts(bomorderSts, childId);
		return;
	}
}
