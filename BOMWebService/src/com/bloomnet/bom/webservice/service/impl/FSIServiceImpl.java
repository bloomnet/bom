package com.bloomnet.bom.webservice.service.impl;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionTemplate;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActAudit;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.BomorderviewV2Id;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.Shopnetworkcoverage;
import com.bloomnet.bom.common.entity.Shopnetworkstatus;
import com.bloomnet.bom.common.entity.SpecialQueues;
import com.bloomnet.bom.common.entity.StsMessage;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.webservice.client.FSIClient;
import com.bloomnet.bom.webservice.exceptions.BloomLinkException;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.Error;
import com.bloomnet.bom.common.jaxb.fsi.Errors;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckb;
import com.bloomnet.bom.common.jaxb.fsi.MessageAckf;
import com.bloomnet.bom.common.jaxb.fsi.MessageCanc;
import com.bloomnet.bom.common.jaxb.fsi.MessageConf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDeni;
import com.bloomnet.bom.common.jaxb.fsi.MessageDisp;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlca;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlcf;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlou;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspc;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspd;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspr;
import com.bloomnet.bom.common.jaxb.fsi.MessageInfo;
import com.bloomnet.bom.common.jaxb.fsi.MessageInqr;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessagePchg;
import com.bloomnet.bom.common.jaxb.fsi.MessageResp;
import com.bloomnet.bom.common.jaxb.fsi.MessageRjct;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.fsi.Recipient;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectoryInterface;
import com.bloomnet.bom.common.jaxb.mdi.MemberDirectorySearchOptions;
import com.bloomnet.bom.common.jaxb.mdi.SearchByShopCode;
import com.bloomnet.bom.common.jaxb.mdi.SearchShopRequest;
import com.bloomnet.bom.common.jaxb.mdi.Security;
import com.bloomnet.bom.common.jaxb.mdi.ServicedZips;
import com.bloomnet.bom.common.jaxb.mdi.ShopAddress;
import com.bloomnet.bom.common.jaxb.fsi.MessageDspu;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.MessageTypeUtil;
import com.bloomnet.bom.common.util.SendGridEmailer;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.webservice.service.FSIService;
import com.bloomnet.bom.webservice.utils.SendStatusUpdates;

@Service("fsiService")
public class FSIServiceImpl implements FSIService {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(FSIServiceImpl.class);

	// Injected property
	@Autowired
	private Properties bomProperties;

	// Injected property
	@Autowired
	private Properties fsiProperties;

	// Injected dependency
	@Autowired
	private MessageProducer messageProducer;

	// Injected dependency
	@Autowired
	private FSIClient fsiClient;

	// Injected DAO implementation
	@Autowired
	private MessageDAO messageDAO;

	// Injected DAO implementation
	@Autowired
	private OrderDAO orderDAO;

	// Injected DAO implementation
	@Autowired
	private ShopDAO shopDAO;

	// Injected DAO implementation
	@Autowired
	private UserDAO userDAO;

	@Autowired
	private BomorderviewV2DAO bomorderviewV2DAO;

	@Autowired
	private TransformService transformService;

	@Autowired
	private TransactionTemplate transactionTemplate;

	@Autowired
	private SendStatusUpdates sendStatusUpdates;

	static final String INTERNAL_USER = "FSI";
	static final String TFSI_USER = "TFSI";

	/***
	 * Sends message to BloomLink
	 * 
	 * @param messageXml
	 * @throws Exception
	 */
	public void sendMessageToBloomlink(String orderNumber, String messageXml, String messageType, String commmethod,
			String sendingShopCode, String receivingShopCode, String messageText) throws Exception {

		String dataEncoded = "";
		String post = "";
		String response = "";

		try {

			System.out.println("message: " + messageXml);

			dataEncoded = URLEncoder.encode(messageXml, "UTF-8");
			post = fsiProperties.getProperty("fsi.endpoint") + BOMConstants.POST_MESSAGE + dataEncoded;

			if (logger.isDebugEnabled()) {
				logger.debug("xml encoded: " + dataEncoded);
				logger.debug("sendMessage xml " + messageXml);
				logger.debug("post " + post);

			}
			byte status = 3;

			response = fsiClient.sendRequest(post, messageType);

			ForeignSystemInterface foreignSystemInterface = transformService.convertToJavaFSI(response);
			orderNumber = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0).getIdentifiers()
					.getGeneralIdentifiers().getBmtOrderNumber();
			String seqNumberOfOrder = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0)
					.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfOrder();
			String seqNumberOfMessage = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0)
					.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();

			logger.debug("Sending " + messageType + " message to BloomLink");
			logger.debug("Order Number: " + orderNumber);
			logger.debug("seqNumberOfOrder: " + seqNumberOfOrder);
			logger.debug("seqNumberOfMessage: " + seqNumberOfMessage);

			int numOfErrors = 0;
			numOfErrors = foreignSystemInterface.getErrors().getError().size();

			if (logger.isDebugEnabled()) {
				logger.debug("numOfErrors " + numOfErrors);
			}

			if (numOfErrors > 0) {
				List<Error> errors = foreignSystemInterface.getErrors().getError();

				for (Error error : errors) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(
							error.getErrorCode() + "|" + error.getDetailedErrorCode() + "|" + error.getErrorMessage());
					// throw new Exception("BloomLink Exception: "
					// +error.getErrorMessage());
					logger.error(error.getErrorMessage());

				}
			}

			Errors messageAckBErrors = foreignSystemInterface.getMessagesOnOrder().get(0).getMessageAckb().get(0)
					.getErrors();

			if (!messageAckBErrors.getError().isEmpty()) {

				List<Error> messageAckBErrorList = messageAckBErrors.getError();

				for (Error messageAckBError : messageAckBErrorList) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(messageAckBError.getErrorCode() + "|" + messageAckBError.getDetailedErrorCode() + "|"
							+ messageAckBError.getErrorMessage());
					logger.error(messageAckBError.getErrorMessage());
					throw new Exception("BloomLink Exception: " + messageAckBError.getErrorMessage());

				}
			}

			if (!orderNumber.equals("ERRORMSG") || !messageType.equals("ORDER")) {
				saveMessage(orderNumber, messageXml, messageType, commmethod, sendingShopCode, receivingShopCode,
						status, messageText, seqNumberOfMessage);
			}

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (BloomLinkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * com.bloomnet.bom.webservice.service.impl.FSIService#processMessage(java.
	 * lang.String)
	 */
	@Override
	public synchronized void processMessage(final String msg) throws Exception {

		logger.debug("*****inbound message: " + msg + "*****");
		logger.debug("**********Start Time: " + new Date() + "*****");

		// transform initial chunk of messages into fsi object
		ForeignSystemInterface foreignSystemInterface = transformService.convertToJavaFSI(msg);

		int numOfOrders = 0;
		int numOfErrors = 0;

		numOfOrders = foreignSystemInterface.getMessagesOnOrder().size();

		if (logger.isDebugEnabled()) {
			logger.debug("\nnumOfOrders " + numOfOrders);

		}

		if (numOfErrors > 0) {
			List<Error> errors = foreignSystemInterface.getErrors().getError();

			for (Error error : foreignSystemInterface.getErrors().getError()) {
				logger.error(error.getErrorCode() + "|" + error.getDetailedErrorCode() + "|" + error.getErrorMessage());
			}
		}

		List<MessagesOnOrder> messages = foreignSystemInterface.getMessagesOnOrder();

		for (MessagesOnOrder orderList : messages) {

			traverseMessages(orderList);
		}

	}

	/**
	 * Iterate through list of messagesOnOrder determine destination, sends
	 * messageOnOrder to destination set status of messageOnORder persist
	 * messageOnOrder, Send read msg ack to BloomLink
	 * 
	 * @param orderList
	 * @throws Exception
	 */
	private void traverseMessages(MessagesOnOrder orderList) throws Exception {

		if (logger.isDebugEnabled()) {
			logger.debug(orderList.getMessageCount());
		}

		List<MessageAckb> messageackbList = orderList.getMessageAckb();
		List<MessageAckf> messageackfList = orderList.getMessageAckf();
		List<MessageCanc> messageCancList = orderList.getMessageCanc();
		List<MessageConf> messageConfList = orderList.getMessageConf();
		List<MessageDeni> messageDeniList = orderList.getMessageDeni();
		List<MessageDlca> messageDlcaList = orderList.getMessageDlca();
		List<MessageDisp> messageDispList = orderList.getMessageDisp();
		List<MessageDspc> messageDspcList = orderList.getMessageDspc();
		List<MessageDspd> messageDspdList = orderList.getMessageDspd();
		List<MessageDspr> messageDsprList = orderList.getMessageDspr();
		List<MessageDspu> messageDspuList = orderList.getMessageDspu();
		List<MessageDlcf> messageDlcfList = orderList.getMessageDlcf();
		List<MessageDlou> messageDlouList = orderList.getMessageDlou();
		List<MessageInfo> messageInfoList = orderList.getMessageInfo();
		List<MessageInqr> messageInqrList = orderList.getMessageInqr();
		MessageOrder messageOrder = orderList.getMessageOrder();
		List<MessagePchg> messagePchgList = orderList.getMessagePchg();
		List<MessageResp> messageRespList = orderList.getMessageResp();
		List<MessageRjct> messageRjctList = orderList.getMessageRjct();

		if (!messageackbList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Proccessing Messageackb**********");
			}
		}
		if (!messageackfList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing Messageackf**********");
			}
			handleMessageAckf(messageackfList);

		}
		if (!messageCancList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageCanc**********");
			}
			handleMessageCanc(messageCancList);
		}
		if (!messageConfList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageConf**********");
			}
			handleMessageConf(messageConfList);
		}
		if (!messageDeniList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageDeni**********");
			}
			handleMessageDeni(messageDeniList);
		}
		if (!messageDlcaList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageaDlca**********");
			}
			handleMessageDlca(messageDlcaList);
		}
		if (!messageDispList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageaDisp**********");
			}
			handleMessageDisp(messageDispList);
		}
		if (!messageDspcList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageaDspc**********");
			}
			handleMessageDspc(messageDspcList);
		}
		if (!messageDspdList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageaDspd**********");
			}
			handleMessageDspd(messageDspdList);
		}
		if (!messageDsprList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageaDspr**********");
			}
			handleMessageDspr(messageDsprList);
		}
		if (!messageDspuList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageaDspu**********");
			}
			handleMessageDspu(messageDspuList);
		}
		if (!messageDlcfList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageDlcf**********");
			}
			handleMessageDlcf(messageDlcfList);
		}
		if (!messageDlouList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageDlou**********");
			}
			handleMessageDlou(messageDlouList);
		}
		if (!messageInfoList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageInfo**********");
			}
			handleMessageInfo(messageInfoList);
		}
		if (!messageInqrList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageInqr**********");
			}
			handleMessageInqr(messageInqrList);
		}
		if (messageOrder != null) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageOrder**********");
			}
			handleMessageOrder(messageOrder);
		}
		if (!messagePchgList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessagePchg**********");
			}
			handleMessagePchg(messagePchgList);
		}
		if (!messageRespList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageResp**********");
			}
			handleMessageResp(messageRespList);
		}
		if (!messageRjctList.isEmpty()) {
			if (logger.isDebugEnabled()) {
				logger.debug("**********Processing MessageRjct**********");
			}
			handleMessageRjct(messageRjctList);
		}
	}

	private void handleMessageAckf(final List<MessageAckf> messageackfList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					logger.debug("handleMessageAckf");

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";

					int numOfMessages = 0;
					numOfMessages = messageackfList.size();

					MessageAckf ackf = messageackfList.get(0);
					orderNumber = ackf.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
					String sendingShopCode = ackf.getSendingShopCode();
					String receivingShopCode = ackf.getReceivingShopCode();
					String fulfillingShopCode = ackf.getFulfillingShopCode();
					String messageText = "ACKF";
					String status = bomProperties.getProperty(BOMConstants.ORDER_ACCEPTED_BY_SHOP);

					String communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
					if (communicationMethod != null) {
						Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
						ForeignSystemInterface parentFSI = transformService.convertToJavaFSI(parentOrder.getOrderXml());
						MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
						String OrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
								.getBmtOrderNumber();
						String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfOrder();
						String bmtSeqNumberOfMessage = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();
						Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
						byte childOrderType = childOrder.getOrderType();

						String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);
						String currentFulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(orderNumber);
						byte orderType = parentOrder.getOrderType();

						fsiXml = transformService.createForeignSystemInterface(BOMConstants.ACKF_MESSAGE_DESC, ackf);

						if (currentFulfillingShopCode.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
							logger.debug(
									"Fulfulling shop is still: " + fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
											+ "sending message to " + destination);

						}

						// BMT Message
						// send message to corresponding shop
						if ((communicationMethod.equals(BOMConstants.COMMMETHOD_API)) && (childOrderType == 1)) {
							logger.debug("communication method: " + BOMConstants.COMMMETHOD_API + " childorderType: "
									+ childOrderType);
							if (sendingShopCode.equals(fulfillingShopCode)) {
								fsiXml = transformService.createOutboundMessage(BOMConstants.ACKF_MESSAGE_DESC, ackf,
										originalSendingShopCode, currentFulfillingShopCode, OrderNumber,
										bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
							} // message from original sending shop
							else if (receivingShopCode.equals(fulfillingShopCode)) {
								bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
								OrderNumber = childOrder.getOrderNumber();
								fsiXml = transformService.createOutboundMessage(BOMConstants.ACKF_MESSAGE_DESC, ackf,
										originalSendingShopCode, currentFulfillingShopCode, OrderNumber,
										bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
							}

							ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
							MessageAckf outBoundAckf = fsi.getMessagesOnOrder().get(0).getMessageAckf().get(0);
							sendingShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
							sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.ACKF_MESSAGE_DESC,
									communicationMethod, outBoundAckf.getSendingShopCode(),
									outBoundAckf.getReceivingShopCode(), messageText);

							orderDAO.persistChildOrderAndActivity(childOrder, OrderNumber, INTERNAL_USER,
									BOMConstants.ACT_ROUTING, status, BOMConstants.FSI_QUEUE);

							MessageInfo messageInfo = sendStatusUpdates.sendSentToShop(parentOrder,
									childOrder.getShopByReceivingShopId());
							String messageXml = transformService
									.createForeignSystemInterface(BOMConstants.INFO_MESSAGE_DESC, messageInfo);
							sendMessageToBloomlink(orderNumber, messageXml, BOMConstants.INFO_MESSAGE_DESC,
									communicationMethod, outBoundAckf.getSendingShopCode(),
									outBoundAckf.getReceivingShopCode(), messageInfo.getMessageText());

						}
						// TEL Message
						else if ((communicationMethod.equals(BOMConstants.COMMMETHOD_API))
								&& (childOrderType == BOMConstants.ORDER_TYPE_TEL)) {
							logger.debug("communication method: " + BOMConstants.COMMMETHOD_API + " childorderType: "
									+ childOrderType);
							fsiXml = transformService.createOutboundMessage(BOMConstants.ACKF_MESSAGE_DESC, ackf,
									originalSendingShopCode, currentFulfillingShopCode, orderNumber,
									bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
							ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
							MessageAckf outBoundAckf = fsi.getMessagesOnOrder().get(0).getMessageAckf().get(0);
							sendingShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
							sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.ACKF_MESSAGE_DESC,
									communicationMethod, outBoundAckf.getSendingShopCode(),
									outBoundAckf.getReceivingShopCode(), messageText);
							// setOrderStatus( parentMessageOrder, destination,
							// status, TFSI_USER );

						}

					} else {

						fsiXml = transformService.createForeignSystemInterface(BOMConstants.ACKF_MESSAGE_DESC, ackf);
						sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
								BOMConstants.ACKF_MESSAGE_DESC, "", communicationMethod, receivingShopCode, messageText,
								" ", " ", " ", " ", " ", " ");

					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});

	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageRjctList
	 * @throws Exception
	 */
	private void handleMessageRjct(final List<MessageRjct> messageRjctList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String messageXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";

					String skillset = BOMConstants.SKILLSET_REJECTION;
					String status = bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP);

					int numOfMessages = messageRjctList.size();
					logger.debug("num of rjcts: " + numOfMessages);

					for (MessageRjct rjct : messageRjctList) {

						orderNumber = rjct.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						if (orderDAO.getOrderByOrderNumber(orderNumber) == null) {
							sendAcknowledgement(rjct);
							return null;
						}
						String sendingShopCode = rjct.getSendingShopCode();
						String receivingShopCode = rjct.getReceivingShopCode();
						String messageText = rjct.getMessageText();
						String bmtSeqNumberOfMessage = rjct.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();
						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);

						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							List<BomorderviewV2> bomorderviewv2 = bomorderviewV2DAO
									.getOrderByOrderNumber(parentOrderNumber);
							for (BomorderviewV2 order : bomorderviewv2) {
								if (order != null) {
									BomorderviewV2Id orderId = order.getId();
									String ordernum = orderId.getParentOrderNumber();
									long shopid = orderId.getSendingShopId();

									destination = order.getId().getRoute();
								}

							}

							// update child and parent order to rejected
							orderDAO.persistChildOrderAndActivity(childOrder, parentOrderNumber, INTERNAL_USER,
									BOMConstants.ACT_ROUTING, status, destination);

							// update parent to be worked
							status = bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED);
							orderDAO.persistOrderactivityByUser(INTERNAL_USER, BOMConstants.ACT_ROUTING, status,
									orderNumber, destination);
							long childId = 0;
							String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderNumber);
							if (!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
								childId = orderDAO.getCurrentFulfillingOrder(orderNumber).getBomorderId();
								logger.debug(
										"order rejected for " + parentOrderNumber + ", current child is: " + childId);
							}

							byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
							BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(parentOrder.getBomorderId());
							bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
							bomorderSts.setStsRoutingId(Byte.parseByte(status));
							bomorderSts.setVirtualqueueId(virtualqueueId);

							// orderDAO.updateBomorderSts(bomorderSts, childId);
							orderDAO.updateBomorderSts(bomorderSts, 0);

							// if order rejected send an inbound order to queue
							// as to be worked
							if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									// destination = BOMConstants.FSI_QUEUE;
									String seqNumberOfMessage = parentOrder.getBmtMessageSequenceNumber();

									Zip zipEntity = orderDAO.getZipByCode(zip);
									String timeZone = zipEntity.getTimeZone();
									sendOrderToQueue(destination, parentOrder.getOrderXml(), parentOrderNumber,
											originalSendingShopCode, deliveryDate, city, zip, occasionCode, skillset,
											BOMConstants.RJCT_MESSAGE_DESC, timeZone);

									messageXml = transformService
											.createForeignSystemInterface(BOMConstants.RJCT_MESSAGE_DESC, rjct);

									// XXX update message status here but what
									// to set message status for rejected order
									// being reworked
									byte messageStatus = Byte
											.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
									messageId = saveMessage(orderNumber, messageXml, BOMConstants.RJCT_MESSAGE_DESC,
											communicationMethod, sendingShopCode, receivingShopCode, messageStatus,
											messageText, seqNumberOfMessage);
									updateMessage(orderNumber, messageId);

								}

							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								messageXml = transformService
										.createForeignSystemInterface(BOMConstants.RJCT_MESSAGE_DESC, rjct);
								destination = BOMConstants.TLO_QUEUE;
								sendMessageToDestination(destination, messageXml, orderNumber, sendingShopCode,
										BOMConstants.RJCT_MESSAGE_DESC, skillset, communicationMethod,
										receivingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city, zip,
										occasionCode, Byte.toString(orderType));

							} else {// New order does not have communication
									// method yet,
								// send message back to queue as need to be work
								messageXml = transformService
										.createForeignSystemInterface(BOMConstants.RJCT_MESSAGE_DESC, rjct);
								sendMessageToDestination(destination, messageXml, orderNumber, sendingShopCode,
										BOMConstants.RJCT_MESSAGE_DESC, skillset, communicationMethod,
										receivingShopCode, messageText, bmtSeqNumberOfOrder, deliveryDate, city, zip,
										occasionCode, Byte.toString(orderType));
							}

							// send ack
							sendAcknowledgement(rjct);
						} else {

							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageXml = transformService.createForeignSystemInterface(BOMConstants.RJCT_MESSAGE_DESC,
									rjct);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, messageXml, orderNumber,
									sendingShopCode, BOMConstants.RJCT_MESSAGE_DESC, skillset, communicationMethod,
									receivingShopCode, messageText,
									rjct.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(rjct);
						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});

	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageRespList
	 * @throws Exception
	 */
	private void handleMessageResp(final List<MessageResp> messageRespList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					byte status = 1;
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_RESPONSE;

					int numOfMessages = messageRespList.size();
					logger.debug("num of resps: " + numOfMessages);

					for (MessageResp resp : messageRespList) {

						orderNumber = resp.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = resp.getSendingShopCode();
						String recievingShopCode = resp.getReceivingShopCode();
						String messageText = resp.getMessageText();
						String fulfillingShopCode = resp.getFulfillingShopCode();
						String bmtSeqNumberOfMessage = resp.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);
							String currentFulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(orderNumber);
							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String OrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.RESP_MESSAGE_DESC,
									resp);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.RESP_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// order has not been fulfilled yet
							// send order TLO queue
							if (currentFulfillingShopCode
									.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
								// if order is in tlo queue send message there
								// too
								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();

									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										logger.debug("Fulfulling shop is still: "
												+ fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
												+ "sending message to " + destination);
										bmtSeqNumberOfMessage = resp.getIdentifiers().getGeneralIdentifiers()
												.getBmtSeqNumberOfMessage();
										// sendMessageToDestination(destination,
										// fsiXml, orderNumber,
										// sendingShopCode,BOMConstants.RESP_MESSAGE_DESC,skillset,communicationMethod,
										// recievingShopCode,messageText,bmtSeqNumberOfOrder,deliveryDate,
										// city, zip, occasionCode,
										// Byte.toString(orderType));

										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											//
											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
										}
										//
									}
								} else {
									return null;
								}
							}

							// if current child order is rejected or confirmed
							// cancelled
							// send message to queue
							else if ((Byte.toString(childOrderStatus)
									.equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))) {

								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("inqr actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								logger.debug("inqr actRoutingList: " + actRoutingList.get(0).getStatus()
										+ "ordernumber: " + OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									logger.debug("inqr virtualQueue: " + virtualQueue + "ordernumber: " + OrderNumber);
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
									}
								}

							}
							// messages for website orders are routed to TLO
							else if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.RESP_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
							}
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									// message from fulfilling shop
									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.RESP_MESSAGE_DESC,
												resp, originalSendingShopCode, fulfillingShopCode, OrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									} // message from original sending shop
									else if (recievingShopCode.equals(fulfillingShopCode)) {
										bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
										OrderNumber = childOrder.getOrderNumber();
										fsiXml = transformService.createOutboundMessage(BOMConstants.RESP_MESSAGE_DESC,
												resp, originalSendingShopCode, currentFulfillingShopCode, OrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageResp outBoundResp = fsi.getMessagesOnOrder().get(0).getMessageResp().get(0);
									sendingShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.RESP_MESSAGE_DESC,
											communicationMethod, outBoundResp.getSendingShopCode(),
											outBoundResp.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

									// TEL Message
								} else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = resp.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.RESP_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));

								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber,
								// sendingShopCode,BOMConstants.RESP_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
								if (!isLocked) {
									String statusstr = bomProperties
											.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);
									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//
							}
							sendAcknowledgement(resp);
						} else {
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.RESP_MESSAGE_DESC,
									resp);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.RESP_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									resp.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(resp);
						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});

	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messagePchgList
	 * @throws Exception
	 */
	private void handleMessagePchg(final List<MessagePchg> messagePchgList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					byte status = 1;
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messagePchgList.size();
					logger.debug("num of pchgs: " + numOfMessages);

					for (MessagePchg pchg : messagePchgList) {

						orderNumber = pchg.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = pchg.getSendingShopCode();
						String recievingShopCode = pchg.getReceivingShopCode();
						String fulfillingShopCode = pchg.getFulfillingShopCode();
						String messageText = pchg.getMessageText();
						String price = pchg.getPrice();
						String bmtSeqNumberOfMessage = pchg.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						if (price.isEmpty()) {
							logger.error("no pchg for order " + orderNumber);
							sendAcknowledgement(pchg);
							return null;
						}

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);

						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							String oldPrice = Double.toString(parentOrder.getPrice());
							String oldXml = parentOrder.getOrderXml();

							String currentFulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(orderNumber);

							String parentOrderXml = parentOrder.getOrderXml();
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String OrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							// update price in order xml
							OrderDetails orderDetails = parentMessageOrder.getOrderDetails();
							orderDetails.setTotalCostOfMerchandise(price);
							parentMessageOrder.setOrderDetails(orderDetails);
							String updatedXml = transformService.createOutboundMessage(BOMConstants.ORDER_MESSAGE_DESC,
									parentMessageOrder, sendingShopCode, recievingShopCode, orderNumber,
									bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
							parentOrder.setOrderXml(updatedXml);
							parentOrder.setPrice(Double.parseDouble(price));

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.PCHG_MESSAGE_DESC,
									pchg);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.PCHG_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// if current fulfilling shops equals
							// BOMConstants.FSI_SHOP_CODE
							// order has not been fulfilled yet
							// and order is on TLO queue
							// send to TLO queue
							if (currentFulfillingShopCode
									.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
								// if order is in tlo queue send message there
								// too
								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);

								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();

									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										logger.debug("Fulfulling shop is still: "
												+ fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
												+ "sending message to " + destination);
										// sendMessageToDestination(destination,
										// fsiXml, orderNumber,
										// sendingShopCode,BOMConstants.PCHG_MESSAGE_DESC,skillset,communicationMethod,
										// recievingShopCode,messageText,bmtSeqNumberOfOrder,deliveryDate,
										// city, zip, occasionCode,
										// Byte.toString(orderType));
										String statusstr = bomProperties
												.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											//
											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
										}
										//
									}
								} else {
									logger.debug(
											"orderactivity has not been entered for order: " + OrderNumber + " yet");
									updateOrderPrice(orderNumber, oldPrice, oldXml, parentOrder);
									updateMessage(orderNumber, messageId);

									return null;
								}
							}

							// if current child order is rejected or confirmed
							// cancelled
							// send message to queue
							else if ((Byte.toString(childOrderStatus)
									.equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))) {

								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("inqr actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								logger.debug("inqr actRoutingList: " + actRoutingList.get(0).getStatus()
										+ "ordernumber: " + OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									logger.debug("inqr virtualQueue: " + virtualQueue + "ordernumber: " + OrderNumber);
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
									}
								}

							}

							// BMT Message
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (recievingShopCode.equals(fulfillingShopCode)) {
										bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
										OrderNumber = childOrder.getOrderNumber();
										fsiXml = transformService.createOutboundMessage(BOMConstants.PCHG_MESSAGE_DESC,
												pchg, fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE),
												currentFulfillingShopCode, OrderNumber, bmtSeqNumberOfOrder,
												bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessagePchg outBoundPchg = fsi.getMessagesOnOrder().get(0).getMessagePchg().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.PCHG_MESSAGE_DESC,
											communicationMethod, outBoundPchg.getSendingShopCode(),
											outBoundPchg.getReceivingShopCode(), messageText);
									updateOrderPrice(orderNumber, oldPrice, oldXml, parentOrder);
									updateMessage(orderNumber, messageId);

									// TEL Message
								} else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = pchg.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.PCHG_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
									updateOrderPrice(orderNumber, oldPrice, oldXml, parentOrder);
									updateMessage(orderNumber, messageId);

								}
								// TLO Message
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								// bmtSeqNumberOfMessage =
								// pchg.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber,
								// sendingShopCode,BOMConstants.PCHG_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
								if (!isLocked) {
									String statusstr = bomProperties
											.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//
							}
							sendAcknowledgement(pchg);
						} else {
							logger.debug("can not process pchg");
							// New order does not have communication method yet
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.PCHG_MESSAGE_DESC,
									pchg);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.PCHG_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									pchg.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(pchg);
						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageOrder
	 * @throws JMSException
	 * @throws Exception
	 */
	private void handleMessageOrder(final MessageOrder messageOrder1) throws JMSException {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					MessageOrder messageOrder = new MessageOrder();
					messageOrder = messageOrder1;
					String fsi = "";
					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;
					String orderNumber = messageOrder.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
					String sendingShopCode = messageOrder.getSendingShopCode();
					String recievingShopCode = messageOrder.getReceivingShopCode();
					String destination = determineRoute(sendingShopCode,
							messageOrder.getDeliveryDetails().getDeliveryDate(), orderNumber);
					String bmtSeqNumberOfOrder = messageOrder.getIdentifiers().getGeneralIdentifiers()
							.getBmtSeqNumberOfOrder();
					String bmtSeqNumberOfMessage = messageOrder.getIdentifiers().getGeneralIdentifiers()
							.getBmtSeqNumberOfMessage();
					String countryCode = messageOrder.getRecipient().getRecipientCountryCode().trim();
					String state = messageOrder.getRecipient().getRecipientState().trim();
					// boolean isStateValid = orderDAO.isStateValid(state);

					boolean isCountryValid = // orderDAO.isCountryValid(countryCode);
							true; // accept all orders now, if it's a country we
									// have not seen, it will be saved in the
									// database

					/*
					 * if (countryCode.equals("USA") ||countryCode.equals("US")
					 * ||countryCode.equals("CA") ||countryCode.equals("CAN")){
					 */
					// if (isCountryValid){
					if ((isCountryValid) || countryCode.equals("US")) {

						try {

							final String deliveryDate = messageOrder.getDeliveryDetails().getDeliveryDate();
							final String occasionCode = messageOrder.getOrderDetails().getOccasionCode();

							String city = messageOrder.getRecipient().getRecipientCity();
							String zip = messageOrder.getRecipient().getRecipientZipCode().replaceAll("\\s+", "");

							Zip zipEntity = verifyZip(messageOrder, countryCode, zip, orderNumber, city, state);

							String cityName = messageOrder.getRecipient().getRecipientCity();
							String stateName = messageOrder.getRecipient().getRecipientState();

							// verify city
							City cityEntity = orderDAO.getCityByNameAndState(cityName, stateName);
							if (cityEntity == null || cityEntity.getCityId() == null) {
								logger.debug("Order Number: " + messageOrder.getOrderDetails().getOrderNumber() + ", "
										+ cityName + " not found in " + stateName);
								cityEntity = orderDAO.getCityByZip(messageOrder.getRecipient().getRecipientZipCode(),
										messageOrder.getRecipient().getRecipientCity());

								if (cityEntity == null || cityEntity.getCityId() == null) {
									logger.debug("Order Number: " + messageOrder.getOrderDetails().getOrderNumber()
											+ ", " + cityName + " not found in "
											+ messageOrder.getRecipient().getRecipientZipCode());
									if (countryCode.equals("USA") || countryCode.equals("US")
											|| countryCode.equals("CA") || countryCode.equals("CAN")
											|| countryCode.equals("PR") || countryCode.equals("PUR")) {
										cityEntity = orderDAO.persistCity(cityName, stateName, countryCode);
									} else {
										cityEntity = orderDAO.persistIntCity(cityName, stateName, countryCode);
										orderDAO.persistCityCoverage(cityEntity);
									}
								}
								Recipient recipient = messageOrder.getRecipient();
								recipient.setRecipientCity(cityEntity.getName());
								messageOrder.setRecipient(recipient);
								city = cityEntity.getName();
							}

							// compare fsi with original xml
							// comment out for performance
							// String originalXml =
							// transformService.createForeignSystemInterface(BOMConstants.ORDER_MESSAGE_DESC,
							// messageOrder);
							fsi = transformService.createOutboundMessage(BOMConstants.ORDER_MESSAGE_DESC, messageOrder,
									sendingShopCode, recievingShopCode, orderNumber, bmtSeqNumberOfOrder,
									bmtSeqNumberOfMessage).replaceAll("%", "%25");

							final String status = bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED);

							boolean isNewOrder = orderDAO
									.isUniqueOrderNumber(messageOrder.getOrderDetails().getOrderNumber());
							// boolean isNewSequence =
							// orderDAO.isUniqueSeqNumberOrder(bmtSeqNumberOfOrder);

							// if ((isNewOrder)&&(isNewSequence)){
							if (isNewOrder) {

								saveOrder(bmtSeqNumberOfOrder, bmtSeqNumberOfMessage, fsi, messageOrder,
										communicationMethod, destination, status);
								String timeZone = zipEntity.getTimeZone();
								if (destination.equals(BOMConstants.TLO_QUEUE)) {
									setOrderStatus(messageOrder, destination, status, INTERNAL_USER);
								} else {
									sendOrderToQueue(destination, fsi, orderNumber, sendingShopCode, deliveryDate, city,
											zip, occasionCode, skillset, BOMConstants.ORDER_MESSAGE_DESC, timeZone);
								}

							} else {
								logger.error("Duplicate order: " + orderNumber + " sequence: " + bmtSeqNumberOfOrder);
							}

							sendAcknowledgement(messageOrder);
						} catch (HibernateException e) {
							// TODO Auto-generated catch block
							logger.error("unable to process order: " + orderNumber + " because " + e.getMessage());
							e.printStackTrace();
							throw new JMSException("Unable to send order: " + orderNumber + " to destination : "
									+ destination + "\n Received exception: " + e.getMessage());
						} catch (Exception e) {
							// TODO Auto-generated catch block
							logger.error("unable to process order: " + orderNumber + " because " + e.getMessage());
							e.printStackTrace();
							throw new JMSException("Unable to send order: " + orderNumber + " to destination : "
									+ destination + "\n Received exception: " + e.getMessage());
						}
					} else {
						try {
							logger.error("Order Number: " + orderNumber + " is an international order from "
									+ countryCode + " , " + state + " that needs to be worked outside of BOM");
							new SendGridEmailer("Order Number: " + orderNumber + " is an international order from "
									+ countryCode + " , " + state + " that needs to be worked outside of BOM");
							sendAcknowledgement(messageOrder);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}

					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageInqrList
	 * @throws Exception
	 */
	private void handleMessageInqr(final List<MessageInqr> messageInqrList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					byte status = 1;
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_INQUIRE;

					int numOfMessages = messageInqrList.size();
					logger.debug("num of inqrs: " + numOfMessages);

					for (MessageInqr inqr : messageInqrList) {

						orderNumber = inqr.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = inqr.getSendingShopCode();
						String recievingShopCode = inqr.getReceivingShopCode();
						String fulfillingShopCode = inqr.getFulfillingShopCode();
						String messageText = inqr.getMessageText();
						String bmtSeqNumberOfMessage = inqr.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						logger.debug("communication method for order number: " + orderNumber + " is : "
								+ communicationMethod);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);
							String currentFulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(orderNumber);
							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String OrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.INQR_MESSAGE_DESC,
									inqr);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.INQR_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// if current fulfilling shops equals
							// BOMConstants.FSI_SHOP_CODE
							// order has not been fulfilled yet
							// and order is on TLO queue
							// send to TLO queue
							logger.debug("child order: " + childOrder.getOrderNumber());
							logger.debug("current fulfilling shop: " + currentFulfillingShopCode + " for order number: "
									+ orderNumber);
							logger.debug("child order type:" + childOrderType);
							logger.debug("parent order type: " + orderType);

							if (currentFulfillingShopCode
									.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
								// if order is in tlo queue send message there
								// too
								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("inqr actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								logger.debug("inqr actRoutingList: " + actRoutingList.get(0).getStatus()
										+ "ordernumber: " + OrderNumber);
								if (!actRoutingList.isEmpty()) {

									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									logger.debug("inqr virtualQueue: " + virtualQueue + "ordernumber: " + OrderNumber);
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										logger.debug("Fulfulling shop is still: "
												+ fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
												+ "sending message to " + destination);
										// sendMessageToDestination(destination,
										// fsiXml, orderNumber,
										// sendingShopCode,BOMConstants.INQR_MESSAGE_DESC,skillset,communicationMethod,
										// recievingShopCode,messageText,bmtSeqNumberOfOrder,deliveryDate,
										// city, zip, occasionCode,
										// Byte.toString(orderType));
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											//
											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
											//
										}
									}

								} else {
									return null;
								}
							}

							// if current child order is rejected or confirmed
							// cancelled
							// send message to queue
							else if ((Byte.toString(childOrderStatus)
									.equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))) {

								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("inqr actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								logger.debug("inqr actRoutingList: " + actRoutingList.get(0).getStatus()
										+ "ordernumber: " + OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									logger.debug("inqr virtualQueue: " + virtualQueue + "ordernumber: " + OrderNumber);
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
									}
								}
							}

							// messages for website orders are routed to TLO
							else if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = inqr.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.INQR_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
							}
							// send message to corresponding shop

							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								logger.debug("child order type for order number: " + orderNumber + " is : "
										+ childOrderType);
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.INQR_MESSAGE_DESC,
												inqr, originalSendingShopCode, currentFulfillingShopCode, OrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									} // message from original sending shop
									else if (recievingShopCode.equals(fulfillingShopCode)) {

										bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
										OrderNumber = childOrder.getOrderNumber();
										fsiXml = transformService.createOutboundMessage(BOMConstants.INQR_MESSAGE_DESC,
												inqr, originalSendingShopCode, currentFulfillingShopCode, OrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}

									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageInqr outBoundInqr = fsi.getMessagesOnOrder().get(0).getMessageInqr().get(0);
									sendingShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.INQR_MESSAGE_DESC,
											communicationMethod, outBoundInqr.getSendingShopCode(),
											outBoundInqr.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

									// TEL Message
								} else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = inqr.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.INQR_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));

								}
							}
							// TLO Message
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								// bmtSeqNumberOfMessage =
								// inqr.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber,
								// sendingShopCode,BOMConstants.INQR_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								destination = BOMConstants.TLO_QUEUE;
								boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
								if (!isLocked) {
									String statusstr = bomProperties
											.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);
									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//
							}

							sendAcknowledgement(inqr);
						} else {
							// New message and does not have communication
							// method yet
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.INQR_MESSAGE_DESC,
									inqr);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.INQR_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									inqr.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(inqr);

						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});

	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageInfoList
	 * @throws Exception
	 */
	private void handleMessageInfo(final List<MessageInfo> messageInfoList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					byte status = 1;
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageInfoList.size();
					logger.debug("num of infos: " + numOfMessages);

					for (MessageInfo info : messageInfoList) {

						orderNumber = info.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = info.getSendingShopCode();
						String recievingShopCode = info.getReceivingShopCode();
						String fulfillingShopCode = info.getFulfillingShopCode();
						String messageText = info.getMessageText();
						String bmtSeqNumberOfMessage = info.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);

						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						logger.debug("communication method for order number: " + orderNumber + " is : "
								+ communicationMethod);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);
							String currentFulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(orderNumber);
							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String OrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.INFO_MESSAGE_DESC,
									info);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.INFO_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// if current fulfilling shops equals
							// BOMConstants.FSI_SHOP_CODE
							// order has not been fulfilled yet and is TLO queue
							// send message to TLO queue
							if (currentFulfillingShopCode
									.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
								// if order is in tlo queue send message there
								// too
								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										logger.debug("Fulfulling shop is still: "
												+ fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
												+ "sending message to " + destination);
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											//
											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
										}
										//
										// sendMessageToDestination(destination,
										// fsiXml, orderNumber,
										// sendingShopCode,BOMConstants.INFO_MESSAGE_DESC,skillset,communicationMethod,
										// recievingShopCode,messageText,bmtSeqNumberOfOrder,deliveryDate,
										// city, zip, occasionCode,
										// Byte.toString(orderType));
									}
								} else {
									return null;
								}
							}

							// if current child order is rejected or confirmed
							// cancelled
							// send message to queue
							else if ((Byte.toString(childOrderStatus)
									.equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))) {

								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("inqr actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								logger.debug("inqr actRoutingList: " + actRoutingList.get(0).getStatus()
										+ "ordernumber: " + OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									logger.debug("inqr virtualQueue: " + virtualQueue + "ordernumber: " + OrderNumber);
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
									}
								}

							}
							// messages for website orders are routed to TLO
							else if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = info.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.INFO_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
							}
							// BMT Message
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								logger.debug("child order type for order number: " + orderNumber + " is : "
										+ childOrderType);
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.INFO_MESSAGE_DESC,
												info, originalSendingShopCode, fulfillingShopCode, OrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									} // message from original sending shop
									else if (recievingShopCode.equals(fulfillingShopCode)) {
										bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
										OrderNumber = childOrder.getOrderNumber();
										fsiXml = transformService.createOutboundMessage(BOMConstants.INFO_MESSAGE_DESC,
												info, originalSendingShopCode, currentFulfillingShopCode, OrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageInfo outBoundInfo = fsi.getMessagesOnOrder().get(0).getMessageInfo().get(0);
									sendingShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.INFO_MESSAGE_DESC,
											communicationMethod, outBoundInfo.getSendingShopCode(),
											outBoundInfo.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = info.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.INFO_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							}
							// TLO Message
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = info.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber,
								// sendingShopCode,BOMConstants.INFO_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
								if (!isLocked) {
									String statusstr = bomProperties
											.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);
									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//
							}

							sendAcknowledgement(info);
						} else {
							// New message and does not have communication
							// method yet
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.INFO_MESSAGE_DESC,
									info);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.INFO_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									info.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(info);

						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageDlouList
	 * @throws Exception
	 */
	private void handleMessageDlou(final List<MessageDlou> messageDlouList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;
					String status = bomProperties.getProperty(BOMConstants.ORDER_OUT_FOR_DELIVERY);

					int numOfMessages = messageDlouList.size();
					logger.debug("num of dlous: " + numOfMessages);

					for (MessageDlou dlou : messageDlouList) {

						orderNumber = dlou.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = dlou.getSendingShopCode();
						String recievingShopCode = dlou.getReceivingShopCode();
						String fulfillingShopCode = dlou.getFulfillingShopCode();
						String messageText = dlou.getLoadedDate();
						String bmtSeqNumberOfMessage = dlou.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							if (dlou.getLoadedDate().contains("/")) {
								dlou.setLoadedDate(DateUtil.toDate(new Date()));
							}

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DLOU_MESSAGE_DESC,
									dlou);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DLOU_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// messages for website orders are routed to TLO
							if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = dlou.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.DLOU_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
								setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);
							}
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.DLOU_MESSAGE_DESC,
												dlou, originalSendingShopCode,
												fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDlou outBoundDlou = fsi.getMessagesOnOrder().get(0).getMessageDlou().get(0);
									sendingShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DLOU_MESSAGE_DESC,
											communicationMethod, outBoundDlou.getSendingShopCode(),
											outBoundDlou.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);
									orderDAO.persistChildOrderAndActivity(childOrder, parentOrderNumber, INTERNAL_USER,
											BOMConstants.ACT_ROUTING, status, BOMConstants.FSI_QUEUE);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = dlou.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DLOU_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
									// setOrderStatus(parentMessageOrder,destination,status,TFSI_USER);

								}
							}
							// TLO Message
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = dlou.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber,
								// sendingShopCode,BOMConstants.DLOU_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								boolean isLocked = orderDAO.isOrderLocked(parentOrderNumber);
								if (!isLocked) {
									status = bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);

									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(status));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//

							}

							sendAcknowledgement(dlou);
						} else {
							// New message and does not have communication
							// method yet
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DLOU_MESSAGE_DESC,
									dlou);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DLOU_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									dlou.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(dlou);

						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageDlcfList
	 * @throws Exception
	 */
	private void handleMessageDlcf(final List<MessageDlcf> messageDlcfList) {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;
					String status = bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP);
					int messageId = 0;

					String communicationMethod = "";

					int numOfMessages = messageDlcfList.size();
					logger.debug("num of dlcfs: " + numOfMessages);

					try {
						for (MessageDlcf dlcf : messageDlcfList) {

							orderNumber = dlcf.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
							String sendingShopCode = dlcf.getSendingShopCode();
							String recievingShopCode = dlcf.getReceivingShopCode();
							String fulfillingShopCode = dlcf.getFulfillingShopCode();
							String messageText = dlcf.getDateOrderDelivered();
							String bmtSeqNumberOfMessage = dlcf.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfMessage();

							communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
							boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

							if ((communicationMethod != null) && (isNewMsg)) {

								String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

								Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
								System.out.println("parentOrder: " + parentOrder.getOrderNumber());
								byte orderType = parentOrder.getOrderType();
								Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
								byte childOrderType = childOrder.getOrderType();
								ForeignSystemInterface parentFSI = transformService
										.convertToJavaFSI(parentOrder.getOrderXml());
								MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0)
										.getMessageOrder();
								String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
										.getBmtOrderNumber();
								System.out.println("parentOrderNumber: " + parentOrderNumber);
								String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfOrder();
								String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
								String city = parentMessageOrder.getRecipient().getRecipientCity();
								String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
								String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

								fsiXml = transformService.createForeignSystemInterface(BOMConstants.DLCF_MESSAGE_DESC,
										dlcf);
								byte messageStatus = Byte
										.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
								messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DLCF_MESSAGE_DESC,
										communicationMethod, sendingShopCode, recievingShopCode, messageStatus,
										messageText, bmtSeqNumberOfMessage);

								// messages for website orders are routed to TLO
								if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
									destination = BOMConstants.TLO_QUEUE;
									bmtSeqNumberOfMessage = dlcf.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, parentOrder.getOrderXml(),
											parentOrder.getOrderNumber(), sendingShopCode,
											BOMConstants.DLCF_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
									setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);
								}
								// send message to corresponding shop
								else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
									// BMT Message
									if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

										destination = BOMConstants.FSI_QUEUE;
										if (sendingShopCode.equals(fulfillingShopCode)) {
											fsiXml = transformService.createOutboundMessage(
													BOMConstants.DLCF_MESSAGE_DESC, dlcf, originalSendingShopCode,
													fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE),
													parentOrderNumber, bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
										}
										ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
										MessageDlcf outBoundDlcf = fsi.getMessagesOnOrder().get(0).getMessageDlcf()
												.get(0);
										sendingShopCode = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
										sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DLCF_MESSAGE_DESC,
												communicationMethod, outBoundDlcf.getSendingShopCode(),
												outBoundDlcf.getReceivingShopCode(), messageText);
										updateMessage(orderNumber, messageId);
										orderDAO.persistChildOrderAndActivity(childOrder, parentOrderNumber,
												INTERNAL_USER, BOMConstants.ACT_ROUTING, status,
												BOMConstants.FSI_QUEUE);

									}
									// TEL Message
									else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
										updateMessage(orderNumber, messageId);
										destination = BOMConstants.TFSI_QUEUE;
										bmtSeqNumberOfMessage = dlcf.getIdentifiers().getGeneralIdentifiers()
												.getBmtSeqNumberOfMessage();
										sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
												BOMConstants.DLCF_MESSAGE_DESC, skillset, communicationMethod,
												recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate,
												city, zip, occasionCode, Byte.toString(orderType));
										// setOrderStatus(parentMessageOrder,destination,status,TFSI_USER);

									}
								}
								// TLO Message
								else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
										|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
										|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
										|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
									destination = BOMConstants.TLO_QUEUE;
									bmtSeqNumberOfMessage = dlcf.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DLCF_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));

									status = bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP);
									setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);

									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(status));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
									//

								}

								sendAcknowledgement(dlcf);
							} else {
								fsiXml = transformService.createForeignSystemInterface(BOMConstants.DLCF_MESSAGE_DESC,
										dlcf);

								sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber,
										sendingShopCode, BOMConstants.DLCF_MESSAGE_DESC, skillset, communicationMethod,
										recievingShopCode, messageText,
										dlcf.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ",
										" ", " ", " ", " ");

								sendAcknowledgement(dlcf);

							}
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (HibernateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JMSException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageDlcaList
	 * @throws Exception
	 */
	private void handleMessageDlca(final List<MessageDlca> messageDlcaList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;
					String status = bomProperties.getProperty(BOMConstants.ORDER_DELIVERY_ATTEMPTED);

					int numOfMessages = messageDlcaList.size();
					logger.debug("num of dlcas: " + numOfMessages);

					for (MessageDlca dlca : messageDlcaList) {

						orderNumber = dlca.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = dlca.getSendingShopCode();
						String recievingShopCode = dlca.getReceivingShopCode();
						String fulfillingShopCode = dlca.getFulfillingShopCode();
						String messageText = dlca.getMessageText();
						String bmtSeqNumberOfMessage = dlca.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DLCA_MESSAGE_DESC,
									dlca);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DLCA_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// messages for website orders are routed to TLO
							if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = dlca.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.DLCA_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
								setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);
							}
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									destination = BOMConstants.FSI_QUEUE;
									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.DLCA_MESSAGE_DESC,
												dlca, originalSendingShopCode,
												fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDlca outBoundDlca = fsi.getMessagesOnOrder().get(0).getMessageDlca().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DLCA_MESSAGE_DESC,
											communicationMethod, outBoundDlca.getSendingShopCode(),
											outBoundDlca.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);
									orderDAO.persistChildOrderAndActivity(childOrder, parentOrderNumber, INTERNAL_USER,
											BOMConstants.ACT_ROUTING, status, BOMConstants.FSI_QUEUE);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = dlca.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DLCA_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
									// setOrderStatus(parentMessageOrder,destination,status,
									// TFSI_USER);

								}
							}
							// TLO Message
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = dlca.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								status = bomProperties.getProperty(BOMConstants.ORDER_DELIVERY_ATTEMPTED);
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber,
								// sendingShopCode,BOMConstants.DLCA_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);

								//
								BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(parentOrder.getBomorderId());
								bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
								bomorderSts.setStsRoutingId(Byte.parseByte(status));
								byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
								bomorderSts.setVirtualqueueId(virtualqueueId);
								orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								//

							}

							// send ack
							sendAcknowledgement(dlca);

						} else {
							// does not have communication method
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DLCA_MESSAGE_DESC,
									dlca);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DLCA_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									dlca.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(dlca);
						}

					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	private void handleMessageDisp(final List<MessageDisp> messageDispList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageDispList.size();
					logger.debug("num of disps: " + numOfMessages);

					for (MessageDisp disp : messageDispList) {

						sendAcknowledgement(disp);

						orderNumber = disp.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = disp.getSendingShopCode();
						String recievingShopCode = disp.getReceivingShopCode();
						String fulfillingShopCode = disp.getFulfillingShopCode();
						String messageText = disp.getMessageText();
						String bmtSeqNumberOfMessage = disp.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String currentFulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String OrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DISP_MESSAGE_DESC,
									disp);

							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DISP_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// if current fulfilling shops equals
							// BOMConstants.FSI_SHOP_CODE
							// order has been fulfilled and is TLO queue
							// send message to TLO queue
							if (currentFulfillingShopCode
									.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
								// if order is in tlo queue send message there
								// too
								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("disp actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								if (!actRoutingList.isEmpty()) {

									int numOfRouting = actRoutingList.size();

									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
									}
								} else {
									return null;
								}

							}
							// if current child order is rejected or confirmed
							// cancelled
							// send message to queue
							else if ((Byte.toString(childOrderStatus)
									.equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))) {

								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("inqr actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								logger.debug("inqr actRoutingList: " + actRoutingList.get(0).getStatus()
										+ "ordernumber: " + OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									logger.debug("inqr virtualQueue: " + virtualQueue + "ordernumber: " + OrderNumber);
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
									}
								}
							}
							// BMT Message
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									destination = BOMConstants.FSI_QUEUE;
									if (recievingShopCode.equals(fulfillingShopCode)) {
										bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
										OrderNumber = childOrder.getOrderNumber();
										fsiXml = transformService.createOutboundMessage(BOMConstants.DISP_MESSAGE_DESC,
												disp, fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE),
												currentFulfillingShopCode, OrderNumber, bmtSeqNumberOfOrder,
												bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDisp outBoundDisp = fsi.getMessagesOnOrder().get(0).getMessageDisp().get(0);
									System.out.println("dispute: " + fsiXml);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DISP_MESSAGE_DESC,
											communicationMethod, outBoundDisp.getSendingShopCode(),
											outBoundDisp.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

									// TEL Message
								} else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = disp.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DISP_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = disp.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber, sendingShopCode,
								// BOMConstants.DISP_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));

								String statusstr = bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
								setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);
								//
								BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(parentOrder.getBomorderId());
								bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
								bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
								byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
								bomorderSts.setVirtualqueueId(virtualqueueId);
								orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								//
							}

						} else {

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DISP_MESSAGE_DESC,
									disp);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DISP_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									disp.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");
							// New message and not have communication method yet

						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	private void handleMessageDspc(final List<MessageDspc> messageDspcList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageDspcList.size();
					logger.debug("num of dscps: " + numOfMessages);

					for (MessageDspc dspc : messageDspcList) {

						sendAcknowledgement(dspc);

						orderNumber = dspc.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = dspc.getSendingShopCode();
						String recievingShopCode = dspc.getReceivingShopCode();
						String fulfillingShopCode = dspc.getFulfillingShopCode();
						String messageText = dspc.getMessageText();
						String bmtSeqNumberOfMessage = dspc.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPC_MESSAGE_DESC,
									dspc);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DSPC_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// messages for website orders are routed to TLO
							if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = dspc.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.DENI_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
							}
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.DSPC_MESSAGE_DESC,
												dspc, originalSendingShopCode,
												fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDspc outBoundDspc = fsi.getMessagesOnOrder().get(0).getMessageDspc().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DSPC_MESSAGE_DESC,
											communicationMethod, outBoundDspc.getSendingShopCode(),
											outBoundDspc.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = dspc.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DSPC_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								// bmtSeqNumberOfMessage =
								// dspc.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber, sendingShopCode,
								// BOMConstants.DENI_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								boolean isLocked = orderDAO.isOrderLocked(parentOrderNumber);
								if (!isLocked) {
									String status = bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);

									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(status));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//
							}

						} else {
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPC_MESSAGE_DESC,
									dspc);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DSPC_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									dspc.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	private void handleMessageDspr(final List<MessageDspr> messageDsprList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageDsprList.size();
					logger.debug("num of dsprs: " + numOfMessages);

					for (MessageDspr dspr : messageDsprList) {

						sendAcknowledgement(dspr);

						orderNumber = dspr.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = dspr.getSendingShopCode();
						String recievingShopCode = dspr.getReceivingShopCode();
						String fulfillingShopCode = dspr.getFulfillingShopCode();
						String messageText = dspr.getMessageText();
						String bmtSeqNumberOfMessage = dspr.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPR_MESSAGE_DESC,
									dspr);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DSPR_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// send message to corresponding shop
							if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.DSPR_MESSAGE_DESC,
												dspr, originalSendingShopCode,
												fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDeni outBoundDeni = fsi.getMessagesOnOrder().get(0).getMessageDeni().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DENI_MESSAGE_DESC,
											communicationMethod, outBoundDeni.getSendingShopCode(),
											outBoundDeni.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = dspr.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DSPR_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								// bmtSeqNumberOfMessage =
								// dspr.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber, sendingShopCode,
								// BOMConstants.DSPR_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								boolean isLocked = orderDAO.isOrderLocked(parentOrderNumber);
								if (!isLocked) {
									String status = bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);

									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(status));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//
							}

						} else {
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPR_MESSAGE_DESC,
									dspr);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DSPR_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									dspr.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});

	}

	private void handleMessageDspd(final List<MessageDspd> messageDspdList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageDspdList.size();
					logger.debug("num of dspd: " + numOfMessages);

					for (MessageDspd dspd : messageDspdList) {

						sendAcknowledgement(dspd);

						orderNumber = dspd.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = dspd.getSendingShopCode();
						String recievingShopCode = dspd.getReceivingShopCode();
						String fulfillingShopCode = dspd.getFulfillingShopCode();
						String messageText = dspd.getMessageText();
						String bmtSeqNumberOfMessage = dspd.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPD_MESSAGE_DESC,
									dspd);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DSPD_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// messages for website orders are routed to TLO
							if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = dspd.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.DSPD_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
							}
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.DSPD_MESSAGE_DESC,
												dspd, originalSendingShopCode,
												fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDspd outBoundDspd = fsi.getMessagesOnOrder().get(0).getMessageDspd().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DSPD_MESSAGE_DESC,
											communicationMethod, outBoundDspd.getSendingShopCode(),
											outBoundDspd.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = dspd.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DSPD_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								// bmtSeqNumberOfMessage =
								// dspd.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage();
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber, sendingShopCode,
								// BOMConstants.DSPD_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
								boolean isLocked = orderDAO.isOrderLocked(parentOrderNumber);
								if (!isLocked) {
									String status = bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, status, INTERNAL_USER);

									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(status));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
								//
							}

						} else {
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPD_MESSAGE_DESC,
									dspd);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DSPD_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									dspd.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	private void handleMessageDspu(final List<MessageDspu> messageDspuList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageDspuList.size();
					logger.debug("num of dsprs: " + numOfMessages);

					for (MessageDspu dspu : messageDspuList) {

						orderNumber = dspu.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = dspu.getSendingShopCode();
						String recievingShopCode = dspu.getReceivingShopCode();
						String fulfillingShopCode = dspu.getFulfillingShopCode();
						String messageText = dspu.getMessageText();
						String bmtSeqNumberOfMessage = dspu.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPU_MESSAGE_DESC,
									dspu);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DSPU_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// send message to corresponding shop
							if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.DSPU_MESSAGE_DESC,
												dspu, originalSendingShopCode,
												fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDspu outBoundDspu = fsi.getMessagesOnOrder().get(0).getMessageDspu().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DSPU_MESSAGE_DESC,
											communicationMethod, outBoundDspu.getSendingShopCode(),
											outBoundDspu.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = dspu.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DSPU_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = dspu.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
										BOMConstants.DSPU_MESSAGE_DESC, skillset, communicationMethod,
										recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city, zip,
										occasionCode, Byte.toString(orderType));
							}

							sendAcknowledgement(dspu);
						} else {
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DSPR_MESSAGE_DESC,
									dspu);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DSPU_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									dspu.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(dspu);

						}
					}
				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageDeniList
	 * @throws Exception
	 */
	private void handleMessageDeni(final List<MessageDeni> messageDeniList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageDeniList.size();
					logger.debug("num of denis: " + numOfMessages);

					for (MessageDeni deni : messageDeniList) {

						orderNumber = deni.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = deni.getSendingShopCode();
						String recievingShopCode = deni.getReceivingShopCode();
						String fulfillingShopCode = deni.getFulfillingShopCode();
						String messageText = deni.getMessageText();
						String bmtSeqNumberOfMessage = deni.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();
							String status = bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_DENI);

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DENI_MESSAGE_DESC,
									deni);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.DENI_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// messages for website orders are routed to TLO
							if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = deni.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.DENI_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
							}
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.DENI_MESSAGE_DESC,
												deni, originalSendingShopCode,
												fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageDeni outBoundDeni = fsi.getMessagesOnOrder().get(0).getMessageDeni().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.DENI_MESSAGE_DESC,
											communicationMethod, outBoundDeni.getSendingShopCode(),
											outBoundDeni.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

								}
								// TEL Message
								else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = deni.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.DENI_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								boolean isLocked = orderDAO.isOrderLocked(parentOrderNumber);
								if (!isLocked) {
									status = bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
								}
							}
							orderDAO.persistChildOrderAndActivity(childOrder, parentOrderNumber, INTERNAL_USER,
									BOMConstants.ACT_ROUTING, status, BOMConstants.FSI_QUEUE);

							sendAcknowledgement(deni);
						} else {
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.DENI_MESSAGE_DESC,
									deni);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.DENI_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									deni.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(deni);

						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageConfList
	 * @throws Exception
	 */
	private void handleMessageConf(final List<MessageConf> messageConfList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageConfList.size();
					logger.debug("num of confs: " + numOfMessages);

					for (MessageConf conf : messageConfList) {

						orderNumber = conf.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = conf.getSendingShopCode();
						String recievingShopCode = conf.getReceivingShopCode();
						String messageText = conf.getMessageText();
						String fulfillingShopCode = conf.getFulfillingShopCode();
						String bmtSeqNumberOfMessage = conf.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String originalSendingShopCode = orderDAO.getOriginalSendingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String parentOrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();
							String status = bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF);

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.CONF_MESSAGE_DESC,
									conf);
							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.CONF_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// messages for website orders are routed to TLO
							if (originalSendingShopCode.equals(BOMConstants.WEBSITE_ORDER)) {
								destination = BOMConstants.TLO_QUEUE;
								bmtSeqNumberOfMessage = conf.getIdentifiers().getGeneralIdentifiers()
										.getBmtSeqNumberOfMessage();
								sendMessageToDestination(destination, parentOrder.getOrderXml(),
										parentOrder.getOrderNumber(), sendingShopCode, BOMConstants.CONF_MESSAGE_DESC,
										skillset, communicationMethod, recievingShopCode, messageText,
										bmtSeqNumberOfMessage, deliveryDate, city, zip, occasionCode,
										Byte.toString(orderType));
							}
							// BMT Message
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									if (sendingShopCode.equals(fulfillingShopCode)) {
										fsiXml = transformService.createOutboundMessage(BOMConstants.CONF_MESSAGE_DESC,
												conf, originalSendingShopCode, "", parentOrderNumber,
												bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageConf outBoundConf = fsi.getMessagesOnOrder().get(0).getMessageConf().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.CONF_MESSAGE_DESC,
											communicationMethod, outBoundConf.getSendingShopCode(),
											outBoundConf.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);

									// TEL Message
								} else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = conf.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.CONF_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {
								destination = BOMConstants.TLO_QUEUE;
								boolean isLocked = orderDAO.isOrderLocked(parentOrderNumber);
								if (!isLocked) {
									status = bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
								}
								// sendMessageToDestination(destination, fsiXml,
								// orderNumber, sendingShopCode,
								// BOMConstants.CONF_MESSAGE_DESC,skillset,communicationMethod,
								// recievingShopCode,messageText,bmtSeqNumberOfMessage,deliveryDate,
								// city, zip, occasionCode,
								// Byte.toString(orderType));
							}

							orderDAO.persistChildOrderAndActivity(childOrder, parentOrderNumber, INTERNAL_USER,
									BOMConstants.ACT_ROUTING, status, BOMConstants.FSI_QUEUE);

							sendAcknowledgement(conf);

						} else {
							// New message and not have communication method yet
							fsiXml = transformService.createForeignSystemInterface(BOMConstants.CONF_MESSAGE_DESC,
									conf);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.CONF_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									conf.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");

							sendAcknowledgement(conf);

						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * Process single message based on communication method
	 * 
	 * @param messageCancList
	 * @throws Exception
	 */
	private void handleMessageCanc(final List<MessageCanc> messageCancList) throws Exception {

		transactionTemplate.execute(new TransactionCallback<Void>() {

			public Void doInTransaction(TransactionStatus txStatus) {
				try {

					String destination = BOMConstants.FSI_QUEUE;

					String fsiXml = "";
					String orderNumber = "";
					int messageId = 0;

					String communicationMethod = "";
					String skillset = BOMConstants.SKILLSET_BASICAGENT;

					int numOfMessages = messageCancList.size();
					logger.debug("num of cancs: " + numOfMessages);

					for (MessageCanc canc : messageCancList) {

						orderNumber = canc.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();
						String sendingShopCode = canc.getSendingShopCode();
						String recievingShopCode = canc.getReceivingShopCode();
						String fulfillingShopCode = canc.getFulfillingShopCode();
						String messageText = canc.getMessageText();
						String bmtSeqNumberOfMessage = canc.getIdentifiers().getGeneralIdentifiers()
								.getBmtSeqNumberOfMessage();

						communicationMethod = messageDAO.getCommunicationMethod(orderNumber);
						boolean isNewMsg = messageDAO.isUniqueMessage(bmtSeqNumberOfMessage, messageText);

						if ((communicationMethod != null) && (isNewMsg)) {

							String currentFulfillingShopCode = orderDAO.getCurrentFulfillingShopCode(orderNumber);

							Bomorder parentOrder = orderDAO.getParentOrderByOrderNumber(orderNumber);
							byte orderType = parentOrder.getOrderType();
							Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);
							byte childOrderType = childOrder.getOrderType();
							byte childOrderStatus = orderDAO.getCurrentOrderStatus(childOrder);
							ForeignSystemInterface parentFSI = transformService
									.convertToJavaFSI(parentOrder.getOrderXml());
							MessageOrder parentMessageOrder = parentFSI.getMessagesOnOrder().get(0).getMessageOrder();
							String OrderNumber = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtOrderNumber();
							String bmtSeqNumberOfOrder = parentMessageOrder.getIdentifiers().getGeneralIdentifiers()
									.getBmtSeqNumberOfOrder();
							String deliveryDate = parentMessageOrder.getDeliveryDetails().getDeliveryDate();
							String city = parentMessageOrder.getRecipient().getRecipientCity();
							String zip = parentMessageOrder.getRecipient().getRecipientZipCode();
							String occasionCode = parentMessageOrder.getOrderDetails().getOccasionCode();

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.CANC_MESSAGE_DESC,
									canc);

							byte messageStatus = Byte
									.parseByte(bomProperties.getProperty(BOMConstants.MESSAGE_TO_BE_WORKED));
							messageId = saveMessage(orderNumber, fsiXml, BOMConstants.CANC_MESSAGE_DESC,
									communicationMethod, sendingShopCode, recievingShopCode, messageStatus, messageText,
									bmtSeqNumberOfMessage);

							// if current fulfilling shops equals
							// BOMConstants.FSI_SHOP_CODE
							// order has been fulfilled and is TLO queue
							// send message to TLO queue
							if (currentFulfillingShopCode
									.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))) {
								// if order is in tlo queue send message there
								// too
								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("canc actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								if (!actRoutingList.isEmpty()) {

									int numOfRouting = actRoutingList.size();

									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										logger.debug("Fulfulling shop is still: "
												+ fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE)
												+ "sending message to " + destination);
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String status = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											orderDAO.persistOrderactivityByUser(INTERNAL_USER, BOMConstants.ACT_ROUTING,
													status, orderNumber, destination);
											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(status));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
										// sendMessageToDestination(destination,
										// fsiXml, orderNumber,
										// sendingShopCode,BOMConstants.CANC_MESSAGE_DESC,skillset,communicationMethod,
										// recievingShopCode,messageText,bmtSeqNumberOfOrder,deliveryDate,
										// city, zip, occasionCode,
										// Byte.toString(orderType));
									}
								} else {
									return null;
								}

							}
							// if current child order is rejected or confirmed
							// cancelled
							// send message to queue
							else if ((Byte.toString(childOrderStatus)
									.equals(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT)))
									|| (Byte.toString(childOrderStatus)
											.equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))) {

								List<ActRouting> actRoutingList = orderDAO.getActRouting(OrderNumber);
								System.out.println("inqr actRoutingList: " + actRoutingList.toString() + "ordernumber: "
										+ OrderNumber);
								logger.debug("inqr actRoutingList: " + actRoutingList.get(0).getStatus()
										+ "ordernumber: " + OrderNumber);
								if (!actRoutingList.isEmpty()) {
									int numOfRouting = actRoutingList.size();
									String virtualQueue = actRoutingList.get(numOfRouting - 1).getVirtualQueue();
									logger.debug("inqr virtualQueue: " + virtualQueue + "ordernumber: " + OrderNumber);
									if (virtualQueue.equals(BOMConstants.TLO_QUEUE)) {
										destination = virtualQueue;
										boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
										if (!isLocked) {
											String statusstr = bomProperties
													.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
											setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);

											BomorderSts bomorderSts = orderDAO
													.getBomorderStsOrderId(parentOrder.getBomorderId());
											bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
											bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
											byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
											bomorderSts.setVirtualqueueId(virtualqueueId);
											orderDAO.updateBomorderSts(bomorderSts, 0);
										}
									}
								}
							}
							// BMT Message
							// send message to corresponding shop
							else if (communicationMethod.equals(BOMConstants.COMMMETHOD_API)) {
								// BMT Message
								if (childOrderType == BOMConstants.ORDER_TYPE_BMT) {

									destination = BOMConstants.FSI_QUEUE;
									if (recievingShopCode.equals(fulfillingShopCode)) {
										bmtSeqNumberOfOrder = childOrder.getBmtOrderSequenceNumber();
										OrderNumber = childOrder.getOrderNumber();
										fsiXml = transformService.createOutboundMessage(BOMConstants.CANC_MESSAGE_DESC,
												canc, fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE),
												currentFulfillingShopCode, OrderNumber, bmtSeqNumberOfOrder,
												bmtSeqNumberOfMessage);
									}
									ForeignSystemInterface fsi = transformService.convertToJavaFSI(fsiXml);
									MessageCanc outBoundCanc = fsi.getMessagesOnOrder().get(0).getMessageCanc().get(0);
									sendMessageToBloomlink(orderNumber, fsiXml, BOMConstants.CANC_MESSAGE_DESC,
											communicationMethod, outBoundCanc.getSendingShopCode(),
											outBoundCanc.getReceivingShopCode(), messageText);
									updateMessage(orderNumber, messageId);
									String status = bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_BY_SSHOP);
									orderDAO.persistChildOrderAndActivity(childOrder, parentOrder.getOrderNumber(),
											INTERNAL_USER, BOMConstants.ACT_ROUTING, status, BOMConstants.FSI_QUEUE);

									// TEL Message
								} else if (childOrderType == BOMConstants.ORDER_TYPE_TEL) {
									updateMessage(orderNumber, messageId);
									destination = BOMConstants.TFSI_QUEUE;
									bmtSeqNumberOfMessage = canc.getIdentifiers().getGeneralIdentifiers()
											.getBmtSeqNumberOfMessage();
									sendMessageToDestination(destination, fsiXml, orderNumber, sendingShopCode,
											BOMConstants.CANC_MESSAGE_DESC, skillset, communicationMethod,
											recievingShopCode, messageText, bmtSeqNumberOfMessage, deliveryDate, city,
											zip, occasionCode, Byte.toString(orderType));
								}
							} else if (communicationMethod.equals(BOMConstants.COMMMETHOD_PHONE)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_FAX)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_EMAIL)
									|| communicationMethod.equals(BOMConstants.COMMMETHOD_OTHER)) {

								destination = BOMConstants.TLO_QUEUE;
								boolean isLocked = orderDAO.isOrderLocked(OrderNumber);
								if (!isLocked) {
									String statusstr = bomProperties
											.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED);
									setOrderStatus(parentMessageOrder, destination, statusstr, INTERNAL_USER);
									//
									BomorderSts bomorderSts = orderDAO
											.getBomorderStsOrderId(parentOrder.getBomorderId());
									bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
									bomorderSts.setStsRoutingId(Byte.parseByte(statusstr));
									byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
									bomorderSts.setVirtualqueueId(virtualqueueId);
									orderDAO.updateBomorderSts(bomorderSts, childOrder.getBomorderId());
								}
							}

							sendAcknowledgement(canc);
						} else {

							fsiXml = transformService.createForeignSystemInterface(BOMConstants.CANC_MESSAGE_DESC,
									canc);

							sendMessageToDestination(BOMConstants.MESSAGES_QUEUE, fsiXml, orderNumber, sendingShopCode,
									BOMConstants.CANC_MESSAGE_DESC, skillset, communicationMethod, recievingShopCode,
									messageText,
									canc.getIdentifiers().getGeneralIdentifiers().getBmtSeqNumberOfMessage(), " ", " ",
									" ", " ", " ");
							// New message and not have communication method yet

							sendAcknowledgement(canc);
						}
					}

				} catch (Exception e) {
					logger.error(e.getMessage());
					txStatus.setRollbackOnly();
					e.printStackTrace();
				}

				return null;
			}

		});
	}

	/**
	 * sends acknowledgement to BloomLink
	 * 
	 * @param message
	 * @throws Exception
	 */
	private void sendAcknowledgement(BloomNetMessage message) throws Exception {

		String dataEncoded = "";
		String post = "";
		String response = "";
		String fsi = "";

		fsi = transformService.createForeignSystemInterface(BOMConstants.ACKF_MESSAGE_DESC, message);

		try {

			dataEncoded = URLEncoder.encode(fsi, "UTF-8");
			post = fsiProperties.getProperty("fsi.endpoint") + BOMConstants.POST_MESSAGE + dataEncoded;

			if (logger.isDebugEnabled()) {
				logger.debug("Sending acknowledgement to Bloomlink " + fsi);
			}
			response = fsiClient.sendRequest(post, "ackf");// XXX place error
															// handling here

			ForeignSystemInterface responseFsi = transformService.convertToJavaFSI(response);

			int numOfErrors = 0;
			numOfErrors = responseFsi.getErrors().getError().size();

			if (numOfErrors > 0) {
				List<Error> errors = responseFsi.getErrors().getError();

				for (Error error : responseFsi.getErrors().getError()) {
					logger.error("recieved error acknowledgement from BloomLink");
					logger.error(
							error.getErrorCode() + "|" + error.getDetailedErrorCode() + "|" + error.getErrorMessage());
					throw new BloomLinkException(error.getErrorMessage());

				}
			}
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (BloomLinkException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Sends FSI message to Queue based on routing logic Sets order number as a
	 * message property Certain shops bypass FSI destination
	 * 
	 * @param message
	 * @param orderNumber
	 * @param sendingShopCode
	 * @throws JMSException
	 */
	public void sendOrderToQueue(String destination, String message, String orderNumber, String sendingShopCode,
			String deliveryDate, String city, String zip, String occasionCode, String skillset, String messageType,
			String timeZone) throws JMSException {

		messageProducer.produceMessage(destination, message, orderNumber, sendingShopCode, deliveryDate, city, zip,
				occasionCode, skillset, messageType, Byte.toString(BOMConstants.ORDER_TYPE_BMT), timeZone);

		String notify = bomProperties.getProperty(BOMConstants.EMAIL_NOTIFY);
		if (notify.equals("ON") && messageType.equals(BOMConstants.ORDER_MESSAGE_DESC)) {
			String toEmail = bomProperties.getProperty(BOMConstants.EMAIL_NOTIFY_TO);
			messageProducer.produceOutboundMessage(BOMConstants.EMAIL_QUEUE, message, orderNumber, " ", deliveryDate,
					occasionCode, toEmail);
		}

	}

	/**
	 * Determines where to send shop based on destinations set in bom.properties
	 * and by shop code
	 * 
	 * @param sendingShopCode
	 * @param orderNumber
	 * @param deliveryDate
	 * @return
	 */
	private String determineRoute(String sendingShopCode, String deliveryDate, String orderNumber) {

		String destination = BOMConstants.FSI_QUEUE;

		String currentDateStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date delDate = DateUtil.toDateFormat(deliveryDate);
		Date currentDate = DateUtil.toDate(currentDateStr);
		int eq = delDate.compareTo(currentDate);
		
		/*SpecialQueues queue = new SpecialQueues();

		queue.setTfsiFirst(1);
		queue.setTloFirst(0);
		queue.setAutomatedOnly(0);
		
		String telefloraFirst = shopDAO.getSpecialQueues(queue);
		
		queue.setTfsiFirst(0);
		queue.setTloFirst(1);
		queue.setAutomatedOnly(0);
		
		String tloFirst = shopDAO.getSpecialQueues(queue);*/

		destination = bomProperties.getProperty(BOMConstants.FIRST_DESTINATION);

		if (eq < 0) {
			logger.error("Order " + orderNumber + "is past-dated.  Routing to TLO Queue");
			destination = BOMConstants.SECOND_DESTINATION;
		}
		
		logger.debug("Sending order to destination: " + destination);
		System.out.println("Sending order to destination: " + destination);
		return destination;
	}

	/**
	 * Sends a message to a predetermine destination saves message and sets it
	 * status equal to to be worked
	 * 
	 * @param destination
	 * @param message
	 * @param orderNumber
	 * @throws JMSException
	 */
	public void sendMessageToDestination(String destination, String messageXml, String orderNumber,
			String sendingShopCode, String messageType, String skillset, String commmethod, String receivingShopCode,
			String messageText, String bmtSeqNumberOfMessage, String deliveryDate, String city, String zip,
			String occasionCode, String orderType) throws JMSException {

		String timeZone = new String();

		if (!zip.trim().isEmpty()) {
			timeZone = orderDAO.getZipByCode(zip).getTimeZone();
		}
		if (destination.equals(BOMConstants.TLO_QUEUE)) {

			Bomorder orderEntity = orderDAO.getOrderByOrderNumber(orderNumber);

			User currentUser = orderEntity.getUser();

			List<String> tloOrders = new ArrayList<String>();
			tloOrders = messageProducer.BrowseQueue();

			boolean onTLO = tloOrders.contains(orderNumber);

			// check to see if order is currently being worked are is on the tlo
			// queue
			if ((currentUser != null) || (tloOrders.contains(orderNumber))) {
				logger.info("Order " + orderEntity.getOrderNumber() + " is currently being worked");
				destination = BOMConstants.MESSAGES_QUEUE;
			} else {
				messageProducer.produceMessage(destination, messageXml, orderNumber, sendingShopCode, deliveryDate,
						city, zip, occasionCode, skillset, messageType, orderType, timeZone);
			}
		}

		else {
			messageProducer.produceMessage(destination, messageXml, orderNumber, sendingShopCode, deliveryDate, city,
					zip, occasionCode, skillset, messageType, orderType, timeZone);
		}

	}

	/**
	 * Persist message and set message status
	 * 
	 * @param messageXml
	 * @param messageTypeDesc
	 * @param commmethod
	 * @param sendingShopCode
	 * @param receivingShopCode
	 */
	private int saveMessage(String orderNumber, String messageXml, String messageTypeDesc, String commmethodDesc,
			String sendingShopCode, String receivingShopCode, byte status, String messageTxt,
			String bmtMessageSequenceNumber) throws Exception {

		ActMessage message = new ActMessage();

		Shop sendingShop = shopDAO.getShopByCode(sendingShopCode);
		Shop receivingShop = shopDAO.getShopByCode(receivingShopCode);

		StsMessage stsMessage = messageDAO.getStsById(status);

		Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc(BOMConstants.ACT_MESSAGE);
		Orderactivity orderactivity = new Orderactivity(oactivitytype);
		User user = userDAO.getUserByUserName(INTERNAL_USER);
		orderactivity.setActMessage(message);
		orderactivity.setUser(user);
		Bomorder order = orderDAO.getOrderByOrderNumber(orderNumber);
		Long bomorderId = order.getBomorderId();
		Bomorder bomorder = orderDAO.getOrderByOrderId(bomorderId);
		orderactivity.setBomorder(bomorder);
		orderactivity.setCreatedDate(new Date());

		Messagetype messagetype = messageDAO.getMessageTypeByShortDesc(messageTypeDesc);

		final Commmethod commmethod = messageDAO.getCommethodByDesc(commmethodDesc);

		byte messageFormat = BOMConstants.ORDER_TYPE_BMT; // messageFormat is
															// equivalent to
															// orderTYpe in
															// bomorder

		message.setOrderactivity(orderactivity);
		message.setMessagetype(messagetype);
		message.setCommmethod(commmethod);
		message.setMessageXml(messageXml);
		message.setShopByReceivingShopId(receivingShop);
		message.setShopBySendingShopId(sendingShop);
		message.setMessageFormat(messageFormat);
		message.setStsMessage(stsMessage);
		message.setMessageText(messageTxt);
		message.setBmtMessageSequenceNumber(bmtMessageSequenceNumber);

		logger.debug("saving message:  [" + messageTxt + "] for order number: " + orderNumber + " to status " + status);
		int messageId = messageDAO.persistMessage(message);

		return messageId;

	}

	public void updateMessage(String orderNumber, int messageId) throws Exception {

		// update the status of the original message
		logger.debug("updating status of message: " + messageId + "for order number: " + orderNumber + "to completed");
		String status = bomProperties.getProperty(BOMConstants.MESSAGE_WORK_COMPLETED);
		String messageIdStr = Integer.toString(messageId);
		long messageIdLong = Long.parseLong(messageIdStr);
		messageDAO.updateMessageStatus(messageIdLong, status);

	}

	/**
	 * persist order
	 * 
	 * @param orderXml
	 * @param messageOrder
	 * @param commmethod
	 */
	private void saveOrder(String bmtSeqNumberOfOrder, String bmtSeqNumberOfMessage, String orderXml,
			MessageOrder messageOrder, String commmethod, String destination, String status) throws Exception {

		try {

			byte commid = BOMConstants.COMMMETHOD_API_ID;
			final Commmethod commmethod1 = orderDAO.getCommmethodById(commid);

			String cityName = messageOrder.getRecipient().getRecipientCity();
			String stateName = messageOrder.getRecipient().getRecipientState();
			String countryName = messageOrder.getRecipient().getRecipientCountryCode();

			City city = orderDAO.getCityByNameAndState(cityName, stateName);
			if (city == null || city.getCityId() == null) {
				logger.debug("Order Number: " + messageOrder.getOrderDetails().getOrderNumber() + ", " + cityName
						+ " not found in " + stateName);
				city = orderDAO.getCityByZip(messageOrder.getRecipient().getRecipientZipCode(),
						messageOrder.getRecipient().getRecipientCity());

				if (city == null || city.getCityId() == null) {
					logger.debug("Order Number: " + messageOrder.getOrderDetails().getOrderNumber() + ", " + cityName
							+ " not found in " + messageOrder.getRecipient().getRecipientZipCode());
					city = orderDAO.persistCity(cityName, stateName, countryName);
					if (!countryName.equals("USA") && !countryName.equals("US") && !countryName.equals("CAN")
							&& !countryName.equals("PUR")) {
						orderDAO.persistCityCoverage(city);
					}
				}
			}
			Shop receivingShop = shopDAO.getShopByCode(messageOrder.getReceivingShopCode());
			Shop sendingShop = shopDAO.getShopByCode(messageOrder.getSendingShopCode());
			Zip zip = orderDAO.getZipByCode(messageOrder.getRecipient().getRecipientZipCode());

			if ((zip == null || zip.getZipCode() == null || zip.getZipCode().trim().equals(""))
					&& !countryName.equals("USA") && !countryName.equals("US") && !countryName.equals("CAN")
					&& !countryName.equals("PUR")) {
				zip = orderDAO.getZipByCode("00000");
				orderXml = orderXml.replace("<recipientZipCode></recipientZipCode>",
						"<recipientZipCode>00000</recipientZipCode>");
			}
			String deliveryDateStr = messageOrder.getDeliveryDetails().getDeliveryDate();
			Date deliveryDate = DateUtil.toDateFormat(deliveryDateStr);

			if ((sendingShop == null)) {
				logger.info("Order Number: " + messageOrder.getOrderDetails().getOrderNumber() + ", "
						+ " Shop cannot be found by shop code: " + messageOrder.getSendingShopCode());
				sendingShop = saveShop(messageOrder.getSendingShopCode());
				if (sendingShop.getShopId() == null) {
					sendAcknowledgement(messageOrder);
					new SendGridEmailer("Order : " + messageOrder.getOrderDetails().getOrderNumber()
							+ " needs to be worked outside of BOM. Sending shop is not a valid shop");
					throw new Exception("Order : " + messageOrder.getOrderDetails().getOrderNumber()
							+ " needs to be worked outside of BOM. Sending shop is not a valid shop");
				}
			}

			orderXml = orderXml.replaceAll("\t", " ").replaceAll("\r", " ").replaceAll("\n", " ").replaceAll("% ", "%25 ").replaceAll("\\|", "-").replaceAll("%.50A", " ").replaceAll("%.50D", " ").replaceAll("%\\.", "%25\\.").replaceAll("%\t","%25 ").replaceAll("%.5", "%25");

			Bomorder order = new Bomorder(messageOrder.getOrderDetails().getOrderNumber(), BOMConstants.ORDER_TYPE_BMT,
					orderXml, city, zip, commmethod1, new Date(), deliveryDate,
					Double.parseDouble(messageOrder.getOrderDetails().getTotalCostOfMerchandise()), receivingShop,
					sendingShop, bmtSeqNumberOfOrder, bmtSeqNumberOfMessage);

			String orderNumber = orderDAO.persistParentOrderAndActivity(order, INTERNAL_USER, BOMConstants.ACT_ROUTING,
					status, destination);

			logger.debug("updating status of order " + orderNumber + " to be worked");
			logger.debug("Completed order: " + order.getOrderNumber());

		} catch (Exception e) {
			logger.error("Can not save order: " + messageOrder.getOrderDetails().getOrderNumber() + "because "
					+ e.getMessage());
			e.printStackTrace();
			throw new Exception("Can not save order: " + messageOrder.getOrderDetails().getOrderNumber() + "because "
					+ e.getMessage());

		}

	}

	/**
	 * Save sending order shop from FSI to BOM
	 * 
	 * @param sendingShopCode
	 * @return
	 * @throws Exception
	 */
	private Shop saveShop(String sendingShopCode) throws Exception {

		logger.debug("Saving shop: " + sendingShopCode);
		Shop shopEntity = new Shop();

		SearchByShopCode searchShopCode = new SearchByShopCode();
		searchShopCode.setShopCode(sendingShopCode);
		searchShopCode.setReturnNonDeliveryDates("true");
		searchShopCode.setReturnFulfilledZipCodes("true");
		searchShopCode.setReturnCommunicationCode("true");

		MemberDirectoryInterface mdi = new MemberDirectoryInterface();
		SearchShopRequest searchshoprequest = new SearchShopRequest();
		MemberDirectorySearchOptions searchOptions = new MemberDirectorySearchOptions();
		searchshoprequest.setMemberDirectorySearchOptions(searchOptions);
		searchOptions.getMemberDirectorySearchOptionsGroup().add(searchShopCode);
		Security security = new Security();

		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));

		searchshoprequest.setSecurity(security);

		mdi.setSearchShopRequest(searchshoprequest);

		String mdiXml = transformService.convertToMDIXML(mdi);

		String xmlEncoded = URLEncoder.encode(mdiXml, "UTF-8");
		String post = fsiProperties.getProperty("fsi.endpoint") + BOMConstants.GET_MEMBER + xmlEncoded;

		String response = fsiClient.sendRequest(post, "GET_MEMBER");// TODO Need
																	// to check
																	// if shop
																	// is found

		MemberDirectoryInterface results = transformService.convertToJavaMDI(response);

		if (results.getSearchShopResponse().getErrors().getError().isEmpty()) {

			com.bloomnet.bom.common.jaxb.mdi.Shop shop = results.getSearchShopResponse().getShops().getShop().get(0);

			ShopAddress address = shop.getAddress();
			String shopAddress1 = address.getAddressLine1();
			String shopAddress2 = address.getAddressLine2();
			String zip = address.getZip();
			String city = address.getCity();
			String state = address.getState();
			String shopContact = address.getAttention();
			String country = address.getCountryCode();

			/*
			 * if ( (country.equals("USA")) ||(country.equals("US")) ){
			 */

			logger.debug("Saving shop: " + sendingShopCode + " from country: " + country);

			// state short name is returned

			String shopPhone = shop.getPhoneNumber();
			logger.debug("shopPhone: " + shopPhone);
			if (shopPhone.isEmpty()) {
				shopPhone = "0000000000";
			}
			String shopFax = shop.getFaxNumber();
			String shopEmail = shop.getEmail();
			String shopName = shop.getName();

			String shopCode = shop.getShopCode();
			String sundayIndicator = shop.getSundayIndicator();
			boolean openSunday = true;
			if (sundayIndicator.equals("N")) {
				openSunday = false;
			}

			shopEntity.setShopAddress1(shopAddress1);
			shopEntity.setShopAddress2(shopAddress2);
			shopEntity.setShopPhone(shopPhone);
			shopEntity.setShopContact(shopContact);
			shopEntity.setShopFax(shopFax);
			shopEntity.setShopEmail(shopEmail);
			shopEntity.setShopName(shopName);
			// shopEntity.setShop800(shop800);

			City cityEntity = orderDAO.getCityByZip(zip, city);
			if (cityEntity.getCityId() == null) {
				logger.error("Saving shop: " + sendingShopCode + ", " + city + " not found ");
				cityEntity = orderDAO.persistCity(city, state, country);
			}
			Zip zipEntity = verifyZipCode(zip);

			shopEntity.setCity(cityEntity);
			shopEntity.setZip(zipEntity);

			User user = userDAO.getUserByUserName(INTERNAL_USER);
			Date createdDate = new Date();

			shopEntity.setUserByCreatedUserId(user);
			shopEntity.setCreatedDate(createdDate);

			shopDAO.persistShop(shopEntity);

			Network network = shopDAO.getNetwork("BloomNet");

			Shopnetworkstatus shopnetworkstatus = shopDAO.getShopnetworkstatus(BOMConstants.SHOPNETWORKSTATUS_ACTIVE);

			Commmethod commmethod = messageDAO.getCommethodByDesc(BOMConstants.COMMMETHOD_API);

			Shopnetwork shopNetwork = new Shopnetwork(shopEntity, network, shopnetworkstatus, user, commmethod, user,
					shopCode, openSunday, createdDate, createdDate);

			shopDAO.persitShopnetwork(shopNetwork);

			Shopnetworkcoverage shopNetworkCoverage = new Shopnetworkcoverage(user, shopNetwork, zipEntity);
			shopNetworkCoverage.setCreatedDate(createdDate);
			shopDAO.persitShopnetworkcoverage(shopNetworkCoverage);

			ServicedZips serviceZips = shop.getServicedZips();
			if (serviceZips != null) {
				List<String> zips = serviceZips.getZip();
				for (String serviceZip : zips) {

					Zip zipCovered = verifyZipCode(serviceZip);

					Shopnetworkcoverage netCoverage2 = new Shopnetworkcoverage(user, shopNetwork, zipCovered);

					netCoverage2.setCreatedDate(new Date());

					shopDAO.persitShopnetworkcoverage(netCoverage2);

				}
			}

			/*
			 * } else{ logger.error("sending shop is from country: " + country);
			 * }
			 */

		} else {
			List<com.bloomnet.bom.common.jaxb.mdi.Error> errors = results.getSearchShopResponse().getErrors()
					.getError();
			for (com.bloomnet.bom.common.jaxb.mdi.Error error : errors) {
				logger.error("error searching for shop " + sendingShopCode + ": " + error.getDetailedErrorCode() + " | "
						+ error.getErrorCode() + " | " + error.getErrorMessage());
			}
		}
		return shopEntity;
	}

	/**
	 * Verify zip code if does not exist, enter it into db
	 * 
	 * @param zip
	 * @return
	 * @throws Exception
	 */
	private Zip verifyZipCode(String zip) throws Exception {

		Zip zipEntity = orderDAO.getZipByCode(zip);
		if (zipEntity == null) {
			logger.error(zip + " not found ");
			zipEntity = orderDAO.persistZip(zip);

		}
		return zipEntity;
	}

	/**
	 * set parent order status
	 * 
	 * @param orderXml
	 * @param messageOrder
	 * @param commmethod
	 */
	private void setOrderStatus(MessageOrder messageOrder, String destination, String status, String user)
			throws Exception {

		final String orderNumber = messageOrder.getIdentifiers().getGeneralIdentifiers().getBmtOrderNumber();

		orderDAO.persistOrderactivityByUser(user, BOMConstants.ACT_ROUTING, status, orderNumber, destination);

		logger.debug("order number : " + orderNumber + "updated with status of :" + status);

	}

	/**
	 * Write a record to act_audit table and update bomorder table
	 * 
	 * @param orderNumber
	 * @param price
	 * @throws Exception
	 */
	public void updateOrderPrice(String orderNumber, String oldPrice, String oldXml, Bomorder parentOrder)
			throws Exception {

		// create order activity
		Oactivitytype actType = orderDAO.getActivityTypeByDesc(BOMConstants.ACT_AUDIT);
		Bomorder order = orderDAO.getOrderByOrderNumber(orderNumber);
		User user = userDAO.getUserByUserName(INTERNAL_USER);
		Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(orderNumber);

		Orderactivity activity = new Orderactivity(actType);
		activity.setBomorder(order);
		activity.setUser(user);
		activity.setCreatedDate(new Date());

		long zipId = order.getZip().getZipId();
		long cityId = order.getCity().getCityId();

		ActAudit actAudit = new ActAudit(activity, oldXml, order.getDeliveryDate(), Double.parseDouble(oldPrice), zipId,
				cityId);

		orderDAO.persistOrderHistory(actAudit);

		orderDAO.updateOrder(parentOrder);
		childOrder.setPrice(Double.valueOf(parentOrder.getPrice()));
		orderDAO.updateOrder(childOrder);

	}

	/**
	 * 
	 * @param errorMsg
	 * @param sendingShopCode
	 * @param messageOrder
	 * @throws Exception
	 *             TODO can be used in future
	 */
	private void sendMessageErrMessageToSendingShop(String errorMsg, String sendingShopCode, MessageOrder messageOrder)
			throws Exception {

		// send message to sending shop
		MessageInqr inqr = new MessageInqr();
		String messageTypeDesc = BOMConstants.INQR_MESSAGE_DESC;
		String receivingShopCode = messageOrder.getSendingShopCode();
		inqr.setReceivingShopCode(receivingShopCode);
		inqr.setFulfillingShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		inqr.setSendingShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		inqr.setMessageText(errorMsg);
		inqr.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
		inqr.setIdentifiers(messageOrder.getIdentifiers());
		int messageType = MessageTypeUtil.getMessageTypeInt(messageTypeDesc);
		inqr.setMessageType(Integer.toString(messageType));
		inqr.setSystemType(BOMConstants.SYSTEM_TYPE);

		String errorXml = transformService.createForeignSystemInterface(BOMConstants.INQR_MESSAGE_DESC, inqr);

		sendMessageToBloomlink("ERRORMSG", errorXml, BOMConstants.INQR_MESSAGE_DESC, BOMConstants.COMMMETHOD_API,
				sendingShopCode, receivingShopCode, errorMsg);

		// TODO write error to db

		sendAcknowledgement(messageOrder);

	}

	private void sendDlcfRequestToFulfillingShop(String orderNumber, String fulfillingShopCode,
			MessageOrder messageOrder) throws Exception {

		String msg = "Please make sure to send a delivery confirmation after completing the order. Thank you.";
		// send message to sending shop
		MessageInqr inqr = new MessageInqr();
		String messageTypeDesc = BOMConstants.INQR_MESSAGE_DESC;
		String receivingShopCode = messageOrder.getSendingShopCode();
		inqr.setReceivingShopCode(receivingShopCode);
		inqr.setFulfillingShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		inqr.setSendingShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		inqr.setMessageText(msg);
		inqr.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));
		inqr.setIdentifiers(messageOrder.getIdentifiers());
		int messageType = MessageTypeUtil.getMessageTypeInt(messageTypeDesc);
		inqr.setMessageType(Integer.toString(messageType));
		inqr.setSystemType(BOMConstants.SYSTEM_TYPE);

		String Xml = transformService.createForeignSystemInterface(BOMConstants.INQR_MESSAGE_DESC, inqr);

		sendMessageToBloomlink(orderNumber, Xml, BOMConstants.INQR_MESSAGE_DESC, BOMConstants.COMMMETHOD_API,
				fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE), receivingShopCode, msg);

	}

	private Zip verifyZip(MessageOrder messageOrder, String countryCode, String zip, String orderNumber, String city,
			String state) throws Exception {

		Zip zipEntity = new Zip();
		boolean zipProvided = true;

		// check for USA order
		if (countryCode.equals("USA") || countryCode.equals("US")) {
			System.out.println("zip.length(): " + zip.length());

			if (zip.length() >= 5) {

				if (!zip.isEmpty()) {
					zip = zip.substring(0, 5);
					Recipient recipient = messageOrder.getRecipient();
					recipient.setRecipientZipCode(zip);
					messageOrder.setRecipient(recipient);
				}

			}

		}

		if (zip.isEmpty() || zip.equals("\n")) {
			zipProvided = false;
			zip = "99999";
			logger.debug("no zip provided for order number: " + orderNumber);
		}

		// verify zip exists, if not find it by city and state
		zipEntity = orderDAO.getZipByCode(zip);

		if ((zipEntity == null) || (zipEntity.getZipCode().trim().isEmpty())) {
			// find CAN order by first 3 initials
			if ((countryCode.equals("CA") || countryCode.equals("CAN"))
					&& messageOrder.getRecipient().getRecipientZipCode().length() >= 3) {
				zipEntity = orderDAO.getZipByCode(messageOrder.getRecipient().getRecipientZipCode().substring(0, 3));
				if (zipEntity != null) {
					Recipient recipient = messageOrder.getRecipient();
					recipient.setRecipientZipCode(zipEntity.getZipCode());
					messageOrder.setRecipient(recipient);
					zip = zipEntity.getZipCode();
				} else {// if unable to CAN Zip then persist zip
					logger.debug("Order Number: " + orderNumber + ", Recipient zip code does not exist: "
							+ messageOrder.getRecipient().getRecipientZipCode());
					logger.debug("Order Number: " + orderNumber + ", Finding zip code by city and state ....");
					zipEntity = orderDAO.getZipByCityAndState(city, state);
					Long zipId = zipEntity.getZipId();
					if (zipId == null) {
						String errorMsg = "Order Number: " + orderNumber
								+ ", There is an issue with the recipient's address information: "
								+ "Recipient zip code: " + messageOrder.getRecipient().getRecipientZipCode()
								+ " Recipient zip city: " + messageOrder.getRecipient().getRecipientCity();

						logger.debug(errorMsg);

						zipEntity = orderDAO.persistZip(messageOrder.getRecipient().getRecipientZipCode());

					}
					Recipient recipient = messageOrder.getRecipient();
					recipient.setRecipientZipCode(zipEntity.getZipCode());
					messageOrder.setRecipient(recipient);
					zip = zipEntity.getZipCode();

				}
			}
		}
		// if zip is not found by zipcode or city/state then persist
		if ((zipEntity == null) || (zipEntity.getZipCode().trim().isEmpty())) {// 41934

			logger.debug("Order Number: " + orderNumber + ", Recipient zip code does not exist: "
					+ messageOrder.getRecipient().getRecipientZipCode());
			logger.debug("Order Number: " + orderNumber + ", Finding zip code by city and state ....");
			zipEntity = orderDAO.getZipByCityAndState(city, state);
			Long zipId = zipEntity.getZipId();
			if (zipId == null) {
				String errorMsg = "Order Number: " + orderNumber
						+ ", There is an issue with the recipient's address information: " + "Recipient zip code: "
						+ messageOrder.getRecipient().getRecipientZipCode() + " Recipient zip city: "
						+ messageOrder.getRecipient().getRecipientCity();

				logger.debug(errorMsg);

				zipEntity = orderDAO.persistZip(messageOrder.getRecipient().getRecipientZipCode());

			}
			Recipient recipient = messageOrder.getRecipient();
			recipient.setRecipientZipCode(zipEntity.getZipCode());
			messageOrder.setRecipient(recipient);
			zip = zipEntity.getZipCode();

		}

		return zipEntity;

	}

}
