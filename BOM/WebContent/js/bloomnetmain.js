//
//
//
    
// defined fields default values
var $quickSearchDefaultValue    = "Quick Search";
var $orderNumberDefaultValue    = "Order Number";
var $occassionDefaultValue      = "Occasion"; 
var $sendingShopDefaultValue    = "Sending Shop Code";
var $fulfillingShopDefaultValue = "Fulfilling Shop Code";
var $zipCodeDefaultValue        = "Zip Code";
var $dateDefaultValue           = "From Date MM/DD/YY";
var $date2DefaultValue          = "To Date MM/DD/YY";
var $statusDefaultValue         = "Status";



// vars used for checking if order is locked
var $orderSelected   = null;    
var $orderLockerId   = null;  
var $orderLockerName = null;
var already = false;

if (!String.prototype.includes) {
    String.prototype.includes = function() {
        'use strict';
        return String.prototype.indexOf.apply(this, arguments) !== -1;
    };
}


$(document).ready(function() {
	
	
	curvyCorners.addEvent(window, 'load', initCorners);
	function initCorners() {
	  var settings = {
		  tl: { radius: 8 },
		  tr: { radius: 8 },
		  bl: { radius: 8 },
		  br: { radius: 8 },
		  antiAlias: true
	  };
	  curvyCorners(settings, "#container");
	  curvyCorners(settings, ".small-box");
	}


	var settings2 = {
	  tl: { radius: 8 },
	  tr: { radius: 8 },
	  antiAlias: true
	};
	curvyCorners(settings2, ".sub-headline");
	

	jQuery("#searchResultsTable tr").click ( 
		function () { 
			$(this).closest("tr").siblings().css("background","#ffffff");
			$(this).css("background","#e1e1e1");
		}
	); 
	 $('#quickSearchButton').click( function() {
	    	
	    	if ( already )
	        {
	            alert("Please wait...we are already processing your request...");
	            return false;
	        }else{
	        	already = true;
		        var $quickSearch= $("#quickSearch").val();
		
		        $.ajax({
		       	   type: 'GET',
		       	   url: 'search.xml?query='+$quickSearch,
		       	   dataType: 'xml',
		       	   
		       	   success: function(data) {
		           	   
		               $(data).find('result').each(function(){
		                   
		                   var $xmlData = $(this);
		                   var $searchError = $xmlData.find('error');
		                   var $orderLink = $xmlData.find('link');
		
		                   if ( $searchError.length ) {
		                       window.alert($searchError.text());
		                   }
		                   else {
		
		                       var html = 'Order '+$quickSearch+' was found.\n\n';
		                       html += 'Please click OK to load';
		                       
		                       //var result = window.confirm( html );
		                       //successCallback( result, $orderLink.text() );
		                       
		                       successCallback( true, $orderLink.text() );
		                   }
		               });  
		               
		       	   },
		       	   
		       	   error: function(msg) {
		       	    //alert("O NOES");
		       	   }
		       	   
		       	});            
	        }
	    	});
	
    $('#nextAssignment').click( function() {
    	
    	if ( already )
        {
            alert("Please wait...we are already processing your request...");
            return false;
        }else{
        	already = true;
        	var newDate = new Date();
        	var newTimestamp = newDate.getFullYear().toString()+newDate.getMonth().toString()+newDate.getDate().toString()+newDate.getHours().toString()+newDate.getMinutes().toString()+newDate.getSeconds();
        	window.location.href = "AgentView.htm?query=showNextOrder&timestamp=" + newTimestamp;
        }	
    	});
    
    $('.slidingDivDLCF').hide();
    $('.slidingDivDLOU').hide();
    $('.slidingDivMessageTxt').hide();
    $('#messageTypeId').change( function() {
    	
        $('#messageTypeId option:selected').each( function () {
        	
        	if( $(this).val() == "Dlcf" ) { 
				  
        		// Expand DIV with additional form fields
        		$('.slidingDivMessageTxt').show();
        		$('.slidingDivDLCF').show();
        		$('.slidingDivDLOU').hide();
        	}
        	else if($(this).val() == "Dlou" ) {
        		
        		$('.slidingDivMessageTxt').show();
        		$('.slidingDivDLCF').hide();
        		$('.slidingDivDLOU').show();
        	}
        	else if($(this).val() != "" ) {
        		
        		$('.slidingDivMessageTxt').show();
        		$('.slidingDivDLCF').hide();
        		$('.slidingDivDLOU').hide();
        	}
        	else  {
        		
        		$('.slidingDivMessageTxt').hide();
        		$('.slidingDivDLCF').hide();
        		$('.slidingDivDLOU').hide();
        	}
        });
    }).trigger('change');
    
    $('.anniversaryType').show();
	$('.birthdayType').show();
	$('.funeralType').show();
	$('.otherType').show();
        
    $('#filterOptions').change( function() {
    	
        $('#filterOptions option:selected').each( function () {
        	
        	if( $(this).val() == "1" ) { 
				  
        		$('.funeralType').show();
        		$('.birthdayType').hide();
        		$('.anniversaryType').hide();
        		$('.otherType').hide();
        		
        		$("#orderCount").html('[' + $('.funeralType').length + ']');
        	}
        	else if($(this).val() == "2" ) {
        		
        		$('.birthdayType').show();
        		$('.funeralType').hide();
        		$('.anniversaryType').hide();
        		$('.otherType').hide();
        		
        		$("#orderCount").html('[' + $('.birthdayType').length + ']');
        	}
        	else if($(this).val() == "3" ) {
        		
        		$('.anniversaryType').show();
        		$('.birthdayType').hide();
        		$('.funeralType').hide();
        		$('.otherType').hide();
        		
        		$("#orderCount").html('[' + $('.anniversaryType').length + ']');
        	}
        	else if($(this).val() == "8" ) {
        		$('.otherType').show();
        		$('.birthdayType').hide();
        		$('.anniversaryType').hide();
        		$('.funeralType').hide();
        		
        		$("#orderCount").html('[' + $('.otherType').length + ']');
        	}
        	
        	else {
        		$('.otherType').show();
        		$('.birthdayType').show();
        		$('.anniversaryType').show();
        		$('.funeralType').show();
        		
        		var total = $('.funeralType').length + $('.birthdayType').length + $('.anniversaryType').length + $('.otherType').length;
        		$("#orderCount").html('[' + total + ']');
        	}
        
        });
    }).trigger('change');
		

	
    $('.slidingDivCommInfo').hide();
    $('input').click( function() {
        	if( $(':checked').val() == "FAX"){ 
        		// Expand DIV with additional form fields
        		$('.slidingDivCommInfo').show();
        	}else{
        		$('.slidingDivCommInfo').hide();
        	}
    });
    
    $('.slidingDivNetwork').hide();
    $('input').click( function() {
        	if( $(':checked').val() == 3 ) { 
        		// Expand DIV with additional form fields
        		$('.slidingDivNetwork').show();
        	}
        	else  {
        		
        		$('.slidingDivNetwork').hide();
        	}

    });
    
    $('.slidingDivBOA').hide();
    $('input').click( function() {
        	if( $(':checked').val() == 2 ) { 
        		// Expand DIV with additional form fields
        		$('.slidingDivBOA').show();
        	}
        	else  {
        		
        		$('.slidingDivBOA').hide();
        	}

    });
    
    
    // Implementation of the AJAX call for the extended search on the order
    // the backing object used is com.bloomnet.bom.mvc.businessobjects.SearchCriteriaExtended    
    $("#quickSearch").val( $quickSearchDefaultValue );
    $("#orderNumber").val( $orderNumberDefaultValue );
    $("#occassion").val( $occassionDefaultValue );
    $("#sendingShop").val( $sendingShopDefaultValue );
    $("#fulfillingShop").val( $fulfillingShopDefaultValue );
    $("#zipCode").val( $zipCodeDefaultValue );
    $("#date").val( $dateDefaultValue );
    $("#date2").val( $date2DefaultValue );
    $("#status").val( $statusDefaultValue );
    


    
    $('#adminSearchButton').click( function() {
    	
    	if ( already )
        {
            alert("Please wait...we are already processing your request...");
            return false;
        }else{
        	
        	already = true;
        	
	    	var $orderNumber    = $("#orderNumber").val();
	    	var $occassion      = $("#occassion").val(); 
	    	var $sendingShop    = $("#sendingShop").val();
	    	var $fulfillingShop = $("#fulfillingShop").val();
	    	var $zipCode        = $("#zipCode").val();
	    	var $date           = $("#date").val();
	    	var $date2          = $("#date2").val();
	    	var $status         = $("#status").val();

	    	
	    	var $params = "?";
	    	
	    	if( $orderNumber    != $orderNumberDefaultValue    ){ $params += "orderNumber="+$orderNumber; } 
	    	if( $occassion      != $occassionDefaultValue      ){ $params += "&occassion="+$occassion; } 
	    	if( $sendingShop    != $sendingShopDefaultValue    ){ $params += "&sendingShop="+$sendingShop; } 
	    	if( $fulfillingShop != $fulfillingShopDefaultValue ){ $params += "&fulfillingShop="+$fulfillingShop; } 
	    	if( $zipCode        != $zipCodeDefaultValue        ){ $params += "&zipCode="+$zipCode; } 
	    	if( $date           != $dateDefaultValue           ){ $params += "&date="+$date; }
	    	if( $date2          != $date2DefaultValue          ){ $params += "&date2="+$date2; }
	    	if( $status         != $statusDefaultValue         ){ $params += "&status="+$status; }

	    	
	        $.ajax({
	        	
	       	   type: 'GET',
	       	   url: 'ManagerViewSearch.xml'+$params,
	       	   dataType: 'xml',
	       	   
	       	   success: function(data) {
	       		   
	       		   $('#orderSearchPane').empty();
	       		   $('#orderSearchCount').empty();
	           	   
	               $(data).find('result').each(function(){
	                   
	                   var $xmlData = $(this);
	                   var $searchError = $xmlData.find('error');
	
	                   if ( $searchError.length ) {
	                	   
	                	   $('#orderStaticCount').show();
	                	   $('#orderStaticPane').show();
	                	   
	                	   $('#orderSearchPane').removeClass('orders-content');
	                	   
	                       window.alert($searchError.text());
	                   }
	                   else {
	
	                	   var html = "";
	                	   
	                	   // reset orders counter
	                	   $('#orderStaticCount').hide();
	                	   $('#orderSearchCount').html( $xmlData.find('count').text() );
	                	   
	                	   // reset order display
	                	  // $('#orderStaticPane').hide();
	
	                	   $xmlData.find('order').each( function(){
	                		   
		   	                   var $order = $(this);
		   	                   html += $order.text();                   
	                	   });
	                	   
	                	   $('#orderStaticPane').addClass('orders-content');
	                	   $('#orderStaticPane').html(html);
	                   }
	               });  
	               
	       	   },
	       	   
	       	   error: function(msg) {
	       	    //alert("O NOES");
	       	   }
	       	   
	       	});            
        }
    	});
    
	
    
    $('#workOrder').click( function() {
    	var orderNum = $('#orderNumber').html();
    	if ( already )
        {
            alert("Please wait...we are already processing your request...");
            return false;
        }else if(orderNum.includes("_OLD")){
        	alert("You have selected to work an archived order. This action is not allowed. Please only work orders that are not archived.");
        }else{
        	already = true;
	    	newDate = new Date();
	    	newTimestamp = newDate.getFullYear().toString()+newDate.getMonth().toString()+newDate.getDate().toString()+newDate.getHours().toString()+newDate.getMinutes().toString()+newDate.getSeconds();
	
	    	window.location.href = "AgentView.htm?" + newTimestamp;
        }
    	});
    
    $('#addFloristButton').click( function() {
    	var $zipCode = $('#orderRecipientZip').val();
    	if($zipCode === "" || $zipCode ==="99999"){
    		alert("Please Change the Zip Code Before Adding a Florist!");
    	}else{
    		var newDate = new Date();
    		var newTimestamp = newDate.getFullYear().toString()+newDate.getMonth().toString()+newDate.getDate().toString()+newDate.getHours().toString()+newDate.getMinutes().toString()+newDate.getSeconds();
    		window.location.href = "AgentViewAddFlorist.htm?timestamp=" + newTimestamp;
    	}
    });
});





function refreshMe() {
	var myPage = parent.location.href;
	
	myString = myPage.toString();
	myString2 = myString;
	var patt=/[\?\&]timestamp=[0-9]+/i;
	var result = myString.match(patt);

	var patt2=/\?/i;

	newDate = new Date();
	newTimestamp = newDate.getFullYear().toString()+newDate.getMonth().toString()+newDate.getDate().toString()+newDate.getHours().toString()+newDate.getMinutes().toString()+newDate.getSeconds();

	if(result != null){ //has timestamp
		var result2 = result.toString().match(patt2);

		if(result2 != null){ //is marked with ?
			myString2 = myString.replace(result.toString(), "?timestamp=" + newTimestamp);
		}
		else { //is marked with &
			myString2 = myString.replace(result.toString(), "&timestamp=" + newTimestamp);
			
		}
	}
	else { // no timestamp
		var result3 = myString.toString().match(patt2);
		if(result3 != null){//has ? in url
			myString2 = myString + "&timestamp=" + newTimestamp;
		}
		else {//no ? in url
			myString2 = myString + "?timestamp=" + newTimestamp;
			
		}
	}
		
		//myPage.replace
	top.location.href = myString2;
	self.parent.modalWindow.close();
}


var modalWindow = {  
    parent:"body",  
    windowId:null,  
    content:null,  
    width:null,  
    height:null,  
    close:function()  {  
    	
        $(".modal-window").remove();  
        $(".modal-overlay").remove();  
    },  
    open:function() {  
        var modal = "";  
        modal += "<div class=\"modal-overlay\"></div>";  
        modal += "<div id=\"" + this.windowId + "\" class=\"modal-window\" style=\"width:" + this.width + "px; height:" + this.height + "px; margin-top:-" + (this.height / 2) + "px; margin-left:-" + (this.width/1.5) + "px;\">";  
        modal += this.content;  
        modal += "</div>";      
  
        $(this.parent).append(modal);  
  
        $(".modal-window").append("<div class=\"close-window\">");  
        $(".close-window").click(function(){modalWindow.close();});  
    }
}; 


var openMyModal = function(source)  {  
	
    modalWindow.windowId = "myModal";  
    modalWindow.width = 510;  
    modalWindow.height = 740;  
    modalWindow.content = "<iframe width='710px' margin-right='150px' height='740px' frameborder='0' scrolling='no' allowtransparency='true' src='" + source + "'></iframe>";  
    modalWindow.open();  
};

var openMyAdminModal = function(source)  {  
	
    modalWindow.windowId = "myModal";  
    modalWindow.width = 840;  
    modalWindow.height = 760;  
    modalWindow.content = "<iframe width='1000px' margin-right='150px' height='760px' frameborder='0' scrolling='no' allowtransparency='true' src='" + source + "'></iframe>";  
    modalWindow.open();  
};

var modalWindowNotes = {  
    parent:"body",  
    windowId:null,  
    content:null,  
    width:null,  
    height:null,  
    close:function()  {  
    	
        $(".modal-windowNotes").remove();  
        $(".modal-overlayNotes").remove();  
    },  
    open:function() {  
        var modal = "";  
        modal += "<div class=\"modal-overlayNotes\"></div>";  
        modal += "<div id=\"" + this.windowId + "\" class=\"modal-windowNotes\" style=\"width:" + this.width + "px; height:" + this.height + "px; margin-top:-" + (this.height / 2) + "px; margin-left:-" + (this.width / 2) + "px;\">";  
        modal += this.content;  
        modal += "</div>";      
  
        $(this.parent).append(modal);  
  
        $(".modal-windowNotes").append("<a class=\"close-windowNotes\"></a>");  
        $(".close-windowNotes").click(function(){modalWindowNotes.close();});  
    }
}; 

var openMyModalNotes = function(source)  {  
	
    modalWindowNotes.windowId = "myModalNotes";  
    modalWindowNotes.width = 510;  
    modalWindowNotes.height = 350;  
    modalWindowNotes.content = "<iframe width='510px' height='350px' frameborder='0' scrolling='no' allowtransparency='true' src='" + source + "'></iframe>";
    modalWindowNotes.open();  
};

var modalWindowEdit = {  
	    parent:"body",  
	    windowId:null,  
	    content:null,  
	    width:null,  
	    height:null,  
	    close:function()  {  
	    	
	        $(".modal-windowEdit").remove();  
	        $(".modal-overlayEdit").remove();  
	    },  
	    open:function() {  
	        var modal = "";  
	        modal += "<div class=\"modal-overlayEdit\"></div>";  
	        modal += "<div id=\"" + this.windowId + "\" class=\"modal-windowEdit\" style=\"width:" + this.width + "px; height:" + this.height + "px; margin-top:-" + (this.height / 2) + "px; margin-left:-" + (this.width / 2) + "px;\">";  
	        modal += this.content;  
	        modal += "</div>";      
	  
	        $(this.parent).append(modal);  
	  
	        $(".modal-windowEdit").append("<a class=\"close-windowEdit\"></a>");  
	        $(".close-windowEdit").click(function(){modalWindowEdit.close();});  
	    }
	}; 

	var openMyModalEdit = function(source)  {  
		
	    modalWindowEdit.windowId = "myModalNotes";  
	    modalWindowEdit.width = 510;  
	    modalWindowEdit.height = 600;  
	    modalWindowEdit.content = "<iframe width='510px' height='600px' frameborder='0' scrolling='no' allowtransparency='true' src='" + source + "'></iframe>";
	    modalWindowEdit.open();  
	};  


function successCallback( result, link ) {

	if ( result ) {

		window.location.href = link;
	}
	else {

		// do nothing
    }
};


function readMessage( messageId ) {
	
	//var $originalMessage = $("#originalMessageText");

    $.ajax({
    	
        type: 'GET',
        url: 'AgentViewReadMessage.xml?query='+messageId,
        dataType: 'xml',
        
        success: function(data) {

            $(data).find('result').each(function(){
                
                var $xmlData = $(this);
                var $searchError = $xmlData.find('error');

                if ( $searchError.length ) {
             	   
                    window.alert( "Could not mark message as read:\n\n"+$searchError.text());
                }
                else {
                	var newDate = new Date();
                	var newTimestamp = newDate.getFullYear().toString()+newDate.getMonth().toString()+newDate.getDate().toString()+newDate.getHours().toString()+newDate.getMinutes().toString()+newDate.getSeconds();
                	window.location.href = "AgentView.htm?" + newTimestamp;
                }
            });  
        },
        
        error: function(msg) {
        	// do nothing
        }
        
     });    
};
// this function is called from searchResults.htm to
// populate a selected order details.
function showOrderDetails( orderId ) {
	
	var query = 'order.json?query='+orderId;
    
    $.getJSON( query, function( json ) {
    	
		$.each( json.results, function( i, order ) {
		   
		   $orderSelected   = orderId;
		   $orderLockerId   = order.lockId;
		   $orderLockerName = order.lockName;
		   
		   var orderNumber = order.orderNumber;
		   var childOrderNumber = order.childOrderNumber;
		   var queue = order.queue;
		   
		   if(childOrderNumber == undefined) childOrderNumber = "";
		   if(queue == undefined) queue = "";
			
		   $("#orderNumber").html( orderNumber );
		   $("#childOrderNumber").html( childOrderNumber );
		   $("#date").html( order.date );
		   $("#total").html( order.total );
		   $("#occasion").html( order.occasion );
		   $("#virtualQueue").html( queue );
		   $("#status").html( order.status ).addClass( 'style="word-wrap: break-word;"' );
		   if(order.status == "Waiting for Time Zone"){$("#status").html( 'Waiting for Time Zone / DLC' ).addClass( 'style="word-wrap: break-word;"' );}
		   $("#viewHistory").show();
		   
		   var html = order.recipient.name+"<br/>"+
		              order.recipient.address1+"<br/>"+
		              order.recipient.address2+"<br/>"+
		              order.recipient.city+", "+order.recipient.state+" "+order.recipient.zip+"<br/>"+ 
		              order.recipient.phone;
		
		   $("#recipientPane").html( html );
		   
		   html = order.sendingShop.name+"<br/>"+
		   		  "("+order.sendingShop.shopCode+")<br/>"+
		          order.sendingShop.address1+"<br/>"+
		          order.sendingShop.address2+"<br/>"+
		          order.sendingShop.city+", "+order.sendingShop.state+" "+order.sendingShop.zip+"<br/>"+ 
		          order.sendingShop.phone;
		   
		   $("#sendingShopPane").html( html );
		   
		   html = order.receivingShop.name+"<br/>"+
		   		  "("+order.receivingShop.shopCode+")<br/>"+
		          order.receivingShop.address1+"<br/>"+
		          order.receivingShop.address2+"<br/>"+
		          order.receivingShop.city+", "+order.receivingShop.state+" "+order.receivingShop.zip+"<br/>"+ 
		          order.receivingShop.phone;
		   
		   $("#receivingShopPane").html( html );
		   
		   html = "<ul>";

		   $.each( order.products, function( i, product ) {
			   
			   //console.log ( product.description );
			   
			   var pel = "<li class='container'>" +
		              		 "<div class='product-image'><img src='images/noimage.PNG' /></div>" +
		              		 "<div class='product-data'>" +
			              		 "<ul class='search-oi-2'>" +
				              		 "<li><strong>Product</strong></li>" +
				              		 "<li>"+product.description+" ( "+product.code+" )</li>" +
				              		 "<li>"+product.cost+"</li>" +
			              		 "</ul>" +
		              		 "</div>" +
		              		 "<div class='clear' />" +
	              		 "</li>";
			   
	            html += pel;
		   });
		   
		   html += "</ul>";
		   
		   $("#productsPane").html( html );
		   
		});
	});
};


function show_alert()
{
alert("I am an alert box!");
}



function displayUser( ) {
	
	var userId = $("#userId").val();
	
	var query = 'user.json?query='+userId;
 
	
 $.getJSON( query, function( json ) {
 	
		$.each( json.results, function( i, user ) {
		   		   
		   var firstName = user.firstName;
		   var lastName = user.lastName;
		   var email = user.email;
		   var password = user.password;
		   var userName = user.userName;
		   var active = user.active;
		  			
		   $("#fname").val( firstName );
		   $("#lname").val( lastName );
		   $("#email").val( email );
		   $("#password").val( password );
		   $("#username").val( userName );	  
		   $("#active").val( active );
		   
		});
	});
};

function displayRoles( ) {
	
	var userId = $("#userId").val();
	
	var query = 'role.json?query='+userId;
 
	
 $.getJSON( query, function( json ) {
 	
		$.each( json.results, function( i, role ) {
		   		   
		   var description = role.description;
		 
		  			
		   $("#roles").html( description );
		  
		   
		});
	});
};

function submitform(){  
	$("#changeStatus").submit();
	}

function workSelectedOrders(){
	var passString = "";
	$('.selectedOrders:checked').each(function() {
	       passString += $(this).val() + ",";
	  });
	
	if(passString === ""){
		
		alert("Please Select One or More Orders to Work");
		return;
	}
	passString = passString.substring(0, passString.length - 1);
	
	var url = "WorkMultipleOrders.htm?ordersString=" + passString;
	window.location.replace(url);

	
}

function moveSelectedOrders(fromWFR){
	var passString = "";
	$('.selectedOrders:checked').each(function() {
	       passString += $(this).val() + ",";
	  });
	
	if(passString === ""){
		
		alert("Please Select One or More Orders to Move");
		return;
	}
	passString = passString.substring(0, passString.length - 1);
	if(fromWFR == true) passString += "&fromWFR=true";
	else if(fromWFR === 'automated') passString += "&fromAutomated=true";
	var url = "MoveMultipleOrders.htm?ordersString=" + passString;
	window.location.replace(url);

	
}

function getOrdersByState(){
	
	var newDate = new Date();
	
	var newTimestamp = newDate.getFullYear().toString()+newDate.getMonth().toString()+newDate.getDate().toString()+newDate.getHours().toString()+newDate.getMinutes().toString()+newDate.getSeconds();

	window.location.href = "outstandingbystate.htm?stateName=" +$("#stateSelection").val() + "&timestamp=" + newTimestamp;
	
}

function confirmCoverageRemoval(zip, shopName){
	var r = confirm("Press OK to remove " + shopName + " from covering the zip code " + zip );
	if(r == true){
		var newDate = new Date();
		var newTimestamp = newDate.getFullYear().toString()+newDate.getMonth().toString()+newDate.getDate().toString()+newDate.getHours().toString()+newDate.getMinutes().toString()+newDate.getSeconds();
		window.location.href = "AgentViewRemoveShopCoverage.htm?timestamp=" + newTimestamp;
	}
}

function filterItems(obj){
	$(".all").hide();
	$("."+$(obj).val()+"").show();
	$("#orderCount").html("["+$("."+$(obj).val()).length+"]");
	if($(obj).val() === "default"){
		$(".all").show();
		$("#orderCount").html("["+$(".all").length+"]");
	}
	
	var currVal = $(obj).val();
	
	$("#recipientSelect").val($('option:first', this).val());
	$("#sendingShopSelect").val($('option:first', this).val());
	$("#deliverySelect").val($('option:first', this).val());
	$("#citySelect").val($('option:first', this).val());
	$("#stateSelect").val($('option:first', this).val());
	$("#zipSelect").val($('option:first', this).val());
	
	$(obj).val(currVal);
	
	
}


function sortTable(row) {
	  var table, rows, switching, i, x, y, shouldSwitch;
	  table = document.getElementById("searchResultsTable");
	  switching = true;
	  /* Make a loop that will continue until
	  no switching has been done: */
	  while (switching) {
	    // Start by saying: no switching is done:
	    switching = false;
	    rows = table.rows;
	    /* Loop through all table rows (except the
	    first, which contains table headers): */
	    for (i = 1; i < (rows.length - 1); i++) {
	      // Start by saying there should be no switching:
	      shouldSwitch = false;
	      /* Get the two elements you want to compare,
	      one from current row and one from the next: */
	      x = rows[i].getElementsByClassName(row)[0];
	      y = rows[i + 1].getElementsByClassName(row)[0];
	      // Check if the two rows should switch place:
	      if (x.innerHTML.toLowerCase() > y.innerHTML.toLowerCase()) {
	        // If so, mark as a switch and break the loop:
	        shouldSwitch = true;
	        break;
	      }
	    }
	    if (shouldSwitch) {
	      /* If a switch has been marked, make the switch
	      and mark that a switch has been done: */
	      rows[i].parentNode.insertBefore(rows[i + 1], rows[i]);
	      switching = true;
	    }
	  }
	}

function confirmExit(){
	var ask = window.confirm("Are you sure you want to exit ALL of your actively locked orders?");
    if (ask) {
        window.location.href = "AgentViewExitMultipleOrders.htm";
    }
}

	
	


