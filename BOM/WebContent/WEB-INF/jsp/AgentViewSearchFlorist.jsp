<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
    <body>
        <form:form method="post" commandName="formShop">
        <div id="container">
            <div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
            </div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
        <c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
        </c:import>
        </div>
        <div class="sidebar">
        <div class="headline">You Are Searching for a florist</div>
        <!-- Close DIV headline -->
        <div class="sidebar-content">
            <div class="florist-list2" align="right">
                Shop Code*: <form:input path="shopCode" /><br /><br />
                               <br />
                <!-- Close DIV send-order -->
            </div>
        </div>
        <!-- Close DIV sidebar-content -->
        <div class="sidebar-footer">
            <form:input type="hidden" path="coveredZip" value="${ MASTERORDER.bean.recipientZipCode }" />
            <input type="image" src="images/add-florist-small.png" alt="Add Florist" class="bottom-buttons" onclick="this.disabled=true,this.form.submit();" /></a>
        </div>
        <!-- Close DIV sidebar-footer -->
        <center>
        <font face="Arial" size="2" color="red">
          <spring:bind path="webAppBomorder.*">
            <c:if test="${status.errors.errorCount > 0}">
              <c:forEach var="error" items="${status.errors.allErrors}">
                <spring:message message="${error}"></spring:message><br />
              </c:forEach>
            </c:if>
          </spring:bind>
        </font>
        </center>
        </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>