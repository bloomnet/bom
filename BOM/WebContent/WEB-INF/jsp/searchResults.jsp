<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
  <body>
	<div id="container">
		<div id="header">
	      <c:import url="/WEB-INF/jsp/includes/header.jsp" />				
		</div> <!-- Close DIV header -->
		<div class="content-container">
		    <div class="manager-area">
				<div class="headline">
					Search &nbsp; &nbsp; <span>Filter: <select id="filterOptions"><option>-- SELECT FILTER --</option><option value="1">Funeral Orders</option><option value="2">Birthday Orders</option><option value="3">Anniversary Orders</option><option value="8">Other Orders</option></select></span>
					&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; <span>Sort Columns By: </span> <select onchange="sortTable(this.value)"><option>-- SELECT SORTING --</option><option value="orderColumn">Order Number</option><option value="recipientColumn">Recipient</option><option value="shopColumn">Sending Shop</option><option value="deliveryColumn">Delivery</option><option value="cityColumn">City</option><option value="stateColumn">State</option><option value="zipColumn">Zip</option></select></span>
				  <div class="clear"></div>
				</div> <!-- Close DIV headline -->
				<div class="manager-all-orders">
				  <div class="sub-headline" >
						Your Results <span id="orderCount">[<c:out value="${ ordersCount }"/>]</span>
				  </div><!-- Close DIV sub-headline -->
				  <c:set var="containsHyperlinkaccess" value="false" scope="page" />
				  <c:set var="containsMoveAccess" value="false" scope="page" />
					<c:forEach var="item" items="${roles}">
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(item.key,'moaccess') }">
								<c:set var="containsMultiOrderAccess" value="true" scope="page" />
							</c:when>
						</c:choose>
						<c:choose>
							<c:when test="${fn:containsIgnoreCase(item.key,'wfrOrders') }">
								<%if(request.getAttribute("noCheckBoxes") == null){ %>
									<c:set var="containsMoveAccess" value="true" scope="page" />
								<%}else{ %>
									<c:set var="containsMoveAccess" value="false" scope="page" />
								<%} %>
							</c:when>
						</c:choose>
					</c:forEach>
				  <div class="search-table-container" >
			        <table class="search-table" id="searchResultsTable">
				  	  
				  	   <tr>
				  	   <c:choose>
						<c:when test='${containsMultiOrderAccess == "true"}'>
							<%if(request.getParameter("stateName") != null){ %>
								<td class="col1 header"></td>
							<%} %>
						</c:when>
					  </c:choose>
					  <c:choose>
						<c:when test='${containsMoveAccess == "true"}'>
							<%if(request.getParameter("stateName") == null){ %>
								<td class="col1 header"></td>
							<%} %>
						</c:when>
					  </c:choose>
					  					
			            <td class="col1 header" ><div style="width: 60px; overflow:hidden;">Order#</div></td>
			            <td class="col2 header"><div style="width:65px;"><select style="width:65px;" id="recipientSelect" onchange="filterItems(this);">
				            <c:forEach var="order" items="${ orders }" varStatus="status" >
				    	  	<c:set var="index1" value="${fn:indexOf(order.orderXml, '<recipientLastName>')}" />
		          		  	<c:set var="index2" value="${fn:indexOf(order.orderXml, '</recipientLastName>')}" />
		          		  	<option value="<c:out value="${fn:replace(fn:replace(fn:replace(fn:replace(fn:substring(order.orderXml, index1+19, index2),' ','_'),'.',''),',',''),'&amp;','')}"/>"><c:out value="${fn:replace(fn:substring(order.orderXml, index1+19, index2),'&amp;','&')}"/></option>
		          		  	</c:forEach>
			            </select></div></td>
			            <td class="col3 header"><div style="width:100px;"><select style="margin-left: -3px; width:110px;" id="sendingShopSelect" onchange="filterItems(this);">
				            <c:forEach var="order" items="${ orders }" varStatus="status" >
	          		  	<option value="<c:out value="${ fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(order.shopBySendingShopId.shopName,' ','_'),'.',''),'\\'',''),'|',''),'/',''),',',''),'&amp;','') }"/>"><c:out value="${ fn:replace(order.shopBySendingShopId.shopName,'&amp;','&') }"/></option>
	          		  	</c:forEach>
			            </select></div></td>
			            <td class="col4 header"><div style="width:65px;"><select style="width:72px;" id="deliverySelect" onchange="filterItems(this);">
				            <c:forEach var="order" items="${ orders }" varStatus="status" >
		          		  	<option value="<fmt:formatDate value="${order.deliveryDate }" pattern="MMddyy" />"><fmt:formatDate value="${ order.deliveryDate }" pattern="MM/dd/yy" /></option>
		          		  	</c:forEach>
			            </select></div></td>
			            <td class="col5 header"><div style="width:90px;"><select style="width:90px;" id="citySelect" onchange="filterItems(this);">
			             	<c:forEach var="order" items="${ orders }" varStatus="status" >
		          		  	<option value="<c:out value="${ fn:replace(order.city.name,' ','_') }"/>"><c:out value="${ order.city.name }"/></option>
		          		  	</c:forEach>
			            </select></div></td>
			            <td class="col6 header"><div style="width:35px;"><select style="width:55px; margin-left: -7px;" id="stateSelect" onchange="filterItems(this);">
			            	<c:forEach var="order" items="${ orders }" varStatus="status" >
		          		  	<option value="<c:out value="${ order.city.state.shortName }"/>"><c:out value="${ order.city.state.shortName }"/></option>
		          		  	</c:forEach>
			            </select></div></td>
			            <td class="col6 header"><div style="width:43px;"><select style="width:43px;" id="zipSelect" onchange="filterItems(this);">
			            	<c:forEach var="order" items="${ orders }" varStatus="status" >
		          		  	<option value="<c:out value="${ order.zip.zipCode }"/>"><c:out value="${ order.zip.zipCode }"/></option>
		          		  	</c:forEach>
			            </select></div></td>
			            <script type="text/javascript">
				            var optionValues =[];
				            $('#recipientSelect option').each(function(){
				               if($.inArray(this.value, optionValues) >-1){
				                  $(this).remove()
				               }else{
				                  optionValues.push(this.value);
				               }
				            });
				            $("#recipientSelect").html($('#recipientSelect option').sort(function(x, y) {        
				                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
				           }));
				           $("#recipientSelect").get(0).selectedIndex = 0;
				           $("#recipientSelect").prepend("<option value=\"default\">Name</option>");
				           $("#recipientSelect").val($('option:first',this).val());
				            optionValues =[];
				            $('#sendingShopSelect option').each(function(){
				               if($.inArray(this.value, optionValues) >-1){
				                  $(this).remove()
				               }else{
				                  optionValues.push(this.value);
				               }
				            });
				            $("#sendingShopSelect").html($('#sendingShopSelect option').sort(function(x, y) {        
				                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
				           }));
				           $("#sendingShopSelect").get(0).selectedIndex = 0;
				           $("#sendingShopSelect").prepend("<option value=\"default\">Sending Shop</option>");
				           $("#sendingShopSelect").val($('option:first',this).val());
				            optionValues =[];
				            $('#deliverySelect option').each(function(){
				               if($.inArray(this.value, optionValues) >-1){
				                  $(this).remove()
				               }else{
				                  optionValues.push(this.value);
				               }
				            });
				            $("#deliverySelect").html($('#deliverySelect option').sort(function(x, y) {        
				                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
				           }));
				           $("#deliverySelect").get(0).selectedIndex = 0;
				           $("#deliverySelect").prepend("<option value=\"default\">Delivery</option>");
				           $("#deliverySelect").val($('option:first',this).val());
				            optionValues =[];
				            $('#citySelect option').each(function(){
				               if($.inArray(this.value, optionValues) >-1){
				                  $(this).remove()
				               }else{
				                  optionValues.push(this.value);
				               }
				            });
				            $("#citySelect").html($('#citySelect option').sort(function(x, y) {        
				                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
				           }));
				           $("#citySelect").get(0).selectedIndex = 0;
				           $("#citySelect").prepend("<option value=\"default\">City</option>");
				           $("#citySelect").val($('option:first',this).val());
				            optionValues =[];
				            $('#stateSelect option').each(function(){
				               if($.inArray(this.value, optionValues) >-1){
				                  $(this).remove()
				               }else{
				                  optionValues.push(this.value);
				               }
				            });
				            $("#stateSelect").html($('#stateSelect option').sort(function(x, y) {        
				                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
				           }));
				           $("#stateSelect").get(0).selectedIndex = 0;
				           $("#stateSelect").prepend("<option value=\"default\">State</option>");
				           $("#stateSelect").val($('option:first',this).val());
				            optionValues =[];
				            $('#zipSelect option').each(function(){
				               if($.inArray(this.value, optionValues) >-1){
				                  $(this).remove()
				               }else{
				                  optionValues.push(this.value);
				               }
				            });
				            $("#zipSelect").html($('#zipSelect option').sort(function(x, y) {        
				                return $(x).text().toUpperCase() < $(y).text().toUpperCase() ? -1 : 1;
				           }));
				           $("#zipSelect").get(0).selectedIndex = 0;
				           $("#zipSelect").prepend("<option value=\"default\">Zip</option>");
				           $("#zipSelect").val($('option:first',this).val());
				           </script>
			            <td class="col8 header">&nbsp;</td>
		            </tr>
			    	  <c:forEach var="order" items="${ orders }" varStatus="status" >
			    	  <%try{ %>
			    	  <c:set var="index1" value="${fn:indexOf(order.orderXml, '<recipientLastName>')}" />
	          		  <c:set var="index2" value="${fn:indexOf(order.orderXml, '</recipientLastName>')}" />
				  	  <c:set var="index3" value="${fn:indexOf(order.orderXml, '<occasionCode>')}" />
	        		  <c:set var="index4" value="${fn:indexOf(order.orderXml, '</occasionCode>')}" />			
	        		  <c:set var="index5" value="${fn:substring(order.orderXml, index3+14, index4)}" />
	        		  		          
			          <c:choose>
			          		<c:when test="${ index5 == 1 }">
			            		<tr class = "all funeralType 
			          		</c:when>
			          		<c:when test="${ index5 == 3 }">
			            		<tr class = "all birthdayType 
			          		</c:when>
			          		<c:when test="${ index5 == 7 }">
			          			<tr class = "all anniversaryType 
			          		</c:when>
			          		<c:otherwise>
			          			<tr class = "all otherType 
			          		</c:otherwise>
	          			</c:choose>
	          			
	          			<c:out value="${ order.orderNumber }"/> <c:out value="${fn:replace(fn:replace(fn:replace(fn:replace(fn:substring(order.orderXml, index1+19, index2),' ','_'),'.',''),',',''),'&amp;','')}"/> <c:out value="${ fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(fn:replace(order.shopBySendingShopId.shopName,' ','_'),'.',''),'\\'',''),'|',''),'/',''),',',''),'&amp;','') }"/> <fmt:formatDate value="${ order.deliveryDate }" pattern="MMddyy" /> <c:out value="${ fn:replace(order.city.name,' ','_') }"/> <c:out value="${ order.city.state.shortName }"/> <c:out value="${ order.zip.zipCode }"/>">	
	    	          
			          <c:choose>
						<c:when test='${containsMultiOrderAccess == "true"}'>
							<%if(request.getParameter("stateName") != null){ %>
								<td><input type="checkbox" value="<c:out value="${ order.bomorderId }"/>" class="selectedOrders"/></td>
							<%} %>
						</c:when>
					  </c:choose>
					   <c:choose>
						<c:when test='${containsMoveAccess == "true"}'>
							<%if(request.getParameter("stateName") == null){ %>
								<td><input type="checkbox" value="<c:out value="${ order.orderNumber }"/>" class="selectedOrders"/></td>
							<%} %>
						</c:when>
					  </c:choose>
						  
	        
	          <c:choose>
	          <c:when test="${ fn:length(order.orderNumber) > 8 }">
	          		
	          		<td class="orders-content-col1" title="<c:out value="${ order.orderNumber }"/>"  ><div style="width: 60px; overflow:hidden;"><c:out value="${ fn:substring(order.orderNumber,0,7)}"/>...</div></td>

	          </c:when>
	          <c:otherwise>
	          
		      		<td class="orders-content-col1 orderColumn" ><div style="width: 60px; overflow:hidden;"><c:out value="${ order.orderNumber }"/></div></td>

	          </c:otherwise>
	          </c:choose>	          				        				
				            <td class="search-content-col2 recipientColumn"><div style="width:65px; overflow:hidden;"><c:out value="${fn:replace(fn:substring(order.orderXml, index1+19, index2),'&amp;','&')}"/></div></td>
				            <td class="search-content-col3 shopColumn"><div style="width: 100px; overflow:hidden;"><c:out value="${ fn:replace(order.shopBySendingShopId.shopName,'&amp;','&') }"/></div><%-- &nbsp;( Shopcode should be here ) --%></td>
				            <td class="search-content-col5 deliveryColumn"><div style="width: 65px; overflow:hidden;"><fmt:formatDate value="${ order.deliveryDate }" pattern="MM/dd/yy" /></div></td>
				            <td class="search-content-col6 cityColumn"><div style="width: 90px; overflow:hidden;"><c:out value="${ order.city.name }"/></div></td>
				            <td class="search-content-col6 stateColumn"><div style="width: 35px; overflow:hidden;"><c:out value="${ order.city.state.shortName }"/></div></td>
				            <td class="search-content-col6 zipColumn"><div style="width: 43px; overflow:hidden;"><c:out value="${ order.zip.zipCode }"/></div></td>
				         <!--  <td class="search-content-col5"><fmt:formatNumber value="${ bean.totalCost }" type="CURRENCY" /></td>-->
				            <td class="search-content-col7"><div style="width: 45px; overflow:hidden;"><a href='#' onclick='showOrderDetails( <c:out value="${ order.bomorderId }"/> )'>Details</a></div></td>
				          </tr>
				       <%}catch(Exception ee){ee.printStackTrace();} %>
					  </c:forEach>
					</table>
				  </div> <!-- Close DIV order-table-container -->
				</div> <!-- Close DIV manager-all-orders -->
		        <div class="clear"></div> <!-- Clear Floats -->
		    </div><!-- Close DIV manager-area -->
	        <div class="search-sidebar">
		        <div class="headline">
					Order Information
		        </div> <!-- Close DIV headline -->
	        <div class="order-search">
	          <ul class="search-oi">
	            <li class="l"><strong>Order #:</strong></li>
	            <li class="c"><strong>Delivery Date:</strong></li>
	            <li class="r"><strong>Status:</strong></li>
	          </ul>
	          <ul class="search-oi">
	            <li class="l">
		            <div id="orderNumber">&nbsp;</div>
	              	<div id="viewHistory" style="display: none;">
		        		<a href="#" onclick="openMyModal('orderHistory.htm?query=masterorder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;">View History</a>

		        	</div>
	            </li>
	            <li class="c"><div id="date">&nbsp;</div></li>
	            <li class="r"><div style="word-wrap: break-word; width: 60px" id="status">&nbsp;</div></li>
	          </ul>
	          <ul class="search-oi">
	            <li class="l"><strong>Current Child:</strong></li>
	            <li class="c">&nbsp;</li>
	            <li class="r"><strong>Queue:</strong></li>
	          </ul>
	          <ul class="search-oi">
	            <li class="l"><div id="childOrderNumber">&nbsp;</div></li>
	            <li class="c">&nbsp;</li>
	            <li class="r"><div id="virtualQueue">&nbsp;</div></li>
	          </ul>
	          <div class="clear hr"></div> <!-- Clear Floats -->
	          <ul class="search-oi-2">
	            <li><strong>Recipient:</strong></li>
	        <li><strong>Occasion:</strong></li>

	          </ul>
	          <ul class="search-oi-2">
	            <li><div style="height: 72px" id="recipientPane" /></li>
	        <li><div id="occasion"/></li>

	          </ul>
	          <div class="clear hr"></div> <!-- Clear Floats -->
	          <ul class="search-oi-2">
	            <li><strong>Sending Shop:</strong></li>
	            <li><strong>Fulfilling Shop:</strong></li>
	          </ul>
	          <ul class="search-oi-2">
	            <li>
	              <div style="height: 93px" id="sendingShopPane" />
	            </li>
	            <li>
	              <div style="height: 93px" id="receivingShopPane" />
				</li>
	          </ul>
	          <div class="clear"></div> 
	          <!-- Clear Floats -->
	        </div>
	        <div class="headline">Order Details</div> 
	        <div class="agents-logged-in">
        		<div class="odetails-b" id="productsPane" />
	        </div><!-- Close DIV agents-logged-in -->
	        <div class="search-sidebar-footer">
	          <a href="#" id="workOrder"><img src="images/work-order.png" alt="Work Order" class="next-order" /></a>
	        </div> <!-- Close DIV search-sidebar-footer -->
	        </div> <!-- Close DIV search-sidebar -->
			<div class="clear" /> <!-- Clear Floats -->
		</div> <!-- Close DIV content-container -->
		<div class="content-footer">
		</div> <!-- Close DIV content-footer -->
		<div id="footer" ></div><!-- Close DIV footer -->
	</div> <!-- Close DIV container -->	
	<c:choose>
		<c:when test='${containsMultiOrderAccess == "true"}'>
			<%if(request.getParameter("stateName") != null){ %>
				<a href="#" onclick="workSelectedOrders();">Work Selected Orders</a> &nbsp;&nbsp;&nbsp;&nbsp;
			<%} %>
		</c:when>
	</c:choose>
	<c:choose>
		<c:when test='${containsMoveAccess == "true"}'>
			<%if(request.getAttribute("fromWFR") != null){ %>
				<a href="#" onclick="moveSelectedOrders(true);">Move Selected Orders To TLO Queue</a>
			<%}else if(request.getAttribute("fromAutomated") != null){ %>
				<a href="#" onclick="moveSelectedOrders('automated');">Move Selected Orders To TLO Queue</a>
			<%}else{ %>
				<a href="#" onclick="moveSelectedOrders();">Move Selected Orders To TLO Queue</a>
			<%} %>
		</c:when>
	</c:choose>
  </body>
</html>