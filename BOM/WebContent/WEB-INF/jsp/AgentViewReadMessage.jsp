<%@ page language="java" contentType="text/xml" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<result>
<c:choose>
  <c:when test="${ not empty error }">
    <error><c:out value="${ error }" /></error>
  </c:when>
</c:choose>
</result>