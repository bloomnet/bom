<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
<body onload="javascript:refreshMe();">
	<c:choose>
	<c:when test='${ fn:containsIgnoreCase( nextView, "closeHistory" ) }'>
	<center>
	No order history is available at this time<br /><br />
	<a href="#" onclick="self.parent.modalWindow.close(); return false;">Close</a>
	</center>
	</c:when>
	<c:otherwise>
    The order has been updated<br /><br />
    <a href="#" onclick="self.parent.modalWindow.close(); return false;">Close</a>
	</c:otherwise>
</c:choose>
</body>
</html>