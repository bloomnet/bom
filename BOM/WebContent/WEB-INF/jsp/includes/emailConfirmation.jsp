<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<form:form method="post" commandName="formOrder">
<div class="headline">Order Confirmation</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="order-confirmation">
		<ul>
			<li>
				<strong>Email Address for Order Confirmation</strong>
			</li>
			<li>
			  <form:input path="emailConfirmation" name="email-confirmation" type="text" class="email-confirmation" /></li>
			<li>
<!-- 			<div class="send-confirmation">
					<a href="#"><img src="images/send-confirmation.png" alt="Send Email Confirmation" /></a>
				</div>
 -->
			</li>
		</ul>
	</div>
	<!-- Close DIV order-confirmation -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer">
	<input type="image" src="images/next-btn.png" alt="Exit Order" value="submit" class="exit-order" />
</div>
<!-- Close DIV sidebar-footer -->
</form:form>
