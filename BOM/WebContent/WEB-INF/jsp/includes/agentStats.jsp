<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">


	<c:set var="orders" scope="page" value="${orders}" />
	<c:set var="touched" scope="page" value="${touched}" />
	<c:set var="lastlogin" scope="page" value="${lastlogin}" />
	<c:set var="workTime" scope="page" value="${workTime}" />
	
	

<div class="headline">Orders Completed Today</div>
<br />
 <div class="admin-list">
    <ol type="1">
		<c:forEach var="order" items="${orders}">
		 <li>
			Order Number: <c:out value="${order.id.parentOrderNumber}" /><br />
			Child Order Number: <c:out value="${order.id.childOrderNumber}" /><br />
			Entered Date: <fmt:formatDate value="${ order.id.parentOrderDate }" pattern="MM-dd-yy hh:mm:ss" /><br />
			Delivery Date: <fmt:formatDate value="${ order.id.deliveryDate }" pattern="MM-dd-yy hh:mm:ss" /><br />
			Route: <c:out value="${order.id.route}" /><br />
			Status: <c:out value="${order.id.status}" /><br /><br />
						<hr />
			
			</li>
			
		</c:forEach>
		</ol>
		
		
		<br />

<div class="headline">Orders Touched Today</div>
	<br />
		
		<ol type="1">
		<c:forEach var="touch" items="${touched}">
		 <li>
			Order Number: <c:out value="${touch.id.parentOrderNumber}" /><br />
			Child Order Number: <c:out value="${touch.id.childOrderNumber}" /><br />
			Entered Date: <fmt:formatDate value="${ touch.id.parentOrderDate }" pattern="MM-dd-yy hh:mm:ss" /><br />
			Delivery Date: <fmt:formatDate value="${ touch.id.deliveryDate }" pattern="MM-dd-yy hh:mm:ss" /><br />
			Route: <c:out value="${touch.id.route}" /><br />
			Status: <c:out value="${touch.id.status}" /><br /><br />
						<hr />
			
			</li>
			
		</c:forEach>
		</ol>
		
		<br />

<div class="headline">Time Worked Today</div>
<br />
<c:out value="${workTime}" />
		
		<br /><br />

	<div class="headline">Last logged in</div>
	<br />
		
		
		<fmt:formatDate value="${ lastlogin }" pattern="MM-dd-yy hh:mm:ss" />
		<br />
		
		</div>
