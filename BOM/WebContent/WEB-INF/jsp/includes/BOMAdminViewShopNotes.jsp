<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<c:set var="shopNotes" scope="session" value="${MASTERORDER.shopBySendingShopId.shopNotes}" />
</head>

<body>
<br />

	<table border="0">
		<tr>
			<th>Shop Notes</th>
		</tr>
		</table><br />
 	<div style="text-align: left; height: 550px; overflow: auto;">
	  <c:forEach var="m" items="${shopNotes}" >
		<ul>
			<li>
				<strong>User: <c:out value="${ m.user.userName }" /></strong>
				<br />
				<strong>Date: <c:out value="${ m.formattedDate }" /></strong>
				<br />
				<strong>Note: <c:out value="${ m.shopNote }" /></strong>
				<br /><br />
			</li>
		</ul>
	  </c:forEach>	
	  </div>

	
</body>

</body>
</html>