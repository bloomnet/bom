<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BOM Admin Page</title>
</head>

<body>
<br/>
<br/>
<br/>
<table border="0" cellpadding="10" cellspacing="10">
	<form:form method="post" commandName="user">
     <tr> 
     <th>Billing Report</th>
      </tr> <tr>
 <td>Start Date:<form:input path="selectedDate"
			class="reply-message-text" /></td>
			</tr>
			<tr>
 <td>End Date:<form:input path="endDate"
			class="reply-message-text" /></td>
			</tr>
			<tr>
			
<tr>	
	<td>
		<input type="image" src="images/btn-send.png" name="submit" value="submit" class="bottom-buttons" />
	</td>
</tr>
	</form:form>
		<br />
	<tr>
		<td>

			<%if(request.getAttribute("downloadFile") != null){ %>
				<a href="<%="/images/"+request.getAttribute("downloadFile") %>"> Download Report </a>
			<%} %>

		
		</td>
	 </tr>
	
	</table>
	<br />
</body>
</html>