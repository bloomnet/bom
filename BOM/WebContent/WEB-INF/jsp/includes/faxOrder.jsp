<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>

<div class="headline">Send Order</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="send-order-fax">
		<ul>
			<li><strong>FAX</strong>
			</li>
			<li><a href="#">Florist Expressions (U831000)</a>
			</li>
			<li>
				<form>
					<input type="text" disabled="disabled" name="phone1" value="212"
						class="phone-1" /> <input type="text" disabled="disabled"
						name="phone2" value="555" class="phone-2" /> <input type="text"
						disabled="disabled" name="phone3" value="1212" class="phone-3" />
				</form></li>
		</ul>
	</div>
	<!-- Close DIV send-order-fax -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer"></div>
<!-- Close DIV sidebar-footer -->
