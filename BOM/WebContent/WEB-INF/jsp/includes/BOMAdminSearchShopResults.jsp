<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BOM Admin Page</title>
</head>

<body>
<c:set var="shop" scope="page" value="${SHOP_RESULTS}" />
<br/>
<br/>
<br/>
<table border="0" cellpadding="10" cellspacing="10">
     <tr> 
     <th> Shop results: [<c:out value="${ ordersCount }"/>]</th>
      </tr>
	</table>
	<br />
	
	<div class="admin-list">
    <ol type="1">
		<c:forEach var="shop" items="${SHOP_RESULTS}">
		 <li>
		    Shop Name: <c:out value="${shop.shopName}" /><br />
			Shop Code : <c:out value="${shop.shopCode}" /><br />
			Shop Phone: <c:out value="${shop.shopPhone}" /><br />
			Covered Zip: <c:out value="${ shop.coveredZip }"  /><br /><br />
						<hr />
			
			</li>
			
		</c:forEach>
		</ol>
		</div>
	



</body>
</html>