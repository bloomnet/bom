<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<div class="headline">&nbsp;</div>
<div class="sidebar-content">
<c:set var="action" scope="session" value="${MASTERORDER.actionStr}" />

<c:choose>

	<c:when test="${action == 'send'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminSendOrder.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'unlock'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminUnlockOrder.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'addUser'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminAddUser.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'ccReport'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminCCReport.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'activityReport'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminActivityReports.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'dailyReport'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminDailyReports.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'billingReport'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminBillingReport.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'telefloraReport'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminTelefloraReport.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'blockShop'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminBlockShop.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'editUser'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminEditUser.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'editTFSIQueue'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminEditTFSIQueue.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'editTLOQueue'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminEditTLOQueue.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'editAutomatedOnlyQueue'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminEditAutomatedOnlyQueue.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'addRole'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminAddRole.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'updateOrderStatus'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminUpdateOrderStatus.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'history'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminOrderHistory.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'vieworderhistory'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminOrderHistory.jsp">
		</c:import>
	</c:when>
	
	
	<c:when test="${action == 'vieworders'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminTLOOrders.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'frozen'}">
		<c:import url="/WEB-INF/jsp/includes/frozenOrders.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'messages'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminMessages.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'viewmessages'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminViewMessages.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'search'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminSearchShop.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'searchresults'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminSearchShopResults.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'shopnotes'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminShopNotes.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'viewshopnotes'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminViewShopNotes.jsp">
		</c:import>
	</c:when>
	
	<c:when test="${action == 'bmsRetailReport'}">
		<c:import url="/WEB-INF/jsp/includes/BOMAdminBMSRetailReport.jsp">
		</c:import>
	</c:when>

	<c:otherwise>
		<c:import url="/WEB-INF/jsp/includes/BOMAdmindefaultSidebar.jsp">
		</c:import>
	</c:otherwise>

</c:choose>
</div>
<!-- Close DIV sidebar-content -->

