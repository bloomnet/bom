<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ page import ="java.util.Date" %>
<div class="headline" ><!-- Close DIV outbound -->
		&nbsp; Administrator View
  		<div class="clear"></div>
	</div> 
	<!-- Close DIV headline -->
<div class="admin-all-orders">
	<br />
	<div class="order-boxes">
    <div class="admin-small-box"><table border="1" width="300">
					<tr style="background-color: #80a66f; color: white;">
						<th>Order Management&nbsp;</th>
					</tr>
					<tr>
						<td><a href="BOMAdminSendOrder.htm?query=BOMAdminSendOrder?timestamp=<%=new Date().getTime()%>">
								Send Order to Queues</a>
						</td>
					</tr>
					<tr>
						<td><a
							href="BOMAdminUnlockOrder.htm?query=BOMAdminUnlockOrder?timestamp=<%=new Date().getTime()%>">
								Unlock Order</a></td>
					</tr>
					<tr>
						<td><a href="BOMAdminOrderHistory.htm?timestamp=<%=new Date().getTime()%>"> View Order history </a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminUpdateOrderStatus.htm?timestamp=<%=new Date().getTime()%>"> Edit Order Status </a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminTLOOrders.htm#?timestamp=<%=new Date().getTime()%>"> View TLO Orders Being Worked </a>
						</td>
					</tr>
					<tr>
						<td><a href="#" onclick="openMyAdminModal('outstanding.htm?query=masterorder&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />'); return false;"> View Outstanding Orders</a>
						</td>
					</tr>
					<tr>         
						<td><a href="frozenOrders.htm#?timestamp=<%=new Date().getTime()%>">  View Outstanding Automated Orders</a>
						</td>
					</tr>
					<tr>         
						<td><a href="BOMAdminMessages.htm#?timestamp=<%=new Date().getTime()%>">  View Messages On Order</a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminTelefloraReport.htm?timestamp=<%=new Date().getTime()%>"> Download BOM Teleflora Report</a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminBMSRetailReport.htm?timestamp=<%=new Date().getTime()%>"> Download BMS Retail Report</a>
						</td>
					</tr>
					<tr>         
						<td><a href="BOMAdminBillingReport.htm#?timestamp=<%=new Date().getTime()%>">  Generate Billing Report</a>
						</td>
					</tr>
					<tr>         
						<td><a href="BOMAdminCCReport.htm#?timestamp=<%=new Date().getTime()%>">  Generate CC Report</a>
						</td>
					</tr>
					<tr>         
						<td><a href="BOMActivityReports.htm#?timestamp=<%=new Date().getTime()%>">  Generate User Activity Reports</a>
						</td>
					</tr>
					<tr>         
						<td><a href="BOMDailyReports.htm#?timestamp=<%=new Date().getTime()%>">  Generate Order Reports</a>
						</td>
					</tr>
					<tr>         
						<td><a href="BOMAdminBlockShop.htm#?timestamp=<%=new Date().getTime()%>">Block/Unblock Shop</a>
						</td>
					</tr>
					<tr>         
						<td><a href="BOMAdminShowShopNotes.htm#?timestamp=<%=new Date().getTime()%>">View Shop Notes</a>
						</td>
					</tr>
				</table>
				</div>
			
				<table border="1" width="300">
					<tr style="background-color: #80a66f; color: white;">
						<th>User Management&nbsp;</th>
					</tr>
					<tr>
						<td><a href="BOMAdminAddUser.htm?query=BOMAdminSendOrder?timestamp=<%=new Date().getTime()%>"> Add User</a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminEditUser.htm?query=BOMAdminSendOrder?timestamp=<%=new Date().getTime()%>"> Edit User</a> 
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminAddRole.htm?query=BOMAdminSendOrder?timestamp=<%=new Date().getTime()%>"> Add Role</a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminEditTFSIQueue.htm?query=BOMAdminSendOrder?timestamp=<%=new Date().getTime()%>"> Edit TFSI First Queue</a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminEditTLOQueue.htm?query=BOMAdminSendOrder?timestamp=<%=new Date().getTime()%>"> Edit TLO First Queue</a>
						</td>
					</tr>
					<tr>
						<td><a href="BOMAdminEditAutomatedOnlyQueue.htm?query=BOMAdminSendOrder?timestamp=<%=new Date().getTime()%>"> Edit Automated Only Queue</a>
						</td>
					</tr>

				</table>
				<br />
				<br/>
				<br /><br />
				<br/><br />
				<br/><br />
				<br/><br/>
				<br/>
				<br/>
				<br/>
				</div>
				<br />
				<br />
				<br />
	<center>
		<c:forEach var="messages" items="order">
			<font color="red" face="Arial" size="3"><c:out
					value="${order.messages}" />
			</font>
			<br />
		</c:forEach>
	</center>
	<!-- Close DIV headline -->
			<div class="clear"></div>
			<!-- Clear Floats -->
			</div>



