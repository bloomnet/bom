<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import ="java.util.Date" %>
<div class="headline" align="center"><!-- Close DIV outbound -->
		&nbsp; Daily Metrics for <fmt:formatDate value="${ now }" pattern=" EEEEEEEEE MMMM dd, yyyy" />
  		<div class="clear"></div>
	</div> 
	<!-- Close DIV headline -->
<div class="report-all-orders">
	
	
		<table border="0" align="center">
			
			
	<c:set var="summaryDailyList" scope="page" value="${summaryDailyList}" />
	<c:set var="summaryMtd" scope="page" value="${summaryMtd}" />
	<c:set var="workDailyAutoList" scope="page" value="${workDailyAutoList}" />
	<c:set var="workDailyTLOList" scope="page" value="${workDailyTLOList}" />
	<%-- <c:set var="dailyUnassigned" scope="page" value="${dailyUnassigned}" />
	<c:set var="dailyAssigned" scope="page" value="${dailyAssigned}" />
	<c:set var="dailyRjct" scope="page" value="${dailyRjct}" />
	<c:set var="dailyCanc" scope="page" value="${dailyCanc}" />
	<c:set var="dailyDlcf" scope="page" value="${dailyDlcf}" /> --%>
	<c:set var="mtdUnassigned" scope="page" value="${mtdUnassigned}" />
	<c:set var="mtdAssigned" scope="page" value="${mtdAssigned}" />
	<c:set var="mtdRjct" scope="page" value="${mtdRjct}" />
	<c:set var="mtdCanc" scope="page" value="${mtdCanc}" />
	<c:set var="mtdDlcf" scope="page" value="${mtdDlcf}" />
	<c:set var="now" scope="page" value="${now}" />
	
	<c:forEach items="${ summaryDailyList }" var="summaryDaily" >
	
	<tr>
	<td>
	
	<div class="sub-headline">
	<div style="float: left;">Daily Fulfillment Summary for <fmt:formatDate value="${ summaryDaily.deliveryDate }" pattern=" EEE MMMM dd, yyyy" /></div>
	</div>
	

	
<table class="report-table">
          <tr>
            <td class="col1 header">*Fulfillment %</td>
            <td class="col2 header">Total Orders</td>
            <td class="col3 header">Unassigned Orders</td>
            <td class="col3 header">Assigned Orders</td>   
            <td class="col4 header">Rejected Orders</td>
            <td class="col4 header">Canceled Orders</td>
            <td class="col4 header">Delivery Confirmations</td>
          </tr>
    </table>
    
    <table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="745">
		<tr>
		<td width="106"> <c:out value="${summaryDaily.fulfillmentPercentage}" /> </td>
		<td width="106"> <c:out value="${summaryDaily.totalOrders}" /> </td>
		<td width="106"> <c:out value="${summaryDaily.unassignedOrders}" />  </td>
		<td width="106"> <c:out value="${summaryDaily.assignedActiveOrders}" />  </td>
		<td width="106"> <c:out value="${summaryDaily.rejectedOrders}" />  </td>
		<td width="106"> <c:out value="${summaryDaily.canceledOrders}" />  </td>
		<td width="106"> <c:out value="${summaryDaily.deliveryConfirmations}" />  </td>
		
		</tr>
		
		</table>
		 <table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="533" align="right">
				<tr>
		<% try{ %>
			<td width="20%" > <fmt:formatNumber type="percent" maxIntegerDigits="4" value="${(summaryDaily.unassignedOrders/summaryDaily.totalOrders)}"/></td>
			<td width="20%"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${(summaryDaily.assignedActiveOrders/summaryDaily.totalOrders)} "/></td>
			<td width="20%"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${(summaryDaily.rejectedOrders/summaryDaily.totalOrders) } "/></td>
			<td width="20%"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${(summaryDaily.canceledOrders/summaryDaily.totalOrders) } "/></td>
			<td width="20%"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${(summaryDaily.deliveryConfirmations/summaryDaily.assignedActiveOrders) } "/></td>
		<% }catch(Exception ee){

		} %>
		</tr>
		
		</table>
		<br />
		</td>


		</tr>
		</c:forEach>

		<tr>
	<td>
	
	<div class="sub-headline">
	<div style="float: left;">Monthly Fulfillment Summary&nbsp;</div>
	</div>
	

	
<table class="report-table">
          <tr>
            <td class="col1 header">*Fulfillment %</td>
            <td class="col2 header">Total Orders</td>
            <td class="col3 header">Unassigned Orders</td>
            <td class="col3 header">Assigned Orders</td>   
            <td class="col4 header">Rejected Orders</td>
            <td class="col4 header">Canceled Orders</td>
            <td class="col4 header">Delivery Confirmations</td>
          </tr>
    </table>
    
    <table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="745">
		<tr>
		<td width="114"> <c:out value="${summaryMtd.fulfillmentPercentage}" /> </td>
		<td width="114"> <c:out value="${summaryMtd.totalOrders}" /> </td>
		<td width="114"> <c:out value="${summaryMtd.unassignedOrders}" />  </td>
		<td width="114"> <c:out value="${summaryMtd.assignedActiveOrders}" />  </td>
		<td width="114"> <c:out value="${summaryMtd.rejectedOrders}" />  </td>
		<td width="114"> <c:out value="${summaryMtd.canceledOrders}" />  </td>
		<td width="114"> <c:out value="${summaryMtd.deliveryConfirmations}" />  </td>
		
		</tr>
		
		</table>
		 <table border="1" cellpadding="3" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="533" align="right">
		<tr>
		<% try{ %>
			<td width="106" > <fmt:formatNumber type="percent" maxIntegerDigits="4" value="${mtdUnassigned} "/></td>
			<td width="106"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${mtdAssigned} "/></td>
			<td width="105"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${mtdRjct} "/></td>
			<td width="106"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${mtdCanc} "/></td>
			<td width="106"><fmt:formatNumber type="percent" maxIntegerDigits="4" value="${mtdDlcf} "/></td>
		
		<% }catch(Exception ee){


		} %>
		</tr>
		</table><br/><br/>
		<table border="0" cellpadding="3" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; 
		width="533" align="right">
		<tr>
		<td align="right" colspan="7">* Less FYF Rejects and Cancels</td></tr>

		</table>
		<br />
		</td>


		</tr>
		<br/>
		
		<tr>
	<td>
	
	<div class="sub-headline">
	<div style="float: left;">Manual Messages Received&nbsp;</div>
	</div>
	

	
<table class="report-table">
          <tr>
            <td class="col1 header">Date</td>
            <td class="col1 header">Orders</td>
            <td class="col2 header">Inqr</td>
            <td class="col3 header">Resp</td>
            <td class="col3 header">Info</td>   
            <td class="col4 header">Rjct</td>
            <td class="col4 header">Dlcf</td>
            <td class="col4 header">Pchg</td>
            <td class="col4 header">Canc</td>
            <td class="col4 header">Conf</td>
            <td class="col4 header">Deni</td>
          </tr>
    </table>
    	<c:forEach items="${ workDailyTLOList }" var="workDailyTLO" >
    
    
    <table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="745">
		<tr>
		<td width="68"> <fmt:formatDate value="${workDailyTLO.dateSent}" pattern="MM/dd/yy" /></td>
		<td width="10%"> <c:out value="${workDailyTLO.orders}" /> </td>
		<td width="10%"> <c:out value="${workDailyTLO.inqr}" /> </td>
		<td width="10%"> <c:out value="${workDailyTLO.resp}" />  </td>
		<td width="10%"> <c:out value="${workDailyTLO.info}" />  </td>
		<td width="10%"> <c:out value="${workDailyTLO.rjct}" />  </td>
		<td width="10%"> <c:out value="${workDailyTLO.dlcf}" />  </td>
		<td width="10%"> <c:out value="${workDailyTLO.pchg}" />  </td>
		<td width="10%"> <c:out value="${workDailyTLO.canc}" />  </td>
		<td width="10%"> <c:out value="${workDailyTLO.conf}" />  </td>
		<td width="10%"> <c:out value="${workDailyTLO.deni}" />  </td>
		
		</tr>
		
		</table>
		</c:forEach>
		<br />
		</td>


		</tr>
		
		<br/>
		
		<tr>
	<td>
	
	<div class="sub-headline">
	<div style="float: left;">Automated Messages Received&nbsp;</div>
	</div>
	

	
<table class="report-table">
          <tr>
            <td class="col1 header">Date</td>
            <td class="col1 header">Orders</td>
            <td class="col2 header">Inqr</td>
            <td class="col3 header">Resp</td>
            <td class="col3 header">Info</td>   
            <td class="col4 header">Rjct</td>
            <td class="col4 header">Dlcf</td>
            <td class="col4 header">Pchg</td>
            <td class="col4 header">Canc</td>
            <td class="col4 header">Conf</td>
            <td class="col4 header">Deni</td>
          </tr>
    </table>
    
        	<c:forEach items="${ workDailyAutoList }" var="workDailyAuto" >
    
    <table border="1" cellpadding="4" bordercolor="e1e1e1"
		style="font-family: verdana; font-size: 12px; text-align: center;"
		width="745">
		<tr>
		<td width="68"> <fmt:formatDate value="${workDailyAuto.dateSent}" pattern="MM/dd/yy" /></td>
		<td width="10%"> <c:out value="${workDailyAuto.orders}" /> </td>
		<td width="10%"> <c:out value="${workDailyAuto.inqr}" /> </td>
		<td width="10%"> <c:out value="${workDailyAuto.resp}" />  </td>
		<td width="10%"> <c:out value="${workDailyAuto.info}" />  </td>
		<td width="10%"> <c:out value="${workDailyAuto.rjct}" />  </td>
		<td width="10%"> <c:out value="${workDailyAuto.dlcf}" />  </td>
		<td width="10%"> <c:out value="${workDailyAuto.pchg}" />  </td>
		<td width="10%"> <c:out value="${workDailyAuto.canc}" />  </td>
		<td width="10%"> <c:out value="${workDailyAuto.conf}" />  </td>
		<td width="10%"> <c:out value="${workDailyAuto.deni}" />  </td>
		</tr>
		
		</table>
		
		</c:forEach>
		 
		<br />
		</td>


		</tr>
		</table>
		
		
		</div>
		
						<br />
				<br />



