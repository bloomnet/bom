<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>BOM Admin Page</title>
</head>

<body>
<br/>
<br/>
<br/>
<table border="0" cellpadding="10" cellspacing="10">
      			
	<form:form method="post" commandName="user">
     <tr> 
     <th> Add Role </th>
      </tr> 
      
    
        <tr>
		<td>User: <form:select path="userId" name="userId" class="userForm" onchange='displayRoles()' >
		
			<option value=""></option>
			<c:forEach var="u" items="${ users }">

			<form:option value="${ u.key }">
			<c:out value="${ u.value }" />
			</form:option>

			</c:forEach>
		</form:select>
		</td>
		
			</tr>
			
			<tr>
			<td>
			<strong>Current Roles:</strong>
			<td>
			</tr>
			<tr>
			<td>
			<div id="roles">&nbsp;</div>
					<br/><br/><hr/><br/>
			<td>
			</tr>
    
      
       <tr>
		<td>New Role: <form:select path="selectedRoleId" name="selectedRoleId"  >
		
		
			<c:forEach var="r" items="${ roles }">

			<form:option value="${ r.key }">
			<c:out value="${ r.value }" />
			</form:option>

			</c:forEach>
		</form:select>
		
		</td>
		
			</tr>

	<tr>	
			<td>
		<input type="image" src="images/btn-send.png" name="submit"
			value="submit" class="bottom-buttons" />
			</td>
</tr>
	</form:form>
	</table>
	<br />
	
	<script type="text/javascript">
		$(".userForm").html($(".userForm option").sort(function (a, b) {
    			return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
		}));
	</script>
	



</body>
</html>