<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.*"%>
<%@ page import="java.util.Map.*"%>
<div class="headline">Agent Productivity</div>
<div class="sidebar-content">
	<c:set var="action" scope="session" value="${MASTERORDER.actionStr}" />
	<table width="220">

		<form:form method="post" commandName="user">

<tr> 
     <th> Select Agent and Date <br/><br/></th>
      </tr>

			<tr>
				<td>Agent: <form:select path="userId" name="userId" id="userForm">
						<form:option value=""></form:option>

						<c:forEach var="u" items="${ users }">

							<form:option value="${ u.key }">
								<c:out value="${ u.value }" />
							</form:option>

						</c:forEach>
					</form:select>
                </td>					
			</tr>

			<tr>
				<td>Date:<form:input path="selectedDate"
						class="reply-message-text" id="datepicker" />
					
				</td>
			</tr>

			<tr>
				<td><input type="submit" value="Generate Metrics" />
				</td>
			</tr>

		</form:form>
	</table>
	<script type="text/javascript">
		$("#userForm").html($("#userForm option").sort(function (a, b) {
    			return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
		}));
	</script>
	<br/>
	<br/>
	<div class="headline">
	<div style="text-align: center;">Agents Logged In</div>
	</div>
	<div class="agents-logged-in">
	<ul>
		<c:forEach  var="u" items="${ applicationScope.logins }" >
			<li><c:out value="${ u.firstName }" />&nbsp;<c:out value="${ u.lastName }" />&nbsp;(&nbsp;<c:out value="${ u.userName }" />&nbsp;)</li>
		</c:forEach>
	</ul>
</div>
<!-- Close DIV agents-logged-in -->
<div class="headline">Orders Being Worked [<c:out value="${ beingWorkedCount }"/>]</div>
<!-- Close DIV headline -->
<div class="admin-list">
    <ol type="1">
		<c:forEach var="user" items="${beingWorked}"><br/>
		 <li>
			Order Number: <c:out value="${user.orderNumber}" /><br />
			
			Agent: <c:out value="${user.lastName}" />, <c:out
						value="${user.firstName}" /><br />
						
			Time in order: <c:out value="${user.timeInOrder}" /><br /><br/><hr />
			  </li>
			
		</c:forEach>
		 </ol> <!-- Close OL products -->
      </div>
	


	
	<!-- Close DIV headline -->
	<div class="clear"></div>
	<!-- Clear Floats -->
</div>
<!-- Close DIV sidebar-content -->

