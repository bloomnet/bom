<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ page import="java.util.Date"%>
<script>
$(function() {
	$( "#selectedDate" ).datepicker();
	$( "#endDate" ).datepicker();
});
</script>


<div class="headline">&nbsp</div>


<br/><br/><br/>
<!-- Close DIV sidebar-content -->
<c:set var="action" scope="session" value="${MASTERORDER.actionStr}" />


<table width="205" >

		<form:form method="post" name="reportForm" commandName="user">

<tr align="center"> 
     <th> Select Date(s) <br/><br/></th>
      </tr>


			<tr>
				<td>From:<form:input path="selectedDate" 
						class="reply-message-text" id="selectedDate"  />
					
				</td>
			</tr>
			<tr>
				<td>To:<form:input path="endDate"
						class="reply-message-text" id="endDate" />
					
				</td>
			</tr>

			<tr>
				<td><a href="#" onclick="document.reportForm.submit()" class="underlined">Generate Report</a>
				</td>
			</tr>
			<c:choose>
			<c:when test="${action == 'export'}">
		
			<tr>
				<td><a href="exportxls.htm" class="underlined">Export to spreadsheet</a>
				</td>
			</tr>
			
				</c:when>
				</c:choose>
			

		</form:form>
	</table>



