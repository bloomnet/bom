<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>

<div class="login-area">Welcome <c:out value="${USER.firstName}" /> <c:out value="${USER.lastName}" /> | <a href="signout.htm">Log Out</a>
</div><!-- Close DIV login-area -->
<div class="stat-bar">
  <!-- <div class="stat-label">
    Time in<br />current order:
    </div>
    <input type="text" value="" disabled="disabled" />
    <div class="stat-label">
    Total Orders<br />Touched:
    </div>
    <input type="text" value="" disabled="disabled" />
    <div class="stat-label">
    Total Orders<br />Worked:
    </div>
    <input type="text" value="" disabled="disabled" />
    <div class="stat-label">
    Total Work<br />Time:
    </div>
    <input type="text" value="" disabled="disabled" /> -->
    <input id="quickSearch" type="text" class="search" 
    	   onfocus="if (this.value==$quickSearchDefaultValue ){this.value='';this.style.color='#000'}"
		   onblur="if (this.value==''){this.value=$quickSearchDefaultValue;this.style.color='#c7c6c6'}"  />
    <a href="#" id="quickSearchButton"><img src="images/search-button.png" class="search-go" alt="" /></a>
    <a href="#" id="nextAssignment"><img src="images/next-assignment.png" alt="" /></a>
    <div class="clear"></div>
</div> <!-- Close DIV stat-bar -->
<a href="default.htm?timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />"><img src="images/logo.png" alt="Bloomnet" class="logo" /></a>
