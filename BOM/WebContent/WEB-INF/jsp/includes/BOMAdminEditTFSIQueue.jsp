<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">

<html>

	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>BOM Admin Page</title>
	</head>
	
	<body>
		<br/>
		<br/>
		<br/>
		
		<table border="0" cellpadding="10" cellspacing="10">    			
			<form:form method="post" commandName="user">
			     <tr> 
			     <th> Edit the TFSI First Queue</th>
			      </tr> 
			      
			    
			        <tr>
						<td>TFSI First Queue: <form:select path="queueId" name="queueId" class="queueForm" >
									<option value="0" selected>SELECT</option>
						
							<c:forEach var="q" items="${ queue }">
				
							<form:option value="${ q.key }">
							<c:out value="${ q.value }" />
							</form:option>
				
							</c:forEach>
							</form:select>
							<br/><br/><hr/><br/>
						</td>
					</tr>
			    
			      
			      <tr>
			 		<td>Shop Code (leave blank to remove a selected shop above from this queue):<form:input path="shopCode" class="reply-message-text" id="shopCode" /></td>
				  </tr>
				<tr>	
						<td>
							<input type="image" src="images/btn-send.png" name="submit" value="submit" class="bottom-buttons" />
						</td>
				</tr>
			</form:form>
				
		</table>
			
		<br />
			
		<script type="text/javascript">
			$(".queueForm").html($(".queueForm option").sort(function (a, b) {
		   			return a.text == b.text ? 0 : a.text < b.text ? -1 : 1
			}));
		</script>
	
	</body>
</html>