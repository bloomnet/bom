<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions"   prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"         prefix="fmt" %>
<div class="headline">Messages</div>
<!-- Close DIV headline -->
<div class="sidebar-content">
	<div class="messages-section">
	  <c:forEach var="m" items="${orderMessages}" varStatus="status" >
		<ul>
			<li>
				<strong><c:out value="${ m.messagetype.description }" /></strong>
				<br /> <br />
				
				<fmt:formatDate value="${m.orderactivity.createdDate}" type="time" timeStyle="short" /> on <fmt:formatDate value="${m.orderactivity.createdDate}" pattern="MM/dd/yyyy" />
				<br /><br />
				
				
				<c:out value='${ fn:replace(fn:replace(m.messageText, "%0A", ""), "%0D", "") }' />
				<br /> <br />
				    <a href="AgentViewMessageReply.htm?query=<c:out value="${ m.orderActivityId }" />&timestamp=<fmt:formatDate value='${now}' pattern='yyyyMMddhhmmss' />">Reply</a>
				    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				    
					 <a href="#" onclick="readMessage( <c:out value="${ m.orderActivityId }" />)">Mark as read</a><br />
					 		
				<br />
			</li>
		</ul>
	  </c:forEach>	
	</div>
	<!-- Close DIV messages-section -->
</div>
<!-- Close DIV sidebar-content -->
<div class="sidebar-footer"></div>
<!-- Close DIV sidebar-footer -->
