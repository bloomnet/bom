<%@ page language="java" contentType="text/html" pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"      prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/fmt"       prefix="fmt" %>

<% pageContext.setAttribute("lf",    new Character('\n') ); %>
<% pageContext.setAttribute("cr",    new Character('\r') ); %>
<% pageContext.setAttribute("quote", new Character('"')  ); %>

{"results":[
<c:if test="${ not empty order }">
  <c:set var="bean"  value="${ order.bean }" />
  <c:set var="ss"  value="${ order.bean.sendingShop }" />
  <c:set var="rs"  value="${ order.bean.receivingShop }" />
  <c:forEach items="${ ss.shopnetworks }" var="sn" >
  	<c:choose>
	  	   <c:when test="${fn:containsIgnoreCase(sn.network.name,'bloomnet')}">
				<c:set var="ssCode"  value="${ sn.shopCode }" />
		   </c:when>
  		</c:choose>
	</c:forEach>
	<c:forEach items="${ rs.shopnetworks }" var="sn" >
     <c:choose>
     	<c:when test="${fn:containsIgnoreCase(sn.network.name,'bloomnet')}">
     		<c:if test='${order.orderQueue == "BMTFSI"}'>
     			<c:set var="rsCode"  value="${ sn.shopCode }" />
     		</c:if>
     	</c:when>
	     	<c:when test="${fn:containsIgnoreCase(sn.network.name,'teleflora')}">
	     		<c:if test='${order.orderQueue == "TFSI" }'>
	     			<c:set var="rsCode"  value="${ sn.shopCode }" />
	     		</c:if>
	     		<c:if test='${sn.shopCode == "43030600" }'>
	     			<c:set var="rsCode"  value="Z9980000" />
	     		</c:if>
	     	</c:when>
	     </c:choose>
	 </c:forEach>
		        {
  "lockId":"<c:out value="${order.lockerId}" />",
  "lockName":"<c:out value="${order.lockerName}" />",
  "childOrderNumber":"<c:out value="${order.outboundOrderNumber}" />",
  "queue":"<c:out value="${order.virtualQueue}" />",
  "orderNumber":"<c:out value="${order.orderNumber}" />",
  "date":"<fmt:formatDate value="${ order.deliveryDate }" pattern="MM/dd/yy" />",
  "total":"<fmt:formatNumber value="${ bean.totalCost }" type="CURRENCY" />",
  "occasion":"<c:out value="${order.occassionDescription}" />",
  "status":"<c:out value="${order.status}" />",
  "recipient":{
    "name":"<c:out value="${bean.recipientFirstName} ${ bean.recipientLastName }" />", 
    "address1":"<c:out value="${bean.recipientAddress1}" />",
    "address2":"<c:out value="${bean.recipientAddress2}" />",
    "city":"<c:out value="${bean.recipientCity}" />",
    "state":"<c:out value="${bean.recipientState}" />",
    "zip":"<c:out value="${bean.recipientZipCode}" />",
    "phone":"<c:out value="${bean.recipientPhoneNumberFormated}" />"},
  "sendingShop":{
    "name":"<c:out value="${ss.shopName}" />", 
    "shopCode":"<c:out value="${ssCode}" />",
    "address1":"<c:out value="${ss.shopAddress1}" />",
    "address2":"<c:out value="${ss.shopAddress2}" />",
    "city":"<c:out value="${ss.city.name}" />",
    "state":"<c:out value="${ss.city.state.shortName}" />",
    "zip":"<c:out value="${ss.zip.zipCode}" />",
    "phone":"<c:out value="${ss.shopPhoneFormatted}" />"},
  "receivingShop":{
    "name":"<c:out value="${rs.shopName}" />", 
    "shopCode":"<c:out value="${rsCode}" />",
    "address1":"<c:out value="${rs.shopAddress1}" />",
    "address2":"<c:out value="${rs.shopAddress2}" />",
    "city":"<c:out value="${rs.city.name}" />",
    "state":"<c:out value="${rs.city.state.shortName}" />",
    "zip":"<c:out value="${rs.zip.zipCode}" />",
    "phone":"<c:out value="${rs.shopPhoneFormatted}" />"},
  "products":[
    <c:forEach var="p" items="${ bean.products }" varStatus="status">
    <c:set var="desc" value="${ fn:replace( p.productDescription, cr, '&nbsp;' ) }"/>
	<c:set var="desc" value="${ fn:replace( desc, lf, '&nbsp;' ) }"/>
	<c:set var="desc" value="${ fn:replace( desc, quote, '&nbsp;' ) }"/>
    { "description":"<c:out value="${ desc }" escapeXml="false" />",
      "code":"<c:out value="${p.productCode}"/>",
      "cost":"<fmt:formatNumber value="${p.costOfSingleProduct}" type="CURRENCY" />",
      "units":"<c:out value="${p.units}" />" }
      <c:if test="${ status.index < (fn:length(bean.products)-1) }">,</c:if>
    </c:forEach> ]
}</c:if>]}
