<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<c:import url="/WEB-INF/jsp/includes/headTag.jsp" />
	<body>
		<div style='display: none' class="overlay-wrapper" id="overlay-1">
<!-- IMPORT OVERLAYS HERE AND MAKE VISIBLE BASED ON WHAT SELECTION IS MADE (CURRENTLY EDITORDER OR ORDERHISTORY -->
		</div>
        <form:form method="post" commandName="formOrder">
		<div id="container">
			<div id="header">
                <c:import url="/WEB-INF/jsp/includes/header.jsp" >
                </c:import>
			</div><!-- Close DIV header -->
        <div class="content-container">
        <div class="order-area">
		<c:import url="/WEB-INF/jsp/includes/orderArea.jsp" >
		</c:import>
        </div>
        <div class="sidebar">
		<div class="headline">Method to Send</div>
		<!-- Close DIV headline -->
		<div class="sidebar-content">
		    <div class="method-send">
		        <ul>
		             
		              <c:forEach items="${ MASTERORDER.contactFlorist.shopnetworks }" var="sn" >
                        <c:choose>
                        <c:when test="${(fn:containsIgnoreCase(sn.network.name,'bloomnet')) && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
                          <c:set var="bloomnet" value="bloomnet" scope="page" />
                        </c:when>
                        <c:when test="${(fn:containsIgnoreCase(sn.network.name,'teleflora')) && (sn.shopnetworkstatus.shopNetworkStatusId == '1') }">
                          <c:set var="teleflora" value="teleflora" scope="page" />
                        </c:when>
                        </c:choose>
                      </c:forEach>
		             
		             <c:choose>
		             <c:when test="${fn:containsIgnoreCase(bloomnet,'bloomnet') }">
		                   <li>
		            	<form:radiobutton path="methodToSend.description" value="API" />
		            	<label>Electronically</label>
		            </li>
		             </c:when>
		             <c:when test="${(teleflora=='teleflora') && (bloomnet!='bloomnet') }">
		                   <li>
		            	<form:radiobutton path="methodToSend.description" value="API" />
		            	<label>Electronically</label>
		            </li>
		             </c:when>
		             </c:choose>
		            
		            <li>
			            <form:radiobutton path="methodToSend.description" value="PHONE" checked="yes"/>
			            <label>Read Over Phone</label>
		            </li>
		            <li>
		            	<form:radiobutton path="methodToSend.description" value="FAX" />
		            	<label>Fax</label>
		            	<div class="slidingDivCommInfo">
		            	<br />
						<form:input path="faxConfirmation" name="fax-confirmation" type="text" class="field-width" value="${MASTERORDER.bean.receivingShop.shopFax}" />
				         <br /><br />
				         </div>
				         </li>
		        </ul>
		    </div>
		    <!-- Close DIV method-send -->
		</div>
		<!-- Close DIV sidebar-content -->
		<div class="sidebar-footer">
				        <input type="image" src="images/next-btn.png" alt="Next" value="submit" class="bottom-buttons" />
		</div>
		<!-- Close DIV sidebar-footer -->
		<center>
		<font face="Arial" size="2" color="red">
		  <spring:bind path="command.*">
		    <c:if test="${status.errors.errorCount > 0}">
		      <c:forEach var="error" items="${status.errors.allErrors}">
		        <spring:message message="${error}"></spring:message><br />
		      </c:forEach>
		    </c:if>
		  </spring:bind>
		</font>
		</center>
	    </div> <!-- Close DIV sidebar -->
        <div class="clear"></div> <!-- Clear Floats -->
        </div> <!-- Close DIV content-container -->
        <div class="content-footer">
        <c:import url="/WEB-INF/jsp/includes/footer.jsp" >
        </c:import>
        </div> <!-- Close DIV content-footer -->
        <div id="footer">
        </div> <!-- Close DIV footer -->
        </div> <!-- Close DIV container -->  
        </form:form>
    </body>
</html>