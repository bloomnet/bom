package com.bloomnet.bom.common.dao;

import static org.junit.Assert.*;

import java.util.List;
import java.util.Properties;

import org.hibernate.HibernateException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.exceptions.AddressException;



@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class ShopDAOTest {

	// Injected property
	@Autowired private Properties bomProperties;
	
	// Injected property
	@Autowired private Properties fsiProperties;
	
	@Autowired protected OrderDAO orderDAO;
    @Autowired private ShopDAO shopDAO;
	
	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetShopByShopId() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetShopByCode() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetShopsByZipCode() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetShopByCity() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetPreviousAttemptedShops() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetAvailableTLOShops() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetAvailableTFSIShops() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testInactivateShop() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testPersistShop() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testDeleteShopShop() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testDeleteShopLong() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetShops() {

		final String zipCodeGood = "07652";
		final String zipCodeBad  = "HELLO";
		
		List<Shop> result;
		try {
			
			result = shopDAO.getShops(zipCodeGood, BOMConstants.COMMMETHOD_PHONE);
			
			System.out.println("-----> FINISHED "+result.size());
		} 
		catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		catch (AddressException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testGetInternationalShopByCountry(){
		
		String result = shopDAO.getInternationalShopByCountry("GRC");
		System.out.println(result);
		
		
	}
}
