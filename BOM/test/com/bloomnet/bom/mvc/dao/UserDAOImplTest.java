/**
 * 
 */
package com.bloomnet.bom.mvc.dao;

import static org.junit.Assert.fail;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.AbstractTransactionalJUnit4SpringContextTests;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Useractivity;

/**
 * @author Danil Svirchtchev
 *
 */
@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})    
@Transactional
public class UserDAOImplTest extends AbstractTransactionalJUnit4SpringContextTests {
	
    @Autowired 
    protected UserDAO userDAO;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {
		
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testGetAllUsers() {
		
		try {
			
			List allStoredUsers = userDAO.getAllUsers();
			
			System.out.println("-----> Found "+allStoredUsers.size()+" users in the db...");
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void getUserByEmail() {
		
	}
	
	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#getUserById(com.bloomnet.bom.common.entity.User)}.
	 */
	@Test
	public void testGetUserById() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#getUsersByRole(java.lang.String)}.
	 */
	@Test
	public void testGetUsersByRole() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#getUsersByOccassionPermission(java.lang.String)}.
	 */
	@Test
	public void testGetUsersByOccassionPermission() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#persistUser(com.bloomnet.bom.common.entity.User)}.
	 */
	@Test
	public void testPersistUser() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#deleteUser(java.lang.Long)}.
	 */
	@Test
	public void testDeleteUser() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#getCurrentOrderBeingWorked(java.lang.Long)}.
	 */
	@Test
	public void testGetCurrentOrderBeingWorked() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#getTotalOrdersTouchedToday(java.lang.Long)}.
	 */
	@Test
	public void testGetTotalOrdersTouchedToday() {
		fail("Not yet implemented");
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#getTotalOrdersWorkedToday(java.lang.Long)}.
	 */
	@Test
	public void testGetTotalOrdersWorkedToday() {
		
		long user = 5;
		
		List<BomorderviewV2> orders = userDAO.getTotalOrdersWorkedToday(user);
		
		System.out.println(orders.size());
	}

	/**
	 * Test method for {@link com.bloomnet.bom.mvc.dao.impl.UserDAOImpl#getTotalWorkTimeToday(java.lang.Long)}.
	 */
	@SuppressWarnings("unused")
	@Test
	public void testGetTotalWorkTimeToday() {
		
		long userId =5;
		
		long totalTime = 0;
		
		List<Long> workTimes = userDAO.getTotalWorkTimeToday(userId );
		
		for (Long workTime:workTimes)
		{
			totalTime = totalTime + workTime;
			String seconds = Integer.toString((int)(workTime % 60));   
			String minutes = Integer.toString((int)((workTime % 3600) / 60));   
			String hours = Integer.toString((int)(workTime / 3600));
		
		System.out.println(workTime);
		
		}
		String seconds = Integer.toString((int)(totalTime % 60));   
		String minutes = Integer.toString((int)((totalTime % 3600) / 60));   
		String hours = Integer.toString((int)(totalTime / 3600));
	
	System.out.println("total time : " +totalTime);
	System.out.println("total time : " +hours+":"+minutes+":"+seconds);

		
	}
	
	@Test
	public void testGetCurrentTimeInOrder(){
		Date now = new Date();
System.out.println("now: "+ now);
		DateFormat format = new SimpleDateFormat("HH:mm:ss");   

		
		User user = userDAO.getUserByUserName("jpursoo");
		if (user!=null){
		Date orderTime = userDAO.getCurrentTimeInOrder(user);
		System.out.println("orderTime: "+ orderTime);

		long duration  = now.getTime() - orderTime.getTime(); 
		System.out.println("duration: " + duration);
		long minutes = (duration/ (1000 * 60)) * -1;
		long seconds = (duration/ (1000)% 60 ) * -1;
		System.out.println("minutes" + minutes);
		System.out.println("seconds" + seconds);
		

		
		}
		
		
	}
	
	@Test
	public void testgetLastUserLogin(){
		long userId=5;
		Uactivitytype activitytype = userDAO.getUserActivityType("Logged In");
		
		User user = userDAO.getUserById(userId);
		List<Useractivity> activities = userDAO.getLastUserActivity(user, activitytype);
		
		if (activities!=null){
			Useractivity activity = activities.get(0);
			System.out.println(activity.getCreatedDate());
		}
		
		
	}
	
	
	@Test
	public void testGetUserroleByUser(){
		long userId=5;
		List<Role> roles = new ArrayList<Role>();
		
		
		User user = userDAO.getUserById(userId);
		
		roles= userDAO.getUserroleByUser(user);
		
		for (Role role: roles){
			System.out.println("role:  " +role.getDescription());
		}
		
	}
}
