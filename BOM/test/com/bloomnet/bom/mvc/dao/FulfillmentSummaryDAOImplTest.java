package com.bloomnet.bom.mvc.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.hibernate.HibernateException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.dao.FulfillmentSummaryDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class FulfillmentSummaryDAOImplTest {
	
	@Autowired protected FulfillmentSummaryDAO fulfillmentSummaryDAO;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetFulfillmentSummaryDaily() {
		FulfillmentSummaryDaily fulfillmentSummary = fulfillmentSummaryDAO.getFulfillmentSummaryDaily();
		System.out.println("total orders: " + fulfillmentSummary.getTotalOrders());
	}

	@Test
	public void testGetFulfillmentSummaryByDate() {
		List<FulfillmentSummaryDaily> fulfillmentSummary = fulfillmentSummaryDAO.getFulfillmentSummaryDailyByDate(new Date());
		System.out.println("total orders: " + fulfillmentSummary.get(0).getTotalOrders());
	}
	
	@Test
	public void testGetFulfillmentSummaryMtd() {
		List<FulfillmentSummaryMtd> fulfillmentSummary = fulfillmentSummaryDAO.getFulfillmentSummaryMtdByDate(new Date());
		
	}
	
	@Test
	public void testCallBomDailyMetricsSP(){
		
		//fulfillmentSummaryDAO.callBomDailyMetricsSP("20120301","20120322","20120323");
				
		Date first = DateUtil.getFirstOfMonth(new Date());
		String now = DateUtil.toXmlDateFormatString(first); 
		
		System.out.println(now);


	}
	
	@Test
	public void getFulfillmentSummaryDailyByDateRange(){
		
		String dateStr="06/27/2012";
		String dateToStr="07/08/2012";
		
		List<FulfillmentSummaryDaily> results;
		try {
			results = fulfillmentSummaryDAO.getFulfillmentSummaryDailyByDateRange(dateStr, dateToStr);
		
		
		for (FulfillmentSummaryDaily fulfillmentSummaryDaily:results){
			System.out.println("date: " +fulfillmentSummaryDaily.getDeliveryDate());
		}
		
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	

}
