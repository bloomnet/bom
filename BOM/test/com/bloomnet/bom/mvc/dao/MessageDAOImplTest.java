package com.bloomnet.bom.mvc.dao;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import org.hibernate.SessionFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.RoutingDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.ActPayment;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.StsMessage;
import com.bloomnet.bom.common.entity.StsPayment;
import com.bloomnet.bom.common.entity.User;


@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class MessageDAOImplTest {
	
	
	// Injected DAO implementation
	@Autowired private OrderDAO orderDAO;
	
	@Autowired private MessageDAO messageDAO;
	
	@Autowired private UserDAO userDAO;
	
	@Autowired private ShopDAO shopDAO;

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testDeleteMessage() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommethodByDesc() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetCommunicationMethod() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMessagesByOrderId() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMessagesByOrderNumber() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMessagesByShop() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMessageStatus() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetMessageTypeByShortDesc() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetStsById() {
		fail("Not yet implemented");
	}

	@Test
	public void testPersistMessage() {
		
		ActMessage message = new ActMessage();
		byte  status = 1;
		
		StsMessage stsMessage = new StsMessage();
		stsMessage = messageDAO.getStsById(status);
		
		message.setStsMessage(stsMessage );
		
		Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc(BOMConstants.ACT_MESSAGE);
		Orderactivity orderactivity = new Orderactivity(oactivitytype);
		
		User user = userDAO.getUserByUserName("jpursoo");
	
		orderactivity.setActMessage(message);
		orderactivity.setUser(user);
		List<Bomorder> orders = orderDAO.getOrdersByOrderNumber("3417469");
		Long bomorderId = orders.get(0).getBomorderId();
		
		Bomorder bomorder = orderDAO.getOrderByOrderId(bomorderId);
		orderactivity.setBomorder(bomorder);
		orderactivity.setCreatedDate(new Date());
			
		message.setOrderactivity(orderactivity);
		
		Messagetype messagetype = messageDAO.getMessageTypeByShortDesc(BOMConstants.RJCT_MESSAGE_DESC);
		message.setMessagetype(messagetype);
		
		final Commmethod commmethod= messageDAO.getCommethodByDesc(BOMConstants.COMMMETHOD_API);

		message.setCommmethod(commmethod);
		
		String messageXml = "xml";
		message.setMessageXml(messageXml );
		
		message.setMessageText(messageXml );

		
		Shop sendingShop = shopDAO.getShopByCode("B0270000");
	    Shop receivingShop = shopDAO.getShopByCode("D7340000");
	    
	    message.setShopByReceivingShopId(receivingShop);
		message.setShopBySendingShopId(sendingShop);
		
		byte messageFormat = 1;
		message.setMessageFormat(messageFormat );
	  
		
		messageDAO.persistMessage(message);
	}

	@Test
	public void testUpdateMessageStatus() {

		messageDAO.updateMessageStatus( new Long(80), "5" );
	}
}
