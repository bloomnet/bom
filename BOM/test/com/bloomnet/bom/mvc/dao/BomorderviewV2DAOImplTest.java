package com.bloomnet.bom.mvc.dao;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.util.DateUtil;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class BomorderviewV2DAOImplTest {
	
	@Autowired private BomorderviewV2DAO bomorderviewV2DAO;

	

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetAllOrders() {
		List<BomorderviewV2> orders = bomorderviewV2DAO.getAllOrders();
		
		for (BomorderviewV2 order : orders){
			
			System.out.println(order.getId().getParentBomid());
}
	}

/*	@Test
	public void testGetOrdersForToday() {
		try {
			long unixTime = DateUtil.convertDateToLong();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetOrdersForTommorrow() {
		fail("Not yet implemented"); // TODO
	}
*/
	@Test
	public void testGetOutstandingOrdersForToday() {
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrdersByDate(new Date());

		System.out.println("*******Outstanding orders for today: " + orders.size() +" ************");
		for(BomorderviewV2 order: orders){

			System.out.println(order.getId().getParentOrderNumber());

		}
	}
	
	@Test
	public void testGetOutstandingOrders(){
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrders();
		
		System.out.println("*******Outstanding orders: " + orders.size() +" ************");

		for(BomorderviewV2 order: orders){
			
			System.out.println(order.getId().getParentOrderNumber());
			
		}
		
	}

	/*
	 @Test
	public void testGetOrdersPastDue() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetFutureOrders() {
		fail("Not yet implemented"); // TODO
	}
*/
	@Test
	public void testGetOutstandingOrdersPriorToday() {
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrdersPriorToday();
		
		System.out.println("*******Outstanding orders before today: " + orders.size() +" ************");

		for(BomorderviewV2 order: orders){
			
			System.out.println(order.getId().getParentOrderNumber());
			
		}
	}

	@Test
	public void testGetShopOrdersForToday() {
		//getShopOrdersForToday(Long.parseLong(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID)))
		//jp local
		long shopCode = 15953;
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getShopOrdersByDate(shopCode, new Date());
		System.out.println("*******FROM_YOU_FLOWERS orders for today: " + orders.size() +" ************");

		for(BomorderviewV2 order: orders){
			
			System.out.println(order.getId().getParentOrderNumber());
			
		}
		//jp local
		shopCode = 15960;
		List<BomorderviewV2> orders_jf = bomorderviewV2DAO.getShopOrdersByDate(shopCode, new Date());
		System.out.println("*******JUST_FLOWERS orders for today: " + orders_jf.size() +" ************");

		for(BomorderviewV2 order_jf: orders_jf){
			
			System.out.println(order_jf.getId().getParentOrderNumber());
			
		}
		
		//jp local
		shopCode = 15956;
		List<BomorderviewV2> orders_bt = bomorderviewV2DAO.getShopOrdersByDate(shopCode, new Date());
		System.out.println("*******BLOOMS_TODAY orders for today: " + orders_bt.size() +" ************");

		for(BomorderviewV2 order_bt: orders_bt){
			
			System.out.println(order_bt.getId().getParentOrderNumber());
			
		}
	}
/*	
	@Test
	public void testGetOrderNoteView(){
		
		String parentBomid = "25556142";
		Bomlatestordernoteview orderNote = bomorderviewV2DAO.getOrderNoteView(parentBomid);
		
		System.out.println(orderNote.getId().getNote());
		System.out.println(orderNote.getId().getTimeLogged());
		
		
	}
*/
	@Test
	public void testGetOrdersEnteredByDate(){
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOrdersEnteredByDate(new Date());
		
		for (BomorderviewV2 order : orders){
			
			long time = order.getId().getCreatedUnixTime();
		}
	}
	
	@Test
	public void testGetOrdersEnteredToday(){
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOrdersEnteredToday();
		
		for (BomorderviewV2 order : orders){
			
			long time = order.getId().getCreatedUnixTime();
		}
		
	}
}
