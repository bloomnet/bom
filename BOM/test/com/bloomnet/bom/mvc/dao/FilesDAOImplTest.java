package com.bloomnet.bom.mvc.dao;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.dao.FilesDAO;
import com.bloomnet.bom.common.dao.FulfillmentSummaryDAO;
import com.bloomnet.bom.common.entity.Files;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class FilesDAOImplTest {
	
	@Autowired protected FilesDAO filesDAO;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testFind() {
		int id=2;
		Files files = filesDAO.find(id);
		System.out.println(files.getFilename());
	}

}
