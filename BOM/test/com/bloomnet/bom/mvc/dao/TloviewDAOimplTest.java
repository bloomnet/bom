package com.bloomnet.bom.mvc.dao;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.dao.TloviewDAO;
import com.bloomnet.bom.common.entity.Tloview;
import com.bloomnet.bom.common.entity.Tloviewv2;


@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class TloviewDAOimplTest {
	
	@Autowired private TloviewDAO tloviewDAO;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetOrder() {
		fail("Not yet implemented"); // TODO
	}

	@Test
	public void testGetAllOrders() {
		List<Tloview> orders = tloviewDAO.getAllOrders();
		
		System.out.println(orders.size());
		
	}
		
	@Test
	public void testGetOrderTz() {
		String timezone="'-1','-2','-3','-4','-5','-6','-7','-8','-9','-10','-11'";

		try {
			Tloviewv2 order = tloviewDAO.getOrder(timezone);
			System.out.println(order.getId().getParentOrderNumber());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
