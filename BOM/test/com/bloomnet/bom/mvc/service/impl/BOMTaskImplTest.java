package com.bloomnet.bom.mvc.service.impl;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.mvc.service.BOMTask;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class BOMTaskImplTest {
	
	// Injected service
    @Autowired private BOMTask BOMTask;
	

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSendOrderToQueue() {
		fail("Not yet implemented"); 
	}

	@Test
	public void testSendOrderByFax() {
		fail("Not yet implemented"); 
	}

	@Test
	public void testRemoveUserFromOrder() {
		fail("Not yet implemented"); 
	}
	
	@Test
	public void testReleaseSession() {
		try {
			BOMTask.releaseLockedOrdersTask();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testAuditBOMorders(){
		try {
			BOMTask.auditOrdersTask();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	@Test
	public void testResendOrderTask(){
		try {
			BOMTask.resendOrderTask();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	@Test
	public void testAuditMessages(){
		try {
			BOMTask.auditMessagesTask();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
	}
	
	
}
