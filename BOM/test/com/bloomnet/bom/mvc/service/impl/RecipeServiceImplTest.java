package com.bloomnet.bom.mvc.service.impl;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.bloomnet.bom.common.dao.FulfillmentSummaryDAO;
import com.bloomnet.bom.common.jaxb.recipe.RecipeInfo;
import com.bloomnet.bom.mvc.service.RecipeService;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations = {"classpath:testBloomNetServices.xml"})  
public class RecipeServiceImplTest {
	
	@Autowired protected RecipeService recipeService;


	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testGetRecipeByProductCode() {
		try {
			List<RecipeInfo> recipeInfoList = recipeService.getRecipeByProductCode("BF62-11KM");
			for(RecipeInfo recipeInfo:recipeInfoList){
				System.out.println(recipeInfo.getDesign());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetRecipeByOrderNumber() {
		try {
			List<RecipeInfo> recipeInfoList = recipeService.getRecipeByOrderNumber("21408956");
			for(RecipeInfo recipeInfo:recipeInfoList){
				System.out.println(recipeInfo.getDesign());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Test
	public void testGetAllRecipeInfo() {
		try {
			List<RecipeInfo> recipeInfoList = recipeService.getAllRecipeInfo();
			for(RecipeInfo recipeInfo:recipeInfoList){
				System.out.println(recipeInfo.getDesign());
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	}

}
