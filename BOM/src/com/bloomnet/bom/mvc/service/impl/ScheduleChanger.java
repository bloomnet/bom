package com.bloomnet.bom.mvc.service.impl;

import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;


public class ScheduleChanger {   
	   private DynamicSchedule dynamicSchedule;   
	
	   public void change() {      
		Random rnd = new Random();
	      int nextTimeout = rnd.nextInt(30000);
	      System.out.println("Changing poll time to: " + nextTimeout);
	      dynamicSchedule.reset(nextTimeout);   
	      }
	}