package com.bloomnet.bom.mvc.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Properties;

import javax.jms.JMSException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.CallDispDAO;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.PaymentDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.TloviewDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.StsPayment;
import com.bloomnet.bom.common.entity.Tloview;
import com.bloomnet.bom.common.entity.Tloviewv2;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.jaxb.fsi.DeliveryDetails;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.jaxb.fsi.Identifiers;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.jaxb.fsi.MessageRjct;
import com.bloomnet.bom.common.jaxb.fsi.MessagesOnOrder;
import com.bloomnet.bom.common.jaxb.fsi.OrderDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductsInfo;
import com.bloomnet.bom.common.jaxb.fsi.Recipient;
import com.bloomnet.bom.common.jaxb.fsi.Security;
import com.bloomnet.bom.common.jaxb.fsi.ShippingDetails;
import com.bloomnet.bom.common.jaxb.fsi.WireServiceCode;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.MessageTypeUtil;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.businessobjects.WebAppActLogacall;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.exceptions.EmailOrderConfirmationException;
import com.bloomnet.bom.mvc.exceptions.NoMessagesOnTheQueueException;
import com.bloomnet.bom.mvc.jms.MessageProducer;
import com.bloomnet.bom.mvc.jms.TLOConsumer;
import com.bloomnet.bom.mvc.service.ActivitiesService;
import com.bloomnet.bom.mvc.service.AgentService;
import com.bloomnet.bom.mvc.service.MessagingService;
import com.bloomnet.bom.webservice.service.TFSIService;

@Service("agentService")
@Transactional(propagation = Propagation.SUPPORTS)
public class AgentServiceImpl extends AbstractService implements AgentService {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( AgentServiceImpl.class );

	// Injected property
	@Autowired private Properties bomProperties;

	// Injected property
	@Autowired private Properties fsiProperties;

	// Injected DAO implementation
	@Autowired private MessageDAO messageDAO;

	// Injected DAO implementation
	@Autowired private OrderDAO orderDAO;

	// Injected DAO implementation
	@Autowired private ShopDAO shopDAO;

    // Injected DAO implementation
    @Autowired private UserDAO userDAO;

    // Injected DAO implementation
    @Autowired private CallDispDAO callDispDAO;
    
    // Injected DAO implementation
    @Autowired private TloviewDAO tloviewDAO;

    // Injected dependency
    @Autowired private MessageProducer messageProducer;
    
    // Injected service
    @Autowired private ActivitiesService activitiesService;
    
    @Autowired private MessagingService messagingService;
	
	// Injected service
    @Autowired private TFSIService tfsiService;
    
    
    @Autowired private PaymentDAO paymentDAO;


	// Injected dependency
	@ Autowired private TLOConsumer tloConsumer;
	
	@Autowired private TransformService transformService;	



	
	// Private constructor
	private AgentServiceImpl(){ }
	

	/**
	 * Send order or message to shop by fsi, tfsi, or fax. If communication
	 * is by phone persist order
	 * @throws Exception
	 */
	@Override
	@Transactional( propagation = Propagation.REQUIRED, rollbackFor = Exception.class )
	public String sendOrderToShop( WebAppOrder order, WebAppUser user ) throws Exception {

		MessageOnOrderBean orderBean = order.getBean();
		
		String childOrderNumber = null;
		String userName         = user.getUserName();
		
		String bomShop = fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE);
		final Shop sendingShop    = shopDAO.getShopByCode(bomShop);
		orderBean.setSendingShop(sendingShop);
		order.getBean().setSendingShop(sendingShop);

		//addPayment( order );

		final byte commMethod = orderBean.getCommMethod();
		final byte orderType  = orderBean.getOrderType();

		if  ( ( commMethod == BOMConstants.COMMMETHOD_API_ID ) &&
		      ( orderType == BOMConstants.ORDER_TYPE_BMT ) ) {
			
			childOrderNumber =  sendOrderByFSI( order, user.getUserName() );
		}
		else if ( ( commMethod == BOMConstants.COMMMETHOD_API_ID ) &&
		          ( orderType == BOMConstants.ORDER_TYPE_TEL ) ) {
			
			childOrderNumber = sendOrderByTFSI( order, user.getUserName() );
		}
		else if ( commMethod == BOMConstants.COMMMETHOD_FAX_ID ){
			
			childOrderNumber = sendOrderByFax( order, user );
		}
		else {

			Bomorder parentOrder     = order.getEntity();
			String parentOrderNumber = parentOrder.getOrderNumber();

			//send order to bloomlink and retrieve assigned order number
			childOrderNumber = generateBMTOrderNumber("BMT");
			orderBean.setOrderNumber( childOrderNumber );

			// create order entity
			ForeignSystemInterface fsi = createFSIOrderFromBean( orderBean,false );

			final String orderString   = transformService.convertToFSIXML( fsi );
			Bomorder childOrderEntity  = createOrderEntity( orderBean, orderString );
			
			// unlock the order and merge the changes
			order = activitiesService.unlockOrder( order );
			
			final String status = bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP);

			orderDAO.persistChildOrderAndActivity(childOrderEntity,
					parentOrderNumber, 
					userName, 
					BOMConstants.ACT_ROUTING, 
					status, 
					BOMConstants.TLO_QUEUE);
			
			WebAppOrder childOrder = new WebAppOrder(orderDAO.getOrderByOrderNumber(childOrderNumber));
			
			// save payment activity on a child order
			persistPayment( childOrder, 
							order,
					  		user, 
					  		BOMConstants.PAYMENT_STATUS_OPEN );
		
			
			logger.debug("Completed order: "+parentOrderNumber);
			logger.debug("Sent order: "+childOrderNumber);
			
			//check for messages that need to be worked
			List<ActMessage> orderMessages = messagingService.getMessagesOnOrder( user, order );
			
			if (!orderMessages.isEmpty()){
				ActMessage message = orderMessages.get(0);
				String messageXml = message.getMessageXml();
				String destination = BOMConstants.TLO_QUEUE;
				String messageType = message.getMessagetype().getShortDesc().toUpperCase();
				
				//Long sendingShopId = message.getShopBySendingShopId().getShopId();
				//String sendingShopCode = shopDAO.getShopCodeFromDefaultShopnetworks(sendingShopId);
				String sendingShopCode = order.getBean().getOriginalSendingShopCode();
				sendMessageToDestination(destination, order, messageXml, parentOrderNumber, sendingShopCode, messageType, Byte.toString(orderType));
			}
		}

		return childOrderNumber;
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.AgentService#getNextWorkItem(com.bloomnet.bom.common.entity.User)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)

	public WebAppOrder getNextWorkItem( WebAppUser user ) throws NoMessagesOnTheQueueException, 
																 Exception {

		WebAppOrder result = new WebAppOrder();
		String status = bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED);
		Bomorder orderEntity = orderDAO.getMostRecentOrderByAgent( user.getUserId(), BOMConstants.ORDER_BEING_WORKED );

		if ( orderEntity == null ) {

			final List<Role> skillSets = user.getRoles();
			
			try {
				System.out.println("user: " + user.getUserName() + "["+ user.getFullName() +"]" + " getting next work item");
				result.setBean ( getNextWorkItem( skillSets ) );
				System.out.println("user: " + user.getUserName() + "["+ user.getFullName() +"]" + " got next work order "+ result.getBean().getBmtOrderNumber());

			} catch (Exception e) {
				e.printStackTrace();
				throw new NoMessagesOnTheQueueException( user );
			}



			orderEntity = orderDAO.getOrderByOrderNumber( result.getBean().getBmtOrderNumber() );
			
		}
		
		result.setEntity( orderEntity );
		
		//check to see if order is currently being worked (locked)
		User currentUser = orderEntity.getUser();

		if (currentUser!=null){
			logger.info("Order " + orderEntity.getOrderNumber() + " is currently being worked");
			//check to see if a different user is currently working order
			Long currentUserId = currentUser.getUserId();
			if (!user.getUserId().equals(currentUserId)){
				logger.error("Order is currently being worked by " + currentUser.getUserName() );
				throw new Exception("Order is currently being worked by " + currentUser.getUserName() );
			}	
		}else{
			// put user in Bomorder db record to lock this order by a user
			if(activitiesService.lockOrder( result, user ) == null){
				throw new Exception(result.getBean().getBmtOrderNumber() + " is currently being worked by another user. Please click next assignment again for a new order");
			}
		}
		

		ForeignSystemInterface fsi = transformService.convertToJavaFSI( orderEntity.getOrderXml() );
		MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();

		// TODO: Add shops not found exceptions
		final Shop fulfillingShop = shopDAO.getShopByCode( messageOrder.getFulfillingShopCode() );

		result.getBean().setOriginalReceivingShopCode( messageOrder.getReceivingShopCode() );
		result.getBean().setOriginalSendingShopCode(messageOrder.getSendingShopCode());
		result.getBean().setFulfillingShop( fulfillingShop );
		
		//get current status before changing status to being worked
		byte currentStatus = orderDAO.getCurrentOrderStatus(orderEntity);
		result.setCurrentOrderStatus(currentStatus);

		orderDAO.persistOrderactivityByUser( user.getUserName(),
											 BOMConstants.ACT_ROUTING,
											 bomProperties.getProperty( BOMConstants.ORDER_BEING_WORKED ),
											 orderEntity.getOrderNumber(),
											 BOMConstants.TLO_QUEUE );
		
		long childId = 0;
		String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderEntity.getOrderNumber());
		if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
			childId = orderDAO.getCurrentFulfillingOrder(orderEntity.getOrderNumber()).getBomorderId();
		}
		
		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TLO_QUEUE );
		BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(orderEntity.getBomorderId());
		bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
		bomorderSts.setStsRoutingId(Byte.parseByte(status));
		bomorderSts.setVirtualqueueId(virtualqueueId);
	
		orderDAO.updateBomorderSts(bomorderSts, childId);
		
		return result;
	}


	/* Retrieve next order or message on order from TLO queue
	 *
	 * @see com.bloomnet.bom.mvc.service.AgentService#getNextWorkItem(java.lang.String)
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public MessageOnOrderBean getNextWorkItem( List<Role> skillSets ) throws Exception {

		System.out.println("begin getNextWorkItem: " + DateUtil.toTimeFormatString(new Date()));
		//List<String> messageProperties =  tloConsumer.consumeMessageTZ() ;
		List<String> messageProperties = getOrderFromTLOView();
		System.out.println("end getNextWorkItem: " + DateUtil.toTimeFormatString(new Date()));

		String skillset = BOMConstants.SKILLSET_BASICAGENT;
		String messageTypeDesc = BOMConstants.ORDER_MESSAGE_DESC;
		String messageXml = messageProperties.get(0);
		String orderNumber = messageProperties.get(1);
		//String orderType = messageProperties.get(5);
		//List<ActMessage> orderMessages = new ArrayList<ActMessage> ();

	/*	if (orderType.equals(Byte.toString(BOMConstants.ORDER_TYPE_TEL))){
			Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
			messageXml = bomorder.getOrderXml();
		}*/


		final ForeignSystemInterface fsi = transformService.convertToJavaFSI( messageXml.toString() );

		if ( messageTypeDesc.equals(BOMConstants.ORDER_MESSAGE_DESC ) ) {
			
			final MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();

			// TODO: Add shops not found exceptions
			final Shop receivingShop  = shopDAO.getShopByCode(messageOrder.getReceivingShopCode());
			final Shop sendingShop    = shopDAO.getShopByCode(messageOrder.getSendingShopCode());
			final Shop fulfillingShop = shopDAO.getShopByCode(messageOrder.getFulfillingShopCode());

			MessageOnOrderBean orderBean = new MessageOnOrderBean();


			orderBean = transformService.createBeanFromFSI(fsi,receivingShop,sendingShop,fulfillingShop);
			if (orderBean.getRecipientState()==null ||
					orderBean.getRecipientState().isEmpty()){
				
				Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);
				String state = bomorder.getCity().getState().getShortName();
				orderBean.setRecipientState(state);
				
			}
			//Check for related messages that are on queue
			orderBean = getMessagesOnOrderFromQueue(orderBean);
			orderBean.setSkillset(skillset);
			orderBean.setOrderNumber(orderNumber);
			orderBean.setBmtOrderNumber(orderNumber);

			return orderBean;
		}
		else {

			int messageType = MessageTypeUtil.getMessageTypeInt(messageTypeDesc);

			MessageOnOrderBean orderBean = new MessageOnOrderBean();
			Bomorder parentOrder = orderDAO.getOrderByOrderNumber(orderNumber);

			String orderXML = parentOrder.getOrderXml();
			ForeignSystemInterface orderFSI = transformService.convertToJavaFSI(orderXML);
			MessageOrder messageOrder = orderFSI.getMessagesOnOrder().get(0).getMessageOrder();

			Shop receivingShop   = shopDAO.getShopByCode(messageOrder.getReceivingShopCode());
			Shop sendingShop     = shopDAO.getShopByCode(messageOrder.getSendingShopCode());
			Shop fulfillingShop  = shopDAO.getShopByCode(messageOrder.getFulfillingShopCode());

			orderBean = transformService.createBeanFromFSI(orderFSI,receivingShop,sendingShop,fulfillingShop);
			orderBean.setMessageType(messageType);

			orderBean.setSkillset(skillset);
			orderBean.setOrderNumber(orderNumber);
			orderBean.setBmtOrderNumber(orderNumber);

			return orderBean;
		}
	}
	
	private List<String> getOrderFromTLOView() throws Exception{
		
		ArrayList<String> messageProperties = new ArrayList<String>();

		// BUILD OPEN TIMEZONES BASED ON HOUR ON SERVER BEING KEY, AND list of timezones BEING VALUE
		// example: est begins at 7 and ends at 6
		HashMap<String,String> myTZMap = new HashMap<String,String>();
		myTZMap.put("00","'12','11','10','9','8','7','6','5','4','3','2'");
		myTZMap.put("01","'11','10','9','8','7','6','5','4','3','2','1'");
		myTZMap.put("02","'10','9','8','7','6','5','4','3','2','1','0'");
		myTZMap.put("03","'9','8','7','6','5','4','3','2','1','0','-1'");
		myTZMap.put("04","'8','7','6','5','4','3','2','1','0','-1','-2'");
		myTZMap.put("05","'7','6','5','4','3','2','1','0','-1','-2','-3'");
		myTZMap.put("06","'6','5','4','3','2','1','0','-1','-2','-3','-4'");
		myTZMap.put("07","'5','4','3','2','1','0','-1','-2','-3','-4','-5'");
		myTZMap.put("08","'4','3','2','1','0','-1','-2','-3','-4','-5','-6'");
		myTZMap.put("09","'3','2','1','0','-1','-2','-3','-4','-5','-6','-7'");
		myTZMap.put("10","'2','1','0','-1','-2','-3','-4','-5','-6','-7','-8'");
		myTZMap.put("11","'1','0','-1','-2','-3','-4','-5','-6','-7','-8','-9'");
		myTZMap.put("12","'0','-1','-2','-3','-4','-5','-6','-7','-8','-9','-10'");
		myTZMap.put("13","'-1','-2','-3','-4','-5','-6','-7','-8','-9','-10','-11'");
		myTZMap.put("14","'-2','-3','-4','-5','-6','-7','-8','-9','-10','-11','12'");
		myTZMap.put("15","'-3','-4','-5','-6','-7','-8','-9','-10','-11','12','11'");
		myTZMap.put("16","'-4','-5','-6','-7','-8','-9','-10','-11','12','11','10'");
		myTZMap.put("17","'-5','-6','-7','-8','-9','-10','-11','12','11','10','9'");
		myTZMap.put("18","'-6','-7','-8','-9','-10','-11','12','11','10','9','8'");
		myTZMap.put("19","'-7','-8','-9','-10','-11','12','11','10','9','8','7'");
		myTZMap.put("20","'-8','-9','-10','-11','12','11','10','9','8','7','6'");
		myTZMap.put("21","'-9','-10','-11','12','11','10','9','8','7','6','5'");
		myTZMap.put("22","'-10','-11','12','11','10','9','8','7','6','5','4'");
		myTZMap.put("23","'-11','12','11','10','9','8','7','6','5','4','3'");

		String myZoneValues;

		//GET CURRENT HOUR and then Zone range
		String myCurrentHour = DateUtil.toHourFormatString(new Date());
		System.out.println("*******myCurrentHour******* " + myCurrentHour);
		logger.debug("*******myCurrentHour******* " + myCurrentHour);

		if(!myCurrentHour.equals("")){
			 myZoneValues = myTZMap.get(myCurrentHour);

		}else{
			logger.error("unable to get myCurrentHour");
			System.out.println("unable to get myCurrentHour");
			myZoneValues = myTZMap.get("12");

		}
		
		Tloviewv2 orders = tloviewDAO.getOrder(myZoneValues);
		String xml = orders.getId().getOrderXml();
		String orderNumber =orders.getId().getParentOrderNumber();

		messageProperties.add(0, xml);
		messageProperties.add(1,orderNumber);

		
		return messageProperties;
	}



	


	/**
	 * gets messages of a specific order from queue
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public MessageOnOrderBean getMessagesOnOrderFromQueue (MessageOnOrderBean orderBean) throws Exception {
		//FIXME rename method to takeMessagesOfQueue

	
		String orderNumber = orderBean.getOrderNumber();
		List<String> messageXmls = getTloConsumer()
				.browseQueueForMessagesOnOrder(orderNumber);

		return orderBean;
	}


	/**
	 * Finds and returns related orders from the queue
	 */
	@Override
	public List<MessageOnOrderBean> findRelatedOrdersFromQueue(MessageOnOrderBean orderBean) throws Exception {

		ForeignSystemInterface fsi = new ForeignSystemInterface();
		List<MessageOnOrderBean> messages = new ArrayList<MessageOnOrderBean>();

		String orderNumber = orderBean.getOrderNumber();
		String zip = orderBean.getRecipientZipCode();
		String city = orderBean.getRecipientCity();
		List<String> messageXmls = getTloConsumer().browseQueueForRelatedOrders(zip, city, orderNumber);

		for(int i=0;i<messageXmls.size();i++){
			String messageXml = "";
			MessageOnOrderBean foundMessagesBean = new MessageOnOrderBean();


			messageXml = messageXmls.get(i);
			fsi = transformService.convertToJavaFSI(messageXml);


			MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();
			Shop receivingShop = shopDAO.getShopByCode(messageOrder.getReceivingShopCode());
			Shop sendingShop  = shopDAO.getShopByCode(messageOrder.getSendingShopCode());
			Shop fulfillingShop  = shopDAO.getShopByCode(messageOrder.getFulfillingShopCode());

			foundMessagesBean = transformService.createBeanFromFSI(fsi,receivingShop,sendingShop,fulfillingShop);
			messages.add(foundMessagesBean);

		}

		return messages;
	}

	

	/**
	 * Saves agents call text and disposition
	 * Updates Order activity
	 */
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void logCall( User user, Shop selectedShop, WebAppActLogacall callDisposition, String orderNumber ) throws Exception {

		final Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_LOGACALL );
		final Orderactivity orderactivity = new Orderactivity(oactivitytype);


		Bomorder bomOrder = orderDAO.getOrderByOrderNumber(orderNumber);

		if( bomOrder == null ) {

			throw new Exception( "Call had been logged for this order" );
		}

		Calldisp callDisp = callDispDAO.getCallDispByDesc(callDisposition.getDescription());
		

		ActLogacall call = new ActLogacall();
		call.setOrderactivity(orderactivity);
		call.setLogAcallText(callDisposition.getLogAcallText());
		call.setCalldisp(callDisp);
		call.setShopCalled(selectedShop);

		orderactivity.setActLogacall( call );
		orderactivity.setUser( user );
		orderactivity.setBomorder(bomOrder);
		orderactivity.setCreatedDate(new Date());

		callDispDAO.persistCall( call );

		// update shopnetwork status
		if ( callDisposition.getShopNetworkStatusId() != null &&
		    ! callDisposition.getShopNetworkStatusId().equals("")) {

			final Byte id = Byte.parseByte( callDisposition.getShopNetworkStatusId() );
			shopDAO.setShopStatus( selectedShop, id );
		}
	}
	
	@Override
	@Transactional(propagation = Propagation.REQUIRED)
	public void logCallNoForm( User user, Shop selectedShop, String orderNumber ) throws Exception {

		final Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_LOGACALL );
		final Orderactivity orderactivity = new Orderactivity(oactivitytype);


		Bomorder bomOrder = orderDAO.getOrderByOrderNumber(orderNumber);

		if( bomOrder == null ) {

			throw new Exception( "Call had been logged for this order" );
		}

		Calldisp callDisp = callDispDAO.getCallDispByDesc(BOMConstants.LOGACALL_OTHER);
		

		ActLogacall call = new ActLogacall();
		call.setOrderactivity(orderactivity);
		call.setLogAcallText("Exited Multiple Orders");
		call.setCalldisp(callDisp);
		call.setShopCalled(selectedShop);

		orderactivity.setActLogacall( call );
		orderactivity.setUser( user );
		orderactivity.setBomorder(bomOrder);
		orderactivity.setCreatedDate(new Date());

		callDispDAO.persistCall( call );

	}


	/**
	 * @param orderBean
	 * @param userName
	 * @return
	 * @throws Exception
	 */
	@Override
	public String sendOrderByFSI( WebAppOrder order,String userName ) throws Exception {
		
		MessageOnOrderBean orderBean = order.getBean();
		String orderNumber = orderBean.getOrderNumber();
		
		String containsPerishables = "";
		String pickupCode = "";
		try{
			containsPerishables = order.getEntity().getOrderXml().split("<containsPerishables>")[1].split("</containsPerishables>")[0];
		}catch(Exception ee){}
		try {
			pickupCode = order.getEntity().getOrderXml().split("<pickupCode>")[1].split("</pickupCode>")[0];
		}catch(Exception ee){}
		
		
		//convert orderbean to fsi object
		Shop sendingShop    = shopDAO.getShopByCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		orderBean.setSendingShop(sendingShop);
		//orderBean.setCaptureDate(new Date());
		ForeignSystemInterface fsi = createFSIOrderFromBean( orderBean, true );
		fsi.getMessagesOnOrder().get(0).getMessageOrder().setContainsPerishables(containsPerishables);
		fsi.getMessagesOnOrder().get(0).getMessageOrder().setPickupCode(pickupCode);

		//convert fsi object to String
		String orderString = transformService.convertToFSIXML(fsi);
		
		String parentOrderNumber = orderBean.getOrderNumber();

		//send order to bloomlink and retrieve assigned order number, seqNumberOfOrder, seqNumberOfMessage
		GeneralIdentifiers generalids = messagingService.sendMessageToBloomlink(orderString);

		String childOrderNumber   = generalids.getBmtOrderNumber();
		String seqNumberOfOrder   = generalids.getBmtSeqNumberOfOrder();
		String seqNumberOfMessage = generalids.getBmtSeqNumberOfMessage();

		if ( ! "".equals( childOrderNumber ) ) {
		  orderBean.setOrderNumber(childOrderNumber);
		  orderBean.setBmtOrderNumber(childOrderNumber);
		}
		if ( ! "".equals( seqNumberOfOrder ) ) {
		  orderBean.setBmtSeqNumberOfOrder(Long.parseLong(seqNumberOfOrder));
		}
		if ( ! "".equals( seqNumberOfMessage ) ) {
		  orderBean.setBmtSeqNumberOfMessage(Long.parseLong(seqNumberOfMessage));
		}

		if (childOrderNumber!=null){
			
			MessageOrder childOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();
		
			
			OrderDetails orderdetails = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails();
			orderdetails.setOrderNumber(childOrderNumber);
			childOrder.setOrderDetails(orderdetails);
			generalids.setExternalShopOrderNumber(BOMConstants.EXTERNAL_SHOP_NUMBER);
			
			Identifiers identifiers = new Identifiers();
			identifiers.setGeneralIdentifiers(generalids);
			childOrder.setIdentifiers(identifiers );
			MessagesOnOrder sendingMessagesOnOrder = new MessagesOnOrder();
			
			sendingMessagesOnOrder.setMessageOrder(childOrder);
			fsi.getMessagesOnOrder().add(sendingMessagesOnOrder);
			
			String childOrderXML = transformService.convertToFSIXML(fsi);

			Bomorder orderEntity = createOrderEntity(orderBean, childOrderXML);
	
			final String status = bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP);
			
			orderDAO.persistOrderactivityByUser(
					userName, 
					BOMConstants.ACT_ROUTING, 
					status, 
					parentOrderNumber, 
					BOMConstants.TLO_QUEUE);
	
			orderNumber =orderDAO.persistChildOrderAndActivity(orderEntity,
					parentOrderNumber, 
					"FSI", 
					BOMConstants.ACT_ROUTING, 
					status, 
					BOMConstants.FSI_QUEUE);
					
			
			//set order status to sent to shop
			logger.debug("updating status of order " + orderNumber + "sent to shop" );
			logger.debug("Completed order: "+orderNumber);


		}
		
		activitiesService.unlockOrder( order );
		
		return childOrderNumber;
	}

	/**
	 * 
	 */
	@Override
	public String sendOrderByTFSI( WebAppOrder order, String userName ) throws Exception {

		String tempOrderNumber = new String();
		MessageOnOrderBean orderBean = order.getBean();
		
		String orderNumber = "";
		
		if(order.getOrderNumber() != null)
			orderNumber = order.getOrderNumber();
		else if(order.getBloomnetOrderNumber() != null)
			orderNumber = order.getBloomnetOrderNumber();
		
		orderDAO.persistOrderactivityByUser(userName, BOMConstants.ACT_ROUTING, bomProperties.getProperty( BOMConstants.ORDER_SENT_TO_SHOP ), orderNumber, BOMConstants.TLO_QUEUE);

		tempOrderNumber = tfsiService.sendOrder(orderBean);
		
		activitiesService.unlockOrder( order );
		
		return tempOrderNumber;

	}

	/**
	 * Send order by fax to fulfilling shop
	 * @param orderBean
	 * @throws Exception
	 */
	@Override
	@Transactional(propagation=Propagation.MANDATORY,rollbackFor=Exception.class)
	public String sendOrderByFax( WebAppOrder order, WebAppUser user ) throws Exception {
		
		MessageOnOrderBean orderBean = order.getBean();
		String parentOrderNumber = orderBean.getOrderNumber();
		
		String childOrderNumber = generateBMTOrderNumber("BMT");
		orderBean.setOrderNumber( childOrderNumber );
		String userName = order.getUser().getUserName();

		// create order entity
		ForeignSystemInterface fsi = createFSIOrderFromBean( orderBean,false );

		final String orderString = transformService.convertToFSIXML( fsi );
		Bomorder childOrderEntity = createOrderEntity( orderBean, orderString );
		
		// unlock the order and merge the changes
		order = activitiesService.unlockOrder( order );
		
		logger.debug("updating status of order " + order.getOrderNumber() + " to sent to shop" );

		final String status = bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP);

		orderDAO.persistChildOrderAndActivity(childOrderEntity,
												parentOrderNumber, 
												userName, 
												BOMConstants.ACT_ROUTING, 
												status, 
												BOMConstants.FAX_OUTBOUND_ORDER_QUEUE);
		
		logger.debug("Completed order: "+ order.getOrderNumber() );

		final String code = orderBean.getOccasionId();
		final String shop = fsi.getMessagesOnOrder().get(0).getMessageOrder().getReceivingShopCode();
		final String date = orderBean.getDeliveryDate();
		
		final String destination = BOMConstants.FAX_OUTBOUND_ORDER_QUEUE;
		final String fax = orderBean.getFulfillingShop().getShopFax();
		logger.debug("Sending fax to:" + fax);
		messageProducer.produceOutboundMessage(destination, orderString, order.getOrderNumber(), shop, date, code, fax);

		WebAppOrder childOrder = new WebAppOrder(orderDAO.getOrderByOrderNumber(childOrderNumber));
		
		// save payment activity on a child order
		persistPayment( childOrder, 
						order,
				  		user, 
				  		BOMConstants.PAYMENT_STATUS_OPEN );
		
		return childOrderNumber;
	}


	@Override
	public Bomorder createOrderEntity( MessageOnOrderBean orderbean, String orderXml ) throws Exception {

		byte  commid = orderbean.getCommMethod();
		final Commmethod commmethod1= orderDAO.getCommmethodById(commid);

		String cityName = orderbean.getRecipientCity();
		String stateName = orderbean.getRecipientState();
		String countryName = orderbean.getRecipientCountryCode();


		City city          = orderDAO.getCityByNameAndState(cityName,stateName);
		if(city == null || city.getCityId()==null){
			logger.error(cityName +" not found in " + stateName);
			city = orderDAO.getCityByZip(orderbean.getRecipientZipCode(), orderbean.getRecipientCity());

			if(city == null || city.getCityId()==null){
				logger.error(cityName +" not found in " + orderbean.getRecipientZipCode());
				city = orderDAO.persistCity(cityName, stateName,countryName);
			}
		}
		Zip zip = orderDAO.getZipByCode(orderbean.getRecipientZipCode());
		if (zip==null){
			logger.error("Recipient zip code does not exist: " + orderbean.getRecipientZipCode());
			logger.error("Finding zip code by city and state ....");
			zip = orderDAO.getZipByCityAndState(cityName,stateName);
			Long zipId = zip.getZipId();
			if (zipId==null){
				String errorMsg = "There is an issue with the recipient's address information: " +
				"Recipient zip code: " + orderbean.getRecipientZipCode() +
				" Recipient zip city: " + orderbean.getRecipientCity();
				logger.error(errorMsg);
				zip = orderDAO.persistZip(orderbean.getRecipientZipCode());

			}

		}

		String deliveryDateStr = orderbean.getDeliveryDate();
		Date deliveryDate = DateUtil.toDateFormat(deliveryDateStr);


		Bomorder order = new Bomorder( orderbean.getOrderNumber(),
				   					   orderbean.getOrderType(),
									   orderXml,
									   city,
									   zip,
									   commmethod1,
									   new Date(),
									   deliveryDate,
									   Double.parseDouble(orderbean.getTotalCost()),
									   orderbean.getReceivingShop(),
									   orderbean.getSendingShop(),
									   Long.toString(orderbean.getBmtSeqNumberOfOrder()),
									   Long.toString(orderbean.getBmtSeqNumberOfMessage())
									 );
		return order;
	}



	/**
	 * Send order to next queue to be worked
	 * @throws Exception
	 */
	@Deprecated
	@Override
	public void sendOrderToNextQueue(MessageOnOrderBean orderBean) throws Exception {

		String orderNumber = orderBean.getOrderNumber();
		String destination= "";
		//Keep this commented out until biz logic dao is implemented
		//destination = businessLogicService.DetermineRoute(orderNumber,sendingShopCode);
		destination = "BMTFSI";

		ForeignSystemInterface fsi = createFSIOrderFromBean(orderBean,false);

		String message = transformService.convertToFSIXML(fsi);

		//TODO get DESC representation of
		String messageType = Integer.toString(orderBean.getMessageType());

		//TODO this can be more than just an order
		String sendingShopCode = fsi.getMessagesOnOrder().get(0).getMessageOrder().getSendingShopCode();

		//TODO determine how and what to set skill set to
		String skillset = "1";

		String deliveryDate = fsi.getMessagesOnOrder().get(0).getMessageOrder().getDeliveryDetails().getDeliveryDate();
		String city = fsi.getMessagesOnOrder().get(0).getMessageOrder().getRecipient().getRecipientCity();
		String zip  = fsi.getMessagesOnOrder().get(0).getMessageOrder().getRecipient().getRecipientZipCode();
		String timeZone = orderDAO.getZipByCode(zip).getTimeZone();
		String occasionCode  = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().getOccasionCode();
		messageProducer.produceMessage(destination, message,  orderNumber, sendingShopCode, deliveryDate,
				city, zip,occasionCode, skillset,messageType,Byte.toString(orderBean.getOrderType()),timeZone) ;

		Integer.parseInt(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED));

		//messageDAO.persistMessage(Object);
	}


	/**
	 * get agents total orders touched today
	 *
	 * (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.AgentService#getTotalOrdersTouchedToday(com.bloomnet.bom.common.entity.User)
	 */
	@Override
	public int getTotalOrdersTouchedToday(User user) {
		int totalOrders = 0;
		try {
			List<BomorderviewV2> orders = userDAO.getTotalOrdersTouchedToday(user.getUserId());
			totalOrders = orders.size();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return totalOrders;
	}


	/**
	 * get agents total orders worked today
	 */
	@Override
	public int getTotalOrdersWorkedToday(User user)throws Exception {

		 List<BomorderviewV2> orders = userDAO.getTotalOrdersWorkedToday(user.getUserId());

		return orders.size();
	}

	
	/**
	 * get agents total work time today
	 */
	@Override
	public double getTotalWorkTimeToday(User user) {
		
		List<Long> time;
		try {
			time = userDAO.getTotalWorkTimeToday(user.getUserId());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return 0;
	}
	@Override
	public String getCurrentTimeInOrder(User user){

		String time = new String();

		if (user!=null){

			Date orderTime = userDAO.getCurrentTimeInOrder(user);
			System.out.println("orderTime: "+ orderTime);
			Date now = new Date();
			System.out.println("now: "+ now);

			long duration  = now.getTime() - orderTime.getTime();
			System.out.println("duration: " + duration);
			long minutes = (duration/ (1000 * 60)) * -1;
			long seconds = (duration/ (1000)% 60 ) * -1;
			System.out.println("minutes" + minutes);
			System.out.println("seconds" + seconds);

			time = minutes+ ":" + seconds;

		}

		return time;

	}


	/**
	 * Sends email confirmation fulfilling shop
	 * @throws Exception
	 */
	@Override
	public void sendEmailConfirmation( String orderNumber,
									   Shop shop,
									   MessageOnOrderBean messageOnOrderBean ) throws EmailOrderConfirmationException {

		try {

			String destination= BOMConstants.EMAIL_QUEUE;

			String toEmail = messageOnOrderBean.getReceivingShop().getShopEmail();

			ForeignSystemInterface fsi = createFSIOrderFromBean(messageOnOrderBean,false);
			String orderString = transformService.convertToFSIXML(fsi);
			String occasionCode=messageOnOrderBean.getOccasionId();
			String receivingShop=fsi.getMessagesOnOrder().get(0).getMessageOrder().getReceivingShopCode();
			String deliveryDate=messageOnOrderBean.getDeliveryDate();

			messageProducer.produceOutboundMessage(destination, orderString, orderNumber, receivingShop, deliveryDate, occasionCode, toEmail);
		}
		catch ( Exception e ) {

			throw new EmailOrderConfirmationException( shop );
		}

	}

	/**
	 * add payment activity to child order
	 */
	@Override
	public void persistPayment( WebAppOrder order, WebAppOrder parentOrder, UserInterface user, String status ) throws Exception {
			
			// create order activity
			Oactivitytype m_type   = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_PAYMENT );
			StsPayment    m_status = paymentDAO.getStatusById( bomProperties.getProperty( status ) );
			
			Orderactivity activity = new Orderactivity( m_type );
			activity.setBomorder( order );
			activity.setUser( (User) user );
			order.setOrderactivity( activity );

	
			order.setStsPayment( m_status );
			order.setPaymentAmountStr(parentOrder.getPaymentAmountStr());
			order.setPaymenttype(parentOrder.getPaymenttype());
			order.setPayTo(parentOrder.getPayTo());
			
			paymentDAO.persistPayment( order );
		}

	@Deprecated
	protected ForeignSystemInterface createFSIOutboundOrderFromBean(MessageOnOrderBean orderBean) throws Exception{

		MessagesOnOrder aMessageOnOrder = new MessagesOnOrder();
		Security security = new Security();

		Recipient recipient = new Recipient();
		ShippingDetails shippingDetails = new ShippingDetails();

		MessageOrder messageOrder = new MessageOrder();

		messageOrder.setWireServiceCode(WireServiceCode.valueOf(BOMConstants.WIRE_SERVICE_CODE));
		messageOrder.setMessageType(BOMConstants.ORDER_MESSAGE_TYPE);
		messageOrder.setSystemType((BOMConstants.SYSTEM_TYPE));
		messageOrder.setMessageCreateTimestamp(DateUtil.toXmlFormatString(new Date()));


		DeliveryDetails deliveryDetails = new DeliveryDetails();
		deliveryDetails.setDeliveryDate(orderBean.getDeliveryDate());
		deliveryDetails.setFlexDeliveryDate(orderBean.getFlexDeliveryDate());
		deliveryDetails.setSpecialInstruction(orderBean.getSpecialInstruction());

		messageOrder.setDeliveryDetails(deliveryDetails);
		String fulfillingShopCode = shopDAO.getShopCodeFromDefaultShopnetworks( orderBean.getFulfillingShop().getShopId() );
		String receivingShopCode  = shopDAO.getShopCodeFromDefaultShopnetworks( orderBean.getReceivingShop().getShopId() );
		String sendingShopCode    = shopDAO.getShopCodeFromDefaultShopnetworks( orderBean.getSendingShop().getShopId() );

		messageOrder.setFulfillingShopCode(fulfillingShopCode);
		messageOrder.setReceivingShopCode(receivingShopCode);
		messageOrder.setSendingShopCode(sendingShopCode);

		//messageOrder.setMessageStatus()
		messageOrder.setOrderCaptureDate(DateUtil.toXmlFormatString(orderBean.getCaptureDate()));
		recipient.setRecipientAddress1(orderBean.getRecipientAddress1());
		recipient.setRecipientAddress2(orderBean.getRecipientAddress2());
		//recipient.setRecipientAttention(orderBean.getre);
		recipient.setRecipientCity(orderBean.getRecipientCity());
		recipient.setRecipientCountryCode(orderBean.getRecipientCountryCode());
		recipient.setRecipientFirstName(orderBean.getRecipientFirstName());
		recipient.setRecipientLastName(orderBean.getRecipientLastName());
		recipient.setRecipientPhoneNumber(orderBean.getRecipientPhoneNumber());
		recipient.setRecipientState(orderBean.getRecipientState());
		recipient.setRecipientZipCode(orderBean.getRecipientZipCode());

		messageOrder.setRecipient(recipient);

		shippingDetails.setShipperCode(orderBean.getShipperCode());
		shippingDetails.setShippingDate(DateUtil.toString(orderBean.getShippingDate()));
		shippingDetails.setShippingMethod(orderBean.getShippingMethod());
		shippingDetails.setTrackingNumber(orderBean.getTrackingNumber());

		messageOrder.setShippingDetails(shippingDetails);

		Identifiers identifiers = new Identifiers();
		GeneralIdentifiers generalIdentifiers = new GeneralIdentifiers();
		//generalIdentifiers.setBmtOrderNumber(orderBean.getOrderNumber());

		//XXX set externalShopOrderNumber
		generalIdentifiers.setExternalShopOrderNumber("10001");
	    /*
	    generalIdentifiers.setBmtSeqNumberOfMessage(value)
		generalIdentifiers.setBmtSeqNumberOfOrder(value)
		generalIdentifiers.setExternalShopMessageNumber(value)
		generalIdentifiers.setExternalShopOrderNumber(value)
		generalIdentifiers.setInwireSequenceNo(value)
		*/

		identifiers.setGeneralIdentifiers(generalIdentifiers);
		OrderProductInfoDetails aOrderProductInfoDetails = new OrderProductInfoDetails();

		messageOrder.setIdentifiers(identifiers );

		OrderProductsInfo orderproductsInfo = new OrderProductsInfo();
		for (OrderProductInfoDetails product : orderBean.getProducts()) {

			aOrderProductInfoDetails.setCostOfSingleProduct(product.getCostOfSingleProduct());
			aOrderProductInfoDetails.setProductDescription(product.getProductDescription());
			aOrderProductInfoDetails.setUnits(product.getUnits());
			aOrderProductInfoDetails.setProductCode(product.getProductCode());

			orderproductsInfo.getOrderProductInfoDetails().add(aOrderProductInfoDetails);

		}

		OrderDetails orderDetails = new OrderDetails();
		orderDetails.setOrderProductsInfo(orderproductsInfo);
		orderDetails.setOccasionCode(orderBean.getOccasionId());
		orderDetails.setOrderCardMessage(orderBean.getCardMessage());
		//orderDetails.setOrderNumber(orderBean.getOrderNumber());
		orderDetails.setTotalCostOfMerchandise(orderBean.getTotalCost());
		orderDetails.setOrderType("1");

		messageOrder.setOrderDetails(orderDetails);

		aMessageOnOrder.setMessageCount(1);
		aMessageOnOrder.setMessageOrder(messageOrder);

		security.setPassword(fsiProperties.getProperty(BOMConstants.FSI_PASSWORD));
		security.setShopCode(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE));
		security.setUsername(fsiProperties.getProperty(BOMConstants.FSI_USERNAME));

		Transform transform = new Transform();

		transform.PROPERTIES = fsiProperties;

		ForeignSystemInterface foreignSystemInterface = new ForeignSystemInterface();

		aMessageOnOrder.setMessageCount(1);


		foreignSystemInterface.getMessagesOnOrder().add(aMessageOnOrder);

		foreignSystemInterface.setSecurity(security);


		return foreignSystemInterface;
	}

	


	

	private String generateBMTOrderNumber(String orderName){
		boolean isNew = false;

		String orderNumber = new String();

		SimpleDateFormat xmlFormat = new SimpleDateFormat("yyyyMMddhhmmss");

		orderNumber = xmlFormat.format(new Date());
		orderNumber = orderName + orderNumber;



		try {
			isNew = orderDAO.isUniqueOrderNumber(orderNumber);
			if(!isNew){
				generateBMTOrderNumber(orderName);
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}


		return orderNumber;
	}



	public MessageProducer getMessageProducer() {
		return this.messageProducer;
	}
	public void setMessageProducer(MessageProducer messageProducer) {
		this.messageProducer = messageProducer;
	}

	public TLOConsumer getTloConsumer() {
		return this.tloConsumer;
	}
	public void setTloConsumer(TLOConsumer tloConsumer) {
		this.tloConsumer = tloConsumer;
	}


	@Override
	@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
	public void exitAssignment( WebAppOrder order ) throws Exception {
		
		MessageOnOrderBean messageOnOrderBean = order.getBean();

		String userName          = order.getUser().getUserName();
		String userFName          = order.getUser().getFirstName();
		String sendingShopCode   = messageOnOrderBean.getOriginalSendingShopCode();
		int    messageTypeInt    = messageOnOrderBean.getMessageType();
		String messageType       = MessageTypeUtil.getMessageDesc(messageTypeInt);
		String orderNumber = messageOnOrderBean.getOrderNumber();


		
		ForeignSystemInterface fsi = createFSIOrderFromBean(messageOnOrderBean,false);
		String zip  = fsi.getMessagesOnOrder().get(0).getMessageOrder().getRecipient().getRecipientZipCode();


		String skillset = BOMConstants.SKILLSET_BASICAGENT;
		//String timeZone = order.getZip().getTimeZone();
		String timeZone = orderDAO.getZipByCode(zip).getTimeZone();




		final String orderXml = transformService.convertToFSIXML(fsi);

		Byte commMethod=2;
		messageOnOrderBean.setCommMethod( commMethod );

		String status = Byte.toString(order.getCurrentOrderStatus());
		
		final String destination = order.getVirtualQueue();

		orderDAO.persistOrderactivityByUser( userName, BOMConstants.ACT_ROUTING, status, orderNumber, destination );
		long childId = 0;
		String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(orderNumber);
		if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
			childId = orderDAO.getCurrentFulfillingOrder(orderNumber).getBomorderId();
		}
		
		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination);
		BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(order.getBomorderId());
		bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
		bomorderSts.setStsRoutingId(Byte.parseByte(status));
		bomorderSts.setVirtualqueueId(virtualqueueId);
		
		orderDAO.updateBomorderSts(bomorderSts, childId);

		String deliveryDate = fsi.getMessagesOnOrder().get(0).getMessageOrder().getDeliveryDetails().getDeliveryDate();
		String city = fsi.getMessagesOnOrder().get(0).getMessageOrder().getRecipient().getRecipientCity();
		String occasionCode  = fsi.getMessagesOnOrder().get(0).getMessageOrder().getOrderDetails().getOccasionCode();
		byte prevStatus = order.getCurrentOrderStatus();
		String sentToShopStr = bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP);
		String rjctByBmtStr = bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_BMT);
		String cancConfStr = bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_CONF);
		String acceptStr = bomProperties.getProperty(BOMConstants.ORDER_ACCEPTED_BY_SHOP);
		String dlouStr = bomProperties.getProperty(BOMConstants.ORDER_OUT_FOR_DELIVERY);
		String dlcaStr = bomProperties.getProperty(BOMConstants.ORDER_DELIVERY_ATTEMPTED);
		String dlcfStr = bomProperties.getProperty(BOMConstants.ORDER_DELIVERED_BY_SHOP);
		String wfrStr = bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP);
		String wftzStr = bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE);
		String researchStr = bomProperties.getProperty(BOMConstants.ORDER_BEING_RESEARCHED);
		String outsideStr = bomProperties.getProperty(BOMConstants.ORDER_COMPLETED_OUTSIDE_BOM);
		String completeStr = bomProperties.getProperty(BOMConstants.ORDER_COMPLETED);


		if((prevStatus!=Byte.valueOf(sentToShopStr))
				&&(prevStatus!=Byte.valueOf(rjctByBmtStr))
				&&(prevStatus!=Byte.valueOf(cancConfStr))
				&&(prevStatus!=Byte.valueOf(acceptStr))
				&&(prevStatus!=Byte.valueOf(dlouStr))
				&&(prevStatus!=Byte.valueOf(dlcaStr))
				&&(prevStatus!=Byte.valueOf(dlcfStr))
				&&(prevStatus!=Byte.valueOf(wfrStr))
				&&(prevStatus!=Byte.valueOf(wftzStr))
				&&(prevStatus!=Byte.valueOf(researchStr))
				&&(prevStatus!=Byte.valueOf(outsideStr))
				&&(prevStatus!=Byte.valueOf(completeStr)))

			
		{
			messageProducer.produceMessage( destination, orderXml,  orderNumber, sendingShopCode, deliveryDate,
				city, zip,occasionCode, skillset,messageType,Byte.toString(messageOnOrderBean.getOrderType()), timeZone) ;
		}

		//remove user id from bomorder
		activitiesService.unlockOrder( order );
		
		logger.debug("User " + userName + " (" +userFName+ ") exited order: "+orderNumber);
	}


	@Transactional( propagation = Propagation.REQUIRED, rollbackFor = Exception.class )
	public void sendMessageToDestination(String destination, WebAppOrder order , String messageXml,String orderNumber, String sendingShopCode,  String messageType,String orderType) throws JMSException {
	
			String deliveryDate = order.getBean().getDeliveryDate();
			String zip = order.getBean().getRecipientZipCode();
			String city = order.getBean().getRecipientCity();
			String skillset = BOMConstants.SKILLSET_BASICAGENT;
			String timeZone = orderDAO.getZipByCode(zip).getTimeZone();

			
			if (messageType.equals(BOMConstants.INQR_MESSAGE_DESC)){
				skillset=BOMConstants.SKILLSET_INQUIRE;
			}
			else if (messageType.equals(BOMConstants.RJCT_MESSAGE_DESC)){
				skillset=BOMConstants.SKILLSET_REJECTION;
			}
			else if (messageType.equals(BOMConstants.RESP_MESSAGE_DESC)){
				skillset=BOMConstants.SKILLSET_RESPONSE;
			}
			
			String occasionCode = order.getBean().getOccasionId();
			messageProducer.produceMessage(destination, messageXml, orderNumber,sendingShopCode,  deliveryDate,	city, zip,  
					occasionCode, skillset, messageType, orderType,timeZone);
		

	}
	
	
	@Override
	public Shop getShopByShopCode(String shopCode)	throws Exception{
		
		Shop shop = shopDAO.getShopByCode(shopCode);
		
		return shop;
	}
	
	
	
	@Override
	protected Properties getFsiProperties() {
		return fsiProperties;
	}

	@Override
	protected ShopDAO getShopDAO() {
		return shopDAO;
	}
}
