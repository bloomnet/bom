package com.bloomnet.bom.mvc.service.impl;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.fsi.ForeignSystemInterface;
import com.bloomnet.bom.common.jaxb.fsi.MessageOrder;
import com.bloomnet.bom.common.util.Transform;
import com.bloomnet.bom.common.util.TransformService;
import com.bloomnet.bom.mvc.fax.FaxTemplate;
import com.bloomnet.bom.mvc.fax.SendFax;
import com.bloomnet.bom.mvc.service.FaxService;
import com.itextpdf.text.DocumentException;

@Service("faxService")
@Transactional(propagation = Propagation.REQUIRED)
public class FaxServiceImpl implements FaxService {
	
    // Define a static logger variable
    static Logger logger = Logger.getLogger( FaxServiceImpl.class );

	// Injected DAO implementation
	@Autowired private ShopDAO shopDAO;
	@Autowired private SendFax sendFax;
	@Autowired private FaxTemplate faxTemplate;
	@Autowired private TransformService transformService;	


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.mvc.service.impl.FaxService#SendFax(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	@Transactional(propagation=Propagation.REQUIRED,rollbackFor=Exception.class)
	public void SendFax( String order, String shopCode, String fax ) throws Exception {

		try {

			
			ForeignSystemInterface fsi = transformService.convertToJavaFSI(order);

			MessageOrder messageOrder = fsi.getMessagesOnOrder().get(0).getMessageOrder();

			String recievingShopcode = messageOrder.getReceivingShopCode();
			Shop receivingShop  = shopDAO.getShopByCode(recievingShopcode);
			Shop sendingShop    = shopDAO.getShopByCode(messageOrder.getSendingShopCode());
			Shop fulfillingShop = shopDAO.getShopByCode(messageOrder.getFulfillingShopCode());
			
			receivingShop.setShopFax(fax);
			fulfillingShop.setShopFax(fax);

			MessageOnOrderBean orderBean = transformService.createBeanFromFSI(fsi,receivingShop, sendingShop, fulfillingShop);

			// Warning / Axis 1.4 compatibility: those items are nillable, even if
			// the WSDL doesn't say so.
			SetNillable(com.bloomnet.bom.mvc.fax.soap.submission.WSFile.getTypeDesc());
			SetNillable(com.bloomnet.bom.mvc.fax.soap.submission.Transport.getTypeDesc());
			SetNillable(com.bloomnet.bom.mvc.fax.soap.submission.Attachment.getTypeDesc());
			SetNillable(com.bloomnet.bom.mvc.fax.soap.query.WSFile.getTypeDesc());
			SetNillable(com.bloomnet.bom.mvc.fax.soap.query.QueryRequest.getTypeDesc());

			createFax(orderBean, shopCode);
			sendFax.Run(orderBean);
			
		} catch ( Exception e ) {
			e.printStackTrace();
			logger.error(e);
			
			throw new Exception( "Could not send order by fax", e);
		}
	}

	
	private void SetNillable(org.apache.axis.description.TypeDesc td) {

		org.apache.axis.description.FieldDesc[] fields = td.getFields();

		for (int i = 0; i < fields.length; i++) {

			((org.apache.axis.description.ElementDesc) fields[i]).setNillable(true);
		}
	}
	

	// create fax and save to data directory
	private void createFax( MessageOnOrderBean messageOnOrderBean, String shopCode ) throws IOException, 
	                                                                                        DocumentException {
		
		faxTemplate.createPdf(messageOnOrderBean, shopCode);
	}
}
