package com.bloomnet.bom.mvc.service;

import java.util.Date;
import java.util.List;

import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.entity.WorkOverviewDaily;

public interface SpreadSheetService {
	
	void generateDailySpreadSheet(FulfillmentSummaryDaily summaryDaily, FulfillmentSummaryMtd summaryMtd, 
			   WorkOverviewDaily workDailyMan, WorkOverviewDaily workDailyAuto, String dateStr)throws Exception;
		   
	
	void generateDateRangeSpreadSheet(List<FulfillmentSummaryDaily> summaryDailyList,
			List<FulfillmentSummaryMtd> summaryMtdList,
			List<WorkOverviewDaily> workDailyManList,
			List<WorkOverviewDaily> workDailyAutoList, Date date, Date endDate)throws Exception;


}
