package com.bloomnet.bom.mvc.service;

import java.util.Comparator;
import java.util.List;

import org.hibernate.HibernateException;

import com.bloomnet.bom.common.entity.Uactivitytype;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.common.entity.Useractivity;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.businessobjects.WebAppUserInterface;
import com.bloomnet.bom.mvc.exceptions.UserNotFoundException;
import com.bloomnet.bom.mvc.exceptions.UserWrongPasswordException;
import com.bloomnet.bom.mvc.exceptions.WorkingShopNotFoundException;

/**
 * A custom service for authenticating users against database.
 * 
 * @author Danil Svirchtchev
 */
public interface AuthUserDetailsService {
    
    @SuppressWarnings("rawtypes")
    static final Comparator ACTIVITIES_BY_DATE = new Comparator () {
        
        public int compare ( Object o1, Object o2 ) {
            
            Useractivity v1 = (Useractivity)o1;
            Useractivity v2 = (Useractivity)o2;
            
            return v1.getCreatedDate ().compareTo ( v2.getCreatedDate() );
        }
    };

	/**
	 * Retrieves a user record containing the user's credentials and access.
	 *
	 * @param user User entity
	 * @return User if found, null if not
	 * @throws Exception User exception
	 */
	WebAppUser authenticate(WebAppUserInterface user) throws HibernateException,
															 UserNotFoundException,
															 UserWrongPasswordException,
															 WorkingShopNotFoundException;
	
	void updateUserDetails ( UserInterface user, Uactivitytype activity ) throws HibernateException;
	
	List<Useractivity> getUserActivities ( Long userId ) throws HibernateException;
}
