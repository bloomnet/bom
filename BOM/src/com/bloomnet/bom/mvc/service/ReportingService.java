package com.bloomnet.bom.mvc.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.entity.WorkOverviewDaily;

public interface ReportingService {
	
	List<BomorderviewV2> getFSIOrdersForToday() throws Exception;
	
	List<BomorderviewV2> getTFSIOrdersForToday() throws Exception;
	
	List<BomorderviewV2> getTLOOrdersForToday() throws Exception;

	List<BomorderviewV2> getOutstandingOrdersForToday() throws Exception;
	
	List<BomorderviewV2> getOutstandingOrdersPriorToday() throws Exception;
	
	List<BomorderviewV2> getShopOrdersForToday(long shopCode) throws Exception;
	
	List<BomorderviewV2> getShopOrdersForTodayAccepted(long shop) throws Exception;

	List<Bomorder> getStdOrders() throws Exception;
	
	List<Bomorder> getDirOnlineOrders() throws Exception;
	
	void sendSummaryReport() throws Exception;
	
	void sendOutstandingReport() throws Exception;

	public Map<String,Integer> getOrdersByOcassion(Map<String,String> orderOcassions)
			throws Exception;

	void sendMessageCountReport() throws Exception;

	List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDate(Date date);
	
	List<FulfillmentSummaryMtd> getFulfillmentSummaryMtdByDate(Date date);
	

	List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDate(Date date);

	List<WorkOverviewDaily> getTLOWorkOverviewDailyByDate(Date date);


	void generateMetricsSpreadSheet(FulfillmentSummaryDaily summaryDaily,
			FulfillmentSummaryMtd summaryMtd, WorkOverviewDaily workOverviewDailyMan, WorkOverviewDaily workOverviewDailyAuto, String dateStr)throws Exception;

	List<FulfillmentSummaryDaily> getFulfillmentSummaryDailyByDateRange(
			String date, String dateTo) throws Exception;

	List<WorkOverviewDaily> getTLOWorkOverviewDailyByDateRange(Date date,
			Date toDate);

	List<WorkOverviewDaily> getAutomatedWorkOverviewDailyByDateRange(Date date,
			Date toDate);


	void generateMetricsDRSpreadSheet(
			List<FulfillmentSummaryDaily> summaryDailyList,
			List<FulfillmentSummaryMtd> summaryMtdList,
			List<WorkOverviewDaily> workDailyManList,
			List<WorkOverviewDaily> workDailyAutoList, Date date, Date endDate)throws Exception;


}
