package com.bloomnet.bom.mvc.service;

import java.util.List;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.mvc.businessobjects.WebAppActLogacall;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.exceptions.EmailOrderConfirmationException;

public interface AgentService {

	/**
	 * This service method returns an order bean based on user role.
	 *
	 * @param skillSet
	 * @return
	 * @throws Exception
	 */
	MessageOnOrderBean getNextWorkItem( List<Role> skillSets ) throws Exception;

	/**
	 * This service method returns an order associated with a user.
	 *
	 * @param user
	 * @return
	 * @throws Exception
	 */
	WebAppOrder getNextWorkItem( WebAppUser user ) throws Exception;

	String sendOrderToShop( WebAppOrder order, WebAppUser user ) throws Exception;

	void sendOrderToNextQueue(MessageOnOrderBean order) throws Exception;

	
	MessageOnOrderBean getMessagesOnOrderFromQueue(MessageOnOrderBean orderBean) throws Exception;

	/**
	 * This method will store call disposition activity
	 *
	 * @param orderBean
	 * @param selectedShop
	 * @param user
	 * @param description
	 * @param shopNetworkStatusId
	 * @throws Exception
	 */
	void logCall( User user, Shop selectedShop, WebAppActLogacall callDisposition, String orderNumber ) throws Exception;

	int getTotalOrdersTouchedToday(User user);

	int getTotalOrdersWorkedToday(User user)throws Exception;

	double getTotalWorkTimeToday(User user);

	public String getCurrentTimeInOrder(User user) throws Exception;

	void sendEmailConfirmation( String orderNumber, Shop shop, MessageOnOrderBean messageOnOrderBean ) throws EmailOrderConfirmationException;

	List<MessageOnOrderBean> findRelatedOrdersFromQueue(MessageOnOrderBean orderBean) throws Exception;

	void exitAssignment( WebAppOrder order )throws Exception;
	
	
	Bomorder createOrderEntity( MessageOnOrderBean orderbean, String orderXml ) throws Exception;
	 
	String sendOrderByFSI( WebAppOrder order,String userName ) throws Exception;

	String sendOrderByFax(WebAppOrder order, WebAppUser user) throws Exception;

	String sendOrderByTFSI( WebAppOrder order, String userName ) throws Exception;

	void persistPayment(WebAppOrder order,  WebAppOrder parentOrder, UserInterface user, String status)
			throws Exception;

	Shop getShopByShopCode(String shopCode)	throws Exception;

	void logCallNoForm(User user, Shop selectedShop, String orderNumber) throws Exception;

}
