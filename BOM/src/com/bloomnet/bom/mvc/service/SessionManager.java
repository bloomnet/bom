package com.bloomnet.bom.mvc.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import com.bloomnet.bom.common.entity.Bomorder;

public interface SessionManager {

	//TODO: Fix incoming and outgoing types 
	Object getApplicationUser(HttpServletRequest request);
	void setApplicationUser(HttpServletRequest request, Object applicationUser);

	//TODO: Fix incoming and outgoing types 
	Object getMasterOrder(HttpServletRequest request);
	void setMasterOrder(HttpServletRequest request,Object masterOrder);
	void removeMasterOrder(HttpServletRequest request);
	
	//TODO: Fix incoming and outgoing types 
	Object getOrderBean(HttpServletRequest request);
	void setOrderBean(HttpServletRequest request,Object orderBean);
	
	List<? extends Bomorder> getSearchResults( HttpServletRequest request );
	void setSearchResults( HttpServletRequest request , List<? extends Bomorder> searchResults );

	/**
	 * Removes all of the object wich the application might have had kept in
	 * session.  This method is usefull for the sign out user action. This method
	 * checks for all methods starting with "remove".
	 *
	 * @param request
	 */
	void deleteAllApplicationObjectsFromSession(HttpServletRequest request);
	
	
	Object getOrders(HttpServletRequest request);
	void setOrders(HttpServletRequest request, Object masterOrder);
	void removeOrders(HttpServletRequest request);
	Object getAllUsers(HttpServletRequest request);
	void setAllUsers(HttpServletRequest request, Object AllUsers);
	void removeSelectedDate(HttpServletRequest request);
	void setSelectedDate(HttpServletRequest request, Object masterOrder);
	Object getSelectedDate(HttpServletRequest request);
	Object getSelectedEndDate(HttpServletRequest request);
	void setSelectedEndDate(HttpServletRequest request, Object masterOrder);
	void removeSelectedEndDate(HttpServletRequest request);
}