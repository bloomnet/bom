package com.bloomnet.bom.mvc.dao.impl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.apache.log4j.Logger;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.dao.TloviewDAO;
import com.bloomnet.bom.common.entity.Tloview;
import com.bloomnet.bom.common.entity.Tloviewv2;

@Transactional
@Repository("tloviewDAO")
public class TloviewDAOimpl implements TloviewDAO {
	
	@Autowired private SessionFactory sessionFactory;
	
	static Logger logger = Logger.getLogger( TloviewDAOimpl.class );



	@SuppressWarnings("unchecked")
	@Override
	public Tloview getOrder() throws Exception {
	
		Tloview order;
		
		String str = "from Tloview where Status='To be worked' order by DeliveryDate,timezone,occasion";   
		Query<Tloview> query = sessionFactory.getCurrentSession().createSQLQuery(str); 

		order = (Tloview) query.uniqueResult();
		
		return order;
	}
	
	
	

	@SuppressWarnings("unchecked")
	@Override
	public Tloviewv2 getOrder(String timeZone) throws Exception {
		Tloviewv2 order;
		
		System.out.println("timeZone: "+ timeZone) ;
		
		String sql="Select * from tloview_v2 "+
		" where timeZone in ("+timeZone+")" +
		" order by DeliveryDate,timezone desc,occasion" +
		" limit 1";
		
		System.out.println("tlo getOrder sql:"+ sql);
		
		Query<Tloviewv2> query = sessionFactory.getCurrentSession().createSQLQuery(sql).addEntity(Tloviewv2.class ); 

		order = (Tloviewv2) query.uniqueResult();
		if (order==null){
			
			List<Tloviewv2> order2 = new ArrayList<Tloviewv2>();

			
			String sql2="Select * from tloview_v2"+
			" order by DeliveryDate,timezone desc,occasion";
			
			System.out.println("tlo getOrder sql2:"+ sql2);
			
			Query<Tloviewv2> query2 = sessionFactory.getCurrentSession().createSQLQuery(sql2).addEntity(Tloviewv2.class );
			order2 = (List<Tloviewv2>) query2.list();
			
			order = order2.get(0);

			
		}

		
		
		return order;
	}
	
	//@Override
	@SuppressWarnings("unchecked")
	public List<Tloview> getOutstandingOrders() {
		
		List<Tloview> orders = new ArrayList<Tloview>();
		
		String str = "from Tloview where sts_routing_id in (1,2,10,13)";   
		Query<Tloview> query = sessionFactory.getCurrentSession().createQuery(str); 

		orders = query.list();

		return orders;
	}
	
	@SuppressWarnings({ })
	@Override
	public List<Tloview> getAllOrders() {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Tloview> cq = cb.createQuery(Tloview.class);
        
        List<Tloview> orders = session.createQuery(cq).getResultList();
		
		return orders;
	
	}

}
