package com.bloomnet.bom.mvc.dao.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.persistence.criteria.CriteriaQuery;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.entity.Bomlatestdispositionview;
import com.bloomnet.bom.common.entity.Bomlatestordernoteview;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.BomorderviewV2Id;
import com.bloomnet.bom.common.util.DateUtil;

@Transactional
@Repository("bomorderviewV2DAO")
public class BomorderviewV2DAOImpl implements BomorderviewV2DAO {
	
	@Autowired private SessionFactory sessionFactory;
	
	@Autowired private Properties bomProperties;

	
    static Logger logger = Logger.getLogger( BomorderviewV2DAOImpl.class );



	@Override
	public List<BomorderviewV2> getAllOrders() {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaQuery<BomorderviewV2> cq = session.getCriteriaBuilder().createQuery(BomorderviewV2.class);
        cq.from(BomorderviewV2.class);
        
        List<BomorderviewV2> orders = session.createQuery(cq).getResultList();
		
        
		
		return orders;
	
	}

	

	 
	@Override
	@Deprecated
	public List<BomorderviewV2> getOrdersForToday(){
		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());

		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = getAllOrders();

		for (BomorderviewV2 order: orders){
			if (order!=null){

			Date date = order.getId().getDeliveryDate();
			String cdate = DateUtil.toXmlNoTimeFormatString(date);
			if (todayStr.equals(cdate)){
				result.add(order);
			}
			}
			
		}

		return result;
	}
	
	@Override
	public List<BomorderviewV2> getOrderByDeliveryDate(Date date){
		
		String todayStr = DateUtil.toXmlNoTimeFormatString(date);

		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = getAllOrders();

		for (BomorderviewV2 order: orders){
			if (order!=null){

			Date delDate = order.getId().getDeliveryDate();
			String ddate = DateUtil.toXmlNoTimeFormatString(delDate);
			if (todayStr.equals(ddate)){
				result.add(order);
			}
			}
			
		}

		

		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<BomorderviewV2> getOrderByDeliveryToday(){
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date today = DateUtil.toDate(todayStr);
		String tomorrowStr = DateUtil.tomorrowsNoTimeDate();
		Date tomorrow = DateUtil.toDate(tomorrowStr);
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where deliverydate >= :today and deliverydate < :tomorrow";   
		result = session.createQuery(str).setParameter("today", today).setParameter("tomorrow", tomorrow).getResultList();
		

		return result;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOrdersEnteredToday(){
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date today = DateUtil.toDate(todayStr);
		String tomorrowStr = DateUtil.tomorrowsNoTimeDate();
		Date tomorrow = DateUtil.toDate(tomorrowStr);
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where parentorderdate >= :today and parentorderdate < :tomorrow";   
		result = session.createQuery(str).setParameter("today", today).setParameter("tomorrow", tomorrow).getResultList();
		
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getManualOrdersEnteredByDate(Date date){
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		String todayStr = DateUtil.toXmlNoTimeFormatString(date);
		Date today = DateUtil.toDate(todayStr);
		String tomorrowStr = DateUtil.nextDate(date);
		Date tomorrow = DateUtil.toDateFormat(tomorrowStr);
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where parentOrderDate >= :today and parentOrderDate < :tomorrow " +
				"and  virtualqueue_id in (3,4)";   
		result = session.createQuery(str).setParameter("today", today).setParameter("tomorrow", tomorrow).getResultList();
		
		
		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getAutomatedOrdersEnteredByDate(Date date){
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		String todayStr = DateUtil.toXmlNoTimeFormatString(date);
		Date today = DateUtil.toDate(todayStr);
		String tomorrowStr = DateUtil.nextDate(date);
		Date tomorrow = DateUtil.toDateFormat(tomorrowStr);
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where parentOrderDate >= :today and parentOrderDate < :tomorrow " +
				"and  virtualqueue_id in (1,2)";  
		result = session.createQuery(str).setParameter("today", today).setParameter("tomorrow", tomorrow).getResultList();
		
		

		return result;
	}
	
	
	 
	 
	@Override
	public List<BomorderviewV2> getOrdersForTommorrow(){
		String tommorrowStr = DateUtil.tomorrowsNoTimeDate();
 		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = getAllOrders();
		
		for (BomorderviewV2 order: orders){
			if (order!=null){

			Date date = order.getId().getDeliveryDate();
			String cdate = DateUtil.toXmlNoTimeFormatString(date);
			if (tommorrowStr.equals(cdate)){
				result.add(order);
			}
			}
			
		}
	 
		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOutstandingOrders() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id in (1,2,10,13,18,33) and virtualqueue_id in (3,4)";
		result = session.createQuery(str).getResultList();
		
		

		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOutstandingAutomatedOrders() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id in (1,2) and virtualqueue_id in (1,2)"; 
		result = session.createQuery(str).getResultList();
		

		System.out.println("**************total electronic orders waiting************** " + result.size());

		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOutstandingOrdersNoWfrs() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id in (1,2,10) and virtualqueue_id in (3,4)"; 
		result = session.createQuery(str).getResultList();
		

		System.out.println("**************total orders waiting************** " + result.size());

		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getPreviousOutstandingOrdersNoWfrs() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		Date todayMidnight = new DateTime().toDateMidnight().toDate();

		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id in (1,2,10) and virtualqueue_id in (3,4) and deliverydate < :todayMidnight"; 
		result = session.createQuery(str).setParameter("todayMidnight", todayMidnight).getResultList();
		

		System.out.println("************** orders waiting previous ************** " + result.size());

		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getFutureOutstandingOrdersNoWfrs() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		Date todayMidnight = new DateTime().toDateMidnight().toDate();
		Date tomorrowMidnight  = new DateTime(todayMidnight).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id in (1,2,10) and virtualqueue_id in (3,4) and deliverydate >=:tomorrowMidnight";   
		result = session.createQuery(str).setParameter("tomorrowMidnight", tomorrowMidnight).getResultList();
		


		System.out.println("************** orders waiting future ************** " + result.size());

		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getTodayOutstandingOrdersNoWfrs() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		Date todayMidnight = new DateTime().toDateMidnight().toDate();
		Date tomorrowMidnight  = new DateTime(todayMidnight).plusHours(24).minusSeconds(1).toDate();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id in (1,2,10) and virtualqueue_id in (3,4) and deliverydate >=:todayMidnight and deliverydate <:tomorrowMidnight"; 
		result = session.createQuery(str).setParameter("todayMidnight", todayMidnight).setParameter("tomorrowMidnight", tomorrowMidnight).getResultList();
		
		
		System.out.println("************** orders waiting today ************** " + result.size());

		return result;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getFrozenAutomatedOrders() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id in (1,2) and virtualqueue_id in (1,2)";  
		result = session.createQuery(str).getResultList();
		
 
		System.out.println("**************total orders waiting************** " + result.size());

		return result;
	}
	

	@Override
	public List<BomorderviewV2> getOutstandingOrdersForToday() {
		
		List<BomorderviewV2> orders = getOrderByDeliveryToday();
		
		List<BomorderviewV2> todaysOrders = new ArrayList<BomorderviewV2>();

		for(BomorderviewV2 order: orders){
			if (order!=null){


				BomorderviewV2Id orderId = order.getId();
				
				Byte status = orderId.getStsRoutingId();
				String childOrderNumber = orderId.getChildOrderNumber();
				boolean noChild = childOrderNumber.trim().isEmpty();
				long sendingshop = orderId.getSendingShopId();
				
				if(( (Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))
						||(Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED))))
						&&(!Long.toString(sendingshop).equals(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID))) ){


					if(noChild){

						todaysOrders.add(order);
					}
				}



			}
		}
		
		/*System.out.println("*********Orders for today  ************:   "+ todaysOrders.size());
		for(BomorderviewV2 todaysOrder: todaysOrders){

		
		
		System.out.println("order number: " + todaysOrder.getId().getParentOrderNumber() + ", sending shop: " + todaysOrder.getId().getSendingShopCode());
		}*/
		
		return todaysOrders;
	}
	
	@Override
	public List<BomorderviewV2> getOutstandingOrdersByDate(Date date) {
		
		List<BomorderviewV2> orders = getOrderByDeliveryDate(date);
		
		List<BomorderviewV2> todaysOrders = new ArrayList<BomorderviewV2>();
		
		for(BomorderviewV2 order: orders){
			if (order!=null){

				BomorderviewV2Id orderId = order.getId();

				Byte status = orderId.getStsRoutingId();
				long sendingshop = orderId.getSendingShopId();
				
				


				if( (Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED)))
						||(Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))
						||(Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)))
						||(Byte.toString(status).equals(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE)))
						&&(!Long.toString(sendingshop).equals(bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID))) ){

					todaysOrders.add(order);
				}

			}
		}
		
		
		return todaysOrders;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOrdersPastDue(){
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();

		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date today = DateUtil.toDate(todayStr);
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where deliverydate < :today " +
				"and sts_routing_id in (1,2) and sendingshop_id !=:sendingshop";    
		result = session.createQuery(str).setParameter("today", today).setParameter("sendingshop", bomProperties.getProperty(BOMConstants.FROM_YOU_FLOWERS_ID)).getResultList();
		
		
		return result;
	}
	
	@Override
	public List<BomorderviewV2> getFutureOrders(){
		
	
		Date todayMidnightDate = new DateTime().toDateMidnight().toDate();

		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		List<BomorderviewV2> orders = getAllOrders();

		for (BomorderviewV2 order: orders){
			if (order!=null){

			Date date = order.getId().getDeliveryDate();
			int eq = date.compareTo(todayMidnightDate);
			if (eq>0){
				result.add(order);
			}
			}
		}
	 
		return result;
	}

	@Override
	public List<BomorderviewV2> getOutstandingOrdersPriorToday(){
		
		List<BomorderviewV2> orders = getOrdersPastDue();
		
		
		
		
		return orders;
	}

	@Override
	public List<BomorderviewV2> getShopOrdersForToday(long shopCode) {
		
		List<BomorderviewV2> orders = getOrdersEnteredToday();
		
		List<BomorderviewV2> shopOrders = new ArrayList<BomorderviewV2>();
		
		//System.out.println("*********getShopOrdersForToday************: " + shopCode);
		
		for(BomorderviewV2 order: orders){
			if (order!=null){

				BomorderviewV2Id orderId = order.getId();

				long sendingshop = orderId.getSendingShopId();
				//System.out.println("ordernum: "+ orderId.getParentOrderNumber() + ", shopid: " + sendingshop);


				if( sendingshop==shopCode ){
					shopOrders.add(order);
				}

			}
		}
		
		
		return shopOrders;
	}
	
	@Override
	public List<BomorderviewV2> getShopOrdersByDate(long shopCode, Date date) {
		
		List<BomorderviewV2> orders = getOrdersEnteredByDate(date);
		
		List<BomorderviewV2> shopOrders = new ArrayList<BomorderviewV2>();
		
		for(BomorderviewV2 order: orders){
			if (order!=null){

				BomorderviewV2Id orderId = order.getId();

				long sendingshop = orderId.getSendingShopId();
				

				if( sendingshop==shopCode ){
					shopOrders.add(order);
				}

			}
		}
		
		
		return shopOrders;
	}
	
	@Override
	public List<Bomlatestordernoteview> getAllOrderNoteView() {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaQuery<Bomlatestordernoteview> cq = session.getCriteriaBuilder().createQuery(Bomlatestordernoteview.class);
        cq.from(Bomlatestordernoteview.class);
        
        List<Bomlatestordernoteview> orderNotes = session.createQuery(cq).getResultList();
        
        
		
		return orderNotes;
	}
	
	@Override
	public Bomlatestordernoteview getOrderNoteView(String orderNumber) {
		
		List<Bomlatestordernoteview> orderNotes = getAllOrderNoteView();
		
		for(Bomlatestordernoteview orderNote: orderNotes){
			if(orderNote.getId().getParentOrderNumber().equals(orderNumber)){
				return orderNote;
			}
		}
		return null;
	}

	@Override
	public List<Bomlatestdispositionview> getAllDispositionview() {
		
		Session session = sessionFactory.getCurrentSession();
        
		CriteriaQuery<Bomlatestdispositionview> cq = session.getCriteriaBuilder().createQuery(Bomlatestdispositionview.class);
        cq.from(Bomlatestdispositionview.class);
        
        List<Bomlatestdispositionview> disp = session.createQuery(cq).getResultList();
        
        
		
		return disp;
	}

	@Override
	public Bomlatestdispositionview getDispositionView(String orderNumber) {
		
		List<Bomlatestdispositionview> disps = getAllDispositionview();
		
		for(Bomlatestdispositionview disp: disps){
			if(disp.getId().getParentOrderNumber().equals(orderNumber)){
				return disp;
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOrdersWaitingForResponse() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id=13 and virtualqueue_id in (3,4)";     
		result = session.createQuery(str).getResultList();
		
		  
		System.out.println("**************total orders waiting for response************** " + result.size());

		return result;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOrdersWaitingForTimeZone() {
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id=33 and virtualqueue_id in (3,4)";   
		result = session.createQuery(str).getResultList();
		
		 
		System.out.println("**************total orders waiting for response************** " + result.size());

		return result;
	}




	@Override
	public List<BomorderviewV2> getOrdersEnteredByDate(Date date) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOrderByOrderNumber(String orderNumber){
		
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
	
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where parentOrderNumber =:orderNumber ";   
		result = session.createQuery(str).setParameter("orderNumber", orderNumber).getResultList();
		
		
		return result;
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<BomorderviewV2> getOutstandingMessages() {
		List<BomorderviewV2> result = new ArrayList<BomorderviewV2>();
		
		Session session = sessionFactory.getCurrentSession();
		String str = "from BomorderviewV2 where sts_routing_id = 18 and virtualqueue_id in (3,4)";   
		result = session.createQuery(str).getResultList();
		

		System.out.println("**************total messages waiting************** " + result.size());

		return result;
	}
	




}
