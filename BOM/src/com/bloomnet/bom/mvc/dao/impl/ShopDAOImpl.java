/**
 * 
 */
package com.bloomnet.bom.mvc.dao.impl;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.query.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Internationalshops;
import com.bloomnet.bom.common.entity.Network;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.ShopNotes;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.Shopnetworkcoverage;
import com.bloomnet.bom.common.entity.ShopnetworkcoverageAudit;
import com.bloomnet.bom.common.entity.Shopnetworkstatus;
import com.bloomnet.bom.common.entity.SpecialQueues;
import com.bloomnet.bom.common.entity.State;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.WebserviceMonitor;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.exceptions.AddressException;

/**
 * @author Danil Svirchtchev
 *
 */
@Transactional
@Repository("shopDAO")
public class ShopDAOImpl implements ShopDAO {
	
	@Autowired private SessionFactory sessionFactory;
	@Autowired private MessageDAO messageDAO;
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopByShopId(java.lang.Long)
	 */
	@Override
	public Shop getShopByShopId(Long shopID) throws HibernateException {
		return (Shop) sessionFactory.getCurrentSession().get( Shop.class, shopID );
	}
	
	@Override
	public Shop getShopByPhoneNumber(String phoneNumber) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shop> cq = cb.createQuery(Shop.class);
        
        Root<Shop> root = cq.from(Shop.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shopPhone"), phoneNumber));
        List<Shop> shops = session.createQuery(cq).getResultList();
		
		if(shops != null){
			return shops.get(0);
		}else{
			return null;
		}
	}
	
	@Override
	public List<Shop> getShopsByPhoneNumber(String phoneNumber) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shop> cq = cb.createQuery(Shop.class);
        
        Root<Shop> root = cq.from(Shop.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shopPhone"), phoneNumber));
        List<Shop> shops = session.createQuery(cq).getResultList();
		
		if(shops != null){
			return shops;
		}else{
			return null;
		}
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopByCode(java.lang.String)
	 */
	@Override
	public Shop getShopByCode(String shopCode) throws HibernateException {
	
		final Shop result;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shopnetwork> cq = cb.createQuery(Shopnetwork.class);
        
        Root<Shopnetwork> root = cq.from(Shopnetwork.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shopCode"), shopCode));
        List<Shopnetwork> shopnetworks = session.createQuery(cq).getResultList();
		
		if (shopnetworks.isEmpty()){
			return null;
		}
		
		Shopnetwork shopnetwork = shopnetworks.get(0);
		
		if( shopnetwork != null ) {
			
			shopnetwork = (Shopnetwork) sessionFactory.getCurrentSession()
													  .get(Shopnetwork.class, shopnetwork.getShopNetworkId());

			result = (Shop) sessionFactory.getCurrentSession()
										  .get( Shop.class, shopnetwork.getShop().getShopId() );
		}
		else {
			
			result = null;
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopsByZipCode(java.lang.String)
	 */
	@Override
	public List<Shop> getShopsByZipCode( String zipCode ) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shop> cq = cb.createQuery(Shop.class);
        
        Root<Shop> root = cq.from(Shop.class);
        cq.select(root);
        cq.where(cb.equal(root.get("zip"), zipCode));
        List<Shop> results = session.createQuery(cq).getResultList();
		
        return results;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopByCity(java.lang.String)
	 */
	@Override
	public List<Shop> getShopByCity(String cityName) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shop> cq = cb.createQuery(Shop.class);
        
        Root<Shop> root = cq.from(Shop.class);
        cq.select(root);
        cq.where(cb.equal(root.get("city.name"), cityName));
        List<Shop> results = session.createQuery(cq).getResultList();

        return results;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getPreviousAttemptedShops(java.lang.String)
	 */
	@Override
	public List<String> getPreviousAttemptedShops(long orderId) throws HibernateException {
		
		List<String> shopCodes = new ArrayList<String>();
		String[] BLOOMNET_NETWORK_NAMES  = {"BMT","BloomNet"};
		String[] TELEFLORA_NETWORK_NAMES = {"Teleflora"};
		String[] networkNames = BLOOMNET_NETWORK_NAMES;//default to bmt

		Bomorder order = (Bomorder) sessionFactory.getCurrentSession().get( Bomorder.class, orderId );
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
        cq.where(cb.equal(root.get("parentorderId"), order.getParentorderId()));
		List<Bomorder> childOrders = session.createQuery(cq).getResultList();

		for(Bomorder childOrder: childOrders){
			byte orderType = childOrder.getOrderType();
			Shop shop = childOrder.getShopByReceivingShopId();
			Long shopId = shop.getShopId();

			//get network id based on order type
			if (orderType==BOMConstants.ORDER_TYPE_BMT){
				networkNames  = BLOOMNET_NETWORK_NAMES;
			}
			else if (orderType==BOMConstants.ORDER_TYPE_TEL){
				networkNames  = TELEFLORA_NETWORK_NAMES;
			}
			
			//get shopcode of current receiving shop based on network
			List<Shopnetwork> shopNetworks = getShopnetworks(shopId, networkNames);
			
			if (!shopNetworks.isEmpty()){
				
				for ( Shopnetwork shopNetwork: shopNetworks){
				
					String shopCode = shopNetwork.getShopCode();
					shopCodes.add(shopCode);
				
				}
			}
		}

		return shopCodes;		
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getTLOShopsByZip(java.lang.String)
	 */
	@Override
	public List<Shop> getAvailableTLOShops( String deliveryDate,String zip ) throws HibernateException {
		throw new HibernateException("Not implemented");
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getUNIVShopsByZip(java.lang.String)
	 */
	@Override
	public List<Shop> getAvailableTFSIShops(String dayOfTheWeek, Zip zip) throws HibernateException {
		
		List<Shop> results = new ArrayList<Shop>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shopnetworkcoverage> cq = cb.createQuery(Shopnetworkcoverage.class);
        
        Root<Shopnetworkcoverage> root = cq.from(Shopnetworkcoverage.class);
        cq.select(root);
        cq.where(cb.equal(root.get("zip"), zip));
        List<Shopnetworkcoverage> coverages = session.createQuery(cq).getResultList();
		
		if ( coverages == null || coverages.isEmpty() ) {
			//throw new HibernateException( "There is no coverage for zip code "+zip.getZipCode() );
			System.out.println("There is no coverage for zip code "+zip.getZipCode());
			return results;
		}
		
		for ( Shopnetworkcoverage coverage : coverages) {
			Shopnetwork shopNetwork = coverage.getShopnetwork();
			if(dayOfTheWeek.equals("Sun")){
				if ( shopNetwork != null 
						&& shopNetwork.getNetwork().getNetworkId() == 2 
						&& shopNetwork.getCommmethod().getCommMethodId() == 1
						&& shopNetwork.getShopnetworkstatus().getShopNetworkStatusId() == 1
						&& shopNetwork.getOpenSunday()) {
					
					final Shop shop = shopNetwork.getShop();
					results.add(shop);
				}
			}else{
				if ( shopNetwork != null 
						&& shopNetwork.getNetwork().getNetworkId() == 2 
						&& shopNetwork.getCommmethod().getCommMethodId() == 1 
						&& shopNetwork.getShopnetworkstatus().getShopNetworkStatusId() == 1) {
					
					final Shop shop = shopNetwork.getShop();
					results.add(shop);
				}
			}
		}
		return results;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#deleteShop(long)
	 */
	@Override
	public void deleteShop(long shopId) throws HibernateException {
		final Shop m_shop = getShopByShopId(shopId);
		deleteShop(m_shop);
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#setShopStatus(com.bloomnet.bom.common.entity.Shop, com.bloomnet.bom.common.entity.Shopnetworkstatus)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void setShopStatus( Shop shop, Byte shopnetworkStatusId  ) throws HibernateException {

		String q = "update Shopnetwork set shopnetworkstatus="+shopnetworkStatusId+" where shop="+shop.getShopId();
		Query<Shopnetwork> query = sessionFactory.getCurrentSession().createQuery( q );
		query.executeUpdate();
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#persistShop(com.bloomnet.bom.common.entity.Shop)
	 */
	@Override
	public String persistShop(Shop shop) throws HibernateException {
		System.out.println("persisting shop: "+ shop.getShopName());
		final String result = shop.getShopName();
		sessionFactory.getCurrentSession().saveOrUpdate(shop);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#deleteShop(java.lang.Long)
	 */
	@Override
	public String deleteShop(Shop shop) throws HibernateException {
		final String result = shop.getShopName();
		sessionFactory.getCurrentSession().delete(shop);
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShops(java.lang.String, java.lang.String)
	 */
	@Override
	public List<Shop> getShops(String zipCode, String comMethodDescription) throws HibernateException,
																				   AddressException {

		Set<Shop> result = new HashSet<Shop>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shopnetworkcoverage> cq = cb.createQuery(Shopnetworkcoverage.class);
        
        Root<Shopnetworkcoverage> root = cq.from(Shopnetworkcoverage.class);
        cq.select(root);
        cq.where(cb.equal(root.get("zip"), getZip(zipCode)));
        List<Shopnetworkcoverage> m_shopnetworkcoverage = session.createQuery(cq).getResultList();
		
		if ( m_shopnetworkcoverage == null || m_shopnetworkcoverage.isEmpty() ) {
			
			throw new HibernateException( "There is no coverage for zip code "+zipCode+"\n" );
		}
		
		final Commmethod m_commethod = messageDAO.getCommethodByDesc(comMethodDescription);
		
		for ( Shopnetworkcoverage m_coverage : m_shopnetworkcoverage) {
			
			final Shopnetwork m_shopnetwork = m_coverage.getShopnetwork();
			
			if ( m_shopnetwork != null ) {
				
				final Shop m_shop = m_shopnetwork.getShop();
				
				if ( m_commethod != null && m_commethod.equals(m_shopnetwork.getCommmethod() ) ) {
					
					result.add(m_shop);
				} 
			}
		}
				
		return new ArrayList<Shop>(result);
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopsWithStatus(java.lang.String, java.lang.String)
	 */
	@Override
	public Map<Shop, Shopnetworkstatus> getShopsWithStatus(String zipCode, 
														   String comMethodDescription) 
														   throws AddressException {
		
		Map<Shop, Shopnetworkstatus> result = new HashMap<Shop, Shopnetworkstatus>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shopnetworkcoverage> cq = cb.createQuery(Shopnetworkcoverage.class);
        
        Root<Shopnetworkcoverage> root = cq.from(Shopnetworkcoverage.class);
        cq.select(root);
        cq.where(cb.equal(root.get("zip"), getZip(zipCode)));
        List<Shopnetworkcoverage> m_shopnetworkcoverage = session.createQuery(cq).getResultList();
		
		if ( m_shopnetworkcoverage == null || m_shopnetworkcoverage.isEmpty() ) {
			
			System.out.println( "There is no coverage for zip code "+zipCode );
			
			return result;
		}
		
		final Commmethod m_commethod = messageDAO.getCommethodByDesc(comMethodDescription);
		
		for ( Shopnetworkcoverage m_coverage : m_shopnetworkcoverage) {
			
			Shopnetwork m_shopnetwork = m_coverage.getShopnetwork();
			
			if ( m_shopnetwork != null ) {
				
				Shop m_shop = m_shopnetwork.getShop();

				if ( m_shop != null && 
					 m_commethod != null && 
					 m_commethod.equals(m_shopnetwork.getCommmethod() ) ) {

					result.put( m_shop,  m_shopnetwork.getShopnetworkstatus() );
				} 
			}
		}
				
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopNetworks(com.bloomnet.bom.common.entity.Shop)
	 */
	@Override
	public List<Shopnetwork> getShopNetworks ( Long shopId ) throws HibernateException {

		Shop shop = getShopByShopId(shopId);
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shopnetwork> cq = cb.createQuery(Shopnetwork.class);
        
        Root<Shopnetwork> root = cq.from(Shopnetwork.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shop"),shop));
        List<Shopnetwork> shopnetworks = session.createQuery(cq).getResultList();		
		
		return shopnetworks;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getBloomNetShopCode(java.lang.Long)
	 */
	@Override
	public Shopnetwork getBloomNetShopnetwork( Long shopId ) throws HibernateException {
		
		
		final List<Shopnetwork> networs = getShopnetworks( shopId, BLOOMNET_NETWORK_NAMES ); 
		
		if ( !networs.isEmpty() ) {
			
			for(Shopnetwork sn : networs) {
				if(sn.getShopnetworkstatus().getShopNetworkStatusId() == 1) {
					return sn;
				}
			}
			return networs.get(0);
		}
		
		return null;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getTelefloraShopnetwork(java.lang.Long)
	 */
	@Override
	public Shopnetwork getTelefloraShopnetwork( Long shopId ) throws HibernateException {

		final Shopnetwork result;
		
		final List<Shopnetwork> networs = getShopnetworks( shopId, TELEFLORA_NETWORK_NAMES ); 
		
		if ( !networs.isEmpty() ) {
			
			for(Shopnetwork sn : networs) {
				if(sn.getShopnetworkstatus().getShopNetworkStatusId() == 1) {
					result = sn;
					return result;
				}
			}
			
			result = networs.get(0);
		}
		else {
			
			result = null;
		}
		
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getTelefloraShopnetwork(java.lang.Long)
	 */
	@Override
	public Shopnetwork getFTDShopnetwork( Long shopId ) throws HibernateException {

		final Shopnetwork result;
		
		final List<Shopnetwork> networs = getShopnetworks( shopId, FTD_NETWORK_NAMES ); 
		
		if ( !networs.isEmpty() ) {
			
			result = networs.get(0);
		}
		else {
			
			result = null;
		}
		
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getTelefloraShopnetwork(java.lang.Long)
	 */
	@Override
	public Shopnetwork getOtherShopnetwork( Long shopId ) throws HibernateException {

		final Shopnetwork result;
		
		final List<Shopnetwork> networs = getShopnetworks( shopId, OTHER_NETWORK_NAMES ); 
		
		if ( !networs.isEmpty() ) {
			
			result = networs.get(0);
		}
		else {
			
			result = null;
		}
		
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopnetworks(java.lang.Long, java.lang.String[])
	 */
	@Override
	public List<Shopnetwork> getShopnetworks( final Long shopId, String[] networkNames ) throws HibernateException {
		
		Set<Shopnetwork> result = new HashSet<Shopnetwork>();
		
		List<String> m_networkNames = new ArrayList<String>();
		
		if ( networkNames != null ) {
			
			m_networkNames = new ArrayList<String>( Arrays.asList( networkNames ) );
		}
		else {
			
			m_networkNames = new ArrayList<String>( Arrays.asList( BLOOMNET_NETWORK_NAMES ) );
			m_networkNames.addAll(Arrays.asList( TELEFLORA_NETWORK_NAMES ));
			m_networkNames.addAll(Arrays.asList( FTD_NETWORK_NAMES ));
			m_networkNames.addAll(Arrays.asList( OTHER_NETWORK_NAMES ));

		}
		
		for ( int i = 0; i < m_networkNames.size(); i++ ) {
			
			final String m_networkName = m_networkNames.get(i);
			
			for ( Shopnetwork db_shopnetwork : getShopNetworks ( shopId ) ) {
				
				final Network m_network = db_shopnetwork.getNetwork();
				
				if ( m_network != null ) {
					
					final String db_networkName = m_network.getName();
					
					if( m_networkName.equalsIgnoreCase( db_networkName ) ) {
						
						result.add(db_shopnetwork);
					}
				}
			}
		}

		return new ArrayList<Shopnetwork>(result);
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getBloomNetShopCode(java.lang.Long)
	 */
	@Override
	public String getShopCodeFromDefaultShopnetworks( final Long shopId ) throws HibernateException {
		
		final String result;

		final List<Shopnetwork> shopnetworks = getShopnetworks( shopId, null );

		if ( ! shopnetworks.isEmpty() ) {
			
			Collections.sort(shopnetworks, ShopDAO.SHOP_NETWORK_BY_ID);
			
			for(Shopnetwork network : shopnetworks) {
				if(network.getShopnetworkstatus().getShopNetworkStatusId() == 1 && network.getNetwork().getNetworkId() ==  1) {
					result = network.getShopCode();
					return result;
				}
			}

			result = shopnetworks.get(0).getShopCode();
		}
		else {

			result = null;
		}

		return result; 
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getStates()
	 */
	@Override
	public List<State> getStates() throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<State> cq = cb.createQuery(State.class);
        
        Root<State> root = cq.from(State.class);
        cq.select(root);
        cq.orderBy(cb.asc(root.get("shortName")));
        List<State> states = session.createQuery(cq).getResultList();

		return states;
	}
	

	/* (non-Javadoc)
     * @see com.bloomnet.bom.common.dao.ShopDAO#getState(java.lang.Long)
     */
    @Override
    public State getState(Long stateId) throws HibernateException {

        return (State) sessionFactory.getCurrentSession().get( State.class, stateId );
    }

    
    /* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getZip(java.lang.String)
	 */
	@Override
	public Zip getZip( final String zipCode ) throws HibernateException, AddressException {
		
		final Zip result;
		
		String newZip = zipCode.replaceAll("\\s+","");
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Zip> cq = cb.createQuery(Zip.class);
        
        Root<Zip> root = cq.from(Zip.class);
        cq.select(root);
        cq.where(cb.equal(root.get("zipCode"),newZip));
        List<Zip> m_zips = session.createQuery(cq).getResultList();

		if ( m_zips.isEmpty() ) {
			
			result = new Zip();
			result.setZipCode(newZip);
			throw new AddressException( result );
		}
		else {
			
			result = m_zips.get(0);
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getCity(java.lang.String)
	 */
	@Override
	public City getCity( final String cityName ) throws HibernateException, AddressException {
		
		final City result;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<City> cq = cb.createQuery(City.class);
        
        Root<City> root = cq.from(City.class);
        cq.select(root);
        cq.where(cb.equal(root.get("name"),cityName));
        List<City> m_cities = session.createQuery(cq).getResultList();
		
		if ( m_cities.isEmpty() ) {
			
			result = new City();
			result.setName(cityName);
			throw new AddressException( result );
		}
		else {
			
			result = m_cities.get(0);
			
			for ( City m_city : m_cities ) {
				
				result.getCityZipXrefs().addAll( m_city.getCityZipXrefs() );
			}
		}
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#persitShopnetworkcoverage(com.bloomnet.bom.common.entity.Shopnetworkcoverage)
	 */
	@Override
	public void persitShopnetworkcoverage( Shopnetworkcoverage shopnetworkcoverage ) throws HibernateException {
		
		sessionFactory.getCurrentSession().saveOrUpdate( shopnetworkcoverage );
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#persitShopnetwork(com.bloomnet.bom.common.entity.Shopnetwork)
	 */
	@Override
	public void persitShopnetwork( Shopnetwork shopnetwork )throws HibernateException {
		
		sessionFactory.getCurrentSession().saveOrUpdate( shopnetwork );
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getNetwork(java.lang.String)
	 */
	@Override
	public Network getNetwork(String networkName) throws HibernateException {

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Network> cq = cb.createQuery(Network.class);
        
        Root<Network> root = cq.from(Network.class);
        cq.select(root);
        cq.where(cb.equal(root.get("name"),networkName));
        Network result = session.createQuery(cq).uniqueResult();
		
		return result;
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.ShopDAO#getShopnetworkstatus(java.lang.Byte)
	 */
	@Override
	public Shopnetworkstatus getShopnetworkstatus( Byte shopNetworkStatusId ) throws HibernateException {

		return (Shopnetworkstatus) sessionFactory.getCurrentSession()
		                                         .get( Shopnetworkstatus.class, shopNetworkStatusId );
	}
	
	@Override
	public Shop getShopByAddressAndPhone(String address, String phone) throws HibernateException {
		
		final StringBuilder phoneBuilder = new StringBuilder();
	    final StringCharacterIterator iterator = new StringCharacterIterator(phone);
	    
	    char character =  iterator.current();
	    
	    while (character != CharacterIterator.DONE ){
	    	
	      if (character == '(' || character == ')' || character == '-' || character == ' ') {
	      }else {
	        phoneBuilder.append(character);
	      }
	      
	      character = iterator.next();
	    }
	    
	    String finalPhone = phoneBuilder.toString();
	    
	    Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shop> cq = cb.createQuery(Shop.class);
        
        Root<Shop> root = cq.from(Shop.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shopPhone"),finalPhone));
        List<Shop> shops = session.createQuery(cq).getResultList();
	     
		
		if(shops.size() == 0) return null;
		else return shops.get(0);
	}
	
	@Override
	public String getShopCodeByPhoneNumber(String phoneNumber) throws HibernateException {

		String result;

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shop> cq = cb.createQuery(Shop.class);
        
        Root<Shop> root = cq.from(Shop.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shopPhone"),phoneNumber));
        List<Shop> shops = session.createQuery(cq).getResultList();
		
		Shop shop = shops.get(0);
		
		if (shop !=null){
			 result = getShopCodeFromDefaultShopnetworks(shop.getShopId());
		}
		else {
			result = null;
		}


		return result;
	}
	
	@Override
	public Map<String,Long> getAllNetworks() throws HibernateException {

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Network> cq = cb.createQuery(Network.class);
        
        Root<Network> root = cq.from(Network.class);
        cq.select(root);
        cq.orderBy(cb.asc(root.get("name")));
        List<Network> networks = session.createQuery(cq).getResultList();
		
		Map<String,Long> results = new LinkedHashMap<String,Long>();
		
		for(int ii=0; ii<networks.size(); ++ii){
				results.put(networks.get(ii).getName(),networks.get(ii).getNetworkId());
		}
		
		return results;
	}

	@Override
	public void mergeShop(Shop shop) throws HibernateException {
		sessionFactory.getCurrentSession().merge(shop);
		
	}

	@Override
	public void mergeShopNetwork(Shopnetwork network) throws HibernateException {
		sessionFactory.getCurrentSession().merge(network);
		
	}

	@Override
	public List<Shopnetworkcoverage> getShopNetworkCoverage(Shopnetwork shopnetwork) {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Shopnetworkcoverage> cq = cb.createQuery(Shopnetworkcoverage.class);
        
        Root<Shopnetworkcoverage> root = cq.from(Shopnetworkcoverage.class);
        cq.select(root);
        cq.where(cb.equal(root.get("shopnetwork"),shopnetwork));
        List<Shopnetworkcoverage> shopnetworks = session.createQuery(cq).getResultList();
		
		System.out.println(shopnetworks.get(0).getZip().getZipCode());
		
		return shopnetworks;
		
	}
	
	@Override
	public String getInternationalShopByCountry(String countryCode){
		
		String results  = null;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Internationalshops> cq = cb.createQuery(Internationalshops.class);
        
        Root<Internationalshops> root = cq.from(Internationalshops.class);
        cq.select(root);
        cq.where(cb.equal(root.get("countryCode"),countryCode));
        List<Internationalshops> shops = session.createQuery(cq).getResultList();
		
		 if(shops != null && shops.size() > 0)
			 results = shops.get(0).getShopCode();
		
		/*//get country
		Country country =  (Country) sessionFactory.getCurrentSession()
							.createCriteria( Country.class )
							.add( Restrictions.eq( "shortName", countryCode ) )
							.uniqueResult();
		//get state
		
		
		//get cities
		
		
		//get shop in cities
*/		
		
		return results;
		
	}
	
	@Override
	public WebserviceMonitor getLastQuery(Network network) throws Exception{
		
		WebserviceMonitor result = null;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<WebserviceMonitor> cq = cb.createQuery(WebserviceMonitor.class);
        
        Root<WebserviceMonitor> root = cq.from(WebserviceMonitor.class);
        cq.select(root);
        cq.where(cb.equal(root.get("network"),network));
        cq.orderBy(cb.desc(root.get("webserviceMonitorId")));
        result = session.createQuery(cq).setMaxResults(1).uniqueResult();
		
		System.out.println("WebserviceMonitor numofMessages: "+ result.getNumOfMessages());
		
		return result;
		
	}
	
	@Override
	public void saveOrUpdateBloomlinkQuery(WebserviceMonitor webservicemonitor) {
		
		
		sessionFactory.getCurrentSession().saveOrUpdate(webservicemonitor);
		
	}

	@Override
	public void persistShopNotes(ShopNotes shopNotes) {
		
		sessionFactory.getCurrentSession().saveOrUpdate( shopNotes );
		
	}
	
	@Override
	public String getSpecialQueues(SpecialQueues queue) {
		
		String result = "";

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<SpecialQueues> cq = cb.createQuery(SpecialQueues.class);
        
        Root<SpecialQueues> root = cq.from(SpecialQueues.class);
        cq.select(root);
        
        List<SpecialQueues> queues = null;
        
        if(queue.getTfsiFirst() == 1) {
	        cq.where(cb.equal(root.get("tfsiFirst"),1));
	        queues = session.createQuery(cq).getResultList();

        }else if(queue.getTloFirst() == 1) {
        	cq.where(cb.equal(root.get("tloFirst"),1));
	        queues = session.createQuery(cq).getResultList();
			
        }else if(queue.getAutomatedOnly() == 1) {
        	cq.where(cb.equal(root.get("automatedOnly"),1));
	        queues = session.createQuery(cq).getResultList();
			
        }
        
        for(SpecialQueues q : queues) {
			result+=q.getShopCode() + ",";
		}
		
		return result;
		
	}
	
	@Override
	public List<SpecialQueues> getAllSpecialQueues() {
		

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<SpecialQueues> cq = cb.createQuery(SpecialQueues.class);
        cq.from(SpecialQueues.class);

        List<SpecialQueues> results = session.createQuery(cq).getResultList();
		
		return results;
	}
	
	@Override
	public void removeSpecialQueues(SpecialQueues queue) {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<SpecialQueues> cq = cb.createQuery(SpecialQueues.class);
        
        Root<SpecialQueues> root = cq.from(SpecialQueues.class);
        cq.select(root);
	    cq.where(cb.equal(root.get("shopCode"),queue.getShopCode()));
	    
	   SpecialQueues q = session.createQuery(cq).getResultList().get(0);
	   
	   if(q != null) {
		   sessionFactory.getCurrentSession().delete(q);
	   }	
	}
	
	@Override
	public void saveOrUpdateSpecialQueues(SpecialQueues queue) {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<SpecialQueues> cq = cb.createQuery(SpecialQueues.class);
        
        Root<SpecialQueues> root = cq.from(SpecialQueues.class);
        cq.select(root);
	    cq.where(cb.equal(root.get("queueId"),queue.getQueueId()));
	    
	   List<SpecialQueues> results = null;
	   
	   if(queue.getShopCode()== null || queue.getShopCode().trim().equals(""))
		   results = session.createQuery(cq).getResultList();
	   
	   SpecialQueues q = null;
	   if(results != null && results.size() > 0){ 
		   q = results.get(0);
		   q.setAutomatedOnly(queue.getAutomatedOnly());
		   q.setTfsiFirst(queue.getTfsiFirst());
		   q.setTloFirst(queue.getTloFirst());
		   sessionFactory.getCurrentSession().update(q);	
	   }else {
		   cq.where(cb.equal(root.get("shopCode"),queue.getShopCode()));
		   results = session.createQuery(cq).getResultList();
		   if(results != null && results.size() > 0){
			   q = results.get(0);
			   q.setAutomatedOnly(queue.getAutomatedOnly());
			   q.setTfsiFirst(queue.getTfsiFirst());
			   q.setTloFirst(queue.getTloFirst());
			   sessionFactory.getCurrentSession().update(q);
		   }else{
			   queue.setQueueId(0);
			   sessionFactory.getCurrentSession().save(queue);
		   }
	   }
	}

	
	@Override
	public void removeCoverage(Shop myShop, Zip zip, User user) {
		
		Iterator<Shopnetwork> it = myShop.getShopnetworks().iterator();
		
		while(it.hasNext()){
			
			Session session = sessionFactory.getCurrentSession();
			
			CriteriaBuilder cb = session.getCriteriaBuilder();
			CriteriaQuery<Shopnetworkcoverage> cq = cb.createQuery(Shopnetworkcoverage.class);
	        
	        Root<Shopnetworkcoverage> root = cq.from(Shopnetworkcoverage.class);
	        cq.select(root);
	        cq.where(cb.and(cb.equal(root.get("shopnetwork"), it.next()),cb.equal(root.get("zip"), zip)));
	        List<Shopnetworkcoverage> coverages = session.createQuery(cq).getResultList();
			
			if(coverages != null && coverages.size() > 0){
				
				for(Shopnetworkcoverage snc : coverages){
					
					ShopnetworkcoverageAudit audit = new ShopnetworkcoverageAudit();
					
					audit.setZip(snc.getZip());
					audit.setUserByModifiedUserId(snc.getUserByModifiedUserId());
					audit.setUserByCreatedUserId(snc.getUserByCreatedUserId());
					audit.setShopnetwork(snc.getShopnetwork());
					audit.setModifiedDate(snc.getModifiedDate());
					audit.setCreatedDate(snc.getCreatedDate());
					audit.setUserByDeletedUserId(user);
					audit.setDeletedDate(new Date());
					
					sessionFactory.getCurrentSession().persist(audit);
					sessionFactory.getCurrentSession().delete(snc);
				}
				
			}
		}
		
	}
}
