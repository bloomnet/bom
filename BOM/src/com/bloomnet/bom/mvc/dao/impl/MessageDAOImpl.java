/**
 * 
 */
package com.bloomnet.bom.mvc.dao.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.Oactivitytype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.StsMessage;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers;
import com.bloomnet.bom.common.util.DateUtil;

/**
 * @author Danil Svirchtchev
 *
 */
@Transactional
@Repository("messageDAO")  
public class MessageDAOImpl implements MessageDAO {
	
	@Autowired private SessionFactory sessionFactory;
	
    // Injected DAO implementations
    @Autowired private UserDAO  userDAO;	
    @Autowired private ShopDAO  shopDAO;
    @Autowired private OrderDAO orderDAO;
   // @Autowired private MessagingService messagingService;

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#deleteMessage(java.lang.Long)
	 */
	@Override
	public int deleteMessage(Long messageId) throws HibernateException {
		final ActMessage m_msg = (ActMessage) sessionFactory.getCurrentSession().get( ActMessage.class, messageId );
		final int result = (int) m_msg.getOrderActivityId();
		sessionFactory.getCurrentSession().delete( m_msg );
		return result;
	}
	

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getCommethodByDesc(java.lang.String)
	 */
	@Override
	public Commmethod getCommethodByDesc(String desc) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Commmethod> cq = cb.createQuery(Commmethod.class);

        Root<Commmethod> root = cq.from(Commmethod.class);
        cq.select(root);
		cq.where(cb.equal(root.get("description"), desc));
        
        Commmethod commMethod = session.createQuery(cq).uniqueResult();
		
        
        
		return commMethod;
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getCommethodByCommId(java.lang.Byte)
	 */
	@Override
	public Commmethod getCommethodByCommId(byte commId) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Commmethod> cq = cb.createQuery(Commmethod.class);

        Root<Commmethod> root = cq.from(Commmethod.class);
        cq.select(root);
		cq.where(cb.equal(root.get("commMethodId"), commId));
        
        Commmethod commMethod = session.createQuery(cq).uniqueResult();
		
        
		
		return commMethod;
	}
	
	@SuppressWarnings("unused")
	@Override
	public String getLastMessageSequenceNumber() throws HibernateException {

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<ActMessage> cq = cb.createQuery(ActMessage.class);
        
        Root<ActMessage> root = cq.from(ActMessage.class);
        cq.select(root);
		cq.where(cb.equal(root.get("messageFormat"), BOMConstants.ORDER_TYPE_TEL));
		cq.orderBy(cb.asc(root.get("orderActivityId")));

        List<ActMessage> actMessages = session.createQuery(cq).setMaxResults(1000).getResultList();
		
        
		
		ActMessage actMessage = new ActMessage();
		
		for(int ii=0; ii<actMessages.size(); ++ii){
			actMessage = actMessages.get(ii);
			if(!String.valueOf(actMessage.getBmtMessageSequenceNumber()).equals("0") && actMessage.getBmtMessageSequenceNumber().length()<6) break;
		}
		if(actMessage != null){
			if(actMessage.getBmtMessageSequenceNumber() != null){
				if(actMessage.getBmtMessageSequenceNumber().equals("99999"))return "0";
				else return String.valueOf(actMessage.getBmtMessageSequenceNumber());
			}
		}else return "0";
		return "0";
	}

	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getCommunicationMethod(java.lang.String)
	 */
	@Override
	public String getCommunicationMethod(String orderNumber) throws HibernateException {
		
		final String result;
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Bomorder> cq = cb.createQuery(Bomorder.class);
        
        Root<Bomorder> root = cq.from(Bomorder.class);
        cq.select(root);
		cq.where(cb.equal(root.get("orderNumber"), orderNumber));

		Bomorder m_order = session.createQuery(cq).uniqueResult();
		
        
        
		if( m_order != null ) {
			
			result = m_order.getCommmethod().getDescription();
		}
		else {
			
			result = null;
		}
		
		return result;
	}
	
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getMessagesByOrderId(java.lang.Long)
	 */
	@Override
	public List<ActMessage> getMessagesByOrderId(Long orderId) throws HibernateException {

		List<ActMessage> result = new ArrayList<ActMessage>();
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
		cq.where(cb.equal(root.get("bomorder.bomorderId"), orderId));

		List<Orderactivity> m_orderActivities = session.createQuery(cq).getResultList();
		
		
		for( Orderactivity m_oredeActivity : m_orderActivities ) {
			
			if ( m_oredeActivity.getActMessage() != null ) {
				result.add( m_oredeActivity.getActMessage() );
			}
		}
		
		
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getMessageTypeByShortDesc(java.lang.String)
	 */
	@Override
	public Messagetype getMessageTypeByShortDesc(String shortDesc) throws HibernateException {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Messagetype> cq = cb.createQuery(Messagetype.class);
        
        Root<Messagetype> root = cq.from(Messagetype.class);
        cq.select(root);
		cq.where(cb.equal(root.get("shortDesc"), shortDesc));

		Messagetype mType = session.createQuery(cq).uniqueResult();
		
        
		
		return mType;
	}


	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getStsById(byte)
	 */
	@Override
	public StsMessage getStsById(byte id) throws HibernateException {
		return (StsMessage) sessionFactory.getCurrentSession().get( StsMessage.class, id );
	
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#persistMessage(com.bloomnet.bom.common.entity.ActMessage)
	 */
	@Override
	public int persistMessage(ActMessage message) throws HibernateException {
		int result = 0;
		
		sessionFactory.getCurrentSession().saveOrUpdate(message);
		result = (int) message.getOrderActivityId();
		
		return result;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getCommmethods()
	 */
	@Override
	public List<Commmethod> getCommmethods() throws HibernateException {
        
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Commmethod> cq = cb.createQuery(Commmethod.class);
        cq.from(Commmethod.class);
        
		List<Commmethod> commMethods = session.createQuery(cq).getResultList();
		
        
		
		return commMethods;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#getMessageTypes()
	 */
	@Override
	public List<Messagetype> getMessageTypes() throws HibernateException {
        
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Messagetype> cq = cb.createQuery(Messagetype.class);
        cq.from(Messagetype.class);
        
		List<Messagetype> messageTypes = session.createQuery(cq).getResultList();
		
        
		
		return messageTypes;
	}

	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#persistMessageAndActivity(java.lang.Long, com.bloomnet.bom.common.jaxb.fsi.GeneralIdentifiers, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public int persistMessageAndActivity( Long userId,
										   GeneralIdentifiers identifiers, 
										   String xml, 
										   String messageTypeDesc,
									   	   String status, 
									   	   String commmethodDesc, 
									   	   String sendingShopCode,
									   	   String receivingShopCode, 
									   	   String messageText ) throws HibernateException {
		
		User user = userDAO.getUserById(userId);

		if ( user == null ) {
			
			user = userDAO.getUserByUserName( UserDAO.INTERNAL_USER );
		}
		
		ActMessage message = new ActMessage();

		final Shop sendingShop   = shopDAO.getShopByCode(sendingShopCode);
		final Shop receivingShop = shopDAO.getShopByCode(receivingShopCode);
		final StsMessage stsMessage = getStsById( Byte.parseByte( status ) );
		final Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc(BOMConstants.ACT_MESSAGE);
		final Bomorder bomorder = orderDAO.getOrderByOrderNumber( identifiers.getBmtOrderNumber() );
		final Messagetype messagetype = getMessageTypeByShortDesc( messageTypeDesc );
		final Commmethod commmethod = getCommethodByDesc( commmethodDesc );
		// messageFormat is equivalent to orderTYpe in bomorder
		final byte messageFormat = BOMConstants.ORDER_TYPE_BMT; 

		Orderactivity orderactivity = new Orderactivity(oactivitytype);
		orderactivity.setActMessage(message);
		orderactivity.setUser(user);
		orderactivity.setBomorder( bomorder );
		orderactivity.setCreatedDate( new Date() );
			
		message.setOrderactivity( orderactivity );
		message.setMessagetype( messagetype );
		message.setCommmethod( commmethod );
		message.setMessageXml( xml );
		message.setShopByReceivingShopId( receivingShop );
		message.setShopBySendingShopId( sendingShop );
		message.setMessageFormat( messageFormat );
		message.setStsMessage( stsMessage );
		message.setMessageText( messageText );
		message.setBmtMessageSequenceNumber( identifiers.getBmtSeqNumberOfOrder() );
		
		sessionFactory.getCurrentSession().persist( orderactivity );

		int messageId = persistMessage( message );
		
		return messageId;
	}
	
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#updateMessageStatus(java.lang.Long, java.lang.String)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void updateMessageStatus( Long messageId, String status ) throws HibernateException {
		
		String q = "update ActMessage m set m.stsMessage="+status+" where m.orderActivityId="+messageId;
		Query<ActMessage> query = sessionFactory.getCurrentSession().createQuery( q );
		query.executeUpdate();
	}
	
	public ActMessage getMessageByID(long id) {
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<ActMessage> cq = cb.createQuery(ActMessage.class);
        
        Root<ActMessage> root = cq.from(ActMessage.class);
        cq.select(root);
		cq.where(cb.equal(root.get("orderActivityId"), id));
		
		ActMessage actMessage = session.createQuery(cq).getSingleResult();
		
		return actMessage;
	}
		
	/* (non-Javadoc)
	 * @see com.bloomnet.bom.common.dao.MessageDAO#updateMessageStatus(com.bloomnet.bom.common.entity.ActMessage, java.lang.String)
	 */
	@Override
	public void updateMessageStatus( ActMessage message, String status ) throws HibernateException {
		
		final StsMessage stsMessage = getStsById( Byte.parseByte( status ) );
		final Oactivitytype oactivitytype = orderDAO.getActivityTypeByDesc( BOMConstants.ACT_MESSAGE );

		Orderactivity orderactivity = new Orderactivity(oactivitytype);
		orderactivity.setActMessage( message );
		orderactivity.setUser( message.getOrderactivity().getUser() );
		orderactivity.setBomorder( message.getOrderactivity().getBomorder() );
		orderactivity.setCreatedDate( new Date() );
		orderactivity.setActMessage( message );
		orderactivity.setCreatedDate( new Date() );
		
		sessionFactory.getCurrentSession().persist( orderactivity );
		
		message = getMessageByID(message.getOrderActivityId());
			
		message.setOrderactivity( orderactivity );
		message.setStsMessage( stsMessage );
		persistMessage( message );
	}
	
	public List<ActMessage> getMessageActivity(Bomorder bomorder)throws HibernateException{

		List<ActMessage> result = new ArrayList<ActMessage>();

		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<Orderactivity> cq = cb.createQuery(Orderactivity.class);
        
        Root<Orderactivity> root = cq.from(Orderactivity.class);
        cq.select(root);
		cq.where(cb.equal(root.get("bomorder"), bomorder));
        
		List<Orderactivity> orderActivities = session.createQuery(cq).getResultList();
		

		if ( ! orderActivities.isEmpty() ) {

			Collections.sort ( orderActivities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			Collections.reverse( orderActivities );

			for(Orderactivity activity : orderActivities){

				if( activity != null ) {

					ActMessage message = activity.getActMessage();

					if ( message != null ) {
						result.add(message);
					}
				}
			}
		}
		
		
		return result;
	}
	

	public List<ActMessage> getMessageByBmtMessageSequenceNumber(String bmtMessageSequenceNumber)throws HibernateException{
		
		Session session = sessionFactory.getCurrentSession();
		
		CriteriaBuilder cb = session.getCriteriaBuilder();
		CriteriaQuery<ActMessage> cq = cb.createQuery(ActMessage.class);
        
        Root<ActMessage> root = cq.from(ActMessage.class);
        cq.select(root);
		cq.where(cb.equal(root.get("bmtMessageSequenceNumber"), bmtMessageSequenceNumber));
        
		List<ActMessage> actMessages = session.createQuery(cq).getResultList();
		
        
		
		return actMessages;
	}
	
	@Override
	public boolean isUniqueMessage(String bmtMessageSequenceNumber, String messageText)throws Exception {
		
		boolean isNew = true;
		
		List<ActMessage> messages= getMessageByBmtMessageSequenceNumber(bmtMessageSequenceNumber);
		
		for(ActMessage message: messages){
			if ((message.getBmtMessageSequenceNumber().equals(bmtMessageSequenceNumber))
				&&(message.getMessageText().equals(messageText))){
				
				return false;
			}
		}
		
		
		return isNew;
	}
	
	@SuppressWarnings({ "unused", "unchecked" })
	@Override
	public List<BloomNetMessage> getMessagesEnteredToday( Date today ) throws HibernateException {
		
		List<BloomNetMessage> result = new ArrayList<BloomNetMessage>();
		List<ActMessage> actMessages = new ArrayList<ActMessage>();
		
		String todayStr = DateUtil.toXmlNoTimeFormatString(new Date());
		Date todayDate = DateUtil.toDate(todayStr);
		String tomorrowStr = DateUtil.tomorrowsNoTimeDate();
		Date tomorrow = DateUtil.toDate(tomorrowStr);

		String str = "from ActMessage " +
				"LEFT OUTER JOIN orderActivity oa ON ar.OrderActivity_ID = oa.OrderActivity_ID " +
				"INNER JOIN messagetype mt on ar.MessageType_ID = mt.MessageType_ID " +
				"where " +
				" oa.OActivityType_ID = 3" +
				" and " +
				" oa.User_ID =2 " +
				"and " +
				"ar.ReceivingShop_ID=19529" +
				"and " +
				"createddate >= :today and createddate < :tomorrow";   
		

		Query<ActMessage> query = sessionFactory.getCurrentSession().createSQLQuery(str)
		.addEntity(ActMessage.class); 
		query.setParameter("today", todayDate);
		query.setParameter("tomorrow", tomorrow);
		

		actMessages = query.list();
		for( ActMessage actMessage: actMessages){
			/*BloomNetMessage message = messagingService.buildMessageObject(actMessage);
			result.add(message);*/
		}

		return result;

	}
}
