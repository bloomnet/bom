package com.bloomnet.bom.mvc.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.common.jaxb.fsi.BloomNetMessage;
import com.bloomnet.bom.common.jaxb.fsi.MessageDlcf;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.MessageFactory;
import com.bloomnet.bom.mvc.businessobjects.WebAppActMessage;

/**
 * Validator for Master Order
 * 
 * @author Danil Svirchtchev
 */
public class MessageValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(MessageValidator.class);
	
	private MessageFactory msgFactory = MessageFactory.getInstance();
	
	public static final String DATE_FORMAT = "MM/dd/yy";

	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppActMessage.class);
	}

	/**
	 * Validates the ApplicationUser command object. Ensures that a password and
	 * username are specified.
	 * 
	 * @see com.bloomnet.bom.mvc.businessobjects.WebAppUser
	 */
	public void validateForm( Object target, Errors errors ) {

		WebAppActMessage msg = (WebAppActMessage) target;

		final String messageTypeId = msg.getMessageTypeId();

		if ((messageTypeId == null) || messageTypeId.equals("")) {
		    
			errors.rejectValue("messageTypeId", "error.message.type.none");
		}
		else {
		    
		   validateDeliveryConfirmationUserInput( msg, errors ); 
		}
		
		if ( msg.isSent() ) {
			
			errors.rejectValue( "sent", "error.message.processed");
		}
	}

	
    private void validateDeliveryConfirmationUserInput( WebAppActMessage msg, Errors errors  ) {
        
        BloomNetMessage m_message = null;
        
        try {
            
            m_message = msgFactory.createMessageObjectFromMessageType( msg.getMessageTypeId() );
            
            if( m_message.getClass() == MessageDlcf.class ) {
                
                final String signature = msg.getSignature();
                
                if ((signature == null) || signature.equals("")) {
                    
                    errors.rejectValue("signature", "error.message.type.signature");
                }
                
                validateDate( msg, errors );
            }
            else {
                
                final String text = msg.getMessageText();
                
                if ((text == null) || text.equals("")) {
                    
                    errors.rejectValue("messageText", "error.logacall.notext");
                }
            }
            
        } catch ( Exception e ) {
            
            logger.error( e );
            errors.reject( "java.exception.message", new Object[] { e.getMessage() }, null );
        }
    }

    private void validateDate( WebAppActMessage msg, Errors errors ) {
        
        final String date = msg.getDateOrderDelivered();

        if ( ( date == null) || date.equals("") ) {
            
            errors.rejectValue("dateOrderDelivered", "error.message.type.deliveryDate");
            return;
        }
        
        // set date format, this can be changed to whatever format
        // you want, MM-dd-yyyy, MM.dd.yyyy, dd.MM.yyyy etc.
        SimpleDateFormat sdf = new SimpleDateFormat( DATE_FORMAT );
         
        // declare and initialize testDate variable, this is what will hold
        // our converted string
        Date testDate = null;

        // we will now try to parse the string into date form
        try {
            
            testDate = sdf.parse(date);
        }
        catch ( ParseException e ) {
            
            errors.rejectValue( "dateOrderDelivered", "error.message.type.deliveryDate.format", new Object[] { DATE_FORMAT }, null );
            return;
        }

        // dateformat.parse will accept any date as long as it's in the format
        // you defined, it simply rolls dates over, for example, december 32
        // becomes jan 1 and december 0 becomes november 30
        // This statement will make sure that once the string
        // has been checked for proper formatting that the date is still the
        // date that was entered, if it's not, we assume that the date is invalid
        if ( ! sdf.format( testDate ).equals( date ) ) {
            
            errors.rejectValue("dateOrderDelivered", "error.message.type.deliveryDate");
            return;
        }
        
        msg.setDateOrderDeliveredParsed( DateUtil.toXmlFormatString(testDate) );
    }
}
