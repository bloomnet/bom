package com.bloomnet.bom.mvc.validators;

import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

public class ScrubComMethod extends ScrubData {
	
	public WebAppOrder scrubComMethod(WebAppOrder order){
		
		final String comMethod = order.getMethodToSend().getDescription();
		final String faxNumber = order.getFaxConfirmation();
		
		String scrubbedComMethod = "";
		String scrubbedFaxNumber = "";
		
		if(comMethod != null) scrubbedComMethod = scrubData(comMethod);
		if(faxNumber != null) scrubbedFaxNumber = allowNumbersOnly(faxNumber);
		
		order.getMethodToSend().setDescription(scrubbedComMethod);
		order.setFaxConfirmation(scrubbedFaxNumber);
		
		return order;
		
	}
}
