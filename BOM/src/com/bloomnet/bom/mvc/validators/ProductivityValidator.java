package com.bloomnet.bom.mvc.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;

/**
 * Validator for Agent Productivity
 * 
 * @author BloomNet Technologies
 */
public class ProductivityValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(ProductivityValidator.class);
	
	
	public static final String DATE_FORMAT = "MM/dd/yyyy";

	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppUser.class);
	}

	/**
	 * Validates the ApplicationUser command object. Ensures that a password and
	 * username are specified.
	 * 
	 * @see com.bloomnet.bom.mvc.businessobjects.WebAppUser
	 */
	public void validateForm( Object target, Errors errors ) {

		WebAppUser user = (WebAppUser) target;

		String dateStr = user.getSelectedDate();

		if ((dateStr == null) || dateStr.equals("")) {
		    
			errors.rejectValue("selectedDate", "error.message.type.none");
		}
		else {
		    
			validateDate( user, errors ); 
		}
		
	}

	
   

    private void validateDate( WebAppUser user, Errors errors ) {
        
        final String date = user.getSelectedDate();

        if ( ( date == null) || date.equals("") ) {
            
            errors.rejectValue("selectedDate", "error.message.type.deliveryDate");
            return;
        }
        
        // set date format, this can be changed to whatever format
        // you want, MM-dd-yyyy, MM.dd.yyyy, dd.MM.yyyy etc.
        SimpleDateFormat sdf = new SimpleDateFormat( DATE_FORMAT );
         
        // declare and initialize testDate variable, this is what will hold
        // our converted string
        Date testDate = null;

        // we will now try to parse the string into date form
        try {
            
            testDate = sdf.parse(date);
        }
        catch ( ParseException e ) {
            
            errors.rejectValue( "selectedDate", "error.message.type.deliveryDate.format", new Object[] { DATE_FORMAT }, null );
            return;
        }

        // dateformat.parse will accept any date as long as it's in the format
        // you defined, it simply rolls dates over, for example, december 32
        // becomes jan 1 and december 0 becomes november 30
        // This statement will make sure that once the string
        // has been checked for proper formatting that the date is still the
        // date that was entered, if it's not, we assume that the date is invalid
        if ( ! sdf.format( testDate ).equals( date ) ) {
            
            errors.rejectValue("selectedDate", "error.message.type.deliveryDate");
            return;
        }
        
        user.setSelectedDate( DateUtil.toXmlFormatString(testDate) );
    }
}
