package com.bloomnet.bom.mvc.validators;

import java.util.ArrayList;
import java.util.List;

import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

public class ScrubWebAppOrder extends ScrubData{
	
	public WebAppOrder scrubWebAppOrder(WebAppOrder order){
	
		final String address1 = order.getBean().getRecipientAddress1();
		final String address2 = order.getBean().getRecipientAddress2();
		final String firstName = order.getBean().getRecipientFirstName();
		final String lastName = order.getBean().getRecipientLastName();
		final String phone = order.getBean().getRecipientPhoneNumber();
		final String state = order.getBean().getRecipientState();
		final List<OrderProductInfoDetails> products = order.getBean().getProducts();
		final String cardMessage = order.getBean().getCardMessage();
		final String specialInstructions = order.getBean().getSpecialInstruction();
		
		String scrubbedAddress1 = "";
		String scrubbedAddress2 = "";
		String scrubbedFirstName = "";
		String scrubbedLastName = "";
		String scrubbedPhone = "";
		String scrubbedState = "";
		String scrubbedCardMessage = "";
		String ScrubbedSpecialInstructions = "";
		List<OrderProductInfoDetails> scrubbedProducts = new ArrayList<OrderProductInfoDetails>();
		
		if(address1 != null) scrubbedAddress1 = scrubData(address1);
		if(address2 != null) scrubbedAddress2 = scrubData(address2);
		if(firstName != null) scrubbedFirstName = scrubData(firstName);
		if(lastName != null) scrubbedLastName = scrubData(lastName);
		if(phone != null) scrubbedPhone = allowNumbersOnly(phone);
		if(state != null) scrubbedState = scrubData(state);
		if(cardMessage != null) scrubbedCardMessage = scrubData(cardMessage);
		if(specialInstructions != null) ScrubbedSpecialInstructions = scrubData(specialInstructions);
		
		for(OrderProductInfoDetails product : products){
			
			final String productCode = product.getProductCode();
			final String productDescription = product.getProductDescription();
			
			String scrubbedProductCode = "";
			String scrubbedProductDescription = "";
			
			if(productCode != null) scrubbedProductCode = scrubData(productCode);
			if(productDescription != null) scrubbedProductDescription = scrubData(productDescription);
			
			product.setProductCode(scrubbedProductCode);
			product.setProductDescription(scrubbedProductDescription);
			
			scrubbedProducts.add(product);
			
		}
		
		order.getBean().setRecipientAddress1(scrubbedAddress1);
		order.getBean().setRecipientAddress2(scrubbedAddress2);
		order.getBean().setRecipientFirstName(scrubbedFirstName);
		order.getBean().setRecipientLastName(scrubbedLastName);
		order.getBean().setRecipientPhoneNumber(scrubbedPhone);
		order.getBean().setRecipientState(scrubbedState);
		order.getBean().setProducts(scrubbedProducts);
		order.getBean().setCardMessage(scrubbedCardMessage);
		order.getBean().setSpecialInstruction(ScrubbedSpecialInstructions);
		
		return order;
	}
}
