package com.bloomnet.bom.mvc.validators;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;

/**
 * Validator for Master Order
 * 
 * @author Danil Svirchtchev
 */
public class MasterOrderValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(MasterOrderValidator.class);
	
	
	public static final String DATE_FORMAT = "MM/dd/yyyy";
	

	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppOrder.class);
	}

	/**
	 * Validates the ApplicationUser command object. Ensures that a password and
	 * username are specified.
	 * 
	 * @see com.bloomnet.bom.mvc.businessobjects.WebAppUser
	 */
	public void validateForm( Object target, Errors errors ) {

		WebAppOrder order = (WebAppOrder) target;
		
		if ( order.getMethodToSend() != null && "fax".equalsIgnoreCase( order.getMethodToSend().getDescription() ) ) {
			
			final String faxNumber = order.getFaxConfirmation();
			
			if ( ( faxNumber == null ) || faxNumber.equals("") ) {
				
				errors.rejectValue("faxConfirmation", "error.sendmethod.fax.none");
				return;
			}
			else {
				
				try {
					
					Long.parseLong( faxNumber.trim() );
				} 
				catch ( NumberFormatException e ) {
					
					errors.reject( "error.sendmethod.fax.invalid", new Object[] { faxNumber }, null );
				}
			}
		}
		
		if ( "editOrder".equals( order.getActionStr() ) ) {
			
			validateDate( order, errors );
		}
	}

	
    private void validateDate( WebAppOrder order, Errors errors ) {
        
        final String date = order.getBean().getDeliveryDate();

        if ( ( date == null) || date.equals("") ) {
            
            errors.rejectValue("bean.deliveryDate", "error.order.deliveryDate");
            return;
        }
        
        // set date format, this can be changed to whatever format
        // you want, MM-dd-yyyy, MM.dd.yyyy, dd.MM.yyyy etc.
        SimpleDateFormat sdf = new SimpleDateFormat( DATE_FORMAT );
         
        // declare and initialize testDate variable, this is what will hold
        // our converted string
        Date testDate = null;

        // we will now try to parse the string into date form
        try {
            
            testDate = sdf.parse(date);
        }
        catch ( ParseException e ) {
            
            errors.rejectValue( "bean.deliveryDate", "error.order.deliveryDate.format", new Object[] { DATE_FORMAT }, null );
            return;
        }

        // dateformat.parse will accept any date as long as it's in the format
        // you defined, it simply rolls dates over, for example, december 32
        // becomes jan 1 and december 0 becomes november 30
        // This statement will make sure that once the string
        // has been checked for proper formatting that the date is still the
        // date that was entered, if it's not, we assume that the date is invalid
        if ( ! sdf.format( testDate ).equals( date ) ) {
            
            errors.rejectValue("bean.deliveryDate", "error.order.deliveryDate");
            return;
        }
        
        order.getBean().setNewDeliveryDate( testDate );
    }
}
