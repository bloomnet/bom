package com.bloomnet.bom.mvc.validators;

import org.apache.log4j.Logger;
import org.springframework.validation.Errors;

import com.bloomnet.bom.mvc.businessobjects.WebAppActLogacall;

/**
 * Validator for Master Order
 * 
 * @author Danil Svirchtchev
 */
public class ActLogacallValidator extends AbstractValidator {

	// Define a static logger variable
	static Logger logger = Logger.getLogger(ActLogacallValidator.class);

	public boolean supports(Class<?> clazz) {

		return clazz.equals(WebAppActLogacall.class);
	}

	/**
	 * Validates the ApplicationUser command object. Ensures that a password and
	 * username are specified.
	 * 
	 * @see com.bloomnet.bom.mvc.businessobjects.WebAppUser
	 */
	public void validateForm(Object target, Errors errors) {

		WebAppActLogacall logacall = (WebAppActLogacall) target;

		final String reason = logacall.getDescription();
		final String text = logacall.getLogAcallText();

		if ((text == null) || text.equals("")) {
			errors.rejectValue("logAcallText", "error.logacall.notext");
		}
		
		if ((reason == null) || reason.equals("")) {
			errors.rejectValue("description", "error.logacall.nodescription");
		}
	}
}
