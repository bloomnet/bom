package com.bloomnet.bom.mvc.jms;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.List;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.ObjectMessage;
import javax.jms.QueueBrowser;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.activemq.ActiveMQQueueBrowser;
import org.apache.activemq.command.ActiveMQObjectMessage;
import org.apache.activemq.command.ActiveMQTextMessage;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.JmsException;
import org.springframework.jms.core.BrowserCallback;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;

@Service("messageProducer")
@Transactional(propagation = Propagation.SUPPORTS)
public class MessageProducer {
	
	static final Logger logger = Logger.getLogger(MessageProducer.class);

	@Autowired
	  JmsTemplate jmsProducer;
	
	
	public MessageProducer() {
	}

/**
 * Send message to different virtual queue
 * @param destination
 * @param message
 * @param orderNumber
 * @param sendingShopCode
 * @param deliveryDate
 * @param city
 * @param zip
 * @param occasionCode
 * @param skillset
 * @param messageType
 * @param orderType
 */
  @Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
  public void produceMessage(String destination, String message, final String orderNumber,final String sendingShopCode,final String deliveryDate,
		  final String city,final String zip,final String occasionCode, final String skillset,final String messageType, final String orderType, final String timeZone) {

	  final String msg = message;

	  try {
		jmsProducer.send(destination,new MessageCreator(){
			  public Message createMessage(Session session)throws JMSException{

				  String ORDERNUMBER = "";
				  String SENDINGSHOP = "";
				  String DELIVERYDATE="";
				  String CITY="";
				  String ZIP="";
				  String OCCASSIONCODE="";
				  String SKILLSET="";
				  String MESSAGETYPE="";
				  String ORDERTYPE="";
				  String TIMEZONE="";


				  ORDERNUMBER = "ORDERNUMBER";
				  String value1 = orderNumber;

				  SENDINGSHOP = "SENDINGSHOP";
				  String value2 = sendingShopCode;

				  DELIVERYDATE = "DELIVERYDATE";
				  String value3 = deliveryDate;

				  CITY = "CITY";
				  String value4 = city;

				  ZIP = "ZIP";
				  String value5 =zip;

				  OCCASSIONCODE = "OCCASSIONCODE";
				  String value6  = occasionCode;

				  SKILLSET="SKILLSET";
				  String value7 = skillset;

				  MESSAGETYPE = "MESSAGETYPE";
				  String value8 = messageType;

				  ORDERTYPE = "ORDERTYPE";
				  String value9 = orderType;
				  
				  TIMEZONE = "TIMEZONE";
				  String value10 = timeZone;


				  TextMessage textMessage = session.createTextMessage(msg);
				  textMessage.setStringProperty(ORDERNUMBER, value1);
				  textMessage.setStringProperty(SENDINGSHOP, value2);
				  textMessage.setStringProperty(DELIVERYDATE, value3);
				  textMessage.setStringProperty(CITY, value4);
				  textMessage.setStringProperty(ZIP, value5);
				  textMessage.setStringProperty(OCCASSIONCODE, value6);
				  textMessage.setStringProperty(SKILLSET, value7);
				  textMessage.setStringProperty(MESSAGETYPE, value8);
				  textMessage.setStringProperty(ORDERTYPE, value9);
				  textMessage.setStringProperty(TIMEZONE, value10);


				  return textMessage;
			  }
		  });

		  if(logger.isDebugEnabled()) {   	
			  logger.debug("sending message to " + destination);
		  }
	} catch (JmsException e) {
		logger.error("Unable to send message to destination : " + destination + "\n Received exception: " + e.getMessage());

	}	
  }
  
	
	  

	  
	
	/**
	 * Send order to Faxoutbound destination
	 * @param destination
	 * @param message
	 * @param orderNumber
	 * @param receivingShop
	 * @param deliveryDate
	 * @param occasionCode
	 */
	public void produceOutboundMessage(String destination, String message, final String orderNumber, final String receivingShop, final String deliveryDate, final String occasionCode, final String contactInfo) {
		  final String msg = message;
				
		  try {
			jmsProducer.send(destination,new MessageCreator(){
				  public Message createMessage(Session session)throws JMSException{
					  String ORDERNUMBER = "";
					  String RECEIVINGSHOP = "";
					  String DELIVERYDATE="";
					  String OCCASSIONCODE="";


					  ORDERNUMBER = "ORDERNUMBER";
					  String value1 = orderNumber;

					  RECEIVINGSHOP = "RECEIVINGSHOP";
					  String value2 = receivingShop;


					  DELIVERYDATE = "DELIVERYDATE";
					  String value3 = deliveryDate;

					  OCCASSIONCODE = "OCCASSIONCODE";
					  String value4  = occasionCode;
					  
					  String CONTACTINFO = "CONTACTINFO";
					  String value5 = contactInfo;

					  TextMessage textMessage = session.createTextMessage(msg);
					  textMessage.setStringProperty(ORDERNUMBER, value1);
					  textMessage.setStringProperty(RECEIVINGSHOP, value2);
					  textMessage.setStringProperty(DELIVERYDATE, value3);
					  textMessage.setStringProperty(OCCASSIONCODE, value4);
					  textMessage.setStringProperty(CONTACTINFO, value5);


					  return textMessage;
				  }
			  });

			  if(logger.isDebugEnabled()) {   	
				  logger.debug("sending message to " + destination);
			  }
		} catch (JmsException e) {
			logger.error("Unable to send message to destination : " + destination + "\n Received exception: " + e.getMessage());

		}	
	}

/**
 * sends messageOnOrder to NEW-MESSAGES and MESSAGES destinations
 * @param destination
 * @param message
 * @param orderNumber
 */
@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
public void produceMessage(String destination, String message, final String orderNumber,final String sendingShopCode,final String messageType,final String skillset) {
	  final String msg = message;
			

	  try {
		jmsProducer.send(destination,new MessageCreator(){
			  public Message createMessage(Session session)throws JMSException{
				  String ORDERNUMBER = "";
				  String SENDINGSHOP = "";
				  String MESSAGETYPE = "";
				  String SKILLSET = "";

				  ORDERNUMBER = "ORDERNUMBER";
				  String value1 = orderNumber;

				  SENDINGSHOP = "SENDINGSHOP";
				  String value2 = sendingShopCode;

				  MESSAGETYPE = "MESSAGETYPE";
				  String value3 = messageType;

				  SKILLSET="SKILLSET";
				  String value4 = skillset;

				  TextMessage textMessage = session.createTextMessage(msg);
				  textMessage.setStringProperty(ORDERNUMBER, value1);
				  textMessage.setStringProperty(SENDINGSHOP, value2);
				  textMessage.setStringProperty(MESSAGETYPE, value3);
				  textMessage.setStringProperty(SKILLSET, value4);


				  return textMessage;
			  }
		  });

		  if(logger.isDebugEnabled()) {   	
			  logger.debug("sending message to " + destination);
		  }
		} catch (JmsException e) {
			logger.error("Unable to send message to destination : " + destination + "\n Received exception: " + e.getMessage());
	
		}	
	}

@Transactional(propagation=Propagation.REQUIRED, rollbackFor=Exception.class)
public void produceTFMessage(String destination, String message, final String orderNumber,final String sendingShopCode,final String messageType,final String bundleId) {
	  final String msg = message;
			

	  try {
		jmsProducer.send(destination,new MessageCreator(){
			  public Message createMessage(Session session)throws JMSException{
				  String ORDERNUMBER = "";
				  String SENDINGSHOP = "";
				  String MESSAGETYPE = "";
				  String BUNDLEID = "";

				  ORDERNUMBER = "ORDERNUMBER";
				  String value1 = orderNumber;

				  SENDINGSHOP = "SENDINGSHOP";
				  String value2 = sendingShopCode;

				  MESSAGETYPE = "MESSAGETYPE";
				  String value3 = messageType;

				  BUNDLEID="BUNDLEID";
				  String value4 = bundleId;

				  TextMessage textMessage = session.createTextMessage(msg);
				  textMessage.setStringProperty(ORDERNUMBER, value1);
				  textMessage.setStringProperty(SENDINGSHOP, value2);
				  textMessage.setStringProperty(MESSAGETYPE, value3);
				  textMessage.setStringProperty(BUNDLEID, value4);


				  return textMessage;
			  }
		  });

		  if(logger.isDebugEnabled()) {   	
			  logger.debug("sending message to " + destination);
		  }
		} catch (JmsException e) {
			logger.error("Unable to send message to destination : " + destination + "\n Received exception: " + e.getMessage());
	
		}	
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<String>  BrowseQueue(){
		
		List<String> tloOrders  = new ArrayList<String>();

		tloOrders = (List<String>)jmsProducer.browse("TLO", new BrowserCallback(){

			@Override
			public List<String> doInJms(Session arg0, QueueBrowser browser)
					throws JMSException {
				
				List<String> orders  = new ArrayList<String>();

				Enumeration e = browser.getEnumeration();
				if (e instanceof ActiveMQQueueBrowser) {
					ActiveMQQueueBrowser activemqbrowser = (ActiveMQQueueBrowser) e;

					while (activemqbrowser.hasMoreElements()) {
						Object o = activemqbrowser.nextElement();
						
						if (o instanceof TextMessage) {
							TextMessage message = (TextMessage) o;
							
							String order = message.getStringProperty("ORDERNUMBER");
							System.out.println(order);
							
							orders.add(order);
				
						}
					}
				}
				return orders;
			}
		});
		
		return tloOrders;

	}

	public void setJmsProducer(JmsTemplate jmsProducer2) {
		// TODO Auto-generated method stub
		
	}
	
}

