package com.bloomnet.bom.mvc.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;
import javax.jms.TextMessage;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.listener.SessionAwareMessageListener;

import com.bloomnet.bom.mvc.service.EmailService;

public class EmailMessageListener implements SessionAwareMessageListener<Message> {

	// Define a static logger variable
	static final Logger logger = Logger.getLogger(EmailMessageListener.class);

	// Injected dependency
	@Autowired private EmailService emailService;
	
	@Override
	public void onMessage(Message message, Session session) throws JMSException {

		String order = "";

		TextMessage msg = (TextMessage) message;
		try {
			if (logger.isDebugEnabled()) {

				logger.debug("***************EmailMessageListener onMessage***************");
				logger.debug("received: " + msg.getText());
			}
			order = msg.getText();
			String orderId = msg.getStringProperty("ORDERNUMBER");
			
			
			
			String email = "";
			email = msg.getStringProperty("CONTACTINFO");
			
			
			
			
			emailService.sendEmail(order,  email);

		} catch (JMSException ex) {
			
			ex.printStackTrace();
	
			
		} catch (Exception e) {
			logger.error("Unable to send email,, reason: " + e.getMessage());
		}
	}

	


	
}
