package com.bloomnet.bom.mvc.test;

import java.util.List;

import javax.jms.*;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.activemq.command.ActiveMQQueue;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.jms.core.JmsTemplate;

import com.bloomnet.bom.mvc.jms.TLOConsumer;

public class TLOConsumerTest {
	
	private static ConnectionFactory cf = new ActiveMQConnectionFactory("tcp://localhost:61616");
	private static Connection conn = null;
	private static Session session = null;
	//int occassion = 4;
	//String sSelector = "occassion  = '" + occassion+ "'";

	TLOConsumer consumer = new TLOConsumer();
	
	@Before
	public void setup(){
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-test.xml"));

		JmsTemplate bmttelConsumer = (JmsTemplate) factory.getBean("bmttelConsumer");
		
		consumer.setBmttelConsumer(bmttelConsumer);


		try {
			conn =cf.createConnection();
			session=conn.createSession(false,Session.AUTO_ACKNOWLEDGE);
			Destination destination=new ActiveMQQueue("TLO");


			MessageProducer producer=session.createProducer(destination);
			producer.setPriority(8);
			
			TextMessage message=session.createTextMessage();
			message.setText("Test");
			
			message.setStringProperty("SKILLSET", "3");
			message.setStringProperty("ORDERNUMBER", "ABC123");


			producer.send(message);
			} catch(JMSException e){
			// handleexception?
			} finally{
			try {
			if (session!=null){
			session.close();
			}
			if (conn!=null){
			conn.close();
			}
			} catch(JMSException ex){
			}
			
			}
		
	}
	
	@Test
	public void testConsumeMessage() throws Exception  {
		
		
		
		String skillset = "4";
		//List<String> messageProperties = consumer.consumeMessage(skillset);
		
	//	Assert.assertNotNull("message is null", messageProperties);

	}
	
	@Test
	public void testBrowseQueueNotPresent()  {
		
		String message = new String();
		
		
		String skillset = "4";
		 
			//consumer.browseQueue("ORDERNUMBER = 3414936");
		
		List<String> messages = consumer.browseQueueForMessagesOnOrder("ABC123");

		
		//Assert.assertNotNull("message is null", message);

	}
	
	/*@Test
	public void testConsumeMessageEmpty()  {
		
		String message = new String();
		String message2 = new String();

		
		String skillset = "4";
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-test.xml"));
		
		JmsTemplate jmsWSConsumer = (JmsTemplate) factory.getBean("jmsWSConsumer");
		consumer.setJmsTemplate(jmsWSConsumer);
		message = consumer.consumeMessage(skillset );
		
		Assert.assertNotNull("message is null", message);
		message2 = consumer.consumeMessage(skillset);
		
		Assert.assertEquals("message is blank", "", message2);


	}*/
}
