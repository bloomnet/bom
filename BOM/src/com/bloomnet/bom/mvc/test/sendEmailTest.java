package com.bloomnet.bom.mvc.test;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.MailSender;


import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.jaxb.fsi.OrderProductInfoDetails;
import com.bloomnet.bom.mvc.service.AgentService;
import com.bloomnet.bom.mvc.service.impl.AgentServiceImpl;

public class sendEmailTest {
	
	private AgentService agentService;

	@Before
	public void setup(){
		BeanFactory factory = new XmlBeanFactory(new FileSystemResource("WebContent/WEB-INF/BloomNetServices-servlet.xml"));
		agentService = (AgentService) factory.getBean("agentService");   
	  
	}
	
	@Test
	public void sendEmailConfirmationTest(){
		
		MessageOnOrderBean messageOnOrderBean = new MessageOnOrderBean();
		
		Shop fulfillingShop = null;
		fulfillingShop.setShopEmail("jpursoo@bloomnet.net");
		messageOnOrderBean.setFulfillingShop(fulfillingShop );
		messageOnOrderBean.setBmtSeqNumberOfOrder(456543);
		messageOnOrderBean.setCardMessage("best wishes");
		messageOnOrderBean.setDeliveryDate("12/3/2012");
		messageOnOrderBean.setOccasionId("occassion");
		messageOnOrderBean.setOrderNumber("12345");
		List<OrderProductInfoDetails> products = new ArrayList();
		OrderProductInfoDetails product = new OrderProductInfoDetails();
		product.setCostOfSingleProduct("5.99");
		product.setProductCode("245");
		product.setProductDescription("dozen flowers");
		product.setUnits(1);
		products.add(product);
		messageOnOrderBean.setProducts(products);
		messageOnOrderBean.setRecipientAddress1("one old country rd");
		messageOnOrderBean.setRecipientCity("carle place");
		messageOnOrderBean.setRecipientState("NY");
		messageOnOrderBean.setRecipientZipCode("11550");
		messageOnOrderBean.setRecipientFirstName("mark");
		messageOnOrderBean.setRecipientLastName("silver");
		messageOnOrderBean.setSpecialInstruction("dont leave at back door");
		
		
		
		try {
			//agentService.sendEmailConfirmation("12345",messageOnOrderBean);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		System.out.println("email sent");
		
		
	}

	public void setAgentService(AgentService agentService) {
		this.agentService = agentService;
	}
	
	

}
