/**
 * 
 */
package com.bloomnet.bom.mvc.exceptions;

/**
 * @author Administrator
 *
 */
public class UserRoleNoAssignmentException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UserRoleNoAssignmentException(String userName) {
		super("User "+userName+" has no roles");
	}
}
