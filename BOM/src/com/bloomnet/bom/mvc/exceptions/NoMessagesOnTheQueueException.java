/**
 * 
 */
package com.bloomnet.bom.mvc.exceptions;

import com.bloomnet.bom.common.entity.User;

/**
 * @author Danil svirchtchev
 *
 */
public class NoMessagesOnTheQueueException extends Exception {

	private static final long serialVersionUID = 1L;

	/**
	 * @param e
	 */
	public NoMessagesOnTheQueueException( Throwable e ) {
		super(e);
	}
	
	public NoMessagesOnTheQueueException( User user ) {
		super( "There are no orders for "+user.getFirstName()+" "+user.getLastName() );
	}
}
