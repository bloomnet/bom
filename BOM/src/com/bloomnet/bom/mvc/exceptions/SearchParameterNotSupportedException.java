package com.bloomnet.bom.mvc.exceptions;


/**
 * @author Danil svirchtchev
 *
 */
public class SearchParameterNotSupportedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	public SearchParameterNotSupportedException( String searchParameterName ) {
		super( "Search for [ "+searchParameterName+"] is not implemented yet." );
	}
}
