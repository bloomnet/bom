/**
 * 
 */
package com.bloomnet.bom.mvc.exceptions;

/**
 * @author Administrator
 *
 */
public class UserOrRoleNotActiveException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public UserOrRoleNotActiveException(String userOrRoleName) {
		super(userOrRoleName+" is not active");
	}
}
