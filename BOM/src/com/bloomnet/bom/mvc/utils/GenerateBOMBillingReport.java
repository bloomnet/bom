package com.bloomnet.bom.mvc.utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Date;

public class GenerateBOMBillingReport {
	
	public File generateBOMBillingReport(String startDate, String endDate){
			
		SQLData mySQL = new SQLData();
		mySQL.login();
		String statement = "USE bloomnetordermanagement;";
		mySQL.executeStatement(statement);
		
		Writer writer = null;
		String dateString = String.valueOf(new Date().getTime());
		
		File file = null;
		
		try {
			 file = new File("/var/lib/tomcat/webapps/BOMMVC/images/"+dateString+".csv");
			 writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(file), "utf-8"));
			 writer.write("OrderNumber,BOMOrder_ID,pid,ParentOrderNumber,LastName,CreatedDate,User_ID,PaymentAmount,PayTo,Description,BMTShopCode,TFShopCode,FTDShopCode,MOMPOP,ShopName,ShopAddress1,ShopAddress2,ShopPhone,ShopContact,ShopFax,Shop800,ShopEmail,Name,Short_Name,Zip_Code\n");
			 writer.flush();
		} catch (UnsupportedEncodingException e1) {
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String query = "SELECT bomorder.OrderNumber,bomorder.BOMOrder_ID,bomorder.ParentOrder_ID AS pid, "
				+ "(SELECT bomorder.OrderNumber FROM bomorder WHERE BOMOrder_ID = pid) AS ParentOrderNumber, "
				+ "SUBSTRING(OrderXML,(LOCATE('<recipientLastName>',OrderXML)+19),(LOCATE('</recipientLastName>', "
				+ "OrderXML))-LOCATE('<recipientLastName>',OrderXML)-19) AS LastName,bomorder.CreatedDate,orderactivity.User_ID, "
				+ "act_payment.PaymentAmount,act_payment.PayTo,paymenttype.Description, "
				+ "(SELECT ShopCode FROM shopnetwork WHERE Shop_ID = bomorder.ReceivingShop_ID AND Network_ID = 1 LIMIT 1) as BMTShopCode, "
				+ "(SELECT ShopCode FROM shopnetwork WHERE Shop_ID = bomorder.ReceivingShop_ID AND Network_ID = 2 LIMIT 1) as TFShopCode, "
				+ "(SELECT ShopCode FROM shopnetwork WHERE Shop_ID = bomorder.ReceivingShop_ID AND Network_ID = 3 LIMIT 1) as FTDShopCode, "
				+ "(SELECT ShopCode FROM shopnetwork WHERE Shop_ID = bomorder.ReceivingShop_ID AND Network_ID = 4 LIMIT 1) as MOMPOP, "
				+ "shop.ShopName,shop.ShopAddress1,shop.ShopAddress2,shop.ShopPhone,shop.ShopContact,shop.ShopFax,shop.Shop800,shop.ShopEmail,city.Name,state.Short_Name,zip.Zip_Code "
				+ "FROM bomorder "
				+ "INNER JOIN orderactivity ON bomorder.BomOrder_ID = orderactivity.BomOrder_ID "
				+ "INNER JOIN act_payment ON orderactivity.OrderActivity_ID = act_payment.OrderActivity_ID "
				+ "INNER JOIN paymenttype ON act_payment.PaymentType_ID = paymenttype.PaymentType_ID "
				+ "INNER JOIN shop ON bomorder.ReceivingShop_ID = shop.Shop_ID "
				+ "INNER JOIN city ON shop.City_ID = city.City_ID "
				+ "INNER JOIN state ON city.State_ID = state.State_ID "
				+ "INNER JOIN zip ON shop.Zip_ID = zip.Zip_ID "
				+ "WHERE bomorder.CreatedDate >= \""+startDate+"\" AND bomorder.CreatedDate < \""+endDate+"\"AND bomorder.OrderNumber LIKE \"BMT%\";";
		
		ResultSet results = mySQL.executeQuery(query);
		
		try {
			while(results.next()){
				String orderNumber = results.getString(1);
				String orderID = results.getString(2);
				String pid = results.getString(3);
				String parentOrder = results.getString(4);
				String lastName = results.getString(5).replaceAll(",", " ");
				String createdDate = results.getString(6);
				String userID = results.getString(7);
				String paymentAmount = results.getString(8);
				String payTo = results.getString(9).replaceAll(",", " ");
				String description = results.getString(10);
				String BMTCode = results.getString(11);
				String TFCode = results.getString(12);
				String FTDCode = results.getString(13);
				String momPopCode = results.getString(14);
				String shopName = results.getString(15).replaceAll(",", " ");
				String address1 = results.getString(16).replaceAll(",", " ");
				String address2 = results.getString(17);
				if(address2 != null)
					address2 = address2.replaceAll(",", " ");
				String phone = results.getString(18);
				String contact = results.getString(19).replaceAll(",", " ");
				String fax = results.getString(20);
				String shop800 = results.getString(21);
				String email = results.getString(22);
				String city = results.getString(23).replaceAll(",", " ");
				String state = results.getString(24);
				String zip = results.getString(25);
				
				String line = orderNumber+","+orderID+","+pid+","+parentOrder+","+lastName+","+createdDate+","+userID+","+paymentAmount+","+payTo+","+description+","+BMTCode+","+TFCode+","+FTDCode+","+momPopCode+","+shopName+","+address1+","+address2+","+phone+","+contact+","+fax+","+shop800+","+email+","+city+","+state+","+zip+"\n";
				line = line.replaceAll("null", "");
				
				writer.write(line);
				writer.flush();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				writer.close();
				mySQL.closeStatement();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		return file;
	}

}
