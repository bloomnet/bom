package com.bloomnet.bom.mvc.utils;

import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.apache.poi.xssf.usermodel.XSSFCell; 
import org.apache.poi.xssf.usermodel.XSSFRow; 
import org.apache.poi.xssf.usermodel.XSSFSheet; 
import org.apache.poi.xssf.usermodel.XSSFWorkbook; 


public class ActivityDetailReport {
	
	SQLData mySQL = new SQLData();

    public File getActivityDetailReport(String startDate, String endDate){
        mySQL.login();
        String statement = "USE bloomnetordermanagement;";
        mySQL.executeStatement(statement);
        
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd");
        String start = "";
        String end = "";
        try {
			start = sdf2.format(sdf.parse(startDate));
			end = sdf2.format(sdf.parse(endDate));
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
        
        Calendar c = Calendar.getInstance(); 
        c.setTime(new Date()); 
        c.add(Calendar.DATE, 1);
        
        statement = "SET @start := \""+start+" 00:00:00\", @end := \""+end+" 23:59:59\"";
        mySQL.executeStatement(statement);
        
        System.out.println("INITIALIZED AND DATABASE SELECTED");
        String query = "SELECT bo.OrderNumber,user.FirstName,user.LastName,sr.description,bv.Route,oa.CreatedDate FROM act_routing al INNER JOIN orderactivity oa ON al.OrderActivity_ID = oa.OrderActivity_ID INNER JOIN bomorder bo ON oa.BOMOrder_ID = bo.ParentOrder_ID INNER JOIN bomorderview_v2 bv ON bo.ParentOrder_ID = bv.ParentBOMid INNER JOIN user ON oa.User_ID = user.User_ID INNER JOIN sts_routing sr ON al.Sts_Routing_ID = sr.Sts_Routing_ID WHERE oa.User_ID NOT IN (1,2,3) AND oa.CreatedDate >= @start AND oa.CreatedDate < @end AND bo.BOMOrder_ID = bo.ParentOrder_ID AND al.Sts_Routing_ID != 1 ORDER BY bo.OrderNumber,oa.CreatedDate DESC,LastName,FirstName;";
        
        File file = new File("/var/lib/tomcat/webapps/BOMMVC/images/ActivityDetail.xlsx");
        String filePath = file.getPath();
        
        createWorkbook(filePath, query);
        
        return file;
    }
    
    private void createWorkbook(String filePath, String query){
    	try
        {
    		ResultSet queryResults = mySQL.executeQuery(query);
            @SuppressWarnings("resource")
			XSSFWorkbook wb = new XSSFWorkbook();
            XSSFSheet sheet = wb.createSheet("Sheet1");
            
            XSSFRow row = sheet.createRow(0);
            
            XSSFCell cell = row.createCell(0);
            cell.setCellValue("Order Number");
            cell = row.createCell(1);
            cell.setCellValue("First Name");
            cell = row.createCell(2);
            cell.setCellValue("Last Name");
            cell = row.createCell(3);
            cell.setCellValue("Status");
            cell = row.createCell(4);
            cell.setCellValue("Route");
            cell = row.createCell(5);
            cell.setCellValue("Status Created Date");

    		int ii = 1;
    				
            while(queryResults.next())
            {
            	row = sheet.createRow(ii);
            	
                String orderNumber = queryResults.getString("OrderNumber");
                String firstName = queryResults.getString("FirstName");
                String lastName = queryResults.getString("LastName");
                String status = queryResults.getString("description");
                String route = queryResults.getString("Route");
                String created = queryResults.getString("CreatedDate");
                
                cell = row.createCell(0);
                cell.setCellValue(String.valueOf(orderNumber).toString());
                cell = row.createCell(1);
                cell.setCellValue(String.valueOf(firstName).toString());
                cell = row.createCell(2);
                cell.setCellValue(String.valueOf(lastName).toString());
                cell = row.createCell(3);
                cell.setCellValue(String.valueOf(status).toString());
                cell = row.createCell(4);
                cell.setCellValue(route);
                cell = row.createCell(5);
                cell.setCellValue(created);
                ii++;
            }
            
            FileOutputStream fileOut = new FileOutputStream(filePath);
            wb.write(fileOut);
            fileOut.flush();
            fileOut.close();

            }
        catch(IOException e){
            e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}
    }
}
