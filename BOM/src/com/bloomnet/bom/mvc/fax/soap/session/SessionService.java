/**
 * SessionService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.session;

public interface SessionService extends javax.xml.rpc.Service {
    public java.lang.String getSessionServiceSoapAddress();

    public com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_PortType getSessionServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.bloomnet.bom.mvc.fax.soap.session.SessionServiceSoap_PortType getSessionServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
