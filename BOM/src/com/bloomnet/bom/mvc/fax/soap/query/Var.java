/**
 * Var.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class Var  implements java.io.Serializable {
    private java.lang.String attribute;

    private com.bloomnet.bom.mvc.fax.soap.query.VAR_TYPE type;

    private java.lang.String simpleValue;

    private int nValues;

    private java.lang.String[] multipleStringValues;

    private int[] multipleLongValues;

    private double[] multipleDoubleValues;

    public Var() {
    }

    public Var(
           java.lang.String attribute,
           com.bloomnet.bom.mvc.fax.soap.query.VAR_TYPE type,
           java.lang.String simpleValue,
           int nValues,
           java.lang.String[] multipleStringValues,
           int[] multipleLongValues,
           double[] multipleDoubleValues) {
           this.attribute = attribute;
           this.type = type;
           this.simpleValue = simpleValue;
           this.nValues = nValues;
           this.multipleStringValues = multipleStringValues;
           this.multipleLongValues = multipleLongValues;
           this.multipleDoubleValues = multipleDoubleValues;
    }


    /**
     * Gets the attribute value for this Var.
     * 
     * @return attribute
     */
    public java.lang.String getAttribute() {
        return attribute;
    }


    /**
     * Sets the attribute value for this Var.
     * 
     * @param attribute
     */
    public void setAttribute(java.lang.String attribute) {
        this.attribute = attribute;
    }


    /**
     * Gets the type value for this Var.
     * 
     * @return type
     */
    public com.bloomnet.bom.mvc.fax.soap.query.VAR_TYPE getType() {
        return type;
    }


    /**
     * Sets the type value for this Var.
     * 
     * @param type
     */
    public void setType(com.bloomnet.bom.mvc.fax.soap.query.VAR_TYPE type) {
        this.type = type;
    }


    /**
     * Gets the simpleValue value for this Var.
     * 
     * @return simpleValue
     */
    public java.lang.String getSimpleValue() {
        return simpleValue;
    }


    /**
     * Sets the simpleValue value for this Var.
     * 
     * @param simpleValue
     */
    public void setSimpleValue(java.lang.String simpleValue) {
        this.simpleValue = simpleValue;
    }


    /**
     * Gets the nValues value for this Var.
     * 
     * @return nValues
     */
    public int getNValues() {
        return nValues;
    }


    /**
     * Sets the nValues value for this Var.
     * 
     * @param nValues
     */
    public void setNValues(int nValues) {
        this.nValues = nValues;
    }


    /**
     * Gets the multipleStringValues value for this Var.
     * 
     * @return multipleStringValues
     */
    public java.lang.String[] getMultipleStringValues() {
        return multipleStringValues;
    }


    /**
     * Sets the multipleStringValues value for this Var.
     * 
     * @param multipleStringValues
     */
    public void setMultipleStringValues(java.lang.String[] multipleStringValues) {
        this.multipleStringValues = multipleStringValues;
    }


    /**
     * Gets the multipleLongValues value for this Var.
     * 
     * @return multipleLongValues
     */
    public int[] getMultipleLongValues() {
        return multipleLongValues;
    }


    /**
     * Sets the multipleLongValues value for this Var.
     * 
     * @param multipleLongValues
     */
    public void setMultipleLongValues(int[] multipleLongValues) {
        this.multipleLongValues = multipleLongValues;
    }


    /**
     * Gets the multipleDoubleValues value for this Var.
     * 
     * @return multipleDoubleValues
     */
    public double[] getMultipleDoubleValues() {
        return multipleDoubleValues;
    }


    /**
     * Sets the multipleDoubleValues value for this Var.
     * 
     * @param multipleDoubleValues
     */
    public void setMultipleDoubleValues(double[] multipleDoubleValues) {
        this.multipleDoubleValues = multipleDoubleValues;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Var)) return false;
        Var other = (Var) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.attribute==null && other.getAttribute()==null) || 
             (this.attribute!=null &&
              this.attribute.equals(other.getAttribute()))) &&
            ((this.type==null && other.getType()==null) || 
             (this.type!=null &&
              this.type.equals(other.getType()))) &&
            ((this.simpleValue==null && other.getSimpleValue()==null) || 
             (this.simpleValue!=null &&
              this.simpleValue.equals(other.getSimpleValue()))) &&
            this.nValues == other.getNValues() &&
            ((this.multipleStringValues==null && other.getMultipleStringValues()==null) || 
             (this.multipleStringValues!=null &&
              java.util.Arrays.equals(this.multipleStringValues, other.getMultipleStringValues()))) &&
            ((this.multipleLongValues==null && other.getMultipleLongValues()==null) || 
             (this.multipleLongValues!=null &&
              java.util.Arrays.equals(this.multipleLongValues, other.getMultipleLongValues()))) &&
            ((this.multipleDoubleValues==null && other.getMultipleDoubleValues()==null) || 
             (this.multipleDoubleValues!=null &&
              java.util.Arrays.equals(this.multipleDoubleValues, other.getMultipleDoubleValues())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getAttribute() != null) {
            _hashCode += getAttribute().hashCode();
        }
        if (getType() != null) {
            _hashCode += getType().hashCode();
        }
        if (getSimpleValue() != null) {
            _hashCode += getSimpleValue().hashCode();
        }
        _hashCode += getNValues();
        if (getMultipleStringValues() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMultipleStringValues());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMultipleStringValues(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMultipleLongValues() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMultipleLongValues());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMultipleLongValues(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getMultipleDoubleValues() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getMultipleDoubleValues());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getMultipleDoubleValues(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Var.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "Var"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attribute");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "attribute"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("type");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "type"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "VAR_TYPE"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("simpleValue");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "simpleValue"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NValues");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multipleStringValues");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "multipleStringValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multipleLongValues");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "multipleLongValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("multipleDoubleValues");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "multipleDoubleValues"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "double"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "double"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
