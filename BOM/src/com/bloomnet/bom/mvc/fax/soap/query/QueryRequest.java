/**
 * QueryRequest.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class QueryRequest  implements java.io.Serializable {
    private java.lang.String filter;

    private java.lang.String sortOrder;

    private java.lang.String attributes;

    private int nItems;

    private boolean includeSubNodes;

    private boolean searchInArchive;

    public QueryRequest() {
    }

    public QueryRequest(
           java.lang.String filter,
           java.lang.String sortOrder,
           java.lang.String attributes,
           int nItems,
           boolean includeSubNodes,
           boolean searchInArchive) {
           this.filter = filter;
           this.sortOrder = sortOrder;
           this.attributes = attributes;
           this.nItems = nItems;
           this.includeSubNodes = includeSubNodes;
           this.searchInArchive = searchInArchive;
    }


    /**
     * Gets the filter value for this QueryRequest.
     * 
     * @return filter
     */
    public java.lang.String getFilter() {
        return filter;
    }


    /**
     * Sets the filter value for this QueryRequest.
     * 
     * @param filter
     */
    public void setFilter(java.lang.String filter) {
        this.filter = filter;
    }


    /**
     * Gets the sortOrder value for this QueryRequest.
     * 
     * @return sortOrder
     */
    public java.lang.String getSortOrder() {
        return sortOrder;
    }


    /**
     * Sets the sortOrder value for this QueryRequest.
     * 
     * @param sortOrder
     */
    public void setSortOrder(java.lang.String sortOrder) {
        this.sortOrder = sortOrder;
    }


    /**
     * Gets the attributes value for this QueryRequest.
     * 
     * @return attributes
     */
    public java.lang.String getAttributes() {
        return attributes;
    }


    /**
     * Sets the attributes value for this QueryRequest.
     * 
     * @param attributes
     */
    public void setAttributes(java.lang.String attributes) {
        this.attributes = attributes;
    }


    /**
     * Gets the nItems value for this QueryRequest.
     * 
     * @return nItems
     */
    public int getNItems() {
        return nItems;
    }


    /**
     * Sets the nItems value for this QueryRequest.
     * 
     * @param nItems
     */
    public void setNItems(int nItems) {
        this.nItems = nItems;
    }


    /**
     * Gets the includeSubNodes value for this QueryRequest.
     * 
     * @return includeSubNodes
     */
    public boolean isIncludeSubNodes() {
        return includeSubNodes;
    }


    /**
     * Sets the includeSubNodes value for this QueryRequest.
     * 
     * @param includeSubNodes
     */
    public void setIncludeSubNodes(boolean includeSubNodes) {
        this.includeSubNodes = includeSubNodes;
    }


    /**
     * Gets the searchInArchive value for this QueryRequest.
     * 
     * @return searchInArchive
     */
    public boolean isSearchInArchive() {
        return searchInArchive;
    }


    /**
     * Sets the searchInArchive value for this QueryRequest.
     * 
     * @param searchInArchive
     */
    public void setSearchInArchive(boolean searchInArchive) {
        this.searchInArchive = searchInArchive;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof QueryRequest)) return false;
        QueryRequest other = (QueryRequest) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.filter==null && other.getFilter()==null) || 
             (this.filter!=null &&
              this.filter.equals(other.getFilter()))) &&
            ((this.sortOrder==null && other.getSortOrder()==null) || 
             (this.sortOrder!=null &&
              this.sortOrder.equals(other.getSortOrder()))) &&
            ((this.attributes==null && other.getAttributes()==null) || 
             (this.attributes!=null &&
              this.attributes.equals(other.getAttributes()))) &&
            this.nItems == other.getNItems() &&
            this.includeSubNodes == other.isIncludeSubNodes() &&
            this.searchInArchive == other.isSearchInArchive();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getFilter() != null) {
            _hashCode += getFilter().hashCode();
        }
        if (getSortOrder() != null) {
            _hashCode += getSortOrder().hashCode();
        }
        if (getAttributes() != null) {
            _hashCode += getAttributes().hashCode();
        }
        _hashCode += getNItems();
        _hashCode += (isIncludeSubNodes() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += (isSearchInArchive() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(QueryRequest.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "QueryRequest"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("filter");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "filter"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sortOrder");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "sortOrder"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("attributes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "attributes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NItems");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nItems"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("includeSubNodes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "includeSubNodes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("searchInArchive");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "searchInArchive"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
