/**
 * SubmissionResults.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class SubmissionResults  implements java.io.Serializable {
    private java.lang.String submissionID;

    private int nTransport;

    private int[] transportIDs;

    public SubmissionResults() {
    }

    public SubmissionResults(
           java.lang.String submissionID,
           int nTransport,
           int[] transportIDs) {
           this.submissionID = submissionID;
           this.nTransport = nTransport;
           this.transportIDs = transportIDs;
    }


    /**
     * Gets the submissionID value for this SubmissionResults.
     * 
     * @return submissionID
     */
    public java.lang.String getSubmissionID() {
        return submissionID;
    }


    /**
     * Sets the submissionID value for this SubmissionResults.
     * 
     * @param submissionID
     */
    public void setSubmissionID(java.lang.String submissionID) {
        this.submissionID = submissionID;
    }


    /**
     * Gets the nTransport value for this SubmissionResults.
     * 
     * @return nTransport
     */
    public int getNTransport() {
        return nTransport;
    }


    /**
     * Sets the nTransport value for this SubmissionResults.
     * 
     * @param nTransport
     */
    public void setNTransport(int nTransport) {
        this.nTransport = nTransport;
    }


    /**
     * Gets the transportIDs value for this SubmissionResults.
     * 
     * @return transportIDs
     */
    public int[] getTransportIDs() {
        return transportIDs;
    }


    /**
     * Sets the transportIDs value for this SubmissionResults.
     * 
     * @param transportIDs
     */
    public void setTransportIDs(int[] transportIDs) {
        this.transportIDs = transportIDs;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof SubmissionResults)) return false;
        SubmissionResults other = (SubmissionResults) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.submissionID==null && other.getSubmissionID()==null) || 
             (this.submissionID!=null &&
              this.submissionID.equals(other.getSubmissionID()))) &&
            this.nTransport == other.getNTransport() &&
            ((this.transportIDs==null && other.getTransportIDs()==null) || 
             (this.transportIDs!=null &&
              java.util.Arrays.equals(this.transportIDs, other.getTransportIDs())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSubmissionID() != null) {
            _hashCode += getSubmissionID().hashCode();
        }
        _hashCode += getNTransport();
        if (getTransportIDs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransportIDs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransportIDs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(SubmissionResults.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "SubmissionResults"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submissionID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "submissionID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NTransport");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "nTransport"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportIDs");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "transportIDs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:SubmissionService", "int"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
