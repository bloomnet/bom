/**
 * ATTACHMENTS_FILTER.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class ATTACHMENTS_FILTER implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected ATTACHMENTS_FILTER(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _FILTER_NONE = "FILTER_NONE";
    public static final java.lang.String _FILTER_ALL = "FILTER_ALL";
    public static final java.lang.String _FILTER_CONVERTED = "FILTER_CONVERTED";
    public static final java.lang.String _FILTER_SOURCE = "FILTER_SOURCE";
    public static final ATTACHMENTS_FILTER FILTER_NONE = new ATTACHMENTS_FILTER(_FILTER_NONE);
    public static final ATTACHMENTS_FILTER FILTER_ALL = new ATTACHMENTS_FILTER(_FILTER_ALL);
    public static final ATTACHMENTS_FILTER FILTER_CONVERTED = new ATTACHMENTS_FILTER(_FILTER_CONVERTED);
    public static final ATTACHMENTS_FILTER FILTER_SOURCE = new ATTACHMENTS_FILTER(_FILTER_SOURCE);
    public java.lang.String getValue() { return _value_;}
    public static ATTACHMENTS_FILTER fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        ATTACHMENTS_FILTER enumeration = (ATTACHMENTS_FILTER)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static ATTACHMENTS_FILTER fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ATTACHMENTS_FILTER.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "ATTACHMENTS_FILTER"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
