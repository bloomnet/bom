/**
 * ActionResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class ActionResult  implements java.io.Serializable {
    private int nSucceeded;

    private int nFailed;

    private int nItem;

    private int[] transportIDs;

    private java.lang.String[] errorReason;

    public ActionResult() {
    }

    public ActionResult(
           int nSucceeded,
           int nFailed,
           int nItem,
           int[] transportIDs,
           java.lang.String[] errorReason) {
           this.nSucceeded = nSucceeded;
           this.nFailed = nFailed;
           this.nItem = nItem;
           this.transportIDs = transportIDs;
           this.errorReason = errorReason;
    }


    /**
     * Gets the nSucceeded value for this ActionResult.
     * 
     * @return nSucceeded
     */
    public int getNSucceeded() {
        return nSucceeded;
    }


    /**
     * Sets the nSucceeded value for this ActionResult.
     * 
     * @param nSucceeded
     */
    public void setNSucceeded(int nSucceeded) {
        this.nSucceeded = nSucceeded;
    }


    /**
     * Gets the nFailed value for this ActionResult.
     * 
     * @return nFailed
     */
    public int getNFailed() {
        return nFailed;
    }


    /**
     * Sets the nFailed value for this ActionResult.
     * 
     * @param nFailed
     */
    public void setNFailed(int nFailed) {
        this.nFailed = nFailed;
    }


    /**
     * Gets the nItem value for this ActionResult.
     * 
     * @return nItem
     */
    public int getNItem() {
        return nItem;
    }


    /**
     * Sets the nItem value for this ActionResult.
     * 
     * @param nItem
     */
    public void setNItem(int nItem) {
        this.nItem = nItem;
    }


    /**
     * Gets the transportIDs value for this ActionResult.
     * 
     * @return transportIDs
     */
    public int[] getTransportIDs() {
        return transportIDs;
    }


    /**
     * Sets the transportIDs value for this ActionResult.
     * 
     * @param transportIDs
     */
    public void setTransportIDs(int[] transportIDs) {
        this.transportIDs = transportIDs;
    }


    /**
     * Gets the errorReason value for this ActionResult.
     * 
     * @return errorReason
     */
    public java.lang.String[] getErrorReason() {
        return errorReason;
    }


    /**
     * Sets the errorReason value for this ActionResult.
     * 
     * @param errorReason
     */
    public void setErrorReason(java.lang.String[] errorReason) {
        this.errorReason = errorReason;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ActionResult)) return false;
        ActionResult other = (ActionResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.nSucceeded == other.getNSucceeded() &&
            this.nFailed == other.getNFailed() &&
            this.nItem == other.getNItem() &&
            ((this.transportIDs==null && other.getTransportIDs()==null) || 
             (this.transportIDs!=null &&
              java.util.Arrays.equals(this.transportIDs, other.getTransportIDs()))) &&
            ((this.errorReason==null && other.getErrorReason()==null) || 
             (this.errorReason!=null &&
              java.util.Arrays.equals(this.errorReason, other.getErrorReason())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getNSucceeded();
        _hashCode += getNFailed();
        _hashCode += getNItem();
        if (getTransportIDs() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTransportIDs());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTransportIDs(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getErrorReason() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getErrorReason());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getErrorReason(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ActionResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "ActionResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NSucceeded");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nSucceeded"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NFailed");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nFailed"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NItem");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nItem"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportIDs");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "transportIDs"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "int"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("errorReason");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "errorReason"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "string"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
