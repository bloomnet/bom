/**
 * RESOURCE_TYPE.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class RESOURCE_TYPE implements java.io.Serializable {
    private java.lang.String _value_;
    private static java.util.HashMap _table_ = new java.util.HashMap();

    // Constructor
    protected RESOURCE_TYPE(java.lang.String value) {
        _value_ = value;
        _table_.put(_value_,this);
    }

    public static final java.lang.String _TYPE_SALESFORCE_LEAD_EMAILBODY = "TYPE_SALESFORCE_LEAD_EMAILBODY";
    public static final java.lang.String _TYPE_SALESFORCE_LEAD_FAXCOVER = "TYPE_SALESFORCE_LEAD_FAXCOVER";
    public static final java.lang.String _TYPE_SALESFORCE_CONTACT_EMAILBODY = "TYPE_SALESFORCE_CONTACT_EMAILBODY";
    public static final java.lang.String _TYPE_SALESFORCE_CONTACT_FAXCOVER = "TYPE_SALESFORCE_CONTACT_FAXCOVER";
    public static final java.lang.String _TYPE_SALESFORCE_SETTINGS = "TYPE_SALESFORCE_SETTINGS";
    public static final java.lang.String _TYPE_STYLESHEET = "TYPE_STYLESHEET";
    public static final java.lang.String _TYPE_IMAGE = "TYPE_IMAGE";
    public static final java.lang.String _TYPE_COVER = "TYPE_COVER";
    public static final RESOURCE_TYPE TYPE_SALESFORCE_LEAD_EMAILBODY = new RESOURCE_TYPE(_TYPE_SALESFORCE_LEAD_EMAILBODY);
    public static final RESOURCE_TYPE TYPE_SALESFORCE_LEAD_FAXCOVER = new RESOURCE_TYPE(_TYPE_SALESFORCE_LEAD_FAXCOVER);
    public static final RESOURCE_TYPE TYPE_SALESFORCE_CONTACT_EMAILBODY = new RESOURCE_TYPE(_TYPE_SALESFORCE_CONTACT_EMAILBODY);
    public static final RESOURCE_TYPE TYPE_SALESFORCE_CONTACT_FAXCOVER = new RESOURCE_TYPE(_TYPE_SALESFORCE_CONTACT_FAXCOVER);
    public static final RESOURCE_TYPE TYPE_SALESFORCE_SETTINGS = new RESOURCE_TYPE(_TYPE_SALESFORCE_SETTINGS);
    public static final RESOURCE_TYPE TYPE_STYLESHEET = new RESOURCE_TYPE(_TYPE_STYLESHEET);
    public static final RESOURCE_TYPE TYPE_IMAGE = new RESOURCE_TYPE(_TYPE_IMAGE);
    public static final RESOURCE_TYPE TYPE_COVER = new RESOURCE_TYPE(_TYPE_COVER);
    public java.lang.String getValue() { return _value_;}
    public static RESOURCE_TYPE fromValue(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        RESOURCE_TYPE enumeration = (RESOURCE_TYPE)
            _table_.get(value);
        if (enumeration==null) throw new java.lang.IllegalArgumentException();
        return enumeration;
    }
    public static RESOURCE_TYPE fromString(java.lang.String value)
          throws java.lang.IllegalArgumentException {
        return fromValue(value);
    }
    public boolean equals(java.lang.Object obj) {return (obj == this);}
    public int hashCode() { return toString().hashCode();}
    public java.lang.String toString() { return _value_;}
    public java.lang.Object readResolve() throws java.io.ObjectStreamException { return fromValue(_value_);}
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumSerializer(
            _javaType, _xmlType);
    }
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new org.apache.axis.encoding.ser.EnumDeserializer(
            _javaType, _xmlType);
    }
    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(RESOURCE_TYPE.class);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "RESOURCE_TYPE"));
    }
    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

}
