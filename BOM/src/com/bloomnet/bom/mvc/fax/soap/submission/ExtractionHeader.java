/**
 * ExtractionHeader.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.submission;

public class ExtractionHeader  implements java.io.Serializable {
    private java.lang.String extractionJobID;

    private java.lang.String extractionDocID;

    private int offset;

    private int transportIndex;

    public ExtractionHeader() {
    }

    public ExtractionHeader(
           java.lang.String extractionJobID,
           java.lang.String extractionDocID,
           int offset,
           int transportIndex) {
           this.extractionJobID = extractionJobID;
           this.extractionDocID = extractionDocID;
           this.offset = offset;
           this.transportIndex = transportIndex;
    }


    /**
     * Gets the extractionJobID value for this ExtractionHeader.
     * 
     * @return extractionJobID
     */
    public java.lang.String getExtractionJobID() {
        return extractionJobID;
    }


    /**
     * Sets the extractionJobID value for this ExtractionHeader.
     * 
     * @param extractionJobID
     */
    public void setExtractionJobID(java.lang.String extractionJobID) {
        this.extractionJobID = extractionJobID;
    }


    /**
     * Gets the extractionDocID value for this ExtractionHeader.
     * 
     * @return extractionDocID
     */
    public java.lang.String getExtractionDocID() {
        return extractionDocID;
    }


    /**
     * Sets the extractionDocID value for this ExtractionHeader.
     * 
     * @param extractionDocID
     */
    public void setExtractionDocID(java.lang.String extractionDocID) {
        this.extractionDocID = extractionDocID;
    }


    /**
     * Gets the offset value for this ExtractionHeader.
     * 
     * @return offset
     */
    public int getOffset() {
        return offset;
    }


    /**
     * Sets the offset value for this ExtractionHeader.
     * 
     * @param offset
     */
    public void setOffset(int offset) {
        this.offset = offset;
    }


    /**
     * Gets the transportIndex value for this ExtractionHeader.
     * 
     * @return transportIndex
     */
    public int getTransportIndex() {
        return transportIndex;
    }


    /**
     * Sets the transportIndex value for this ExtractionHeader.
     * 
     * @param transportIndex
     */
    public void setTransportIndex(int transportIndex) {
        this.transportIndex = transportIndex;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ExtractionHeader)) return false;
        ExtractionHeader other = (ExtractionHeader) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.extractionJobID==null && other.getExtractionJobID()==null) || 
             (this.extractionJobID!=null &&
              this.extractionJobID.equals(other.getExtractionJobID()))) &&
            ((this.extractionDocID==null && other.getExtractionDocID()==null) || 
             (this.extractionDocID!=null &&
              this.extractionDocID.equals(other.getExtractionDocID()))) &&
            this.offset == other.getOffset() &&
            this.transportIndex == other.getTransportIndex();
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getExtractionJobID() != null) {
            _hashCode += getExtractionJobID().hashCode();
        }
        if (getExtractionDocID() != null) {
            _hashCode += getExtractionDocID().hashCode();
        }
        _hashCode += getOffset();
        _hashCode += getTransportIndex();
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ExtractionHeader.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionHeader"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extractionJobID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionJobID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("extractionDocID");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "ExtractionDocID"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("offset");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "offset"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("transportIndex");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SubmissionService", "transportIndex"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
