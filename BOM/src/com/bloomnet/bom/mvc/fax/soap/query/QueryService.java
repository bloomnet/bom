/**
 * QueryService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public interface QueryService extends javax.xml.rpc.Service {
    public java.lang.String getQueryServiceSoapAddress();

    public com.bloomnet.bom.mvc.fax.soap.query.QueryServiceSoap_PortType getQueryServiceSoap() throws javax.xml.rpc.ServiceException;

    public com.bloomnet.bom.mvc.fax.soap.query.QueryServiceSoap_PortType getQueryServiceSoap(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
