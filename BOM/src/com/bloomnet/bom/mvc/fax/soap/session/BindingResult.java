/**
 * BindingResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.session;

public class BindingResult  implements java.io.Serializable {
    private java.lang.String sessionServiceLocation;

    private java.lang.String submissionServiceLocation;

    private java.lang.String queryServiceLocation;

    private java.lang.String sessionServiceWSDL;

    private java.lang.String submissionServiceWSDL;

    private java.lang.String queryServiceWSDL;

    public BindingResult() {
    }

    public BindingResult(
           java.lang.String sessionServiceLocation,
           java.lang.String submissionServiceLocation,
           java.lang.String queryServiceLocation,
           java.lang.String sessionServiceWSDL,
           java.lang.String submissionServiceWSDL,
           java.lang.String queryServiceWSDL) {
           this.sessionServiceLocation = sessionServiceLocation;
           this.submissionServiceLocation = submissionServiceLocation;
           this.queryServiceLocation = queryServiceLocation;
           this.sessionServiceWSDL = sessionServiceWSDL;
           this.submissionServiceWSDL = submissionServiceWSDL;
           this.queryServiceWSDL = queryServiceWSDL;
    }


    /**
     * Gets the sessionServiceLocation value for this BindingResult.
     * 
     * @return sessionServiceLocation
     */
    public java.lang.String getSessionServiceLocation() {
        return sessionServiceLocation;
    }


    /**
     * Sets the sessionServiceLocation value for this BindingResult.
     * 
     * @param sessionServiceLocation
     */
    public void setSessionServiceLocation(java.lang.String sessionServiceLocation) {
        this.sessionServiceLocation = sessionServiceLocation;
    }


    /**
     * Gets the submissionServiceLocation value for this BindingResult.
     * 
     * @return submissionServiceLocation
     */
    public java.lang.String getSubmissionServiceLocation() {
        return submissionServiceLocation;
    }


    /**
     * Sets the submissionServiceLocation value for this BindingResult.
     * 
     * @param submissionServiceLocation
     */
    public void setSubmissionServiceLocation(java.lang.String submissionServiceLocation) {
        this.submissionServiceLocation = submissionServiceLocation;
    }


    /**
     * Gets the queryServiceLocation value for this BindingResult.
     * 
     * @return queryServiceLocation
     */
    public java.lang.String getQueryServiceLocation() {
        return queryServiceLocation;
    }


    /**
     * Sets the queryServiceLocation value for this BindingResult.
     * 
     * @param queryServiceLocation
     */
    public void setQueryServiceLocation(java.lang.String queryServiceLocation) {
        this.queryServiceLocation = queryServiceLocation;
    }


    /**
     * Gets the sessionServiceWSDL value for this BindingResult.
     * 
     * @return sessionServiceWSDL
     */
    public java.lang.String getSessionServiceWSDL() {
        return sessionServiceWSDL;
    }


    /**
     * Sets the sessionServiceWSDL value for this BindingResult.
     * 
     * @param sessionServiceWSDL
     */
    public void setSessionServiceWSDL(java.lang.String sessionServiceWSDL) {
        this.sessionServiceWSDL = sessionServiceWSDL;
    }


    /**
     * Gets the submissionServiceWSDL value for this BindingResult.
     * 
     * @return submissionServiceWSDL
     */
    public java.lang.String getSubmissionServiceWSDL() {
        return submissionServiceWSDL;
    }


    /**
     * Sets the submissionServiceWSDL value for this BindingResult.
     * 
     * @param submissionServiceWSDL
     */
    public void setSubmissionServiceWSDL(java.lang.String submissionServiceWSDL) {
        this.submissionServiceWSDL = submissionServiceWSDL;
    }


    /**
     * Gets the queryServiceWSDL value for this BindingResult.
     * 
     * @return queryServiceWSDL
     */
    public java.lang.String getQueryServiceWSDL() {
        return queryServiceWSDL;
    }


    /**
     * Sets the queryServiceWSDL value for this BindingResult.
     * 
     * @param queryServiceWSDL
     */
    public void setQueryServiceWSDL(java.lang.String queryServiceWSDL) {
        this.queryServiceWSDL = queryServiceWSDL;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof BindingResult)) return false;
        BindingResult other = (BindingResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.sessionServiceLocation==null && other.getSessionServiceLocation()==null) || 
             (this.sessionServiceLocation!=null &&
              this.sessionServiceLocation.equals(other.getSessionServiceLocation()))) &&
            ((this.submissionServiceLocation==null && other.getSubmissionServiceLocation()==null) || 
             (this.submissionServiceLocation!=null &&
              this.submissionServiceLocation.equals(other.getSubmissionServiceLocation()))) &&
            ((this.queryServiceLocation==null && other.getQueryServiceLocation()==null) || 
             (this.queryServiceLocation!=null &&
              this.queryServiceLocation.equals(other.getQueryServiceLocation()))) &&
            ((this.sessionServiceWSDL==null && other.getSessionServiceWSDL()==null) || 
             (this.sessionServiceWSDL!=null &&
              this.sessionServiceWSDL.equals(other.getSessionServiceWSDL()))) &&
            ((this.submissionServiceWSDL==null && other.getSubmissionServiceWSDL()==null) || 
             (this.submissionServiceWSDL!=null &&
              this.submissionServiceWSDL.equals(other.getSubmissionServiceWSDL()))) &&
            ((this.queryServiceWSDL==null && other.getQueryServiceWSDL()==null) || 
             (this.queryServiceWSDL!=null &&
              this.queryServiceWSDL.equals(other.getQueryServiceWSDL())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getSessionServiceLocation() != null) {
            _hashCode += getSessionServiceLocation().hashCode();
        }
        if (getSubmissionServiceLocation() != null) {
            _hashCode += getSubmissionServiceLocation().hashCode();
        }
        if (getQueryServiceLocation() != null) {
            _hashCode += getQueryServiceLocation().hashCode();
        }
        if (getSessionServiceWSDL() != null) {
            _hashCode += getSessionServiceWSDL().hashCode();
        }
        if (getSubmissionServiceWSDL() != null) {
            _hashCode += getSubmissionServiceWSDL().hashCode();
        }
        if (getQueryServiceWSDL() != null) {
            _hashCode += getQueryServiceWSDL().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(BindingResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:SessionService", "BindingResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionServiceLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SessionService", "sessionServiceLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submissionServiceLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SessionService", "submissionServiceLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryServiceLocation");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SessionService", "queryServiceLocation"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("sessionServiceWSDL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SessionService", "sessionServiceWSDL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("submissionServiceWSDL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SessionService", "submissionServiceWSDL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("queryServiceWSDL");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:SessionService", "queryServiceWSDL"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
