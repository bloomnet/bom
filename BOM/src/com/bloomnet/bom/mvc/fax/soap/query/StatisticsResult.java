/**
 * StatisticsResult.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package com.bloomnet.bom.mvc.fax.soap.query;

public class StatisticsResult  implements java.io.Serializable {
    private int nTypes;

    private java.lang.String[] typeName;

    private com.bloomnet.bom.mvc.fax.soap.query.StatisticsLine[] typeContent;

    public StatisticsResult() {
    }

    public StatisticsResult(
           int nTypes,
           java.lang.String[] typeName,
           com.bloomnet.bom.mvc.fax.soap.query.StatisticsLine[] typeContent) {
           this.nTypes = nTypes;
           this.typeName = typeName;
           this.typeContent = typeContent;
    }


    /**
     * Gets the nTypes value for this StatisticsResult.
     * 
     * @return nTypes
     */
    public int getNTypes() {
        return nTypes;
    }


    /**
     * Sets the nTypes value for this StatisticsResult.
     * 
     * @param nTypes
     */
    public void setNTypes(int nTypes) {
        this.nTypes = nTypes;
    }


    /**
     * Gets the typeName value for this StatisticsResult.
     * 
     * @return typeName
     */
    public java.lang.String[] getTypeName() {
        return typeName;
    }


    /**
     * Sets the typeName value for this StatisticsResult.
     * 
     * @param typeName
     */
    public void setTypeName(java.lang.String[] typeName) {
        this.typeName = typeName;
    }


    /**
     * Gets the typeContent value for this StatisticsResult.
     * 
     * @return typeContent
     */
    public com.bloomnet.bom.mvc.fax.soap.query.StatisticsLine[] getTypeContent() {
        return typeContent;
    }


    /**
     * Sets the typeContent value for this StatisticsResult.
     * 
     * @param typeContent
     */
    public void setTypeContent(com.bloomnet.bom.mvc.fax.soap.query.StatisticsLine[] typeContent) {
        this.typeContent = typeContent;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof StatisticsResult)) return false;
        StatisticsResult other = (StatisticsResult) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.nTypes == other.getNTypes() &&
            ((this.typeName==null && other.getTypeName()==null) || 
             (this.typeName!=null &&
              java.util.Arrays.equals(this.typeName, other.getTypeName()))) &&
            ((this.typeContent==null && other.getTypeContent()==null) || 
             (this.typeContent!=null &&
              java.util.Arrays.equals(this.typeContent, other.getTypeContent())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getNTypes();
        if (getTypeName() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTypeName());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTypeName(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getTypeContent() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getTypeContent());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getTypeContent(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(StatisticsResult.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "StatisticsResult"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("NTypes");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "nTypes"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeName");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "typeName"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "string"));
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("typeContent");
        elemField.setXmlName(new javax.xml.namespace.QName("urn:QueryService", "typeContent"));
        elemField.setXmlType(new javax.xml.namespace.QName("urn:QueryService", "StatisticsLine"));
        elemField.setNillable(true);
        elemField.setItemQName(new javax.xml.namespace.QName("urn:QueryService", "StatisticsLine"));
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
