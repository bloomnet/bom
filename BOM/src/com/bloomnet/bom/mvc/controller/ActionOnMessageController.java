package com.bloomnet.bom.mvc.controller;

import java.util.Properties;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.MessagingService;
import com.bloomnet.bom.mvc.service.SessionManager;

/**
 * Controller for processing actions related to messages on order
 *
 * @author Danil Svirchtchev
 */
@Controller
public class ActionOnMessageController {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( ActionOnMessageController.class );
    
    
	// Injected property
	@Autowired private Properties bomProperties;
    
    @Autowired private SessionManager sessionManager;
    @Autowired private MessagingService messagingService;
    
	
	/**
	 * Mark a message as read action.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping( value = "AgentViewReadMessage.xml", method = RequestMethod.GET )
	public String readMessage( HttpServletRequest request, 
							   HttpServletResponse response,	
							   SearchCriteria searchCriteria, 
							   BindingResult bindResult, 
							   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "AgentViewReadMessage";

		try {
			
			Long messageID = null;
			ActMessage selectedMsg = null;
			
			if( searchCriteria.getQuery() != null && searchCriteria.getQuery().length() > 0 ) {
				
				messageID = Long.valueOf( searchCriteria.getQuery() );
			}
			else {
				
				return nextView;
			}
				
			// Get the order object from session
			final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
			
			for( ActMessage orderMsg : order.getOrderMessages() ) {
				
				final Long orderMsgID = Long.valueOf( orderMsg.getOrderActivityId() );
				
				if (  messageID.equals( orderMsgID ) ) {
					
					selectedMsg = orderMsg;
				}
			}
				
			if ( selectedMsg != null ) {
				
				// update the status of the message to 
				final String status = bomProperties.getProperty(BOMConstants.MESSAGE_WORK_COMPLETED);

				messagingService.updateMessageStatus( user, selectedMsg, status );
				
				order.getOrderMessages().remove( selectedMsg );
			}
		}
		catch ( Exception e ) {
			
			model.addAttribute( "error", e.getMessage() );
			logger.error( e );
		}
		
		return nextView;
	}
}
