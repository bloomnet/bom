package com.bloomnet.bom.mvc.controller;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.support.SessionStatus;

import com.bloomnet.bom.common.bean.MessageOnOrderBean;
import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.CallDispDAO;
import com.bloomnet.bom.common.dao.MessageDAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.ShopDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActMessage;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.City;
import com.bloomnet.bom.common.entity.CityZipXref;
import com.bloomnet.bom.common.entity.Commmethod;
import com.bloomnet.bom.common.entity.Messagetype;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Paymenttype;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.ShopNotes;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.StsRouting;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Zip;
import com.bloomnet.bom.common.exceptions.AddressException;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.OccassionUtil;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteriaExtended;
import com.bloomnet.bom.mvc.businessobjects.WebAppActLogacall;
import com.bloomnet.bom.mvc.businessobjects.WebAppActMessage;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppShop;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.exceptions.RecommendedFloristsException;
import com.bloomnet.bom.mvc.service.ActivitiesService;
import com.bloomnet.bom.mvc.service.AgentService;
import com.bloomnet.bom.mvc.service.BomService;
import com.bloomnet.bom.mvc.service.MessagingService;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.service.ShopDataService;
import com.bloomnet.bom.mvc.utils.HibernateHelper;
import com.bloomnet.bom.mvc.validators.ActLogacallValidator;
import com.bloomnet.bom.mvc.validators.ActPaymentValidator;
import com.bloomnet.bom.mvc.validators.ComMethodValidator;
import com.bloomnet.bom.mvc.validators.MasterOrderValidator;
import com.bloomnet.bom.mvc.validators.MessageValidator;
import com.bloomnet.bom.mvc.validators.ScrubActLogACall;
import com.bloomnet.bom.mvc.validators.ScrubActPayment;
import com.bloomnet.bom.mvc.validators.ScrubComMethod;
import com.bloomnet.bom.mvc.validators.ScrubData;
import com.bloomnet.bom.mvc.validators.ScrubShop;
import com.bloomnet.bom.mvc.validators.ScrubWebAppActMessage;
import com.bloomnet.bom.mvc.validators.ScrubWebAppOrder;
import com.bloomnet.bom.mvc.validators.ShopValidator;
import com.bloomnet.bom.mvc.validators.OrderNotesValidator;

/**
 * Controller for the "Master Order" web application screen.
 * 
 * Controls the URL requests related to "Agent Views".
 *
 * @author Mark Silver and Jay Pursoo  Honorable mention: Danil Svirchtchev
 */
@Controller
public class MasterOrderViewController extends AbstractController {

	
	@Autowired private Properties fsiProperties;

	
    // Define a static logger variable
    static final Logger logger = Logger.getLogger( MasterOrderViewController.class );
    
    ScrubData scrubData = new ScrubData();
    ScrubShop scrubShop = new ScrubShop();
    ScrubActLogACall scrubLogacall = new ScrubActLogACall();
    ScrubActPayment scrubActPayment = new ScrubActPayment();
    ScrubComMethod scrubComMethod = new ScrubComMethod();
    ScrubWebAppActMessage scrubWebAppActMessage = new ScrubWebAppActMessage();
    ScrubWebAppOrder scrubWebAppOrder = new ScrubWebAppOrder();
    
    // Injected property
	@Autowired private Properties bomProperties;
    
	
    @Autowired private SessionManager sessionManager;
    @Autowired private AgentService agentService;
    @Autowired private MessageDAO messageDAO;
    @Autowired private OrderDAO orderDAO;
    @Autowired private UserDAO userDAO;
    @Autowired private ShopDAO shopDAO;
    @Autowired private CallDispDAO callDispDAO;
    @Autowired private ActPaymentValidator paymentTypeValidator;
    @Autowired private ComMethodValidator comMethodValidator;
    @Autowired private ActLogacallValidator logacallValidator;
    @Autowired private MasterOrderValidator orderValidator;
    @Autowired private MessageValidator messageValidator;
    @Autowired private ShopValidator shopValidator;
    @Autowired private ShopDataService shopDataService;
    @Autowired private MessagingService messagingService;
    @Autowired private ActivitiesService activitiesService;
    @Autowired private OrderNotesValidator notesValidator;
    @Autowired private BomService bomService;
   
   
    
	/**
	 * Default action, displays the "Master Order" page.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping(value = "AgentView.htm", method = RequestMethod.GET)
	public String index( HttpServletRequest request, 
						 SearchCriteria searchCriteria, 
						 BindingResult bindResult, 
						 Model model ) {

		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		List<ActMessage> orderMessages       = null;
		
		if ( order == null || ( order.isComplete() && "showNextOrder".equals( searchCriteria.getQuery() ) ) ) {

			order = findNextOrder( user );
			
			getRecommendedFlorists( order );
			sessionManager.setMasterOrder(request, order);
		} 
		else {
			
			Bomorder orderEntity = orderDAO.getMostRecentOrderByAgent( user.getUserId(), BOMConstants.ORDER_BEING_WORKED );
			
			if(orderEntity == null || orderEntity.getOrderNumber().equals(order.getBean().getBmtOrderNumber()) || orderEntity.getUser().getUserId() == user.getUserId()){
				order.setBloomnetOrderNumber(order.getBean().getBmtOrderNumber());
			
				try {
					byte previousStatus = order.getCurrentOrderStatus();
					byte sentToShop=3;
					byte searchResult=0;
					byte messageToBeWorked=18;
					//order.setCurrentOrderStatus(currentStatus);
					if(previousStatus==messageToBeWorked){
						List<Orderactivity> activities = new LinkedList<Orderactivity>();
						
						activities.addAll( order.getOrderactivities() );
						
						Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
						Collections.reverse(activities);
						
						ActRouting currentStatus = null;
							
						for( Orderactivity activity : activities ) {
								
							final ActRouting m_routing = activity.getActRouting();
								
							if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE)))) {
								
								currentStatus = m_routing;
								previousStatus = m_routing.getStsRouting().getStsRoutingId();
								break;
								
							}
						}
					}
					if (previousStatus==sentToShop) {//if previous status was sent to shop
						
						if(order.getOrderMessages()== null || (order.getOrderMessages()!= null && order.getOrderMessages().isEmpty())){
							order.setActionStr("redirect:default.htm"); 
							
							orderDAO.persistOrderactivityByUser(userDAO.getUserById(1L).getUserName(), BOMConstants.ACT_ROUTING, Byte.toString(sentToShop), order.getOrderNumber(), BOMConstants.TLO_QUEUE);
							long childId = 0;
							String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getOrderNumber());
							if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
								childId = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber()).getBomorderId();
							}
							
							byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TLO_QUEUE );
							BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(order.getBomorderId());
							bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
							bomorderSts.setStsRoutingId(Byte.parseByte(Byte.toString(sentToShop)));
							bomorderSts.setVirtualqueueId(virtualqueueId);
							
							orderDAO.updateBomorderSts(bomorderSts, childId);
									
							// remove user from Bomorder db record to unlock this order
							activitiesService.unlockOrder( order );
						
							// clear gui messages
							order.getMessages().clear();
							order.getMessages().add("There no more messages on order: " + order.getOrderNumber());
							return order.getActionStr();
						}else{
							order.setActionStr("redirect:AgentView.htm?timestamp="+new Date().getTime() ); 
						}
	
					}
					else if (previousStatus==searchResult) {
						//order is being loaded from search
					    //because previous Status is set when agent pulls order from queue
						
						//check to see if order is currently being worked (locked)
						Bomorder bomorder = orderDAO.getOrderByOrderNumber(order.getBean().getBmtOrderNumber());
						if (bomorder==null){
							bomorder = orderDAO.getOrderByOrderNumber(order.getOrderNumber());
						}
						User currentUser = bomorder.getUser();
	
						if (currentUser!=null){
							logger.info("Order " + order.getOrderNumber() + " is currently being worked");
							//check to see if a different user is currently working order
							Long currentUserId = currentUser.getUserId();
							
							if (!user.getUserId().equals(currentUserId)){
								logger.error("Order is currently being worked by " + currentUser.getUserName() );
								throw new Exception("Order is currently being worked by " + currentUser.getUserName() );
							}	
						}
						
						if(activitiesService.lockOrder( order, user) == null){
							throw new Exception(orderEntity.getOrderNumber() + " is currently being worked by another user. Please click next assignment again for a new order");
						}
											
						byte currentStatus = orderDAO.getCurrentOrderStatus(order.getEntity());
						order.setCurrentOrderStatus(currentStatus);
						
						byte orderBeingWorked  = 2;
						orderDAO.persistOrderactivityByUser(user.getUserName(), BOMConstants.ACT_ROUTING, Byte.toString(orderBeingWorked), order.getOrderNumber(), BOMConstants.TLO_QUEUE);
						long childId = 0;
						String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getOrderNumber());
						if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
							childId = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber()).getBomorderId();
						}
						
						byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TLO_QUEUE );
						BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(order.getBomorderId());
						bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
						bomorderSts.setStsRoutingId(orderBeingWorked);
						bomorderSts.setVirtualqueueId(virtualqueueId);
						
						orderDAO.updateBomorderSts(bomorderSts, childId);
						
					}
			
					order.setActionStr( "AgentView" );
					
					Long contactFlorist = null;
			
					//get messages from queue
					agentService.getMessagesOnOrderFromQueue(order.getBean());
					orderMessages = messagingService.getMessagesOnOrder( user, order );
					order.setOrderMessages( orderMessages );
					
					String query = searchCriteria.getQuery();
					
					if (!query.trim().isEmpty()){
						
					try{
						contactFlorist = Long.valueOf( query );
					}catch(Exception ee){
						contactFlorist = Long.valueOf("0");
					}
					
					System.out.println("ContactFlorist: "+contactFlorist );
					
					for( Shop recommendedFlorist : order.getRecommendedFlorists() ) {
						
						if ( recommendedFlorist.getShopId().equals( contactFlorist ) ) {
							
							order.setContactFlorist( recommendedFlorist );
							
				        	order.getBean().setReceivingShop( recommendedFlorist );
				        	order.getBean().setFulfillingShop( recommendedFlorist );
				        	
				        	order.setActionStr( "redirect:AgentViewContactFlorist.htm?timestamp="+new Date().getTime() );
						}
					}
					
				}
					getRecommendedFlorists( order );
					sessionManager.setMasterOrder(request, order);
				} 
				catch (Exception e) {
					e.printStackTrace();
					order.setActionStr("redirect:default.htm"); 
				}			
			}else{
				order.getMessages().add("You are already locked to an order. Please hit Next Assignent to get the order and either complete it or exit. Thank you!");
				order.setOrderMessages(orderMessages);
				order.setActionStr("redirect:default.htm"); 
				return order.getActionStr();	
			}
		}
	

		List<Role> roles = user.getRoles();
		Map<String,Long> results = new LinkedHashMap<String,Long>();
			
			for(int i=0; i<roles.size(); ++i){
					results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
			}

		model.addAttribute("roles", results);
		

		
		return order.getActionStr();
	}
	
	/**
	 * Multiple Order action, displays the Search Results page with the selected locked orders
	 */
	@Transactional
	@RequestMapping(value = "WorkMultipleOrders.htm", method = RequestMethod.GET)
	public String workMultipleOrders( HttpServletRequest request, 
						HttpServletResponse response,
						Model model,
						@RequestParam(value="ordersString", required=false) String ordersString) {

		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
		String[] ordersArray = ordersString.split(",");
		List<WebAppOrder> successfullyLockedOrders = new ArrayList<WebAppOrder>();
		
		for(int ii=0; ii<ordersArray.length; ++ii){
			
			Bomorder o = orderDAO.getOrderByOrderId(Long.valueOf(ordersArray[ii]));
			WebAppOrder order = new WebAppOrder(o);
			
			try{
				String orderId = "";
				orderId = String.valueOf(order.getBomorderId());
				if(!orderId.equals("")){
		
					User currentUser = order.getUser();
					if (currentUser!=null){
						logger.info("Order " + order.getOrderNumber() + " is currently being worked");
						//check to see if a different user is currently working order
						Long currentUserId = currentUser.getUserId();
						if (!user.getUserId().equals(currentUserId)){
							logger.info("Order is currently being worked by " + currentUser.getUserName() );
							continue;
						}	
					}
					if(activitiesService.lockOrder( order, user) != null){			
						successfullyLockedOrders.add(order);
					
						byte currentStatus = orderDAO.getCurrentOrderStatus(order.getEntity());
						order.setCurrentOrderStatus(currentStatus);
						
						byte orderBeingWorked  = 2;
						orderDAO.persistOrderactivityByUser(user.getUserName(), BOMConstants.ACT_ROUTING, Byte.toString(orderBeingWorked), order.getOrderNumber(), BOMConstants.TLO_QUEUE);
						long childId = 0;
						String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getOrderNumber());
						if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
							childId = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber()).getBomorderId();
						}
						
						byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TLO_QUEUE );
						BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(order.getBomorderId());
						bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
						bomorderSts.setStsRoutingId(orderBeingWorked);
						bomorderSts.setVirtualqueueId(virtualqueueId);
						
						orderDAO.updateBomorderSts(bomorderSts, childId);
					}
					
				}
			}catch(Exception ee){
				logger.error(ee.getMessage());
				ee.printStackTrace();
			}
		}

		
		response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
//
		sessionManager.setSearchResults( request, successfullyLockedOrders );
		//
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", successfullyLockedOrders );
		Long ordersCount = Long.valueOf(successfullyLockedOrders.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		
		return nextView;
		
		
	}
	
	/**
	 * Multiple Order action, moves selected orders to the TLO queue
	 */
	@Transactional
	@RequestMapping(value = "MoveMultipleOrders.htm", method = RequestMethod.GET)
	public String moveMultipleOrders( HttpServletRequest request, 
						HttpServletResponse response,
						Model model,
						@RequestParam(value="ordersString", required=false) String ordersString,
						@RequestParam(value="fromWFR", required=false) String fromWFR,
						@RequestParam(value="fromAutomated", required=false) String fromAutomated) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}

		String[] ordersArray = ordersString.split(",");
		
		for(int ii=0; ii<ordersArray.length; ++ii){
			
			try {
				bomService.sendOrderToQueue(ordersArray[ii],"TLO");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";	
		List<Bomorder> orders = null;
		if(fromWFR != null)
			orders = bomService.getWFRS();
		else if(fromAutomated != null)
			orders = bomService.getOutstandingAutomatedOrders();
		else
			orders = bomService.getWFTZS();
		List<Bomorder> orders2 = new ArrayList<Bomorder>();
		for(Bomorder order : orders){
			if(!ordersString.contains(order.getOrderNumber())){
				orders2.add(order);
			}
		}
		sessionManager.setSearchResults( request, orders2 );
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", orders2 );
		Long ordersCount = Long.valueOf(orders2.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		List<Role> roles = user.getRoles();
		Map<String,Long> results = new LinkedHashMap<String,Long>();
			
			for(int i=0; i<roles.size(); ++i){
					results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
			}

		model.addAttribute("roles", results);
		
		return nextView;
		
		
	}
	
	/**
	 * A helper view which closes a modal window and redirects to the 
	 * address passed via searchCriteria.getQuery() parameter.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping(value = "AgentViewCloseModalAndRedirect.htm", method = RequestMethod.GET)
	public String closeModalAndRedirect( HttpServletRequest request, 
										 SearchCriteria searchCriteria, 
										 Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
		model.addAttribute( "nextView", searchCriteria.getQuery() );
		return "AgentViewCloseModalAndRedirect";
	}
	
	
	/**
	 * This method will render an order edit view accessible via editOrder.htm
	 * 
	 * @param request
	 * @param searchCriteria
	 * @param bindResult
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "editOrder.htm", method = RequestMethod.GET)
	public String editOrder( HttpServletRequest request, 
			                 Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
	
		final String nextView;
		
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);

		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			nextView = "editOrder";
			model.addAttribute( "command", order );
			model.addAttribute( "occassions", new OccassionUtil().getOccassions() );
			model.addAttribute("states", orderDAO.getAllStates());
		}
		
		return nextView;
	}
	
    @RequestMapping( value = "editOrder.htm", method = RequestMethod.POST )
    public String editOrder( HttpServletRequest request,
    						 @ModelAttribute("command") WebAppOrder formOrder, 
    		                 BindingResult result, 
    		                 SessionStatus status,
    		                 Model model ) {
    	
    	String nextView = "editOrder";
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	
    	formOrder.setActionStr( nextView );
    	
    	// put back the occasions code into a model
    	model.addAttribute( "occassions", new OccassionUtil().getOccassions() );
    	model.addAttribute("states", orderDAO.getAllStates());
    	
    	formOrder = scrubWebAppOrder.scrubWebAppOrder(formOrder);
    	
    	orderValidator.validateForm( formOrder, result );
        
        if ( result.hasErrors() ) {
        	// do nothing
        } else {
        	
        	try {
        		
        		order.getMessages().clear();
        		
				if( isEdited( order, formOrder ) ) {
					
					// write audit record to the db
					activitiesService.persistActivityAudit( order, user );
					getRecommendedFlorists( order );
				}

	        	nextView = "redirect:AgentViewCloseModalAndRedirect.htm?query=AgentViewContactFlorist.htm";
				
			} catch ( Exception e ) {

				order.getMessages().add( e.getMessage() );
				result.reject( "java.exception.message", new Object[] { e.getMessage() }, null );
			}
        }
		
		return nextView;
    }
    
    @RequestMapping(value = "recipe.htm", method = RequestMethod.GET)
	public String recipe( HttpServletRequest request, 
			                 Model model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
	
		final String nextView;
		
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);

		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			nextView = "recipe";
			model.addAttribute( "formOrder", order );
		}
		
		return nextView;
	}
    
	@RequestMapping(value = "orderNotes.htm", method = RequestMethod.GET)
	public String orderNotes( HttpServletRequest request, 
			                 Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
	
		final String nextView;
		
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);

		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			nextView = "orderNotes";
			model.addAttribute( "command", order );
		}
		
		return nextView;
	}
	
    @RequestMapping( value = "orderNotes.htm", method = RequestMethod.POST )
    public String orderNotes( HttpServletRequest request,
    						 @ModelAttribute("command") WebAppOrder formOrder, 
    		                 BindingResult result, 
    		                 SessionStatus status,
    		                 Model model ) {
    	
    	String nextView = "orderNotes";
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	
    	formOrder.setActionStr( nextView );
    	
		notesValidator.validate( formOrder, result );
        
        if ( result.hasErrors() ) {
        	// do nothing
        } else {
        	String scrubbedText = scrubData.scrubData(formOrder.getLogAcallText());
        	try {
        		
        		order.getMessages().clear();
        		
        		order.setLogAcallText( scrubbedText );
        		
				activitiesService.persistActivityOrderNote( order, user );

	        	nextView = "redirect:AgentViewCloseModalAndRedirect.htm?query=AgentViewContactFlorist.htm";
				
			} catch ( Exception e ) {

				order.getMessages().add( e.getMessage() );
				result.reject( "java.exception.message", new Object[] { e.getMessage() }, null );
			}
        }
		
		return nextView;
    }
    
    @RequestMapping(value = "shopNotes.htm", method = RequestMethod.GET)
	public String shopNotes( HttpServletRequest request, 
			                 Model model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
	
		final String nextView;
		
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);

		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			ShopNotes shopNotes = new ShopNotes();
			
			shopNotes.setCreatedDate(new Date());
			shopNotes.setShop(shopDAO.getShopByShopId(Long.valueOf(request.getParameter("shopID").toString())));
			shopNotes.setUser((WebAppUser) sessionManager.getApplicationUser(request));
			shopNotes.setShopNote(request.getParameter("shopNote").toString());
			
			shopDAO.persistShopNotes(shopNotes);
			
			nextView = "redirect:AgentView.htm?query=showNextOrder&timestamp=" + new Date().getTime();
		}
		
		return nextView;
	}
    
	
    /**
     * Check if the order had been changed based on the values in fields:
     * 
     * deliveryDate;
     * price; // no price, this is ActPayment activity
     * zipId;
     * cityId;
     * 
     * @param sessionOrder
     * @param formOrder
     * @return
     * @throws Exception 
     * @throws AddressException 
     */
    private boolean isEdited( WebAppOrder sessionOrder, WebAppOrder formOrder ) throws Exception {
    	
    	boolean result = false;
    	
    	MessageOnOrderBean target = formOrder.getBean();
    	
    	final String city = target.getRecipientCity();
    	final String zip  = target.getRecipientZipCode();
    	final String state  = target.getRecipientState();
    	
    	Map<String, Object> valAddr = shopDataService.validateCityAndZip( city, zip, state );
			
    	if( ! sessionOrder.getDeliveryDate().equals( new Timestamp( target.getNewDeliveryDate().getTime() ) ) ) {

    		sessionOrder.getBean().setNewDeliveryDate( target.getNewDeliveryDate() );
    		result = true;
    	}
    	
    	if( ! ( ( City ) valAddr.get("city") ).getCityId().equals( sessionOrder.getCityId() ) ) {
    		
    		sessionOrder.getBean().setNewCity( ( City ) valAddr.get("city") );
    		result = true;
    	}
    	
    	if( ! ( ( Zip ) valAddr.get("zip") ).getZipId().equals( sessionOrder.getZipId() ) ) {
    		
    		sessionOrder.getBean().setNewZip( ( Zip ) valAddr.get("zip") );
    		result = true;
    	}
    	
    	String s1 = sessionOrder.getJsonForInputFields().replaceAll("\\\\r","").replaceAll("\\\\n","");
    	String s2 = formOrder.getJsonForInputFields().replaceAll("\\\\r","").replaceAll("\\\\n","");
    	
    	if( s1.compareToIgnoreCase( s2 ) != 0 ) {
    		
    		result = true;
    	}
    	
    	sessionOrder.getBean().setCardMessage( formOrder.getBean().getCardMessage() );
    	sessionOrder.getBean().setSpecialInstruction( formOrder.getBean().getSpecialInstruction() );
    	sessionOrder.getBean().setRecipientFirstName(formOrder.getBean().getRecipientFirstName());
    	sessionOrder.getBean().setRecipientLastName(formOrder.getBean().getRecipientLastName());
    	sessionOrder.getBean().setRecipientPhoneNumber(formOrder.getBean().getRecipientPhoneNumber());
    	sessionOrder.getBean().setDeliveryDate(formOrder.getBean().getDeliveryDate());
    	sessionOrder.getBean().setOccasionId(formOrder.getBean().getOccasionId());
    	sessionOrder.getBean().setRecipientAddress1(formOrder.getBean().getRecipientAddress1());
    	sessionOrder.getBean().setRecipientAddress2(formOrder.getBean().getRecipientAddress2());
    	sessionOrder.getBean().setRecipientCity(formOrder.getBean().getRecipientCity());
    	sessionOrder.getBean().setRecipientState(formOrder.getBean().getRecipientState());
    	sessionOrder.getBean().setRecipientZipCode(formOrder.getBean().getRecipientZipCode());
    	sessionOrder.getBean().setProducts(formOrder.getBean().getProducts());
		
    	return result;
	}

    @RequestMapping( value = "AgentViewContactFlorist.htm", method = RequestMethod.GET )
    public String processContactFlorist ( HttpServletRequest request, 
    									  ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			if( order.getContactFlorist() == null ) {

				final Shop myShop = HibernateHelper.initializeAndUnproxy( user.getMyShop() );
				order.setContactFlorist( myShop );
			}

			order.getMessages().clear();
			order.getMessages().add("Shop ["+order.getContactFlorist().getShopName()+"] has been selected");
			System.out.println("Shop ["+order.getContactFlorist().getShopName()+"] has been selected for order "+ order.getOrderNumber());

			
			
			model.put("callDisposition", new WebAppActLogacall() );
			model.put("defaultShopnetworkStatusId", CallDispDAO.NO_CONTACT_DEFAULT_SHOPNETWORK_STATUS_ID);
				
			order.setDispositions( callDispDAO.getAllCallDisp() );
			
			loadShopCode("receiving", order, order.getContactFlorist().getShopId() );
			
			model.addAttribute("command", order);
			
			nextView = "AgentViewContactFlorist";
		}
		
		return nextView;
    }
	
    @RequestMapping( value = "AgentViewContactFlorist.htm", method = RequestMethod.POST )
    public String processContactFlorist( HttpServletRequest request,
			    						 @ModelAttribute("command") WebAppActLogacall formActLogacall, 
			    		                 BindingResult result,
			    		                 SessionStatus status ) {
    	
    	String nextView = "AgentViewContactFlorist";
    	
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	WebAppUser  user  = (WebAppUser)  sessionManager.getApplicationUser(request);
    	
    	formActLogacall = scrubLogacall.scrubActLogaCall(formActLogacall);
    	logacallValidator.validateForm( formActLogacall, result );
        
        if ( result.hasErrors() ) {
        	// do nothing
        } else {
        	
        	order.getMessages().clear();
        	
        	try {
        		
        		final Shop m_shop = HibernateHelper.initializeAndUnproxy( order.getContactFlorist() );
        		final String orderNumber = order.getBean().getBmtOrderNumber();

        		agentService.logCall( user, m_shop, formActLogacall, orderNumber );
        		
				order.getMessages().add("Call disposition for shop ["+m_shop.getShopName()+"] was created");
				order.getRecommendedFlorists().remove( m_shop );
				order.getBean().setReceivingShop( new Shop() );
				
				nextView = "redirect:AgentView.htm?query=showNextOrder&timestamp=" + new Date().getTime();
				
			} catch (Exception e) {

				order.getMessages().add(e.getMessage());
				logger.error(e);
				
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
			}
        }
		
		return nextView;
    }
	
    @RequestMapping( value = "AgentViewSelectPaymentType.htm", method = RequestMethod.GET )
    public String processSelectPaymentType ( HttpServletRequest request, 
    									   ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			// Get the list of all payment types and put them into model
			order.setPaymentTypes( orderDAO.getPaymentTypes() );

			model.addAttribute( "command", order );
			
			nextView = "AgentViewSelectPaymentType";
		}
		
		return nextView;
    }
	
    @RequestMapping( value = "AgentViewSelectPaymentType.htm", method = RequestMethod.POST )
    public String processSelectPaymentType( HttpServletRequest request,
			    							@ModelAttribute("formOrder") WebAppOrder formOrder, 
			    		                    BindingResult result, 
			    		                    SessionStatus status ) {
    	
    	final String nextView;
    	
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	
    	order.setPaymentTypeStr( formOrder.getPaymentTypeStr() );
    	order.setPaymentAmountStr( formOrder.getPaymentAmountStr() );
    	order.setPayTo( formOrder.getPayTo() );
    	order.setSelectedShopnetwork( formOrder.getSelectedShopnetwork() );
    	
    	order  = scrubActPayment.scrubActPayment(order);
    	paymentTypeValidator.validateForm( order, result );
        
        if (result.hasErrors()) {
        	
        	nextView = "AgentViewSelectPaymentType"; 
        	
        } else {
        	        	
        	for( Paymenttype pType : order.getPaymentTypes() ) {
        		
        		if( pType.getPaymentTypeId().equals( Byte.parseByte( order.getPaymentTypeStr() ) ) ) {
        			
        			order.setPaymenttype(pType);
        		}
        	}
        	
        	if(order.getPaymentTypeStr().equals("3")){
        		
        		for ( Shopnetwork selectedNetwork : order.getContactFlorist().getShopnetworks() ) {
        	
        		
	        		final String id = String.valueOf( selectedNetwork.getShopNetworkId().longValue() );
	        		
	        		if ( order.getSelectedShopnetwork().equals( id ) ) {
	        			
		    			long num = selectedNetwork.getNetwork().getNetworkId().longValue();
		    			byte networkId = (byte) num;
	        			
		    			order.getBean().setOrderType( networkId );
	        		}
        		}
        	}

        	nextView = "redirect:AgentViewSubmitOrder.htm?timestamp=" + new Date().getTime();
        }
		
		return nextView;
    }
    
    @RequestMapping( value = "AgentViewSelectSendMethod.htm", method = RequestMethod.GET )
    public String processSelectSendMethod ( HttpServletRequest request, 
    									    ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			// clear gui messages
			order.getMessages().clear();
			
			// Get the list of all com methods types and put them into model
			order.setComMethods( messageDAO.getCommmethods() );
			
			model.addAttribute( "command", order );
			nextView = "AgentViewSelectSendMethod";
		}
		
		return nextView;
    }
	
    @RequestMapping( value = "AgentViewSelectSendMethod.htm", method = RequestMethod.POST )
    public String processSelectSendMethod( HttpServletRequest request,
			    						   @ModelAttribute("formOrder") WebAppOrder formOrder, 
			    		                   BindingResult result, 
			    		                   SessionStatus status ) {
    	
    	final String nextView;
    	
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	
    	formOrder = scrubComMethod.scrubComMethod(formOrder);
    	comMethodValidator.validateForm(formOrder, result);
        
        if ( result.hasErrors() ) {
        	
        	nextView = "AgentViewSelectSendMethod";
        	
        } else {
        	
			for ( Commmethod m_comMethod : order.getComMethods() ) {
				
				if ( m_comMethod.getDescription().equals( formOrder.getMethodToSend().getDescription() ) ) {
					
					order.setMethodToSend( m_comMethod );

					if ( "FAX".equals( m_comMethod.getDescription() ) ) {
				    	order.getBean().getReceivingShop().setShopFax( formOrder.getFaxConfirmation() );
					}
				}
			}

			nextView = "redirect:AgentViewSelectPaymentType.htm?timestamp=" + new Date().getTime();
        }
		
		return nextView;
    }
    
	/**
	 * Default action, displays the "Select Order Message" page.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping(value = "AgentViewMessageReply.htm", method = RequestMethod.GET)
	public String processMessageReply( HttpServletRequest request, 
									   SearchCriteria searchCriteria, 
									   BindingResult bindResult, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
		String nextView = "AgentViewMessageReply";
		
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		WebAppActMessage  newMessage = new WebAppActMessage();
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm";
		} 
		else {
									
			Long selectedMessage = null;

			try {
				
				selectedMessage = Long.valueOf( searchCriteria.getQuery() );
				
				for( ActMessage message : order.getOrderMessages() ) {
					
					if ( Long.valueOf( message.getOrderActivityId() ).equals( selectedMessage ) ) {
						
						order.setSelectedMessage( message );
						newMessage.setOriginalMessage( message );
					}
				}
			} 
			catch ( Exception e ) {				

				logger.error( e );
			}			
		}	
		
		model.addAttribute("command", newMessage );
		model.addAttribute("messageTypes", messageDAO.getMessageTypes() );

		return nextView;
	}
	
    @RequestMapping( value = "AgentViewMessageReply.htm", method = RequestMethod.POST )
    public String processMessageReply( HttpServletRequest request,
			    					   @ModelAttribute("command") WebAppActMessage formMessage, 
			    		               BindingResult result, 
			    		               SessionStatus status,
			    		               ModelMap model ) {
    	
    	String nextView = "AgentViewMessageReply";
    	
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
    	
    	final List<Messagetype> messageTypes = messageDAO.getMessageTypes();
    	model.addAttribute("messageTypes", messageTypes );
    	
    	formMessage = scrubWebAppActMessage.scrubWebAppActMessage(formMessage);
    	
		formMessage.setOriginalMessage( order.getSelectedMessage() );
		
		messageValidator.validate(formMessage, result);

        if ( result.hasErrors() ) {
        	// do nothing
        } else {
    	
        	for( Messagetype m_type : messageTypes ) {
        		
        		if( m_type.getShortDesc().equalsIgnoreCase( formMessage.getMessageTypeId() ) ) {
        			
        			formMessage.setMessagetype(m_type);
        		}
        	}
        	
        	try {
        		
        		messagingService.sendNewMessageToShop( user, order, formMessage );
        		order.getOrderMessages().remove( order.getSelectedMessage() );
        		formMessage.setSent( true );
        		
        		// if there are no more messages left on the order reset statuses and 
        		// redirect to the landing page
        		byte previousStatus = order.getCurrentOrderStatus();
        		byte saveStatus = 0;
        		Byte sentToShop = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP));
        		Byte toBeWorked = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED));
        		Byte activelyBeingWorked = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED));
        		Byte cancelled = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_CANCELLED_BY_SSHOP));
        		Byte wfr = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP));
        		Byte wftz = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE));
        		
        		if (( order.getOrderMessages().isEmpty() )&&(previousStatus!=toBeWorked && 
        				previousStatus!=activelyBeingWorked && previousStatus!=wfr && previousStatus!=wftz)) {
        			
        			byte otherStatus = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED));

					byte currStatus = order.getCurrentOrderStatus();
					if ( currStatus == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)) || currStatus == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE))){
						order.setCurrentOrderStatus(otherStatus);
					}else{
						
						List<Orderactivity> activities = new LinkedList<Orderactivity>();
						
						activities.addAll( order.getOrderactivities() );
						
						Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
						Collections.reverse(activities);
						
						ActRouting currentStatus = null;
							
						for( Orderactivity activity : activities ) {
								
							final ActRouting m_routing = activity.getActRouting();
								
							if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE)))) {
								
								currentStatus = m_routing;
								saveStatus = m_routing.getStsRouting().getStsRoutingId();
								break;
								
							}
						}
						
						order.setCurrentOrderStatus(currentStatus.getStsRouting().getStsRoutingId());
						order.setVirtualQueue(currentStatus.getVirtualQueue());
					}
					if(saveStatus != 1)
						orderDAO.persistOrderactivityByUser(userDAO.getUserById(1L).getUserName(), BOMConstants.ACT_ROUTING, Byte.toString(saveStatus), order.getOrderNumber(), BOMConstants.TLO_QUEUE);

					long childId = 0;
					String currentFulfillingShop = orderDAO.getCurrentFulfillingShopCode(order.getOrderNumber());
					if(!currentFulfillingShop.equals(fsiProperties.getProperty(BOMConstants.FSI_SHOPCODE))){
						childId = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber()).getBomorderId();
					}
					
					if(saveStatus == sentToShop){
						byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(BOMConstants.TLO_QUEUE );
	
						BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(order.getBomorderId());
						bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
						bomorderSts.setStsRoutingId(Byte.parseByte(Byte.toString(sentToShop)));
						bomorderSts.setVirtualqueueId(virtualqueueId);
					
						orderDAO.updateBomorderSts(bomorderSts, childId);
					}
        			
        			if(saveStatus == 1 || !order.getOrderMessages().isEmpty()){
            			nextView = "redirect:AgentView.htm?timestamp=" + new Date().getTime(); 
        			}

        			else{
        				nextView = "redirect:default.htm"; 
        				order.getMessages().clear();
            			order.getMessages().add("There no more messages on order: " + order.getOrderNumber());
            			activitiesService.unlockOrder( order );
        			}
        			
        		
        			
        		}else if( order.getOrderMessages().isEmpty()){
        			
        			//orderDAO.persistOrderactivityByUser(user.getUserName(), BOMConstants.ACT_ROUTING, Byte.toString(sentToShop), order.getOrderNumber(), BOMConstants.TLO_QUEUE);

        			nextView = "redirect:AgentView.htm?timestamp=" + new Date().getTime(); 
        			
        		}
        	}
        	catch ( Exception e ) {
        		
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
        	}
        }

		return nextView;
    }
    
    
    @RequestMapping( value = "AgentViewSubmitOrder.htm", method = RequestMethod.GET )
    public String processSubmitOrder ( HttpServletRequest request, 
    								   ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null || order.isComplete() ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			model.addAttribute( "command", order );
			nextView = "AgentViewSubmitOrder";
		}
		
		return nextView;
    }
    
    
    @RequestMapping( value = "AgentViewSubmitOrder.htm", method = RequestMethod.POST )
    public String processSubmitOrder( HttpServletRequest request,
			    					  @ModelAttribute("command") WebAppOrder formOrder, 
			    		              BindingResult result, 
			    		              SessionStatus status,
			    		              ModelMap model ) {
    	
    	final String nextView = "AgentViewSubmitOrder";
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
    	WebAppOrder order = getOrderInProcess( request, result );
    	
    	// Validate the FAX number
    	orderValidator.validate( formOrder, result );
    	
        if ( result.hasErrors() ) {
        } else {
    	
	    	try {
	    		
	    		order.setBloomnetOrderNumber( order.getBean().getBmtOrderNumber() );
	    		order.setOutboundOrderNumber( agentService.sendOrderToShop( order, user ) );
				order.setComplete(true);
				order.setStatus("Sent to Shop");
				
				order.getMessages().clear();
				order.getMessages().add("Order has been sent to a shop");
				
				// Try to send an email confirmation if an email address
				// is passed in from the UI
				String email = formOrder.getEmailConfirmation();
				
				email = scrubData.scrubData(email);
				
				if ( email != null && email.length() > 0 ) {
					
					order.getBean().getReceivingShop().setShopEmail( email );
					
					agentService.sendEmailConfirmation( order.getOutboundOrderNumber(), 
														order.getBean().getReceivingShop(), 
														order.getBean() );
					
					order.getMessages().add("Email confimation sent to: " + email );
				}

			} catch ( Exception e ) {
				
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error("Unable to submit order " + order.getBloomnetOrderNumber() + " because "+ e);
				e.printStackTrace();
			}
        }
     
		return nextView;
    }
	
	
    @RequestMapping( value = "AgentViewAddFlorist.htm", method = RequestMethod.GET )
    public String processAddFlorist ( HttpServletRequest request, 
    							      ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			// clear gui messages
			order.getMessages().clear();
			
			populateStates(order, model);
			
			Map<String, Long> networks = shopDAO.getAllNetworks();
			model.addAttribute( "networks", networks );
			
			model.addAttribute( "command", new WebAppShop() );
			
			nextView = "AgentViewAddFlorist";
		}
		
		return nextView;
    }
	
    
    @RequestMapping( value = "AgentViewAddFlorist.htm", method = RequestMethod.POST )
    public String processAddFlorist( HttpServletRequest request,
	    						     @ModelAttribute("command") WebAppShop formShop, 
	    		                     BindingResult result, 
	    		                     SessionStatus status,
	    		                     ModelMap model ) {
        
        String nextView = "AgentViewAddFlorist";
    	
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);

    	getOrderInProcess( request, result );
    	populateStates(order, model);
    	
    	// validate the user input
    	formShop = scrubShop.scrubShop(formShop);
    	shopValidator.validateForm( formShop, result );
    	
        if ( formShop.getShop().equals( order.getAddedShop() ) ) {
               
            result.reject("error.shop.added");
        }
        
        if ( result.hasErrors() ) {
        	// do nothing
        } else {
			try {
				
			    order.getMessages().clear();
			    logger.debug("adding shop: " + formShop.getShop().getShopName()+" "+formShop.getShop().getShopPhone());
			    order.setAddedShop( shopDataService.persistFlorist( user, formShop ) );
				order.setRecommendedFlorists( getRecommendedFlorists( order.getBean() ) );
				order.getMessages().add("Florist ["+formShop.getShopName()+"] has been saved");
				
				nextView = "redirect:AgentView.htm?query=showNextOrder&timestamp=" + new Date().getTime();
				
			} catch (Exception e) {
				
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
			}
        }
		
		return nextView;
    }
    
    @RequestMapping( value = "AgentViewEditFlorist.htm", method = RequestMethod.GET )
    public String processEditFlorist ( HttpServletRequest request, 
    							      ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			// clear gui messages
			order.getMessages().clear();
			
			populateStates(order, model);
			
			Map<String, Long> networks = shopDAO.getAllNetworks();
			model.addAttribute( "networks", networks );
			
			Shop myShop = order.getContactFlorist();
			WebAppShop was = new WebAppShop(myShop);
			
			model.addAttribute( "command", was );
			
			nextView = "AgentViewEditFlorist";
		}
		
		return nextView;
    }
	
    
    @RequestMapping( value = "AgentViewEditFlorist.htm", method = RequestMethod.POST )
    public String processEditFlorist( HttpServletRequest request,
	    						     @ModelAttribute("command") WebAppShop formShop, 
	    		                     BindingResult result, 
	    		                     SessionStatus status,
	    		                     ModelMap model ) {
        
        String nextView = "AgentViewEditFlorist";
    	
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);

    	getOrderInProcess( request, result );
    	populateStates(order, model);
    	
    	formShop = scrubShop.scrubShop(formShop);
    	shopValidator.validateForm( formShop, result );
        
        if ( result.hasErrors() ) {
        	// do nothing
        } else {
			try {
				
				City providedCity = orderDAO.getCityByNameAndState(formShop.getCity().getName(), formShop.getSelectedState());
				Zip providedZip = orderDAO.getZipByCode(formShop.getZip().getZipCode());
				boolean isValid = false;
				for(CityZipXref xref : providedCity.getCityZipXrefs()){
					if(xref.getZip().getZipId().equals(providedZip.getZipId())){
						isValid = true;
					}
				}
				if(providedCity != null && providedZip != null && isValid){
				    order.getMessages().clear();
				    shopDataService.mergeFlorist( user, formShop );
					order.setRecommendedFlorists( getRecommendedFlorists( order.getBean() ) );
					order.getMessages().add("Florist ["+formShop.getShopName()+"] has been edited");
					nextView = "redirect:AgentView.htm?query=showNextOrder&timestamp=" + new Date().getTime();
				}else{
					nextView = "AgentView";
					order.getMessages().add("Invalid City/State/Zip Data entered. Please correct this data and try again.");
				}
				
			} catch (Exception e) {
				
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
			}
        }
		
		return nextView;
    } 	
    
    @RequestMapping( value = "AgentViewRemoveShopCoverage.htm", method = RequestMethod.GET )
    public String processRemoveShopCoverage ( HttpServletRequest request, 
    							      ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			// clear gui messages
			order.getMessages().clear();
			
			populateStates(order, model);
			
			Shop myShop = order.getContactFlorist();
			Zip zip = order.getZip();
			
			shopDAO.removeCoverage(myShop,zip,user);
			
			nextView = "redirect:AgentView.htm?query=showNextOrder&timestamp="+new Date().getTime();
		}
		
		return nextView;
    }
	
    	
    
    @RequestMapping( value = "AgentViewSearchFlorist.htm", method = RequestMethod.GET )
    public String processSearchFlorist ( HttpServletRequest request, 
    							      ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			// clear gui messages
			order.getMessages().clear();
			
			//populateStates(order, model);
			
			model.addAttribute( "formShop", new WebAppShop() );
			
			nextView = "AgentViewSearchFlorist";
		}
		
		return nextView;
    }
	
    
    @RequestMapping( value = "AgentViewSearchFlorist.htm", method = RequestMethod.POST )
    public String processSearchFlorist( HttpServletRequest request,
	    						     @ModelAttribute("formShop") WebAppShop formShop, 
	    		                     BindingResult result, 
	    		                     SessionStatus status,
	    		                     ModelMap model ) {
        
        String nextView = "AgentViewSearchFlorist";
    	
		//WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);

    	getOrderInProcess( request, result );
    	//populateStates(order, model);
    	
    	
    	String shopCode = formShop.getShopCode();
    	
        if ( shopCode.trim().isEmpty() ) {
               
            result.reject("error.shop.nocode");
        }
        
        if ( result.hasErrors() ) {
        	// do nothing
        } else {
			try {
				
			    order.getMessages().clear();
			    Shop shopReturned = agentService.getShopByShopCode(shopCode);
			    List<Shop> recommendedFlorist= new ArrayList<Shop>();
			    recommendedFlorist = getRecommendedFlorists( order.getBean() );
			    if (shopReturned!=null){
			    	recommendedFlorist.add(shopReturned);
					order.getMessages().add("Florist ["+shopReturned.getShopName()+"] has been found");

			    }
			    else{
					order.getMessages().add("Florist ["+shopCode+"] has not been found");

			    }
				order.setRecommendedFlorists( recommendedFlorist);
				
				nextView = "AgentView";
				
			} catch (Exception e) {
				
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
			}
        }
		
		return nextView;
    } 	
        
    @RequestMapping( value = "AgentViewExitMultipleOrders.htm", method = RequestMethod.GET )
    public String processExitMultipleOrders( HttpServletRequest request,
    								HttpServletResponse response,
						            SessionStatus status,
						            ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}

		String nextView = "redirect:default.htm";
		List<Bomorder> orders = orderDAO.getOrdersInProgress( user.getUserId() );
		
		for(Bomorder orderb : orders){
			
			WebAppOrder order = new WebAppOrder(orderb);
			order.getMessages().clear();
		
			try {
				
				final String orderNumber = order.getBean().getBmtOrderNumber();
					
					
				List<Orderactivity> activities = new LinkedList<Orderactivity>();
				
				activities.addAll( order.getOrderactivities() );
				
				Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
				Collections.reverse(activities);
				
				ActRouting currentStatus = null;
					
				for( Orderactivity activity : activities ) {
						
					final ActRouting m_routing = activity.getActRouting();
						
					if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))
							&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED)))) {
						
						currentStatus = m_routing;
						break;
						
					}
				}
				
				order.setCurrentOrderStatus(currentStatus.getStsRouting().getStsRoutingId());
				order.setVirtualQueue(currentStatus.getVirtualQueue());
				order.setUser(userDAO.getUserById(1L));
				
				
				agentService.logCallNoForm( user, null, orderNumber );
				agentService.exitAssignment( order );
				try{
					sessionManager.removeMasterOrder(request);
				}catch(Exception ee){}
	
				nextView = "redirect:default.htm";
		
			} catch (Exception e) {
		
				order.getMessages().add(e.getMessage());
				e.printStackTrace();
	
				logger.error(e);
	
			}
		}
		List<String> messages = new ArrayList<String>();
		messages.add(orders.size()+" Orders Exited Successfully");
		model.addAttribute("messages", messages);
		
		return nextView;
    }
    

	@RequestMapping( value = "AgentViewExitOrder.htm", method = RequestMethod.GET )
    public String processExitOrder ( HttpServletRequest request, 
    							     WebAppOrder requestOrder, 
									 BindingResult result, 
									 ModelMap model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
				
		WebAppOrder order = getOrderInProcess( request, result );
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			model.put("command", new WebAppActLogacall() );
			model.put("defaultShopnetworkStatusId", CallDispDAO.NO_CONTACT_DEFAULT_SHOPNETWORK_STATUS_ID);
			
			
			List<Role> roles = user.getRoles();
			Map<String,Long> results = new LinkedHashMap<String,Long>();
				
				for(int i=0; i<roles.size(); ++i){
						results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
				}

				model.addAttribute("roles", results);
				
			order.setDispositions( callDispDAO.getAllCallDisp() );
			
			nextView = "AgentViewExitOrder";
		}
		
		return nextView;
    }
    
    
    @RequestMapping( value = "AgentViewExitOrder.htm", method = RequestMethod.POST )
    public String processExitOrder( HttpServletRequest request,
									@ModelAttribute("command") WebAppActLogacall formActLogacall, 
						            BindingResult result, 
						            SessionStatus status ) {

		String nextView = "AgentViewExitOrder";
		
		WebAppOrder order = getOrderInProcess( request, result );
		WebAppUser  user  = (WebAppUser)  sessionManager.getApplicationUser(request);
		
		formActLogacall = scrubLogacall.scrubActLogaCall(formActLogacall);
		logacallValidator.validateForm(formActLogacall, result);
		
		if ( result.hasErrors() ) {
			// do nothing
		} else {
		
			order.getMessages().clear();
		
			try {
				
				final String orderNumber = order.getBean().getBmtOrderNumber();
				
				
				if (formActLogacall.getDescription().equals(BOMConstants.LOGACALL_WFR)){
					
					byte wfrStatus = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP));

					order.setCurrentOrderStatus(wfrStatus);
					
				}
				else if (formActLogacall.getDescription().equals(BOMConstants.LOGACALL_WFTZ)){
					
					byte wftzStatus = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE));

					order.setCurrentOrderStatus(wftzStatus);
					
				}
				else if (formActLogacall.getDescription().equals(BOMConstants.LOGACALL_OUTSIDE)){
					
					byte atlasStatus = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_COMPLETED_OUTSIDE_BOM));

					order.setCurrentOrderStatus(atlasStatus);
					
				}
				else if (formActLogacall.getDescription().equals(BOMConstants.LOGACALL_COMPLETED)){
					
					byte completeStatus = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_SENT_TO_SHOP));

					order.setCurrentOrderStatus(completeStatus);
					
				}
				else if (formActLogacall.getDescription().equals(BOMConstants.LOGACALL_OTHER)){
					
					byte otherStatus = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED));

					byte currStatus = order.getCurrentOrderStatus();
					if ( currStatus == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)) || currStatus == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE))){
						order.setCurrentOrderStatus(otherStatus);
						order.setVirtualQueue(BOMConstants.TLO_QUEUE);
						order.setUser(userDAO.getUserById(1L));
					}else{
						
						List<Orderactivity> activities = new LinkedList<Orderactivity>();
						
						activities.addAll( order.getOrderactivities() );
						
						Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
						Collections.reverse(activities);
						
						ActRouting currentStatus = null;
							
						for( Orderactivity activity : activities ) {
								
							final ActRouting m_routing = activity.getActRouting();
								
							if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED)))) {
								
								currentStatus = m_routing;
								break;
								
							}
						}
						
						if ( currentStatus.getStsRouting().getStsRoutingId() == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)) || currentStatus.getStsRouting().getStsRoutingId() == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE))){
							order.setCurrentOrderStatus(otherStatus);
							order.setVirtualQueue(BOMConstants.TLO_QUEUE);
							order.setUser(userDAO.getUserById(1L));
							
						}else{
							order.setCurrentOrderStatus(currentStatus.getStsRouting().getStsRoutingId());
							order.setVirtualQueue(currentStatus.getVirtualQueue());
							order.setUser(userDAO.getUserById(1L));
						}
					}
				}else{
					byte currStatus = order.getCurrentOrderStatus();
					byte toBeWorked = Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_TO_BE_WORKED));
					
					if ( currStatus == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)) || currStatus == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE))){
						order.setUser(userDAO.getUserById(1L));
					}else if(currStatus == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED))){
						
						List<Orderactivity> activities = new LinkedList<Orderactivity>();
						
						activities.addAll( order.getOrderactivities() );
						
						Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
						Collections.reverse(activities);
						
						ActRouting currentStatus = null;
							
						for( Orderactivity activity : activities ) {
								
							final ActRouting m_routing = activity.getActRouting();
								
							if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))
									&& !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_MESSAGE_TO_BE_WORKED)))) {
								
								currentStatus = m_routing;
								break;
								
							}
						}
						
						if ( currentStatus.getStsRouting().getStsRoutingId() == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_RESP)) || currentStatus.getStsRouting().getStsRoutingId() == Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_WAITING_FOR_TIME_ZONE))){
							order.setUser(userDAO.getUserById(1L));
							order.setCurrentOrderStatus(currentStatus.getStsRouting().getStsRoutingId());
							
						}else{
							order.setCurrentOrderStatus(toBeWorked);
							order.setVirtualQueue(BOMConstants.TLO_QUEUE);
							order.setUser(userDAO.getUserById(1L));
						}
					}else{
						order.setCurrentOrderStatus(toBeWorked);
						order.setVirtualQueue(BOMConstants.TLO_QUEUE);
						order.setUser(userDAO.getUserById(1L));
					}
				}
				
				
				agentService.logCall( user, null, formActLogacall, orderNumber );
				agentService.exitAssignment( order );
				
				// remove order from session
				sessionManager.removeMasterOrder(request);
	
				nextView = "redirect:default.htm";
		
			} catch (Exception e) {
		
				order.getMessages().add(e.getMessage());
				e.printStackTrace();

				logger.error(e);
		
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
			}
		}
		
		return nextView;
    }
    
    
    private WebAppOrder getOrderInProcess( HttpServletRequest request, BindingResult result ) {
    	
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	
    	if ( order.isComplete() ) {
    		
    	    final String msg = "Order "+order.getBloomnetOrderNumber()+" had been completed";
    	    order.getMessages().clear();
    	    order.getMessages().add(msg);
    		result.reject("error.order.processed", new Object[] { order.getBloomnetOrderNumber() }, msg );
    	}
    	
    	return order;
    }
    
    
	private synchronized WebAppOrder findNextOrder(WebAppUser user) {

		String errorMessage = null;
		WebAppOrder order = new WebAppOrder();
		List<ActMessage> orderMessages = null;

		try {
			order = agentService.getNextWorkItem(user);
			agentService.getMessagesOnOrderFromQueue(order.getBean()); // if
																		// pulling
																		// order
																		// from
																		// db/session
																		// have
																		// to
																		// remove
																		// messages
																		// from
																		// queue
			orderMessages = messagingService.getMessagesOnOrder(user, order);
			order.setActionStr("AgentView");
			order.getBean().setReceivingShop(new Shop());
			order.setBloomnetOrderNumber(order.getBean().getBmtOrderNumber());
			
			List<Orderactivity> activities = new LinkedList<Orderactivity>();
			
			activities.addAll( order.getOrderactivities() );
			
			Collections.sort ( activities, OrderDAO.ORDER_ORDERACTIVITY_BY_DATE );
			Collections.reverse(activities);
			
			ActRouting currentStatus = null;
				
			for( Orderactivity activity : activities ) {
					
				final ActRouting m_routing = activity.getActRouting();
					
				if (m_routing != null && !m_routing.getStsRouting().getStsRoutingId().equals(Byte.valueOf(bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED)))) {
					currentStatus = m_routing;
					break;
					
				}
			}
			
			if(currentStatus.getStatus()!=null)
				order.setStatus(currentStatus.getStatus());
			
			Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber());
			
			String virtualQueueString = "";
			virtualQueueString = currentStatus.getVirtualQueue();
			
			if(currentStatus.getStatus().equals("Rejected by BloomNet")){
				
				order.setOutboundOrderNumber("Rejected");
				
			}else if(currentStatus.getStatus().equals("Cancel Confirmed by BloomNet")){
			
				order.setOutboundOrderNumber("Cancelled");
				
			}else if(currentStatus.getStatus().equals("Completed outside of BOM")){
			
				order.setOutboundOrderNumber("External");
				
			}else if(childOrder != null && !childOrder.getOrderNumber().equals(order.getOrderNumber())
					&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.getStatus().equals("To be worked")) 
					&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.getStatus().equals("Actively being worked"))
					&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.getStatus().equals("Waiting for Response"))
					&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.getStatus().equals("Waiting for Time Zone"))
					&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.getStatus().equals("Rejected by Shop"))){
				
				if((currentStatus.getStatus().equals("Message to be Worked") && childOrder.getOrderNumber() != null && childOrder.getOrderNumber().startsWith("BMT")) || !currentStatus.getStatus().equals("Message to be Worked")){
					order.getBean().setReceivingShop((childOrder.getShopByReceivingShopId()));
					order.setOutboundOrderNumber(childOrder.getOrderNumber());
				}
				
			}
			
		} catch (RecommendedFloristsException e) {

			errorMessage = e.getMessage();
			logger.error(e);
		} catch (Exception e) {

			errorMessage = e.getMessage();
			order.setActionStr("redirect:default.htm");
			logger.error(e);
			e.printStackTrace();
		} finally {

			order.getMessages().add(errorMessage);
			order.setOrderMessages(orderMessages);

			if (errorMessage == null) {

				order.getMessages().add(
						"Order [" + order.getBean().getOrderNumber()
								+ "] has been loaded");
				loadShopCode("sending", order, order.getSendingShop()
						.getShopId());
			}
		}

		return order;
	}

	
	private void populateStates( WebAppOrder order, ModelMap model ) {
		
		try {
			
			model.addAttribute( "states", shopDataService.getStates() );
			
		} catch (Exception e) {

			order.getMessages().add( e.getMessage() );
		}
	}

	
	/**
	 * Default action, displays the "Select Order Message" page.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping(value = "AgentViewMessageNew.htm", method = RequestMethod.GET)
	public String processMessageNew( HttpServletRequest request, 
									 SearchCriteria searchCriteria, 
									 BindingResult bindResult, 
									 Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
		String nextView = "AgentViewMessageNew";
		
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		WebAppActMessage  newMessage = new WebAppActMessage();
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm";
		} 
		else {
			
		}	
		
		model.addAttribute("command", newMessage );
		model.addAttribute("messageTypes", messageDAO.getMessageTypes() );

		return nextView;
	}
	
	
    @RequestMapping( value = "AgentViewMessageNew.htm", method = RequestMethod.POST )
    public String processMessageNew( HttpServletRequest request,
			    					 @ModelAttribute("command") WebAppActMessage formMessage, 
			    		             BindingResult result, 
			    		             SessionStatus status,
			    		             ModelMap model ) {
    	
    	String nextView = "AgentViewMessageNew";
    	
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
    	
    	final List<Messagetype> messageTypes = messageDAO.getMessageTypes();
    	model.addAttribute("messageTypes", messageTypes );
    	
    	formMessage = scrubWebAppActMessage.scrubWebAppActMessage(formMessage);
    	
		messageValidator.validate(formMessage, result);

        if ( result.hasErrors() ) {
        	// do nothing
        } else {
    	
        	for( Messagetype m_type : messageTypes ) {
        		
        		if( formMessage.getMessageTypeId() == m_type.getShortDesc() ) {
        			
        			formMessage.setMessagetype(m_type);
        		}
        	}
        	
        	try {
        		
        		messagingService.sendNewMessageToShop( user, order, formMessage );
        		formMessage.setSent( true );
        		if(formMessage.getMessageTypeId().equals("Dlcf") && formMessage.getMessageText() != null && !formMessage.getMessageText().equals("")){
        			formMessage.setMessageTypeId("Inqr");
        			messagingService.sendNewMessageToShop( user, order, formMessage );
        		}
        		if(order.isComplete() == true) nextView = "redirect:default.htm";

        	}
        	catch ( Exception e ) {
				e.printStackTrace();
				result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
				logger.error(e);
        	}
        }

		return nextView;
    }
    
    @RequestMapping( value = "AgentViewChangeStatus.htm", method = RequestMethod.GET )
    public String processChangeOrderStatus( HttpServletRequest request, 
    									    ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		final String nextView;
		
		final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order == null ) {
			
			nextView = "redirect:default.htm"; 
		}
		else {
			
			// clear gui messages
			order.getMessages().clear();
			
			
			model.addAttribute( "formOrder", order );
			model.addAttribute("statuses", orderDAO.getAllStatuses());
			nextView = "AgentViewChangeStatus";
		}
		
		return nextView;
    }
    
    @RequestMapping( value = "AgentViewChangeStatus.htm", method = RequestMethod.POST )
    public String processChangeOrderStatus( HttpServletRequest request,
    		@ModelAttribute("formOrder") WebAppOrder formOrder, 
    		BindingResult result, 
    		SessionStatus status,
    		ModelMap model ) {
    	
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		WebAppUser user = (WebAppUser) sessionManager.getApplicationUser(request);


    	String  nextView = "redirect:AgentView.htm?timestamp=" + new Date().getTime();
    	String orderNumber = order.getBean().getBmtOrderNumber();
    	String destination = BOMConstants.TLO_QUEUE;
    	String order_status = formOrder.getStatus();
		String userName     = user.getUserName();
		long childId = 0;

		
		try {
		Bomorder bomorder = orderDAO.getOrderByOrderNumber(orderNumber);

		

    	if (bomorder!=null){
    		
    		orderDAO.persistOrderactivityByUser(userName, 
    				BOMConstants.ACT_ROUTING, 
    				order_status, 
    				orderNumber, 
    				destination);
    		
    		childId = orderDAO.getCurrentFulfillingOrder(bomorder.getOrderNumber()).getBomorderId();
    		long parentOrderId = bomorder.getParentorderId();
    		
    		byte virtualqueueId = VirtualQueueUtil.getVirtualQueueId(destination );
			BomorderSts bomorderSts = orderDAO.getBomorderStsOrderId(bomorder.getBomorderId());
			bomorderSts.setCreatedUnixTime(DateUtil.convertDateToLong());
			bomorderSts.setStsRoutingId(Byte.parseByte(order_status));
			bomorderSts.setVirtualqueueId(virtualqueueId);
			
			if (childId==parentOrderId){
				orderDAO.updateBomorderSts(bomorderSts, 0);	
			}else{
			orderDAO.updateBomorderSts(bomorderSts, childId);
			}
			
    		order.getMessages().clear();
    		order.getMessages().add("order " + orderNumber + " upated to status "+ order_status+ ", queue " + destination);
    	}
		
		
		}catch( Exception e  ){

    		nextView  = "AgentViewChangeStatus";
    		model.addAttribute("statuses", orderDAO.getAllStatuses());
    		
    		order.getMessages().clear();
    		order.getMessages().add(e.getMessage());

    		result.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    	}
    	return nextView; 
		

    }
    
    
    @RequestMapping( value = "AgentViewCancelChangeStatus.htm", method = RequestMethod.GET )
    public String processCancelChangeOrderStatus( HttpServletRequest request, 
    									    ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
		String  nextView = "redirect:AgentView.htm?timestamp=" + new Date().getTime();

		
		return nextView;
    }
    
    @RequestMapping( value = "AgentViewRejectPhoneOrder.htm", method = RequestMethod.GET )
    @Transactional
    public String processPhoneRejection( HttpServletRequest request, 
    									    ModelMap model ) {
    	
    	WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
    	
    	String nextView = "redirect:AgentView.htm?query=showNextOrder&timestamp=" + new Date().getTime();
    	String rejectedOrderNumber = "RJCT" + new Date().getTime();
    	WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
    	
    	String currentBMTNumber = order.getOutboundOrderNumber();
    	
    	Bomorder currentChildOrder = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber());
    	currentChildOrder.setOrderNumber(rejectedOrderNumber);

    	orderDAO.updateChildOrder(currentChildOrder, order.getOrderNumber());
    	
    	try {
    		    		
			orderDAO.persistChildOrderAndActivity(currentChildOrder, 
					order.getOrderNumber(),
					user.getUserName(), 
				    BOMConstants.ACT_ROUTING, 
					bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP), 
					BOMConstants.TLO_QUEUE);
			
			orderDAO.persistOrderactivityByUser(user.getUserName(), BOMConstants.ACT_ROUTING, 
					bomProperties.getProperty(BOMConstants.ORDER_REJECTED_BY_SHOP), 
					order.getOrderNumber(), 
					BOMConstants.TLO_QUEUE );
			
			//update parent to be worked
			orderDAO.persistOrderactivityByUser( user.getUserName(), BOMConstants.ACT_ROUTING, 
					bomProperties.getProperty(BOMConstants.ORDER_BEING_WORKED), 
					order.getOrderNumber(), 
					BOMConstants.TLO_QUEUE );
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		Bomorder editedOrder = orderDAO.getOrderByOrderNumber(order.getOrderNumber());
    	order.setEntity(editedOrder);
    	order.setOutboundOrderNumber(null);
    	order.setComplete(false);
    	order.setCurrentOrderStatus(Byte.valueOf(bomProperties.getProperty("order.rjct")));
    	order.setOrderStatus("Rejected by Shop");
    	order.setOrderactivities(editedOrder.getOrderactivities());
		
		sessionManager.setMasterOrder(request, order);
		
		return nextView;
    }
    
    /**
	 * Show all messages for an order
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping( value = "AgentViewShowAllMessages.htm", method = RequestMethod.GET )
	public String showAllMessages( HttpServletRequest request, 
							   HttpServletResponse response,	
							   SearchCriteria searchCriteria, 
							   BindingResult bindResult, 
							   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "redirect:AgentView.htm?timestamp=" + new Date().getTime();

		try {
			
			final WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
			
			messagingService.showAllMessages(user, order);
			
		}catch ( Exception e ) {
			logger.error( e );
		}
		
		return nextView;
	}

	@Override
	protected ShopDataService getShopDataService() throws Exception {
		return shopDataService;
	}
}
