package com.bloomnet.bom.mvc.controller;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.service.ShopDataService;

/**
 * Controller for the "Default" web application screen.
 *
 * @author Danil Svirchtchev
 */
@Controller
@RequestMapping("/default.htm")
public class DefaultViewController {

    // Define a static logger variable
    static Logger logger = Logger.getLogger( DefaultViewController.class );
    
    @Autowired private SessionManager sessionManager;
    @Autowired private ShopDataService shopDataService;	
    @Autowired private BomorderviewV2DAO bomorderviewV2DAO;
    
    @RequestMapping(method = RequestMethod.GET)
	public String handleRequest( HttpServletRequest request,
								 ModelMap model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		try {
			
			Shop shop = shopDataService.getShopByCode( user.getSecurityElement().getShopcode() );
			user.setMyShop(shop);
		} 
		catch (Exception e) {
			
			logger.error(e);
		}
		
		if ( order != null ) {
			model.addAttribute( "messages", order.getMessages() );
			sessionManager.removeMasterOrder(request);
		}
		
		Date todayMidnight = new DateTime().toDateMidnight().toDate();
		Date tomorrowMidnight  = new DateTime(todayMidnight).plusHours(24).minusSeconds(1).toDate();
		new DateTime(todayMidnight).minusHours(24).toDateMidnight().toDate();
		
		int today = 0;
		int todayWFR = 0;
		int previous = 0;
		int previousWFR = 0;
		int future = 0;
		int futureWFR = 0;
		int totalWFR = 0;
		int totalMessages = 0;
		int totalWFTZ = 0;
		
		List<BomorderviewV2> orders = bomorderviewV2DAO.getOutstandingOrders();
		Map<String,String> orderOcassions = new HashMap<String,String>();
		
		for(BomorderviewV2 orderView : orders){
			if(orderView != null && orderView.getId() != null){
			
			Date deliveryDate = orderView.getId().getDeliveryDate();
			String status = orderView.getId().getStatus();
			String orderNumber = orderView.getId().getParentOrderNumber();
			if(status.equals("Message to be Worked")){
				totalMessages++;
			}
			if(deliveryDate.getTime() < todayMidnight.getTime()){
				if(!status.equals("Waiting for Response") && !status.equals("Waiting for Time Zone") && !status.equals("Message to be Worked")){
					previous++;
					orderOcassions.put(orderNumber, "previous");
				}
				else if(status.equals("Waiting for Response")){
					previousWFR++;
					totalWFR++;
				}
				else if(status.equals("Waiting for Time Zone")){
					totalWFTZ++;
				}
			}else if(deliveryDate.getTime() > tomorrowMidnight.getTime()){
				if(!status.equals("Waiting for Response") && !status.equals("Waiting for Time Zone") && !status.equals("Message to be Worked")){
					future++;
					orderOcassions.put(orderNumber, "future");
				}
				else if(status.equals("Waiting for Response")){
					futureWFR++; 
					totalWFR++;
				}
				else if(status.equals("Waiting for Time Zone")){
					totalWFTZ++;
				}
			}else{
				if(!status.equals("Waiting for Response") && !status.equals("Waiting for Time Zone") && !status.equals("Message to be Worked")){
					today++;
					orderOcassions.put(orderNumber, "today");
				}
				else if(status.equals("Waiting for Response")){
					todayWFR++;
					totalWFR++;
				}
				else if(status.equals("Waiting for Time Zone")){
					totalWFTZ++;
				}
			}
		}
		}
		
		/*Map<String, Integer> orderTypes = null;
		try {
			orderTypes = reportingService.getOrdersByOcassion(orderOcassions);
		} catch (Exception e) {
			e.printStackTrace();
		}*/
		
		try{
			List<Role> roles = user.getRoles();
			Map<String,Long> results = new LinkedHashMap<String,Long>();
				
				for(int i=0; i<roles.size(); ++i){
						results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
				}

			model.addAttribute("roles", results);
				
			model.addAttribute("totalWaiting", orders.size()-totalWFR-totalMessages-totalWFTZ);
			model.addAttribute("totalMessages", totalMessages);
			model.addAttribute("todayWaiting", today);
			model.addAttribute("totalWFR", totalWFR);
			model.addAttribute("todayWFR", todayWFR);
			model.addAttribute("previousWaiting", previous);
			model.addAttribute("previousWFR", previousWFR);
			model.addAttribute("futureWaiting", future);
			model.addAttribute("futureWFR", futureWFR);
			model.addAttribute("totalWFTZ", totalWFTZ);
			
		/*	model.addAttribute("funeral", orderTypes.get("funeral"));
			model.addAttribute("anniversary", orderTypes.get("anniversary"));
			model.addAttribute("birthday", orderTypes.get("birthday"));
			model.addAttribute("funeralToday", orderTypes.get("funeralToday"));
			model.addAttribute("anniversaryToday", orderTypes.get("anniversaryToday"));
			model.addAttribute("birthdayToday", orderTypes.get("birthdayToday"));
			model.addAttribute("funeralPrev", orderTypes.get("funeralPrev"));
			model.addAttribute("anniversaryPrev", orderTypes.get("anniversaryPrev"));
			model.addAttribute("birthdayPrev", orderTypes.get("birthdayPrev"));
			model.addAttribute("funeralFuture", orderTypes.get("funeralFuture"));
			model.addAttribute("anniversaryFuture", orderTypes.get("anniversaryFuture"));
			model.addAttribute("birthdayFuture", orderTypes.get("birthdayFuture"));*/
			
		}catch(Exception ee){
			logger.error(ee.getMessage());
		}
		return "default";
	}
}
