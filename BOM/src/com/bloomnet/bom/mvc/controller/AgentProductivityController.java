package com.bloomnet.bom.mvc.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.dao.UserDAO;
import com.bloomnet.bom.common.entity.ActRouting;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderSts;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.Userrole;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.common.util.VirtualQueueUtil;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.WebAppActMessage;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.BomService;
import com.bloomnet.bom.mvc.service.MessagingService;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.validators.MessageValidator;
import com.bloomnet.bom.mvc.validators.ProductivityValidator;
import com.bloomnet.bom.mvc.validators.ScrubData;

@Controller
public class AgentProductivityController {
	
    @Autowired private SessionManager sessionManager;
    @Autowired private BomService bomService;
	@Autowired private OrderDAO orderDAO;
	@Autowired private UserDAO userDAO;


    
    ScrubData scrubData = new ScrubData();



    @RequestMapping(value = "ProductivityView.htm", method = RequestMethod.GET)
    public String index(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(usera == null) {
			
			return "redirect:signin.htm";
		}

    	WebAppOrder  order = new WebAppOrder();
    	WebAppUser  user = new WebAppUser();
		Map<Long,String> results = new LinkedHashMap<Long,String>();
    	List<WebAppUser> beingWorked= new ArrayList<WebAppUser>();


		String nextView = "ProductivityView";
		
		try {
			List<User> userList = userDAO.getAllUsers();
			for (User aUser: userList){
				if(aUser.getUserId()!=1 &&
	    				aUser.getUserId()!=2 &&
	    				aUser.getUserId()!=3 &&
	    				(aUser.getActive() == null || !aUser.getActive().equals("N"))){
					
					results.put(aUser.getUserId(),aUser.getFirstName()+ " " + aUser.getLastName());
					
				}
			}
			
			
			beingWorked = bomService.getTLOAgentsWorking();

			model.addAttribute("order", order );
			model.addAttribute("command", user );
			model.addAttribute("users", results);
			model.addAttribute("beingWorked", beingWorked );
			model.addAttribute("beingWorkedCount", beingWorked.size() );

			
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
    
    
    @RequestMapping(value = "ProductivityView.htm", method = RequestMethod.POST)
    public String viewAgentProductivity(HttpServletRequest request, 
    		@ModelAttribute("order") WebAppOrder order,
    		@ModelAttribute("command") WebAppUser user,
    		ModelMap modeled,
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
		String nextView  = "ProductivityView";

    	WebAppOrder  wao = new WebAppOrder();
    	wao.setActionStr("stats");
    	List<BomorderviewV2> orders;
    	List<BomorderviewV2> touched;
    	
    
    	List<WebAppUser> beingWorked= new ArrayList<WebAppUser>();
    	
	
    	try {
    		
    		
    		
    		String dateStr = user.getSelectedDate();
			Date date  = DateUtil.toDateFormat(dateStr);
			
			if ((dateStr == null) || dateStr.equals("")) {
			    
				throw new Exception("Please enter date using MM/dd/yyyy format");
			}
    		
    		long user_id = user.getUserId();
    		orders = bomService.getTotalOrdersWorkedByDate(user_id, date);
    		
    		touched = bomService.getTotalOrdersTouchedByDate(user_id, date);
    		
    		Date lastlogin = bomService.getLastLogInTime(user_id);
    		

    		String workTime = bomService.getTotalWorkTimeByDate(user_id, date);
    		
    		beingWorked = bomService.getTLOAgentsWorking();
    		//List<Bomorder> ordersForToday = orderDAO.getOrdersForToday();
        	//List<Bomorder> ordersForTomorrow = orderDAO.getOrdersForTommorrow();
    		
    		model.addAttribute("orders", orders );
    		model.addAttribute("ordersCount", orders.size() );
    		model.addAttribute("touched", touched );
    		model.addAttribute("touchedCount", touched.size() );
    		model.addAttribute("beingWorked", beingWorked );
			model.addAttribute("beingWorkedCount", beingWorked.size() );
    		//model.addAttribute( "ordersTodayCount", ordersForToday.size() );
        	//model.addAttribute( "ordersTomorrowCount", ordersForTomorrow.size() );

    		if (lastlogin!=null){
    		model.addAttribute("lastlogin", lastlogin );
    		}
    		if (workTime!=null){
    		model.addAttribute("workTime",workTime);
    		}
    		index(request, searchCriteria, bindResult, model);

    		WebAppUser selectedUser = bomService.getSelectedUser(user_id);
    		wao.getMessages().clear();
    		wao.getMessages().add("Results for " + selectedUser.getFullName()+ " on " + dateStr);
    		
    		
    		
    	
    	} catch (Exception e) {
    		nextView  = "ProductivityView";
    		wao.getMessages().clear();
    		wao.getMessages().add(e.getMessage());
    		bindResult.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();
			//nextView = "ProductivityView.htm?timestamp=" + new Date().getTime(); 

    		System.err.println(e.getMessage());
    		index(request, searchCriteria, bindResult, model);

		}
    	
    	sessionManager.setMasterOrder(request, wao);

    	return nextView;
    }
    
   
    
   
	 
}
