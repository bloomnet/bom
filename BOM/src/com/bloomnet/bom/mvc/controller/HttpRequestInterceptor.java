package com.bloomnet.bom.mvc.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.web.servlet.HandlerInterceptor;

import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.utils.SimpleHttpSessionManagerImpl;

/**
 * Intercepts HTTP requests to ensure user is signed in; it also closes the
 * Hibernate session for the current thread.
 *
 * @author Danil Svirchtchev
 */
public class HttpRequestInterceptor implements HandlerInterceptor {
    
    
    // Define a static logger variable
    static final Logger logger = Logger.getLogger( HttpRequestInterceptor.class );
    
    
    private String         signInPage = "signin.htm";
    private SessionManager sessionManager;
    
    
    /**
     * Uses SessionManager to ensure user is logged in; if not, then
     * user is forwarded to the sign-in page.
     *
     * @see SimpleHttpSessionManagerImpl
     */
    public boolean preHandle( HttpServletRequest request, 
                              HttpServletResponse response, 
                              Object handler) throws Exception {
        
        final boolean result;
        
        WebAppUser user = (WebAppUser) sessionManager.getApplicationUser(request);
        
        if ( user == null ) {
            
            response.sendRedirect( signInPage );
            result = false;
        }
        else {
            
            final String requestURI = request.getRequestURI();

            if ( user.isViewAllowed( requestURI ) ) { 
            
                result = true;
            }
            else {
                
                /* TODO: Could redirect back to the page where user came from, by
                         extracting the referer string from the http header. */
                response.sendRedirect( signInPage );
                result = false;
            }
        }
        
        return result;
    }
    

    ///////////////////////////////////////////////////////////////////////////
    //
    // START SETTERS AND GETTERS FOR SPRING DEPENDENCY INJECTION
    //
    public String getSignInPage() {        
        return signInPage;
    }
    public void setSignInPage(String signInPage) {
        this.signInPage = signInPage;
    }
    
    public SessionManager getSessionManager() {
        return sessionManager;
    }
    public void setSessionManager(SessionManager sessionManager) {
        this.sessionManager = sessionManager;
    }
}
