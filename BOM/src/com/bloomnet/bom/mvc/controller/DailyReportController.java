package com.bloomnet.bom.mvc.controller;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.hibernate.HibernateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.FulfillmentSummaryDaily;
import com.bloomnet.bom.common.entity.FulfillmentSummaryMtd;
import com.bloomnet.bom.common.entity.WorkOverviewDaily;
import com.bloomnet.bom.common.util.DateUtil;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.service.ReportingService;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.validators.ScrubData;

@Controller
public class DailyReportController {
	
    @Autowired private SessionManager sessionManager;
	@Autowired private ReportingService reportingService;
	
	// Injected property
	@Autowired private Properties bomProperties;
    
    ScrubData scrubData = new ScrubData();



    @RequestMapping(value = "ReportView.htm", method = RequestMethod.GET)
    public String index(HttpServletRequest request, 
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
    	WebAppUser  usera  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(usera == null) {
			
			return "redirect:signin.htm";
		}

    	
    	sessionManager.removeMasterOrder(request);
    	sessionManager.removeSelectedDate(request);
		sessionManager.removeSelectedEndDate(request);


    	WebAppOrder  order = new WebAppOrder();
    	WebAppUser  user = new WebAppUser();
    	List<FulfillmentSummaryDaily> summaryDailyList = new ArrayList<FulfillmentSummaryDaily>();
    	List<FulfillmentSummaryMtd> summaryMtdList = new ArrayList<FulfillmentSummaryMtd>();
    	List<WorkOverviewDaily> workDailyAutoList  = new ArrayList<WorkOverviewDaily>(); 
    	List<WorkOverviewDaily> workDailyManList = new ArrayList<WorkOverviewDaily>(); 

    	Date date = new Date();
       
		String nextView = "ReportView";
		
		boolean fileExist = false;
		
		String dateStr = DateUtil.todashFormatString(date);
		
		try{
		

			if (!summaryDailyList.isEmpty()){
				model.addAttribute("summaryDailyList", summaryDailyList );
			}
			if (!summaryMtdList.isEmpty()){
				model.addAttribute("summaryMtd", summaryMtdList.get(0) );
			}
			if (!workDailyAutoList.isEmpty()){
				model.addAttribute("workDailyAuto", workDailyAutoList.get(0) );
			}
			if (!workDailyManList.isEmpty()){
				model.addAttribute("workDailyTLO", workDailyManList.get(0) );
			}
				
			model.addAttribute("dailyUnassigned", 0.0 );
			model.addAttribute("dailyAssigned", 0.0 );
			model.addAttribute("dailyRjct", 0.0 );
			model.addAttribute("dailyCanc", 0.0 );
			model.addAttribute("dailyDlcf", 0.0 );
			model.addAttribute("mtdUnassigned", 0.0 );
			model.addAttribute("mtdAssigned", 0.0 );
			model.addAttribute("mtdRjct", 0.0 );
			model.addAttribute("mtdCanc", 0.0 );
			model.addAttribute("mtdDlcf", 0.0 );
			
		
			
			model.addAttribute("order", order );
			model.addAttribute("command", user );
			model.addAttribute("now", date );
			
	
			
		} catch (HibernateException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	sessionManager.setMasterOrder(request, order);

    	return nextView;
    }
    
    
    @RequestMapping(value = "ReportView.htm", method = RequestMethod.POST)
    public String viewDailyMetrics(HttpServletRequest request, 
    		@ModelAttribute("order") WebAppOrder order,
    		@ModelAttribute("command") WebAppUser user,
    		ModelMap modeled,
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model){
    	
		String nextView  = "ReportView";

    	WebAppOrder  wao = new WebAppOrder();
    	wao.setActionStr("export");

      	List<FulfillmentSummaryDaily> summaryDailyList;
    	List<FulfillmentSummaryMtd> summaryMtdList = new ArrayList<FulfillmentSummaryMtd>();
    	List<WorkOverviewDaily> workDailyAutoList; 
    	List<WorkOverviewDaily> workDailyManList; 	
    	String dateStr = new String();
    	String endDateStr = new String();
    	

    	
    	dateStr = user.getSelectedDate();
		Date date  = DateUtil.toDateFormat(dateStr);
		endDateStr = user.getEndDate();
		Date endDate  = DateUtil.toDateFormat(endDateStr);

	
    	try {

			if ((dateStr == null) || dateStr.equals("")) {
			    
				throw new Exception("Please enter date using MM/dd/yyyy format");
			}
    		

			if (endDate != null) {
				
				summaryDailyList = reportingService.getFulfillmentSummaryDailyByDateRange(dateStr, endDateStr);
				summaryMtdList = reportingService.getFulfillmentSummaryMtdByDate(endDate);
				workDailyAutoList  = reportingService.getAutomatedWorkOverviewDailyByDateRange(date, endDate);
				workDailyManList =  reportingService.getTLOWorkOverviewDailyByDateRange(date, endDate);
				
			}else{
				
				summaryDailyList = reportingService.getFulfillmentSummaryDailyByDate(date);
				summaryMtdList = reportingService.getFulfillmentSummaryMtdByDate(date);
				workDailyAutoList  = reportingService.getAutomatedWorkOverviewDailyByDate(date);
				workDailyManList =  reportingService.getTLOWorkOverviewDailyByDate(date);
			}
			
			

			
			if (!summaryDailyList.isEmpty()){
				model.addAttribute("summaryDailyList", summaryDailyList );

			}
			if (!summaryMtdList.isEmpty()){
				model.addAttribute("summaryMtd", summaryMtdList.get(0) );
				
				double unassigned = (double)summaryMtdList.get(0).getUnassignedOrders()/(double)summaryMtdList.get(0).getTotalOrders();
				double assigned = (double)summaryMtdList.get(0).getAssignedActiveOrders()/(double)summaryMtdList.get(0).getTotalOrders();
				double rjct = (double)summaryMtdList.get(0).getRejectedOrders()/(double)summaryMtdList.get(0).getTotalOrders();
				double canc = (double)summaryMtdList.get(0).getCanceledOrders()/(double)summaryMtdList.get(0).getTotalOrders();
				double dlcf = (double)summaryMtdList.get(0).getDeliveryConfirmations()/(double)summaryMtdList.get(0).getAssignedActiveOrders();
				
				if (Double.isNaN(unassigned)){
					unassigned=0;
				}
				if (Double.isNaN(assigned)){
					assigned=0;
				}
				if (Double.isNaN(rjct)){
					rjct=0;
				}
				if (Double.isNaN(canc)){
					canc=0;
				}
				if (Double.isNaN(dlcf)){
					dlcf=0;
				}

				model.addAttribute("mtdUnassigned", unassigned );
				model.addAttribute("mtdAssigned", assigned );
				model.addAttribute("mtdRjct", rjct );
				model.addAttribute("mtdCanc", canc );
				model.addAttribute("mtdDlcf", dlcf );
			}
			if (!workDailyAutoList.isEmpty()){
				model.addAttribute("workDailyAutoList", workDailyAutoList );
			}
			if (!workDailyManList.isEmpty()){
				model.addAttribute("workDailyTLOList", workDailyManList );
			}
			model.addAttribute("now", date );

    		
			Date selectedDate  = DateUtil.toDateFormat(dateStr);
	        String selectedDateStr = DateUtil.toXmlDateFormatString(selectedDate);
	        
			if (endDate != null) {
				
				reportingService.generateMetricsDRSpreadSheet(summaryDailyList, summaryMtdList, 
						workDailyManList, workDailyAutoList,date,endDate );
				
			}else{
				reportingService.generateMetricsSpreadSheet(summaryDailyList.get(0), summaryMtdList.get(0), 
				workDailyManList.get(0), workDailyAutoList.get(0),selectedDateStr );
			}
				
	       
    		
    	}catch (IndexOutOfBoundsException ioobe){
    		nextView  = "ReportView";
    		wao.getMessages().clear();
    		wao.getMessages().add("No results for " + DateUtil.toWeekString(date));
    		bindResult.reject("java.exception.message", new Object[] { "No results found" }, null );
    		ioobe.printStackTrace();

    		index(request, searchCriteria, bindResult, model);
    	
    	} catch (Exception e) {
    		nextView  = "ReportView";
    		wao.getMessages().clear();
    		wao.getMessages().add(e.getMessage());
    		bindResult.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();

    		System.err.println(e.getMessage());
    		index(request, searchCriteria, bindResult, model);

		}
    	
    	sessionManager.setSelectedDate(request, dateStr);
    	sessionManager.setSelectedEndDate(request, endDateStr);
    	sessionManager.setMasterOrder(request, wao);
    	

    	return nextView;
    }
    
    
   
    /**
     * export to spreadsheet
     */
	@RequestMapping(value = "exportxls.htm", method = RequestMethod.GET)
    public String exportToSpreadsheet(HttpServletRequest request,
    		@ModelAttribute("order") WebAppOrder order,
    		SearchCriteria searchCriteria, 
    		BindingResult bindResult, 
    		Model model,
            HttpServletResponse response) {
    	     
    	WebAppOrder  wao = new WebAppOrder();
		String nextView = "ReportView";


        try {
			String selectedDate = (String)sessionManager.getSelectedDate(request);
			String endDate = (String)sessionManager.getSelectedEndDate(request);

			
			if ((selectedDate == null) || selectedDate.equals("")) {
			    
				throw new Exception("Please search by date using MM/dd/yyyy format");
			}
			
	
			
			Date date  = DateUtil.toDateFormat(selectedDate);
			Date date2  = DateUtil.toDateFormat(endDate);

			boolean endDateExist = (date2!=null);
			 
	           String fromDateStr = DateUtil.toXmlDateFormatString(date);
	           String toDateStr = DateUtil.toXmlDateFormatString(date2);
	           
	           String fileNameDate = new String();
	           if (endDateExist){
	               fileNameDate = fromDateStr+ "_thru_"+toDateStr;  

	           }else{
	        	   fileNameDate = fromDateStr;

	           }
	         
      
			 String rpt_dir = bomProperties.getProperty(BOMConstants.REPORT_DIR);
			 String filename = rpt_dir+"BOM_Metrics_" + fileNameDate +".xls";

			File file = new File(filename);
			FileInputStream fileIn = new FileInputStream(file);

			response.setContentType("application/octet-stream");
			response.setHeader("Content-Disposition","attachment; filename=\"" + "BOM_Metrics_" + fileNameDate +".xls" +"\"");

			ServletOutputStream out = response.getOutputStream();
			
			byte[] outputByte = new byte[4096];
			//copy binary contect to output stream
			while(fileIn.read(outputByte, 0, 4096) != -1)
			{
				out.write(outputByte, 0, 4096);
			}
			fileIn.close();
			out.flush();
			out.close();
		} 	 catch (Exception e) {
    		wao.getMessages().clear();
    		wao.getMessages().add(e.getMessage());
    		bindResult.reject("java.exception.message", new Object[] { e.getMessage() }, null );
    		e.printStackTrace();
    		
    		index(request, searchCriteria, bindResult, model);

        	return nextView;


		}

        

        return null;

    }
   
      
}