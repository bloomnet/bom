package com.bloomnet.bom.mvc.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.support.SessionStatus;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.dao.BomorderviewV2DAO;
import com.bloomnet.bom.common.dao.OrderDAO;
import com.bloomnet.bom.common.entity.Bomorder;
import com.bloomnet.bom.common.entity.BomorderviewV2;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteria;
import com.bloomnet.bom.mvc.businessobjects.SearchCriteriaExtended;
import com.bloomnet.bom.mvc.businessobjects.WebAppOrder;
import com.bloomnet.bom.mvc.businessobjects.WebAppUser;
import com.bloomnet.bom.mvc.controller.AdminViewController.Page;
import com.bloomnet.bom.mvc.exceptions.SearchParameterNotSupportedException;
import com.bloomnet.bom.mvc.service.AdminService;
import com.bloomnet.bom.mvc.service.BomService;
import com.bloomnet.bom.mvc.service.SessionManager;
import com.bloomnet.bom.mvc.service.ShopDataService;
import com.bloomnet.bom.mvc.validators.ScrubData;

/**
 * Controller for the simple order search by order number.
 *
 * @author Danil Svirchtchev
 */
@Controller
public class SearchController extends AbstractController {
	
	ScrubData scrubData = new ScrubData();

    // Define a static logger variable
    static Logger logger = Logger.getLogger( SearchController.class );
    
	private static TreeMap<Integer, Page> PAGES_MAP = new TreeMap<Integer, Page>();

    
    @Autowired private SessionManager  sessionManager;
    @Autowired private AdminService    adminService;
    @Autowired private ShopDataService shopDataService;
    @Autowired private OrderDAO orderDAO;
	@Autowired private BomService bomService;

    
    
	/**
	 * This method will render an XML order view accessible via order.xml
	 * 
	 * @param request
	 * @param searchCriteria
	 * @param bindResult
	 * @param model
	 * @return
	 */
	@RequestMapping(value = "order.json", method = RequestMethod.GET)
	@Transactional
	public String createJsonOrder( HttpServletRequest request, 
								   HttpServletResponse response,
					               SearchCriteria searchCriteria, 
					               BindingResult bindResult, 
					               Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "order";
		
		try {

			Long selectedOrderId = null;
			
			if ( ( searchCriteria.getQuery() != null ) && !searchCriteria.getQuery().equals("") ) {
				
				selectedOrderId = Long.valueOf( searchCriteria.getQuery() );
			}
			
			// Get selected order from the search results list
			// stored in user session
			
			Bomorder selectedOrder = orderDAO.getOrderByOrderId(selectedOrderId);
				
			Shop origReceivingShop = selectedOrder.getShopByReceivingShopId();
			
			if( selectedOrder.getBomorderId().equals( selectedOrderId ) ) {
				
				Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(selectedOrder.getOrderNumber());
				
				WebAppOrder wao = new WebAppOrder( selectedOrder );
				
				String virtualQueueString = "";
				virtualQueueString = orderDAO.getLatestOrderActivity(wao).getActRouting().getVirtualQueue();
				String currentStatus = wao.getStatus();
				
				if(childOrder != null && currentStatus.equals("Rejected by BloomNet")){
					
					wao.setOutboundOrderNumber("Rejected");
					
				}else if(childOrder != null && currentStatus.equals("Cancel Confirmed by BloomNet")){
				
					wao.setOutboundOrderNumber("Cancelled");
					
				}else if(currentStatus.equals("Completed outside of BOM")){
				
					wao.setOutboundOrderNumber("External");
					
				}else if(childOrder != null && !childOrder.getOrderNumber().equals(selectedOrder.getOrderNumber())
						&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.equals("To be worked")) 
						&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.equals("Actively being worked"))
						&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.equals("Waiting for Response"))
						&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.equals("Waiting for Time Zone"))
						&& !(virtualQueueString.equals(BOMConstants.TLO_QUEUE) && currentStatus.equals("Rejected by Shop"))){
					
					if((currentStatus.equals("Message to be Worked") && childOrder.getOrderNumber() != null && childOrder.getOrderNumber().startsWith("BMT")) || !currentStatus.equals("Message to be Worked")){
						selectedOrder.setShopByReceivingShopId(childOrder.getShopByReceivingShopId());
						wao = new WebAppOrder(selectedOrder);
						wao.setOrderNumber(selectedOrder.getOrderNumber());
						wao.setOutboundOrderNumber(childOrder.getOrderNumber());
					}
					
				}
				
				wao.getEntity().setShopByReceivingShopId(origReceivingShop);
				
				model.addAttribute( "order", wao);
				sessionManager.setMasterOrder( request, wao );

			}
			selectedOrderId = null;
		}
		catch ( Exception e ) {
			
			model.addAttribute( "error", e.getMessage() );
			logger.error( e );
			e.printStackTrace();
		}
		
		return nextView;
	}


	/**
	 * Default action, displays the "Order search" XML page.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping( value = "search.xml", method = RequestMethod.GET )
	public String index( HttpServletRequest request, 
						 HttpServletResponse response,	
						 SearchCriteria searchCriteria, 
						 BindingResult bindResult, 
						 Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
		
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "search";

		WebAppOrder order = (WebAppOrder) sessionManager.getMasterOrder(request);
		
		if ( order != null ) {
			
			final String oldOrderNumber = order.getOrderNumber();
			model.addAttribute( "sessionOrderNumber", oldOrderNumber );
		}
		
		try {
			
			SearchCriteriaExtended query = new SearchCriteriaExtended();
			
			if ( ( searchCriteria.getQuery() != null ) && !searchCriteria.getQuery().equals("") ) {
				
				String q = searchCriteria.getQuery();
				
				q = scrubData.scrubData(q);
				
				if( q.indexOf('/') > 0 ) {
					
					query.setDate( q );
				}
				else {
					
					query.setOrderNumber( q );
				}
			}
			
			// Get search results:
			if ( isAllEmpty( query ) ) {
				
				model.addAttribute( "error", "Please enter a search criteria" );
			}
			else {
				long currTime = System.currentTimeMillis();
				doSearch( request, extractObjectValues( query ) );
				long endTime = System.currentTimeMillis();
				System.out.println(endTime - currTime);
				model.addAttribute( "orders", sessionManager.getSearchResults(request) );
				Long ordersCount = Long.valueOf(sessionManager.getSearchResults(request).size());
				model.addAttribute( "ordersCount", ordersCount);
			}
			
			query = null;
		}
		catch ( Exception e ) {
			
			StringBuffer error = new StringBuffer ( e.getMessage()+" for "+searchCriteria.getQuery() );
			model.addAttribute( "error", error );
			logger.error( e );
		}
		
		return nextView;
	}
	
	
	/**
	 * Default action, displays the "Order Search" XML page for ManagerView.
	 * 
	 * @param searchCriteria The criteria to search for
	 */
	@RequestMapping( value = "ManagerViewSearch.xml", method = RequestMethod.GET )
	public String index( HttpServletRequest request, 
						 HttpServletResponse response,
						 SearchCriteriaExtended query, 
						 BindingResult bindResult, 
						 Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "ManagerViewSearch";

		try {
			
			// Get search results:
			if ( isAllEmpty( query ) ) {
				
				model.addAttribute( "error", "Please enter a search criteria" );
			}
			else {
				
				List<Bomorder> result = new ArrayList<Bomorder>();
				List<Bomorder> ordersForToday = orderDAO.getOrdersForToday();
	        	List<Bomorder> ordersForTomorrow = orderDAO.getOrdersForTommorrow();
	        	
				result = doManagerSearch( request, extractObjectValues( query ) );
				model.addAttribute( "orders", sessionManager.getSearchResults(request) );
				model.addAttribute( "ordersCount", result.size() );
	        	//model.addAttribute( "paginationLinks", createPaginationLinks( nextView ) );
	        	model.addAttribute( "ordersTodayCount", ordersForToday.size() );
	        	model.addAttribute( "ordersTomorrowCount", ordersForTomorrow.size() );
	            //model.addAttribute( "orders", getOrders( "1", result ) );

			}
		}
		catch ( Exception e ) {
			
			model.addAttribute( "error", e.getMessage() );
			logger.error( e );
		}
		
		return nextView;
	}
	
	
	/**
	 * Process GET method for URL searchResult.htm and load the "formSearch" - 
	 * a web form backing object.
	 * 
	 * @param request
	 * @param response
	 * @param query
	 * @param bindResult
	 * @param model
	 * @return
	 */
	@RequestMapping( value = "searchResults.htm", method = RequestMethod.GET )
	public String processSearchOrder ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
		List<Bomorder> orders = (List<Bomorder>) sessionManager.getSearchResults(request);
		List<Bomorder> results = new ArrayList<Bomorder>();
		
		for( Bomorder order : orders ) {
			
			Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber());
			
			order.setShopByReceivingShopId(childOrder.getShopByReceivingShopId());
			results.add(order);

		}
		
		

		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", results );
		model.addAttribute( "ordersCount", results.size() );

		
		return nextView;
	}
	

	@RequestMapping( value = "searchResults.htm", method = RequestMethod.POST )
	public String processSearchOrder ( HttpServletRequest request,
									   @ModelAttribute("formSearch") SearchCriteriaExtended query, 
						               BindingResult result, 
						               SessionStatus status,
						               Model model ) {
		
		final String nextView = "searchResults";
		
		List<WebAppOrder> orders = (List<WebAppOrder>) sessionManager.getSearchResults(request);
		List<WebAppOrder> results = new ArrayList<WebAppOrder>();
		
		for( Bomorder order : orders ) {
			
			Bomorder childOrder = orderDAO.getCurrentFulfillingOrder(order.getOrderNumber());
			
			order.setShopByReceivingShopId(childOrder.getShopByReceivingShopId());
			WebAppOrder wao = new WebAppOrder(order);
			results.add(wao);

		}
		
		
		model.addAttribute( "orders", results );
		model.addAttribute( "ordersCount", results.size() );
		
		return nextView;
	}
	
	
	private Map<String, String> extractObjectValues( SearchCriteriaExtended query ) {
		
		Map<String, String> result = new TreeMap<String, String>();
		
		String criteria = query.toString();
		
		int firstBracket  = criteria.indexOf('['); 
		int secondBracket = criteria.lastIndexOf(']'); 
		
		criteria = criteria.substring(firstBracket+1, secondBracket);
		
		String kv[] = criteria.split(",");
		
		for( int i = 0; i < kv.length; i++ ) {
			
			String tmp = kv[i].trim();
			
			if ( tmp.indexOf("=") > 0 ) {
				
				String[] keyVal = kv[i].trim().split("=");
				
				if( keyVal.length == 1 ){
					
					result.put( keyVal[0], null );
				}
				else {
					
					result.put( keyVal[0], keyVal[1] );
				}
			}
		}
		
		return result;
	}
	
	/**
	 * Perform simple validation on the empty fields.
	 * 
	 * @param request
	 * @param query
	 * @param MODEL
	 */
	private boolean isAllEmpty( SearchCriteriaExtended query ) {
		
		final boolean result;
		
		String criteria = query.toString();
		
		int firstBracket  = criteria.indexOf('['); 
		int secondBracket = criteria.lastIndexOf(']'); 
		
		criteria = criteria.substring(firstBracket+1, secondBracket);
		
		if ( ( criteria == null ) || criteria.equals("") ) {

			result = true;
		}
		else {
			
			result = false;
		}
		
		return result;
	}


	/**
	 * Populate SEARCHRESULT session object based on the extended search criteria
	 * 
	 * @param request
	 * @param params
	 * @throws Exception
	 */
	private void doSearch( HttpServletRequest request, 
						   Map<String, String> params ) throws Exception {

		List<Bomorder> result = null;
		
		try {
			result = adminService.getOrders( params );
		} 
		catch ( SearchParameterNotSupportedException e ) {

			logger.error( e );
		}
		finally {
			
			sessionManager.setSearchResults( request, result );
		}
	}
	
	/**
	 * Populate SEARCHRESULT session object based on the extended search criteria
	 * 
	 * @param request
	 * @param params
	 * @throws Exception
	 */
	private List<Bomorder> doManagerSearch( HttpServletRequest request, 
						   Map<String, String> params ) throws Exception {

		List<WebAppOrder> result = null;//		result = convert(parentorders, true);
		List<Bomorder> bomorders = new ArrayList<Bomorder>();
		
		try {
			bomorders = adminService.getManagerOrders( params );
		} 
		catch ( SearchParameterNotSupportedException e ) {

			logger.error( e );
		}
		finally {
			result = convert(bomorders, true);

			sessionManager.setSearchResults( request, result );
		}
		return bomorders;
	}
	
	
	@Override
	protected ShopDataService getShopDataService() throws Exception {
		return shopDataService;
	}
	
	
	@RequestMapping( value = "wfr.htm", method = RequestMethod.GET )
	public String processWFROrder ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
//
		List<Bomorder> orders = bomService.getWFRS();			
		sessionManager.setSearchResults( request, orders );
		//
		
		List<Role> roles = user.getRoles();
		Map<String,Long> results = new LinkedHashMap<String,Long>();
			
			for(int i=0; i<roles.size(); ++i){
					results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
			}

		model.addAttribute("roles", results);
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", orders );
		Long ordersCount = Long.valueOf(orders.size());
		model.addAttribute( "ordersCount", ordersCount);
		request.setAttribute("fromWFR", "true");
		
		return nextView;
	}
	
	@RequestMapping( value = "wftz.htm", method = RequestMethod.GET )
	public String processWFTZOrder ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
//
		List<Bomorder> orders = bomService.getWFTZS();			
		sessionManager.setSearchResults( request, orders );
		//
		
		List<Role> roles = user.getRoles();
		Map<String,Long> results = new LinkedHashMap<String,Long>();
			
			for(int i=0; i<roles.size(); ++i){
					results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
			}

		model.addAttribute("roles", results);
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", orders );
		Long ordersCount = Long.valueOf(orders.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		return nextView;
	}
	
	@RequestMapping( value = "auto.htm", method = RequestMethod.GET )
	public String processAutomatedOrder ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
//
		List<Bomorder> gottenOrders = bomService.getOutstandingAutomatedOrders();			
		sessionManager.setSearchResults( request, gottenOrders );
		
		List<Role> roles = user.getRoles();
		Map<String,Long> results = new LinkedHashMap<String,Long>();
			
			for(int i=0; i<roles.size(); ++i){
					results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
			}

		model.addAttribute("roles", results);
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", gottenOrders );
		Long ordersCount = Long.valueOf(gottenOrders.size());
		model.addAttribute( "ordersCount", ordersCount);
		request.setAttribute("fromAutomated", "true");
		
		return nextView;
	}
	
	@RequestMapping( value = "outstanding.htm", method = RequestMethod.GET )
	public String processOutstandingOrders ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
//
		List<Bomorder> orders = bomService.getOutstandingOrders();
		sessionManager.setSearchResults( request, orders );
		//
		
		List<Role> roles = user.getRoles();
		Map<String,Long> results = new LinkedHashMap<String,Long>();
			
			for(int i=0; i<roles.size(); ++i){
					results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
			}

			model.addAttribute("roles", results);
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", orders );
		model.addAttribute("recipientLastName");
		Long ordersCount = Long.valueOf(orders.size());
		model.addAttribute( "ordersCount", ordersCount);
		request.setAttribute("noCheckBoxes", "true");
		
		return nextView;
	}
	
	@RequestMapping( value = "outstandingbystate.htm", method = RequestMethod.GET )
	public String processOutstandingOrdersByState ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
		String state = request.getParameter("stateName");
		List<Bomorder> gottenOrders = new ArrayList<Bomorder>();
		if(state != null && !state.equals(""))
			gottenOrders = bomService.getOutstandingOrdersByState(state);
		sessionManager.setSearchResults( request, gottenOrders );
		//
		
		List<Role> roles = user.getRoles();
		Map<String,Long> results = new LinkedHashMap<String,Long>();
			
			for(int i=0; i<roles.size(); ++i){
					results.put(roles.get(i).getDescription(),roles.get(i).getRoleId());
			}

			model.addAttribute("roles", results);
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", gottenOrders );
		Long ordersCount = Long.valueOf(gottenOrders.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		return nextView;
	}
	
	@RequestMapping( value = "outstandingToday.htm", method = RequestMethod.GET )
	public String processTodayOutstandingOrders ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
		List<Bomorder> gottenOrders = bomService.getTodayOutstandingOrders();			
		sessionManager.setSearchResults( request, gottenOrders );
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", gottenOrders );
		Long ordersCount = Long.valueOf(gottenOrders.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		return nextView;
	}
	
	
	@RequestMapping( value = "outstandingPrevious.htm", method = RequestMethod.GET )
	public String processPreviousOutstandingOrders ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
		List<Bomorder> gottenOrders = bomService.getPreviousOutstandingOrders();			
		sessionManager.setSearchResults( request, gottenOrders );
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", gottenOrders );
		Long ordersCount = Long.valueOf(gottenOrders.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		return nextView;
	}
	
	
	@RequestMapping( value = "outstandingFuture.htm", method = RequestMethod.GET )
	public String processFutureOutstandingOrders ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
		List<Bomorder> gottenOrders = bomService.getFutureOutstandingOrders();			
		sessionManager.setSearchResults( request, gottenOrders );
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", gottenOrders );
		Long ordersCount = Long.valueOf(gottenOrders.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		return nextView;
	}
	
	@RequestMapping( value = "outstandingMessages.htm", method = RequestMethod.GET )
	public String processOutstandingMessages ( HttpServletRequest request, 
									   HttpServletResponse response,
									   SearchCriteriaExtended query, 
									   Model model ) {
		
		WebAppUser  user  = (WebAppUser) sessionManager.getApplicationUser(request);
		if(user == null) {
			
			return "redirect:signin.htm";
		}
		
        // set the response parameters
        response.setHeader("Cache-Control", "no-cache"); 
		
		final String nextView = "searchResults";
		
		List<Bomorder> gottenOrders = bomService.getOutstandingMessages();			
		sessionManager.setSearchResults( request, gottenOrders );
		
		model.addAttribute( "formSearch", new SearchCriteriaExtended() );
		model.addAttribute( "orders", gottenOrders );
		Long ordersCount = Long.valueOf(gottenOrders.size());
		model.addAttribute( "ordersCount", ordersCount);
		
		return nextView;
	}
	
	
	
	
	
    /**
     * Simple inner class to represent a page
     * 
     * @author Administrator
     *
     */
    protected class Page {
    	
    	public int startIndex, pageSize;
    	
		public Page( int startIndex, int pageSize ) {

			this.startIndex = startIndex;
			this.pageSize = pageSize;
		}

		public int getStartIndex() {
			return startIndex;
		}
		public void setStartIndex(int startIndex) {
			this.startIndex = startIndex;
		}

		public int getPageSize() {
			return pageSize;
		}
		public void setPageSize(int pageSize) {
			this.pageSize = pageSize;
		}
    }
    
	private List<WebAppOrder> convert( List<Bomorder> orders, boolean reverse ) throws Exception {
		
		List<WebAppOrder> result = new ArrayList<WebAppOrder>();

        for ( Bomorder entity : orders ) {
            
            result.add( new WebAppOrder( entity ) );
        }
        
        Collections.sort ( result, OrderDAO.ORDER_BY_DELIVERY_DATE );
        if(reverse) Collections.reverse( result );
        
        return result;
	}
	


}
