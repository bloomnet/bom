/**
 * 
 */
package com.bloomnet.bom.mvc.businessobjects;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.http.HttpSessionBindingEvent;

import com.bloomnet.bom.common.bean.FsiSecurityElementInterface;
import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.User;
import com.bloomnet.bom.common.entity.UserInterface;
import com.bloomnet.bom.common.entity.Userrole;

/**
 * A user object to represent a user logged in to the
 * BOM MVC web application.
 * 
 * @author Danil Svirchtchev
 *
 */
@SuppressWarnings("unchecked")
public class WebAppUser extends User implements WebAppUserInterface {
	
	private static final long serialVersionUID = 1L;

	private Long userId;
	private String userName;
	private String firstName;
	private String lastName;
	private String email;
	private String actionStr;
	private Set<ApplicationView> userViews;
	private Date signInTime;
	private Date signOutTime;
	private Shop myShop;
	private FsiSecurityElementInterface securityElement;
	private String orderNumber;
	private String timeInOrder;
	private  Map<Long,String> users;
	private String selectedRoleId;
	private String selectedDate;
	private String endDate;
	
	
	public WebAppUser() {
		// TODO Auto-generated constructor stub
	}
	
	
    public WebAppUser( User user ) {
		this();
		initialize( user );
	}

    
	/**
     * Sets the object fields from an interface
     */
	public void initialize ( UserInterface user ) {

		if ( user != null ) {
			
			this.userId    = user.getUserId();
			this.userName  = user.getUserName();
			this.firstName = user.getFirstName();
			this.lastName  = user.getLastName();
			this.email     = user.getEmail(); 
			
			this.setUserByCreatedUserId( user.getUserByCreatedUserId() );
			this.setPassword( user.getPassword() ); 
			this.setActive(user.getActive());
			this.setCreatedDate ( user.getCreatedDate() );
			this.setCreatedDate( user.getCreatedDate() );
			this.setUserrolesForUserId( user.getUserrolesForUserId() );
			this.setUseractivities( user.getUseractivities() );
		}
		
		this.setMyShop( new Shop() );
	}
	
	@Override
	public boolean isViewAllowed(String requestURI) throws Exception {
		
        boolean b = false;
        
        if ( userViews != null ) {
            
            for ( ApplicationView view : userViews ) {

                if ( requestURI.indexOf ( view.getUrl () ) != -1 ) {
                    
                    b = true;
                }
            }
        }
        
        return b;
	}
	
	@Override
	public void valueBound( HttpSessionBindingEvent event ) {
		
		List<WebAppUser> logins = (List<WebAppUser>) event.getSession().getServletContext().getAttribute("logins");
		
		if ( ! logins.contains(this) ) {
			logins.add(this);
		}
	}

	@Override
	public void valueUnbound( HttpSessionBindingEvent event ) {
		
		List<WebAppUser> logins = (List<WebAppUser>) event.getSession().getServletContext().getAttribute("logins");
		logins.remove(this);
	}

	@Override
	public String getActionStr() {
		return actionStr;
	}
	@Override
	public void setActionStr(String actionStr) {
		this.actionStr=actionStr;
	}

	@Override
	public Set<ApplicationView> getUserViews() {
		return userViews;
	}
	@Override
	public void setUserViews(Set<ApplicationView> userViews) {
		this.userViews=userViews;
	}

	@Override
	public void setSignInTime(Date signInTime) {
		this.signInTime = signInTime;
	}
	@Override
	public Date getSignInTime() {
		return signInTime;
	}
	
	@Override
	public void setSignOutTime(Date signOutTime) {
		this.signOutTime = signOutTime;
	}
	@Override
	public Date getSignOutTime() {
		return signOutTime;
	}
	
	@Override
	public Long getUserId() {
		return userId;
	}
	@Override
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	@Override
	public String getUserName() {
		return userName;
	}
	@Override
	public void setUserName(String userName) {
		this.userName = userName;
	}

	@Override
	public String getFirstName() {
		return firstName;
	}
	@Override
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	@Override
	public String getLastName() {
		return lastName;
	}
	@Override
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	@Override
	public String getEmail() {
		return email;
	}
	@Override
	public void setEmail(String email) {
		this.email = email;
	}

	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}


	public String getOrderNumber() {
		return orderNumber;
	}


	public void setSelectedRoleId(String selectedRoleId) {
		this.selectedRoleId = selectedRoleId;
	}


	public String getSelectedRoleId() {
		return selectedRoleId;
	}


	public void setSelectedDate(String selectedDate) {
		this.selectedDate = selectedDate;
	}


	public String getSelectedDate() {
		return selectedDate;
	}


	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}


	public String getEndDate() {
		return endDate;
	}


	public void setTimeInOrder(String timeInOrder) {
		this.timeInOrder = timeInOrder;
	}


	public String getTimeInOrder() {
		return timeInOrder;
	}
	

	public Role getHighestRole() {

		final Role result;
		
		List<Role> roles = new LinkedList<Role>();
		List<Userrole> userRoles = new ArrayList<Userrole>( this.getUserrolesForUserId() );
		
		for( Userrole userRole : userRoles ) {
			
			roles.add(userRole.getRole());
		}
		
		Collections.sort ( roles, WebAppUserInterface.ORDER_ROLES_BY_ID );
		Collections.reverse(roles);

		if(roles.isEmpty()){
			result = null;
		}
		else {
			result = roles.get(0);
		}
		
		return result;		
	}
	
	public List<Role> getRoles() {

		List<Role> roles;
		
		roles = new LinkedList<Role>();
		List<Userrole> userRoles = new ArrayList<Userrole>( this.getUserrolesForUserId() );
		
		for( Userrole userRole : userRoles ) {
			
			roles.add(userRole.getRole());
		}
		
		Collections.sort ( roles, WebAppUserInterface.ORDER_ROLES_BY_ID );
		Collections.reverse(roles);

		if(roles.isEmpty()){
			roles = null;
		}
		
		return roles;		
	}
	
	public void setUsers(Map<Long,String> users) {
		this.users = users;
	}


	public Map<Long,String> getUsers() {
		return users;
	}


	public void setMyShop(Shop myShop) {
		this.myShop = myShop;
	}

	public Shop getMyShop() {		
		return myShop;
	}

	public void setSecurityElement(FsiSecurityElementInterface securityElement) {
		this.securityElement = securityElement;
	}

	public FsiSecurityElementInterface getSecurityElement() {
		return securityElement;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email     == null) ? 0 : email.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName  == null) ? 0 : lastName.hashCode());
		result = prime * result + ((userId    == null) ? 0 : userId.hashCode());
		result = prime * result + ((userName  == null) ? 0 : userName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof WebAppUser)) {
			return false;
		}
		WebAppUser other = (WebAppUser) obj;
		if (email == null) {
			if (other.email != null) {
				return false;
			}
		} else if (!email.equals(other.email)) {
			return false;
		}
		if (firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		if (userId == null) {
			if (other.userId != null) {
				return false;
			}
		} else if (!userId.equals(other.userId)) {
			return false;
		}
		if (userName == null) {
			if (other.userName != null) {
				return false;
			}
		} else if (!userName.equals(other.userName)) {
			return false;
		}
		return true;
	}
	
	public String getFullName() {
		final String result = firstName+" "+lastName;
		return result;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("WebAppUser [userId=");
		builder.append(userId);
		builder.append(", userName=");
		builder.append(userName);
		builder.append(", firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", email=");
		builder.append(email);
		builder.append(", active=");
		builder.append(this.getActive());
		builder.append(", actionStr=");
		builder.append(actionStr);
		builder.append("]");
		return builder.toString();
	}
}
