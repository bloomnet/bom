package com.bloomnet.bom.mvc.businessobjects;

import java.util.Set;

/**
 * Interface to present an allowed HTML view.
 * 
 * @author Danil Svirchtchev
 *
 */
public interface WebAppUserViews {
	
	boolean isViewAllowed(String viewName) throws Exception;
	
	Set<ApplicationView> getUserViews();
	void setUserViews(Set<ApplicationView> userViews);
}
