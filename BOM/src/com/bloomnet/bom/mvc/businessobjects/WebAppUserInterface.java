package com.bloomnet.bom.mvc.businessobjects;

import java.util.Comparator;
import java.util.Date;

import javax.servlet.http.HttpSessionBindingListener;

import com.bloomnet.bom.common.entity.Role;
import com.bloomnet.bom.common.entity.UserInterface;

/**
 * A marker interface for a user of the web application
 * 
 * @author Danil Svirchtchev
 *
 */
public interface WebAppUserInterface extends UserInterface,
											 CommandClassInterface,
											 HttpSessionBindingListener,
											 WebAppUserViews {
    
    @SuppressWarnings("rawtypes")
    static final Comparator ORDER_ROLES_BY_ID = new Comparator () {
        
        public int compare ( Object o1, Object o2 ) {
            
            Role v1 = (Role)o1;
            Role v2 = (Role)o2;
            
            return v1.getRoleId ().compareTo ( v2.getRoleId () );
        }
    };
	
	void setSignInTime( Date signInTime );
	Date getSignInTime();
	
	void setSignOutTime( Date signInTime );
	Date getSignOutTime();
	
	Role getHighestRole();
}
