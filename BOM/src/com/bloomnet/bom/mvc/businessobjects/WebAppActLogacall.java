package com.bloomnet.bom.mvc.businessobjects;

import com.bloomnet.bom.common.entity.ActLogacall;
import com.bloomnet.bom.common.entity.Calldisp;
import com.bloomnet.bom.common.entity.Orderactivity;
import com.bloomnet.bom.common.entity.Shop;

public class WebAppActLogacall extends ActLogacall implements CommandClassInterface {

	private static final long serialVersionUID = 1L;
	
	private String actionStr;
	private String description;
	private String shopNetworkStatusId;
	private Shop shopCalled;

	public WebAppActLogacall() {
		super();
	}

	public WebAppActLogacall( Orderactivity orderactivity, 
			                  Calldisp calldisp,
		                  	  String logAcallText,
		                  	  Shop shopCalled ) {
		
		super(orderactivity, calldisp, logAcallText, shopCalled);
	}

	@Override
	public String getActionStr() {
		return actionStr;
	}
	@Override
	public void setActionStr(String actionStr) {
		this.actionStr = actionStr;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public String getShopNetworkStatusId() {
		return shopNetworkStatusId;
	}
	public void setShopNetworkStatusId(String shopNetworkStatusId) {
		this.shopNetworkStatusId = shopNetworkStatusId;
	}

	public void setShopCalled(Shop shopCalled) {
		this.shopCalled = shopCalled;
	}

	public Shop getShopCalled() {
		return shopCalled;
	}
}
