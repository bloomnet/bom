package com.bloomnet.bom.mvc.businessobjects;

import net.sf.ehcache.hibernate.HibernateUtil;

import com.bloomnet.bom.common.constants.BOMConstants;
import com.bloomnet.bom.common.entity.Shop;
import com.bloomnet.bom.common.entity.Shopnetwork;
import com.bloomnet.bom.common.entity.Shopnetworkstatus;
import com.bloomnet.bom.mvc.utils.HibernateHelper;

/**
 * A shop object to represent a BOM Shop in the BOM MVC web application.
 * 
 * @author Danil Svirchtchev
 *
 */
public class WebAppShop extends Shop implements CommandClassInterface {

	private static final long serialVersionUID = 1L;
	
	private String actionStr;
	private String selectedState;
	private int openSunday;
	private int hasWireService;
	private String coveredZip;
	private String network;
	private String shopCode;
	private String bmtShopCode;
	private String tfShopCode;
	private int bmtActive;
	private int tfActive;
	
	public WebAppShop(){
		super();
	}

	public WebAppShop(Shop myShop) {
		
		this.setShopId(myShop.getShopId());
		this.setUserByCreatedUserId( myShop.getUserByCreatedUserId() );
		this.setZip( myShop.getZip() );
		this.setCity( myShop.getCity() );
		this.setShopName( myShop.getShopName() );
		this.setShopAddress1( myShop.getShopAddress1() );
		this.setShopAddress2( myShop.getShopAddress2() );
		this.setShopPhone( myShop.getShopPhone() );
		this.setShopContact( myShop.getShopContact() );
		this.setShopFax( myShop.getShopFax() );
		this.setShop800( myShop.getShop800() );
		this.setShopEmail( myShop.getShopEmail() );
		this.setCreatedDate( myShop.getCreatedDate() );
		for(Shopnetwork network : myShop.getShopnetworks()){
			if(network.getNetwork().getNetworkId().equals(Long.valueOf("1"))){
				this.setBmtShopCode(network.getShopCode());
				if(network.getShopnetworkstatus().getShopNetworkStatusId().equals(BOMConstants.SHOPNETWORKSTATUS_ACTIVE)){
					this.setBmtActive(1);
				}else{
					this.setBmtActive(0);
				}
				if(network.getOpenSunday().equals(Boolean.valueOf("true"))){
					this.setOpenSunday(1);
				}else{
					this.setOpenSunday(0);
				}
				if(network.getCommmethod().getCommMethodId().equals(BOMConstants.COMMMETHOD_API)){
					this.setHasWireService(1);
				}else{
					this.setHasWireService(0);
				}
			}else if(network.getNetwork().getNetworkId().equals(Long.valueOf("2"))){
				this.setTfShopCode(network.getShopCode());
				if(network.getShopnetworkstatus().getShopNetworkStatusId().equals(BOMConstants.SHOPNETWORKSTATUS_ACTIVE)){
					this.setTfActive(1);
				}else{
					this.setTfActive(0);
				}
				if(network.getOpenSunday().equals(Boolean.valueOf("true"))){
					this.setOpenSunday(1);
				}else{
					this.setOpenSunday(0);
				}
				if(network.getCommmethod().getCommMethodId().equals(BOMConstants.COMMMETHOD_API)){
					this.setHasWireService(1);
				}else{
					this.setHasWireService(0);
				}
			}
			
		}
	}

	@Override
	public String getActionStr() {
		return actionStr;
	}
	@Override
	public void setActionStr(String actionStr) {
		this.actionStr=actionStr;
	}
	
	public Shop getShop() {
		
		Shop shop = new Shop();
		
		shop.setShopId( this.getShopId() );
		shop.setShopnetworks(this.getShopnetworks());
		shop.setUserByCreatedUserId( this.getUserByCreatedUserId() );
		shop.setZip( this.getZip() );
		shop.setCity( this.getCity() );
		shop.setShopName( this.getShopName() );
		shop.setShopAddress1( this.getShopAddress1() );
		shop.setShopAddress2( this.getShopAddress2() );
		shop.setShopPhone( this.getShopPhone() );
		shop.setShopContact( this.getShopContact() );
		shop.setShopFax( this.getShopFax() );
		shop.setShop800( this.getShop800() );
		shop.setShopEmail( this.getShopEmail() );
		shop.setCreatedDate( this.getCreatedDate() );
		
		return shop;
	}
	
	public Long getShopId(){
		return shopId;
	}
	
	public void setShopId(Long shopId){
		this.shopId = shopId;
	}
	
    public void setSelectedState(String selectedState) {
        this.selectedState = selectedState;
    }
    public String getSelectedState() {
        return selectedState;
    }

	public void setOpenSunday(int openSunday) {
		this.openSunday = openSunday;
	}
	public int getOpenSunday() {
		return openSunday;
	}

	public void setCoveredZip(String coveredZip) {
		this.coveredZip = coveredZip;
	}

	public String getCoveredZip() {
		return coveredZip;
	}

	public void setNetwork(String network) {
		this.network = network;
	}

	public String getNetwork() {
		return network;
	}

	public void setShopCode(String shopCode) {
		this.shopCode = shopCode;
	}

	public String getShopCode() {
		return shopCode;
	}

	public void setBmtShopCode(String bmtShopCode) {
		this.bmtShopCode = bmtShopCode;
	}

	public String getBmtShopCode() {
		return bmtShopCode;
	}

	public void setTfShopCode(String tfShopCode) {
		this.tfShopCode = tfShopCode;
	}

	public String getTfShopCode() {
		return tfShopCode;
	}

	public void setHasWireService(int hasWireService) {
		this.hasWireService = hasWireService;
	}

	public int getHasWireService() {
		return hasWireService;
	}

	public void setBmtActive(int bmtActive) {
		this.bmtActive = bmtActive;
	}

	public int getBmtActive() {
		return bmtActive;
	}

	public void setTfActive(int tfActive) {
		this.tfActive = tfActive;
	}

	public int getTfActive() {
		return tfActive;
	}

}
